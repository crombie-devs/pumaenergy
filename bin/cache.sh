#!/usr/bin/env bash

app/console cache:clear -v
app/console cache:warmup -v
