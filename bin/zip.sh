#!/usr/bin/env bash

rm app/cache/*/ -rf
rm web/bundles/* -rf
rm web/assetic/* -rf

composer dump-autoload -o
app/console assets:install -e prod
app/console assetic:dump -e prod

rm -f project.zip
7z a project.zip -tzip ./* -r -x@./bin/exclude.lst
