#!/usr/bin/env bash

php-cs-fixer fix --level=symfony --config=sf23 .
php-formatter formatter:use:sort src/
