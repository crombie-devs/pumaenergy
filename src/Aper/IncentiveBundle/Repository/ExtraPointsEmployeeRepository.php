<?php

namespace Aper\IncentiveBundle\Repository;

class ExtraPointsEmployeeRepository extends \Doctrine\ORM\EntityRepository
{

    public function deleteExtraPointsByYear($year){
        $qb = $this->createQueryBuilder("ExtraPointsEmployee");
        $qb->delete();
        $qb->where('ExtraPointsEmployee.year = :year')->setParameter('year', $year);
        return $qb->getQuery()->getResult();
    }

    public function findDisqualifiedStoreByYear($store,$year,$disqualified){
        $qb = $this->createQueryBuilder("ExtraPointsEmployee");
        $qb->where('ExtraPointsEmployee.year = :year')->setParameter('year', $year);
        $qb->andwhere('ExtraPointsEmployee.store = :store')->setParameter('store', $store);
        $qb->andwhere('ExtraPointsEmployee.disqualified = :disqualified')->setParameter('disqualified',$disqualified);
        return $qb->getQuery()->getOneOrNullResult();
    }

}