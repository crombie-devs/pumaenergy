<?php

namespace Aper\IncentiveBundle\Repository;

class ExtraPointsStoreRepository extends \Doctrine\ORM\EntityRepository
{

    public function deleteExtraPointsByYear($year){
        $qb = $this->createQueryBuilder("ExtraPointsStore");
        $qb->delete();
        $qb->where('ExtraPointsStore.year = :year')->setParameter('year', $year);
        return $qb->getQuery()->getResult();
    }

    public function findDisqualifiedStoreByYear($store,$year,$disqualified){
        $qb = $this->createQueryBuilder("ExtraPointsStore");
        $qb->where('ExtraPointsStore.year = :year')->setParameter('year', $year);
        $qb->andwhere('ExtraPointsStore.store = :store')->setParameter('store', $store);
        $qb->andwhere('ExtraPointsStore.disqualified = :disqualified')->setParameter('disqualified',$disqualified);
        return $qb->getQuery()->getOneOrNullResult();
    }

}
