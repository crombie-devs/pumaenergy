<?php

namespace Aper\CourseBundle\DataFixtures\ORM;

use Aper\IncentiveBundle\Entity\GoalType;
use Closure;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


class LoadGoalTypes extends AbstractFixture implements OrderedFixtureInterface
{
    /*
     * CORRER FIXTURE:
     php app/console doctrine:fixtures:load --fixtures src/Aper/IncentiveBundle/DataFixtures/ORM/loadGoalTypes.php --append
     */

    private $year = 2018;
    private $month = 02;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $types=array(
            array('Overshort abs. Puntos Objetivo', 'Concesiones', 'D2',$this->month,$this->year),
            array('PERCAP Puntos Objetivo', 'Concesiones', 'H2',$this->month,$this->year),
            array('INCIDENCIA DE BEBIDAS', 'Concesiones', 'L2',$this->month,$this->year),
            array('INCIDENCIA DE POCHOCLOS', 'Concesiones', 'P2',$this->month,$this->year),
            array('% COMBO MEGA', 'Concesiones', 'T2',$this->month,$this->year),
            array('MEJORA % PARTICIPACION DE ATM, WEB & BOLETERIA', 'Concesiones', 'X2',$this->month,$this->year),
            array('Objetivo Mensual', 'Concesiones', 'AB2',$this->month,$this->year),
            array('% VENTAS REFILL', 'Concesiones', 'AF2',$this->month,$this->year),
            array('MISTERY SHOPPER ', 'Concesiones', 'AJ2',$this->month,$this->year),
            array('Suscripciones Cine Fan Box','Concesiones','AN2',$this->month,$this->year),
            array('TOTAL_OBJETIVO', 'Concesiones', 'AR2',$this->month,$this->year),
            array('TOTAL_RESULTADO', 'Concesiones', 'AS2',$this->month,$this->year),
            array('Atención en podio ', 'Servicios', 'AT2',$this->month,$this->year),
            array('Experiencia en sala ', 'Servicios', 'AX2',$this->month,$this->year),
            array('Imagen interior', 'Servicios', 'BB2',$this->month,$this->year),
            array('Imagen Exterior ', 'Servicios', 'BF2',$this->month,$this->year),
            array('TOTAL_OBJETIVO', 'Servicios', 'BJ2',$this->month,$this->year),
            array('TOTAL_RESULTADO', 'Servicios', 'BK2',$this->month,$this->year),
            array('Sesiones cancelables', 'Proyección', 'BL2',$this->month,$this->year),
            array('Imagen', 'Proyección', 'BP2',$this->month,$this->year),
            array('Sonido', 'Proyección', 'BT2',$this->month,$this->year),
            array('TOTAL_OBJETIVO', 'Proyección', 'BX2',$this->month,$this->year),
            array('TOTAL_RESULTADO', 'Proyección', 'BY2',$this->month,$this->year),
            array('MISTERY', 'Mantenimiento', 'MISTERY',$this->month,$this->year),
            array('Estado general del piso de la sala ', 'Mantenimiento', 'BZ2',$this->month,$this->year),
            array('Iluminación de la sala', 'Mantenimiento', 'CD2',$this->month,$this->year),
            array('Presentación general de los pasillos de salas y lobby ', 'Mantenimiento', 'CH2',$this->month,$this->year),
            array('Estado de presentación del snack y boletería ', 'Mantenimiento', 'CL2',$this->month,$this->year),
            array('Estado general de los baños ', 'Mantenimiento', 'CP2',$this->month,$this->year),
            array('EXERT', 'Mantenimiento', 'EXERT',$this->month,$this->year),
            array('Estado general, señalética e iluminación de los vestuarios de personal ', 'Mantenimiento', 'CT2',$this->month,$this->year),
            array('Estado general del playroom ', 'Mantenimiento', 'CX2',$this->month,$this->year),
            array('Iluminación general de las salas ', 'Mantenimiento', 'DB2',$this->month,$this->year),
            array('Estado general de los pasillos de salidas de emergencia ', 'Mantenimiento', 'DF2',$this->month,$this->year),
            array('Control de elementos de lucha contra incendio ', 'Mantenimiento', 'DJ2',$this->month,$this->year),
            array('Estado general de los cuartos de lentes y de los fregaderos ', 'Mantenimiento', 'DN2',$this->month,$this->year),
            array('TOTAL_OBJETIVO', 'Mantenimiento', 'DR2',$this->month,$this->year),
            array('TOTAL_RESULTADO', 'Mantenimiento', 'DS2',$this->month,$this->year),
            array('TOTAL_CINE_OBJETIVO', 'TOTAL_CINE_OBJETIVO', 'DT2',$this->month,$this->year),
            array('TOTAL_CINE_RESULTADO', 'TOTAL_CINE_RESULTADO', 'DU2',$this->month,$this->year)
        );

        foreach($types as $data){
            $goalType = new GoalType();
            $goalType->setName($data[0]);
            $goalType->setCategory($data[1]);
            $goalType->setCell($data[2]);
            $goalType->setMonth($data[3]);
            $goalType->setYear($data[4]);

            $manager->persist($goalType);
            $manager->flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

}