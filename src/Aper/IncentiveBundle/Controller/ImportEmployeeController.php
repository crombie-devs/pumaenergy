<?php

namespace Aper\IncentiveBundle\Controller;
ini_set('max_execution_time', 3000);

use Aper\IncentiveBundle\Entity\EmployeePodium;
use Aper\IncentiveBundle\Entity\EmployeeImportLog;
//use Aper\IncentiveBundle\Entity\Podium;
use Aper\IncentiveBundle\Entity\EmployeeSpecialGoalMessage;
use Aper\IncentiveBundle\Form\EmployeeImportLogType;
use Aper\IncentiveBundle\Form\ImportLogStationType;
use Aper\IncentiveBundle\Form\ImportLogType;
use Aper\IncentiveBundle\Form\ImportAuditoryLogType;
use Aper\UserBundle\Entity\Employee;
use Aper\IncentiveBundle\Entity\ExtraPointsEmployee;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class ImportEmployeeController extends Controller
{

    const DIRECTORY = 'uploads/import_file/files';

    /**
     * Add a new xls file
     *
     * @Route()e("/", name="aper_importxls_employee_add")
     * @Method()d({"GET","POST"})
     */
    public function addAction(Request $request)
    {
        $errores= array();
        $importLog = new EmployeeImportLog();
        $importLog->setUser($this->get('security.token_storage')->getToken()->getUser());
        $importLog->setCreatedAt(new \DateTime("now"));

        $form = $this->createForm(EmployeeImportLogType::class, $importLog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
//            die('llego aca 0');
            $em->persist($importLog);
            $em->flush();
//            die('llego aca');
            $repository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeePodium');
            $month = $importLog->getMonth();
            $year = $importLog->getYear();
            $podium = $importLog->getPodium();

            $podio = $repository->findOneByMY($month, $year);

            if (!$podio)
                $podio = new EmployeePodium();

            $podio->setMonth($month);
            $podio->setYear($year);
            $podio->setPosition($podium);

            $em->persist($podio);
            $em->flush();

            $repository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeeSpecialGoalMessage');
            $month = $importLog->getMonth();
            $year = $importLog->getYear();
            $message = $importLog->getMessage();

            $specialGoalMessage = $repository->findOneByMonthYear($month, $year);

            // MENSAJE NO DEFINIDO - SE ELIMINA SI EXISTE
            if(empty($message)){
                if($specialGoalMessage !== null) {
                    $em->remove($specialGoalMessage);
                    $em->flush();
                }
            }
            else {
                if($specialGoalMessage === null)
                    $specialGoalMessage = new EmployeeSpecialGoalMessage();

                $specialGoalMessage->setMonth($month);
                $specialGoalMessage->setYear($year);
                $specialGoalMessage->setMessage($message);

                $em->persist($specialGoalMessage);
                $em->flush();
            }

            $errores = $this->get('importer_xls')->importEmployee($importLog);
            if (!$errores) {
                $this->addFlash('success', 'Archivo procesado con éxito 100%.');
            } else {
                $this->addFlash('success', 'Archivo procesado con errores');
            }

            return $this->render('IncentiveBundle:ImportEmployee:add.html.twig', array('errores'=>$errores, 'form' => $form->createView()));
        }

        return $this->render('IncentiveBundle:ImportEmployee:add.html.twig', array('errores'=>$errores,'form' => $form->createView()));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $importLog = $em->getRepository('IncentiveBundle:EmployeeImportLog')->find($id);
        $form = $this->createForm(EmployeeImportType::class, $importLog);
        $form->handleRequest($request);

        if(!$importLog){
            throw $this->createNotFoundException('Log no encontrado');
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $em->flush();

            $this->addFlash('mensaje', 'El Log ha sido modificado');

            return $this->redirectToRoute('cya_importxls_index');
        }

        return $this->render('IncentiveBundle:ImportLog:edit.html.twig', array('importlog' => $importLog, 'form' => $form->createView()));
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $importLog = $em->getRepository('IncentiveBundle:ImportLog')->find($id);
        $em->remove($importLog);
        $em->flush();
        $successMessage = 'Log eliminado';
        $this->addFlash('mensaje', $successMessage);

        return $this->redirectToRoute('cya_importxls_index');
    }

    public function indexAction(Request $request)
    {
        $importLog = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeeImportLog')->findLogs();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $importLog, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('IncentiveBundle:ImportEmployee:index.html.twig', array('pagination' => $pagination, 'importlog' => $importLog));
    }


    /**
     * @Route("/import_auditory_employee", name="aper_auditory_import_employee_new")
     * @Method({"GET", "POST"})
     */
    public function newAuditoryAction(Request $request)
    {
        $errores= array();
        //$years= $this->getDoctrine()->getRepository('IncentiveBundle:Ranking')->findYears();

        $form = $this->createForm(ImportAuditoryLogType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $year = $form["year"]->getData();
            $file = $form['file']->getData();
            $type = $form['type']->getData();

            $fileName = $file->getClientOriginalName();


            if(!$file->isValid()){
                $errores[]="El archivo no pudo ser subido correctamente";
            }
            else if(!$this->isCSV($file->getClientMimeType())){
                $errores[]="El archivo debe ser una plánilla de cálculo";
            }
            else {
                // MOVER ARCHIVO A DIRECTORIO DE UPLOADS - IMPORTS
                $file->move(ImportEmployeeController::DIRECTORY, $fileName);

                // PROCESAR PLANILLA
                $excelObj = $this->get('phpexcel')->createPHPExcelObject(ImportEmployeeController::DIRECTORY . '/' . $fileName);
                $sheet = $excelObj->getActiveSheet()->toArray(null, true, true, true);

                // LIMPIAR TABLA EXTRA POINTS STORE DEL AÑO SELECCIONADO
                $result = $this->getDoctrine()->getRepository(ExtraPointsEmployee::class)->deleteExtraPointsByYear($year);

                foreach ($sheet as $i => $row) {

                    /*
                     * ORDEN COLUMNAS
                     *      1 --> code store
                     *      2 --> puntaje
                     *      3 --> puntaje objetivo
                     *      4 --> descalificado (bool)
                     *      5 --> razon descalificación
                     */

                    //EVITANDO PRIMERA FILA CON NOMBRE DE COLUMNAS
                    if ($i > 0) {
                        $storeCode = $row["A"];
                        $storePoints = $row["B"];
                        $ptsObjetives = $row["C"];
                        $storeDisqualified = ((int)$row["D"] == 1) ? true : false;
                        $reason = !empty($row["E"]) ? $row["E"] : "";

                        // BUSCAR STORE POR CODIGO
                        $store = $this->getDoctrine()->getRepository(Employee::class)->findOneByCode($storeCode);

                        // SI EXISTE STORE CON CODIGO GUARDAMOS NUEVO EXTRA POINT
                        if ($store !== null) {
                            $newExtraPoints = new ExtraPointsEmployee();
                            $newExtraPoints->setEmployee($store);
                            $newExtraPoints->setYear($year);
                            $newExtraPoints->setReason($reason);
                            $newExtraPoints->setType($type);
                            $newExtraPoints->setPoints($storePoints);
                            $newExtraPoints->setPtsObjetives($ptsObjetives);
                            $newExtraPoints->setDisqualified($storeDisqualified);

                            $em = $this->getDoctrine()->getManager();
                            $em->persist($newExtraPoints);
                            $em->flush();
                        }
                    }
                }
            }

            if (!$errores) {
                $this->addFlash('success', 'Archivo procesado con éxito 100%.');
            }
            else {
                $this->addFlash('danger',implode("<br />",$errores));
            }

            return $this->render(
                'IncentiveBundle:ImportEmployee:add.html.twig',
                array(
                    'errores'=>$errores,
                    'form' => $form->createView()
                )
            );
        }

        return $this->render('IncentiveBundle:ImportEmployee:add.html.twig', array(
            'errores'=>$errores,
            'form' => $form->createView()
            )
        );
    }

    public function isCSV($mime){
        $allowedMimes=array(
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.ms-excel",
            "application/msexcel",
            "application/x-msexcel",
            "application/x-ms-excel",
            "application/x-excel",
            "application/x-dos_ms_excel",
            "application/xls",
            "application/x-xls"
        );

        return in_array($mime,$allowedMimes);
    }
}
