<?php

namespace Aper\IncentiveBundle\Controller;

use Aper\IncentiveBundle\Form\TermType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Aper\IncentiveBundle\Entity\Term;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class TermController extends Controller
{

    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $terms = $em->getRepository('IncentiveBundle:Term')->findall();
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $terms, $request->query->getInt('page', 1),
            1000
        );

        return $this->render('IncentiveBundle:Term:index.html.twig',
            array('pagination' => $pagination, 'terms' => $terms));

    }


    public function addAction(Request $request)
    {
        $term = new Term();
        $form = $this->createForm('Aper\IncentiveBundle\Form\TermType', $term);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($term);
            $em->flush($term);

            return $this->redirectToRoute('aper_term_show', array('id' => $term->getId()));
        }

        return $this->render('IncentiveBundle:Term:add.html.twig', array(
            'term' => $term,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $term = $em->getRepository('IncentiveBundle:Term')->find($id);
        $form = $this->createForm(TermType::class, $term);
        $form->handleRequest($request);

        if (!$term) {
            throw $this->createNotFoundException('Término/Condición no encontrado');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'El término/Condición ha sido modificado');

            return $this->redirectToRoute('aper_term_index');
        }

        return $this->render('IncentiveBundle:Term:update.html.twig', ['term' => $term, 'form' => $form->createView()]);
    }


    public function showAction($id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:Term');

        $term = $repository->find($id);

        if (!$term) {
            $messageException = 'Término/Condición no encontrado';
            throw $this->createNotFoundException($messageException);
        }

        return $this->render('IncentiveBundle:Term:show.html.twig', ['term' => $term]);
    }

    public function deleteAction($id, Request $request)
    {
            $em = $this->getDoctrine()->getManager();
            $term = $em->getRepository('IncentiveBundle:Term')->find($id);
            $em->remove($term);
            $em->flush();
            $successMessage = 'Término/Condición  eliminado';
            $this->addFlash('success', $successMessage);

        return $this->redirectToRoute('aper_term_index');
    }

}
