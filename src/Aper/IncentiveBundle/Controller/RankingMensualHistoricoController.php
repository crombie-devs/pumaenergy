<?php

namespace Aper\IncentiveBundle\Controller;


use Aper\StoreBundle\Entity\Store;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RankingMensualHistoricoController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request) {
        ini_set("memory_limit",-1);
        $selectorChoices = $this->getSelectorChoices2(new \DateTime());
        $defaultMonth = current($selectorChoices);
        $year = $request->query->get('year', $defaultMonth->format('Y'));
        $month = $request->query->get('month', $defaultMonth->format('n'));
        $selectedMonth = new \DateTime("{$year}-{$month}-01");

        $storeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');

        // BUSCAR ULTIMOS OBJETIVOS ANTERIORES AL AÑO ESTABLECIDO
        $lastUpload = $storeRepository->findLastUploadByMonth2BeforeYear(date("Y",time()));

        if(isset($lastUpload[0]))
            $lastUploadDate = new \DateTime("{$lastUpload[0]['y']}-{$lastUpload[0]['m']}-01");
        else
            $lastUploadDate = false;

        if(isset($lastUpload[0]))
            $lastUploadDate2 = new \DateTime("{$lastUpload[1]['y']}-{$lastUpload[1]['m']}-01");
        else
            $lastUploadDate2 = false;


        /* @var Store $store */
        $store = $this->getUser()->getEmployee()->getStore();
        $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
        $storeRankings = new ArrayCollection($storeRepository->findRankingByMonth($month, $year, 1000, $store->getZona()));

        $goalTypeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');
        $categories = $goalTypeRepository->getUniqueCategories();

        $inTop = false;

        foreach ($storeRankings as $rank) {
            if ($rank == $store) {
                $inTop = true;
            }
        }

        $selectorChoices = $this->getSelectorChoices(new \DateTime());

        $user = $this->get('security.token_storage')->getToken()->getUser();

        //Buscador de podio
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:Podium');
        $podio = $repository->findOneByMY($month, $year);

        if (!$podio)
            $podium = 3;
        else
            $podium= $podio->getPosition();
        
        //Buscador de mensaje objetivo mensual sorpresa
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:SpecialGoalMessage');
        $specialGoalMessage = $repository->findOneByMonthYear($month, $year);

        $textSpecialGoalMessage= (!$specialGoalMessage) ? "No está leyendo la base de datos" : $specialGoalMessage->getMessage();

        return $this->render('@Incentive/Frontend/Incentive/ajax/ajax_history_months_statistics.html.twig',
            compact('store', 'storeRankings', 'selectedMonth', 'selectorChoices','selectorYear', 'categories', 'lastUploadDate','lastUploadDate2','inTop', 'user', 'podium','textSpecialGoalMessage')
        );
    }

    /**
     * @param \DateTime $now
     * @return \DateTime[]
     */
    public function getSelectorChoices(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadByMonthBeforeYear(date("Y",time()));
        $year = $lastUpload[0]["y"];
        $month = $lastUpload[0]["m"];

        $now = new \DateTime("$year-$month-01");

        $choices = [];
        for ($i = 0; $i < 12; $i++) {
            $var = clone $now;
            $aux = $var->modify("-{$i} month");
    
            if($aux->format('Y') == $year)
                $choices[] = $aux;
        }

        foreach ($choices as $i => $choice){
            $month2 = $choice->format('n');
            $year2  = $choice->format('Y');
            $haveMonth = $goalRepository->findGoalsByMonth($month2,$year2);

            if ($haveMonth == null){
                unset($choices[$i]);
            }
            //EXCLUYE MESES QUE NO TENGAN RESULTADOS
            // else {
            //     $have = $goalRepository->find1GoalByMonth($month2,$year2);
            //     if ($have->getResult()==0){
            //         unset($choices[$i]);
            //     }
            // }
        }

        reset($choices);

        return $choices;
    }
    
    public function getSelectorChoices2(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadByMonthBeforeYear(date("Y",time()));

        $year = $lastUpload[0]["y"];
        $month = $lastUpload[0]["m"];

        $now = new \DateTime("$year-$month-01");

        $choices = [];
        // for ($i = 0; $i < 12; $i++) {
        //     $var = clone $now;
        //     $choices[] = $var->modify("-{$i} month");
        // }
        $choices[] = $now;
        return $choices;
    }
}
