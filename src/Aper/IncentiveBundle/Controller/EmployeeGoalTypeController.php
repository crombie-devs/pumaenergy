<?php

namespace Aper\IncentiveBundle\Controller;

use Aper\IncentiveBundle\Entity\EmployeeGoalType;
use Aper\IncentiveBundle\Entity\GoalType;
use Aper\IncentiveBundle\Form\DTO\ImportDTO;
use Aper\IncentiveBundle\Form\EmployeeGoalTypeType;
use Aper\IncentiveBundle\Form\EmployeeImportGoalTypType;
use Aper\IncentiveBundle\Form\GoalTypeType;
use Aper\IncentiveBundle\Form\ImportGoalTypType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Goaltype controller.
 *
 * @Route("/admin/goal-type-employee", name="goal_type_employee")
 */
class EmployeeGoalTypeController extends Controller
{
    /**
     * Lists all goalType entities.
     *
     * @Route("/index-gt-employee/", name="goal_type_employee_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $goalTypes = $em->getRepository('IncentiveBundle:EmployeeGoalType')->findBy([], [
            'year' => 'DESC',
            'month' => 'DESC',
            'cell' => 'ASC',

        ]);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $goalTypes,
            $request->query->getInt('page' , 1),
            10
        );

        return $this->render('@Incentive/EmployeeGoalType/index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * Creates a new goalType entity.
     *
     * @Route("/new", name="goal_type_employee_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $goalType = new EmployeeGoaltype();
        $form = $this->createForm(EmployeeGoalTypeType::class, $goalType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $goalType->setParent(
                $this->findParent($form->get('parentType')->getData(), $em, $goalType)
            );

            $em->persist($goalType);
            $em->flush();

            $this->addFlash('success', sprintf(
                'Se ha creado una nueva configuración para el tipo de objetivo "%s"',
                $goalType->getName()
            ));

            return $this->redirectToRoute('goal_type_employee_index');
        }

        return $this->render('@Incentive/EmployeeGoalType/new.html.twig', array(
            'goalType' => $goalType,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a goalType entity.
     *
     * @Route("/{id}", name="goal_type_employee_show")
     * @Method("GET")
     */
    public function showAction(EmployeeGoalType $goalType)
    {
        $deleteForm = $this->createDeleteForm($goalType);

        return $this->render('@Incentive/EmployeeGoalType/show.html.twig', array(
            'goalType' => $goalType,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing goalType entity.
     *
     * @Route("/{id}/edit", name="goal_type_employee_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, EmployeeGoalType $goalType)
    {
        $deleteForm = $this->createDeleteForm($goalType);
        $editForm = $this->createForm(EmployeeGoalTypeType::class, $goalType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $goalType->setParent(
                $this->findParent($editForm->get('parentType')->getData(), $em, $goalType)
            );

            $em->flush();

            $this->addFlash('success', sprintf(
                'Se ha editado una configuración para el tipo de objetivo "%s"',
                $goalType->getName()
            ));

            return $this->redirectToRoute('goal_type_employee_index');
        }

        return $this->render('@Incentive/EmployeeGoalType/edit.html.twig', array(
            'goalType' => $goalType,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a goalType entity.
     *
     * @Route("/{id}/delete", name="goal_type_employee_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, GoalType $goalType)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($goalType);
        $em->flush();

        return $this->redirectToRoute('goal_type_employee_index');
    }

    /**
     * Creates a form to delete a goalType entity.
     *
     * @param GoalType $goalType The goalType entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(GoalType $goalType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('goal_type_employee_delete', array('id' => $goalType->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Importer of Goal Types
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/import-gt-employee/", name="goal_type_employee_importer")
     * @Method({"GET", "POST"})
     */
    public function import(Request $request)
    {
        $import = new ImportDTO();
        $form = $this->createForm(EmployeeImportGoalTypType::class, $import);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $errors = $this->get('importer_goal_types_employee_xls')->import($import);

            if (empty($errors)) {
                $this->addFlash('success', 'La importación fue exitosa');
            } else {
                $this->addFlash('error', 'El archivo no se ha importado. Estos fueron los errores:');
                foreach ($errors as $error) {
                    $this->addFlash('error', $error);
                }
            }
        }


        return $this->render('@Incentive/EmployeeGoalType/import.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * @param string $parentType
     * @param ObjectManager $em
     * @param EmployeeGoalType $goalType
     * @return EmployeeGoalType|object|null
     */
    private function findParent($parentType, ObjectManager $em, EmployeeGoalType $goalType)
    {
        if (in_array($parentType, [GoalType::EXERT, GoalType::MYSTERY_SHOPPER])) {
            $parent = $em->getRepository('IncentiveBundle:EmployeeGoalType')->findOneBy([
                'name' => (string) $parentType === GoalType::EXERT ? 'EXERT' : 'MISTERY',
                'year' => $goalType->getYear(),
                'month' => $goalType->getMonth(),
            ]);

            if (null === $parent) {
                $parent = (string) $parentType === EmployeeGoalType::EXERT
                    ? EmployeeGoalType::createExert($goalType->getYear(), $goalType->getMonth())
                    : EmployeeGoalType::createMisteryShooper($goalType->getYear(), $goalType->getMonth());

                $em->persist($parent);
            }
        } else {
            $parent = null;
        }

        return $parent;
    }
}
