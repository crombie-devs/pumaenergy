<?php

namespace Aper\IncentiveBundle\Controller;
ini_set('max_execution_time', 3000);

use Aper\IncentiveBundle\Entity\ImportLog;
use Aper\IncentiveBundle\Entity\Podium;
use Aper\IncentiveBundle\Entity\SpecialGoalMessage;
use Aper\IncentiveBundle\Form\ImportLogStationType;
use Aper\IncentiveBundle\Form\ImportLogType;
use Aper\IncentiveBundle\Form\ImportAuditoryLogType;
use Aper\StoreBundle\Entity\Store;
use Aper\IncentiveBundle\Entity\ExtraPointsStore;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class ImportController extends Controller
{

    const DIRECTORY = 'uploads/import_file/files';

    /**
     * Add a new xls file
     *
     * @Route()e("/", name="aper_importxls_add")
     * @Method()d({"GET","POST"})
     */
    public function addAction(Request $request)
    {

        // $entrada = array("a", "b", "c", "d", "e");

        // $salida1 = array_slice($entrada, 0, 3);      // devuelve "c", "d", y "e"
        // $salida2 = array_slice($entrada, 3, 3);      // devuelve "c", "d", y "e"

        // print_r($salida1);
        // print_r($salida2);
        // exit;


        $errores= array();
        $importLog = new ImportLog();
        $importLog->setUser($this->get('security.token_storage')->getToken()->getUser());
        $importLog->setCreatedAt(new \DateTime("now"));

        $form = $this->createForm(ImportLogType::class, $importLog);
        $form->handleRequest($request);

        if (isset($_GET['page']) || ($form->isSubmitted() && $form->isValid())) {

            if (!isset($_GET['page'])) {
                $em = $this->getDoctrine()->getManager();

                $em->persist($importLog);
                $em->flush();

                $repository = $this->getDoctrine()->getRepository('IncentiveBundle:Podium');
                $month = $importLog->getMonth();
                $year = $importLog->getYear();
                $podium = $importLog->getPodium();

                $podio = $repository->findOneByMY($month, $year);

                if (!$podio)
                    $podio = new Podium();

                $podio->setMonth($month);
                $podio->setYear($year);
                $podio->setPosition($podium);

                $em->persist($podio);
                $em->flush();

                $repository = $this->getDoctrine()->getRepository('IncentiveBundle:SpecialGoalMessage');
                $month = $importLog->getMonth();
                $year = $importLog->getYear();
                $message = $importLog->getMessage();

                $specialGoalMessage = $repository->findOneByMonthYear($month, $year);

                // MENSAJE NO DEFINIDO - SE ELIMINA SI EXISTE
                if(empty($message)){
                    if($specialGoalMessage !== null) {
                        $em->remove($specialGoalMessage);
                        $em->flush();
                    }
                }
                else {
                    if($specialGoalMessage === null)
                        $specialGoalMessage = new SpecialGoalMessage();

                    $specialGoalMessage->setMonth($month);
                    $specialGoalMessage->setYear($year);
                    $specialGoalMessage->setMessage($message);

                    $em->persist($specialGoalMessage);
                    $em->flush();
                }
    
                $errores = $this->get('importer_xls')->import($importLog);
            } else {
                $importLog = $this->getDoctrine()->getRepository('IncentiveBundle:ImportLog')->findLogs();
                $importLog = $importLog[0];

                $errores = $this->get('importer_xls')->import($importLog);
            }


            if (!$errores) {
                $this->addFlash('success', 'Archivo procesado con éxito 100%.');
            } else {
                // var_dump($errores);exit;
                $this->addFlash('success', 'Archivo procesado con errores');
            }

            return $this->render('IncentiveBundle:Import:add.html.twig', array('errores'=>$errores, 'form' => $form->createView()));
        }

        return $this->render('IncentiveBundle:Import:add.html.twig', array('errores'=>$errores,'form' => $form->createView()));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $importLog = $em->getRepository('IncentiveBundle:ImportLog')->find($id);
        $form = $this->createForm(ImportType::class, $importLog);
        $form->handleRequest($request);

        if(!$importLog){
            throw $this->createNotFoundException('Log no encontrado');
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $em->flush();

            $this->addFlash('mensaje', 'El Log ha sido modificado');

            return $this->redirectToRoute('cya_importxls_index');
        }

        return $this->render('IncentiveBundle:ImportLog:edit.html.twig', array('importlog' => $importLog, 'form' => $form->createView()));
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $importLog = $em->getRepository('IncentiveBundle:ImportLog')->find($id);
        $em->remove($importLog);
        $em->flush();
        $successMessage = 'Log eliminado';
        $this->addFlash('mensaje', $successMessage);

        return $this->redirectToRoute('cya_importxls_index');
    }

    public function indexAction(Request $request)
    {
        $importLog = $this->getDoctrine()->getRepository('IncentiveBundle:ImportLog')->findLogs();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $importLog, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('IncentiveBundle:Import:index.html.twig', array('pagination' => $pagination, 'importlog' => $importLog));
    }

    /**
     * @Route("/import_auditory", name="aper_auditory_import_new")
     * @Method({"GET", "POST"})
     */
    public function newAuditoryAction(Request $request)
    {
        $errores= array();
        //$years= $this->getDoctrine()->getRepository('IncentiveBundle:Ranking')->findYears();

        $form = $this->createForm(ImportAuditoryLogType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $year = $form["year"]->getData();
            $file = $form['file']->getData();
            $type = $form['type']->getData();

            $fileName = $file->getClientOriginalName();


            if(!$file->isValid()){
                $errores[]="El archivo no pudo ser subido correctamente";
            }
            else if(!$this->isCSV($file->getClientMimeType())){
                $errores[]="El archivo debe ser una plánilla de cálculo";
            }
            else {
                // MOVER ARCHIVO A DIRECTORIO DE UPLOADS - IMPORTS
                $file->move(ImportController::DIRECTORY, $fileName);

                // PROCESAR PLANILLA
                $excelObj = $this->get('phpexcel')->createPHPExcelObject(ImportController::DIRECTORY . '/' . $fileName);
                $sheet = $excelObj->getActiveSheet()->toArray(null, true, true, true);

                // LIMPIAR TABLA EXTRA POINTS STORE DEL AÑO SELECCIONADO
                $result = $this->getDoctrine()->getRepository(ExtraPointsStore::class)->deleteExtraPointsByYear($year);

                foreach ($sheet as $i => $row) {

                    /*
                     * ORDEN COLUMNAS
                     *      1 --> code store
                     *      2 --> puntaje
                     *      3 --> puntaje objetivo
                     *      4 --> descalificado (bool)
                     *      5 --> razon descalificación
                     */

                    //EVITANDO PRIMERA FILA CON NOMBRE DE COLUMNAS
                    if ($i > 0) {
                        $storeCode = $row["A"];
                        $storePoints = $row["B"];
                        $ptsObjetives = $row["C"];
                        $storeDisqualified = ((int)$row["D"] == 1) ? true : false;
                        $reason = !empty($row["E"]) ? $row["E"] : "";

                        // BUSCAR STORE POR CODIGO
                        $store = $this->getDoctrine()->getRepository(Store::class)->findOneByCode($storeCode);

                        // SI EXISTE STORE CON CODIGO GUARDAMOS NUEVO EXTRA POINT
                        if ($store !== null) {
                            $newExtraPoints = new ExtraPointsStore();
                            $newExtraPoints->setStore($store);
                            $newExtraPoints->setYear($year);
                            $newExtraPoints->setReason($reason);
                            $newExtraPoints->setType($type);
                            $newExtraPoints->setPoints($storePoints);
                            $newExtraPoints->setPtsObjetives($ptsObjetives);
                            $newExtraPoints->setDisqualified($storeDisqualified);

                            $em = $this->getDoctrine()->getManager();
                            $em->persist($newExtraPoints);
                            $em->flush();
                        }
                    }
                }
            }

            if (!$errores) {
                $this->addFlash('success', 'Archivo procesado con éxito 100%.');
            }
            else {
                $this->addFlash('danger',implode("<br />",$errores));
            }

            return $this->render(
                'IncentiveBundle:Import:add.html.twig',
                array(
                    'errores'=>$errores,
                    'form' => $form->createView()
                )
            );
        }

        return $this->render('IncentiveBundle:Import:add.html.twig', array(
            'errores'=>$errores,
            'form' => $form->createView()
            )
        );
    }

    public function isCSV($mime){
        $allowedMimes=array(
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.ms-excel",
            "application/msexcel",
            "application/x-msexcel",
            "application/x-ms-excel",
            "application/x-excel",
            "application/x-dos_ms_excel",
            "application/xls",
            "application/x-xls"
        );

        return in_array($mime,$allowedMimes);
    }
}
