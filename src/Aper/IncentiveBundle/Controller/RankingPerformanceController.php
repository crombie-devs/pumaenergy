<?php

namespace Aper\IncentiveBundle\Controller;


use Aper\StoreBundle\Entity\Store;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Aper\IncentiveBundle\Entity\ImportLog;


class RankingPerformanceController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {

        if (isset($_GET['userlegal'])) {
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $user->setLegales();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            echo json_encode(array('status' => 1));
            exit;
        }


        ini_set("memory_limit",-1);
        if (isset($_GET['json']) && $_GET['json'] && isset($_GET['cat']) && !empty($_GET['cat']) && isset($_GET['store']) && !empty($_GET['store'])) {

            $selectorChoices = $this->getSelectorChoices2(new \DateTime()); //Lista de meses con resultados!
            $defaultMonth = current($selectorChoices);
            $year = $request->query->get('year', $defaultMonth->format('Y'));
            $month = $request->query->get('month', $defaultMonth->format('n'));
            $selectedMonth = new \DateTime("{$year}-{$month}-01");
			

            $result = [];

            $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
            $store = $storeRepository->findOneById((int) $_GET['store']);

            $goalTypeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');
            $categories = $goalTypeRepository->getUniqueCategories();



            $result['porcentajeTotal'] = 34;
            $result['promedioTotal'] = 44;
            $result['leyendaCalidad'] = false;
            $result['items'] = [];


            //Buscamos el promedio de cada una de las variables de TODAS LA RED
            $repository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
            list($promediosRed, $promediosRedCategory) = $repository->findGoalsAVG($month, $year);


            foreach ($categories as $key => $type) {
                if ($_GET['cat'] == $type) {
                    $total_objective = 0;
					
                    if ($goal = $store->getGoalsByMonthAndName($selectedMonth, 'PUNTAJES OBJETIVO', $type)){
                        $currentGoal = current($goal);
                        $goal = array_shift($currentGoal);
                        if ($goal) {
                            $total_objective = $goal->getObjetive();
                        }

                        // $total_objective = $goal->getObjetive();
                    }

                    $total_result = 0;
                    if ($goal = $store->getGoalsByMonthAndName($selectedMonth, 'PUNTAJES RESULTADO', $type)){
                        $currentGoal = current($goal);
                        $goal = array_shift($currentGoal);
                        if ($goal) {
                            $total_result = $goal->getResult();
                        }
                        // $total_objective = $goal->getObjetive();
                    }

                    $items = $store->getGoalsByMonthAndCategory($selectedMonth, $type);

                    foreach ($items as $key => $item) {
                        if ($item->getGoalType()->getName() != 'PUNTAJES OBJETIVO' &&  $item->getGoalType()->getName() != 'PUNTAJES RESULTADO' && (ucfirst($item->getGoalType()->getMonth())) == $month && (ucfirst($item->getGoalType()->getYear())) == $year ) {

                            $_item = [];
							
							$_item['month'] = (ucfirst($item->getGoalType()->getMonth()));
							$_item['year'] = (ucfirst($item->getGoalType()->getYear()));
                            $_item['id_g'] = (ucfirst($item->getId()));
                            $_item['id_gt'] = (ucfirst($item->getGoalType()->getId()));
                            $_item['titulo'] = (ucfirst($item->getGoalType()->getName()));
                            $_item['especial'] = ($item->getGoalType()->isSpecial());
							$_item['textoinfo']   = ($item->getGoalType()->getPopupText());
                            if ($item->getGoalType()->getCategory() == 'OBJETIVOS DE VENTA' || $item->getGoalType()->getCategory() == 'CALIDAD, IMAGEN E INFRAESTRUCTURA' || $item->getGoalType()->getCategory() == 'CAPACITACIÓN') {
                                $_item['cumple'] = round($item->getResultMet());
                                $_item['objetivo'] = round($item->getObjetiveMet());
                            } else {
                                $_item['cumple'] = round($item->getResult());
                                $_item['objetivo'] = round($item->getObjetive());
                            }
                            $_item['promedio'] = (isset($promediosRed[$item->getGoalType()->getId()]) ? $promediosRed[$item->getGoalType()->getId()] : 0);
                            // print_r($_item);exit;
                            $result['items'][] = $_item;
                        }
                    }

                }
                    // {% include '@Incentive/Frontend/Incentive/widgetsEmployee/points-table.html.twig' with {
                    // 'render_values': true,
                    // 'title': $type,
                    // 'total_objetive': $store->getGoalsByMonthAndName($selectedMonth, 'PUNTAJES OBJETIVO', $type).empty ? 0 : $store->getGoalsByMonthAndName($selectedMonth, 'PUNTAJES OBJETIVO', $type).first.result,
                    // 'total_result': $store->getGoalsByMonthAndName($selectedMonth, 'PUNTAJES RESULTADO', $type).empty ? 0 : $store->getGoalsByMonthAndName($selectedMonth, 'PUNTAJES RESULTADO', $type).first.result,
                    // 'items': $store->getGoalsByMonthAndCategory($selectedMonth, $type),
                    // 'message': textSpecialGoalMessageSelector,
                    // } only %}

            }

            echo json_encode($result);
            exit;
            // echo 'aca';exit;
        }

        $selectorChoices = $this->getSelectorChoices2(new \DateTime());
        $defaultMonth = current($selectorChoices);
        $year = $request->query->get('year', $defaultMonth->format('Y'));
        $month = $request->query->get('month', $defaultMonth->format('n'));

        $selectedYear = new \DateTime("{$year}-{$month}-01");

        $storeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $storeRepository->findLastUploadByMonth2();

        if($lastUpload){
            $lastUploadDate = new \DateTime("{$lastUpload[0]['y']}-{$lastUpload[0]['m']}-01");
        }


        $user = $this->get('security.token_storage')->getToken()->getUser();
        // $responsable = 'Sebastian Castro';

        $searchZona = (isset($_GET['zone']) ? $_GET['zone'] : null);
        $idStores = null;

        if ($user->getMaxRoleType() == 'ROLE_HEAD_OFFICE') {
            $responsable = $user->getId();
            $idStore = null;
        } elseif ($user->getMaxRoleType() == 'ROLE_MANAGER') {
            $responsable = null;
            $idStore = null;
            $idStores = $this->getUser()->getEmployee()->getStores();
            if (empty($idStores)) {
                $idStore = $this->getUser()->getEmployee()->getStore()->getId();
            } else {
                $idStores = explode(',', $idStores);
                $idStores[] = $this->getUser()->getEmployee()->getStore()->getCode();
            }
        } else {
            $responsable = null;
            $idStore = null;
            if (is_null($searchZona)) {
                $searchZona = 'Centro';
            }
        }


        /* @var Store $store */
        $store = $this->getUser()->getEmployee()->getStore();

        $goalTypeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');

        $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');

        $storeRankings = new ArrayCollection($storeRepository->findPerformance($month, $year, 1000, $responsable, $searchZona, $idStore, $idStores));
        $categories = $goalTypeRepository->getUniqueCategories();
		
		//var_dump($categories); exit;

        $lastGoal = $goalTypeRepository->findLastObjetivesBeforeMonthAndYear($month, $year);
        $_month = $month;
        $_year = $year;
        if ($lastGoal) {
            $_month = $lastGoal[0]->getMonth();
            $_year = $lastGoal[0]->getYear();

        }
        $items = $goalTypeRepository->findByMonthAndYearSpecific($_year, $_month);
        $itemsList = [];
        $itemsListOtros = [];
        foreach ($items as $key => $value) {
            $cat = $value->getCategory();
            if ($cat != 'TOTAL_STORE_OBJETIVO' && $cat != 'TOTAL_STORE_RESULTADO') {
                $itemsList[$cat][] = $value;
                $itemsListOtros[$cat][$value->getId()] = $value->getName();
            }
        }
        // print_r($itemsListOtros);

        $storesComplete = new ArrayCollection($storeRepository->findStoresByRepresentante($responsable, $searchZona, $idStore, $idStores));


        //Buscamos el promedio de cada una de las variables de TODAS LA RED
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        list($promediosRed, $promediosRedCategory) = $repository->findGoalsAVG($month, $year);

        // if (isset($_GET['lucas'])) {
        //     var_dump($promediosRed);
        //     var_dump($promediosRedCategory);exit;
        // }

        $promediosRedCat = [];
        $_global_objetivo = 0;
        $_global_promedio = 0;
        $_red_promedio_total = 0;
        foreach ($promediosRedCategory as $catName => $values) {
            $_objetivo = 0;
            $_promedio = 0;
            foreach ($values as $key => $value) {
                if ($catName != 'MYSTERY SHOPPER' || ($catName == 'MYSTERY SHOPPER' && $value['special'])) {
                    $_objetivo+= $value['objetivo'];
                    $_promedio+= $value['promedio'];
                    $_global_objetivo+= $value['objetivo'];
                    $_global_promedio+= $value['promedio'];
                }
            }

            $promediosRedCat[$catName] = ($_objetivo ? round($_promedio * 100 / $_objetivo) : 0);
            if ($promediosRedCat[$catName] > 100) {
                $promediosRedCat[$catName] = 100;
            }
            $_red_promedio_total += $promediosRedCat[$catName];
        }

        // print_r($promediosRedCategory);

        $promedioGlobal = ($_global_objetivo ? round($_global_promedio * 100 / $_global_objetivo) : 0);
        // $promedioGlobal = ($_global_objetivo ? round($_red_promedio_total * 100 / $_global_objetivo) : 0);




        //Buscamos el promedio de cada una de las variables de TODAS LAS ESTACIONES QUE PERTENECEN AL USUARIO
        $storesIds = [];
        $zonas = [];
        foreach ($storeRankings as $key => $value) {
            $storesIds[] = $value->getId();

            $nZone = ucfirst(strtolower($value->getZona()));

            $zonas[$nZone] = $nZone;
        }
        $zonas = implode(' / ', $zonas);

        list($promediosEstaciones, $promediosEstacionesCategory) = $repository->findGoalsAVGStores($month, $year, $storesIds);


        // if (isset($_GET['lucas'])) {
        //     var_dump($promediosEstaciones);
        //     var_dump($promediosEstacionesCategory);exit;
        // }

        $promediosEstacionesCat = [];
        $_global_objetivo_estaciones = 0;
        $_global_promedio_estaciones = 0;


        $_global_promedio_total = 0;
        foreach ($promediosEstacionesCategory as $catName => $values) {
            $_objetivo = 0.000001; //Cuidado con esto!!!
            $_promedio = 0;
            $_porcentaje = 0;
            $_porcentajeMs = 0;
                // var_dump($values);
            foreach ($values as $key => $value) {

                // if ($catName != 'MYSTERY SHOPPER' || ($catName == 'MYSTERY SHOPPER' && $value['special'])) {
                if ($catName != 'MYSTERY SHOPPER') {
                    $_objetivo+= $value['objetivo'];
                    $_promedio+= $value['promedio'];
                    $_porcentaje+= $value['porcentaje'];

                    $_global_objetivo_estaciones+= $value['objetivo'];
                    $_global_promedio_estaciones+= $value['promedio'];
                } elseif ($catName == 'MYSTERY SHOPPER' && $value['nombre'] == 'PUNTAJES RESULTADO') {
                    $_porcentajeMs = $value['promedio'];
                }
            }

            if ($catName != 'MYSTERY SHOPPER') {
				//var_dump($catName);//exit;
                $promediosEstacionesCat[$catName] = round($_promedio * 100 / $_objetivo);
            } else {
                $promediosEstacionesCat[$catName] = round($_porcentajeMs);
            }
            if ($promediosEstacionesCat[$catName] > 100) {
                $promediosEstacionesCat[$catName] = 100;
            }
            $_global_promedio_total += $promediosEstacionesCat[$catName];
        }
        // print_r($promediosEstaciones);
        // print_r($promediosEstacionesCategory);


        $promedioGlobalEstaciones = 0;
        if ($_global_objetivo_estaciones) {
            $promedioGlobalEstaciones = round($_global_promedio_estaciones * 100 / $_global_objetivo_estaciones);
        }

        $promedioGlobalEstaciones = round($_global_promedio_total / 4);


        $goalTypeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');
        $categories = $goalTypeRepository->getUniqueCategories();


        $_globalStoreRankings = [];
        foreach ($categories as $i => $nameCat) {

            $_storeRankings = [];
            $_storeRankingsPos = [];

            $_tempStores = [];
			
			//var_dump ($selectedYear);exit;
            foreach ($storeRankings as $key => $store) {
                $_totalPoints = 0;
                if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'PUNTAJES OBJETIVO', $nameCat)){
                    $currentGoal = current($goal);
                    $goal = array_shift($currentGoal);
                    if ($goal)
                        $_totalPoints = $goal->getObjetive();
                }

                $_completedPoints = 0;
                if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'PUNTAJES RESULTADO', $nameCat)){
                    $currentGoal = current($goal);
                    $goal = array_shift($currentGoal);
                    if ($goal) {
                        if ($goal->getGoalType()->getCategory() == 'MYSTERY SHOPPER') {
                            $_completedPoints = $goal->getResultMet();
                        } else {
                            $_completedPoints = $goal->getResult();
                        }
                    }
                }

                $_stoRank = 0;
                if ($ss = $store->getRankingByMonthAndYear($selectedYear, $store)){
                    $currentSs = current($ss);
                    $ss = array_shift($currentSs);
                    if ($ss){
                        $_stoRank = $ss->getPosition();
                    }

                    // $_stoRank = $store->getRankingByMonthAndYear($selectedYear, $store)[0]->getPosition();
                }

                $_completedPointsReal = $_completedPoints;
                if ($_completedPoints < 0) {
                    $_completedPoints = 0;
                }

                $n = 0;
				
                if ($_totalPoints != 0)  {
                    if ($_completedPoints == $_totalPoints) {
						//Cuidado con esto por que si son iguales, da 1 por misma division y se rompe la logica; con el $n en una cantidad infima distinta de 1 anda piola. Rolfi 27/12/21	
                        $_completedPoints = number_format($_completedPoints-0.000000000001,12);
						//$n = number_format(($_completedPoints-0.00000001) / $_totalPoints, 12);
                        $n = 0.999999999999;
                        // if (isset($_GET['lucas'])) {	
                        //     print_r($_completedPoints);
                        //   // print_r(' ee:');  print_r($n);  
                        // }
                    } else {
						$_completedPoints = number_format($_completedPoints,12);
                        $n = number_format($_completedPoints / $_totalPoints, 12);
                          // print_r(' aa:');  print_r($n);  
                    }
                }
                $num = explode('.', $n);

                $_position = $num[0];
                if (isset($num[1])) $_position = $num[1];

                $_position = substr($_position, 0, 12);
                if ($_position == 1) $_position = 100;

                $_storeRankingsPos[] = $_position;

                $aa =[];
                $aa['store'] = $store;
                $aa['totalPoints'] = $_totalPoints;
                $aa['completedPoints'] = $_completedPoints;
                $aa['completedPointsReal'] = $_completedPointsReal;
                $aa['stoRank'] = $_stoRank;

                $_storeRankings[] = $aa;

                $_tempStores[$store->getId()] = $store->getId();
            }

            // foreach ($storesComplete as $key => $store) {
            //     if (!isset($_tempStores[$store->getId()])) {
            //         $aa =[];
            //         $aa['store'] = $store;
            //         $aa['totalPoints'] = 0;
            //         $aa['completedPoints'] = 0;
            //         $aa['stoRank'] = 0;
            //         $_storeRankings[] = $aa;
            //         $_storeRankingsPos[] = 0;
            //     }
            // }

            asort($_storeRankingsPos, SORT_NUMERIC);

            $finalPos = array_reverse($_storeRankingsPos, true);
            
            $_tempStoreRankings = [];
            foreach ($finalPos as $key => $value) {
                $_tempStoreRankings[] = $_storeRankings[$key];
            }

            foreach ($storesComplete as $key => $store) {
                if (!isset($_tempStores[$store->getId()])) {
                    $aa =[];
                    $aa['store'] = $store;
                    $aa['totalPoints'] = 0;
                    $aa['completedPoints'] = 0;
                    $aa['completedPointsReal'] = 0;
                    $aa['stoRank'] = 0;
                    $_tempStoreRankings[] = $aa;
                    // $_storeRankingsPos[] = 0;
                }
            }


            $lastSort = [];
            $negativeSort = [];

            foreach($_tempStoreRankings as $aa) {
                if ($aa['completedPointsReal'] >= 0) {
                    $lastSort[] = $aa;
                } else {
                    $negativeSort[abs($aa['completedPointsReal'])] = $aa;
                }
            }

            ksort($negativeSort);

            $_tempStoreRankings = array_merge($lastSort, $negativeSort);


            $_globalStoreRankings[$nameCat] = $_tempStoreRankings;

        }

        $storeRankings = $_globalStoreRankings;


        $_promedioEstacion = 0;
        foreach ($storeRankings['MYSTERY SHOPPER'] as $key => $station) {
            $totalPoints = $station['totalPoints'];
            $completedPoints = $station['completedPoints'];
            $_promedioEstacion += $totalPoints ? ($completedPoints * 100) / $totalPoints : 0;
			//var_dump($station);
        }
        // $promediosEstacionesCat['MYSTERY SHOPPER'] = round($_promedioEstacion ? $_promedioEstacion / count($storeRankings['MYSTERY SHOPPER']) : 0);
// var_dump($promediosEstacionesCat);

        // $_promedioEstacion = 0;
        // foreach ($storeRankings['OBJETIVOS DE VENTA'] as $key => $station) {
        //     $totalPoints = $station['totalPoints'];
        //     $completedPoints = $station['completedPoints'];
        //     $_promedioEstacion += $totalPoints ? ($completedPoints * 100) / $totalPoints : 0;
        // }
        // $promediosEstacionesCat['OBJETIVOS DE VENTA'] = round($_promedioEstacion ? $_promedioEstacion / count($storeRankings['OBJETIVOS DE VENTA']) : 0);

        // $_promedioEstacion = 0;
        // foreach ($storeRankings['CALIDAD, IMAGEN E INFRAESTRUCTURA'] as $key => $station) {
        //     $totalPoints = $station['totalPoints'];
        //     $completedPoints = $station['completedPoints'];
        //     $_promedioEstacion += $totalPoints ? ($completedPoints * 100) / $totalPoints : 0;
        // }
        // $promediosEstacionesCat['CALIDAD, IMAGEN E INFRAESTRUCTURA'] = round($_promedioEstacion ? $_promedioEstacion / count($storeRankings['CALIDAD, IMAGEN E INFRAESTRUCTURA']) : 0);

        // $_promedioEstacion = 0;
        // foreach ($storeRankings['CAPACITACIÓN'] as $key => $station) {
        //     $totalPoints = $station['totalPoints'];
        //     $completedPoints = $station['completedPoints'];
        //     $_promedioEstacion += $totalPoints ? ($completedPoints * 100) / $totalPoints : 0;
        // }
        // $promediosEstacionesCat['CAPACITACIÓN'] = round($_promedioEstacion ? $_promedioEstacion / count($storeRankings['CAPACITACIÓN']) : 0);



        $selectorChoices = $this->getSelectorChoices(new \DateTime()); //Lista de meses con resultados!

        if (!$zonas) {
            $zonas = [];
            foreach ($storesComplete as $key => $value) {
                $nZone = ucfirst(strtolower($value->getZona()));
                $zonas[$nZone] = $nZone;
            }
            $zonas = implode(' / ', $zonas);

        }


        $promedGlobal = 0;
        foreach ($categories as $key => $cName) {
            $promedGlobal += (isset($promediosRedCat[$cName]) ? $promediosRedCat[$cName] : 0);
            // promedGlobal
        }

        $promedioGlobal = round($promedGlobal / 4);


        $_proMys = (isset($promediosRedCat['MYSTERY SHOPPER']) ? $promediosRedCat['MYSTERY SHOPPER']*40/100 : 0);
        $_proVen = (isset($promediosRedCat['OBJETIVOS DE VENTA']) ? $promediosRedCat['OBJETIVOS DE VENTA']*30/100 : 0);
		
        $_proCal = (isset($promediosRedCat['CALIDAD, IMAGEN E INFRAESTRUCTURA']) ? $promediosRedCat['CALIDAD, IMAGEN E INFRAESTRUCTURA']*20/100 : 0);
        $_proCap = (isset($promediosRedCat['CAPACITACIÓN']) ? $promediosRedCat['CAPACITACIÓN']*10/100 : 0);
        $promedioGlobal = round($_proMys + $_proVen + $_proCal + $_proCap);

        $_proMys = (isset($promediosEstacionesCat['MYSTERY SHOPPER']) ? $promediosEstacionesCat['MYSTERY SHOPPER']*40/100 : 0);
        $_proVen = (isset($promediosEstacionesCat['OBJETIVOS DE VENTA']) ? $promediosEstacionesCat['OBJETIVOS DE VENTA']*30/100 : 0);
		//var_dump($_proVen);exit;
        $_proCal = (isset($promediosEstacionesCat['CALIDAD, IMAGEN E INFRAESTRUCTURA']) ? $promediosEstacionesCat['CALIDAD, IMAGEN E INFRAESTRUCTURA']*20/100 : 0);
        $_proCap = (isset($promediosEstacionesCat['CAPACITACIÓN']) ? $promediosEstacionesCat['CAPACITACIÓN']*10/100 : 0);
        $promedioGlobalEstaciones = round($_proMys + $_proVen + $_proCal + $_proCap);

        if (isset($_GET['rolfi'])) {
        
		

        }

        return $this->render('@Incentive/Frontend/Incentive/ajax/ajax_performance.html.twig',
            compact(
                'storesComplete'. 'store', 'storeRankings', 
                'itemsList', 'selectedYear','lastUploadDate', 'categories', 'zonas', 
                'promediosRed', 'promediosRedCat', 'promedioGlobal', 'promediosRedCategory', 
                'promediosEstaciones', 'promediosEstacionesCat', 'promedioGlobalEstaciones', 'promediosEstacionesCategory', 
                'year', 'month', 'selectorChoices'
            )
        );
    }

    /**
     * @param \DateTime $now
     * @return \DateTime[]
     */
    private function getSelectorChoices(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadWhitResultsByMonthAfterYear(date("Y",time()));
        if($lastUpload){
            $year = $lastUpload[0]["y"];
            $month = $lastUpload[0]["m"];
        }  else {
            $year = date("Y",time());
            $month = date("m",time());
        }


        $now = new \DateTime("$year-$month-01");

        $choices = [];
        for ($i = 0; $i < 12; $i++) {
            $var = clone $now;
            $choices[] = $var->modify("-{$i} month");
        }

        foreach ($choices as $i => $choice){
            $month2 = $choice->format('n');
            $year2  = $choice->format('Y');
            $haveMonth = $goalRepository->findGoalsByMonthWithResults($month2,$year2);

            if ($haveMonth == null){
                unset($choices[$i]);
            }
            //EXCLUYE MESES QUE NO TENGAN RESULTADOS
            // else {
            //     $have = $goalRepository->find1GoalByMonth($month2,$year2);
            //     if ($have->getResult()==0){
            //         unset($choices[$i]);
            //     }
            // }
        }

        reset($choices);

        return $choices;
    }

    public function getSelectorChoices2(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadWhitResultsByMonthAfterYear(date("Y",time()));

        if (isset($lastUpload[0])){
            $year = $lastUpload[0]["y"];
            $month = $lastUpload[0]["m"];
        }
        else {
            $year = date("Y",time());
            $month = date("m",time());
        }

        $now = new \DateTime("$year-$month-01");

        $choices = [];
        for ($i = 0; $i < 12; $i++) {
            $var = clone $now;
            $choices[] = $var->modify("-{$i} month");
        }

        return $choices;

    }

}

