<?php

namespace Aper\IncentiveBundle\Controller;


use Aper\StoreBundle\Entity\Store;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RankingMensualController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request) {
        ini_set("memory_limit",-1);
        $selectorChoices = $this->getSelectorChoices2(new \DateTime()); //Lista de meses con resultados!
        $defaultMonth = current($selectorChoices);
        $year = $request->query->get('year', $defaultMonth->format('Y'));
        $month = $request->query->get('month', $defaultMonth->format('n'));
        $selectedMonth = new \DateTime("{$year}-{$month}-01");

        $SpecialMessageRepository = $this->getDoctrine()->getRepository('IncentiveBundle:SpecialGoalMessage');
        $storeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');

        //BUSCAR ULTIMOS OBJETIVOS NUEVOS - NO CORRESPONDIENTES AL HISTORICO
        $lastUpload = $storeRepository->findLastUploadWithoutResultsByMonth2AfterYear(date("Y",time()));

        //ES POSIBLE QUE NO EXISTAN DOS MESES DE DATOS NUEVOS
        if(isset($lastUpload[0])){
            $lastUploadDate = new \DateTime("{$lastUpload[0]['y']}-{$lastUpload[0]['m']}-01");
            $specialGoalMessage = $SpecialMessageRepository->findOneByMonthYear($lastUpload[0]['m'], $lastUpload[0]['y']);
            $textSpecialGoalMessage = (!$specialGoalMessage) ? "-" : $specialGoalMessage->getMessage();
        }
        else{
            $lastUploadDate = false;
            $textSpecialGoalMessage = null;
        }

        if(isset($lastUpload[1])) {
            $lastUploadDate2 = new \DateTime("{$lastUpload[1]['y']}-{$lastUpload[1]['m']}-01");
            $specialGoalMessage2 = $SpecialMessageRepository->findOneByMonthYear($lastUpload[1]['m'], $lastUpload[1]['y']);
        }
        else{
            $lastUploadDate2 = false;
            $specialGoalMessage2 = null;
        }

        $textSpecialGoalMessage2 = (!$specialGoalMessage2) ? "-" : $specialGoalMessage2->getMessage();

        /* @var Store $store */
        $store = $this->getUser()->getEmployee()->getStore();
        $userStore = $store;

        $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
        $storeRankings = new ArrayCollection($storeRepository->findRankingByMonth($month, $year, 1000, (isset($_GET['zone']) ? $store->getZona() : null)));
        $goalTypeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');
        $categories = $goalTypeRepository->getUniqueCategories();

        if(empty($year))
            $year = date("Y",time());

        $inTop = false;

        foreach ($storeRankings as $rank) {
            if ($rank == $store) {
                $inTop = true;
            }
        }

        $selectorChoices = $this->getSelectorChoices(new \DateTime());

        $user = $this->get('security.token_storage')->getToken()->getUser();

        //Buscador de podio
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:Podium');
        $defaultMonth = current($selectorChoices);

        $year2 = $request->query->get('year', $defaultMonth->format('Y'));
        $month2 = $request->query->get('month', $defaultMonth->format('n'));
        $podio = $repository->findOneByMY($month2, $year2);

        if ($podio)
            $podium = $podio->getPosition();
        else
            $podium = null;
        
        //Buscador de mensaje objetivo mensual sorpresa
        $specialGoalMessageSelector = $SpecialMessageRepository->findOneByMonthYear($month, $year);
        $textSpecialGoalMessageSelector = (!$specialGoalMessageSelector) ? "-" : $specialGoalMessageSelector->getMessage();

        $store = $userStore;
        return $this->render('@Incentive/Frontend/Incentive/ajax/ajax_months_statistics.html.twig',
            compact('store', 'storeRankings', 'selectedMonth', 'selectorChoices', 'categories', 'lastUploadDate','lastUploadDate2','inTop', 'user', 'podium','textSpecialGoalMessage','textSpecialGoalMessage2','textSpecialGoalMessageSelector')
        );
    }

    /**
     * @param \DateTime $now
     * @return \DateTime[]
     */
    public function getSelectorChoices(\DateTime $now) //SIN MESES SIN RESULTADOS
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadWhitResultsByMonthAfterYear(date("Y",time()));

        if (isset($lastUpload[0])){
            $year = $lastUpload[0]["y"];
            $month = $lastUpload[0]["m"];
        }
        else {
            $year = date("Y",time());
            $month = date("m",time());
        }

        $now = new \DateTime("$year-$month-01");

        $choices = [];
        for ($i = 0; $i < 12; $i++) {
            $var = clone $now;
            $aux = $var->modify("-{$i} month");
    
            if($aux->format('Y') == $year)
                $choices[] = $aux;
        }

        foreach ($choices as $i => $choice){
            $month2 = $choice->format('n');
            $year2  = $choice->format('Y');
            $haveMonth = $goalRepository->findGoalsByMonthWithResults($month2,$year2);

            if ($haveMonth == null){
                unset($choices[$i]);
            }
            //EXCLUYE MESES QUE NO TENGAN RESULTADOS
            // else {
            //     $have = $goalRepository->find1GoalByMonth($month2,$year2);
            //     if ($have->getResult()==0){
            //         unset($choices[$i]);
            //     }
            // }
        }
        reset($choices);

        return $choices;
    }
    
    public function getSelectorChoices2(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        // LLAMAMOS AL MISMO METODO QUE EL SELECTOR ANTERIOR PERO NOS QUEDAMOS CON SOLO UN MES Y AÑO
        $lastUpload = $goalRepository->findLastUploadWhitResultsByMonthAfterYear(date("Y",time()));

        if (isset($lastUpload[0])){
            $year = $lastUpload[0]["y"];
            $month = $lastUpload[0]["m"];
        }
        else {
            $year = date("Y",time());
            $month = date("m",time());
        }

        $now = new \DateTime("$year-$month-01");
        // $choices = [];
        // for ($i = 0; $i < 12; $i++) {
        //     $var = clone $now;
        //     $choices[] = $var->modify("-{$i} month");
        // }
        $choices[] = $now;

        return $choices;
    }
}
