<?php

namespace Aper\IncentiveBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use WebFactory\Bundle\UserBundle\Controller\ContextController;

/**
 * Class IncentiveController
 *
 * @Route("incentive")
 */
class FrontendIncentiveController extends ContextController
{
    /**
     * @Route("/")
     *
     * @param
     * @return Response
     */
    public function indexAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $role = $user->getMaxRoleType();
        switch ($role) {
            case 'ROLE_HEAD_OFFICE':
                return $this->redirect('ranking/performance');
                break;
            case 'ROLE_EMPLOYEE':
                return $this->redirect('ranking/month-employee');
                break;
            
            default:
                return $this->redirect('ranking/month');
                break;
        }
        // return $this->redirect('ranking/month');
    }

    /**
     * @Route("/terms-and-conditions")
     *
     * @param $request
     * @return Response
     */
    public function termsShowAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT t FROM IncentiveBundle:Term t ORDER BY t.id ASC";
        $terms = $em->createQuery($dql);


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $terms, $request->query->getInt('page', 1),
            1000
        );

        return $this->render('IncentiveBundle:Frontend/Incentive/ajax:ajax_bases.html.twig',
            array('pagination' => $pagination, 'terms' => $terms));

    }
}