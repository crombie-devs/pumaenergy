<?php

namespace Aper\IncentiveBundle\Controller;


use Aper\StoreBundle\Entity\Store;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Aper\IncentiveBundle\Entity\ExtraPointsStore;

class RankingAnualController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        ini_set("memory_limit",-1);

        $selectorChoices = $this->getSelectorChoices(new \DateTime());

        $defaultYear = current($selectorChoices);
        $year = $request->query->get('year', $defaultYear->format('Y'));
        $selectedYear = new \DateTime("{$year}-01-01");

        $zone = (isset($_GET['zone']) ? $_GET['zone'] : '');
        $showZone = 1;


        if ($zone == 'general') {
            unset($_GET['zone']);
        }

        if ($this->getUser()->getMaxRoleType() != 'ROLE_HEAD_OFFICE' && $this->getUser()->getMaxRoleType() != 'ROLE_MANAGER_GLOBAL') {
            $_GET['zone'] = 'zone';
            $showZone = 0;
        }

        /* @var Store $store */
        $store = $this->getUser()->getEmployee()->getStore();
        $userStore = $store;

        $searchZone = null;
        if (isset($_GET['zone'])) {
            if ($this->getUser()->getMaxRoleType() != 'ROLE_MANAGER_GLOBAL') {
                $searchZone = $store->getZona();
            } else {
                $searchZone = $zone;
            }
        }

        $idStores = null;
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->getMaxRoleType() == 'ROLE_MANAGER') {
            $idStores = $this->getUser()->getEmployee()->getStores();
            if (!is_null($idStores)) {
                $idStores = explode(',', $idStores);
                $idStores[] = $store->getCode();
            }
        }
        $storesComplete = [];
        if ($idStores) {
            $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
            $storesComplete = new ArrayCollection($storeRepository->findStoresByCodes(NULL, $year, $idStores));
        }

        $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
        $storeRankings=new ArrayCollection($storeRepository->findRankingByYear($year, $searchZone));


        $_storeRankings = [];
        $_storeRankingsPos = [];

        $_tempStores = [];
        foreach ($storeRankings as $key => $store) {
            $_totalPoints = 0;
            $_completedPoints = 0;

            // print_r($store->getId());exit;


            if (!isset($_GET['lucas'])) {
                // $_totalPoints += $store->getGoalsByYearAndName($selectedYear, 'PUNTAJES OBJETIVO', 'MYSTERY SHOPPER');
                // $_totalPoints += $store->getGoalsByYearAndName($selectedYear, 'PUNTAJES OBJETIVO', 'OBJETIVOS DE VENTA');
                // $_totalPoints += $store->getGoalsByYearAndName($selectedYear, 'PUNTAJES OBJETIVO', 'CALIDAD, IMAGEN E INFRAESTRUCTURA');
                // $_totalPoints += $store->getGoalsByYearAndName($selectedYear, 'PUNTAJES OBJETIVO', 'CAPACITACIÓN');

                $_completedPoints += $store->getGoalsByYearAndName($selectedYear, 'PUNTAJES RESULTADO', 'MYSTERY SHOPPER');
                $_completedPoints += $store->getGoalsByYearAndName($selectedYear, 'PUNTAJES RESULTADO', 'OBJETIVOS DE VENTA');
                $_completedPoints += $store->getGoalsByYearAndName($selectedYear, 'PUNTAJES RESULTADO', 'CALIDAD, IMAGEN E INFRAESTRUCTURA');
                $_completedPoints += $store->getGoalsByYearAndName($selectedYear, 'PUNTAJES RESULTADO', 'CAPACITACIÓN');
                $_totalPoints = 100;
            } else {
                if ($_totalPoints = $store->getGoalsByYearAndName($selectedYear, 'TOTAL_STORE_OBJETIVO')){

                    // print_r($goal);exit;

                    // $goal = array_shift(current($goal));
                    // $_totalPoints = $goal->getObjetive();
                }

                if ($_completedPoints = $store->getGoalsByYearAndName($selectedYear, 'TOTAL_STORE_RESULTADO')){
                    // $goal = array_shift(current($goal));
                    // $_completedPoints = $goal->getResult();
                }

            }

            



            $_stoRank = 0;
            if ($ss = $store->getRankingByYear($selectedYear)){
                $ss = array_shift(current($ss));
                $_stoRank = $ss->getPosition();

                // $_stoRank = $store->getRankingByMonthAndYear($selectedYear, $store)[0]->getPosition();
            }

            $_completedPointsReal = $_completedPoints;
            if ($_completedPoints < 0) {
                $_completedPoints = 0;
            }

            $n = 0;
            if ($_totalPoints != 0) {
                if ($_completedPoints == $_totalPoints) {
                    // if (isset($_GET['t'])) {
                    //     print_r($_totalPoints);exit;
                    //   // print_r(' ee:');  print_r($n);  
                    // }
                    //$n = number_format(($_completedPoints-0.1999) / $_totalPoints, 4);
                    $n = 0.999999999999999999999;
                    // if (isset($_GET['lucas'])) {
                        // print_r($_completedPoints);
                      // print_r(' ee:');  print_r($n);  
                    // }
                } else {
                    $n = number_format($_completedPoints / $_totalPoints, 12);
                      // print_r(' aa:');  print_r($n);  
                }
            }
            $num = explode('.', $n);

            $_position = $num[0];
            if (isset($num[1])) $_position = $num[1];

            $_position = substr($_position, 0, 4);
            if ($_position == 1) $_position = 100;

            $_storeRankingsPos[] = $_position;

            $aa =[];
            $aa['store'] = $store;
            $aa['totalPoints'] = $_totalPoints;
            $aa['completedPoints'] = $_completedPoints;
            $aa['completedPointsReal'] = $_completedPointsReal;
            $aa['stoRank'] = $_stoRank;

            $_storeRankings[] = $aa;

            $_tempStores[$store->getId()] = $store->getId();
        }

        asort($_storeRankingsPos, SORT_NUMERIC);

        $finalPos = array_reverse($_storeRankingsPos, true);
        
        $_tempStoreRankings = [];
        foreach ($finalPos as $key => $value) {
            $_tempStoreRankings[] = $_storeRankings[$key];
        }

        foreach ($storesComplete as $key => $store) {
            if (!isset($_tempStores[$store->getId()])) {
                $aa =[];
                $aa['store'] = $store;
                $aa['totalPoints'] = 0;
                $aa['completedPoints'] = 0;
                $aa['completedPointsReal'] = 0;
                $aa['stoRank'] = 0;
                $_tempStoreRankings[] = $aa;
                // $_storeRankingsPos[] = 0;
            }
        }

        $storeRankings = $_tempStoreRankings;

        $myRankStores = [];
        $count = 0;
        foreach($storeRankings as $keyR => $stoR) {
            foreach($storesComplete as $key => $sto) {
                if ($stoR['store']->getId() == $sto->getId()) {
                    $myRankStores[$count] = $stoR;
                }
            }
            $count++;
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $podium = 1; //solo se muestra una copa en el ranking anual
        $store = $userStore;
        return $this->render('@Incentive/Frontend/Incentive/ajax/ajax_years_statistics.html.twig',
            compact('store', 'storeRankings', 'selectedYear', 'selectorChoices', 'user', 'podium', 'zone', 'showZone', 'idStores', 'storesComplete', 'myRankStores')
        );
    }

    /**git push
     * @param \DateTime $now
     * @return \DateTime[]
     */
    private function getSelectorChoices(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadByMonth();

        if (isset($lastUpload[0])){
            $year=$lastUpload[0]["y"];
            $month=$lastUpload[0]["m"];
        }
        else {
            $year = date("Y",time());
            $month = date("m",time());
        }

        
        $now= new \DateTime("$year-$month-01");
        $choices = [];
        for ($i = 0 ; $i < 1 ; $i++) {
            $var = clone $now;
            $choices[] = $var->modify("-{$i} year");
        }

//         print_r($choices);exit;

//         foreach ($choices as $i => $choice){
//             // if($choice->format('Y') == 2018){
//             //     unset($choices[$i]);
//             //     continue;
//             // }

//             $year2  = $choice->format('Y');
//             $haveMonth = $goalRepository->findGoalsByYear($year2);
// echo 'adasd';exit;

//             if ($haveMonth == null){
//                 unset($choices[$i]);
//             }
//         }
        reset($choices);


        return $choices;
    }
}

