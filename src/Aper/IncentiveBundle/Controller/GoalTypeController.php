<?php

namespace Aper\IncentiveBundle\Controller;

use Aper\IncentiveBundle\Entity\GoalType;
use Aper\IncentiveBundle\Form\DTO\ImportDTO;
use Aper\IncentiveBundle\Form\GoalTypeType;
use Aper\IncentiveBundle\Form\ImportGoalTypType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Goaltype controller.
 *
 * @Route("goal-type")
 */
class GoalTypeController extends Controller
{
    /**
     * Lists all goalType entities.
     *
     * @Route("/", name="goal_type_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $goalTypes = $em->getRepository('IncentiveBundle:GoalType')->findBy([], [
            'year' => 'DESC',
            'month' => 'DESC',
            'cell' => 'ASC',

        ]);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $goalTypes,
            $request->query->getInt('page' , 1),
            10
        );

        return $this->render('@Incentive/GoalType/index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * Creates a new goalType entity.
     *
     * @Route("/new", name="goal_type_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $goalType = new Goaltype();
        $form = $this->createForm(GoalTypeType::class, $goalType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

           // $goalType->setParent(
           //     $this->findParent($form->get('parentType')->getData(), $em, $goalType)
           // );

            $em->persist($goalType);
            $em->flush();

            $this->addFlash('success', sprintf(
                'Se ha creado una nueva configuración para el tipo de objetivo "%s"',
                $goalType->getName()
            ));

            return $this->redirectToRoute('goal_type_index');
        }

        return $this->render('@Incentive/GoalType/new.html.twig', array(
            'goalType' => $goalType,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a goalType entity.
     *
     * @Route("/{id}", name="goal_type_show")
     * @Method("GET")
     */
    public function showAction(GoalType $goalType)
    {
        $deleteForm = $this->createDeleteForm($goalType);

        return $this->render('@Incentive/GoalType/show.html.twig', array(
            'goalType' => $goalType,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing goalType entity.
     *
     * @Route("/{id}/edit", name="goal_type_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, GoalType $goalType)
    {
        $deleteForm = $this->createDeleteForm($goalType);
        $editForm = $this->createForm(GoalTypeType::class, $goalType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

         //   $goalType->setParent(
         //       $this->findParent($editForm->get('parentType')->getData(), $em, $goalType)
         //   );

            $em->flush();

            $this->addFlash('success', sprintf(
                'Se ha editado una configuración para el tipo de objetivo "%s"',
                $goalType->getName()
            ));

            return $this->redirectToRoute('goal_type_index');
        }

        return $this->render('@Incentive/GoalType/edit.html.twig', array(
            'goalType' => $goalType,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a goalType entity.
     *
     * @Route("/{id}/delete", name="goal_type_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, GoalType $goalType)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($goalType);
        $em->flush();

        return $this->redirectToRoute('goal_type_index');
    }

    /**
     * Creates a form to delete a goalType entity.
     *
     * @param GoalType $goalType The goalType entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(GoalType $goalType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('goal_type_delete', array('id' => $goalType->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Importer of Goal Types
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/import/", name="goal_type_importer")
     * @Method({"GET", "POST"})
     */
    public function import(Request $request)
    {
        $import = new ImportDTO();
        $form = $this->createForm(ImportGoalTypType::class, $import);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $errors = $this->get('importer_goal_types_xls')->import($import);

            if (empty($errors)) {
                $this->addFlash('success', 'La importación fue exitosa');
            } else {
                $this->addFlash('error', 'El archivo no se ha importado. Estos fueron los errores:');
                foreach ($errors as $error) {
                    $this->addFlash('error', $error);
                }
            }
        }


        return $this->render('@Incentive/GoalType/import.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * @param string $parentType
     * @param ObjectManager $em
     * @param GoalType $goalType
     * @return GoalType|object|null
     */
    private function findParent($parentType, ObjectManager $em, GoalType $goalType)
    {
        if (in_array($parentType, [GoalType::EXERT, GoalType::MYSTERY_SHOPPER])) {
            $parent = $em->getRepository('IncentiveBundle:GoalType')->findOneBy([
                'name' => (string) $parentType === GoalType::EXERT ? 'EXERT' : 'MISTERY',
                'year' => $goalType->getYear(),
                'month' => $goalType->getMonth(),
            ]);

            if (null === $parent) {
                $parent = (string) $parentType === GoalType::EXERT
                    ? GoalType::createExert($goalType->getYear(), $goalType->getMonth())
                    : GoalType::createMisteryShooper($goalType->getYear(), $goalType->getMonth());

                $em->persist($parent);
            }
        } else {
            $parent = null;
        }

        return $parent;
    }
}
