<?php

namespace Aper\IncentiveBundle\Controller;


use Aper\StoreBundle\Entity\Store;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Aper\IncentiveBundle\Entity\ImportLog;


class RankingMensualAnualController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        ini_set("memory_limit",-1);
        // exit;
        $selectorChoices = $this->getSelectorChoices2(new \DateTime());
        $defaultMonth = current($selectorChoices);
        $year = $request->query->get('year', $defaultMonth->format('Y'));
        $month = $request->query->get('month', $defaultMonth->format('n'));
        $selectedYear = new \DateTime("{$year}-{$month}-01");

        $storeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $storeRepository->findLastUploadByMonth2();
        if($lastUpload){
            $lastUploadDate = new \DateTime("{$lastUpload[0]['y']}-{$lastUpload[0]['m']}-01");
        }


        $zone = (isset($_GET['zone']) ? $_GET['zone'] : '');
        $showZone = 1;


        if ($zone == 'general') {
            unset($_GET['zone']);
        }

        if ($this->getUser()->getMaxRoleType() != 'ROLE_HEAD_OFFICE' && $this->getUser()->getMaxRoleType() != 'ROLE_MANAGER_GLOBAL') {
            $_GET['zone'] = 'zone';
            $showZone = 0;
        }

        /* @var Store $store */
        $store = $this->getUser()->getEmployee()->getStore();
		//var_dump($store); exit;
        $userStore = $store;

        $theZones = array(
            'Centro' => 'Centro',
            'Norte' => 'Norte',
            'Sur' => 'Sur',
            'Red Propia' => 'Red Propia'
        );


		
		
		
        $searchZone = null;
        if (isset($_GET['zone'])) {
            if ($this->getUser()->getMaxRoleType() != 'ROLE_MANAGER_GLOBAL') {
                if ($this->getUser()->getMaxRoleType() == 'ROLE_HEAD_OFFICE') {
                    $searchZone = $this->getUser()->getEmployee()->getArea();
												
						
					
                    if (!isset($theZones[$searchZone])) {
                        $searchZone = $store->getZona();
						
						
                    }
                } else {
                    $searchZone = $store->getZona();
                }
            } else {
                $searchZone = $zone;
            }
        }


        $idStores = null;

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->getMaxRoleType() == 'ROLE_MANAGER') {
            $idStores = $this->getUser()->getEmployee()->getStores();
            if (!is_null($idStores)) {
                $idStores = explode(',', $idStores);
                $idStores[] = $store->getCode();
            }
        }

        $storesComplete = [];
        if ($idStores) {
            $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
            $storesComplete = new ArrayCollection($storeRepository->findStoresByCodes($month, $year, $idStores));
        }

		if ($this->getUser()->getMaxRoleType() == 'ROLE_HEAD_OFFICE' && $zone == 'zone' ) {
			$responsable2 = $this->getUser()->getId();
		//var_dump($responsable2);exit; 
		}
		else {$responsable2 = null;}

        $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
        //$storeRankings = new ArrayCollection($storeRepository->findRankingByMonth($month, $year, 1000, (isset($_GET['zone']) ? $store->getZona() : null)));
        $storeRankings = new ArrayCollection($storeRepository->findRankingByMonth($month, $year, 500, $searchZone, $responsable2 ));
        $goalTypeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');


        $_storeRankings = [];
        $_storeRankingsPos = [];

        $_tempStores = [];
        foreach ($storeRankings as $key => $store) {
            $_totalPoints = 0;
            $_completedPoints = 0;
			
		

            if (!isset($_GET['lucas'])) {
                // if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'PUNTAJES OBJETIVO', 'MYSTERY SHOPPER')){
                //     $currentGoal = current($goal);
                //     if ($currentGoal) {
                //         $goal = array_shift($currentGoal);
                //         $_totalPoints += $goal->getObjetive();
                //     }
                // }
                // if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'PUNTAJES OBJETIVO', 'OBJETIVOS DE VENTA')){
                //     $currentGoal = current($goal);
                //     if ($currentGoal) {
                //         $goal = array_shift($currentGoal);
                //         $_totalPoints += $goal->getObjetive();
                //     }
                // }
                // if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'PUNTAJES OBJETIVO', 'CALIDAD, IMAGEN E INFRAESTRUCTURA')){
                //     $currentGoal = current($goal);
                //     if ($currentGoal) {
                //         $goal = array_shift($currentGoal);
                //         $_totalPoints += $goal->getObjetive();
                //     }
                // }
                // if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'PUNTAJES OBJETIVO', 'CAPACITACIÓN')){
                //     $currentGoal = current($goal);
                //     if ($currentGoal) {
                //         $goal = array_shift($currentGoal);
                //         $_totalPoints += $goal->getObjetive();
                //     }
                // }


                if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'PUNTAJES RESULTADO', 'MYSTERY SHOPPER')){
                    $currentGoal = current($goal);
                    if ($currentGoal) {
                        $goal = array_shift($currentGoal);
                        $_completedPoints += $goal->getResultMet();
                    }
                }
                if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'PUNTAJES RESULTADO', 'OBJETIVOS DE VENTA')){
                    $currentGoal = current($goal);
                    if ($currentGoal) {
                        $goal = array_shift($currentGoal);
                        $_completedPoints += $goal->getResult();
                    }
                }
                if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'PUNTAJES RESULTADO', 'CALIDAD, IMAGEN E INFRAESTRUCTURA')){
                    $currentGoal = current($goal);
                    if ($currentGoal) {
                        $goal = array_shift($currentGoal);
                        $_completedPoints += $goal->getResult();
                    }
                }
                if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'PUNTAJES RESULTADO', 'CAPACITACIÓN')){
                    $currentGoal = current($goal);
                    if ($currentGoal) {
                        $goal = array_shift($currentGoal);
                        $_completedPoints += $goal->getResult();
                    }
                }
                $_totalPoints = 100;
            } else {

                if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'TOTAL_STORE_OBJETIVO')){
                    $currentGoal = current($goal);
                    $goal = array_shift($currentGoal);
                    $_totalPoints = $goal->getObjetive();
                }

                if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'TOTAL_STORE_RESULTADO')){
                    $currentGoal = current($goal);
                    $goal = array_shift($currentGoal);
                    $_completedPoints = $goal->getResult();
                }
            }


            $_stoRank = 0;
            if ($ss = $store->getRankingByMonthAndYear($selectedYear, $store)){
                $currentSs = current($ss);
                $ss = array_shift($currentSs);
                $_stoRank = $ss->getPosition();

                // $_stoRank = $store->getRankingByMonthAndYear($selectedYear, $store)[0]->getPosition();
            }

            $_completedPointsReal = $_completedPoints;
            if ($_completedPoints < 0) {
                $_completedPoints = 0;
            }

            if ($_completedPoints == $_totalPoints) {
                //$n = number_format(($_completedPoints-0.19999999) / $_totalPoints, 8);
                $n = 0.999999999999999999999;
                if (isset($_GET['lucas'])) {
                    print_r($_completedPoints);
                  // print_r(' ee:');  print_r($n);  
                }
            } else {
                $n = number_format($_completedPoints / $_totalPoints, 12);
                  // print_r(' aa:');  print_r($n);  
            }
            $num = explode('.', $n);

            $_position = $num[0];
            if (isset($num[1])) $_position = $num[1];

            $_position = substr($_position, 0, 4);
            if ($_position == 1) $_position = 100;

            $_storeRankingsPos[] = $_position;

            $aa =[];
            $aa['store'] = $store;
            $aa['totalPoints'] = $_totalPoints;
            $aa['completedPoints'] = $_completedPoints;
            $aa['completedPointsReal'] = $_completedPointsReal;
            $aa['stoRank'] = $_stoRank;
			
		
            $_storeRankings[] = $aa;

            $_tempStores[$store->getId()] = $store->getId();
        }
		
//var_dump($_storeRankings);exit;

        asort($_storeRankingsPos, SORT_NUMERIC);

        $finalPos = array_reverse($_storeRankingsPos, true);
        
        $_tempStoreRankings = [];
        foreach ($finalPos as $key => $value) {
            $_tempStoreRankings[] = $_storeRankings[$key];
        }

        foreach ($storesComplete as $key => $store) {
            if (!isset($_tempStores[$store->getId()])) {
                $aa =[];
                $aa['store'] = $store;
                $aa['totalPoints'] = 0;
                $aa['completedPoints'] = 0;
                $aa['completedPointsReal'] = 0;
                $aa['stoRank'] = 0;
                $_tempStoreRankings[] = $aa;
                // $_storeRankingsPos[] = 0;
                // $myRankStores[$store->getId()]['stoRank'] = 0;
            // } else {
            //     $myRankStores[$store->getId()]['stoRank'] = $_tempStores[$store->getId()];
            }
        }


        $storeRankings = $_tempStoreRankings;

        $myRankStores = [];
        $count = 0;
        foreach($storeRankings as $keyR => $stoR) {
            foreach($storesComplete as $key => $sto) {
                if ($stoR['store']->getId() == $sto->getId()) {
                    $myRankStores[$count] = $stoR;
                }
            }
            $count++;
        }

        $categories = $goalTypeRepository->getUniqueCategories();

        $inTop = false;
        foreach ($storeRankings as $rank) {

            if ($rank == $store) {
                $inTop = true;
            }
        }
        $selectorChoices = $this->getSelectorChoices(new \DateTime());

        //Buscador de podio
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:Podium');
        $podio = $repository->findOneByMY($month, $year);

        if (!$podio){
            $podium = 3;
        }else{
            $podium = $podio->getPosition();
        }        

        // print_r($month);exit;
        // echo 'asdasd';exit;
        // print_r(count($showZone));
        // exit;

        $store = $userStore;
        return $this->render('@Incentive/Frontend/Incentive/ajax/ajax_monthsyears_statistics.html.twig',
            compact('store', 'storeRankings', 'selectedYear', 'selectorChoices','lastUploadDate','categories', 'inTop', 'podium', 'zone', 'showZone', 'idStores', 'storesComplete', 'myRankStores')
        );
    }

    /**
     * @param \DateTime $now
     * @return \DateTime[]
     */
    private function getSelectorChoices(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadWhitResultsByMonthAfterYear(date("Y",time()));
        if($lastUpload){
            $year = $lastUpload[0]["y"];
            $month = $lastUpload[0]["m"];
        }  else {
            $year = date("Y",time());
            $month = date("m",time());
        }


        $now = new \DateTime("$year-$month-01");

        $choices = [];
        for ($i = 0; $i < 12; $i++) {
            $var = clone $now;
            $choices[] = $var->modify("-{$i} month");
        }

        foreach ($choices as $i => $choice){
            $month2 = $choice->format('n');
            $year2  = $choice->format('Y');
            $haveMonth = $goalRepository->findGoalsByMonthWithResults($month2,$year2);

            if ($haveMonth == null){
                unset($choices[$i]);
            }
            //EXCLUYE MESES QUE NO TENGAN RESULTADOS
            // else {
            //     $have = $goalRepository->find1GoalByMonth($month2,$year2);
            //     if ($have->getResult()==0){
            //         unset($choices[$i]);
            //     }
            // }
        }

        reset($choices);

        return $choices;
    }

    public function getSelectorChoices2(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadWhitResultsByMonthAfterYear(date("Y",time()));

        if (isset($lastUpload[0])){
            $year = $lastUpload[0]["y"];
            $month = $lastUpload[0]["m"];
        }
        else {
            $year = date("Y",time());
            $month = date("m",time());
        }

        $now = new \DateTime("$year-$month-01");

        $choices = [];
        for ($i = 0; $i < 12; $i++) {
            $var = clone $now;
            $choices[] = $var->modify("-{$i} month");
        }

        return $choices;

    }

}

