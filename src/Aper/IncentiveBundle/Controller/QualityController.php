<?php

namespace Aper\IncentiveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Goaltype controller.
 *
 * @Route("/quality", name="quality_download")
 */
class QualityController extends Controller
{
    /**
     * Add a new image file
     *
     * @Route()("/{code}", name="download_file_index")
     * @Method()({"GET","POST"})
     */
    public function indexAction($code)
    {
        $directory = null;
        $existDirectory = false;
        $path = $this->get('kernel')->getRootDir(). "/../calidad/";
        $scanned_directory = array_diff(scandir($path), array('..', '.'));

        foreach ($scanned_directory as $key => $value) {
            $directoryCode = explode("_", $value);
            if ($directoryCode[0] == $code) {
                $path = $path.$value."/";
                $directory = $value;
                $existDirectory = true;
                break;
            }
        }

        
		//$filesName = array_diff(scandir($path), array('..', '.'));
		$filesName = array_diff(preg_grep('~\.(pdf|PDF)$~',scandir($path)), array('..', '.'));

        return $this->render('@Incentive/Frontend/Incentive/ajax/quality.html.twig',
            compact('filesName', 'directory', 'existDirectory')
        );
    }

    /**
     * Add a new image file
     *
     * @Route()("/download/{directory}/{filename}", name="download_file")
     * @Method()({"GET","POST"})
     */
    public function downloadFileAction($directory, $filename)
    {
        $path = $this->get('kernel')->getRootDir(). "/../calidad/";
        $path = $path.$directory."/".$filename;

        $response = new BinaryFileResponse($path);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);


        return $response;
    }
}
