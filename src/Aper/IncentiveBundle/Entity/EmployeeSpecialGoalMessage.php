<?php

namespace Aper\IncentiveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmployeeSpecialGoalMessage
 *
 * @ORM\Table(name="employee_special_goal_message")
 * @ORM\Entity(repositoryClass="Aper\IncentiveBundle\Repository\EmployeeSpecialGoalMessageRepository")
 */
class EmployeeSpecialGoalMessage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="month", type="integer")
     */
    private $month;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $message;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set month
     *
     * @param integer $month
     *
     * @return EmployeeSpecialGoalMessage
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return EmployeeSpecialGoalMessage
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return EmployeeSpecialGoalMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function getMessageByMonthYear(\DateTime $month, \DateTime $year)
    {
    }
}

