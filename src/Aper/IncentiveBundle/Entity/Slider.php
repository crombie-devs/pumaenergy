<?php

namespace Aper\IncentiveBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContext;


/**
 * Slider
 *
 * @ORM\Table(name="slider")
 * @ORM\Entity(repositoryClass="Aper\IncentiveBundle\Repository\SliderRepository")
 */
class Slider
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     *
     * @var SliderFile
     * @ORM\OneToOne(targetEntity="SliderFile", cascade={"all"}, mappedBy="slider")
     * @Assert\Valid()
     */

    protected $file;


    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer")
     * @Assert\NotBlank()
     */
    private $sort;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text", nullable=true)
     * @Assert\Url()
     */
    private $url;

    /**
     * @var string[]
     *
     * @ORM\Column(name="categories", type="simple_array", length=100, nullable=true)
     */
    private $categories;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param SliderFile $file
     *
     * @return Slider
     */
    public function setFile($file)
    {
        $this->file = $file;
        $file->setSlider($this);

        return $this;
    }

    /**
     * Get file
     *
     * @return SliderFile
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return Slider
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set categories.
     *
     * @param string $categories
     *
     * @return Slider
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Get categories.
     *
     * @return string
     */
    public function getCategories()
    {
        return $this->categories;
    }


    /**
     * Set url
     *
     * @param string $url
     *
     * @return Slider
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }


}

