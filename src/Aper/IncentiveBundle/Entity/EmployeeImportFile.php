<?php

namespace Aper\IncentiveBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use WebFactory\Bundle\FileBundle\Model\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @package WebFactory\Bundle\FileBundle\Model
 *
 */
class EmployeeImportFile extends File
{
    /**
     * Directorio relativo a guardar los archivos
     */
    const DIRECTORY = 'uploads/import_file/files';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var EmployeeImportLog
     * @ORM\OneToOne(targetEntity="EmployeeImportLog", inversedBy="file")
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"application/vnd.ms-excel"},
     *     mimeTypesMessage = "Ingrese un archivo de excel válido",
     * )
     *
     */
    protected $log;



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return EmployeeImportLog
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param EmployeeImportLog $log
     */
    public function setLog($log)
    {
        $this->log = $log;
    }

}
