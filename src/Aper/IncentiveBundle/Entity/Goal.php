<?php

namespace Aper\IncentiveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Proxies\__CG__\Aper\StoreBundle\Entity\Store;

/**
 * Goal
 *
 * @ORM\Table(name="goal")
 * @ORM\Entity(repositoryClass="Aper\IncentiveBundle\Repository\GoalRepository")
 */
class Goal
{
    /**
     * @var GoalType
     *
     * @ORM\ManyToOne(targetEntity="GoalType", inversedBy="goal", cascade="persist")
     * @ORM\JoinColumn(name="goaltype_id", referencedColumnName="id")
     */
    protected $goalType;


    /**
     * @var store
     *
     * @ORM\ManyToOne(targetEntity="Aper\StoreBundle\Entity\Store", inversedBy="goals")
     * @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     */
    protected $store;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="month", type="smallint")
     */
    private $month;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="result", type="float")
     */
    private $result;

    /**
     * @var float
     *
     * @ORM\Column(name="objetive", type="float")
     */
    private $objetive;

    /**
     * @var string
     *
     * @ORM\Column(name="result_met", type="string", length=255)
     */
    private $resultMet;

    /**
     * @var string
     *
     * @ORM\Column(name="objetive_met", type="string", length=255)
     */
    private $objetiveMet;

    /**
     * @var string
     *
     * @ORM\Column(name="symbolf", type="string", length=20)
     */
    private $simbolf;

    /**
     * @var string
     *
     * @ORM\Column(name="symbolb", type="string", length=20)
     */
    private $simbolb;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set month
     *
     * @param integer $month
     *
     * @return Goal
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return Goal
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Goal
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set result
     *
     * @param float $result
     *
     * @return Goal
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return float
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set objetive
     *
     * @param float $objetive
     *
     * @return Goal
     */
    public function setObjetive($objetive)
    {
        $this->objetive = $objetive;

        return $this;
    }

    /**
     * Get objetive
     *
     * @return float
     */
    public function getObjetive()
    {
        return $this->objetive;
    }


    /**
     * Set resultMet
     *
     * @param string $resultMet
     *
     * @return Goal
     */
    public function setResultMet($resultMet)
    {
        $this->resultMet = $resultMet;

        return $this;
    }

    /**
     * Get resultMet
     *
     * @return string
     */
    public function getResultMet()
    {
        return $this->resultMet;
    }

    /**
     * Set objetiveMet
     *
     * @param string $objetiveMet
     *
     * @return Goal
     */
    public function setObjetiveMet($objetiveMet)
    {
        $this->objetiveMet = $objetiveMet;

        return $this;
    }

    /**
     * Get objetiveMet
     *
     * @return string
     */
    public function getObjetiveMet()
    {
        return $this->objetiveMet;
    }

    /**
     * @return GoalType
     */
    public function getGoalType()
    {
        return $this->goalType;
    }

    /**
     * @param GoalType $goalType
     */
    public function setGoalType($goalType)
    {
        $this->goalType = $goalType;
    }

    /**
     * @return Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * @param Store $store
     */
    public function setStore($store)
    {
        $this->store = $store;
    }

    /**
     * @return string
     */
    public function getSymbolf()
    {
        return $this->simbolf;
    }

    /**
     * @param string $simbolf
     */
    public function setSymbolf($simbolf)
    {
        $this->simbolf = $simbolf;
    }

    /**
     * @return string
     */
    public function getSymbolb()
    {
        return $this->simbolb;
    }

    /**
     * @param string $simbolb
     */
    public function setSymbolb($simbolb)
    {
        $this->simbolb = $simbolb;
    }




}

