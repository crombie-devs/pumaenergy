<?php

namespace Aper\IncentiveBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContext;


/**
 * HomeDestacado
 *
 * @ORM\Table(name="home_destacado")
 * @ORM\Entity(repositoryClass="Aper\IncentiveBundle\Repository\HomeDestacadoRepository")
 */
class HomeDestacado
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     *
     * @var HomeDestacadoFile
     * @ORM\OneToOne(targetEntity="HomeDestacadoFile", cascade={"all"}, mappedBy="homeDestacado")
     * @Assert\Valid()
     */

    protected $homeDestacadoFile;
//
//    /**
//     *
//     * @var SliderFile
//     * @ORM\OneToOne(targetEntity="SliderFile", cascade={"all"}, mappedBy="slider")
//     * @Assert\Valid()
//     */
//
//    protected $file;


    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer", nullable=true)
     */
    private $sort;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text", nullable=true)
     * @Assert\Url()
     */
    private $url;

    /**
     * @var string[]
     *
     * @ORM\Column(name="categories", type="simple_array", length=100, nullable=true)
     */
    private $categories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean", nullable=true)
     */
    private $activo;

    /**
     * @var string
     *
     * @ORM\Column(name="role_string", type="string", length=100, nullable=true)
     */
    private $roleString;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param HomeDestacadoFile $homeDestacadoFile
     *
     * @return HomeDestacado
     */
    public function setHomeDestacadoFile($homeDestacadoFile)
    {
        $this->homeDestacadoFile = $homeDestacadoFile;
        $homeDestacadoFile->setHomeDestacado($this);

        return $this;
    }

    /**
     * Get homeDestacadoFile
     *
     * @return HomeDestacadoFile
     */
    public function getHomeDestacadoFile()
    {
        return $this->homeDestacadoFile;
    }


    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return HomeDestacado
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set categories.
     *
     * @param string $categories
     *
     * @return Slider
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Get categories.
     *
     * @return string
     */
    public function getCategories()
    {
        return $this->categories;
    }


    /**
     * Set url
     *
     * @param string $url
     *
     * @return HomeDestacado
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return bool
     */
    public function isActivo()
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return string
     */
    public function getRoleString()
    {
        return $this->roleString;
    }

    /**
     * @param string $roleString
     */
    public function setRoleString($roleString)
    {
        $this->roleString = $roleString;
    }

}

