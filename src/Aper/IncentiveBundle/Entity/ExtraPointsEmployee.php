<?php

namespace Aper\IncentiveBundle\Entity;

use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;



/**
 * ExtraPointsEmployee
 *
 * @ORM\Table(name="extra_points_employee")
 * @ORM\Entity(repositoryClass="Aper\IncentiveBundle\Repository\ExtraPointsEmployeeRepository")
 */
class ExtraPointsEmployee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var employee
     *
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\Employee", inversedBy="extraPoints")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     */
    protected $employee;

    /**
     * @var int
     *
     * @ORM\Column(name="points", type="integer")
     */
    private $points;
    
    /**
     * @var int
     *
     * @ORM\Column(name="points_objetive", type="integer")
     */
    private $ptsObjetives;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=255)
     */
    private $reason;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;


    /**
     * @var boolean
     *
     * @ORM\Column(name="disqualified", type="boolean")
     */
    private $disqualified;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set employee
     *
     * @param Aper\UserBundle\Employee; $employee
     *
     * @return ExtraPointsEmployee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Get ptsObjetives
     *
     * @return integer
     */
    public function getPtsObjetives()
    {
        return $this->ptsObjetives;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Get type
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return ExtraPointsEmployee
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Set ptsObjetives
     *
     * @param integer $ptsObjetives
     *
     * @return ptsObjetives
     */
    public function setPtsObjetives($ptsObjetives)
    {
        $this->ptsObjetives = $ptsObjetives;

        return $this;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return ExtraPointsEmployee
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }


    /**
     * Set reason
     *
     * @param string $reason
     *
     * @return ExtraPointsEmployee
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get year
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set type
     *
     * @return ExtraPointsEmployee
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get disqulified
     *
     * @return boolean
     */
    public function getDisqualified()
    {
        return $this->disqualified;
    }

    /**
     * Set type
     *
     * @return ExtraPointsEmployee
     */
    public function setDisqualified($disqualified)
    {
        $this->disqualified = $disqualified;
        return $this;
    }


}

