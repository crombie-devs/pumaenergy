<?php

namespace Aper\IncentiveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Aper\UserBundle\Entity\Employee;

/**
 * Goal
 *
 * @ORM\Table(name="employee_goal")
 * @ORM\Entity(repositoryClass="Aper\IncentiveBundle\Repository\EmployeeGoalRepository")
 */
class EmployeeGoal
{
    /**
     * @var EmployeeGoalType
     *
     * @ORM\ManyToOne(targetEntity="EmployeeGoalType", inversedBy="goal")
     * @ORM\JoinColumn(name="employeeGoaltype_id", referencedColumnName="id")
     */
    protected $employeeGoalType;


    /**
     * @var employee
     *
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\Employee", inversedBy="goals")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     */
    protected $employee;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="month", type="smallint")
     */
    private $month;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="result", type="float")
     */
    private $result;

    /**
     * @var float
     *
     * @ORM\Column(name="objetive", type="float")
     */
    private $objetive;

    /**
     * @var string
     *
     * @ORM\Column(name="result_met", type="string", length=255)
     */
    private $resultMet;

    /**
     * @var string
     *
     * @ORM\Column(name="objetive_met", type="string", length=255)
     */
    private $objetiveMet;

    /**
     * @var string
     *
     * @ORM\Column(name="symbolf", type="string", length=20)
     */
    private $simbolf='';

    /**
     * @var string
     *
     * @ORM\Column(name="symbolb", type="string", length=20)
     */
    private $simbolb='';


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set month
     *
     * @param integer $month
     *
     * @return EmployeeGoal
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return EmployeeGoal
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Goal
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set result
     *
     * @param float $result
     *
     * @return EmployeeGoal
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return float
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set objetive
     *
     * @param float $objetive
     *
     * @return EmployeeGoal
     */
    public function setObjetive($objetive)
    {
        $this->objetive = $objetive;

        return $this;
    }

    /**
     * Get objetive
     *
     * @return float
     */
    public function getObjetive()
    {
        return $this->objetive;
    }


    /**
     * Set resultMet
     *
     * @param string $resultMet
     *
     * @return EmployeeGoal
     */
    public function setResultMet($resultMet)
    {
        $this->resultMet = $resultMet;

        return $this;
    }

    /**
     * Get resultMet
     *
     * @return string
     */
    public function getResultMet()
    {
        return $this->resultMet;
    }

    /**
     * Set objetiveMet
     *
     * @param string $objetiveMet
     *
     * @return EmployeeGoal
     */
    public function setObjetiveMet($objetiveMet)
    {
        $this->objetiveMet = $objetiveMet;

        return $this;
    }

    /**
     * Get objetiveMet
     *
     * @return string
     */
    public function getObjetiveMet()
    {
        return $this->objetiveMet;
    }

    /**
     * @return EmployeeGoalType
     */
    public function getEmployeeGoalType()
    {
        return $this->employeeGoalType;
    }

    /**
     * @param EmployeeGoalType $employeeGoalType
     */
    public function setEmployeeGoalType($employeeGoalType)
    {
        $this->employeeGoalType = $employeeGoalType;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param Employee $employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    /**
     * @return string
     */
    public function getSymbolf()
    {
        return $this->simbolf;
    }

    /**
     * @param string $simbolf
     */
    public function setSymbolf($simbolf)
    {
        $this->simbolf = $simbolf;
    }

    /**
     * @return string
     */
    public function getSymbolb()
    {
        return $this->simbolb;
    }

    /**
     * @param string $simbolb
     */
    public function setSymbolb($simbolb)
    {
        $this->simbolb = $simbolb;
    }




}

