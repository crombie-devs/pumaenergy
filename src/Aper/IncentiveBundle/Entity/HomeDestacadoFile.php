<?php

namespace Aper\IncentiveBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\FileBundle\Model\Image;
use Symfony\Component\Validator\Constraints\Callback;

/**
 * @ORM\Entity
 * @package WebFactory\Bundle\FileBundle\Model
 * @Callback(methods={"validate"})
 *
 */
class HomeDestacadoFile extends Image
{
    /**
     * Directorio relativo a guardar los archivos
     */
    const DIRECTORY = 'uploads/slider/files';
//ver
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var HomeDestacado
     * @ORM\OneToOne(targetEntity="HomeDestacado", inversedBy="homeDestacadoFile")
     */
    protected $homeDestacado;

//    /**
//     * @var Slider
//     * @ORM\OneToOne(targetEntity="Slider", inversedBy="file")
//     */
//    protected $slider;

    /**
     * @var UploadedFile
     * @Assert\Image(
     *     minWidth = 200,
     *     minHeight = 200,
     *     maxWidth = 4000,
     *     maxHeight = 4000
     * )
     */
    protected $file;



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return HomeDestacado
     */
    public function getHomeDestacado()
    {
        return $this->homeDestacado;
    }

    /**
     * @param HomeDestacado $homeDestacado
     */
    public function setHomeDestacado($homeDestacado)
    {
        $this->homeDestacado = $homeDestacado;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        $this->ensureRequiredFile($context);
    }
}
