<?php

namespace Aper\IncentiveBundle\Entity;

use Aper\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;



/**
 * ExtraPointsStore
 *
 * @ORM\Table(name="extra_points_store")
 * @ORM\Entity(repositoryClass="Aper\IncentiveBundle\Repository\ExtraPointsStoreRepository")
 */
class ExtraPointsStore
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var store
     *
     * @ORM\ManyToOne(targetEntity="Aper\StoreBundle\Entity\Store", inversedBy="extraPoints")
     * @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     */
    protected $store;

    /**
     * @var int
     *
     * @ORM\Column(name="points", type="integer")
     */
    private $points;
    
    /**
     * @var int
     *
     * @ORM\Column(name="points_objetive", type="integer")
     */
    private $ptsObjetives;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=255)
     */
    private $reason;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;


    /**
     * @var boolean
     *
     * @ORM\Column(name="disqualified", type="boolean")
     */
    private $disqualified;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set store
     *
     * @param \Aper\StoreBundle\Entity\Store $store
     *
     * @return ExtraPointsStore
     */
    public function setStore($store)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return string
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Get ptsObjetives
     *
     * @return integer
     */
    public function getPtsObjetives()
    {
        return $this->ptsObjetives;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Get type
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return ExtraPointsStore
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Set ptsObjetives
     *
     * @param integer $ptsObjetives
     *
     * @return ptsObjetives
     */
    public function setPtsObjetives($ptsObjetives)
    {
        $this->ptsObjetives = $ptsObjetives;

        return $this;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return ExtraPointsStore
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }


    /**
     * Set reason
     *
     * @param string $reason
     *
     * @return ExtraPointsStore
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get year
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set type
     *
     * @return ExtraPointsStore
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get disqulified
     *
     * @return boolean
     */
    public function getDisqualified()
    {
        return $this->disqualified;
    }

    /**
     * Set type
     *
     * @return ExtraPointsStore
     */
    public function setDisqualified($disqualified)
    {
        $this->disqualified = $disqualified;
        return $this;
    }


}

