<?php

namespace Aper\IncentiveBundle\Entity;

use Aper\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;



/**
 * EmployeeImportLog
 *
 * @ORM\Table(name="employee_import_log")
 * @ORM\Entity(repositoryClass="Aper\IncentiveBundle\Repository\EmployeeImportLogRepository")
 */
class EmployeeImportLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     *
     * @var EmployeeImportFile
     * @ORM\OneToOne(targetEntity="EmployeeImportFile", cascade={"all"}, mappedBy="log")
     */

    protected $file;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\User")
     */
    protected $user;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var int
     *
     * @ORM\Column(name="month", type="smallint")
     */
    private $month;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var int
     *
     * @ORM\Column(name="podium", type="integer", nullable=true)
     */
    private $podium;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255 , nullable=true )
     */
    private $message;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param EmployeeImportFile $file
     *
     * @return EmployeeImportLog
     */
    public function setFile($file)
    {
        $this->file = $file;
        $file->setLog($this);

        return $this;
    }

    /**
     * Get file
     *
     * @return EmployeeImportFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return EmployeeImportLog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set month
     *
     * @param integer $month
     *
     * @return EmployeeImportLog
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return EmployeeImportLog
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }
    
    /**
     * Set podium
     *
     * @param integer $podium
     *
     * @return EmployeeImportLog
     */
    public function setPodium($podium)
    {
        $this->podium = $podium;

        return $this;
    }

    /**
     * Get podium
     *
     * @return int
     */
    public function getPodium()
    {
        return $this->podium;
    }

    /**
     * Set message
     *
     * @param integer $message
     *
     * @return EmployeeImportLog
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set user
     *
     * @param \Aper\UserBundle\Entity\User $user
     *
     * @return EmployeeImportLog
     */
    public function setUser(\Aper\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Aper\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }



}

