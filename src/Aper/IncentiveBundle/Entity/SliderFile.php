<?php

namespace Aper\IncentiveBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\FileBundle\Model\Image;
use Symfony\Component\Validator\Constraints\Callback;

/**
 * @ORM\Entity
 * @package WebFactory\Bundle\FileBundle\Model
 * @Callback(methods={"validate"})
 *
 */
class SliderFile extends Image
{
    /**
     * Directorio relativo a guardar los archivos
     */
    const DIRECTORY = 'uploads/slider/files';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Slider
     * @ORM\OneToOne(targetEntity="Slider", inversedBy="file")
     */
    protected $slider;

    /**
     * @var UploadedFile
     * @Assert\Image(
     *     minWidth = 200,
     *     minHeight = 200,
     *     maxWidth = 4000,
     *     maxHeight = 4000
     * )
     */
    protected $file;



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Slider
     */
    public function getSlider()
    {
        return $this->slider;
    }

    /**
     * @param Slider $slider
     */
    public function setSlider($slider)
    {
        $this->slider = $slider;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        $this->ensureRequiredFile($context);
    }
}
