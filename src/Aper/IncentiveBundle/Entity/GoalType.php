<?php

namespace Aper\IncentiveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GoalType
 *
 * @ORM\Table(name="goal_type")
 * @ORM\Entity(repositoryClass="Aper\IncentiveBundle\Repository\GoalTypeRepository")
 */
class GoalType
{
    const EXERT = 'exert';

    const MYSTERY_SHOPPER = 'mystery_shopper';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Goal
     *
     * @ORM\OneToMany(targetEntity="Goal", mappedBy="goalType")
     */
    protected $goal;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=255)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="cell", type="string", length=15)
     */
    private $cell;


    /**
     * @var int
     *
     * @ORM\Column(name="month", type="smallint")
     */
    private $month;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="front_symbol", type="string", length=20, nullable=true)
     */
    private $frontSymbol;

    /**
     * @var string
     *
     * @ORM\Column(name="back_symbol", type="string", length=20, nullable=true)
     */
    private $backSymbol;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_special", type="boolean", nullable=true)
     */
    private $isSpecial;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_totalizer", type="boolean", nullable=true)
     */
    private $isTotalizer;

    /**
     * @var self
     *
     * @ORM\ManyToOne(targetEntity="GoalType")
     * @ORM\JoinColumn(nullable=true)
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_type", type="string", length=20)
     */
    private $parentType;


    /**
     * @var int
     *
     * @ORM\Column(name="bt", type="integer")
     */
    private $bt;

    /**
     * @var string
     *
     * @ORM\Column(name="popup_text", type="string", length=255, nullable=true)
     */
    private $popupText;

    public function __construct()
    {
        $this->frontSymbol = '';
        $this->backSymbol = '';
        $this->isSpecial = false;
        $this->isTotalizer = false;
        $this->parentType = '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GoalType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
 * Set category
 *
 * @param string $category
 *
 * @return GoalType
 */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }


    /**
     * Set cell
     *
     * @param string $cell
     *
     * @return GoalType
     */
    public function setCell($cell)
    {
        $this->cell = $cell;

        return $this;
    }

    /**
     * Get cell
     *
     * @return string
     */
    public function getCell()
    {
        return $this->cell;
    }

    /**
     * Set month
     *
     * @param integer $month
     *
     * @return ImportLog
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return ImportLog
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @return string
     */
    public function getFrontSymbol()
    {
        return $this->frontSymbol;
    }

    /**
     * @param string $frontSymbol
     */
    public function setFrontSymbol($frontSymbol)
    {
        $this->frontSymbol = (string) $frontSymbol;
    }

    /**
     * @return string
     */
    public function getBackSymbol()
    {
        return $this->backSymbol;
    }

    /**
     * @param string $backSymbol
     */
    public function setBackSymbol($backSymbol)
    {
        $this->backSymbol = (string) $backSymbol;
    }

    /**
     * @return bool
     */
    public function isSpecial()
    {
        return $this->isSpecial;
    }

    /**
     * @param bool $isSpecial
     */
    public function setIsSpecial($isSpecial)
    {
        $this->isSpecial = (bool) $isSpecial;
    }

    /**
     * @return bool
     */
    public function isTotalizer()
    {
        return $this->isTotalizer;
    }

    /**
     * @param bool $isTotalizer
     */
    public function setIsTotalizer($isTotalizer)
    {
        $this->isTotalizer = (bool) $isTotalizer;
    }

    /**
     * @return self
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param self|null $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        if (null === $parent) {
            $this->setParentType(null);
        } else if ('EXERT' === $parent->getName()) {
            $this->setParentType(self::EXERT);
        } else if ('MISTERY' === $parent->getName()) {
            $this->setParentType(self::MYSTERY_SHOPPER);
        } else {
            $this->setParentType(null);
        }
    }

    /**
     * @return string
     */
    public function getParentType()
    {
        return $this->parentType;
    }

    /**
     * @param string $parentType
     */
    public function setParentType($parentType)
    {
        $this->parentType = (string) $parentType;
    }

    /**
     * @return bool
     */
    public function isExert()
    {
        return self::EXERT === $this->parentType;
    }

    /**
     * @return bool
     */
    public function isMysteryShopper()
    {
        return self::MYSTERY_SHOPPER === $this->parentType;
    }


    /**
     * @return int
     */
    public function getBt()
    {
        return $this->bt;
    }

    /**
     * @param int $bt
     */
    public function setBt($bt)
    {
        $this->bt = $bt;
    }

    /**
     * Set popupText
     *
     * @param string $popupText
     *
     * @return GoalType
     */
    public function setPopupText($popupText)
    {
        $this->popupText = $popupText;

        return $this;
    }

    /**
     * Get popupText
     *
     * @return string
     */
    public function getPopupText()
    {
        return $this->popupText;
    }

    /**
     * @param $year
     * @param $month
     * @return self
     */
    public static function createExert($year, $month)
    {
        $type = new self;
        $type->setYear((int) $year);
        $type->setMonth((int) $month);
        $type->setName('EXERT');
        $type->setCategory('Mantenimiento');
        $type->setCell('EXERT');
        $type->setBackSymbol('Ptos');

        return $type;
    }

    /**
     * @param $year
     * @param $month
     * @return self
     */
    public static function createMisteryShooper($year, $month)
    {
        $type = new self;
        $type->setYear((int) $year);
        $type->setMonth((int) $month);
        $type->setName('MISTERY');
        $type->setCategory('Mantenimiento');
        $type->setCell('MISTERY');
        $type->setBackSymbol('Ptos');

        return $type;
    }
}
