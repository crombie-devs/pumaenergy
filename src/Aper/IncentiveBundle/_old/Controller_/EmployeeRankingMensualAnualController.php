<?php

namespace Aper\IncentiveBundle\Controller;


use Aper\StoreBundle\Entity\Store;
use Aper\UserBundle\Entity\Employee;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Aper\IncentiveBundle\Entity\ImportLog;


class EmployeeRankingMensualAnualController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        //aca tengo que tocar
        $selectorChoices = $this->getSelectorChoices2(new \DateTime());
        $defaultMonth = current($selectorChoices);
        $year = $request->query->get('year', $defaultMonth->format('Y'));
        $month = $request->query->get('month', $defaultMonth->format('n'));
        $selectedYear = new \DateTime("{$year}-{$month}-01");

        $estacionRepository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeeGoal');
        $lastUpload = $estacionRepository->findLastUploadByMonth2();
        $lastUploadDate = [];
        if ($lastUpload) {
            $lastUploadDate = new \DateTime("{$lastUpload[0]['y']}-{$lastUpload[0]['m']}-01");
        }


//        /* @var Store $estacion */
        /* @var Employee $store */
        $store = $this->getUser()->getEmployee();
        $employeeStore = $this->getUser()->getEmployee()->getStore();

        $estacionRepository = $this->getDoctrine()->getRepository('UserBundle:Employee');
        $storeRankings = new ArrayCollection($estacionRepository->findRankingByMonthEmployee($month, $year, 1000, $employeeStore->getId()));
        $goalTypeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeeGoalType');
        $categories = $goalTypeRepository->getUniqueCategories();

        $inTop = false;
        foreach ($storeRankings as $rank) {

            if ($rank == $store) {
                $inTop = true;

            }
        }


        $selectorChoices = $this->getSelectorChoices(new \DateTime());

        //Buscador de podio
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeePodium');
        $podio = $repository->findOneByMY($month, $year);

        if (!$podio){
            $podium = 3;
        }else{
            $podium = $podio->getPosition();
        }


        return $this->render('@Incentive/Frontend/Incentive/ajax/ajax_monthsyears_statistics_employee.html.twig',
            compact('store', 'storeRankings', 'selectedYear', 'selectorChoices','lastUploadDate','categories', 'inTop', 'podium')
        );
    }

    /**
     * @param \DateTime $now
     * @return \DateTime[]
     */
    private function getSelectorChoices(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeeGoal');
        $lastUpload = $goalRepository->findLastUploadWhitResultsByMonthAfterYear(date("Y",time()));
        $choices = [];

        if ($lastUpload) {
            $year = $lastUpload[0]["y"];
            $month = $lastUpload[0]["m"];

            $now = new \DateTime("$year-$month-01");

            for ($i = 0; $i < 12; $i++) {
                $var = clone $now;
                $choices[] = $var->modify("-{$i} month");
            }

            foreach ($choices as $i => $choice){
                $month2 = $choice->format('n');
                $year2  = $choice->format('Y');
                $haveMonth = $goalRepository->findGoalsByMonthWithResults($month2,$year2);

                if ($haveMonth == null){
                    unset($choices[$i]);
                }
                //EXCLUYE MESES QUE NO TENGAN RESULTADOS
                // else {
                //     $have = $goalRepository->find1GoalByMonth($month2,$year2);
                //     if ($have->getResult()==0){
                //         unset($choices[$i]);
                //     }
                // }
            }

            reset($choices);
        }

        return $choices;
    }

    public function getSelectorChoices2(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeeGoal');
        $lastUpload = $goalRepository->findLastUploadWhitResultsByMonthAfterYear(date("Y",time()));

        if (isset($lastUpload[0])){
            $year = $lastUpload[0]["y"];
            $month = $lastUpload[0]["m"];
        }
        else {
            $year = date("Y",time());
            $month = date("m",time());
        }

        $now = new \DateTime("$year-$month-01");

        $choices = [];
        for ($i = 0; $i < 12; $i++) {
            $var = clone $now;
            $choices[] = $var->modify("-{$i} month");
        }

        return $choices;

    }

}

