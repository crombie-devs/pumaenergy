<?php

namespace Aper\IncentiveBundle\Controller;

use Aper\CourseBundle\Entity\Course;
use Aper\CourseBundle\Entity\EmployeeExamen;
use Aper\CourseBundle\Entity\CursoPadre;

use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\Register;
use Aper\StoreBundle\Entity\Localidad;
use Aper\StoreBundle\Entity\Provincia;
use Aper\StoreBundle\Entity\Store;
use Aper\UserBundle\Entity\User;
use Aper\UserBundle\Entity\Profile;
use Aper\UserBundle\Model\Role;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MyEmployeesController extends Controller
{

    private function activateReg($id, $actualUser, $empleador, $estacion){
        $em = $this->getDoctrine()->getManager();
        $reg = $em->getRepository('UserBundle:Register')->find($id);
        if($reg){
            // \Doctrine\Common\Util\Debug::dump($reg);

            $role = '';
            if ($reg->getRole()){
                $role = $reg->getRole();
                switch ($role) {
                    case 'ROLE_HEAD_OFFICE':
                        $role = 'PERFIL 1';
                        break;
                    case 'ROLE_MANAGER':
                        $role = 'PERFIL 2';
                        break;
                    case 'ROLE_EMPLOYEE':
                        $role = 'PERFIL 3';
                        break;
                    case 'ROLE_ADMIN':
                        $role = 'PERFIL 5';
                        break;
                }
            }

            $user = new User();
            $user->setUsername($reg->getDni());
            $user->setEmail($reg->getEmail());
            $user->setPlainPassword('123456');
            $user->setEnabled(true);
            $user->addRole(Role::convertRole($role));
            $user->setCantEmployee(0);
            $employee = new Employee();
            $employee->setCode($reg->getDni());
            $profile = new Profile();
            $profile->setName($reg->getNombre() . ' ' . $reg->getApellido());
        
            $provincia = $em->getRepository('StoreBundle:Provincia')->find($reg->getProvincia());
            $profile->setCorpMail($reg->getEmail());
            $profile->setProvince($provincia->getNombre());

            $employee->setProfile($profile);
            $user->setEmployee($employee);


            if (!is_null($actualUser)) {
                $actualUser->getEmployee()->setEmployee($employee);
            }

            $employee->setStore($estacion);
            $employee->setStores('');

            $em->persist($user);
            $em->remove($reg); // CUANDO LO ACTIVA, LO BORRA
            $em->flush();
        }
    }

    public function ActiveRegisterBckAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $provincias = $em->getRepository('StoreBundle:Provincia')->findAll();
        $localidades = $em->getRepository('StoreBundle:Localidad')->findAll();
        $stores = $em->getRepository('StoreBundle:Store')->findAll();

        $register = $this->getDoctrine()->getRepository(Register::class)->findAll();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $register, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('IncentiveBundle:MyEmployees:indexregbkd.html.twig', array('pagination' => $pagination, "provincias" => $provincias, "localidades" => $localidades, "stores" => $stores));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $empleador = $user->getEmployee();
        $year = date("Y");
        $month = date("m")-1;
        $selectedYear = new \DateTime("{$year}-{$month}-01");

        $estacion = $empleador->getStore();
        $empleadosRepository = $this->getDoctrine()->getRepository(Employee::class);

        $idStores = $empleador->getStores();
        if (!empty($idStores)) {
            $idStores = explode(',', $idStores);
            $idStores[] = $estacion->getCode();
        }

        $empleados = $empleadosRepository->findAllEmployeesByStationActiveOrNotAndNotDeleted($estacion,true, $idStores);

        $curso_padre_repository = $this->getDoctrine()->getRepository(CursoPadre::class);
        $cursos = $curso_padre_repository->findAll();
        // print_r($cursos);exit;

        $porcentajesPorEmpleado = [];

        foreach ($empleados as $empleado){
            $porcentaje_curso = [];

            foreach ($cursos as $curso) {
                // print_r($curso->getId());
                $clases_repository = $this->getDoctrine()->getRepository(Course::class);
                $clases = $clases_repository->findBy(['cursoPadre' => $curso->getId(), 'active' => true]);

                $cont = 0;
                $examenes = [];
                foreach ($clases as $clase) {
                    if($clase->getExamenFinal()){
                        $examenes[] = $clase->getExamenFinal();
                        $cont++;
                    }
                }
                $clases_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);
                $cont_aprobados = 0;
                foreach ($examenes as $examen) {
                    $examenes = $clases_repository->findBy(['employee' => $empleado->getId(),
                        'examen' => $examen, 'aprobado' => true
                    ]);

                    if($examenes){
                        if($examenes[0]->getAprobado())
                            $cont_aprobados++;
                    }
                }
                if($cont_aprobados > 0)
                    $porcentaje = ($cont_aprobados * 100) / $cont;
                else
                    $porcentaje = 0;
                $porcentaje_curso[] = [$curso->getId(), $porcentaje];
            }
            $porcentajesPorEmpleado[$empleado->getId()] = $porcentaje_curso;
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $empleados, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('IncentiveBundle:MyEmployees:index.html.twig', array('pagination' => $pagination, 'empleados' => $empleados, 'selectedYear' => $selectedYear, 'porcentajesPorEmpleado'=>$porcentajesPorEmpleado, 'cursos' => $cursos, 'totalEmpleados' => sizeof($empleados), 'empleador' => $empleador));
    }


    /**
     * @param Request $request
     * @return Response
     */
    public function indexEmployeesInactiveAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $empleador = $user->getEmployee();
        $estacion = $empleador->getStore();

        $provincias = $em->getRepository('StoreBundle:Provincia')->findAll();
        $localidades = $em->getRepository('StoreBundle:Localidad')->findAll();
        $stores = $em->getRepository('StoreBundle:Store')->findAll();
        // Busco los registrados con el id del store.
        // Los hago empleados inactivos y luego los elimino.
        $em = $this->getDoctrine()->getManager();
        $reg = $em->getRepository('UserBundle:Register')->findBy(['store' => $estacion->getId()]);
        $user->setCantEmployee(sizeof($reg)); 
        $em->flush();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $reg, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('IncentiveBundle:MyEmployees:indexInactives.html.twig', array('pagination' => $pagination, 'empleador' => $empleador, "provincias" => $provincias, "localidades" => $localidades, "stores" => $stores));
    }


    public function activeEmployeeAction($id, Request $request){
        $actualUser = $this->get('security.token_storage')->getToken()->getUser();
        $empleador = $actualUser->getEmployee();
        $estacion = $empleador->getStore();
        $this->activateReg($id, $actualUser, $empleador, $estacion);
        return $this->redirect($this->generateUrl('aper_my_employees_inactive'));
    }

    public function activeEmployeeBKAction($id, Request $request){
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $reg = $em->getRepository('UserBundle:Register')->find($id);

        if($reg){
            $store_id = $reg->getStore();
            $employees = $em->getRepository('UserBundle:Employee')->findBy(["store" => $store_id]);
            $actualUser = null;
            $empleador = null;
            $estacion = null;

            foreach ($employees as $key => $employee) {
                if(in_array('ROLE_MANAGER', $employee->getUser()->getRoles())){
                    $actualUser = $employee->getUser();
                    $empleador = $employee;
                    $estacion = $employee->getStore();
                    break;
                }
            }

            if (is_null($estacion)) {
                $estacion = $em->getRepository('StoreBundle:Store')->find($store_id);
            }

            $this->activateReg($id, $actualUser, $empleador, $estacion);
        }
        return $this->redirect($this->generateUrl('backend_employees_registered'));
    }

    public function disableEmployeeAction($id, Request $request)
    {
//        die();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $employeeRepository = $this->getDoctrine()->getRepository('UserBundle:Employee');
        $empleado = $employeeRepository->find($id);
//        if($empleado->getBoss()==$user->getEmployee()){
            $em = $this->getDoctrine()->getManager();
            $userEmployee = $empleado->getUser();
            $userEmployee->setEnabled(false);
            $em->persist($userEmployee);
            $em->flush();

//        }

        return $this->redirect($this->generateUrl('aper_my_employees'));

    }

    public function activateEmployeeAction($id, Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $employeeRepository = $this->getDoctrine()->getRepository('UserBundle:Employee');
        $empleado = $employeeRepository->find($id);
//        if($empleado->getBoss()==$user->getEmployee()){
            $em = $this->getDoctrine()->getManager();
            $userEmployee = $empleado->getUser();
            $userEmployee->setEnabled(true);
            $em->persist($userEmployee);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('aper_my_employees_inactive'));

    }

    public function deleteEmployeeAction($id, Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $employeeRepository = $this->getDoctrine()->getRepository('UserBundle:Employee');
        $empleado = $employeeRepository->find($id);
        $em = $this->getDoctrine()->getManager();
        $empleado->setDeleted(true);
        $em->persist($empleado);
        $userEmployee = $empleado->getUser();
        $userEmployee->setEnabled(false);
        $em->persist($userEmployee);
        $em->flush();

        return $this->redirect($this->generateUrl('aper_my_employees'));

    }


}
