<?php

namespace Aper\IncentiveBundle\Controller;

use Aper\IncentiveBundle\Entity\Slider;
use Aper\IncentiveBundle\Entity\SliderFile;
use Aper\IncentiveBundle\Form\SliderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;


class SliderController extends Controller
{


    /**
     * Add a new image file
     *
     * @Route()e("/", name="aper_slider_add")
     * @Method()d({"GET","POST"})
     */
    public function addAction(Request $request)
    {

        $slider = new Slider();

        $form = $this->createForm(SliderType::class, $slider);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($slider);
            $em->flush();
            $this->addFlash('success', 'Ha sido creado un nuevo slider en la posición: '.$slider->getSort());

            return $this->redirectToRoute('aper_slider_index');


        }

        return $this->render('IncentiveBundle:Slider:add.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $slider = $em->getRepository('IncentiveBundle:Slider')->find($id);

        $form = $this->createForm(SliderType::class, $slider);
        $form->handleRequest($request);
       // $slider->setFile(New SliderFile($slider->getFile()));



        if(!$slider){
            throw $this->createNotFoundException('Slider no encontrado');
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $em->flush();

            $this->addFlash('success', 'El Slider ha sido modificado');

            return $this->redirectToRoute('aper_slider_index');
        }

        return $this->render('IncentiveBundle:Slider:edit.html.twig', array('slider' => $slider, 'form' => $form->createView()));
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $slider = $em->getRepository('IncentiveBundle:Slider')->find($id);
        $em->remove($slider);
        $em->flush();
        $successMessage = 'Slider eliminado';
        $this->addFlash('success', $successMessage);

        return $this->redirectToRoute('aper_slider_index');
    }

    public function indexAction(Request $request)
    {
        //$user = $this->get('security.token_storage')->getToken()->getUser();
        $slider = $this->getDoctrine()->getRepository('IncentiveBundle:Slider')->findSlider();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $slider, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('IncentiveBundle:Slider:index.html.twig', array('pagination' => $pagination, 'slider' => $slider));
    }


    public function showAction($id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:Slider');

        $slider = $repository->find($id);

        if (!$slider) {
            $messageException = 'Slider no encontrado';
            throw $this->createNotFoundException($messageException);
        }

        return $this->render('IncentiveBundle:Slider:show.html.twig', ['slider' => $slider]);
    }

}
