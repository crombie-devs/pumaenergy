<?php

namespace Aper\IncentiveBundle\Controller;


use Aper\StoreBundle\Entity\Store;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EmployeeRankingMensualHistoricoController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request) {
        $selectorChoices = $this->getSelectorChoices2(new \DateTime());
        $defaultMonth = current($selectorChoices);
        $year = $request->query->get('year', $defaultMonth->format('Y'));
        $month = $request->query->get('month', $defaultMonth->format('n'));
        $selectedMonth = new \DateTime("{$year}-{$month}-01");

        $estacionRepository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeeGoal');

        // BUSCAR ULTIMOS OBJETIVOS ANTERIORES AL AÑO ESTABLECIDO
        $lastUpload = $estacionRepository->findLastUploadByMonth2BeforeYear(date("Y",time()));

        if(isset($lastUpload[0]))
            $lastUploadDate = new \DateTime("{$lastUpload[0]['y']}-{$lastUpload[0]['m']}-01");
        else
            $lastUploadDate = false;

        if(isset($lastUpload[0]))
            $lastUploadDate2 = new \DateTime("{$lastUpload[1]['y']}-{$lastUpload[1]['m']}-01");
        else
            $lastUploadDate2 = false;


        /* @var Store $estacion */
        $estacion = $this->getUser()->getEmployee()->getStore();
        $estacionRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
        $estacionRankings = new ArrayCollection($estacionRepository->findRankingByMonth($month, $year));

        $goalTypeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeeGoalType');
        $categories = $goalTypeRepository->getUniqueCategories();

        $inTop = false;

        foreach ($estacionRankings as $rank) {
            if ($rank == $estacion) {
                $inTop = true;
            }
        }

        $selectorChoices = $this->getSelectorChoices(new \DateTime());

        $user = $this->get('security.token_storage')->getToken()->getUser();

        //Buscador de podio
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeePodium');
        $podio = $repository->findOneByMY($month, $year);

        if (!$podio)
            $podium = 3;
        else
            $podium= $podio->getPosition();
        
        //Buscador de mensaje objetivo mensual sorpresa
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeeSpecialGoalMessage');
        $specialGoalMessage = $repository->findOneByMonthYear($month, $year);

        $textSpecialGoalMessage= (!$specialGoalMessage) ? "No está leyendo la base de datos" : $specialGoalMessage->getMessage();

        return $this->render('@Incentive/Frontend/Incentive/ajax/ajax_history_months_statistics.html.twig',
            compact('store', 'storeRankings', 'selectedMonth', 'selectorChoices','selectorYear', 'categories', 'lastUploadDate','lastUploadDate2','inTop', 'user', 'podium','textSpecialGoalMessage')
        );
    }

    /**
     * @param \DateTime $now
     * @return \DateTime[]
     */
    public function getSelectorChoices(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeeGoal');
        $lastUpload = $goalRepository->findLastUploadByMonthBeforeYear(date("Y",time()));
        $year = $lastUpload[0]["y"];
        $month = $lastUpload[0]["m"];

        $now = new \DateTime("$year-$month-01");

        $choices = [];
        for ($i = 0; $i < 12; $i++) {
            $var = clone $now;
            $aux = $var->modify("-{$i} month");
    
            if($aux->format('Y') == $year)
                $choices[] = $aux;
        }

        foreach ($choices as $i => $choice){
            $month2 = $choice->format('n');
            $year2  = $choice->format('Y');
            $haveMonth = $goalRepository->findGoalsByMonth($month2,$year2);

            if ($haveMonth == null){
                unset($choices[$i]);
            }
            //EXCLUYE MESES QUE NO TENGAN RESULTADOS
            // else {
            //     $have = $goalRepository->find1GoalByMonth($month2,$year2);
            //     if ($have->getResult()==0){
            //         unset($choices[$i]);
            //     }
            // }
        }

        reset($choices);

        return $choices;
    }
    
    public function getSelectorChoices2(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeeGoal');
        $lastUpload = $goalRepository->findLastUploadByMonthBeforeYear(date("Y",time()));

        $year = $lastUpload[0]["y"];
        $month = $lastUpload[0]["m"];

        $now = new \DateTime("$year-$month-01");

        $choices = [];
        // for ($i = 0; $i < 12; $i++) {
        //     $var = clone $now;
        //     $choices[] = $var->modify("-{$i} month");
        // }
        $choices[] = $now;
        return $choices;
    }
}
