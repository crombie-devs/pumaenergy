<?php

namespace Aper\IncentiveBundle\Controller;


use Aper\StoreBundle\Entity\Store;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Aper\IncentiveBundle\Entity\ImportLog;


class RankingPerformanceController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {

        if (isset($_GET['userlegal'])) {
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $user->setLegales();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            echo json_encode(array('status' => 1));
            exit;
        }


        ini_set("memory_limit",-1);
        if (isset($_GET['json']) && $_GET['json'] && isset($_GET['cat']) && !empty($_GET['cat']) && isset($_GET['store']) && !empty($_GET['store'])) {

            $selectorChoices = $this->getSelectorChoices2(new \DateTime()); //Lista de meses con resultados!
            $defaultMonth = current($selectorChoices);
            $year = $request->query->get('year', $defaultMonth->format('Y'));
            $month = $request->query->get('month', $defaultMonth->format('n'));
            $selectedMonth = new \DateTime("{$year}-{$month}-01");

            $result = [];

            $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
            $store = $storeRepository->findOneById((int) $_GET['store']);

            $goalTypeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');
            $categories = $goalTypeRepository->getUniqueCategories();



            $result['porcentajeTotal'] = 34;
            $result['promedioTotal'] = 44;
            $result['leyendaCalidad'] = false;
            $result['items'] = [];


            //Buscamos el promedio de cada una de las variables de TODAS LA RED
            $repository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
            list($promediosRed, $promediosRedCategory) = $repository->findGoalsAVG($month, $year);


            foreach ($categories as $key => $type) {
                if ($_GET['cat'] == $type) {
                    $total_objective = 0;
                    if ($goal = $store->getGoalsByMonthAndName($selectedMonth, 'PUNTAJES OBJETIVO', $type)){
                        $goal = array_shift(current($goal));
                        if ($goal) {
                            $total_objective = $goal->getResult();
                        }

                        // $total_objective = $goal->getObjetive();
                    }

                    $total_result = 0;
                    if ($goal = $store->getGoalsByMonthAndName($selectedMonth, 'PUNTAJES RESULTADO', $type)){
                        $goal = array_shift(current($goal));
                        if ($goal) {
                            $total_result = $goal->getResult();
                        }
                        // $total_objective = $goal->getObjetive();
                    }

                    $items = $store->getGoalsByMonthAndCategory($selectedMonth, $type);

                    foreach ($items as $key => $item) {
                        if ($item->getGoalType()->getName() != 'PUNTAJES OBJETIVO' &&  $item->getGoalType()->getName() != 'PUNTAJES RESULTADO') {
                            $_item = [];
                            $_item['titulo'] = (ucfirst($item->getGoalType()->getName()));
                            $_item['especial'] = ($item->getGoalType()->isSpecial());
                            $_item['cumple'] = round($item->getResultMet());
                            $_item['objetivo'] = round($item->getObjetiveMet());
                            $_item['promedio'] = $promediosRed[$item->getGoalType()->getId()];
                            // print_r($_item);exit;
                            $result['items'][] = $_item;
                        }
                    }

                }
                    // {% include '@Incentive/Frontend/Incentive/widgetsEmployee/points-table.html.twig' with {
                    // 'render_values': true,
                    // 'title': $type,
                    // 'total_objetive': $store->getGoalsByMonthAndName($selectedMonth, 'PUNTAJES OBJETIVO', $type).empty ? 0 : $store->getGoalsByMonthAndName($selectedMonth, 'PUNTAJES OBJETIVO', $type).first.result,
                    // 'total_result': $store->getGoalsByMonthAndName($selectedMonth, 'PUNTAJES RESULTADO', $type).empty ? 0 : $store->getGoalsByMonthAndName($selectedMonth, 'PUNTAJES RESULTADO', $type).first.result,
                    // 'items': $store->getGoalsByMonthAndCategory($selectedMonth, $type),
                    // 'message': textSpecialGoalMessageSelector,
                    // } only %}

            }

            echo json_encode($result);
            exit;
            // echo 'aca';exit;
        }

        $selectorChoices = $this->getSelectorChoices2(new \DateTime());
        $defaultMonth = current($selectorChoices);
        $year = $request->query->get('year', $defaultMonth->format('Y'));
        $month = $request->query->get('month', $defaultMonth->format('n'));

        $selectedYear = new \DateTime("{$year}-{$month}-01");

        $storeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $storeRepository->findLastUploadByMonth2();

        if($lastUpload){
            $lastUploadDate = new \DateTime("{$lastUpload[0]['y']}-{$lastUpload[0]['m']}-01");
        }


        $user = $this->get('security.token_storage')->getToken()->getUser();
        // $responsable = 'Sebastian Castro';

        $searchZona = (isset($_GET['zone']) ? $_GET['zone'] : null);
        $idStores = null;

        if ($user->getMaxRoleType() == 'ROLE_HEAD_OFFICE') {
            $responsable = $user->getId();
            $idStore = null;
        } elseif ($user->getMaxRoleType() == 'ROLE_MANAGER') {
            $responsable = null;
            $idStore = null;
            $idStores = $this->getUser()->getEmployee()->getStores();
            if (empty($idStores)) {
                $idStore = $this->getUser()->getEmployee()->getStore()->getId();
            } else {
                $idStores = explode(',', $idStores);
                $idStores[] = $this->getUser()->getEmployee()->getStore()->getCode();
            }
        } else {
            $responsable = null;
            $idStore = null;
            if (is_null($searchZona)) {
                $searchZona = 'Centro';
            }
        }

        /* @var Store $store */
        $store = $this->getUser()->getEmployee()->getStore();

        $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
        $storeRankings = new ArrayCollection($storeRepository->findPerformance($month, $year, 1000, $responsable, $searchZona, $idStore, $idStores));
        $goalTypeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');
        $categories = $goalTypeRepository->getUniqueCategories();

        $items = $goalTypeRepository->findByMonthAndYear($year, $month);
        $itemsList = [];
        $itemsListOtros = [];
        foreach ($items as $key => $value) {
            $cat = $value->getCategory();
            if ($cat != 'TOTAL_STORE_OBJETIVO' && $cat != 'TOTAL_STORE_RESULTADO') {
                $itemsList[$cat][] = $value;
                $itemsListOtros[$cat][$value->getId()] = $value->getName();
            }
        }
        // print_r($itemsListOtros);

        $storesComplete = new ArrayCollection($storeRepository->findStoresByRepresentante($responsable, $searchZona, $idStore, $idStores));
        


        //Buscamos el promedio de cada una de las variables de TODAS LA RED
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        list($promediosRed, $promediosRedCategory) = $repository->findGoalsAVG($month, $year);

        // print_r($promediosRed);
        // print_r($promediosRedCategory);exit;

        $promediosRedCat = [];
        $_global_objetivo = 0;
        $_global_promedio = 0;
        foreach ($promediosRedCategory as $catName => $values) {
            $_objetivo = 0;
            $_promedio = 0;
            foreach ($values as $key => $value) {
                $_objetivo+= $value['objetivo'];
                $_promedio+= $value['promedio'];
                $_global_objetivo+= $value['objetivo'];
                $_global_promedio+= $value['promedio'];
            }

            $promediosRedCat[$catName] = round($_promedio * 100 / $_objetivo);
        }

        $promedioGlobal = round($_global_promedio * 100 / $_global_objetivo);




        //Buscamos el promedio de cada una de las variables de TODAS LAS ESTACIONES QUE PERTENECEN AL USUARIO
        $storesIds = [];
        $zonas = [];
        foreach ($storeRankings as $key => $value) {
            $storesIds[] = $value->getId();

            $nZone = ucfirst(strtolower($value->getZona()));

            $zonas[$nZone] = $nZone;
        }
        $zonas = implode(' / ', $zonas);


        list($promediosEstaciones, $promediosEstacionesCategory) = $repository->findGoalsAVGStores($month, $year, $storesIds);

        $promediosEstacionesCat = [];
        $_global_objetivo_estaciones = 0;
        $_global_promedio_estaciones = 0;
        foreach ($promediosEstacionesCategory as $catName => $values) {
            $_objetivo = 0;
            $_promedio = 0;
            foreach ($values as $key => $value) {
                $_objetivo+= $value['objetivo'];
                $_promedio+= $value['promedio'];
                $_global_objetivo_estaciones+= $value['objetivo'];
                $_global_promedio_estaciones+= $value['promedio'];
            }

            $promediosEstacionesCat[$catName] = round($_promedio * 100 / $_objetivo);
        }

        $promedioGlobalEstaciones = 0;
        if ($_global_objetivo_estaciones) {
            $promedioGlobalEstaciones = round($_global_promedio_estaciones * 100 / $_global_objetivo_estaciones);
        }


        $goalTypeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');
        $categories = $goalTypeRepository->getUniqueCategories();


        $_globalStoreRankings = [];
        foreach ($categories as $i => $nameCat) {
            $_storeRankings = [];
            $_storeRankingsPos = [];

            $_tempStores = [];
            foreach ($storeRankings as $key => $store) {
                $_totalPoints = 0;
                if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'PUNTAJES OBJETIVO', $nameCat)){
                    $goal = array_shift(current($goal));
                    $_totalPoints = $goal->getObjetive();
                }

                if ($goal = $store->getGoalsByMonthAndName($selectedYear, 'PUNTAJES RESULTADO', $nameCat)){
                    $goal = array_shift(current($goal));
                    $_completedPoints = $goal->getResult();
                }

                $_stoRank = 0;
                if ($ss = $store->getRankingByMonthAndYear($selectedYear, $store)){
                    $ss = array_shift(current($ss));
                    $_stoRank = $ss->getPosition();

                    // $_stoRank = $store->getRankingByMonthAndYear($selectedYear, $store)[0]->getPosition();
                }

                $n = ($_completedPoints / $_totalPoints);
                $num = explode('.', $n);
                $_position = $num[0];
                if (isset($num[1])) $_position = $num[1];

                $_position = substr($_position, 0, 2);
                if ($_position == 1) $_position = 100;

                $_storeRankingsPos[] = $_position;

                $aa =[];
                $aa['store'] = $store;
                $aa['totalPoints'] = $_totalPoints;
                $aa['completedPoints'] = $_completedPoints;
                $aa['stoRank'] = $_stoRank;

                $_storeRankings[] = $aa;

                $_tempStores[$store->getId()] = $store->getId();
            }

            foreach ($storesComplete as $key => $store) {
                if (!isset($_tempStores[$store->getId()])) {
                    $aa =[];
                    $aa['store'] = $store;
                    $aa['totalPoints'] = 0;
                    $aa['completedPoints'] = 0;
                    $aa['stoRank'] = 0;
                    $_storeRankings[] = $aa;
                    $_storeRankingsPos[] = 0;
                }
            }

            asort($_storeRankingsPos);

            $finalPos = array_reverse($_storeRankingsPos, true);
            
            $_tempStoreRankings = [];
            foreach ($finalPos as $key => $value) {
                $_tempStoreRankings[] = $_storeRankings[$key];
            }

            $_globalStoreRankings[$nameCat] = $_tempStoreRankings;

        }

        $storeRankings = $_globalStoreRankings;
// echo count($itemsList);

        return $this->render('@Incentive/Frontend/Incentive/ajax/ajax_performance.html.twig',
            compact(
                'storesComplete'. 'store', 'storeRankings', 
                'itemsList', 'selectedYear','lastUploadDate', 'categories', 'zonas', 
                'promediosRed', 'promediosRedCat', 'promedioGlobal', 'promediosRedCategory', 
                'promediosEstaciones', 'promediosEstacionesCat', 'promedioGlobalEstaciones', 'promediosEstacionesCategory'
            )
        );
    }

    /**
     * @param \DateTime $now
     * @return \DateTime[]
     */
    private function getSelectorChoices(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadWhitResultsByMonthAfterYear(date("Y",time()));
        if($lastUpload){
            $year = $lastUpload[0]["y"];
            $month = $lastUpload[0]["m"];
        }  else {
            $year = date("Y",time());
            $month = date("m",time());
        }


        $now = new \DateTime("$year-$month-01");

        $choices = [];
        for ($i = 0; $i < 12; $i++) {
            $var = clone $now;
            $choices[] = $var->modify("-{$i} month");
        }

        foreach ($choices as $i => $choice){
            $month2 = $choice->format('n');
            $year2  = $choice->format('Y');
            $haveMonth = $goalRepository->findGoalsByMonthWithResults($month2,$year2);

            if ($haveMonth == null){
                unset($choices[$i]);
            }
            //EXCLUYE MESES QUE NO TENGAN RESULTADOS
            // else {
            //     $have = $goalRepository->find1GoalByMonth($month2,$year2);
            //     if ($have->getResult()==0){
            //         unset($choices[$i]);
            //     }
            // }
        }

        reset($choices);

        return $choices;
    }

    public function getSelectorChoices2(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadWhitResultsByMonthAfterYear(date("Y",time()));

        if (isset($lastUpload[0])){
            $year = $lastUpload[0]["y"];
            $month = $lastUpload[0]["m"];
        }
        else {
            $year = date("Y",time());
            $month = date("m",time());
        }

        $now = new \DateTime("$year-$month-01");

        $choices = [];
        for ($i = 0; $i < 12; $i++) {
            $var = clone $now;
            $choices[] = $var->modify("-{$i} month");
        }

        return $choices;

    }

}

