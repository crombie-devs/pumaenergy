<?php

namespace Aper\IncentiveBundle\Controller;


use Aper\StoreBundle\Entity\Store;
use Aper\UserBundle\Entity\Employee;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Aper\IncentiveBundle\Entity\ExtraPointsStore;

class EmployeeRankingAnualController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $selectorChoices = $this->getSelectorChoices(new \DateTime());
        if ($selectorChoices) {
            $defaultYear = current($selectorChoices);
            $year = $request->query->get('year', $defaultYear->format('Y'));
        } else {
            $year = date('Y');
        }
        $selectedYear = new \DateTime("{$year}-01-01");

        $store = $this->getUser()->getEmployee();
        $employeeStore = $this->getUser()->getEmployee()->getStore();

        
        $empleadoRepository = $this->getDoctrine()->getRepository('UserBundle:Employee');
        $storeRankings = new ArrayCollection($empleadoRepository->findRankingByYearEmployee($year, $employeeStore->getId()));


        $user = $this->get('security.token_storage')->getToken()->getUser();

        $podium = 1; //solo se muestra una copa en el ranking anual

        
        return $this->render('@Incentive/Frontend/Incentive/ajax/ajax_years_statistics_employee.html.twig',
            compact('store', 'storeRankings', 'selectedYear', 'selectorChoices', 'user', 'podium')
        );
    }

    /**git push
     * @param \DateTime $now
     * @return \DateTime[]
     */
    private function getSelectorChoices(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:EmployeeGoal');
        $lastUpload = $goalRepository->findLastUploadByMonth();
        $choices = [];
        if ($lastUpload) {
            $year=$lastUpload[0]["y"];
            $month=$lastUpload[0]["m"];
            $now= new \DateTime("$year-$month-01");

            $choices = [];
            for ($i = 0 ; $i < 12 ; $i++) {
                $var = clone $now;
                $choices[] = $var->modify("-{$i} year");
            }

            foreach ($choices as $i => $choice){
                $year2  = $choice->format('Y');
                $haveMonth = $goalRepository->findGoalsByYear($year2);

                if ($haveMonth == null){
                    unset($choices[$i]);
                }
            }
            reset($choices);
        }


        return $choices;
    }
}

