<?php

namespace Aper\IncentiveBundle\Controller;


use Aper\StoreBundle\Entity\Store;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Aper\IncentiveBundle\Entity\ImportLog;


class RankingMensualAnualController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        ini_set("memory_limit",-1);
        // exit;
        $selectorChoices = $this->getSelectorChoices2(new \DateTime());
        $defaultMonth = current($selectorChoices);
        $year = $request->query->get('year', $defaultMonth->format('Y'));
        $month = $request->query->get('month', $defaultMonth->format('n'));
        $selectedYear = new \DateTime("{$year}-{$month}-01");

        $storeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $storeRepository->findLastUploadByMonth2();
        if($lastUpload){
            $lastUploadDate = new \DateTime("{$lastUpload[0]['y']}-{$lastUpload[0]['m']}-01");
        }


        $zone = (isset($_GET['zone']) ? $_GET['zone'] : '');
        $showZone = 1;


        if ($zone == 'general') {
            unset($_GET['zone']);
        }

        if ($this->getUser()->getMaxRoleType() != 'ROLE_HEAD_OFFICE' && $this->getUser()->getMaxRoleType() != 'ROLE_MANAGER_GLOBAL') {
            $_GET['zone'] = 'zone';
            $showZone = 0;
        }

        /* @var Store $store */
        $store = $this->getUser()->getEmployee()->getStore();

        $theZones = array(
            'Centro' => 'Centro',
            'Norte' => 'Norte',
            'Sur' => 'Sur',
            'Red Propia' => 'Red Propia'
        );

        $searchZone = null;
        if (isset($_GET['zone'])) {
            if ($this->getUser()->getMaxRoleType() != 'ROLE_MANAGER_GLOBAL') {
                if ($this->getUser()->getMaxRoleType() == 'ROLE_HEAD_OFFICE') {
                    $searchZone = $this->getUser()->getEmployee()->getArea();
                    if (!isset($theZones[$searchZone])) {
                        $searchZone = $store->getZona();
                    }
                } else {
                    $searchZone = $store->getZona();
                }
            } else {
                $searchZone = $zone;
            }
        }


        $idStores = null;

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->getMaxRoleType() == 'ROLE_MANAGER') {
            $idStores = $this->getUser()->getEmployee()->getStores();
            if (!is_null($idStores)) {
                $idStores = explode(',', $idStores);
                $idStores[] = $store->getCode();
            }
        }

        $storesComplete = [];
        if ($idStores) {
            $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
            $storesComplete = new ArrayCollection($storeRepository->findStoresByCodes($month, $year, $idStores));
        }



        $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
        //$storeRankings = new ArrayCollection($storeRepository->findRankingByMonth($month, $year, 1000, (isset($_GET['zone']) ? $store->getZona() : null)));
        $storeRankings = new ArrayCollection($storeRepository->findRankingByMonth($month, $year, 220, $searchZone));
        $goalTypeRepository = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');
        $categories = $goalTypeRepository->getUniqueCategories();
// echo 'asdasdasd - ';
// echo count($storeRankings);exit;

        $inTop = false;
        foreach ($storeRankings as $rank) {

            if ($rank == $store) {
                $inTop = true;
            }
        }
        $selectorChoices = $this->getSelectorChoices(new \DateTime());

        //Buscador de podio
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:Podium');
        $podio = $repository->findOneByMY($month, $year);

        if (!$podio){
            $podium = 3;
        }else{
            $podium = $podio->getPosition();
        }        

        // print_r($month);exit;
        // echo 'asdasd';exit;
        // print_r(count($showZone));
        // exit;

        return $this->render('@Incentive/Frontend/Incentive/ajax/ajax_monthsyears_statistics.html.twig',
            compact('store', 'storeRankings', 'selectedYear', 'selectorChoices','lastUploadDate','categories', 'inTop', 'podium', 'zone', 'showZone', 'idStores', 'storesComplete')
        );
    }

    /**
     * @param \DateTime $now
     * @return \DateTime[]
     */
    private function getSelectorChoices(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadWhitResultsByMonthAfterYear(date("Y",time()));
        if($lastUpload){
            $year = $lastUpload[0]["y"];
            $month = $lastUpload[0]["m"];
        }  else {
            $year = date("Y",time());
            $month = date("m",time());
        }


        $now = new \DateTime("$year-$month-01");

        $choices = [];
        for ($i = 0; $i < 12; $i++) {
            $var = clone $now;
            $choices[] = $var->modify("-{$i} month");
        }

        foreach ($choices as $i => $choice){
            $month2 = $choice->format('n');
            $year2  = $choice->format('Y');
            $haveMonth = $goalRepository->findGoalsByMonthWithResults($month2,$year2);

            if ($haveMonth == null){
                unset($choices[$i]);
            }
            //EXCLUYE MESES QUE NO TENGAN RESULTADOS
            // else {
            //     $have = $goalRepository->find1GoalByMonth($month2,$year2);
            //     if ($have->getResult()==0){
            //         unset($choices[$i]);
            //     }
            // }
        }

        reset($choices);

        return $choices;
    }

    public function getSelectorChoices2(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadWhitResultsByMonthAfterYear(date("Y",time()));

        if (isset($lastUpload[0])){
            $year = $lastUpload[0]["y"];
            $month = $lastUpload[0]["m"];
        }
        else {
            $year = date("Y",time());
            $month = date("m",time());
        }

        $now = new \DateTime("$year-$month-01");

        $choices = [];
        for ($i = 0; $i < 12; $i++) {
            $var = clone $now;
            $choices[] = $var->modify("-{$i} month");
        }

        return $choices;

    }

}

