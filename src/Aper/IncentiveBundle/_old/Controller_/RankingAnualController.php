<?php

namespace Aper\IncentiveBundle\Controller;


use Aper\StoreBundle\Entity\Store;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Aper\IncentiveBundle\Entity\ExtraPointsStore;

class RankingAnualController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        ini_set("memory_limit",-1);

        $selectorChoices = $this->getSelectorChoices(new \DateTime());

        $defaultYear = current($selectorChoices);
        $year = $request->query->get('year', $defaultYear->format('Y'));
        $selectedYear = new \DateTime("{$year}-01-01");

        $zone = (isset($_GET['zone']) ? $_GET['zone'] : '');
        $showZone = 1;


        if ($zone == 'general') {
            unset($_GET['zone']);
        }

        if ($this->getUser()->getMaxRoleType() != 'ROLE_HEAD_OFFICE' && $this->getUser()->getMaxRoleType() != 'ROLE_MANAGER_GLOBAL') {
            $_GET['zone'] = 'zone';
            $showZone = 0;
        }

        /* @var Store $store */
        $store = $this->getUser()->getEmployee()->getStore();

        $theZones = array(
            'Centro' => 'Centro',
            'Norte' => 'Norte',
            'Sur' => 'Sur',
            'Red Propia' => 'Red Propia'
        );

        $searchZone = null;
        if (isset($_GET['zone'])) {
            if ($this->getUser()->getMaxRoleType() != 'ROLE_MANAGER_GLOBAL') {
                if ($this->getUser()->getMaxRoleType() == 'ROLE_HEAD_OFFICE') {
                    $searchZone = $this->getUser()->getEmployee()->getArea();
                    if (!isset($theZones[$searchZone])) {
                        $searchZone = $store->getZona();
                    }
                } else {
                    $searchZone = $store->getZona();
                }
            } else {
                $searchZone = $zone;
            }
        }

        $idStores = null;
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->getMaxRoleType() == 'ROLE_MANAGER') {
            $idStores = $this->getUser()->getEmployee()->getStores();
            if (!is_null($idStores)) {
                $idStores = explode(',', $idStores);
                $idStores[] = $store->getCode();
            }
        }
        $storesComplete = [];
        if ($idStores) {
            $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
            $storesComplete = new ArrayCollection($storeRepository->findStoresByCodes(NULL, $year, $idStores));
        }

        $storeRepository = $this->getDoctrine()->getRepository('StoreBundle:Store');
        $storeRankings=new ArrayCollection($storeRepository->findRankingByYear($year, $searchZone));

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $podium = 1; //solo se muestra una copa en el ranking anual
        return $this->render('@Incentive/Frontend/Incentive/ajax/ajax_years_statistics.html.twig',
            compact('store', 'storeRankings', 'selectedYear', 'selectorChoices', 'user', 'podium', 'zone', 'showZone', 'idStores', 'storesComplete')
        );
    }

    /**git push
     * @param \DateTime $now
     * @return \DateTime[]
     */
    private function getSelectorChoices(\DateTime $now)
    {
        $goalRepository = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $lastUpload = $goalRepository->findLastUploadByMonth();

        if (isset($lastUpload[0])){
            $year=$lastUpload[0]["y"];
            $month=$lastUpload[0]["m"];
        }
        else {
            $year = date("Y",time());
            $month = date("m",time());
        }

        
        $now= new \DateTime("$year-$month-01");
        $choices = [];
        for ($i = 0 ; $i < 1 ; $i++) {
            $var = clone $now;
            $choices[] = $var->modify("-{$i} year");
        }

//         print_r($choices);exit;

//         foreach ($choices as $i => $choice){
//             // if($choice->format('Y') == 2018){
//             //     unset($choices[$i]);
//             //     continue;
//             // }

//             $year2  = $choice->format('Y');
//             $haveMonth = $goalRepository->findGoalsByYear($year2);
// echo 'adasd';exit;

//             if ($haveMonth == null){
//                 unset($choices[$i]);
//             }
//         }
        reset($choices);


        return $choices;
    }
}

