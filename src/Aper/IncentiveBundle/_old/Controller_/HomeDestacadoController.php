<?php

namespace Aper\IncentiveBundle\Controller;

use Aper\IncentiveBundle\Entity\HomeDestacado;
use Aper\IncentiveBundle\Entity\Slider;
use Aper\IncentiveBundle\Entity\SliderFile;
use Aper\IncentiveBundle\Entity\HomeDestacadoFile;
use Aper\IncentiveBundle\Form\HomeDestacadoType;
use Aper\IncentiveBundle\Form\SliderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;

class HomeDestacadoController extends Controller
{
    /**
     * Add a new image file
     *
     * @Route()e("/", name="aper_home_destacado_add")
     * @Method()d({"GET","POST"})
     */
    public function addAction(Request $request)
    {
        $homeDestacado = new HomeDestacado();

        $form = $this->createForm(HomeDestacadoType::class, $homeDestacado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();


            //aqui, si el nuevo home es ACTIVO, se cambia otros
            // home que tengan el estado activo para el mismo rol a inactivo
            if($homeDestacado->isActivo()){
                $this->cambiarActivoSegunRole($homeDestacado);
            }

            $em->persist($homeDestacado);
            $em->flush();
            $this->addFlash('success', 'Ha sido creado un nuevo slider en la posición: '.$homeDestacado->getSort());

            return $this->redirectToRoute('aper_home_destacado_index');

        }


        return $this->render('IncentiveBundle:HomeDestacado:add.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $homeDestacado = $em->getRepository('IncentiveBundle:HomeDestacado')->find($id);

        $form = $this->createForm(HomeDestacadoType::class, $homeDestacado);
        $form->handleRequest($request);
       // $slider->setFile(New SliderFile($slider->getFile()));



        if(!$homeDestacado){
            throw $this->createNotFoundException('Home Destacado no encontrado');
        }

        if ($form->isSubmitted() && $form->isValid()) {

            if($homeDestacado->isActivo()){
                $this->cambiarActivoSegunRole($homeDestacado);
            }

            $em->flush();

            $this->addFlash('success', 'El Home Destacado ha sido modificado');

            return $this->redirectToRoute('aper_home_destacado_index');
        }

        return $this->render('IncentiveBundle:HomeDestacado:edit.html.twig', array('slider' => $homeDestacado, 'form' => $form->createView()));
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $slider = $em->getRepository('IncentiveBundle:HomeDestacado')->find($id);
        $em->remove($slider);
        $em->flush();
        $successMessage = 'Home Destacado eliminado';
        $this->addFlash('success', $successMessage);

        return $this->redirectToRoute('aper_home_destacado_index');
    }

    public function indexAction(Request $request)
    {
        //$user = $this->get('security.token_storage')->getToken()->getUser();
        $slider = $this->getDoctrine()->getRepository('IncentiveBundle:HomeDestacado')->findHomeDestacado();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $slider, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('IncentiveBundle:HomeDestacado:index.html.twig', array('pagination' => $pagination, 'slider' => $slider));
    }


    public function showAction($id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('IncentiveBundle:HomeDestacado');

        $slider = $repository->find($id);

        if (!$slider) {
            $messageException = 'Home Destacado no encontrado';
            throw $this->createNotFoundException($messageException);
        }

        return $this->render('IncentiveBundle:HomeDestacado:show.html.twig', ['slider' => $slider]);
    }

    private function cambiarActivoSegunRole(HomeDestacado $homeDestacado){
        $stringDeRole = $homeDestacado->getRoleString();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('IncentiveBundle:HomeDestacado');
        $homeActivos = $repository->findBy(['activo'=>true]);
        
        foreach ($homeActivos as $home){
            if($stringDeRole==$home->getRoleString()){
                $home->setActivo(false);
                $em->persist($home);
                $em->flush();
//                return;
            }
//                return true;
        }
//        return false;
    }

}
