<?php

namespace Aper\IncentiveBundle\Util;

use Aper\StoreBundle\Entity\Store;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\Profile;
use Aper\UserBundle\Entity\User;
use Aper\UserBundle\Model\Role;

class EmployeeManager {

    private $em;

    public function __construct( $entityManager ) {
        $this->em = $entityManager;
    }

    private function comprobarCadena( $cadena ){
        $codigos = array(
//        "'", '"', "(", ")", "DROP", "TABLE", "ALTER", "VALUES", "*",  "%",
            "INSERT", "UPDATE", "SELECT", "DELETE", "`", "[", "]", "{", "}", "\x00", "\x1a",
        );
        return str_replace( $codigos, " ", $cadena );
    }

    public function contarTotalFasesUpdate() {
        return 2;
    }

    /**
     * 
     * @param integer $fase
     */
    public function actualizarRegistros( $fase = 1 ) {

    }

    public function agregarRegistros( $valor ) {

        foreach( $valor as $key => $fila ) 
        {
            $fila_limpia = $this->comprobarCadena( $fila );
            $renglon = explode( ";", $fila_limpia );

            if( count( $renglon ) != 15 ) 
            {
                print_r( $renglon );
                continue;
            }

            $category     = $renglon[0];
            $code         = $renglon[1];
            $name         = $renglon[2];
            $gender       = $renglon[3];
            $birthday     = $renglon[4];
            $startat      = $renglon[5];
            $area         = $renglon[6];
            $departure_at = $renglon[7];
            $agreement    = $renglon[8];
            $store_code   = $renglon[9];
            $location     = $renglon[10];
            $dni          = $renglon[11];
            $rol          = $renglon[12];
            $corpmail     = $renglon[13];
            $office_phone = $renglon[14];
            $province     = $renglon[15];


            $user = new User();
            $user->setUsername($dni);
            $user->setEmail($corpmail);
            $user->setPlainPassword('123456');
            $user->setEnabled(true);
            $user->addRole(Role::convertRole(strtoupper($rol)));

            $employee = new Employee();
            $employee->setCategory($category);
            $employee->setCode($code);
            $employee->setArea($area);
            $employee->setAgreement($agreement);

            $profile = new Profile();
            $profile->setName($name);
            $profile->setSex($gender);
        
            if(isset($corpmail))
                $profile->setCorpMail($corpmail);
        
            if(isset($office_phone))
                $profile->setOfficePhone($office_phone);
        
            if(isset($province))
                $profile->setProvince($province);

            $profile = $this->updateProfileDates($birthday, $startat, $departure_at, $profile);
            $employee->setProfile($profile);
            $user->setEmployee($employee);

            $store = $this->searchStore($store_code, $location);

            $employee->setStore($store);

            $this->em->persist($user);
        }
        $this->em->flush();
    }

    /**
     * @param $row
     *
     * @return Store
     */
    private function searchStore($store_code, $location)
    {
        $em = $this->em;
        $estacion = $em->getRepository('StoreBundle:Store')->findOneByCode($store_code);

        if (!$estacion) {
            $estacion = new Store();
            $estacion->setCode($store_code);
            $estacion->setLocation($location);
            $estacion->setBrand("PUMA-SIN-CLASIFICAR");
            $estacion->setCliente('SIN-CLASIFICAR');
            $estacion->setTipoComercio('EESS');
            $estacion->setSegmento('');
            $estacion->setBandera('PUMA');
            $estacion->setResponsableComercial('SIN-CLASIFICAR');
            $em->persist($estacion);
            $em->flush();
        }

        return $estacion;
    }

    /**
     * @param $birthday 
     * @param $startat
     * @param $departure_at
     * @param $row
     * @param $profile
     * @return mixed
     */
    private function updateProfileDates($birthday, $startat, $departure_at, Profile $profile)
    {
        if (!empty($birthday)) {
            $profile->setBirthDay(\DateTime::createFromFormat('d/m/Y', $birthday)->setTime(0,0,0));
        }
        if (!empty($startat)) {
            $profile->setStartAt(\DateTime::createFromFormat('d/m/Y', $startat)->setTime(0,0,0));
        }
        if (!empty($departure_at)) {
            $profile->setDepartureAt(\DateTime::createFromFormat('d/m/Y', $departure_at)->setTime(0,0,0));
        }
        return $profile;
    }

    /**
     * 
     * @param int $cantidadRegistros
     */
    public function borrarRegistros( $cantidadRegistros = 1000 ) {
        // $db = $this->em->getConnection();
        // $sql = "DELETE FROM tabla LIMIT $cantidadRegistros;";
        // $stmt = $db->prepare( $sql );
        // $stmt->execute();
       die;
    }

    public function contarRegistros() {
        $db = $this->em->getConnection();
        $sql = "SELECT COUNT( * ) AS cantidad FROM users";
        
        $stmt = $db->prepare( $sql );
        $stmt->execute();
        $res = $stmt->fetchAll();
        
        return $res[ 0 ][ "cantidad" ];
    }
}