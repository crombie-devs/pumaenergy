<?php

namespace Aper\IncentiveBundle\Services;

use Aper\StoreBundle\Entity\Store;
use Aper\IncentiveBundle\Entity\EmployeeGoal;
use Aper\IncentiveBundle\Entity\EmployeeImportFile;
use Aper\IncentiveBundle\Entity\EmployeeImportLog;
use Aper\IncentiveBundle\Entity\EmployeeRanking;
use Aper\IncentiveBundle\Entity\GoalType;
use Aper\IncentiveBundle\Entity\ImportLog;
use Aper\IncentiveBundle\Entity\Ranking;
use Aper\IncentiveBundle\Entity\Goal;
use Liuggio\ExcelBundle\Factory;
use WebFactory\Bundle\FileBundle\Util\FileLocator;
use Doctrine\ORM\EntityManager;

class Importer implements ImporterInterface
{
    /**
     * @var Factory
     */
    private $factory;

    /**
     * @var FileLocator
     */
    private $locator;

    /**
     * @var EntityManager
     */
    private $entity;

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(Factory $factory, FileLocator $locator, EntityManager $entity, EntityManager $em)
    {
        $this->factory = $factory;
        $this->locator = $locator;
        $this->entity = $entity;
        $this->em = $em;
    }

    private function newRanking($em, $month, $year, $i, $sheet)
    {
        //NUEVO MES
        $rank = new Ranking();
        $rank->setStore($em);
        $rank->setMonth($month);
        $rank->setYear($year);
        $rank->setType("MONTH");
        $cellRanking = 'B' . $i;
        $rank->setPosition($sheet->getCell($cellRanking)->getValue());
        $this->em->persist($rank);
        $this->em->flush();

        //NUEVO AÑO
        $rank = new Ranking();
//        dump($em);
//        die();
        $rank->setStore($em);
        $rank->setYear($year);
        $rank->setType("YEAR");
        $cellRanking = 'C' . $i;
        $rank->setPosition($sheet->getCell($cellRanking)->getValue());

        $this->em->persist($rank);
        $this->em->flush();

    }

    private function newRankingEmployee($em, $month, $year, $i, $sheet)
    {
        //NUEVO MES
        $rank = new EmployeeRanking();
        $rank->setEmployee($em);
        $rank->setMonth($month);
        $rank->setYear($year);
        $rank->setType("MONTH");
        $cellRanking = 'B' . $i;
        $rank->setPosition($sheet->getCell($cellRanking)->getValue());
        $this->em->persist($rank);
        $this->em->flush();

        //NUEVO AÑO
        $rank = new EmployeeRanking();
//        dump($em);
//        die();
        $rank->setEmployee($em);
        $rank->setYear($year);
        $rank->setType("YEAR");
        $cellRanking = 'C' . $i;
        $rank->setPosition($sheet->getCell($cellRanking)->getValue());

        $this->em->persist($rank);
        $this->em->flush();

    }

    public function import(ImportLog $log)
    {
        ini_set("memory_limit",-1);

        $array = array();

        if (isset($_GET['file'])) {
            $location = $_GET['file'];
        } else {
            $location = $this->locator->getAbsolutePath($log->getFile());
        }

        $excel = $this->factory->createPHPExcelObject($location);
        $sheet = $excel->getActiveSheet();
        $highestRow = $this->getHighestDataRow($sheet);

        $position = 1;
        if (isset($_GET['page'])) {
            $position = $_GET['page'];
        }

        //obtengo mes y año
        $month = $log->getMonth();
        $year = $log->getYear();

        if ($position == 1) {
            //BORRA  RANKINGS MENSUALES
            $this->deleteRankings($month, $year);

            //BORRA  RANKING ANUAL
            $this->deleteRankings(null, $year);
        }

        //INSERTA PUNTAJES
        $fin = $this->insertGoal($highestRow, $sheet, $month, $year, $position);

        if ($fin) {

            //recorre celda de cine
            for ($i = 3; $i <= $highestRow; $i++) {
                $cell = 'A' . $i;
                $store = $sheet->getCell($cell)->getValue();

                //valida si el codigo de cine está correcto
                if ($store != '' && is_numeric($store)) {
                    $em = $this->entity->getRepository('StoreBundle:Store')->findOneByCode($store);

                    if ($em == null) { //no encuentra el cine
                        array_push($array, 'No es un código de sala válido:' . $store);
                    } else {
                        //INSERTA NUEVO RANKING
                        $this->newRanking($em, $month, $year, $i, $sheet);
                    }
                } else { //encuentra espacio en blanco
                    array_push($array, 'Error de código de cine, en la fila del excel Nº:' . $i);
                }
            }
        } else {
            // print_r();exit;
            // print_r($location);exit;
            header('Location: '.$_SERVER['REDIRECT_URL'].'?file=' . $location . '&page=' . ($position+1));
            exit;

        }


        return $array;
    }

    public function importEmployee(EmployeeImportLog $log)
    {
        $array = array();
        $location = $this->locator->getAbsolutePath($log->getFile());
        $excel = $this->factory->createPHPExcelObject($location);
        $sheet = $excel->getActiveSheet();
        $highestRow = $this->getHighestDataRow($sheet);

        //obtengo mes y año

        $month = $log->getMonth();
        $year = $log->getYear();

        //BORRA  RANKINGS MENSUALES
        $this->deleteRankingsEmployee($month, $year);

        //BORRA  RANKING ANUAL
        $this->deleteRankingsEmployee(null, $year);

        //INSERTA PUNTAJES
        $this->insertGoalEmployee($highestRow, $sheet, $month, $year);

        //INSERTA EXERT Y SHOPPER
        // $this->insertExertShopperEmployee($month, $year, $highestRow, $sheet);

        //recorre celda de cine
        for ($i = 3; $i <= $highestRow; $i++) {
            $cell = 'A' . $i;
            $store = $sheet->getCell($cell)->getValue();
            //valida si el codigo de cine está correcto
            if ($store != '' && is_numeric($store)) {
                $em = $this->entity->getRepository('UserBundle:Employee')->findOneByCode($store);

                if ($em == null) { //no encuentra el cine
                    array_push($array, 'No es un código de empleado válido:' . $store);
                } else {
                    //INSERTA NUEVO RANKING
                    $this->newRankingEmployee($em, $month, $year, $i, $sheet);
                }
            } else { //encuentra espacio en blanco
                array_push($array, 'Error de código de empleado, en la fila del excel Nº:' . $i);
            }
        }

        return $array;
    }

    private function addGoalTypes2018($cellType, $category, $sheet)
    {
        if ($cellType != "MISTERY" && $cellType != "EXERT") {
            $name = $sheet->getCell($cellType)->getValue();

        }
        switch ($cellType) {
            // PUNTAJE OBJETIVO
            case "AR2":
            case "BJ2":
            case "BX2":
            case "DR2":
                $name = "TOTAL_OBJETIVO";
                break;

            // PUNTAJE RESULTADO
            case "AS2":
            case "BK2":
            case "BY2":
            case "DS2":
                $name = "TOTAL_RESULTADO";
                break;

            // PUNTAJE TOTAL OBJETIVO
            case "DT2":
                $name = "TOTAL_STORE_OBJETIVO";
                break;

            // PUNTAJE TOTAL RESULTADO
            case "DU2":
                $name = "TOTAL_STORE_RESULTADO";
                break;

            case "MISTERY":
                $name = "MISTERY";
                break;

            case "EXERT":
                $name = "EXERT";
                break;
        }

        $goalType = New GoalType();
        $goalType->setName($name);
        $goalType->setCategory($category);
        $goalType->setCell($cellType);

        $this->em->persist($goalType);
        $this->em->flush();
    }

    private function insertGoal($highestRow, $sheet, $month, $year, $position)
    {
        //BORRA GOALS DEL MES/AÑO
        if ($position == 1) {
            $this->deleteGoals($month, $year);
        }
        // var_dump('asdasdasd');exit;

        $array = array();
        $cellObjetive = null;
        $cellObjetiveMet = null;
        $cellResultMet = null;
        $cellResult = null;
        $symbolf = null;
        $symbolb = null;
        $mshr = null;
        $msho = null;
        $mshrm = null;
        $mshom = null;

        $repo = $this->entity->getRepository('IncentiveBundle:GoalType');

        //BUSCAR MES Y AÑO DE ULTIMA DEFINICION DE TIPOS DE OBJETIVOS
        $latestTypeOfGoals = $repo->findLastObjetivesBeforeMonthAndYear($month, $year);
        // print_r($latestTypeOfGoals[0]->getYear());exit;

        //BUSCAR TIPOS DE OBJETIVOS DE MES Y AÑO
        $listGT = $repo->findByMonthAndYearSpecific($latestTypeOfGoals[0]->getYear(), $latestTypeOfGoals[0]->getMonth());

        $from = 50 * ($position-1);
        $listGT = array_slice($listGT, $from, 50);

        /** @var GoalType $gt_aux */


        $total = 0;
        foreach ($listGT as $gt_aux) {
            $total++;

            // if ($total != $position) continue;

            $cellType = $gt_aux->getCell();
            $goalType = $gt_aux;

            if ($goalType->getCategory() == 'MYSTERY SHOPPER') continue;

            // var_dump($cellType);exit;

            // $uno = 0;
            // if ($goalType->getCell() != 'AJ2' && $uno) {
            //     continue;
            // }
            // if ($goalType->getCell() == 'AJ2') {
            //     $uno = 1;
            //     continue;
            // }
            // print_r($goalType->getCell());exit;

            /*
            if ('EXERT' === $goalType->getCell() || 'MISTERY' === $goalType->getCell()) {
                for ($i = 3; $i <= $highestRow; $i++) {
                    $store = $sheet->getCell('A'.$i)->getValue();

                    //valida si el codigo de cine está correcto
                    if ($store != '' && is_numeric($store)) {
                        $em = $this->entity->getRepository('StoreBundle:Store')->findOneByCode($store);

                        if ($em == null) { //no encuentra el cine
                            array_push($array, 'No es un código de sala válido:' . $store);
                        } else {
                            //INSERTA GOALS
                            $this->addGoal($em, $month, $year, $sheet, 'A2',
                                'A2', 'A2', 'A2',
                                $gt_aux->getFrontSymbol(), $gt_aux->getBackSymbol(), $goalType);
                        }
                    }
                }
            } else {
            */

            // if (true) {
                $colIndex = $this->columnIndexFromString($cellType);
                $colAlpha = $this->columnStringFromIndex($colIndex);

                if ($gt_aux->isTotalizer()) {
                    $colObjetiveMetAlpha = $colAlpha;
                    $colResultMetAlpha = $colAlpha;
                    $colResultAlpha = $colAlpha;
                } else {
                    $colObjetiveMetAlpha = $this->columnStringFromIndex($colIndex + 1);
                    $colResultMetAlpha = $this->columnStringFromIndex($colIndex + 2);
                    $colResultAlpha = $this->columnStringFromIndex($colIndex + 3);
                }

                $sqlgoals = [];
                for ($i = 3; $i <= $highestRow; $i++) {
                    // var_dump($goalType->getCategory());
                    // if ($goalType->getCategory() != 'MYSTERY SHOPPER') {
                        // print_r($goalType->getCategory());exit;
                        $cellObjetive = $colAlpha . $i;
                        $cellObjetiveMet = $colObjetiveMetAlpha . $i;
                        $cellResultMet = $colResultMetAlpha . $i;
                        $cellResult = $colResultAlpha . $i;

                        // var_dump($goalType->getCategory());exit;
                        // print_r($cellResult);exit;

                        $cell = 'A' . $i;
                        $store = $sheet->getCell($cell)->getValue();

                        //valida si el codigo de cine está correcto
                        if ($store != '' && is_numeric($store)) {
                            $em = $this->entity->getRepository('StoreBundle:Store')->findOneByCode($store);

                            if ($em == null) { //no encuentra el cine
                                array_push($array, 'No es un código de estacion válido:' . $store);
                            } else {

                                $sqlgoals[] = $this->addGoal($em, $month, $year, $sheet, $cellObjetive,
                                    $cellObjetiveMet, $cellResultMet, $cellResult,
                                    $gt_aux->getFrontSymbol(), $gt_aux->getBackSymbol(), $goalType);
                            }
                        }
                    // }
                }

                $conn = $this->em->getConnection();
                $sql = 'INSERT INTO `goal` (`store_id`, `month`, `year`, `type`, `objetive`, `objetive_met`, `result`, `result_met`, `symbolf`, `symbolb`, `goaltype_id`) VALUES ';

                $sql .= implode(',', $sqlgoals);
                $sql .= ';';

                // print_r($sqlgoals);
                // print_r($sql);exit;


                $stmt = $conn->prepare($sql);
                $stmt->execute();

                // if ($uno) {
                    // $text = $sql . "\n";
                    // $text = $goalType->getCell() . "----------------------------------------------------- " . "\n";
            
                    // file_put_contents("_method.log", $text, FILE_APPEND);

                // }

            // }
        }

        if (count($listGT) < 50) {
            return true;
        }
        return false;
    }


    private function insertGoalEmployee($highestRow, $sheet, $month, $year)
    {
        //BORRA GOALS DEL MES/AÑO
        $this->deleteGoalsEmployee($month, $year);
        $array = array();
        $cellObjetive = null;
        $cellObjetiveMet = null;
        $cellResultMet = null;
        $cellResult = null;
        $symbolf = null;
        $symbolb = null;
        $mshr = null;
        $msho = null;
        $mshrm = null;
        $mshom = null;

        $repo = $this->entity->getRepository('IncentiveBundle:EmployeeGoalType');

        //BUSCAR MES Y AÑO DE ULTIMA DEFINICION DE TIPOS DE OBJETIVOS
        $latestTypeOfGoals = $repo->findLastObjetivesBeforeMonthAndYear($month, $year);

        //BUSCAR TIPOS DE OBJETIVOS DE MES Y AÑO
        $listGT = $repo->findByMonthAndYear($latestTypeOfGoals[0]->getYear(), $latestTypeOfGoals[0]->getMonth());

        /** @var EmployeeGoalType $gt_aux */
        foreach ($listGT as $gt_aux) {
            $cellType = $gt_aux->getCell();
            $goalType = $gt_aux;

            if ('EXERT' === $goalType->getCell() || 'MISTERY' === $goalType->getCell()) {
                for ($i = 3; $i <= $highestRow; $i++) {
                    $store = $sheet->getCell('A'.$i)->getValue();

                    //valida si el codigo de cine está correcto
                    if ($store != '' && is_numeric($store)) {
                        $em = $this->entity->getRepository('UserBundle:Employee')->findOneByCode($store);

                        if ($em == null) { //no encuentra el cine
                            array_push($array, 'No es un código de empleado válido:' . $store);
                        } else {
                            //INSERTA GOALS
                            $this->addGoalEmployee($em, $month, $year, $sheet, 'A2',
                                'A2', 'A2', 'A2',
                                $gt_aux->getFrontSymbol(), $gt_aux->getBackSymbol(), $goalType);
                        }
                    }
                }
            } else {
                $colIndex = $this->columnIndexFromString($cellType);
                $colAlpha = $this->columnStringFromIndex($colIndex);

                if ($gt_aux->isTotalizer()) {
                    $colObjetiveMetAlpha = $colAlpha;
                    $colResultMetAlpha = $colAlpha;
                    $colResultAlpha = $colAlpha;
                } else {
                    $colObjetiveMetAlpha = $this->columnStringFromIndex($colIndex + 1);
                    $colResultMetAlpha = $this->columnStringFromIndex($colIndex + 2);
                    $colResultAlpha = $this->columnStringFromIndex($colIndex + 3);
                }

                $sqlgoals = [];
                for ($i = 3; $i <= $highestRow; $i++) {
                    $cellObjetive = $colAlpha . $i;
                    $cellObjetiveMet = $colObjetiveMetAlpha . $i;
                    $cellResultMet = $colResultMetAlpha . $i;
                    $cellResult = $colResultAlpha . $i;

                    $cell = 'A' . $i;
                    $store = $sheet->getCell($cell)->getValue();


                    //valida si el codigo de cine está correcto
                    if ($store != '' && is_numeric($store)) {
                        $em = $this->entity->getRepository('UserBundle:Employee')->findOneByCode($store);

                        if ($em == null) { //no encuentra el cine
                            array_push($array, 'No es un código de estacion válido:' . $store);
                        } else {
                            //INSERTA GOALS
                            $sqlgoals[] = $this->addGoalEmployee($em, $month, $year, $sheet, $cellObjetive,
                                $cellObjetiveMet, $cellResultMet, $cellResult,
                                $gt_aux->getFrontSymbol(), $gt_aux->getBackSymbol(), $goalType);
                        }
                    }
                }

                $conn = $this->em->getConnection();
                $sql = 'INSERT INTO `employee_goal` (`employee_id`, `month`, `year`, `type`, `objetive`, `objetive_met`, `result`, `result_met`, `symbolf`, `symbolb`, `employeeGoaltype_id`) VALUES ';

                $sql .= implode(',', $sqlgoals);
                $sql .= ';';

                // print_r($sql);exit;

                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }
        }
    }

    /**
     * @param Store $em
     * @param integer $month
     * @param integer $year
     * @param \PHPExcel_IComparable $sheet
     * @param string $cellObjetive
     * @param string $cellObjetiveMet
     * @param string $cellResultMet
     * @param string $cellResult
     * @param string $symbolf
     * @param string $symbolb
     * @param GoalType $goalType
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function addGoal($em, $month, $year, $sheet, $cellObjetive,
                             $cellObjetiveMet, $cellResultMet, $cellResult, $symbolf, $symbolb, $goalType)
    {
        if ($cellResult) {
            $ob = 0;
            $obm = 0;
            $re = 0;
            $rem = 0;

            if (!('EXERT' === $goalType->getCell() || 'MISTERY' === $goalType->getCell())) {
                $ob = $sheet->getCell($cellObjetive)->getValue();
                $obm = $sheet->getCell($cellObjetiveMet)->getValue();
                $re = $sheet->getCell($cellResult)->getValue();
                $rem = $sheet->getCell($cellResultMet)->getValue();

                // Verifico que se haya entrado una fila válida
                // if (null === $ob || null === $obm || null === $re || null === $rem) {
                //     return;
                // }

                if ($goalType->isTotalizer()) {
                    $obm = 0;
                    $rem = 0;

                    if ('TOTAL_STORE_RESULTADO' === $goalType->getCategory()) {
                        $ob = 0;
                    }
                }
            }
// echo 123123;exit;

            $goal = new Goal();
            $goal->setStore($em);
            $goal->setMonth($month);
            $goal->setYear($year);

            $goal->setObjetive($ob);
            $goal->setObjetiveMet($obm);
            $goal->setResult($re);
            $goal->setResultMet($rem);

            $goal->setType('MONTH');
            $goal->setSymbolf($symbolf);
            $goal->setSymbolb($symbolb);
            $goal->setGoalType($goalType);
            $storeID = $em->getId();
            $goalID = $goalType->getId();
            return "($storeID, '$month', '$year', 'MONTH', '$ob', '$obm', '$re', '$rem', '$symbolf', '$symbolb', '{$goalID}')";

            // $this->em->persist($goal);
            // $this->em->flush();
        }
    }


    /**
     * @param Employee $em
     * @param integer $month
     * @param integer $year
     * @param \PHPExcel_IComparable $sheet
     * @param string $cellObjetive
     * @param string $cellObjetiveMet
     * @param string $cellResultMet
     * @param string $cellResult
     * @param string $symbolf
     * @param string $symbolb
     * @param GoalType $goalType
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function addGoalEmployee($em, $month, $year, $sheet, $cellObjetive,
                             $cellObjetiveMet, $cellResultMet, $cellResult, $symbolf, $symbolb, $goalType)
    {
        if ($cellResult) {
            $ob = 0;
            $obm = 0;
            $re = 0;
            $rem = 0;

            if (!('EXERT' === $goalType->getCell() || 'MISTERY' === $goalType->getCell())) {
                $ob = $sheet->getCell($cellObjetive)->getValue();
                $obm = $sheet->getCell($cellObjetiveMet)->getValue();
                $re = $sheet->getCell($cellResult)->getValue();
                $rem = $sheet->getCell($cellResultMet)->getValue();

                // Verifico que se haya entrado una fila válida
                if (null === $ob || null === $obm || null === $re || null === $rem) {
                    return;
                }

                if ($goalType->isTotalizer()) {
                    $obm = 0;
                    $rem = 0;

                    if ('TOTAL_EMPLEADO_RESULTADO' === $goalType->getCategory()) {
                        $ob = 0;
                    }
                }
            }

            $goal = new EmployeeGoal();
            $goal->setEmployee($em);
            $goal->setMonth($month);
            $goal->setYear($year);

            $goal->setObjetive($ob);
            $goal->setObjetiveMet($obm);
            $goal->setResult($re);
            $goal->setResultMet($rem);

            $goal->setType('MONTH');
            $goal->setSymbolf($symbolf);
            $goal->setSymbolb($symbolb);
            $goal->setEmployeeGoalType($goalType);

            $employee_id = $em->getId();
            $goalID = $goalType->getId();


            return "($employee_id, '$month', '$year', 'MONTH', '$ob', '$obm', '$re', '$rem', '$symbolf', '$symbolb', '{$goalID}')";

            // $this->em->persist($goal);
            // $this->em->flush();
        }
    }

    /**
     * @param Store $em
     * @param integer $month
     * @param integer $year
     * @param \PHPExcel_IComparable $sheet
     * @param string $cellObjetive
     * @param string $cellObjetiveMet
     * @param string $cellResultMet
     * @param string $cellResult
     * @param string $symbolf
     * @param string $symbolb
     * @param GoalType $goalType
     * @throws \Doctrine\ORM\OptimisticLockException
     */

    private function deleteRankings($month, $year)
    {

        $conn = $this->em->getConnection();
        if ($month) {
            $sql = "DELETE FROM `ranking` WHERE month={$month} and year={$year};";
        } else {
            $sql = "DELETE FROM `ranking` WHERE month=NULL and year={$year};";
        }
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // $ranking = $this->entity->getRepository('IncentiveBundle:Ranking')->findOnesBy3Y($month, $year);
        // foreach ($ranking as $rank) {
        //     $this->entity->remove($rank);
        //     $this->entity->flush();
        // }
    }

    private function deleteRankingsEmployee($month, $year)
    {
        $conn = $this->em->getConnection();
        if ($month) {
            $sql = "DELETE FROM `employee_ranking` WHERE month={$month} and year={$year};";
        } else {
            $sql = "DELETE FROM `employee_ranking` WHERE month=NULL and year={$year};";
        }
        $stmt = $conn->prepare($sql);
        $stmt->execute();


        // $ranking = $this->entity->getRepository('IncentiveBundle:EmployeeRanking')->findOnesBy3Y($month, $year);

        // foreach ($ranking as $rank) {
        //     $this->entity->remove($rank);
        //     $this->entity->flush();
        // }
    }

    private function deleteGoals($month, $year)
    {
        $conn = $this->em->getConnection();
        $sql = "DELETE FROM `goal` WHERE month={$month} and year={$year};";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // $goals = $this->entity->getRepository('IncentiveBundle:Goal')->findGoalsByMonth($month, $year);

        // foreach ($goals as $goal) {
        //     $this->entity->remove($goal);
        //     $this->entity->flush();
        // }
    }

    private function deleteGoalsEmployee($month, $year)
    {
        $conn = $this->em->getConnection();
        $sql = "DELETE FROM `employee_goal` WHERE month={$month} and year={$year};";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // $goals = $this->entity->getRepository('IncentiveBundle:EmployeeGoal')->findGoalsByMonth($month, $year);

        // foreach ($goals as $goal) {
        //     $this->entity->remove($goal);
        //     $this->entity->flush();
        // }
    }

    private function insertExertShopper($month, $year, $highestRow, $sheet)
    {
        $array = [];
        for ($i = 3; $i <= $highestRow; $i++) {
            $mso = 0;
            $msr = 0;
            $msom = 0;
            $msrm = 0;
            $exo = 0;
            $exr = 0;
            $exom = 0;
            $exrm = 0;

            $cell = 'A' . $i;
            $store = $sheet->getCell($cell)->getValue();

            //valida si el codigo de cine está correcto
            if ($store != '' && is_numeric($store)) {
                $em = $this->entity->getRepository('StoreBundle:Store')->findOneByCode($store);

                if ($em == null) { //no encuentra el cine
                    array_push($array, 'No es un código de sala válido:' . $store);
                } else {
                    $goals = $this->entity->getRepository('IncentiveBundle:Goal')->findGoalsByMonth($month, $year);
                    foreach ($goals as $goal) {
                        if ($goal->getGoalType()->isMysteryShopper()) {
                            if ($goal->getStore() == $em) {
                                $mso = $mso + $goal->getObjetive();
                                $msr = $msr + $goal->getResult();
                                $msom = $msom + $goal->getObjetiveMet();
                                $msrm = $msrm + $goal->getResultMet();

                            }
                        }

                        if ($goal->getGoalType()->isExert()) {
                            if ($goal->getStore() == $em) {
                                $exo = $exo + $goal->getObjetive();
                                $exr = $exr + $goal->getResult();
                                $exom = $exom + $goal->getObjetiveMet();
                                $exrm = $exrm + $goal->getResultMet();
                            }
                        }
                    }

                    //ACTUALIZA  EXERT
                    $exert = $this->entity->getRepository('IncentiveBundle:Goal')->findGoalsExertByMonth($month, $year, $em);

                    if ($exert) {
                        $exert->setObjetive($exo);
                        $exert->setObjetiveMet($exom);
                        $exert->setResult($exr);
                        $exert->setResultMet($exrm);
                        $this->em->persist($exert);
                        $this->em->flush();
                    }

                    //ACTUALIZA  MISTERY
                    $mis = $this->entity->getRepository('IncentiveBundle:Goal')->findGoalsMisteryByMonth($month, $year, $em);

                    if ($mis) {
                        $mis->setObjetive($mso);
                        $mis->setObjetiveMet($msom);
                        $mis->setResult($msr);
                        $mis->setResultMet($msrm);
                        $this->em->persist($mis);
                        $this->em->flush();
                    }
                }
            }
        }
    }


    private function insertExertShopperEmployee($month, $year, $highestRow, $sheet)
    {
        for ($i = 3; $i <= $highestRow; $i++) {
            $mso = 0;
            $msr = 0;
            $msom = 0;
            $msrm = 0;
            $exo = 0;
            $exr = 0;
            $exom = 0;
            $exrm = 0;

            $cell = 'A' . $i;
            $store = $sheet->getCell($cell)->getValue();

            //valida si el codigo de cine está correcto
            if ($store != '' && is_numeric($store)) {
                $em = $this->entity->getRepository('UserBundle:Employee')->findOneByCode($store);

                if ($em == null) { //no encuentra el cine
                    array_push($array, 'No es un código de sala válido:' . $store);
                } else {
                    $goals = $this->entity->getRepository('IncentiveBundle:EmployeeGoal')->findGoalsByMonth($month, $year);
                    foreach ($goals as $goal) {
                        if ($goal->getEmployeeGoalType()->isMysteryShopper()) {
                            if ($goal->getEmployee() == $em) {
                                $mso = $mso + $goal->getObjetive();
                                $msr = $msr + $goal->getResult();
                                $msom = $msom + $goal->getObjetiveMet();
                                $msrm = $msrm + $goal->getResultMet();

                            }
                        }

                        if ($goal->getEmployeeGoalType()->isExert()) {
                            if ($goal->getEmployee() == $em) {
                                $exo = $exo + $goal->getObjetive();
                                $exr = $exr + $goal->getResult();
                                $exom = $exom + $goal->getObjetiveMet();
                                $exrm = $exrm + $goal->getResultMet();
                            }
                        }
                    }

                    //ACTUALIZA  EXERT
                    $exert = $this->entity->getRepository('IncentiveBundle:EmployeeGoal')->findGoalsExertByMonth($month, $year, $em);

                    if ($exert) {
                        $exert->setObjetive($exo);
                        $exert->setObjetiveMet($exom);
                        $exert->setResult($exr);
                        $exert->setResultMet($exrm);
                        $this->em->persist($exert);
                        $this->em->flush();
                    }

                    //ACTUALIZA  MISTERY
                    $mis = $this->entity->getRepository('IncentiveBundle:EmployeeGoal')->findGoalsMisteryByMonth($month, $year, $em);

                    if ($mis) {
                        $mis->setObjetive($mso);
                        $mis->setObjetiveMet($msom);
                        $mis->setResult($msr);
                        $mis->setResultMet($msrm);
                        $this->em->persist($mis);
                        $this->em->flush();
                    }
                }
            }
        }
    }

    private function getHighestDataRow($sheet)
    {
        for ($i = 2, $high = $sheet->getHighestDataRow(); $i <= $high; $i++) {
            // Si el valor de la celda A# es null se toma como una fila vacía
            if (null === $sheet->getCell('A'.$i)->getValue()) {
                return $i - 1;
            }
        }

        return $i;
    }

    private function columnIndexFromString($cell) {
        $cell = preg_replace('/[^a-zA-Z]/', '', $cell);

        $cell = strtoupper($cell);
        $length = strlen($cell);
        $number = 0;
        $level = 1;
        while($length >= $level) {
            $char = $cell[$length - $level];
            $c = ord($char) - 64;
            $number += $c * pow(26, ($level - 1));
            $level++;
        }

        return $number;
    }

    private function columnStringFromIndex($number) {
        $number = intval($number);
        if ($number <= 0) {
            return '';
        }

        $alphabet = '';
        while($number != 0) {
            $p = ($number - 1) % 26;
            $number = intval(($number - $p) / 26);
            $alphabet = chr(65 + $p) . $alphabet;
        }

        return $alphabet;
    }
}
