<?php

namespace Aper\IncentiveBundle\Services;

use Aper\IncentiveBundle\Entity\ImportLog;

/**
 * Interface ImporterInterface
 * @package Aper\IncentiveBundle\Service
 */
interface ImporterInterface
{
    /**
     * @param ImportLog $log
     * @return string[] Listado de errores, vacío si fue exitoso
     */
    public function import(ImportLog $log);
}