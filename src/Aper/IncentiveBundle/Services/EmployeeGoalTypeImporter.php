<?php


namespace Aper\IncentiveBundle\Services;

use Aper\IncentiveBundle\Entity\EmployeeGoalType;
use Aper\IncentiveBundle\Entity\GoalType;
use Aper\IncentiveBundle\Form\DTO\ImportDTO;
use Aper\IncentiveBundle\Repository\EmployeeGoalTypeRepository;
use Aper\IncentiveBundle\Repository\GoalTypeRepository;
use Doctrine\ORM\EntityManager;
use Liuggio\ExcelBundle\Factory;

class EmployeeGoalTypeImporter
{
    /**
     * @var Factory
     */
    private  $factory;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EmployeeGoalTypeRepository
     */
    private $repository;

    /**
     * GoalTypeImporter constructor.
     * @param Factory $factory
     * @param EntityManager $em
     */
    public function __construct(Factory $factory, EntityManager $em)
    {
        $this->factory = $factory;
        $this->em = $em;
        $this->repository = $this->em->getRepository('IncentiveBundle:EmployeeGoalType');
    }

    /**
     * @param ImportDTO $import
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \PHPExcel_Exception
     */
    public function import(ImportDTO $import)
    {
        $repository = $this->repository;
        $goalTypes = $repository->findByMonthAndYear($import->year, $import->month);

        uasort($goalTypes, function (EmployeeGoalType $a, EmployeeGoalType $b) {
            return $a->getParent() ? -1 : 1;
        });

        foreach ($goalTypes as $goalType) {
            $this->em->remove($goalType);
        }
        $this->em->flush();

        $headers = array_combine([
                'name',
                'category',
                'cell',
                'parent',
                'frontSymbol',
                'backSymbol',
                'isSpecial',
                'isTotalizer',
            ],
            range('A', 'H')
        );

        $excel = $this->factory->createPHPExcelObject($import->file->getFile()->getPathname());
        $sheet = $excel->getActiveSheet();
        $highestRow = $this->getHighestDataRow($sheet);
        $allErrors = [];
        $cells = [];
        $parents = [];

        for ($i = 2; $i < $highestRow; $i++) {
            $errors = [];
            $name = trim($sheet->getCell($headers['name'] . $i)->getValue());
            if ('' === $name) {
                $errors[] = sprintf('La celda %s%s no puede estar en blanco', $headers['name'], $i);
            }

            $category = trim($sheet->getCell($headers['category'] . $i)->getValue());
            if ('' === $category) {
                $errors[] = sprintf('La celda %s%s no puede estar en blanco', $headers['category'], $i);
            }

            $cell = trim(strtoupper($sheet->getCell($headers['cell'] . $i)->getValue()));;
            if ('' === $cell) {
                $errors[] = sprintf('La celda %s%s no puede estar en blanco', $headers['cell'], $i);
            } else {

                // if ((!preg_match('/\w+/', $cell) || !preg_match('/\d+/', $cell) || preg_match('/[^A-Z]+[^0-9]+/', $cell)) && !in_array($cell, ['EXERT', 'MISTERY'])
                if (!preg_match('/\w+/', $cell) || !preg_match('/\d+/', $cell) || preg_match('/[^A-Z]+[^0-9]+/', $cell)) {
                    $errors[] = sprintf('La celda %s%s posee el valor incorrecto "%s"', $headers['cell'], $i, $cell);
                } else if (in_array($cell, array_keys($cells))) {
                    $errors[] = sprintf('La celda %s%s está duplicada. Ver celda %s', $headers['cell'], $i, $cells[$cell]);
                } else {
                    $cells[$cell] = $headers['cell'].$i;
                }
            }

            $goalType = new EmployeeGoalType();
            $goalType->setYear($import->year);
            $goalType->setMonth($import->month);
            $goalType->setName($name);
            $goalType->setCategory($category);
            $goalType->setCell($cell);
            $goalType->setBackSymbol($sheet->getCell($headers['backSymbol'] . $i)->getValue());
            $goalType->setFrontSymbol($sheet->getCell($headers['frontSymbol'] . $i)->getValue());
            $goalType->setIsSpecial(('si' === trim(strtolower($sheet->getCell($headers['isSpecial'] . $i)->getValue())) ||
                '1' === trim(strtolower($sheet->getCell($headers['isSpecial'] . $i)->getValue()))) ? true : false);
            $goalType->setisTotalizer(('si' === trim(strtolower($sheet->getCell($headers['isTotalizer'] . $i)->getValue())) ||
                '1' === trim(strtolower($sheet->getCell($headers['isTotalizer'] . $i)->getValue()))) ? true : false);
            $parent = trim(strtolower($sheet->getCell($headers['parent'] . $i)->getValue()));
            // Si en la columna 'parent' se encuentra un valor que empieza con 'p-algo' significa que la fila que tenga 'algo' será su hija
            // Se usa para EXERT y MISTERY SHOOP
            if ($parent) {
                if (preg_match('/p-*/', $parent)) {
                    if (in_array($parent, array_keys($parents))) {
                        $errors[] = sprintf('La celda %s%s tiene un valor duplicado "%s"', $headers['cell'], $i, $parent);
                    } else {
                        $parents[$parent] = $goalType;
                    }
                } else {
                    if (!isset($parents['p-'.$parent])) {
                        $errors[] = sprintf('La celda %s%s hace referencia al padre p-%s. Este valor debe estar definido previamente', $headers['cell'], $i, $parent);
                    } else {
                        $goalType->setParent($parents['p-'.$parent]);
                    }
                }
            }

            if(empty($errors)) {
                $this->em->persist($goalType);

            } else {
                $allErrors = array_merge($allErrors, $errors);
            }
        }

        if (empty($allErrors)) {
            $this->em->flush();
        }

        return $allErrors;
    }

    /**
     * @param $sheet
     * @return int
     */
    private function getHighestDataRow($sheet)
    {
        for ($i = 2, $high = $sheet->getHighestDataRow(); $i <= $high; $i++) {
            // Si el valor de la celda A# es null se toma como una fila vacía
            if (
                null === $sheet->getCell('A'.$i)->getValue()
                && null === $sheet->getCell('B'.$i)->getValue()
                && null === $sheet->getCell('C'.$i)->getValue()

            ) {
                return $i - 1;
            }
        }

        return $i;
    }
}