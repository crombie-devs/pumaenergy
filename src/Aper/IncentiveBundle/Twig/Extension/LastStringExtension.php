<?php

namespace Aper\IncentiveBundle\Twig\Extension;

class LastStringExtension extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('slug_me', array($this, 'getLastString')),
        );
    }

    public function getLastString($url)
    {
        return substr($url, strrpos($url, '/') + 1);
    }

    public function getName()
    {
        return 'last_string_extension';
    }
}