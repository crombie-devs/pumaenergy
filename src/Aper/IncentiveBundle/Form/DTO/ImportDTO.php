<?php

namespace Aper\IncentiveBundle\Form\DTO;

use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\FileBundle\Model\File;


class ImportDTO
{
    /**
     * @var integer
     */
    public $month;

    /**
     * @var integer
     */
    public $year;

    /**
     *
     * @var File
     * @Assert\Valid()
     */
    public $file;
}

