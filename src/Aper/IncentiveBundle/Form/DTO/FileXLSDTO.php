<?php

namespace Aper\IncentiveBundle\Form\DTO;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\FileBundle\Model\File;
use Symfony\Component\Validator\Constraints\Callback;

/**
 * @Callback(methods={"validate"})
 *
 */
class FileXLSDTO extends File
{
    /**
     * @var UploadedFile
     * @Assert\File(
     *     mimeTypes = {
     *         "application/zip",
     *         "application/vnd.ms-excel",
     *         "application/msexcel",
     *         "application/x-msexcel",
     *         "application/x-ms-excel",
     *         "application/x-excel",
     *         "application/x-dos_ms_excel",
     *         "application/xls",
     *         "application/x-xls",
     *         "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
     *         "application/vnd.oasis.opendocument.spreadsheet"
     *     },
     *     mimeTypesMessage = "Debe ser un XLS válido"
     * )
     */
     protected $file;

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        $this->ensureRequiredFile($context);
    }
}
