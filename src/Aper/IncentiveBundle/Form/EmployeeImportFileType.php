<?php

namespace Aper\IncentiveBundle\Form;

use Aper\IncentiveBundle\Entity\EmployeeImportFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WebFactory\Bundle\FileBundle\Form\Type\FilePreviewType;

class EmployeeImportFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FilePreviewType::class, [
                'show_legend' => false,
                'show_child_legend' => false,
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => EmployeeImportFile::class,
            ]);
    }

    public function getBlockPrefix()
    {
        return 'avatar';
    }
}