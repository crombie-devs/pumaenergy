<?php

namespace Aper\IncentiveBundle\Form;


use Aper\IncentiveBundle\Entity\ImportAuditoryLog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;    
use Symfony\Component\Form\FormBuilderInterface;

class ImportAuditoryLogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $lastYear=date("Y",strtotime("-1 year"));
        $currentYear=date("Y",time());
        $builder
        ->add('year', ChoiceType::class,
            array(
                'choices' => array(
                    $lastYear => $lastYear,
                    $currentYear => $currentYear
                ),
                'placeholder' => 'Seleccione un año'
            )
        )
        ->add('type', ChoiceType::class,
            array(
                'choices' => array(
                    "AUSENTISM" => "Ausentismo"
                ),
                'placeholder' => 'Seleccione un año'
            )
        )
        ->add('file', FileType::class, ['label' => "Seleccione el archivo"])
        ->add('save', SubmitType::class);
    }
}
