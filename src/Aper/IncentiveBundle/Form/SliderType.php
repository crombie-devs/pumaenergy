<?php

namespace Aper\IncentiveBundle\Form;


use Aper\IncentiveBundle\Entity\Slider;
use Aper\UserBundle\Model\Role;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SliderType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $roles = Role::getChoices();
        unset($roles['ROLE_ADMIN']);
        unset($roles['ROLE_STORE_MANAGER']);
        $builder
             ->add('file', SliderFileType::class, ['label' => false])
             ->add('url', TextType::class, ['required' => false])
             ->add('sort', NumberType::class)
             ->add('categories',ChoiceType::class, [
                 'choices' => $roles,
                 'multiple' => true,
                 'expanded' => true,
                 'omit_collection_item' => 'ROLE_ADMIN'
             ])
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Slider::class,
                'attr' => ['novalidate' => 'novalidate'],
            ]);
    }

    public function getBlockPrefix()
    {
        return 'slider';
    }
}
