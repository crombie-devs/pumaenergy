<?php

namespace Aper\IncentiveBundle\Form;

use Aper\IncentiveBundle\Form\DTO\ImportDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EmployeeImportGoalTypType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $months = range(1, 12);
        $years = range(2020, 2035);

        $builder
            ->add('month', ChoiceType::class, [
                'choices' => array_combine($months, $months),
                'placeholder' => 'Seleccione un mes']
            )
            ->add('year', ChoiceType::class, [
                'choices' => array_combine($years, $years),
                'placeholder' => 'Seleccione un año'
            ])
            ->add('file', EmployeeImportGoalFileType::class, [
                'label' => false,
            ])
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => ImportDTO::class,
            ]);
    }


    public function getBlockPrefix()
    {
        return 'import_goal_types';
    }
}
