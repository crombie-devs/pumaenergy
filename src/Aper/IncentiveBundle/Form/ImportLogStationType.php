<?php

namespace Aper\IncentiveBundle\Form;

use Aper\IncentiveBundle\Entity\ImportLog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class ImportLogStationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder


            ->add('month', ChoiceType::class, array('choices' => array(
                '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5',
                '6' => '6','7' => '7','8' => '8','9' => '9','10' => '10',
                '11' => '11','12' => '12' ), 'placeholder' => 'Seleccione un mes'))
            ->add('year', ChoiceType::class, array('choices' => array(
                '2020' => '2020','2021' => '2021','2022' => '2023',
                '2024' => '2024','2025' => '2025','2026' => '2026',
                '2027' => '2027','2028' => '2028','2029' => '2029',
                '2030' => '2030','2031' => '2031','2032' => '2032',
                '2033' => '2033','2034' => '2034','2035' => '2035'), 'placeholder' => 'Seleccione un año'))
            ->add('file', ImportFileType::class, ['label' => false])
            ->add('message', TextType::class, ['label' => "Mensaje", 'required'   => false])
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => ImportLog::class,
            ]);
    }

    public function getBlockPrefix()
    {
        return 'importlog';
    }
}
