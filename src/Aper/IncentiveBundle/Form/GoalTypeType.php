<?php

namespace Aper\IncentiveBundle\Form;

use Aper\IncentiveBundle\Entity\GoalType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GoalTypeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $months = range(1, 12);
        $years = range(date('Y'), 2020);
        $parentTypes = [
            GoalType::EXERT,
            GoalType::MYSTERY_SHOPPER,
        ];

        $builder
            ->add('name')
            ->add('category')
            ->add('cell')
            ->add('month', ChoiceType::class, [
                'choices' => array_combine($months, $months),
            ])
            ->add('year', ChoiceType::class, [
                'choices' => array_combine($years, $years),
            ])
            // ->add('frontSymbol', null, [
            //     'required' => false,
            // ])
            // ->add('backSymbol', null, [
            //     'required' => false,
            // ])
            ->add('isSpecial')
            ->add('isTotalizer')
            // ->add('parentType', ChoiceType::class, [
            //     'label' => 'Parent',
            //     'required' => false,
            //     'choices' => array_combine($parentTypes, $parentTypes)
            // ])
            ->add('bt', null, ['required' => false, 'label' => 'Be There - ID'])
            ->add('popup_text', null, [
                'label' => 'Texto Emergente',
                'required' => false,
                'show_child_legend' => 'El texto no debe superar los 255 caracteres; si se quiere incluir un enlace se debe usar el siguiente formato: <a href="[[URL ENLACE]]" target="_blank">[[TEXTO ENLACE]]</a>'
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Aper\IncentiveBundle\Entity\GoalType',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_incentivebundle_new_goaltype';
    }


}
