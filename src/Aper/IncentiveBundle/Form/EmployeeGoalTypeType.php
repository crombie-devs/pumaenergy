<?php

namespace Aper\IncentiveBundle\Form;

use Aper\IncentiveBundle\Entity\EmployeeGoalType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeGoalTypeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $months = range(1, 12);
        $years = range(date('Y'), 2020);
        $parentTypes = [
            EmployeeGoalType::EXERT,
            EmployeeGoalType::MYSTERY_SHOPPER,
        ];

        $builder
            ->add('name')
            ->add('category')
            ->add('cell')
            ->add('month', ChoiceType::class, [
                'choices' => array_combine($months, $months),
            ])
            ->add('year', ChoiceType::class, [
                'choices' => array_combine($years, $years),
            ])
            ->add('frontSymbol', null, [
                'required' => false,
            ])
            ->add('backSymbol', null, [
                'required' => false,
            ])
            ->add('isSpecial')
            ->add('isTotalizer')
            ->add('parentType', ChoiceType::class, [
                'label' => 'Parent',
                'required' => false,
                'choices' => array_combine($parentTypes, $parentTypes)
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Aper\IncentiveBundle\Entity\EmployeeGoalType',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_incentivebundle_new_goaltype';
    }


}