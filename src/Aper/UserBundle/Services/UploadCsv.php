<?php

namespace Aper\UserBundle\Services;

use Liuggio\ExcelBundle\Factory;

class UploadCsv
{
    /**
     * @var Factory
     */
    private $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }


    public function parseCSV($pathcsv)
    {

        $location = $pathcsv;
        $excel = $this->factory->createPHPExcelObject($location);
        $sheet = $excel->getActiveSheet();

        $highestRow = $sheet->getHighestRow();


        $rows = array();
        //recorre celda de cine
        for ($i = 2; $i <= $highestRow; $i++) {
            $celda = 'A' . $i;
            $rows[$i-1][0] = $sheet->getCell($celda)->getValue();
            $celda = 'B' . $i;
            $rows[$i-1][1] = $sheet->getCell($celda)->getValue();
            $celda = 'C' . $i;
            $rows[$i-1][2] = $sheet->getCell($celda)->getValue();
            $celda = 'D' . $i;
            $rows[$i-1][3] = $sheet->getCell($celda)->getValue();
            $celda = 'E' . $i;
            $excelDate = $sheet->getCell($celda)->getvalue();
            $rows[$i-1][4] = \PHPExcel_Style_NumberFormat::toFormattedString($excelDate, 'dd/mm/YYYY');
            $celda = 'F' . $i;
            $excelDate = $sheet->getCell($celda)->getvalue();
            $rows[$i-1][5] = \PHPExcel_Style_NumberFormat::toFormattedString($excelDate, 'dd/mm/YYYY');
            $celda = 'G' . $i;
            $rows[$i-1][6] = $sheet->getCell($celda)->getValue();
            $celda = 'H' . $i;
            $rows[$i-1][7] = $sheet->getCell($celda)->getValue();
            $celda = 'I' . $i;
            $rows[$i-1][8] = $sheet->getCell($celda)->getValue();
            $celda = 'J' . $i;
            $rows[$i-1][9] = $sheet->getCell($celda)->getValue();
            $celda = 'K' . $i;
            $rows[$i-1][10] = $sheet->getCell($celda)->getValue();
            $celda = 'L' . $i;
            $rows[$i-1][11] = $sheet->getCell($celda)->getValue();
            $celda = 'M' . $i;
            $rows[$i-1][12] = strtoupper($sheet->getCell($celda)->getValue());
             $celda = 'N' . $i;
            $rows[$i-1][13] = $sheet->getCell($celda)->getValue();
             $celda = 'O' . $i;
            $rows[$i-1][14] = $sheet->getCell($celda)->getValue();
             $celda = 'P' . $i;
            $rows[$i-1][15] = $sheet->getCell($celda)->getValue();
        }

        return $rows;

    }
}
