<?php

namespace Aper\UserBundle\Services;

use Aper\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Liuggio\ExcelBundle\Factory;


class ExcelExporterStore
{
    private $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }


    public function getResponseFromSuccessfulLogin($users, $filter, $filename){
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        // solicitamos el servicio 'phpexcel' y creamos el objeto vacÃ­o...
        $phpExcelObject = $this->factory->createPHPExcelObject();

        // ...y le asignamos una serie de propiedades
        $phpExcelObject->getProperties()
            ->setCreator("Plan de Carrera")
            ->setLastModifiedBy("Plan de Carrera")
            ->setTitle(utf8_decode("Exportación de estadísticas de Ingresos de Usuarios"))
            ->setSubject("Logins")
            ->setDescription("Estadísticas de Ingresos generado el " . date('d/m/Y') . " a las " . date('H:i:s'))
            ->setKeywords("Plan de carrera, ingresos, logins, usuarios");

        // establecemos como hoja activa la primera, y le asignamos un título
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->setTitle('Usarios');

        // escribimos en distintas celdas del documento el tÃ­tulo de los campos que vamos a exportar
        $sheet = $phpExcelObject->setActiveSheetIndex(0);

        $sheet->setCellValue('A1', 'Filtros');
        $sheet->setCellValue('A2', 'Texto Búsqueda:');
        $sheet->setCellValue('B2', $filter['user']);
        $sheet->setCellValue('A3', 'Desde:');
        $filterDateFrom = $filter['dateFrom'] ? $filter['dateFrom']->format('d/m/Y') : '';
        $sheet->setCellValue('B3', $filterDateFrom);
        $sheet->setCellValue('A4', 'Hasta:');
        $filterDateTo = $filter['dateTo'] ? $filter['dateTo']->format('d/m/Y') : '';
        $sheet->setCellValue('B4', $filterDateTo);
        $sheet->setCellValue('A5', 'Fecha y Hora de Exportación:');
        $sheet->setCellValue('B5', date('d/m/Y - H:i:s'));


        $sheet->setCellValue('A7', 'ID Pixelnet');
        $sheet->setCellValue('B7', 'Perfil');
        $sheet->setCellValue('C7', 'Nombre');
        $sheet->setCellValue('D7', 'Correo Electrónico');
        $sheet->setCellValue('E7', 'Teléfono');
        $sheet->setCellValue('F7', 'Estación');
        $sheet->setCellValue('G7', 'Fecha de Alta');
        $sheet->setCellValue('H7', 'Fecha último ingreso');
        $sheet->setCellValue('I7', 'Cantidad de Ingresos');

        $i= 8;
        foreach ($users as $user){
            /** @var User $u */
            $u = $user[0];
            $sheet->setCellValue('A'.$i, $u->getEmployee()->getIdPixelnetUsr());
            $sheet->setCellValue('B'.$i, $u->getEmployee()->getSubRol() ? $u->getEmployee()->getSubRol() : "sin subrol definido");

            $sheet->setCellValue('C'.$i, $u->getEmployee()->getProfile()->getName());
            $sheet->setCellValue('D'.$i, $u->getEmail());
            $sheet->setCellValue('E'.$i, $u->getEmployee()->getProfile()->getPhoneAreaCode() . "-" . $u->getEmployee()->getProfile()->getPhoneNumber());
            $sheet->setCellValue('F'.$i, $u->getEmployee()->getStore());
            $startedAt = $u->getEmployee()->getProfile()->getStartAt();
            $sheet->setCellValue('G'.$i, ($startedAt ? $startedAt->format('d/m/Y - H:i:s') : $startedAt));
            $lastLoginAt = $u->getLastLoginAt();
            $sheet->setCellValue('H'.$i, ($lastLoginAt ? $lastLoginAt->format('d/m/Y - H:i:s') : $lastLoginAt));
            $sheet->setCellValue('I'.$i, $user['countLogins']);
            $i = $i+1;
        }
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);

        // se crea el writer
        $writer = $this->factory->createWriter($phpExcelObject, 'Excel5');
        // se crea el response
        $response = $this->factory->createStreamedResponse($writer);

        // y por Ãºltimo se aÃ±aden las cabeceras
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    public function getResponseFromAllUsers($users, $filter, $filename){
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        // solicitamos el servicio 'phpexcel' y creamos el objeto vacÃ­o...
        $phpExcelObject = $this->factory->createPHPExcelObject();

        // ...y le asignamos una serie de propiedades
        $phpExcelObject->getProperties()
            ->setCreator("Todos los usuarios")
            ->setLastModifiedBy("Todos los usuarios")
            ->setTitle(utf8_decode("Exportación de estadísticas de todos los usuarios"))
            ->setSubject("Logins")
            ->setDescription("Estadísticas de todos los usuarios generado el " . date('d/m/Y') . " a las " . date('H:i:s'))
            ->setKeywords("Todos los usuarios");

        // establecemos como hoja activa la primera, y le asignamos un título
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->setTitle('Usarios');

        // escribimos en distintas celdas del documento el tÃ­tulo de los campos que vamos a exportar
        $sheet = $phpExcelObject->setActiveSheetIndex(0);

        $sheet->setCellValue('A1', 'Filtros');
        $sheet->setCellValue('A2', 'Texto Búsqueda:');
        $sheet->setCellValue('B2', $filter['user']);
        $sheet->setCellValue('A3', 'Fecha y Hora de Exportación:');
        $sheet->setCellValue('B3', date('d/m/Y - H:i:s'));

        $sheet->setCellValue('A5', 'ID Pixelnet');
        $sheet->setCellValue('B5', 'Perfil');
        $sheet->setCellValue('C5', 'Nombre');
        $sheet->setCellValue('D5', 'Correo Electrónico');
        $sheet->setCellValue('E5', 'Teléfono');
        $sheet->setCellValue('F5', 'Estación');
        $sheet->setCellValue('G5', 'Fecha de Alta');
        $sheet->setCellValue('H5', 'Activo');

        $i= 6;
        foreach ($users as $user){
            /** @var User $u */
            $u = $user;
            $sheet->setCellValue('A'.$i, $u->getEmployee()->getIdPixelnetUsr());
            $sheet->setCellValue('B'.$i, $u->getEmployee()->getSubRol() ? $u->getEmployee()->getSubRol() : "sin subrol definido");
            $sheet->setCellValue('C'.$i, $u->getEmployee()->getProfile()->getName());
            $sheet->setCellValue('D'.$i, $u->getEmail());
            $sheet->setCellValue('E'.$i, $u->getEmployee()->getProfile()->getPhoneAreaCode() . "-" . $u->getEmployee()->getProfile()->getPhoneNumber());
            $sheet->setCellValue('F'.$i, $u->getEmployee()->getStore());
            $startedAt = $u->getEmployee()->getProfile()->getStartAt();
            $sheet->setCellValue('G'.$i, ($startedAt ? $startedAt->format('d/m/Y - H:i:s') : $startedAt));
            $sheet->setCellValue('H'.$i, ($u->isEnabled() ? 'Si' : 'No'));
            $i = $i+1;
        }
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        // se crea el writer
        $writer = $this->factory->createWriter($phpExcelObject, 'Excel5');
        // se crea el response
        $response = $this->factory->createStreamedResponse($writer);

        // y por Ãºltimo se aÃ±aden las cabeceras
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }
}
