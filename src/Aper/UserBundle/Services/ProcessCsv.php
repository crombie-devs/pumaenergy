<?php

namespace Aper\UserBundle\Services;

use Aper\StoreBundle\Entity\Store;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\Profile;
use Aper\UserBundle\Entity\User;
use Aper\UserBundle\Model\Role;
use Doctrine\ORM\EntityManager;

/**
 * Class ProcessCsv.
 */
class ProcessCsv
{
    const CATEGORY = 0;

    const CODE = 1;

    const NAME = 2;

    const GENDER = 3; // new

    const BIRTHDAY = 4;

    const STARTAT = 5;

    const AREA = 6; // new

    const DEPARTURE_AT = 7; // new

    const AGREEMENT = 8; // new

    const STORE_CODE = 9; // new

    const LOCATION = 10;

    const DNI = 11;

    const ROL = 12;
    
    const CORPMAIL = 13;

    const OFFICE_PHONE = 14;
    
    const PROVINCE = 15;
    
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * ProcessCsv constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $rows
     */
    public function ProcessCSV($rows)
    {
        $ids = [];
        foreach ($rows as $row) {
            if($row[self::DNI] == "")
                break;

            if ($this->isNew($row)) {
                $ids[] = $this->doAlta($row);
            } else {
                $ids[] = $this->doUpdate($row);
            }
        }

        // $this->disabledNotIn($ids);
    }

    /**
     * @param $row
     *
     * @return bool
     */
    private function isNew($row)
    {
        $repository = $this->em->getRepository('UserBundle:User');
        $resultado = $repository->findOneByUsername($row[self::DNI]);

        return !$resultado;
    }

    /**
     * @param $row
     *
     * @return int
     */
    private function doAlta($row)
    {
        $user = $this->createEntityPuma($row);

        $this->saveEntity($user);

        return $user->getId();
    }

    private function createEntityPuma($row){
        $user = new User();
        $user->setUsername($row[self::DNI]);
        $user->setEmail($row[self::CORPMAIL]);
        $user->setPlainPassword('123456');
        $user->setEnabled(true);
        $user->addRole(Role::convertRole(strtoupper($row[self::ROL])));

        $employee = new Employee();
        $employee->setCategory($row[self::CATEGORY]);
        $employee->setCode($row[self::CODE]);
        $employee->setArea($row[self::AREA]);
        $employee->setAgreement($row[self::AGREEMENT]);

        $profile = new Profile();
        $profile->setName($row[self::NAME]);
        $profile->setSex($row[self::GENDER]);
    
        if(isset($row[self::CORPMAIL]))
            $profile->setCorpMail($row[self::CORPMAIL]);
    
        if(isset($row[self::OFFICE_PHONE]))
            $profile->setOfficePhone($row[self::OFFICE_PHONE]);
    
        if(isset($row[self::PROVINCE]))
            $profile->setProvince($row[self::PROVINCE]);

        $row = $this->updateProfileDates($row, $profile);
        $employee->setProfile($profile);
        $user->setEmployee($employee);

        $store = $this->searchStore($row);

        $employee->setStore($store);

        return $user;
    }

    /**
     * @param $row
     *
     * @return User
     */
    private function createEntity($row)
    {
        $user = new User();
        $user->setUsername($row[self::DNI]);
        $user->setEmail($row[self::DNI].'@example.com');
        $user->setPlainPassword('123456');
        $user->setEnabled(true);
        $user->addRole(Role::convertRole(strtoupper($row[self::ROL])));

        $employee = new Employee();
        $employee->setCategory($row[self::CATEGORY]);
        $employee->setCode($row[self::CODE]);
        $employee->setArea($row[self::AREA]);
        $employee->setAgreement($row[self::AGREEMENT]);

        $profile = new Profile();
        $profile->setName($row[self::NAME]);
        $profile->setSex($row[self::GENDER]);
    
        if(isset($row[self::CORPMAIL]))
            $profile->setCorpMail($row[self::CORPMAIL]);
    
        if(isset($row[self::OFFICE_PHONE]))
            $profile->setOfficePhone($row[self::OFFICE_PHONE]);
    
        if(isset($row[self::PROVINCE]))
            $profile->setProvince($row[self::PROVINCE]);

        $row = $this->updateProfileDates($row, $profile);
        $employee->setProfile($profile);
        $user->setEmployee($employee);

        $store = $this->searchStore($row);

        $employee->setStore($store);

        return $user;
    }

    /**
     * @param $row
     *
     * @return Store
     */
    private function searchStore($row)
    {
        $em = $this->em;
        $estacion = $em->getRepository('StoreBundle:Store')->findOneByCode($row[self::STORE_CODE]);

        if (!$estacion) {
            $estacion = new Store();
            $estacion->setCode($row[self::STORE_CODE]);
            $estacion->setLocation($row[self::LOCATION]);
            $estacion->setBrand("PUMA-SIN-CLASIFICAR");
            $estacion->setCliente('SIN-CLASIFICAR');
            $estacion->setTipoComercio('EESS');
            $estacion->setSegmento('');
            $estacion->setBandera('PUMA');
            $estacion->setResponsableComercial('SIN-CLASIFICAR');
            $em->persist($estacion);
            $em->flush();
        }

        return $estacion;
    }

    /**
     * @param $user
     */
    private function saveEntity($user)
    {
        $em = $this->em;

        $em->persist($user);
        $em->flush();
    }

    /**
     * @param $row
     *
     * @return mixed
     */
    private function doUpdate($row)
    {
        $repo = $this->em->getRepository('UserBundle:User');
        $user = $repo->findOneByUsername($row[self::DNI]);

        $this->updateEntity($user, $row);

        $this->saveEntity($user);

        return $user->getId();
    }

    /**
     * @param User $user
     * @param $row
     *
     * @return User
     */
    private function updateEntity(User $user, $row)
    {
        if($row[self::ROL] == null)
            return;

        $isTrainer = $user->hasRole('ROLE_TRAINER');
        $isTrainerAdmin = $user->hasRole('ROLE_TRAINER_ADMIN');
        $isStoreManager = $user->hasRole('ROLE_STORE_MANAGER');

        $user->setRoles([Role::convertRole(strtoupper($row[self::ROL]))]);
        if ($isTrainer) {
            $user->addRole('ROLE_TRAINER');
        }
        if ($isTrainerAdmin) {
            $user->addRole('ROLE_TRAINER_ADMIN');
        }
        if ($isStoreManager) {
            $user->addRole('ROLE_STORE_MANAGER');
        }

        $user->setEnabled(1);
        $employee = $user->getEmployee();
        $employee->setCategory($row[self::CATEGORY]);
        $employee->setCode($row[self::CODE]);
        $employee->setArea($row[self::AREA]);
        $employee->setAgreement($row[self::AGREEMENT]);

        $profile = $employee->getProfile();
        $profile->setSex($row[self::GENDER]);
        $profile->setName($row[self::NAME]);

         if(isset($row[self::CORPMAIL]))
            $profile->setCorpMail($row[self::CORPMAIL]);
    
        if(isset($row[self::OFFICE_PHONE]))
            $profile->setOfficePhone($row[self::OFFICE_PHONE]);
    
        if(isset($row[self::PROVINCE]))
            $profile->setProvince($row[self::PROVINCE]);

        $row = $this->updateProfileDates($row, $profile);

        $store = $this->searchStore($row);

        $employee->setStore($store);

        return $user;
    }

    /**
     * @param $ids
     */
    private function disabledNotIn($ids)
    {
        if (!$ids) {
            return;
        }

        // UPDATE user where id not in (:ids)
        $q = $this->em->createQueryBuilder()
            ->update('UserBundle:User', 'u')
            ->set('u.enabled', 0)
            ->where("u.id not in (:arr) AND u.username != 'admin'")
            ->setParameter('arr', $ids)
            ->getQuery();

        $q->execute();
    }

    /**
     * @param $row
     * @param $profile
     * @return mixed
     */
    private function updateProfileDates($row, Profile $profile)
    {
        if (!empty($row[self::BIRTHDAY])) {
            $profile->setBirthDay(\DateTime::createFromFormat('d/m/Y', $row[self::BIRTHDAY])->setTime(0,0,0));
        }
        if (!empty($row[self::STARTAT])) {
            $profile->setStartAt(\DateTime::createFromFormat('d/m/Y', $row[self::STARTAT])->setTime(0,0,0));
        }
        if (!empty($row[self::DEPARTURE_AT])) {
            $profile->setDepartureAt(\DateTime::createFromFormat('d/m/Y', $row[self::DEPARTURE_AT])->setTime(0,0,0));
            return $row;
        }
        return $row;
    }
}
