<?php
namespace Aper\UserBundle\Util;
// error_reporting(E_ALL);

use Aper\StoreBundle\Entity\Store;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\Profile;
use Aper\UserBundle\Entity\User;
use Aper\UserBundle\Model\Role;

class EmployeeManager {

    private $em;
    private $factory;

    public function __construct( $entityManager, $factory ) {
        $this->em = $entityManager;
        $this->factory = $factory;
    }

    private function comprobarCadena( $cadena ){
        $codigos = array(
//        "'", '"', "(", ")", "DROP", "TABLE", "ALTER", "VALUES", "*",  "%",
            "INSERT", "UPDATE", "SELECT", "DELETE", "`", "[", "]", "{", "}", "\x00", "\x1a",
        );
        return str_replace( $codigos, " ", $cadena );
    }

    public function contarTotalFasesUpdate() {
        return 2;
    }

    /**
     * 
     * @param integer $fase
     */
    public function actualizarRegistros( $fase = 1 ) {

    }

    public function agregarRegistros( $valor ) {
        $db = $this->em->getConnection();
        $sql_acum = '';
        foreach( $valor as $key => $fila ) 
        {
            $fila_limpia = $this->comprobarCadena( $fila );
            $renglon = explode( ";", $fila_limpia );

            if( count( $renglon ) != 17) {
                print_r( $renglon );
                continue;
            }

            $category      = $renglon[0];
            $code          = $renglon[1];
            $name          = $renglon[2];
            $gender        = $renglon[3];
            $birthday      = $renglon[4];
            $startat       = $renglon[5];
            $area          = $renglon[6];
            $departure_at  = $renglon[7];
            $agreement     = $renglon[8];
            $store_code    = $renglon[9];
            $location      = $renglon[10];
            $dni           = $renglon[11]; // En el original de Aper es DNI, 
            $rol           = $renglon[12]; // pero aca es nombre de usuario
            $corpmail      = $renglon[13];
            $office_phone  = $renglon[14];
            $province      = $renglon[15];
            $idPixelnetUsr = $renglon[16];

            $category = str_replace('/', '', $category);
            $category = str_replace('.', '', $category);
            if($category != '' && !preg_match('/^([A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ]+[\s]*)+$/', $category)){
                echo 'category ';
                print_r($renglon);
                continue;
            }
            if($code != '' && !preg_match('/^[0-9]*$/', $code)){
                echo 'code ';
                print_r($renglon);
                continue;
            }

            if($name != '' && !preg_match('/^([A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ]+[\s\.]*)+$/', $name)){
                echo 'name ';
                print_r($renglon);
                continue;
            }

            if($gender != '' && !preg_match('/^([A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ]+[\s]*)+$/', $gender)){
                echo 'gender ';
                print_r($renglon);
                continue;
            }

            if($birthday != ''){
                $date = str_replace('/', '-', $birthday);
                $birthday = "'".date('Y-m-d', strtotime($date))."'";
            } else {
                $birthday = "NULL";
            }

            if($startat != ''){
                $date = str_replace('/', '-', $startat);
                $startat = "'".date('Y-m-d', strtotime($date))."'";
            } else {
                $startat = "NULL";
            }

            $area = str_replace('/', '', $area);
            $area = str_replace('.', '', $area);
            if($area != '' && !preg_match('/^([A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ]+[\s]*)+$/', $area)){
                echo 'area ';
                print_r($renglon);
                continue;
            }

            if($departure_at != ''){
                $date = str_replace('/', '-', $departure_at);
                $departure_at = "'".date('Y-m-d', strtotime($date))."'";
            } else {
                $departure_at = "NULL";
            }

            if($agreement != '' && !preg_match('/^([A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ]+[\s]*)+$/', $agreement)){
                echo 'agreement ';
                print_r($renglon);
                continue;
            }
            if(strpos('SIN INDICAR', $store_code)){
                $store_code = '0';
            }
            if($store_code != '' && !preg_match('/^[0-9]*$/', $store_code)){
                echo 'store_code ';
                print_r($renglon);
                continue;
            }

            if($dni != ''){
                if(!preg_match('/^([0-9A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ]+[\.\-]*)*$/', $dni)){
                    echo 'usuario ';
                    print_r($renglon);
                    continue;
                }
            }

            $role = '';
            switch ($rol) {

                case 'PERFIL 1':
                    $role = 'a:1:{i:0;s:16:"ROLE_HEAD_OFFICE";}';
                    break;
                case 'PERFIL 2':
                    $role = 'a:1:{i:0;s:12:"ROLE_MANAGER";}';
                    break;
                case 'PERFIL 3':
                    $role = 'a:1:{i:0;s:13:"ROLE_EMPLOYEE";}';
                    break;
                case 'PERFIL 4':
                    $role = 'a:1:{i:0;s:9:"ROLE_MANAGER_GLOBAL";}';
                    break;
                case 'PERFIL 5':
                    $role = 'a:1:{i:0;s:10:"ROLE_ADMIN";}';
                    break;
                case 'PERFIL 6':
                    $role = 'a:1:{i:0;s:19:"ROLE_ADMIN_CONSULTA";}';
                    break;
                case 'PERFIL 7':
                    $role = 'a:1:{i:0;s:23:"ROLE_ADMIN_CAPACITACION";}';
                    break;

                default:
                    $role = 'a:1:{i:0;s:13:"ROLE_EMPLOYEE";}';
                    break;
            }

            if ($corpmail != '' && !filter_var($corpmail, FILTER_VALIDATE_EMAIL)) {
                echo 'corpmail ';
                continue;
            }

            if($office_phone != '' && !preg_match('/^[0-9]+[\s]*$/', $office_phone)){
                echo 'office_phone';
                continue;
            }

            if($idPixelnetUsr != '' && !preg_match('/^[0-9]*$/', $idPixelnetUsr)){
                echo 'idPixelnetUsr';
                continue;
            }

            if($province != '' && !preg_match('/^([A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ]+[\s]*)+$/', $province)){
                echo 'province ';
                echo $province;
                continue;
            }

            // Si lo encuentra, hace update y acumula en el sql_acum
            // Si no lo encuentra, hace los inserts
            $stmt = $db->prepare( "SELECT id FROM users WHERE email LIKE '$corpmail' OR username LIKE '$dni'");
            $stmt->execute();
            $us = $stmt->fetchAll();
            if(empty($us)){
                // Hacer insert
                $user = new User();// you object \Acme\DemoBundle\Entity\User from database
                $encoder = $this->factory->getEncoder($user);
                $salt = $user->getSalt();
                $pass = $encoder->encodePassword('123456', $salt);

                // Hay que crear un store con code = 0, en mi caso es el que tiene id 133
                $sql_acum .= "INSERT INTO users (username, email, enabled, salt, password, roles, locked, expired, credentials_expired, cant_employee) VALUES ('$dni', '$corpmail', 1, '$salt', '$pass', '$role', 0, 0, 0, 0);
SET @last_id_in_users = LAST_INSERT_ID();
INSERT INTO employees (user_id, store_id, category, code, area, agreement, id_pixelnet_usr) VALUES (@last_id_in_users, (SELECT COALESCE((SELECT id FROM stores WHERE code LIKE '$store_code'), 133) ), '$category', '$code', '$area', '$agreement', '$idPixelnetUsr');
SET @last_id_in_employee = LAST_INSERT_ID();
INSERT INTO profiles (employee_id, name, birth_day, start_at, sex, departure_at, corpmail, office_phone, province) VALUES (@last_id_in_employee, '$name',$birthday, $startat, '$gender', $departure_at, '$corpmail', '$office_phone', '$province');
";
    

            } else {
                $id = $us[0]['id'];
                $sql_acum .= "UPDATE users SET username='$dni', email='$corpmail', roles='$role' WHERE id = $id;
";
                $sql_acum .= "UPDATE employees SET 
                    store_id=(SELECT COALESCE((SELECT id FROM stores WHERE code LIKE '$store_code'), 133)), 
                    category='$category', 
                    code='$code', 
                    area='$area', 
                    agreement='$agreement',
                    id_pixelnet_usr='$idPixelnetUsr' 
                    WHERE user_id = $id;
";

                $sql_acum .= "UPDATE profiles SET 
                name='$name', 
                birth_day=$birthday, 
                start_at=$startat, 
                departure_at=$departure_at, 
                corpmail='$corpmail', 
                office_phone='$office_phone', 
                province='$province' WHERE employee_id = (SELECT id FROM employees WHERE user_id = $id LIMIT 1);
";
            }
            $stmt = $this->em->getConnection()->prepare( $sql_acum. ' COMMIT;' );
            $stmt->execute();
            $stmt->closeCursor();
            $sql_acum = '';
        }

        // $this->em-flush();
    }

    /**
     * @param $row
     *
     * @return Store
     */
    private function searchStore($store_code, $location)
    {
        $em = $this->em;
        $estacion = $em->getRepository('StoreBundle:Store')->findOneByCode($store_code);

        if (!$estacion) {
            $estacion = new Store();
            $estacion->setCode($store_code);
            $estacion->setLocation($location);
            $estacion->setBrand("PUMA-SIN-CLASIFICAR");
            $estacion->setCliente('SIN-CLASIFICAR');
            $estacion->setTipoComercio('EESS');
            $estacion->setSegmento('');
            $estacion->setBandera('PUMA');
            $estacion->setResponsableComercial('SIN-CLASIFICAR');
            $em->persist($estacion);
            $em->flush();
        }

        return $estacion;
    }

    /**
     * @param $birthday 
     * @param $startat
     * @param $departure_at
     * @param $row
     * @param $profile
     * @return mixed
     */
    private function updateProfileDates($birthday, $startat, $departure_at, Profile $profile)
    {
        if (!empty($birthday)) {
            $profile->setBirthDay(\DateTime::createFromFormat('d/m/Y', $birthday)->setTime(0,0,0));
        }
        if (!empty($startat)) {
            $profile->setStartAt(\DateTime::createFromFormat('d/m/Y', $startat)->setTime(0,0,0));
        }
        if (!empty($departure_at)) {
            $profile->setDepartureAt(\DateTime::createFromFormat('d/m/Y', $departure_at)->setTime(0,0,0));
        }
        return $profile;
    }

    /**
     * 
     * @param int $cantidadRegistros
     */
    public function borrarRegistros( $cantidadRegistros = 1000 ) {
        // $db = $this->em->getConnection();
        // $sql = "DELETE FROM tabla LIMIT $cantidadRegistros;";
        // $stmt = $db->prepare( $sql );
        // $stmt->execute();
       die;
    }

    public function contarRegistros() {
        $db = $this->em->getConnection();
        $sql = "SELECT COUNT( * ) AS cantidad FROM users";
        
        $stmt = $db->prepare( $sql );
        $stmt->execute();
        $res = $stmt->fetchAll();
        
        return $res[ 0 ][ "cantidad" ];
    }
}