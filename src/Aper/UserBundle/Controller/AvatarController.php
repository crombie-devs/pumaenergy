<?php

namespace Aper\UserBundle\Controller;

use Aper\UserBundle\Form\ProfileAvatarType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WebFactory\Bundle\UserBundle\Controller\ContextController;

class AvatarController extends ContextController
{

    /**
     * @return array
     * @Template()
     */
    public function getAvatarFormAction()
    {
        $user = $this->getUser();
        $form = $this->createAvatarForm($user);

        return ['form' => $form->createView()];
    }

    /**
     * @param Request $request
     * @return array|Response
     * @Route("/profile/update-avatar")
     * @Template("@User/Avatar/getAvatarForm.html.twig")
     */
    public function processAvatarFormAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createAvatarForm($user);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->getUserContext()->getManager('user')->save($user);

            return Response::create('');
        }

        return ['form' => $form->createView()];
    }

    /**
     * @param $user
     * @return \Symfony\Component\Form\Form
     */
    private function createAvatarForm($user)
    {
        $form = $this->createForm(ProfileAvatarType::class, $user, [
            'action' => $this->generateUrl('aper_user_avatar_processavatarform'),
        ]);

        $form->add('submit', SubmitType::class);

        return $form;
    }

}
