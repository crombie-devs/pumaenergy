<?php
namespace Aper\UserBundle\Controller;

use Aper\UserBundle\Util\EmployeeManager;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controlador de los Ajax para subir archivos
 * @Route("/upload/file")
 */
class AjaxUploadController extends Controller {

    /**
     * Muestra formulario para subir archivos al sistema
     * @Route("/", name="UserBundle_upload_ajax_file")
     * @Method({"GET"})
     * Template()
     */
    public function indexAction() {
        return $this->render('UserBundle:Backend/Employee:index1.html.twig', array());
    }


    /**
     * Cuenta registros de la tabla cupones
     * @Route("/ajax/count/cupon", name="UserBundle_upload_ajax_file_count_cupon")
     * @Method({"POST"})
     * Template()
     */
    public function ajaxContarRegistrosCuponAction() {
        $em = $this->getDoctrine()->getManager();
        $cuponManager = new CuponManager( $em );
        $cuponManager->contarCupones();
        die;
    }

    /**
     * Inserta registros en las tablas temporales
     * @Route("/ajax/insert", name="UserBundle_upload_ajax_file_insert")
     * @Method({"POST"})
     * Template()
     */
    public function ajaxInsertAction( Request $request ) {
        // Registros es un string con un array, donde cada row es un registro completo.
        // TipoArchivo es el tipo de archivo que subo.
        // Operacion es si es insert, update o delete
        $em = $this->getDoctrine()->getManager();

        $registros   = $request->get( "registros" );
        $tipoArchivo = $request->get( "tipoArchivo" );

        $registrosFormateados = str_replace( "[", "", $registros );
        $registrosFormateados = str_replace( "]", "", $registrosFormateados );
        $registrosFormateados = explode( '","', $registrosFormateados );
        foreach( $registrosFormateados as $key => $value ) {
            $registrosFormateados[ $key ] = str_replace( '"', "", $registrosFormateados[ $key ] );
            $registrosFormateados[ $key ] = str_replace( '\r', "", $registrosFormateados[ $key ] );
        }
        switch( $tipoArchivo ) {
            case "EMPLOYEE":
                $factory         = $this->get('security.encoder_factory');
                $employeeManager = new EmployeeManager( $em, $factory );
                $employeeManager->agregarRegistros( $registrosFormateados );
                break;
        }
        die;
    }

    /**
     * Actualiza las tablas y ejecuta la fecha de actualizacion para las consultas
     * @Route("/ajax/update", name="UserBundle_upload_ajax_file_update")
     * @Method({"POST"})
     * Template()
     */
    public function ajaxUpdateAction( Request $request ) {
        // Registros es un string con un array, donde cada row es un registro completo.
        // TipoArchivo es el tipo de archivo que subo.
        // Operacion es si es insert, update o delete
        $em = $this->getDoctrine()->getManager();

        $tipoArchivo = $request->get( "tipoArchivo" );
        $fase = (integer) $request->get( "fase" );

//        <input type="radio" name="operacion" value="0">
//        <label class="css-checkbox">AGREGAR</label>
        switch( $tipoArchivo ) {
//        case "0":
            case "EMPLOYEE":
                $employeeManager = new EmployeeManager( $em );
                $employeeManager->actualizarRegistros( $fase );
                break;
        }

        $nombresArchivos = [
            "EMPLOYEE"   => "empleados",
        ];

        die;
    }

    /**
     * Borra los registros de las tablas temporales
     * @Route("/ajax/delete", name="UserBundle_upload_ajax_file_delete")
     * @Method({"POST"})
     * Template()
     */
    public function ajaxDeleteAction( Request $request ) {
        // Registros es un string con un array, donde cada row es un registro completo.
        // TipoArchivo es el tipo de archivo que subo.
        // Operacion es si es insert, update o delete
        $em = $this->getDoctrine()->getManager();

        $tipoArchivo = $request->get( "tipoArchivo" );
        $registros = intval( $request->get( "cantidad" ) );

//        <input type="radio" name="operacion" value="1">
//        <label class="css-checkbox">BORRAR</label>
        switch( $tipoArchivo ) {
//        case "0":
            case "EMPLOYEE":
                $employeeManager = new EmployeeManager($em);
                $employeeManager->borrarRegistros();
                break;
        }
        die;
    }


    /**
     * Cuenta la cantidad de registros que hay en las tablas 
     * @Route("/ajax/count", name="UserBundle_upload_ajax_file_count")
     * @Method({"POST"})
     * Template()
     */
    public function ajaxContarRegistrosAction( Request $request ) {
        // Registros es un string con un array, donde cada row es un registro completo.
        // TipoArchivo es el tipo de archivo que subo.
        // Operacion es si es insert, update o delete
        $em = $this->getDoctrine()->getManager();

        $tipoArchivo = $request->get( "tipoArchivo" );

        switch( $tipoArchivo ) {
            case "EMPLOYEE":
                $employeeManager = new EmployeeManager( $em );
                print_r( $employeeManager->contarRegistros() );
                break;
        }
        die;
    }
    
    /**
     * Muestra la cantidad de updates que hay en las tablas 
     * @Route("/ajax/count/updates", name="UserBundle_upload_ajax_count_updates")
     * @Method({"POST"})
     * Template()
     */
    public function ajaxContarUpdatesAction( Request $request ) {
        // Registros es un string con un array, donde cada row es un registro completo.
        // TipoArchivo es el tipo de archivo que subo.
        // Operacion es si es insert, update o delete
        $tipoArchivo = $request->get( "tipoArchivo" );
        print_r( $this->contarUpdatesAuxiliar( $tipoArchivo ) );
        die;
    }

    /**
     * Cuenta la cantidad de updates que hay en las tablas 
     * @param string $archivo Archivo que se actualiza
     * @return integer Cantidad de updates necesarios para la tabla
     */
    private function contarUpdatesAuxiliar( $archivo ) {
        $salida = 0;
        $em = $this->getDoctrine()->getManager();

        switch( $archivo ) {
//        case "0":
            case "EMPLOYEE":
                $employeeManager = new EmployeeManager($em);
                $salida = $employeeManager->contarTotalFasesUpdate();
                break;
        }
        return (integer) $salida;
    }
}
