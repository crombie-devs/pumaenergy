<?php

namespace Aper\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Aper\UserBundle\Form\RegisterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Aper\StoreBundle\Entity\Localidad;
use Aper\StoreBundle\Entity\Provincia;
use Aper\StoreBundle\Entity\Store;
use Aper\UserBundle\Entity\User;
use Aper\UserBundle\Entity\Profile;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Model\Role;
use Aper\UserBundle\Entity\Register;

use Aper\UserBundle\Entity\UserOldPass;

/**
 * @Route("register")
 */
class RegisterController extends Controller
{
    /**
     * @Route("/", name="register_index")
     * @Method({"GET", "POST"})
     */
    public function RegisterAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $provincias = $em->getRepository('StoreBundle:Provincia')->findall();
        $localidades = $em->getRepository('StoreBundle:Localidad')->findAllOrderedByNombre();

        $stores = $em->getRepository('StoreBundle:Store')->findAllOrderedByNombre();

        // $estacionesrepetidas = [];
        // foreach ($stores as $key => $value) {
        //     $loc = $value->getLocalidad();
        //     $id = '';
        //     if ($loc) {
        //         $id = $loc->getId();
        //     }

        //     $reg = $value->getLocation() . $id; 
        //     if (isset($estacionesrepetidas[$reg])) {
        //         unset($stores[$key]);
        //     } else {
        //         $estacionesrepetidas[$reg] = $reg;
        //     }
        // }

        $listaErr = [];
        $errores = false;
        
        if($request->get('submit')){
            $provincia = $request->get('provincia');
            $localidad = $request->get('localidad');
            $store = $request->get('store');
            $nombre = $request->get('nombre');
            $apellido = $request->get('apellido');
            $dni = $request->get('dni');
            $email = $request->get('email');
            $confirmarEmail = $request->get('confirmarEmail');
            $role = $request->get('role');

            if($nombre == ""){
                $listaErr[] ='No puede estar vacío ningún campo.';
                $errores = true;
            }
            if($apellido == ""){
                $listaErr[] ='No puede estar vacío ningún campo.';
                $errores = true;
            }
            if($dni == ""){
                $listaErr[] ='No puede estar vacío ningún campo.';
                $errores = true;
            }
            if($email == ""){
                $listaErr[] ='No puede estar vacío ningún campo.';
                $errores = true;
            }
            if($confirmarEmail == ""){
                $listaErr[] ='No puede estar vacío ningún campo.';
                $errores = true;
            }
//            if(!preg_match('/^([ÁÉÍÓÚ]{1}[ñáéíóú]+[\s]*)+$/', $nombre)){
//                $listaErr[] ='Nombre tiene caracteres no alfabéticos';
//                $errores = true;
//            }
//            if(!preg_match('/^([ÁÉÍÓÚ]{1}[ñáéíóú]+[\s]*)+$/', $apellido)){
//                $listaErr[] ='Apellido tiene caracteres no alfabéticos';
//                $errores = true;
//            }
            if(!preg_match('/^[0-9]*$/', $dni)){
                $listaErr[] ='El DNI tiene caracteres no numericos';
                $errores = true;
            }
            if(!preg_match('/^[0-9]*$/', $provincia)){
                $listaErr[] ='Provincia tiene caracteres no numericos';
                $errores = true;
            }
            if(!preg_match('/^[0-9]*$/', $localidad)){
                $listaErr[] ='Localidad tiene caracteres no numericos';
                $errores = true;
            }
            if(!preg_match('/^[0-9]*$/', $store)){
                $listaErr[] ='Store tiene caracteres no numericos';
                $errores = true;
            }
            if($email != $confirmarEmail){
                $listaErr[] ='Los Emails no coinciden';
                $errores = true;
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $listaErr[] = "Invalid email format";
                $errores = true;
            }


            if($errores)
                return $this->render('UserBundle:Frontend/Register:index.html.twig', array(
                    'provincias'  => $provincias,
                    'localidades' => $localidades,
                    'stores'      => $stores,
                    'listaErr'     => $listaErr,
                    'modal'       => true,
                ));

            $reg = $em->getRepository('UserBundle:Register')->findBy(['dni' => $dni]);
            if($reg){
                $listaErr[] = 'Esta persona ya esta registrada';
                return $this->render('UserBundle:Frontend/Register:index.html.twig', array(
                    'provincias'  => $provincias,
                    'localidades' => $localidades,
                    'stores'      => $stores,
                    'listaErr'     => $listaErr,
                    'modal'       => true,
                ));
            }

            # Lo hice dos veces a esto por que necesitaba primero que el DNI este asegurado
            $usuario = $em->getRepository('UserBundle:User')->findBy(['username' => $dni]);
            if($usuario){
                $listaErr[] = 'Usuario existente.';
                return $this->render('UserBundle:Frontend/Register:index.html.twig', array(
                    'provincias'  => $provincias,
                    'localidades' => $localidades,
                    'stores'      => $stores,
                    'listaErr'     => $listaErr,
                    'modal'       => true,
                ));
            }

            $provincia = $em->getRepository('StoreBundle:Provincia')->find($provincia);
            $localidad = $em->getRepository('StoreBundle:Localidad')->find($localidad);
            $store = $em->getRepository('StoreBundle:Store')->find($store);

            $usuario = $em->getRepository('UserBundle:User')->findBy(['email' => $email]);
            if($usuario){
                $listaErr[] = 'Ya se encuentra registrado y activo con ese email.';
                return $this->render('UserBundle:Frontend/Register:index.html.twig', array(
                    'provincias'  => $provincias,
                    'localidades' => $localidades,
                    'stores'      => $stores,
                    'listaErr'     => $listaErr,
                    'modal'       => true,
                ));
            }

            $usuario = $em->getRepository('UserBundle:Register')->findBy(['email' => $email]);
            if($usuario){
                $listaErr[] = 'Ya existe un usaurio registrado con el email, aguarde a ser activado.';
                return $this->render('UserBundle:Frontend/Register:index.html.twig', array(
                    'provincias'  => $provincias,
                    'localidades' => $localidades,
                    'stores'      => $stores,
                    'listaErr'     => $listaErr,
                    'modal'       => true,
                ));
            }

            $store = $em->getRepository('StoreBundle:Store')->find($store);

            $reg = new Register();
            $reg->setProvincia($provincia->getId());
            $reg->setLocalidad($localidad->getId());
            $reg->setStore($store->getId());
            $reg->setNombre($nombre);
            $reg->setApellido($apellido);
            $reg->setDni($dni);
            $reg->setEmail($email);

            $cat = $role;
            if ($role == 'CAJERA/O DE TIENDA' || $role == 'OPERARIA/O DE PLAYA' || $role == 'OPERARIO/A DE SERVICIO') {
                $role = 'ROLE_EMPLOYEE';
            }
            if ($role == 'ROLE_MANAGER') {
                $cat = 'ENCARGADO/A';
            }
            if ($role == 'ROLE_MANAGER_GLOBAL') {
                $role = 'ROLE_MANAGER';
                $cat = 'OPERADOR/ADMINISTRADOR';
            }

            $reg->setRole($role);
            $reg->setSubRol($role);
            $reg->setCategory($cat);
            $em->persist($reg);
            $em->flush($reg);

            return $this->render('UserBundle:Frontend/Register:index.html.twig', array(
                'provincias'  => $provincias,
                'localidades' => $localidades,
                'stores'      => $stores,
                'listaErr'     => $listaErr,
                'modal'       => true,
            ));
        }
        return $this->render('UserBundle:Frontend/Register:index.html.twig', array(
            'provincias'  => $provincias,
            'localidades' => $localidades,
            'stores'      => $stores,
            'listaErr'     => $listaErr,
            'modal'       => false,
        ));
    }

    /**
     * @return Form
     */
    public function createContactForm($provincias)
    {
        $form = $this->createForm(RegisterType::class, $provincias);

        $form->add('submit', SubmitType::class, array(
            'label' => 'Confirmar'


        ));

        return $form;
    }


    /**
     * @Route("/resset/see/", name="user_pass_see")
     * @Method({"GET", "POST"})
     * Template()
     */
    public function ajaxUpdateAction( Request $request ) {
        // Viene la contraseña, la convierte y busca en la tabla del usuario registrado
        // los ultimos 15 si existe.
        // Si existe manda un 0
        // Si no existe  manda un 1, agrega la contraseña anterior del usuario y lo agrega a la nueva tabla

        $em          = $this->getDoctrine()->getManager();
        $pass        = $request->get('pass');
        $factory     = $this->get('security.encoder_factory');
        $user        = $this->get('security.token_storage')->getToken()->getUser();
		//var_dump($user); exit;
        $encoder     = $factory->getEncoder($user);
        $commingPass = $encoder->encodePassword($pass, $user->getSalt());
        $oldPass     = $em->getRepository('UserBundle:UserOldPass')->findBy(['user' => $user], ['pass' => 'ASC'], 15);

        if(!preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]?).{10,15}$/', $pass)){
            $json = array('mensaje' => "La nueva Contraseña no es válida: La contraseña debe contener de 10 a 15 caracteres, e incluir al menos una mayúscula y un número.", 'noError' => 0 );
            return new JsonResponse($json);                
        }
        if($commingPass == $user->getPassword()){
            $json = array('mensaje' => "Su contraseña no puede ser igual al nombre de usuario", 'noError' => 0 );
            return new JsonResponse($json);
        }

        if(
            strpos(strtoupper($pass), strtoupper($user->getUserName())) || 
            strpos(strtoupper($user->getUserName()), strtoupper($pass))
        ){
            $json = array('mensaje' => "Su nombre de usuario no debe estar contenido en la contraseña", 'noError' => 0 );
            return new JsonResponse($json);
        }
        $flag = true;
        foreach ($oldPass as $key => $pass) {
            if($commingPass == $pass->getPass()){
                $json = array('mensaje' => "Esa contraseña ya fue utilizada anteriormente", 'noError' => 0 );
                return new JsonResponse($json);
            }
        }

        // El pass actual es igual al que ya tiene o que su nombre este dentro del pass
        $addPass = new UserOldPass();
        $addPass->setUser($user);
        $addPass->setPass($user->getPassword());
        $addPass->setLastRegistered(new \DateTime('now'));
        $em->persist($addPass);
        $em->flush();
        $json = array('mensaje' => '', 'noError' => 1);
        return new JsonResponse($json);
    }
}
