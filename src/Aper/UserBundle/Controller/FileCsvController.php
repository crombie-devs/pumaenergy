<?php

namespace Aper\UserBundle\Controller;

use Aper\UserBundle\Entity\User;
use Aper\UserBundle\Form\FileCsvType;
use Aper\UserBundle\Form\UserFilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\GridBundle\Event\ManipulateQueryEvent;
use WebFactory\Bundle\GridBundle\Grid\Configuration;

use Symfony\Component\HttpFoundation\StreamedResponse;


class FileCsvController extends Controller
{

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        ini_set('memory_limit', '1000M');
        ini_set('max_execution_time', 3000);

        $form = $this->createForm(FileCsvType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fu = $form->get('file')->getData();
            $path = $fu->getPathname();

            $rows = $this->get('upload_csv')->parseCSV($path);
            $this->get('process_csv')->processCSV($rows);
            $this->addFlash('success', 'Archivo procesado.');

            return $this->redirectToRoute('aper_usercsv_add');
        }

        return $this->render('UserBundle:Backend/User:filecsv.html.twig', array(
            'form' => $form->createView(), ));
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $qb = $this->getDoctrine()->getRepository(User::class)->createQueryBuilder('user')->innerJoin('user.employee', 'employee');
        $filters = $this->get('web_factory_grid.form_filter_factory')->create(UserFilterType::class);
        $config = new Configuration(
            $qb,
            $filters,
            'main',
            'UserBundle:Backend/User:indexemp.html.twig'
        );

        $grid = $this->get('web_factory_grid.factory')->create($config);

        $grid->addListener(ManipulateQueryEvent::NAME, function (ManipulateQueryEvent $event) {
            $queryBuilder = $event->getQueryBuilder();
            $filters = $event->getFilters();

            if (null !== $filters['username']) {
                $queryBuilder->andWhere('user.username LIKE :name')->setParameter('name', "%{$filters['username']}%");
            }
        });

        return $grid->handle($request);
    }


    public function exportAction()
    {
        $response = new StreamedResponse();
        $response->setCallback(function() {
            $handle = fopen('php://output', 'w+');

            fputcsv($handle, array('NOMBRE','FECHA_DE_NACIMIENTO',
                'TEXTO PERSONAL', 'TELEFONO','OPERADOR','USUARIO/DNI',
                'EMAIL', 'PUESTO', 'LEGAJO', 'AREA',
                'CONVENIO', 'FECHA_INGRESO', 'FECHA_FIN', 'MAIL CORPORATIVO', 'TEL OFICINA', 'PROVINCIA','HABILITADO'
                ),';');

            $em = $this->getDoctrine()->getManager();
            $dql = "SELECT r FROM UserBundle:User r inner join r.employee e ORDER BY r.id asc";
            $users = $em->createQuery($dql)->getresult();

           foreach($users as $row)
            {
                if(!empty($row->getEmployee()->getProfile()->getBirthDay())) {
                    $fechaNac =  $row->getEmployee()->getProfile()->getBirthDay()->format("d-m-Y");
                }
                else {
                    $fechaNac = '';
                }

                if(!empty($row->getEmployee()->getProfile()->getStartAt())) {
                    $fechaStart =   $row->getEmployee()->getProfile()->getStartAt()->format("d-m-Y");
                }
                else {
                    $fechaStart = '';
                }

                if(!empty($row->getEmployee()->getProfile()->getDepartureAt())) {
                    $fechaDeparture =   $row->getEmployee()->getProfile()->getDepartureAt()->format("d-m-Y");
                }
                else {
                    $fechaDeparture = '';
                }

                $corpMail = $row->getEmployee()->getProfile()->getCorpMail();
                $officePhone = $row->getEmployee()->getProfile()->getOfficePhone();
                $province = $row->getEmployee()->getProfile()->getProvince();

                fputcsv(
                    $handle,
                    array(

                          $row->getEmployee()->getProfile()->getName(),
                          $fechaNac,
                          $row->getEmployee()->getProfile()->getPersonalText(),
                          $row->getEmployee()->getProfile()->getPhoneNumber(),
                          $new->getEmployee()->getProfile()->getPhoneAreaCode(),
                          $row->getEmployee()->getProfile()->getOperator(),
                          $row->getUsername(),
                          $row->getEmail(),
                          $row->getEmployee()->getCategory(),
                          $row->getEmployee()->getCode(),
                          $row->getEmployee()->getArea(),
                          $row->getEmployee()->getAgreement(),
                          $fechaStart,
                          $fechaDeparture,
                          $corpMail,
                          $officePhone,
                          $province,
                          $row->getEnabled()
                    ),
                    ';' // The delimiter
                );
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="export_empleados.csv"');

        return $response;
    }
}
