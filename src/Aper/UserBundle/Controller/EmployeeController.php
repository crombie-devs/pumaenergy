<?php

namespace Aper\UserBundle\Controller;

use Aper\BenefitBundle\Entity\Benefit;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Form\AddBenefitToEmployee;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WebFactory\Bundle\UserBundle\Controller\ContextController;

class EmployeeController extends ContextController
{
    /**
     * @param Request $request
     * @param Employee $employee
     * @return RedirectResponse|Response
     */
    public function benefitAction(Request $request, Employee $employee)
    {
        $benefitRepository = $this->getDoctrine()->getRepository(Benefit::class);
        $form = $this->createForm(new AddBenefitToEmployee($benefitRepository, $employee));
        $form->add('submit', SubmitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getUserContext()->getManager('user')->save($employee);

            $this->addFlash('success', 'Se agregó el beneficio al empleado');
            return $this->redirectToRoute('backend_web_factory_user_user_show', ['id' => $employee->getId()]);
        }

        return $this->render('UserBundle:Backend/Employee:add_benefit_form.html.twig', [
            'form' => $form->createView(),
            'user' => $employee->getUser(),
        ]);
    }
}