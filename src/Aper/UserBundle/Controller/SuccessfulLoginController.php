<?php


namespace Aper\UserBundle\Controller;

use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\Register;
use Aper\StoreBundle\Entity\Localidad;
use Aper\StoreBundle\Entity\Provincia;
use Aper\StoreBundle\Entity\Store;
use Aper\UserBundle\Entity\User;
use Aper\UserBundle\Entity\Profile;
use Aper\UserBundle\Model\Role;

use Aper\UserBundle\Form\SuccessfulLoginFilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\GridBundle\Event\ManipulateQueryEvent;
use WebFactory\Bundle\GridBundle\Event\ProcessPostFilterEvent;
use WebFactory\Bundle\GridBundle\Grid\Configuration;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SuccessfulLoginController
 * @package Aper\UserBundle\Controller
 */
class SuccessfulLoginController extends Controller
{

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \WebFactory\Bundle\GridBundle\Exception\GridHandleException
     */
    public function indexAction(Request $request)
    {
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if($this->isGranted( 'ROLE_MANAGER' )){
            // Retirar esto
            // Reemplazarlo con un count de Register
            $empleador = $user->getEmployee();
            $estacion = $empleador->getStore();
            $reg = $em->getRepository('UserBundle:Register')->findBy(['store' => $estacion->getId()]);
            // if($reg){
            //     // Dejarlo en activar
            //     // ACA HAY QUE HACER UN COUNT DE EMPLEADOS PROPIOS A REGISTRAR EN REGISTER
            //     foreach ($reg as $r) {
            //         $user = new User();
            //         $user->setUsername($r->getDni());
            //         $user->setEmail($r->getEmail());
            //         $user->setPlainPassword('123456');
            //         $user->setEnabled(false);
            //         $user->addRole(Role::convertRole(""));

            //         $employee = new Employee();
            //         $employee->setCode($r->getDni());
            //         $profile = new Profile();
            //         $profile->setName($r->getNombre() . ' ' . $r->getApellido());
                
            //         $provincia = $em->getRepository('StoreBundle:Provincia')->find($r->getProvincia());
            //         $profile->setCorpMail($r->getEmail());
            //         $profile->setProvince($provincia->getNombre());

            //         $employee->setProfile($profile);
            //         $user->setEmployee($employee);

            //         $employee->setStore($estacion);

            //         $em->persist($user);
            //         $em->remove($r);
            //     }
            // }
            // $empleadosRepository = $this->getDoctrine()->getRepository(Employee::class);
            // $empleados = $empleadosRepository->findAllEmployeesByStationActiveOrNotAndNotDeleted($estacion,false);
            $user->setCantEmployee(sizeof($reg)); // MODIFICAR CON LA CANTIDAD DE REGISTROS DE REGISTER
        }
        $user->setCantEmployee(0);
        $em->flush();
        $config = new Configuration(
            $this->getDoctrine()->getRepository(User::class)->getQueryBuilder(),
            $this->get('web_factory_grid.form_filter_factory')->create(SuccessfulLoginFilterType::class),
            'main',
            'UserBundle:Backend/SuccessfulLogin:index.html.twig'
        );

        $grid = $this->get('web_factory_grid.factory')->create($config);


        $grid->addListener(ManipulateQueryEvent::NAME, function (ManipulateQueryEvent $event) {

            $queryBuilder = $event->getQueryBuilder();
            $filters = $event->getFilters();

            $queryBuilder->innerJoin('User.successfulLogin', 'SuccessfulLogin');
            $queryBuilder->innerJoin('User.employee', 'Employee');
            $queryBuilder->innerJoin('Employee.profile', 'Profile');

            if (null !== $filters['user'])
            {
                $queryBuilder->andWhere('User.username LIKE :user OR User.email LIKE :user OR Profile.name LIKE :user')->setParameter('user', "%{$filters['user']}%");
            }

            if (null !== $filters['dateFrom'])
            {
                /** @var \DateTime $dateFrom */
                $dateFrom = $filters['dateFrom'];
                $dateFrom->setTime(0,0,0);
                $queryBuilder->andWhere('SuccessfulLogin.date >= :dateFrom')->setParameter('dateFrom', $dateFrom);
            }

            if (null !== $filters['dateTo'])
            {
                /** @var \DateTime $dateTo */
                $dateTo = $filters['dateTo'];
                $dateTo->setTime(23,59,59);
                $queryBuilder->andWhere('SuccessfulLogin.date <= :dateTo')->setParameter('dateTo', $dateTo);
            }

            $queryBuilder->addSelect('count(SuccessfulLogin.id) as countLogins');
            $queryBuilder->groupBy('User.id');

//            if(null !== $filters['store'])
//            {
//                $queryBuilder->andWhere('Employee.store = :store')->setParameter('store', $filters['store']);
//            }

        });

        $form  = $this->createForm(SuccessfulLoginFilterType::class, null);
        $form->handleRequest($request);

        $export = false;
        $exportAll = false;
        $container = $this->container;
        $dataExport = array();
        $filtersSearch = array();
        $grid->addListener(ProcessPostFilterEvent::NAME, function (ProcessPostFilterEvent $event) use ($form, $container, &$export, &$dataExport, &$filtersSearch, &$exportAll) {

            $queryBuilder = $container->get('doctrine')->getRepository(User::class)->getQueryBuilder();
            $filters = $event->getFilters();
            $filtersSearch = $filters;

            $queryBuilder->innerJoin('User.successfulLogin', 'SuccessfulLogin');
            $queryBuilder->innerJoin('User.employee', 'Employee');
            $queryBuilder->innerJoin('Employee.profile', 'Profile');

            if (null !== $filters['user'])
            {
                $queryBuilder->andWhere('User.username LIKE :user OR User.email LIKE :user OR Profile.name LIKE :user')->setParameter('user', "%{$filters['user']}%");
            }

            if (null !== $filters['dateFrom'])
            {
                /** @var \DateTime $dateFrom */
                $dateFrom = $filters['dateFrom'];
                $dateFrom->setTime(0,0,0);
                $queryBuilder->andWhere('SuccessfulLogin.date >= :dateFrom')->setParameter('dateFrom', $dateFrom);
            }

            if (null !== $filters['dateTo'])
            {
                /** @var \DateTime $dateTo */
                $dateTo = $filters['dateTo'];
                $dateTo->setTime(23,59,59);
                $queryBuilder->andWhere('SuccessfulLogin.date <= :dateTo')->setParameter('dateTo', $dateTo);
            }

            $queryBuilder->addSelect('count(SuccessfulLogin.id) as countLogins');
            $queryBuilder->groupBy('User.id');

//            if(null !== $filters['store'])
//            {
//                $queryBuilder->andWhere('Employee.store = :store')->setParameter('store', $filters['store']);
//            }

            if($form->get('export')->isClicked())
            {
                $export = true;
                $dataExport = $queryBuilder->getQuery()->getResult();
            }
            if($form->get('export_all')->isClicked())
            {
                $export = true;
                $exportAll = true;


                $queryBuilder = $container->get('doctrine')->getRepository(User::class)->getQueryBuilder();
                $filters = $event->getFilters();
                $filtersSearch = $filters;

                $queryBuilder->innerJoin('User.employee', 'Employee');
                $queryBuilder->innerJoin('Employee.profile', 'Profile');

                if (null !== $filters['user'])
                {
                    $queryBuilder->andWhere('User.username LIKE :user OR User.email LIKE :user OR Profile.name LIKE :user')->setParameter('user', "%{$filters['user']}%");
                }

                $dataExport = $queryBuilder->getQuery()->getResult();
            }
        });

        $return = $grid->handle($request);

        if($export){
            $excelExporter = $this->get('aper_user.services.excel_exporter_store');

            if ($exportAll) {
                return $excelExporter->getResponseFromAllUsers(
                    $dataExport,
                    $filtersSearch,
                    'ListadoDeUsuarios.xls'
                );                
            } else {
                return $excelExporter->getResponseFromSuccessfulLogin(
                    $dataExport,
                    $filtersSearch,
                    'ListadoDeLogueos.xls'
                );                
            }

        }
        return $return;
    }


}