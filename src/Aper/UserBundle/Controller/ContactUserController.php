<?php

namespace Aper\UserBundle\Controller;
use Aper\StoreBundle\Entity\Store;
use Aper\ContactBundle\Entity\Contact;
use Aper\UserBundle\Entity\User;
use Aper\UserBundle\Entity\Profile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class ContactUserController extends Controller
{
     /**
      * Lists all Category entities.
      *
      * @Route("/contactuser/index")
      */

    public function indexAction(Request $request)
    {

        $fullName = $request->get('fullname');
        $area = $request->get('area');
        $store = $request->get('store');
        $province= $request->get('province');

        $em = $this->getDoctrine()->getManager();

        if($store){
            $store = $em->getRepository('StoreBundle:Store')->find($store);
            $pasarcine=$store;
        }
        else{$pasarcine = null;}

        $contact = $this->getDoctrine()->getRepository(User::class)->findByFilters($fullName, $area ,$pasarcine, $province);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $contact, $request->query->getInt('page' , 1),
            9
        );

        $stores = $this->getDoctrine()->getRepository(Store::class)->findBy([], ["location" => 'asc']);;


        if ($request->isXmlHttpRequest()) {
            $ajaxContact = [];

            foreach ($pagination as $new) {
                $ajaxContact[] = array(
                    'fullname' => $new->getEmployee()->getProfile()->getName(),
                    'area' => $new->getEmployee()->getArea(),
                    'jobposition' => $new->getEmployee()->getCategory(),
                    'corpmail' => $new->getEmployee()->getProfile()->getCorpMail(),
                    'officenumber' => $new->getEmployee()->getProfile()->getOfficePhone(),
                    'mobile' => $new->getEmployee()->getProfile()->getPhoneNumber(),
                    'mobile_area' => $new->getEmployee()->getProfile()->getPhoneAreaCode(),
                    'province' => $new->getEmployee()->getProfile()->getProvince(),
                    'store' => $new->getEmployee()->getStore()->getLocation(),
                    'avatar' => $new->getEmployee()->getProfile()->getAvatar() ? $new->getEmployee()->getProfile()->getAvatar()->getWebPath() : null
                );
            }

            $response = new  \Symfony\Component\HttpFoundation\Response(json_encode($ajaxContact));
            return $response;
        }

        // $provincias2 =  array(
        //     'Buenos Aires'     => 'Buenos Aires',
        //     'Capital Federal'  => 'Capital Federal',
        //     'Córdoba'          => 'Córdoba',
        //     'Mendoza'          => 'Mendoza',
        //     'Neuquén'          => 'Neuquén',
        //     'Salta'            => 'Salta',
        //     'Santa Fe'         => 'Santa Fe');

        $provincias = $this->getDoctrine()->getRepository(Profile::class)->findAllProvinces();

        $prov = array();
        foreach ($provincias as $value) {
            $val = $value['province'];
            $prov[$val] = $val;
        }

        $areas = $this->getDoctrine()->getRepository(User::class)->findAreas();

        return $this->render('UserBundle:Frontend/Contact:index.html.twig',
            array('stores'=>$stores, 'pagination' => $pagination, 'provincias'=>$prov, 'areas'=>$areas ));
    }
}
