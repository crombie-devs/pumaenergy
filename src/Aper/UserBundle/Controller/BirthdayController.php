<?php

namespace Aper\UserBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\HttpFoundation\Response;


class BirthdayController extends ContainerAwareCommand
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function configure()
    {
        $this
            ->setName('app:birthday_sending')
            ->setDescription('Envía por correo electronico salutaciones por cumpleaños')
        ;
    }


    public function executeAction()
    {

        $what = "Felicidades";
        $from = 'noreply@pumaenergy.com';

        $now = new \DateTime();
        $qb = $this->getContainer()->get('doctrine')->getRepository('UserBundle:User')->createQueryBuilder('User');
        $qb
            ->innerJoin('User.employee', 'Employee')
            ->innerJoin('Employee.profile', 'Profile')
            ->Where('User.enabled = true')
            ->andWhere('DAY(Profile.birthDay) = :dia')
            ->andWhere('MONTH(Profile.birthDay) = :mes')
            ->andWhere('User.email != :email')
            ->setParameter('dia',$now->format('d'))
            ->setParameter('mes',$now->format('m'))
            ->setParameter('email', 'example@example.com')
        ;


        $perfiles = $qb->getQuery()->getResult();

        $countProfiles =  count($perfiles);

        foreach ($perfiles as $user) {
            $fromName = $this->getContainer()->getParameter('email_from');
            $path  = $this->getContainer()->getParameter('absolute_url');
            $twig  = $this->getContainer()->get('twig');
            $body = $twig->render('@User/Frontend/email_birthday_notice.html.twig', array('user'=>$user, 'path'=>$path));
            $mail = $user->getEmail();
            $message = \Swift_Message::newInstance()
                ->setSubject('Felicidades')
                ->setFrom(array($from => $fromName))
                ->setTo($mail)
                ->setBody($body,'text/html');

            $mailer = $this->getContainer()->get('mailer');
            $mailer->send($message);

        }
        return Response::create('Mails de cumpleaños enviados para el día: '.$now->format('d/m/Y'). ': '.$countProfiles);
    }

}