<?php

namespace Aper\UserBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class BirthdayCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:birthday_sending')
            ->setDescription('Envía por correo electronico salutaciones por cumpleaños')
        ;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $what = "Felicidades";
        $from = 'noreply@pumaenergy.com';

        $now = new \DateTime();
        $qb = $this->getContainer()->get('doctrine')->getRepository('UserBundle:User')->createQueryBuilder('User');
        $qb
            ->innerJoin('User.employee', 'Employee')
            ->innerJoin('Employee.profile', 'Profile')
            ->Where('User.enabled = true')
            ->andWhere('DAY(Profile.birthDay) = :dia')
            ->andWhere('MONTH(Profile.birthDay) = :mes')
            ->andWhere('User.email != :email')
            ->setParameter('dia',$now->format('d'))
            ->setParameter('mes',$now->format('m'))
            ->setParameter('email', 'example@example.com')
        ;


        $perfiles = $qb->getQuery()->getResult();
        foreach ($perfiles as $user) {
                        $fromName = $this->getContainer()->getParameter('email_from');
                        $path  = $this->getContainer()->getParameter('absolute_url');
                        $twig  = $this->getContainer()->get('twig');
                        $body = $twig->render('@User/Frontend/email_birthday_notice.html.twig', array('user'=>$user, 'path'=>$path));
                        $mail = $user->getEmail();
                        $message = \Swift_Message::newInstance()
                            ->setSubject('Felicidades')
                            ->setFrom(array($from => $fromName))
                            ->setTo($mail)
                            ->setBody($body,'text/html');

                        $mailer = $this->getContainer()->get('mailer');
                        $mailer->send($message);


        }
    }

    /**
     * @var ContainerInterface
     */
    private $container;



}