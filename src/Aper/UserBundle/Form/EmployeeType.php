<?php

namespace Aper\UserBundle\Form;

use Aper\UserBundle\Entity\Employee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class EmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        ini_set('xdebug.max_nesting_level', 2000);
        $builder
            ->add('profile', ProfileType::class, [
                'show_legend' => false,
                'show_child_legend' => true,
                'label' => false,
            ])
            ->add('sons', CollectionType::class, [
                'type' => SonType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'widget_add_btn' => [
                    'label' => 'AGREGAR',
                    'icon' => '',
                    'attr' => [
                        'class' => 'btn btn-primary',
                        'title' => 'AGREGAR',
                    ],
                ],
                'options' => [
                    'widget_remove_btn' => [
                        'label' => 'ELIMINAR',
                        'attr' => [
                            'class' => 'btn btn-danger',
                            'title' => 'ELIMINAR',
                        ],
                        'icon' => '',
                        'wrapper_div' => [
                            'class' => 'span12',
                        ],
                    ],
                    'attr' => [
                        'class' => 'span12',
                    ],
                    'label' => false,
                    'error_bubbling' => false,
                    'show_child_legend' => false,
                ],
                'show_legend' => false,
                'show_child_legend' => false,
            ])

            ->add('sentimentalStatus', SentimentalStatusType::class)

            ->add('educations', CollectionType::class, [
                'type' => EducationType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'widget_add_btn' => [
                    'label' => 'AGREGAR',
                    'icon' => '',
                    'attr' => [
                        'class' => 'btn btn-primary',
                        'title' => 'AGREGAR',
                    ],
                ],
                'options' => [
                    'widget_remove_btn' => [
                        'label' => 'ELIMINAR',
                        'attr' => [
                            'class' => 'btn btn-danger',
                            'title' => 'ELIMINAR',
                        ],
                        'icon' => '',
                        'wrapper_div' => [
                            'class' => 'span12',
                        ],
                    ],
                    'attr' => [
                        'class' => 'span12',
                    ],
                    'label' => false,
                    'error_bubbling' => false,
                    'show_child_legend' => false,
                    'show_child_legend' => false,
                    'cascade_validation' => true,
                ],
                'show_legend' => false,
                'show_child_legend' => false,

            ])

            ->add('phoneNumber', TextType::class, [
                'property_path' => 'profile.phoneNumber',
            ])

            ->add('phoneAreaCode', TextType::class, [
                'property_path' => 'profile.phoneAreaCode',
            ])
            ->add('operator', ChoiceType::class, [
                'property_path' => 'profile.operator',
                'choices' => ['Nextel', 'Personal', 'Claro', 'Movistar', 'Tuenti'],
                'choice_label' => function ($choice) {
                    return $choice;
                },
                'choices_as_values' => true,
            ])
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'property_path' => 'user.plainPassword',
                'translation_domain' => 'web_factory_user',
                'first_options' => array('label' => 'web_factory_user.form.plain_password_first.label'),
                'second_options' => array('label' => 'web_factory_user.form.plain_password_second.label'),
            ))
            
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Employee::class,
                'show_legend' => false,
                'show_child_legend' => false,
                'label' => false,
            ]);
    }

    public function getBlockPrefix()
    {
        return 'employee';
    }
}
