<?php

namespace Aper\UserBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WebFactory\Bundle\UserBundle\Form\Type\ProfileType;

class UserType extends ProfileType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('employee', EmployeeType::class)
            // ->add('profile', ProfileType::class)
        ;


        parent::buildForm($builder, $options);
        $builder->remove('current_password');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => 'Aper\UserBundle\Entity\User',
                'attr' => ['novalidate' => 'novalidate'],
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'user';
    }
}
