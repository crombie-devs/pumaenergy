<?php

namespace Aper\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WebFactory\Bundle\GridBundle\Grid\Filter\GridFilterHandlerInterface;
use WebFactory\Bundle\GridBundle\Repository\FilterableGrid;

class UserFilterType extends AbstractType implements GridFilterHandlerInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', SearchType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }

    public function manipulateQuery(FilterableGrid $queryBuilder, array $filters)
    {
        if (null !== $filters['username']) {
            $queryBuilder->andWhere('user.username LIKE :name')->setParameter('name', "%{$filters['username']}%");
        }
    }
}
