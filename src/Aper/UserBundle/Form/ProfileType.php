<?php

namespace Aper\UserBundle\Form;

use Aper\UserBundle\Entity\Profile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           
            ->add('avatar', AvatarType::class, [
                'show_legend' => false,
                'show_child_legend' => false,
            ])
            ->add('birthDay', BirthdayType::class, [
                // 'widget' => 'single_text',
                // 'help_block' => 'Fecha de Nacimiento de tu hijo/a',
                // 'html5'=>false,
                // 'attr' => ['class' => 'js-datepicker'],
                'format' => 'ddMMyyy'
            ])
            
            
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Profile::class,
            ]);
    }

    public function getBlockPrefix()
    {
        return 'profile';
    }
}
