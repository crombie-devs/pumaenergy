<?php

namespace Aper\UserBundle\Form;

use Aper\UserBundle\Entity\SentimentalStatus;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SentimentalStatusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'help_block' => 'Fecha de Casamiento',
                'html5'=>false, 'attr' => ['class' => 'js-datepicker'],
                'format' => 'dd/MM/yyyy'
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => SentimentalStatus::class,
            ]);
    }

    public function getBlockPrefix()
    {
        return 'sentimental_status';
    }
}
