<?php

namespace Aper\UserBundle\Form;

use Aper\UserBundle\Entity\Avatar;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WebFactory\Bundle\FileBundle\Form\Type\FilePreviewType;

class AvatarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder


            ->add('file', FilePreviewType::class, [
                'label' => false,
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Avatar::class,
            ]);
    }

    public function getBlockPrefix()
    {
        return 'avatar';
    }
}
