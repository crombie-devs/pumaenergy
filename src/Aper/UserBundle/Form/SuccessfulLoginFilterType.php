<?php


namespace Aper\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SuccessfulLoginFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $fromDefault = new \DateTime('-1 week');
        $fromDefault->setTime(0,0,0);
        $toDefault = new \DateTime();
        $toDefault->setTime(23,59,59);

        $builder
            ->add('user', SearchType::class)
//            ->add('store', EntityType::class, [
//                'class' => 'StoreBundle:Store',
//                'query_builder' => function (EntityRepository $er) {
//                    return $er->createQueryBuilder('t')
//                        ->orderBy('t.code', 'ASC');
//                },
//                'placeholder'  => 'Seleccione una sala para este empleado',
//            ])
            ->add('dateFrom', DateType::class,
                array('label'  => 'Desde',
                      'widget' => 'single_text',
                      'data'   => $fromDefault
                ))
            ->add('dateTo', DateType::class,
                array('label'  => 'Hasta',
                      'widget' => 'single_text',
                      'data'   => $toDefault
                    ))
            ->add('export', SubmitType::class, array('label' => 'Exportar'))
            ->add('export_all', SubmitType::class, array('label' => 'Exportar Todos'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }

}