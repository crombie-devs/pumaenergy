<?php

namespace Aper\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use WebFactory\Bundle\UserBundle\Form\Type\AccountType as BaseAccountType;

class AccountType extends BaseAccountType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->remove('username');
    }
}
