<?php

namespace Aper\UserBundle\Form;

use Aper\BenefitBundle\BenefitRepository;
use Aper\UserBundle\Entity\Employee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddBenefitToEmployee extends AbstractType
{
    /** @var BenefitRepository $benefitRepository */
    private $benefitRepository;

    /** @var Employee $employee */
    private $employee;

    public function __construct(BenefitRepository $benefitRepository, Employee $employee)
    {
        $this->benefitRepository = $benefitRepository;
        $this->employee = $employee;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $benefits = $this->benefitRepository->findByRolesAllowed($this->employee->getUser()->getRoles());

        $builder
            ->add('benefits', null, [
                'choices' => $benefits,
                'expanded' => true,
                'multiple' => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Employee::class,
            'attr' => [
                'novalidate' => 'novalidate'
            ]
        ));
    }

    public function getName()
    {
        return 'add_benefit_to_employee';
    }

}