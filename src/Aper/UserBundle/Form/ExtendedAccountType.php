<?php

namespace Aper\UserBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use WebFactory\Bundle\UserBundle\Form\Type\AccountType as BaseAccountType;

class ExtendedAccountType extends BaseAccountType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phoneNumber', TextType::class, [
                'property_path' => 'employee.profile.phoneNumber',
            ])
            ->add('phoneAreaCode', TextType::class, [
                'property_path' => 'employee.profile.phoneAreaCode',
            ])
            ->add('operator', ChoiceType::class, [
                'property_path' => 'employee.profile.operator',
                'choices' => ['Nextel', 'Personal', 'Claro', 'Movistar', 'Tuenti'],
                'choice_label' => function ($choice) {
                    return $choice;
                },
                'choices_as_values' => true,
            ])
            

        ;
        parent::buildForm($builder, $options);
        $builder->remove('username');
    }
}
