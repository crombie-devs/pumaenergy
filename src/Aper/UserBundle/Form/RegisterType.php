<?php

namespace Aper\UserBundle\Form;

use Doctrine\ORM\Query\Expr\Select;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\File;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $arr = array();
        foreach ($options['data']['provincias'] as $provincia)
        {
            $arr[$provincia->getId()] = $provincia->getNombre();
        }

        $builder
            ->add('provincia', ChoiceType::class, [
                'choices' => $arr,
                'empty_value' => 'Provincia',
                'required' => true
            ])
//            ->add('localidad', TextType::class)
//            ->add('store', TextType::class)
            ->add('nombre', TextType::class)
            ->add('apellido', TextType::class)
            ->add('dni', TextType::class)
            ->add('email', EmailType::class)
            ->add('confirmarEmail', EmailType::class)
            ->add('terminosCondiciones', CheckboxType::class, [
                'required' => true,
                'label'=> 'Acepto terminos y condiciones'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => null
            ]);
    }

    public function getBlockPrefix()
    {
        return 'register';
    }
}