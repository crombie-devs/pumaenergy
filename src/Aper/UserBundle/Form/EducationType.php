<?php

namespace Aper\UserBundle\Form;

use Aper\UserBundle\Entity\Education;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EducationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('level', ChoiceType::class, [
                'choices' => ['Terciario', 'Universitario'],
                'choice_label' => function ($choice) {
                    return $choice;
                },
                'choices_as_values' => true,
            ])
            ->add('institution')
            ->add('career')
            ->add('startAt', DateType::class, [
                'widget' => 'single_text', 'html5'=>false, 'attr' => ['class' => 'js-datepicker'],'format' => 'dd/MM/yyyy'
            ])
            ->add('finishAt', DateType::class, [
                'widget' => 'single_text', 'html5'=>false, 'attr' => ['class' => 'js-datepicker'],'format' => 'dd/MM/yyyy'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Education::class,
            ]);
    }

    public function getBlockPrefix()
    {
        return 'education';
    }
}
