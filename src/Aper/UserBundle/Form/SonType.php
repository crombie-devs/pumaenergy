<?php

namespace Aper\UserBundle\Form;

use Aper\UserBundle\Entity\Son;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name', TextType::class, array('help_block' => 'Nombre de tu hijo/a'))
            ->add('birthDay', BirthdayType::class, [
                'widget' => 'single_text',
                'help_block' => 'Fecha de Nacimiento de tu hijo/a',
                'html5'=>false,
                'attr' => ['class' => 'js-datepicker'],
                'format' => 'dd/MM/yyy'
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Son::class,
                'error_bubbling'=>false,
                'compound' => true,

            ]);
    }

    public function getBlockPrefix()
    {
        return 'son';
    }



}
