<?php

namespace Aper\UserBundle\Form\Backend;

use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WebFactory\Bundle\UserBundle\Form\Type\EditUserType;

class EditUserBackEndType extends EditUserType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('employee', EmployeeBackendType::class)
            ->add('username', TextType::class)
            ->add('email', EmailType::class)
        ;

        parent::buildForm($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => 'Aper\UserBundle\Entity\User',
                'attr' => ['novalidate' => 'novalidate'],
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'user';
    }
}
