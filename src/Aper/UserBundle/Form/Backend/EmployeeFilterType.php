<?php

namespace Aper\UserBundle\Form\Backend;

use Aper\StoreBundle\Entity\Store;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code')
            ->add('name')
            ->add('store', EntityType::class, [
                'required' => false,
                'class' => Store::class,
            ])
            ->add('username')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null
        ]);
    }

    public function getBlockPrefix()
    {
        return 'employee_filter';
    }

    public function manipulateQuery(QueryBuilder $queryBuilder, array $filters)
    {
        $queryBuilder->addSelect('Employee');
        $queryBuilder->addSelect('Profile');
        $queryBuilder->innerJoin('User.employee', 'Employee');
        $queryBuilder->innerJoin('Employee.profile', 'Profile');
        if (!empty($filters['code'])) {
            $queryBuilder->andWhere('Employee.code = :code')->setParameter('code', $filters['code']);
        }
        if (!empty($filters['name'])) {
            $queryBuilder->andWhere('Profile.name LIKE :name')->setParameter('name', "%{$filters['name']}%");
        }
        if (!empty($filters['store'])) {
            $queryBuilder->andWhere('Employee.store = :store')->setParameter('store', $filters['store']);
        }
        if (!empty($filters['username'])) {
            $queryBuilder->andWhere('User.username = :username')->setParameter('username', $filters['username']);
        }
    }

}
