<?php

namespace Aper\UserBundle\Form\Backend;

use Aper\UserBundle\Entity\Profile;

use Aper\UserBundle\Form\AvatarType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ProfileBackendType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class)
            ->add('sex', ChoiceType::class, [
                'choices' => ['Varón' => 'Varón', 'Mujer' => 'Mujer'],
                'placeholder' => 'Seleccione Género',
                'required' => false,
            ])
            ->add('startAt', DateType::class, [
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('departureAt', DateType::class, [
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('birthDay', DateType::class, [
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('personalText',TextType::class, [
                'required' => false,
            ])
            ->add('phoneNumber',TextType::class, [
                'required' => false,
            ])
            ->add('phoneAreaCode',TextType::class, [
                'required' => false,
            ])
            ->add('operator',  ChoiceType::class, array(
                'choices' => array(
                    'Movistar' => 'Movistar',
                    'Personal' => 'Personal', 
                    'Claro' => 'Claro', 
                    'Nextel' => 'Nextel', 
                    'Tuenti' => 'Tuenti'),
                'placeholder' => 'Seleccione un operador de telefonía',
                'required' => false
            ))
            ->add('CorpMail',TextType::class, [
                'label' => 'Mail Corporativo',
                'required' => false,
            ])
            ->add('officePhone',TextType::class, [
                'label' => 'Tel Oficina',
                'required' => false,
            ])
            ->add('province', ChoiceType::class, array('choices' => array(
                    'Buenos Aires'     => 'Buenos Aires',
                    'Capital Federal'  => 'Capital Federal',
                    'Córdoba'          => 'Córdoba',
                    'Mendoza'          => 'Mendoza',
                    'Neuquen'          => 'Neuquen',
                    'Salta'            => 'Salta',
                    'Santa Fe'         => 'Santa Fe'),
                'placeholder' => 'Seleccione una provincia',
                'required' => false))

            ->add('avatar', AvatarType::class)
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Profile::class,
            ]);
    }

    public function getBlockPrefix()
    {
        return 'profile';
    }
}
