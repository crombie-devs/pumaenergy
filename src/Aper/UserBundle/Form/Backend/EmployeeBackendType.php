<?php

namespace Aper\UserBundle\Form\Backend;

use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Form\AchievementType;
use Aper\UserBundle\Form\EducationType;
use Aper\UserBundle\Form\SentimentalStatusType;
use Aper\UserBundle\Form\SonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class EmployeeBackendType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        ini_set('xdebug.max_nesting_level', 2000);
        $builder

            ->add('store', EntityType::class, [
                'class' => 'StoreBundle:Store',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.code', 'ASC');
                },
                'placeholder'  => 'Seleccione una estación para este empleado',
            ])
            ->add('code', TextType::class)
            ->add('category', TextType::class)
            ->add('area', TextType::class, [
                'required' => false,
            ])
            ->add('agreement', TextType::class, [
                'required' => false,
            ])
            ->add('idPixelnetUsr', TextType::class, [
                'required' => false,
            ])
            ->add('stores', TextType::class, [
                'required' => false,
            ])

            ->add('subRol', EntityType::class, array(
                'class' => 'UserBundle:SubRol',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('sr')
                                    ->orderBy('sr.id', 'ASC');
                },
                'expanded' => false,
                'multiple' => false,
            ))

            ->add('profile', ProfileBackendType::class)
            ->add('sentimentalStatus', SentimentalStatusType::class)
            ->add('achievements', CollectionType::class, [
                'type' => AchievementType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'widget_add_btn' => [
                    'label' => 'Agregar Logro',
                    'icon' => '',
                    'attr' => [
                        'class' => 'btn btn-primary',
                        'title' => 'AGREGAR',
                    ],
                ],
                'options' => [
                    'widget_remove_btn' => [
                        'label' => 'ELIMINAR',
                        'attr' => [
                            'class' => 'btn btn-danger',
                            'title' => 'ELIMINAR',
                        ],
                        'icon' => '',
                        'wrapper_div' => [
                            'class' => 'span12',
                        ],
                    ],
                    'attr' => [
                        'class' => 'span12',
                    ],
                    'label' => false,
                    'error_bubbling' => false,
                    'show_child_legend' => false,
                ],
                'show_legend' => false,
                'show_child_legend' => false,
            ])
            ->add('sons', CollectionType::class, [
                'type' => SonType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'widget_add_btn' => [
                    'label' => 'Agregar hijo',
                    'icon' => '',
                    'attr' => [
                        'class' => 'btn btn-primary',
                        'title' => 'AGREGAR',
                    ],
                ],
                'options' => [
                    'widget_remove_btn' => [
                        'label' => 'ELIMINAR',
                        'attr' => [
                            'class' => 'btn btn-danger',
                            'title' => 'ELIMINAR',
                        ],
                        'icon' => '',
                        'wrapper_div' => [
                            'class' => 'span12',
                        ],
                    ],
                    'attr' => [
                        'class' => 'span12',
                    ],
                    'label' => false,
                    'show_child_legend' => false,
                ],
                'show_legend' => false,
                'show_child_legend' => false,
                'error_bubbling' => false,
            ])
            ->add('educations', CollectionType::class, [
                'type' => EducationType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'widget_add_btn' => [
                    'label' => 'Agregar estudio',
                    'icon' => '',
                    'attr' => [
                        'class' => 'btn btn-primary',
                        'title' => 'AGREGAR',
                    ],
                ],
                'options' => [
                    'widget_remove_btn' => [
                        'label' => 'ELIMINAR',
                        'attr' => [
                            'class' => 'btn btn-danger',
                            'title' => 'ELIMINAR',
                        ],
                        'icon' => '',
                        'wrapper_div' => [
                            'class' => 'span12',
                        ],
                    ],
                    'attr' => [
                        'class' => 'span12',
                    ],
                    'label' => false,
                    'error_bubbling' => false,
                    'show_child_legend' => false,
                ],
                'show_legend' => false,
                'show_child_legend' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Employee::class,

            ]);
    }

    public function getBlockPrefix()
    {
        return 'employee';
    }
}
