<?php

namespace Aper\UserBundle\Form\Backend;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WebFactory\Bundle\UserBundle\Form\Type\NewUserType;

class NewUserBackEndType extends NewUserType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('username', TextType::class)
//            ->add('email', EmailType::class)
            ->add('employee', EmployeeBackendType::class)
        ;

        parent::buildForm($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => 'Aper\UserBundle\Entity\User',
                'attr' => ['novalidate' => 'novalidate'],
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'user';
    }

}
