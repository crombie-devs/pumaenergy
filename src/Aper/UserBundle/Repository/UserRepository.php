<?php

namespace Aper\UserBundle\Repository;

use Aper\UserBundle\Entity\User;
use Aper\StoreBundle\Entity\Store;
use Aper\UserBundle\Model\Role;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use WebFactory\Bundle\UserBundle\Repository\UniqueValidationFinderInterface;

/**
 * Class UserRepository.
 */
class UserRepository extends EntityRepository implements UniqueValidationFinderInterface
{
    /**
     * @param $params
     *
     * @return \Aper\UserBundle\Entity\User
     */
    public function findForUniqueValidation(array $params)
    {
        return $this->getEntityManager()->getRepository('AperUserBundle:User')->findBy($params);
    }

    /**
     * @param User $user
     * @param bool $sameStore
     * @param int $limit
     * @return User[]|Collection
     */
    public function findNextBirthDays(User $user, $sameStore, $limit)
    {
        $qb = $this->createQueryBuilder('User');
        $qb
            ->innerJoin('User.employee', 'Employee')
            ->innerJoin('Employee.profile', 'Profile')
            ->where('User != :user')->setParameter('user', $user)
            ->andWhere('User.enabled = true')
            ->andWhere('Employee.store ' . ($sameStore ? '=' : '!=') . ' :store')->setParameter('store', $user->getEmployee()->getStore())
            ->setMaxResults($limit)
            ->orderBy('MONTH(Profile.birthDay)',  'ASC')
            ->addOrderBy('DAY(Profile.birthDay)',  'ASC');

        $dateConditions = [];
        foreach (range(0, 80) as $i) {
            $from = new \DateTime("-{$i} year -1 day");
            $to = new \DateTime("+15 day -{$i} year");
            $dateConditions[] = "(Profile.birthDay BETWEEN :from{$i} AND :to{$i})";
            $qb->setParameter("from{$i}", $from)->setParameter("to{$i}", $to)
            ;
        }

        $qb->andWhere(implode(' OR ', $dateConditions));

        $users = new ArrayCollection($qb->getQuery()->getResult());

        return new ArrayCollection(iterator_to_array($users));;
    }

    /**
     * @return array
     */
    public function findAllRRHH()
    {
        $qb = $this->createQueryBuilder('User');
        $qb
            ->where('User.roles LIKE :role')->setParameter('role', "%" . Role::ROLE_MANAGER_GLOBAL . "%")
            ->andWhere('User.enabled = true')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $username
     * @return User
     */
    public function findOneByUsername($username)
    {
        $qb = $this->createQueryBuilder('User');
        $qb
            ->addSelect('Employee, Profile, Son, Avatar, SentimentalStatus, Education')
            ->innerJoin('User.employee', 'Employee')
            ->innerJoin('Employee.profile', 'Profile')
            ->leftJoin('Profile.avatar', 'Avatar')
            ->leftJoin('Employee.sentimentalStatus', 'SentimentalStatus')
            ->leftJoin('Employee.educations', 'Education')
            ->leftJoin('Employee.sons', 'Son')
            ->where('User.username = :username')->setParameter('username', $username)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $username
     * @return User
     */
    public function findOneByEmail($email)
    {
        $qb = $this->createQueryBuilder('User');
        $qb
            ->addSelect('Employee, Profile, Son, Avatar, SentimentalStatus, Education')
            ->innerJoin('User.employee', 'Employee')
            ->innerJoin('Employee.profile', 'Profile')
            ->leftJoin('Profile.avatar', 'Avatar')
            ->leftJoin('Employee.sentimentalStatus', 'SentimentalStatus')
            ->leftJoin('Employee.educations', 'Education')
            ->leftJoin('Employee.sons', 'Son')
            ->where('User.email = :email')->setParameter('email', $email)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }


    /**
     * @return array
     */
    public function findUsersByProfile($profile)
    {


        $qb = $this->createQueryBuilder('User');
        $qb
            ->where('User.roles LIKE :role')->setParameter('role', "%" . $profile . "%")
            ->andWhere('User.enabled = true')
            ->andwhere ('User.email not like :filtro')
            ->setParameter('filtro','%@example.com')

        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $username
     * @return array
     */
    public function findUsersByStore($store)
    {
        $qb = $this->createQueryBuilder('User');
        $qb
            ->addSelect('Employee')
            ->innerJoin('User.employee', 'Employee')
            ->where('Employee.store = :store')->setParameter('store', $store)
            ->andwhere ('User.email not like :filtro')
            ->setParameter('filtro','%@example.com')
        ;

        return $qb->getQuery()->getResult();
    }


    /**
     * @return array
     */
    public function findUsersMails()
    {

        $qb = $this->createQueryBuilder('User');
        $qb->setMaxResults(1200)
            // ->where ('User.email not like :filtro')
            // ->setParameter('filtro','%@example.com')

        ;

        return $qb->getQuery()->getResult();
    }
    
    //VIENE DE CONTACT
     /**
     *
     * @return Type
     */
    public function findByFilters
    ( $fullName = null,  $area = null, Store $store = null, $province=null)
    {
        $queryBuilder = $this->createQueryBuilder('User')
            ->innerJoin('User.employee', 'Employee')
            ->innerJoin('Employee.profile', 'Profile')
            ->innerJoin('Employee.store', 'Store')
            ->leftJoin('Profile.avatar', 'Avatar')
            ->where('User.enabled = 1')
            ->andWhere('User.roles NOT LIKE \'%ROLE_ADMIN%\'') 
            ->orderBy('Profile.name', 'ASC');

        if ($fullName) {
            $queryBuilder->andWhere('Profile.name like :fullname')
                ->setParameter('fullname', '%'.$fullName.'%');
        }

        if ($area) {
            $queryBuilder->andWhere('Employee.area = :area')
                ->setParameter('area', $area);
        }

        if ($store) {
            $queryBuilder->andWhere('Store.id =:store')
                ->setParameter('store', $store->getId());
        }

        if ($province) {
            $queryBuilder->andWhere('Profile.province like :province')
                ->setParameter('province', '%'.$province.'%');
        }

        return $queryBuilder;
    }

    //BUSCA AREAS
    public function findAreas()
    {
        $queryBuilder = $this->createQueryBuilder('user')
            ->Select('DISTINCT employee.area')
            ->innerJoin('user.employee', 'employee')
            ->where('user.enabled = 1')
            ->andWhere('user.roles NOT LIKE \'%ROLE_ADMIN%\'') 
            ->orderBy('employee.area', 'ASC');

        return new ArrayCollection($queryBuilder->getQuery()->getResult());
    }


    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @param Store|null $store
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function findNextBirthday(\DateTime $from, \DateTime $to, Store $store = null, $limit = null, $offset = null)
    {
        $qb = $this->createQueryBuilder('User');
        $qb
            ->addSelect('Employee')
            ->addSelect('Profile')
            ->addSelect('SentimentalStatus')
            ->innerJoin('User.employee', 'Employee')
            ->innerJoin('Employee.profile', 'Profile')
            ->leftJoin('Employee.sentimentalStatus', 'SentimentalStatus')
            ->andWhere('User.enabled = true')
            ->andWhere("User.roles LIKE '%ROLE_EMPLOYEE%'")
            ->orderBy('MONTH(Profile.birthDay)', 'ASC')
            ->addOrderBy('DAY(Profile.birthDay)', 'ASC')
        ;

        if ($store) {
            $qb->andWhere('Employee.store = :store')
                ->setParameter('store', $store);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        if (null !== $offset) {
            $qb->setFirstResult($offset);
        }

        $qb->andWhere("DATE_FORMAT(Profile.birthDay,'" . date('Y') . "-%m-%d') BETWEEN :from AND :to")
            ->setParameter('from', $from)
            ->setParameter('to', $to)
        ;

        return $qb->getQuery()->getResult();
    }

    public function getQueryBuilder()
    {
        return $this->createQueryBuilder('User')->orderBy('User.id', 'DESC');
    }

    public function getManagers($storeId){
        return $this->createQueryBuilder('User')
            ->innerJoin('User.employee', 'Employee')
            ->innerJoin('Employee.store', 'Store')
            ->where('User.roles LIKE :role')->setParameter('role', "%" . Role::ROLE_STORE_MANAGER . "%")
            ->andWhere('User.enabled = true')
            ->andWhere('Store.id = :id')->setParameter('id', $storeId)
            ->getQuery()->getResult();
    }
}