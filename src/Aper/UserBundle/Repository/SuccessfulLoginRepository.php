<?php


namespace Aper\UserBundle\Repository;


use Doctrine\ORM\EntityRepository;

class SuccessfulLoginRepository extends EntityRepository
{

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->createQueryBuilder('SuccessfulLogin')->orderBy('SuccessfulLogin.id', 'DESC');
    }

}