<?php

namespace Aper\UserBundle\DataFixtures\ORM;

use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\Profile;
use Aper\UserBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmployeeUserFixture extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $this->container->get('web_factory_user.user_context')->setContextName('frontend'); // Fix

        $user = new User();
        $user->setUsername('employee');
        $user->setEmail('example@example.com');
        $user->setPlainPassword('employee');
        $user->setEnabled(true);
        $user->addRole('ROLE_EMPLOYEE');

        $employee = new Employee();
        $employee->setCategory('category');
        $employee->setCode('123');
        $employee->setStore($this->getReference('store'));
        $profile = new Profile();
        $profile->setName('name');
        $employee->setProfile($profile);
        $user->setEmployee($employee);

        $employee->addBenefit($this->getReference('benefit-employee'));

        $manager->persist($user);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }
}
