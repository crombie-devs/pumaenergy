<?php

namespace Aper\UserBundle\Model;

class Role
{
    const ROLE_EMPLOYEE = 'ROLE_EMPLOYEE';
    const ROLE_HEAD_OFFICE = 'ROLE_HEAD_OFFICE';
    const ROLE_MANAGER = 'ROLE_MANAGER';
    const ROLE_MANAGER_GLOBAL = 'ROLE_MANAGER_GLOBAL';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_ADMIN_CONSULTA = 'ROLE_ADMIN_CONSULTA';
    const ROLE_ADMIN_CAPACITACION = 'ROLE_ADMIN_CAPACITACION';
    // const ROLE_STORE_MANAGER = 'ROLE_STORE_MANAGER';

    /**
     * @return array
     */
    public static function getChoices()
    {
        $roles = [
            static::ROLE_HEAD_OFFICE => static::ROLE_HEAD_OFFICE,
            static::ROLE_MANAGER => static::ROLE_MANAGER,
            static::ROLE_EMPLOYEE => static::ROLE_EMPLOYEE,
            static::ROLE_MANAGER_GLOBAL => static::ROLE_MANAGER_GLOBAL,
            static::ROLE_ADMIN => static::ROLE_ADMIN,
            static::ROLE_ADMIN_CONSULTA => static::ROLE_ADMIN_CONSULTA,
            static::ROLE_ADMIN_CAPACITACION => static::ROLE_ADMIN_CAPACITACION,
            // static::ROLE_STORE_MANAGER => static::ROLE_STORE_MANAGER,
        ];

        return $roles;
    }

    /**
     * @param $role
     * @return string
     */
    public static function convertRole($role)
    {
        if($role == "")
            return self::ROLE_EMPLOYEE;
        
       $roles = [
            'PERFIL 1' => self::ROLE_HEAD_OFFICE,
            'PERFIL 2' => self::ROLE_MANAGER,
            'PERFIL 3' => self::ROLE_EMPLOYEE,
            'PERFIL 4' => self::ROLE_MANAGER_GLOBAL,
            'PERFIL 5' => self::ROLE_ADMIN,
            'PERFIL 6' => self::ROLE_ADMIN_CONSULTA,
            'PERFIL 7' => self::ROLE_ADMIN_CAPACITACION,
			'REPRESENTANTE COMERCIAL' => self::ROLE_HEAD_OFFICE,
			'OPERADOR/A' => self::ROLE_MANAGER,
			'JEFE/A EESS' => self::ROLE_MANAGER,
			'ENCARGADO/A' => self::ROLE_MANAGER,
			'GERENTE EESS' => self::ROLE_MANAGER,
			'DUEÑO/A' => self::ROLE_MANAGER,
			'EMPLEADO/A ADMINISTRATIVO' => self::ROLE_MANAGER,
			'PLAYERO/A' => self::ROLE_EMPLOYEE,
			'CAJERO/A DE TIENDA' => self::ROLE_EMPLOYEE,
			'OPERARIO/A DE SERVICIOS' => self::ROLE_EMPLOYEE,
			'OTROS PUESTOS' => self::ROLE_EMPLOYEE,
			'ADMINISTRACION / GERENTES' => self::ROLE_MANAGER_GLOBAL,
			'ADMIN BACKEND' => self::ROLE_ADMIN,
			'ADMIN DE CONSULTA' => self::ROLE_ADMIN_CONSULTA,
			'ADMIN DE CAPACITACIONES ' => self::ROLE_ADMIN_CAPACITACION,
			
        ];

        return $roles[$role];
    }
}