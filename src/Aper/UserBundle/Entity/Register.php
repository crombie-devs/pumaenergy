<?php

namespace Aper\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Aper\UserBundle\Repository\RegisterRepository")
 * @ORM\Table(name="register", indexes={@ORM\Index(name="fk_store", columns={"store"})})
 */
class Register
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="provincia", type="integer")
     */
    private $provincia;

    /**
     * @ORM\Column(name="localidad", type="integer")
     */
    private $localidad;

    /**
     * @ORM\Column(name="store", type="integer")
     */
    private $store;

    /**
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @ORM\Column(name="apellido", type="string", length=100)
     */
    private $apellido;

    /**
     * @ORM\Column(name="dni", type="string", length=15)
     */
    private $dni;

    /**
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @ORM\Column(name="role", type="string", length=100)
     */
    private $role;

    /**
     * @ORM\Column(name="category", type="string", length=100)
     */
    private $category;

    /**
     * @ORM\Column(name="subRol", type="string", length=100)
     */
    private $subRol;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setProvincia($provincia){
        $this->provincia = $provincia;
    }

    public function setLocalidad($localidad){
        $this->localidad = $localidad;
    }

    public function setStore($store){
        $this->store = $store;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function setApellido($apellido){
        $this->apellido = $apellido;
    }

    public function setDni($dni){
        $this->dni = $dni;
    }

    public function setEmail($email){
        $this->email = $email;
    }
    public function setRole($role){
        $this->role = $role;
    }
    public function setCategory($category){
        $this->category = $category;
    }

    public function getProvincia(){
        return $this->provincia;
    }

    public function getLocalidad(){
        return $this->localidad;
    }

    public function getStore(){
        return $this->store;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function getApellido(){
        return $this->apellido;
    }

    public function getDni(){
        return $this->dni;
    }

    public function getEmail(){
        return $this->email;
    }
    public function getRole(){
        return $this->role;
    }
    public function getCategory(){
        return $this->category;
    }

    /**
     * Set subRol
     *
     * @param string $subRol
     *
     * @return Register
     */
    public function setSubRol($subRol)
    {
        $this->subRol = $subRol;

        return $this;
    }

    /**
     * Get subRol
     *
     * @return string
     */
    public function getSubRol()
    {
        return $this->subRol;
    }
}
