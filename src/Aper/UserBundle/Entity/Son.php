<?php

namespace Aper\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Son.
 *
 * @ORM\Table(name="sons")
 * @ORM\Entity(repositoryClass="Aper\UserBundle\Repository\SonRepository")
 */
class Son
{
    /**
     * @ORM\ManyToOne(targetEntity="Employee", inversedBy="sons")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     */
    protected $employee;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     * @Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $name;
    /**
     * @var \datetime
     *
     * @ORM\Column(name="birth_day", type="date", nullable=true)
     * @Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $birthDay;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Son
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get birthday.
     *
     * @return \DateTime
     */
    public function getBirthDay()
    {
        return $this->birthDay;
    }

    /**
     * Set birthday.
     *
     * @param \DateTime $birthDay
     *
     * @return Son
     */
    public function setBirthDay(\DateTime $birthDay = null)
    {
        $this->birthDay = $birthDay;

        return $this;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param Employee $employee
     */
    public function setEmployee(Employee $employee)
    {
        $this->employee = $employee;
    }
}
