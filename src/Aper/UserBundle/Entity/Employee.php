<?php

namespace Aper\UserBundle\Entity;

use Aper\BenefitBundle\Entity\Benefit;
use Aper\PlanCarreraBundle\Entity\CareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\BenefitBundle\Model\Category;
use Aper\IncentiveBundle\Entity\EmployeeGoal;
use Aper\IncentiveBundle\Entity\EmployeeRanking;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Aper\StoreBundle\Entity\Store;
use Aper\CodeBundle\Entity\Code;

/**
 * User.
 *
 * @ORM\Table(name="employees")
 * @ORM\Entity(repositoryClass="Aper\UserBundle\Repository\EmployeeRepository")
 */
class Employee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Goal[]|Collection
     * @ORM\OneToMany(targetEntity="Aper\IncentiveBundle\Entity\EmployeeGoal", mappedBy="employee", cascade={"all"}, orphanRemoval=true)
     */
    private $goals;

    /**
     * @var Ranking[]|Collection
     * @ORM\OneToMany(targetEntity="Aper\IncentiveBundle\Entity\EmployeeRanking", mappedBy="employee", cascade={"all"}, orphanRemoval=true)
     */
    private $rankings;

    /**
     * @var ExtraPointsStore[]|Collection
     * @ORM\oneToMany(targetEntity="Aper\IncentiveBundle\Entity\ExtraPointsEmployee", mappedBy="employee", cascade={"all"}, orphanRemoval=true)
     */
    private $extraPoints;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User", inversedBy="employee")
     */
    protected $user;

    /**
     * @var Profile
     * @ORM\OneToOne(targetEntity="Profile", mappedBy="employee", cascade={"all"})
     * @Assert\Valid()
     */
    protected $profile;

    /**
     * @var Employee
     * @ORM\ManyToOne(targetEntity="Employee", inversedBy="employees")
     */
    protected $boss;

    /**
     * @var Employee[]
     * @ORM\OneToMany(targetEntity="Employee", mappedBy="boss")
     */
    protected $employees;

    /**
     * @var Store
     *
     * @ORM\ManyToOne(targetEntity="Aper\StoreBundle\Entity\Store", inversedBy="employees")
     * @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     */
    protected $store;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=100, nullable=true)
     *  @Assert\NotBlank(groups={"Default", "Profile"})
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=100)
     * @Assert\NotBlank(groups={"Default", "Register"})
     */
    private $code;

    /**
     * @var Son[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Aper\UserBundle\Entity\Son", mappedBy="employee", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $sons;

    /**
     * @var SentimentalStatus
     * @ORM\OneToOne(targetEntity="Aper\UserBundle\Entity\SentimentalStatus", mappedBy="employee", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $sentimentalStatus;

    /**
     * @var Education[]
     * @ORM\OneToMany(targetEntity="Education", mappedBy="employee", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    protected $educations;

    /**
     * @var Achievement[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Aper\UserBundle\Entity\Achievement", mappedBy="employee", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $achievements;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=100,nullable=true)
     * //@Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $area;

    /**
     * @var string
     *
     * @ORM\Column(name="agreement", type="string", length=100,nullable=true)
     * //@Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $agreement;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Aper\BenefitBundle\Entity\Benefit")
     * @ORM\JoinTable(name="employees_benefits",
     *      joinColumns={@ORM\JoinColumn(name="employee_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="benefit_id", referencedColumnName="id")}
     * )
     * @Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $benefits;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=true)
     */
    private $deleted=false;

    /**
     * @var EmployeeCareerPlan[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan", mappedBy="user")
     */
    private $careerPlans;

    /**
     * @var Employee
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\Employee")
     */
    private $trainer;

    /**
     * @var CareerPlan
     * @ORM\ManyToOne(targetEntity="Aper\PlanCarreraBundle\Entity\CareerPlan")
     */
    private $currentPlan;

    /**
     * @ORM\OneToMany(targetEntity="Aper\CodeBundle\Entity\Code", mappedBy="employee")
     */
    protected $codesQr;

    /**
     * @ORM\Column(name="id_pixelnet_usr", type="integer", nullable=true)
     */
    private $idPixelnetUsr;

    /**
     * @var string
     *
     * @ORM\Column(name="stores", type="text", nullable=true)
     */
    private $stores;

    /**
     * @ORM\ManyToOne(targetEntity="SubRol", inversedBy="employee")
     * @ORM\JoinColumn(name="subRol_id", referencedColumnName="id")
     */
    protected $subRol;

    /**
     * Employee constructor.
     */
    public function __construct()
    {
        $this->sons = new ArrayCollection();
        $this->benefits = new ArrayCollection();
        $this->educations = new ArrayCollection();
        $this->achievements = new ArrayCollection();
        $this->goals = new ArrayCollection();
        $this->rankings = new ArrayCollection();
        $this->careerPlans = new ArrayCollection();
        $this->codesQr = new ArrayCollection();
    }

    /**
    * Add Code
    * @param Code $code
    */
    public function addCodesQr( Code $code ) 
    {
        $this->codesQr[] = $code;
    }

    /**
    * Get Code
    * @param ArrayCollection $codesQr
    */
    public function getCodesQr() 
    {
      return $this->codesQr;
    }

    /**
     * @param Code $codesQr
     */
    public function removeCode(Code $codesQr)
    {
        $this->codesQr->removeElement($codesQr);
    }

    /**
     * @return Ranking[]|Collection
     */
    public function getRankings()
    {
        return $this->rankings;
    }

    /**
     * @param Ranking[]|Collection $rankings
     */
    public function setRankings($rankings)
    {
        $this->rankings = $rankings;
    }

    public function setIdPixelnetUsr($idPixelnetUsr){
        $this->idPixelnetUsr = $idPixelnetUsr;
        return $this;
    }

    public function getIdPixelnetUsr(){
        return $this->idPixelnetUsr;
    }

    public function setStores($stores){
        $this->stores = $stores;
        return $this;
    }

    public function getStores(){
        return $this->stores;
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Goal[]|Collection
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * @param Goal[]|Collection $goals
     */
    public function setGoals($goals)
    {
        $this->goals = $goals;
    }

    /**
     * Set category.
     *
     * @param string $category
     *
     * @return Employee
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Employee
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set user.
     *
     * @param User $user
     *
     * @return Employee
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set store.
     *
     * @param Store $store
     *
     * @return Employee
     */
    public function setStore(Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store.
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set profile.
     *
     * @param Profile $profile
     *
     * @return Employee
     */
    public function setProfile(Profile $profile = null)
    {
        $this->profile = $profile;
        $profile->setEmployee($this);

        return $this;
    }

    /**
     * Get profile.
     *
     * @return Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return Son[]
     */
    public function getSons()
    {
        return $this->sons;
    }

    /**
     * @param Son[] $sons
     */
    public function setSons($sons)
    {
        $this->sons = $sons;
    }

    /**
     * @param Son $son
     */
    public function addSon(Son $son)
    {
        $this->sons->add($son);
        $son->setEmployee($this);
    }

    /**
     * @param Son $son
     */
    public function removeSon(Son $son)
    {
        $this->sons->removeElement($son);
    }

    /**
     * @return SentimentalStatus
     */
    public function getSentimentalStatus()
    {
        return $this->sentimentalStatus;
    }

    /**
     * @param SentimentalStatus $sentimentalStatus
     */
    public function setSentimentalStatus($sentimentalStatus)
    {
        $this->sentimentalStatus = $sentimentalStatus;
        $sentimentalStatus->setEmployee($this);
    }

    /**
     * @return ArrayCollection
     */
    public function getEducations()
    {
        return $this->educations;
    }

    /**
     * @param Education[] $educations
     */
    public function setEducations($educations)
    {
        $this->educations = $educations;
    }

    /**
     * @param Education $education
     */
    public function addEducation(Education $education)
    {
        $this->educations->add($education);
        $education->setEmployee($this);
    }

    /**
     * @param Education $education
     */
    public function removeEducation(Education $education)
    {
        $this->educations->removeElement($education);
    }

    /**
     * @return Achievement[]
     */
    public function getAchievements()
    {
        return $this->achievements;
    }

    /**
     * @param Achievement[] $achievements
     */
    public function setAchievements($achievements)
    {
        $this->achievements = $achievements;
    }

    /**
     * @param Achievement $achievement
     */
    public function addAchievement(Achievement $achievement)
    {
        $this->achievements->add($achievement);
        $achievement->setEmployee($this);
    }

    /**
     * @param Achievement $achievement
     */
    public function removeAchievement(Achievement $achievement)
    {
        $this->achievements->removeElement($achievement);
    }

    /**
     * Set area.
     *
     * @param string $area
     *
     * @return Employee
     */
    public function setArea($area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area.
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set agreement.
     *
     * @param string $agreement
     *
     * @return Employee
     */
    public function setAgreement($agreement =  null)
    {
        $this->agreement = $agreement;

        return $this;
    }

    /**
     * Get agreement.
     *
     * @return string
     */
    public function getAgreement()
    {
        return $this->agreement;
    }

    /**
     * @return ArrayCollection
     */
    public function getBenefits()
    {
        return $this->benefits;
    }

    /**
     * @param Benefit $benefit
     */
    public function addBenefit(Benefit $benefit)
    {
        $this->benefits->add($benefit);
    }

    /**
     * @param Benefit $benefit
     */
    public function removeBenefit(Benefit $benefit)
    {
        $this->benefits->removeElement($benefit);
    }

    public function __toString()
    {
        return sprintf('%s - %s (%s)', $this->code, $this->profile->getName(), $this->user->getUsername());
    }

    /**
     * @return Employee
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * @param Employee $trainer
     */
    public function setTrainer($trainer)
    {
        $this->trainer = $trainer;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    public function getUserDni()
    {
        return trim($this->user->getUsername());
    }

    public function getUserEmail()
    {
        return trim($this->user->getEmail());
    }

    public function getProfileName()
    {
        return trim($this->profile->getName());
    }

    /**
     * @return Employee
     */
    public function getBoss()
    {
        return $this->boss;
    }

    /**
     * @param Employee $boss
     */
    public function setBoss($boss)
    {
        $this->boss = $boss;
    }

    /**
     * @return Employee[]
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    public function setEmployee($employee)
    {
        $this->employees[] = $employee;
    }

    /**
     * @param Employee[] $employees
     */
    public function setEmployees($employees)
    {
        $this->employees = $employees;
    }

    public function setCareerPlans(\Doctrine\Common\Collections\Collection $plans)
    {
        $this->careerPlans = $plans;

        return $this;
    }

    public function addCareerPlan(EmployeeCareerPlan $plan) {
        $this->careerPlans[] = $plan;
        return $this;
    }

    public function removeCareerPlan(EmployeeCareerPlan $plan) {
        $this->careerPlans->removeElement($plan);
    }

    /**
     * @return EmployeeCareerPlan[]|ArrayCollection
     */
    public function getCareerPlans()
    {
        return $this->careerPlans;
    }

    /**
     * @param CareerPlan $plan
     * @return bool
     */
    public function isCareerPlanEnabled(CareerPlan $plan){
        $careerPlan = $this->getCareerPlans()->filter(function (EmployeeCareerPlan $entry) use ($plan) {
                return $entry->getPlan() === $plan;
            });

        return !$careerPlan->isEmpty();
    }

    /**
     * @return CareerPlan
     */
    public function getCurrentPlan()
    {
        return $this->currentPlan;
    }

    /**
     * @param CareerPlan $currentPlan
     */
    public function setCurrentPlan($currentPlan)
    {
        $this->currentPlan = $currentPlan;
    }

    /**
    * Add Code
    * @param Code $code
    */
    public function addDataQr( Code $code ) 
    {
        $this->codesQr[] = $code;
    }

    /**
    * Get Code
    * @param Code $code
    */
    public function getDataQr( Code $code ) 
    {
      return $this->employees;
    }

    /**
     * @return array
     */
    public function getCareerPlansStatus()
    {
        $careerPlans = [];
        foreach ($this->careerPlans as $employeeCareerPlan){
            $careerPlans[$employeeCareerPlan->getPlan()->getId()] =
                             ['id'                  => $employeeCareerPlan->getPlan()->getId(),
                              'title'               => $employeeCareerPlan->getPlan()->getTitle(),
                              'big_active_image'    => $employeeCareerPlan->getPlan()->getBigActiveImage(),
                              'big_inactive_image'  => $employeeCareerPlan->getPlan()->getBigInactiveImage(),
                              'tiny_active_image'   => $employeeCareerPlan->getPlan()->getTinyActiveImage(),
                              'tiny_inactive_image' => $employeeCareerPlan->getPlan()->getTinyInactiveImage(),
                              'position'            => $employeeCareerPlan->getPlan()->getPosition(),
                              'modules'             => $employeeCareerPlan->getPlan()->getModules(),
                              'requirements'        => $employeeCareerPlan->getPlan()->getRequirements(),
                              'enabled'             => $employeeCareerPlan->isEnabled(),
                              'approved'            => $employeeCareerPlan->isApproved()];
        }

        return $careerPlans;
    }

    /**
     * @return CareerPlan
     */
    public function getCurrentPlanEmployee()
    {
        $currentPlan = $this->getCurrentPlan();

        if(count($this->careerPlans) && $currentPlan){
            return $this->careerPlans->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($currentPlan) {
                return ($employeeCareerPlan->getPlan() === $currentPlan);
            })->first();
        } else {
            return null;
        }
    }





    /**
     * @param \DateTime $month
     * @param $category
     * @return Goal
     */
    public function getGoalsByMonthAndCategory(\DateTime $month, $category)
    {
        return $this->goals->filter(function (Goal $goal) use ($month, $category) {
            return
                $goal->getGoalType()->getCategory() === $category
                && $goal->getType() === 'MONTH'
                && $goal->getMonth() == $month->format('n')
                && $goal->getYear() == $month->format('Y');
        });
    }

    /**
     * @param \DateTime $month
     * @param $name
     * @param Category $category
     * @return Goal[]
     */
    public function getGoalsByMonthAndName(\DateTime $month, $name, $category = null)
    {
        if($month->format('Y') >= 2018) {
            $result = $this->goals->filter(function (Goal $goal) use ($month, $name, $category) {
                return
                    ($category ? $goal->getGoalType()->getCategory() === $category : true)
                    && $goal->getGoalType()->getName() === $name
                    && $goal->getType() === 'MONTH'
                    && $goal->getGoalType()->getYear() != 0
                    && $goal->getGoalType()->getMonth() != 0
                    && $goal->getMonth() == $month->format('n')
                    && $goal->getYear() == $month->format('Y');
            });
        }
        else{
            $result = $this->goals->filter(function (Goal $goal) use ($month, $name, $category) {
                return
                    ($category ? $goal->getGoalType()->getCategory() === $category : true)
                    && $goal->getGoalType()->getName() === $name
                    && $goal->getType() === 'MONTH'
                    && $goal->getMonth() == $month->format('n')
                    && $goal->getYear() == $month->format('Y');
            });
        }

        return $result;

    }

    public function getGoalsByYearAndName(\DateTime $year, $name, $category = null)
    {
        $extraPoints = $this->isDisabled($year->format('Y'));

        $result = $this->goals->filter(function (EmployeeGoal $goal) use ($year, $name, $category) {
            return
                ($category ? $goal->getEmployeeGoalType()->getCategory() === $category : true)
                && $goal->getEmployeeGoalType()->getName() === $name
                && $goal->getType() === 'MONTH'
                //  && ($goal->getResult() == 0 &&  $goal->getObjetive() > 0)
                && $goal->getResult() > 0

                && $goal->getYear() == $year->format('Y');
        });


        $total = 0;
        $cant = 0;

        foreach ($result as $goal) {
            $cant++;

            if ($name == 'TOTAL_EMPLEADO_OBJETIVO') {
                $total += $goal->getObjetive();
            }
            else {
                $total += $goal->getResult();
            }
        }


        // EXISTE EN TABLA EXTRA POINT
        if(!empty($extraPoints)){

            // TIPO PUNTOS ACUMULADOS
            if($name != 'TOTAL_EMPLEADO_OBJETIVO') {
                $total = $extraPoints->getPoints();
            }
            // TIPO PUNTOS OBJETIVO + EXTRA OBJETIVOS
            else{
                //ver
                $total=(int)(round($total / $cant))+Store::EXTRA_POINTS;
            }
        }
        else{
            if($cant > 0)
                $total=(int)(round($total / $cant));
            else
                $total=0;
        }


        return $total;
    }

    /**
     * @param \DateTime $month
     * @param $store
     * @return Ranking[]
     * @internal param $name
     */
    public function getRankingByMonthAndYear(\DateTime $month, $store)
    {
        return $this->rankings->filter(function (Ranking $ranking) use ($month, $store) {
            return
                $ranking->getStore() === $store
                && $ranking->getMonth() == $month->format('n')
                && $ranking->getYear() == $month->format('Y');
        });
    }

//    /**
//     * @param \DateTime $month
//     * @param $store
//     * @return EmployeeRanking[]
//     * @internal param $name
//     */
    public function getRankingByMonthAndYearEmployee(\DateTime $month, $store)
    {
        return $this->rankings->filter(function (EmployeeRanking $ranking) use ($month, $store) {
            return
                $ranking->getEmployee() === $store
                && $ranking->getMonth() == $month->format('n')
                && $ranking->getYear() == $month->format('Y');
        });
    }

    public function getRankingByYear(\DateTime $year)
    {
        return $this->rankings->filter(function (EmployeeRanking $ranking) use ($year) {
            return $ranking->getYear() == $year->format('Y')
            && $ranking->getType() == 'YEAR';
        });
    }

    /**
     * @param integer $month
     * @param integer $year
     * @return Ranking
     */
    public function getMonthRanking($month, $year)
    {
        return $this->rankings->filter(function (Ranking $ranking) use ($month, $year) {
            return $ranking->getType() === 'MONTH' && $ranking->getYear() == $year && $ranking->getMonth() == $month;
        })->first();
    }

    /**
     * @param integer $year
     * @return Ranking
     */
    public function getYearRanking($year)
    {
        return $this->rankings->filter(function (Ranking $ranking) use ($year) {
            return $ranking->getType() === 'YEAR' && $ranking->getYear() == $year;
        })->first();
    }

    /**
     * Esto es para identificar cines que han sido DESCALIFICADOS
     *
     * @return boolean
     */
    public function isDisabled($year){
        $extraPoint=$this->extraPoints->filter(function (ExtraPointsStore $extraPoint) use ($year) {
            return $extraPoint->getType() === 'AUSENTISM' && $extraPoint->getYear() == $year;
        })->first();

        if(!empty($extraPoint))
            return $extraPoint;
        else
            return null;
    }

    /**
     * @param \DateTime $month
     * @param $name
     * @param Category $category
     * @return EmployeeGoal[]
     */
    public function getGoalsByMonthAndNameEmployee(\DateTime $month, $name, $category = null)
    {

        $result = $this->goals->filter(function (Goal $goal) use ($month, $name, $category) {
            return
                ($category ? $goal->getGoalType()->getCategory() === $category : true)
                && $goal->getGoalType()->getName() == $name
                && $goal->getType() === 'MONTH'
                && $goal->getGoalType()->getYear() != 0
                && $goal->getGoalType()->getMonth() != 0
                && $goal->getMonth() == $month->format('n')
                && $goal->getYear() == $month->format('Y');
        });


        // if($month->format('Y') >= 2018) {
        //     $result = $this->goals->filter(function (EmployeeGoal $goal) use ($month, $name, $category) {
        //         return
        //             ($category ? $goal->getEmployeeGoalType()->getCategory() === $category : true)
        //             && $goal->getEmployeeGoalType()->getName() === $name
        //             && $goal->getType() === 'MONTH'
        //             && $goal->getEmployeeGoalType()->getYear() != 0
        //             && $goal->getEmployeeGoalType()->getMonth() != 0
        //             && $goal->getMonth() == $month->format('n')
        //             && $goal->getYear() == $month->format('Y');
        //     });
        // } else{
        //     $result = $this->goals->filter(function (EmployeeGoal $goal) use ($month, $name, $category) {
        //         return
        //             ($category ? $goal->getEmployeeGoalType()->getCategory() === $category : true)
        //             && $goal->getEmployeeGoalType()->getName() === $name
        //             && $goal->getType() === 'MONTH'
        //             && $goal->getMonth() == $month->format('n')
        //             && $goal->getYear() == $month->format('Y');
        //     });
        // }

        return $result;
    }

    public function getGoalsByMonthAndCategoryEmployee(\DateTime $month, $category)
    {
        return $this->goals->filter(function (Goal $goal) use ($month, $category) {
            return
                $goal->getGoalType()->getCategory() == $category
                && $goal->getType() == 'MONTH'
                && $goal->getMonth() == $month->format('n')
                && $goal->getYear() == $month->format('Y');
        });
        // return $this->goals->filter(function (EmployeeGoal $goal) use ($month, $category) {
        //     return
        //         $goal->getEmployeeGoalType()->getCategory() === $category
        //         && $goal->getType() === 'MONTH'
        //         && $goal->getMonth() == $month->format('n')
        //         && $goal->getYear() == $month->format('Y');
        // });
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Add goal
     *
     * @param \Aper\IncentiveBundle\Entity\EmployeeGoal $goal
     *
     * @return Employee
     */
    public function addGoal(\Aper\IncentiveBundle\Entity\EmployeeGoal $goal)
    {
        $this->goals[] = $goal;

        return $this;
    }

    /**
     * Remove goal
     *
     * @param \Aper\IncentiveBundle\Entity\EmployeeGoal $goal
     */
    public function removeGoal(\Aper\IncentiveBundle\Entity\EmployeeGoal $goal)
    {
        $this->goals->removeElement($goal);
    }

    /**
     * Add ranking
     *
     * @param \Aper\IncentiveBundle\Entity\EmployeeRanking $ranking
     *
     * @return Employee
     */
    public function addRanking(\Aper\IncentiveBundle\Entity\EmployeeRanking $ranking)
    {
        $this->rankings[] = $ranking;

        return $this;
    }

    /**
     * Remove ranking
     *
     * @param \Aper\IncentiveBundle\Entity\EmployeeRanking $ranking
     */
    public function removeRanking(\Aper\IncentiveBundle\Entity\EmployeeRanking $ranking)
    {
        $this->rankings->removeElement($ranking);
    }

    /**
     * Add extraPoint
     *
     * @param \Aper\IncentiveBundle\Entity\ExtraPointsEmployee $extraPoint
     *
     * @return Employee
     */
    public function addExtraPoint(\Aper\IncentiveBundle\Entity\ExtraPointsEmployee $extraPoint)
    {
        $this->extraPoints[] = $extraPoint;

        return $this;
    }

    /**
     * Remove extraPoint
     *
     * @param \Aper\IncentiveBundle\Entity\ExtraPointsEmployee $extraPoint
     */
    public function removeExtraPoint(\Aper\IncentiveBundle\Entity\ExtraPointsEmployee $extraPoint)
    {
        $this->extraPoints->removeElement($extraPoint);
    }

    /**
     * Get extraPoints
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExtraPoints()
    {
        return $this->extraPoints;
    }

    /**
     * Add employee
     *
     * @param \Aper\UserBundle\Entity\Employee $employee
     *
     * @return Employee
     */
    public function addEmployee(\Aper\UserBundle\Entity\Employee $employee)
    {
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \Aper\UserBundle\Entity\Employee $employee
     */
    public function removeEmployee(\Aper\UserBundle\Entity\Employee $employee)
    {
        $this->employees->removeElement($employee);
    }

    /**
     * Remove codesQr
     *
     * @param \Aper\CodeBundle\Entity\Code $codesQr
     */
    public function removeCodesQr(\Aper\CodeBundle\Entity\Code $codesQr)
    {
        $this->codesQr->removeElement($codesQr);
    }

    /**
     * Set subRol
     *
     * @param \Aper\UserBundle\Entity\SubRol $subRol
     *
     * @return Employee
     */
    public function setSubRol(\Aper\UserBundle\Entity\SubRol $subRol = null)
    {
        $this->subRol = $subRol;

        return $this;
    }

    /**
     * Get subRol
     *
     * @return \Aper\UserBundle\Entity\SubRol
     */
    public function getSubRol()
    {
        return $this->subRol;
    }
}
