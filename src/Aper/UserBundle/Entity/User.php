<?php

namespace Aper\UserBundle\Entity;

use Aper\PlanCarreraBundle\Entity\ExternalTraining;
use Aper\SurpriseTargetBundle\Entity\SurpriseTargetUserStock;
use Aper\PlanCarreraBundle\Entity\ExternalTrainingInscription;
use Aper\SurpriseTargetBundle\Entity\SurpriseTargetStock;
use Aper\UserBundle\Model\Role;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\UserBundle\Model\User as BaseUser;

/**
 * User.
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="dni_idx", columns={"username"}), @ORM\UniqueConstraint(name="email_idx", columns={"email"})})
 * @ORM\Entity(repositoryClass="Aper\UserBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    const ROLE_EMPLOYEE = 'ROLE_EMPLOYEE';
    const ROLE_HEAD_OFFICE = 'ROLE_HEAD_OFFICE';
    const ROLE_MANAGER = 'ROLE_MANAGER';
    const ROLE_MANAGER_GLOBAL = 'ROLE_MANAGER_GLOBAL';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_ADMIN_CONSULTA = 'ROLE_ADMIN_CONSULTA';
    const ROLE_ADMIN_CAPACITACION = 'ROLE_ADMIN_CAPACITACION';

    /**
     * @var string
     * @Assert\Regex(
     *     pattern="/@example\.com$/",
     *     match=false,
     *     message="Por favor ingrese un email válido",
     *     groups={"Default", "Account"}
     * )
     */
    protected $email;


    /**
     * @ORM\Column(name="legales", type="integer", nullable=true)
     */
    protected $legales;


    /**
     * @ORM\Column(name="cant_employee", type="integer", nullable=true)
     */
    private $cantEmployee;

    /**
     * @var Employee
     * @ORM\OneToOne(targetEntity="Employee", mappedBy="user", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    protected $employee;

    /**
     * @var SuccessfulLogin[]|Collection
     * @ORM\OneToMany(targetEntity="SuccessfulLogin", mappedBy="user", cascade={"all"}, orphanRemoval=true)
     */
    protected $successfulLogin;

    /**
     *
     * @ORM\OneToMany(targetEntity="Aper\PlanCarreraBundle\Entity\ExternalTraining", mappedBy="trainer", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    protected $expositions;


    /**
     *
     * @ORM\OneToMany(targetEntity="Aper\SurpriseTargetBundle\Entity\SurpriseTargetUserStock", mappedBy="user", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    public $surpriseTargetStocks;


    /**
     * @return ExternalTraining[]|ArrayCollection
     */
    public function getExpositions()
    {
        return $this->expositions;
    }

    public function setEmail($email){
        $this->email = $email;
        return $this;
    }

    public function getEmail(){
        return $this->email;
    }

    /**
     * @return SuccessfulLogin[]|Collection
     */
    public function getSuccessfulLogin()

    {
		
        return $this->successfulLogin;
    }


    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    public function setCantEmployee($cantEmployee){
        $this->cantEmployee = $cantEmployee;
    }


    public function getCantEmployee(){
        return  $this->cantEmployee;
    }

    /**
     * @param Employee $employee
     */
    public function setEmployee(Employee $employee)
    {
        $this->employee = $employee;
        $employee->setUser($this);
    }

    /**
     * @return array
     */
    public function getRoleChoices()
    {
        return Role::getChoices();
    }

    /**
     * @return bool
     */
    public function acceptMultipleRoles()
    {
        return true;
    }

    public function getProfileName()
    {
        if($this->getEmployee()){
            return $this->getEmployee()->getProfile()->getName();
        }
        return '';
    }

    public function getMaxRole()
    {
        $role = 'Perfil 3 (Playeros)'; // Perfil 3
        if(in_array('ROLE_MANAGER', $this->getRoles())){
            $role = 'Perfil 2 (Operadores / Jefes)'; // Perfil 2
        }
        if(in_array('ROLE_STORE_MANAGER', $this->getRoles())){
            $role = 'Gerente de Complejo';
        }
        if(in_array('ROLE_MANAGER_GLOBAL', $this->getRoles())){
            $role = 'Perfil 4 (Administración / Gerentes)'; // Perfil 4
        }
        if(in_array('ROLE_HEAD_OFFICE', $this->getRoles())){
            $role = 'Perfil 1 (Comercial / Oficina Central)'; // Perfil 1
        }
        if(in_array('ROLE_ADMIN', $this->getRoles())){
            $role = 'Perfil 5 (Administrador Backend)'; // Perfil 5
        }
        if(in_array('ROLE_ADMIN_CONSULTA', $this->getRoles())){
            $role = 'Perfil 6 (Adminitrador de Consulta)'; // Perfil 1
        }
        if(in_array('ROLE_ADMIN_CAPACITACION', $this->getRoles())){
            $role = 'Perfil 7 (Adminitrador de Capacitaciones)'; // Perfil 1
        }
 
        return $role;
    }

    public function getMaxRoleType()
    {
        $role = 'ROLE_EMPLOYEE';
        if(in_array('ROLE_MANAGER', $this->getRoles())){
            $role = 'ROLE_MANAGER';
        }
        if(in_array('ROLE_STORE_MANAGER', $this->getRoles())){
            $role = 'ROLE_STORE_MANAGER';
        }
        if(in_array('ROLE_MANAGER_GLOBAL', $this->getRoles())){
            $role = 'ROLE_MANAGER_GLOBAL';
        }
        if(in_array('ROLE_HEAD_OFFICE', $this->getRoles())){
            $role = 'ROLE_HEAD_OFFICE';
        }
        if(in_array('ROLE_ADMIN', $this->getRoles())){
            $role = 'ROLE_ADMIN';
        }
        if(in_array('ROLE_ADMIN_CONSULTA', $this->getRoles())){
            $role = 'ROLE_ADMIN_CONSULTA';
        }
        if(in_array('ROLE_ADMIN_CAPACITACION', $this->getRoles())){
            $role = 'ROLE_ADMIN_CAPACITACION';
        }
        return $role;
    }

    public function __construct()
    {
        parent::__construct();
        $this->expositions = new ArrayCollection();
        $this->surpriseTargetStocks = new ArrayCollection();
    }


    public function __toString()
    {
        if($this->employee){
            return $this->getEmployee()->__toString();
        }
        return parent::__toString();
    }

    public function getActiveExpositions(){
         return $this->expositions->filter(function (ExternalTraining $externalTraining) {
             $operated = $externalTraining->getInscriptions()->filter(function (ExternalTrainingInscription $inscription) {
                return ((!is_null($inscription->getApprovedAt())) and (!is_null($inscription->getAssistance())));
             });
             return ($externalTraining->getInscriptions()->count() and ($operated->count() != $externalTraining->getInscriptions()->count()));
         });
    }


    public function getLegales()
    {
        return $this->legales;
    }
    public function setLegales()
    {
        $this->legales = 1;
    }


    /**
     * @return SurpriseTargetStock[]|ArrayCollection
     */
    public function getSurpriseTargetStocks(){
        return $this->surpriseTargetStocks;
    }

    public function setSurpriseTargetStocks(SurpriseTargetUserStock $surprise){

        if (!$this->surpriseTargetStocks->contains($surprise)) {
            $this->surpriseTargetStocks[] = $surprise;
            $surprise->setUser($this);
        }

        return $this;
    }

}
