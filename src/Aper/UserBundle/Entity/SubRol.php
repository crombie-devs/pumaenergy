<?php

namespace Aper\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubRol
 *
 * @ORM\Table(name="sub_rol")
 * @ORM\Entity(repositoryClass="Aper\UserBundle\Repository\SubRolRepository")
 */
class SubRol
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="short_name", type="string", length=255)
     */
    private $short_name;


    /**
     * @var Employee
     * @ORM\OneToMany(targetEntity="Employee", mappedBy="subRol")
     */
    protected $employee;

    /**
     * @ORM\ManyToMany(targetEntity="Aper\PlanCarreraBundle\Entity\Module", mappedBy="subRol")
     * @ORM\JoinTable(name="subRol_module")
     */
    private $module;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SubRol
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->employee = new \Doctrine\Common\Collections\ArrayCollection();
        $this->module = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Add employee
     *
     * @param \Aper\UserBundle\Entity\Employee $employee
     *
     * @return SubRol
     */
    public function addEmployee(\Aper\UserBundle\Entity\Employee $employee)
    {
        $this->employee[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \Aper\UserBundle\Entity\Employee $employee
     */
    public function removeEmployee(\Aper\UserBundle\Entity\Employee $employee)
    {
        $this->employee->removeElement($employee);
    }

    /**
     * Get employee
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Add module
     *
     * @param \Aper\PlanCarreraBundle\Entity\Module $module
     *
     * @return SubRol
     */
    public function addModule(\Aper\PlanCarreraBundle\Entity\Module $module)
    {
        $this->module[] = $module;

        return $this;
    }

    /**
     * Remove module
     *
     * @param \Aper\PlanCarreraBundle\Entity\Module $module
     */
    public function removeModule(\Aper\PlanCarreraBundle\Entity\Module $module)
    {
        $this->module->removeElement($module);
    }

    /**
     * Get module
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModule()
    {
        return $this->module;
    }
}
