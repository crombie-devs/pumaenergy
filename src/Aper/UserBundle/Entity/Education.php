<?php

namespace Aper\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Education.
 *
 * @ORM\Table(name="educations")
 * @ORM\Entity(repositoryClass="Aper\UserBundle\Repository\AchievementRepository")
 */
class Education
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable = true)
     * @Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="string", length=100)
     *  @Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="institution", type="string", length=100, nullable = true)
     * @Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $institution;

    /**
     * @var string
     *
     * @ORM\Column(name="career", type="string", length=100, nullable = true)
     * @Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $career;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_at", type="date", nullable=true)
     */
    private $startAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish_at", type="date", nullable=true)
     */
    private $finishAt;

    /**
     * @var Employee
     * @ORM\ManyToOne(targetEntity="Employee", inversedBy="educations")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     */
    protected $employee;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title = null)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return string
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * @param string $institution
     */
    public function setInstitution($institution = null)
    {
        $this->institution = $institution;
    }

    /**
     * @return string
     */
    public function getCareer()
    {
        return $this->career;
    }

    /**
     * @param string $career
     */
    public function setCareer($career = null)
    {
        $this->career = $career;
    }

    /**
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * @param \DateTime $startAt
     */
    public function setStartAt(\DateTime $startAt = null)
    {
        $this->startAt = $startAt;
    }

    /**
     * @return \DateTime
     */
    public function getFinishAt()
    {
        return $this->finishAt;
    }

    /**
     * @param \DateTime $finishAt
     */
    public function setFinishAt(\DateTime $finishAt = null)
    {
        $this->finishAt = $finishAt;
    }


    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param Employee $employee
     */
    public function setEmployee(Employee $employee)
    {
        $this->employee = $employee;
    }
}
