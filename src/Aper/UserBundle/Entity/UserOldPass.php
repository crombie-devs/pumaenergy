<?php

namespace Aper\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_old_pass")
 */
class UserOldPass
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     *
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_registered", type="date", nullable=true)
     */
    private $lastRegistered;

    /**
     * @ORM\Column(name="pass", type="string", length=100)
     */
    private $pass;

    public function getId()
    {
        return $this->id;
    }

    public function setLastRegistered($lastRegistered){
        $this->lastRegistered = $lastRegistered;
    }

    public function getLastRegistered(){
        return $this->lastRegistered;
    }

    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

}
