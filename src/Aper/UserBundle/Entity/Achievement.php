<?php

namespace Aper\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SentimentalStatus.
 *
 * @ORM\Table(name="achievements")
 * @ORM\Entity(repositoryClass="Aper\UserBundle\Repository\AchievementRepository")
 */
class Achievement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100 , nullable = true)
     * @Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100, nullable=true)
     * @Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $type;

    /**
     * @var \datetime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     * * @Assert\NotBlank(groups={"Default", "Profile", "Register"})
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Employee", inversedBy="achievements")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     */
    protected $employee;

    //SETTERS & GETTERS

    //ID

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    //TITLE//

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Achievement
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    //DESCRIPTION//

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Achievement
     */
    public function setDescription($description = null  )
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    //DATE//

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Achievement
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    //TYPE//

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Achievement
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param mixed $employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }
}
