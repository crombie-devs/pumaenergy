<?php

namespace Aper\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\FileBundle\Model\Image;

/**
 * @ORM\Entity
 * @ORM\Table(name="avatars")
 */
class Avatar extends Image
{
    /**
     * Directorio relativo a guardar las imágenes.
     */
    const DIRECTORY = 'uploads/avatar';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var UploadedFile
     * @Assert\Image(
     *     minWidth = 20,
     *     maxWidth = 4000,
     *     minHeight = 20,
     *     maxHeight = 4000,
     *     groups={"Default", "Profile", "Register"}
     * )
     */
    protected $file;

    /**
     * @var Profile
     * @ORM\OneToOne(targetEntity="Aper\UserBundle\Entity\Profile", inversedBy="avatar")
     */
    private $profile;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param Profile $profile
     */
    public function setProfile(Profile $profile)
    {
        $this->profile = $profile;
    }
}
