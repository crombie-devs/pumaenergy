<?php

namespace Aper\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User.
 *
 * @ORM\Table(name="profiles", indexes={@ORM\Index(name="birth_day_idx", columns={"birth_day"})})
 * @ORM\Entity(repositoryClass="Aper\UserBundle\Repository\ProfileRepository")
 */
class Profile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Employee
     *
     * @ORM\OneToOne(targetEntity="Employee", inversedBy="profile", cascade={"all"})
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     * @Assert\NotBlank(groups={"Default", "Register"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=100, nullable=true)
     */
    private $sex;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_day", type="date", nullable=true)
     * //@Assert\NotBlank(groups={"Default", "Register"})
     */
    private $birthDay;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_at", type="date", nullable=true)
     * //@Assert\NotBlank(groups={"Default", "Profile"})
     */
    private $startAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="departure_at", type="date", nullable=true)
     */
    private $departureAt;


    /**
     * @var string
     *
     * @ORM\Column(name="personal_text", type="string", length=255, nullable=true)
     */
    private $personalText;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=100, nullable=true)
     */
    private $phoneNumber;
    /**
     * @var string
     *
     * @ORM\Column(name="phone_area_code", type="string", length=100, nullable=true)
     */
    private $phoneAreaCode;

    /**
     * @var string
     *
     * @ORM\Column(name="operator", type="string", length=100, nullable=true)
     */
    private $operator;

    /**
     * @var Avatar
     * @ORM\OneToOne(targetEntity="Aper\UserBundle\Entity\Avatar", mappedBy="profile", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $avatar;

     /**
     * @var string
     * @ORM\Column(name="corpmail", type="string", length=100, nullable=true)
     * @Assert\Email()
     */
    private $corpmail;
    
     /**
     * @var string
     * @ORM\Column(name="office_phone", type="string", length=100, nullable=true)
     */
    private $officePhone;
    
     /**
     * @var string
     * @ORM\Column(name="province", type="string", length=100, nullable=true)
     */
    private $province;
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Profile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get dni.
     *
     * @return string
     */
    public function getDni()
    {
        return $this->employee->getUser()->getUsername();
    }

    /**
     * Set birthDay.
     *
     * @param \DateTime $birthDay
     *
     * @return Profile
     */
    public function setBirthDay(\DateTime $birthDay = null)
    {
        $this->birthDay = $birthDay;

        return $this;
    }

    /**
     * Get birthday.
     *
     * @return \DateTime
     */
    public function getBirthDay()
    {
        return $this->birthDay;
    }

    /**
     * Set startAt.
     *
     * @param \DateTime $startAt
     *
     * @return Profile
     */
    public function setStartAt(\DateTime $startAt = null)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startat.
     *
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set departureAt.
     *
     * @param \DateTime $departureAt
     *
     * @return Profile
     */
    public function setDepartureAt(\DateTime $departureAt = null)
    {
        $this->departureAt = $departureAt;

        return $this;
    }

    /**
     * Get departureat.
     *
     * @return \DateTime
     */
    public function getDepartureAt()
    {
        return $this->departureAt;
    }

    /**
     * @param $phoneNumber
     *
     * @return $this
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phonenumber.
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param $phoneAreaCode
     *
     * @return $this
     */
    public function setPhoneAreaCode($phoneAreaCode)
    {
        $this->phoneAreaCode = $phoneAreaCode;

        return $this;
    }

    /**
     * Get phoneAreaCode.
     *
     * @return string
     */
    public function getPhoneAreaCode()
    {
        return $this->phoneAreaCode;
    }

    //OPERATOR//

    /**
     * Set operator.
     *
     * @param string $operator
     *
     * @return Profile
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;

        return $this;
    }

    /**
     * Get operator.
     *
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * Set personal text.
     *
     * @param string $personalText
     *
     * @return Profile
     */
    public function setPersonalText($personalText)
    {
        $this->personalText = $personalText;

        return $this;
    }

    /**
     * Get personal text.
     *
     * @return string
     */
    public function getPersonalText()
    {
        return $this->personalText;
    }

    /**
     * @return Avatar
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param Avatar $avatar
     */
    public function setAvatar(Avatar $avatar)
    {
        $this->avatar = $avatar;
        $avatar->setProfile($this);
    }

    /**
     * Set employee.
     *
     * @param Employee $employee
     *
     * @return Profile
     */
    public function setEmployee(Employee $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee.
     *
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set sex.
     *
     * @param string $sex
     *
     * @return Employee
     */
    public function setSex($sex = null)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex.
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }
    
    /**
     * Set corpmail.
     *
     * @param string $corpmail
     *
     * @return Employee
     */
    public function setCorpMail($corpmail)
    {
        $this->corpmail = $corpmail;

        return $this;
    }

    /**
     * Get corpmail.
     *
     * @return string
     */
    public function getCorpMail()
    {
        return $this->corpmail;
    }

    /**
     * Set officePhone.
     *
     * @param string $officePhone
     *
     * @return Employee
     */
    public function setOfficePhone($officePhone)
    {
        $this->officePhone = $officePhone;

        return $this;
    }

    /**
     * Get officePhone.
     *
     * @return string
     */
    public function getOfficePhone()
    {
        return $this->officePhone;
    }

    /**
     * Set province.
     *
     * @param string $province
     *
     * @return Employee
     */
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province.
     *
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }
}
