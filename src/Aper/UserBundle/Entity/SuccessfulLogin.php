<?php

namespace Aper\UserBundle\Entity;

use WebFactory\Bundle\UserBundle\Model\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SuccessfulLogin.
 *
 * @ORM\Table(name="successful_login")
 * @ORM\Entity(repositoryClass="Aper\UserBundle\Repository\SuccessfulLoginRepository")
 */
class SuccessfulLogin
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \datetime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     * * @Assert\NotBlank()
     */
    private $date;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="successfulLogin")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     *
     */
    protected $user;



    /**
     * SuccessfulLogin constructor.
     * @param User $user
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
        $this->date = new \DateTime();
    }

    /**
     * @return \datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }




}