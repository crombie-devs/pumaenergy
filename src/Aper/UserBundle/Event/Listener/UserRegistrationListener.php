<?php

namespace Aper\UserBundle\Event\Listener;

use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\Register;
use Aper\UserBundle\Entity\Profile;
use Aper\UserBundle\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping\PostPersist;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserRegistrationListener
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var array
     */
    private $changeSet;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * SendProfileNoticeListener constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {

        $entity = $event->getEntity();
        $sonsent = false;
        switch (true) {
            case $entity instanceof User:
                $this->user = $entity;
                break;
            case $entity instanceof Employee:
                $this->user = $entity->getUser();
                break;
            case $entity instanceof Profile:
                $this->user = $entity->getEmployee()->getUser();
                break;
            case $entity instanceof Avatar:
                $this->user = $entity->getProfile()->getEmployee()->getUser();
                break;
            case $entity instanceof Education:
                $this->user = $entity->getEmployee()->getUser();
                break;
            case $entity instanceof Son:
                $this->user = $entity->getEmployee()->getUser();

                break;
            case $entity instanceof SentimentalStatus:
                $this->user = $entity->getEmployee()->getUser();
                break;
        }

        $this->changeSet = $event->getEntityChangeSet();

        if ($this->user !== $this->getUser()) {
            $this->user = null;
            $this->changeSet = null;
        }
    }

     /**
     * @param PostPersist $event
     */
    public function postPersist(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        switch (true) {
            case $entity instanceof Register:
                $store = $entity->getStore();
                $arrayData['nombre'] = $entity->getNombre();
                $arrayData['apellido'] = $entity->getApellido();
                $arrayData['dni'] = $entity->getDni();
                $arrayData['email'] = $entity->getEmail();
                $arrayData['role'] = $entity->getRole();

                $this->sendNoticeToEmployer($store, $arrayData);
                break;

            case $entity instanceof User:

                $this->sendNoticeToEmployee($entity->getEmployee()->getUser());
                $this->register();
                break;
        }
    }

    // /**
    //  * @param PreUpdateEventArgs $event
    //  * @return bool
    //  */
    // private function isFirstTimeEdition(PreUpdateEventArgs $event)
    // {
    //     if ($event->hasChangedField('email') && !preg_match('/example\.com$/', $event->getNewValue('email'))) {
    //         return true;
    //     }

    //     return false;
    // }

    /**
     * @return User
     */
    private function getUser()
    {
        // $tokenStorage = $this->container->get('security.token_storage');
        // $token = $tokenStorage->getToken();
        // $user = $token ? $token->getUser() : null;

        // return $user instanceof User ? $user : null;
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function postUpdate(LifecycleEventArgs $event)
    {
        $this->register();
    }

    /**
     * @param User $user
     */
    private function sendNoticeToEmployer($store, $arrayData)
    {
        $fromName = $this->container->getParameter('email_from');
        $from = 'noreply@pumaenergy.com';

        $employees = $this->container->get('doctrine')->getRepository('UserBundle:Employee')->findBy(["store" => $store]);
        $empleador = null;

        foreach ($employees as $key => $employee) {
            if(in_array('ROLE_MANAGER', $employee->getUser()->getRoles())){
                $empleador = $employee;
                break;
            }
        }

        if ($empleador) {
            $to = [ $empleador->getUser()->getEmail() ];

            $changeSet = $this->changeSet;

            $message = \Swift_Message::newInstance()
                ->setSubject('Un empleado se ha registrado en la plataforma')
                ->setFrom(array($from => $fromName))
                ->setTo($to)
                ->setBody($this->renderView('', '@User/Frontend/email_user_registration_notice.html.twig',
                    compact('arrayData' )), 'text/html');

            $mailer = $this->container->get('mailer');
            $mailer->send($message);
        }
    }

    /**
     * @param User $user
     */
    private function sendNoticeToEmployee(User $user)
    {
        $from = 'noreply@pumaenergy.com';
        $fromName = $this->container->getParameter('email_from');
        $to = $user->getEmail();

        $message = \Swift_Message::newInstance()
            ->setSubject('Bienvenido a la plataforma de Recursos Humanos')
            ->setFrom(array($from => $fromName))
            ->setTo($to)
            ->setBody($this->renderView('','@User/Frontend/email_welcome.html.twig', compact('user')), 'text/html');

        $mailer = $this->container->get('mailer');
        $mailer->send($message);
    }

    /**
     * @param $view
     * @param array $parameters
     * @return string
     */
    public function renderView($what, $view, array $parameters = array())
    {
        $templating = $this->container->get('twig');

        return $templating->render($view, $parameters);
    }


    private function register()
    {

        $tokenStorage = $this->container->get('security.token_storage');
        $token = $tokenStorage->getToken();
        $user = $token ? $token->getUser() : null;


        if(!is_null($this->getUser()) &&
            !is_null($this->getUser()->getUsername())){
            $code = $this->getUser()->getUsername();
        } else
            return null;

        $url = "https://cms.bonda.us/api/v2/microsite/".self::MICROSITE_ID."/affiliates";

        $header = array(
            'Accept: application/json', 
            'Cache-Control: no-cache', 
            'Content-Type: multipart/form-data',
            'token: '.self::KEY 
        );

        $handle = curl_init($url);

        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_HEADER, true);
        curl_setopt($handle, CURLOPT_HTTPHEADER, $header);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_POST, 1);
        curl_setopt($handle, CURLOPT_POSTFIELDS, array('code' => $code));

        curl_setopt($handle, CURLOPT_FAILONERROR, true);
        curl_setopt($handle, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
        curl_setopt($handle, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($handle);

        if (curl_errno($handle)) {
            $errorMsg = curl_error($handle);
        }

        if (isset($errorMsg) && !empty($errorMsg)) {
        }

        curl_close($handle);
        // $result =  substr($result, strpos($result, "\r\n\r\n")+4);
        // return JsonResponse::create(json_decode($result, true));
    }

}