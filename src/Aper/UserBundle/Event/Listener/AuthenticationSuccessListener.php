<?php

namespace Aper\UserBundle\Event\Listener;


use Aper\UserBundle\Entity\SuccessfulLogin;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use WebFactory\Bundle\UserBundle\Model\UserInterface;

/**
 * Class AuthenticationSuccessListener.
 */
class AuthenticationSuccessListener implements EventSubscriberInterface
{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onSecurityAuthenticationSuccess',
        );
    }

    /**
     * @param AuthenticationEvent $event
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onSecurityAuthenticationSuccess(AuthenticationEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if ($user instanceof UserInterface)
        {
            if(!in_array('ROLE_ADMIN', $user->getRoles())){
                $successfulLogin = new SuccessfulLogin($user);
                $this->em->persist($successfulLogin);
                $this->em->flush();
            }
        }
    }
}
