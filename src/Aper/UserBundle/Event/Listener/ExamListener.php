<?php

namespace Aper\UserBundle\Event\Listener;

use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Entity\ExamPermission;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Entity\PollPermission;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;

class ExamListener
{
    /**
     * @param LifecycleEventArgs $event
     */
    public function postUpdate(LifecycleEventArgs $event)
    {
    	// var_dump(get_class($event->getEntity()));die;
        $entity = $event->getEntity();
		$em = $event->getObjectManager();
        switch (true) {
            case $entity instanceof EmployeeCareerModule:
            	if($entity->isCourseApproved() && !$entity->isApproved()){
	        		$moduleExam = $entity->getModule()->getExam();
	        		$emp = $entity->getEmployeePlan()->getUser();
	            	$examPermission = new ExamPermission($entity, $moduleExam, 1);
	            	$examPermission->setEnabled(true);
	            	$em->persist($examPermission);
	            	$em->flush();
	            	$entity->addExamPermission($examPermission);
	            	$em->persist($entity);
	            	$em->flush();
	        	}
            	break;
	        case $entity instanceof EmployeeModuleExam:
        		if($entity->isApproved()){
        			$employeeCareerModule = $entity->getExamPermission()->getEmployeeModule();
	            	$pollPermission = new PollPermission($employeeCareerModule, $employeeCareerModule->getModule()->getPoll());
	            	$pollPermission->setEnabled(true);
	            	$em->persist($pollPermission);
	            	$em->flush();
	            	$employeeCareerModule->addPollPermission($pollPermission);
	            	$em->persist($employeeCareerModule);
	            	$em->flush();
	            }
	            break;
	    }
    }

}