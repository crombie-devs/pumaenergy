<?php

namespace Aper\UserBundle\Event\Listener;

use Aper\UserBundle\Entity\User;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SendWelcomeEmailListener
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * SendProfileNoticeListener constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {
        $entity = $event->getEntity();

        if (!$entity instanceof User) {
            return;
        }

        if ($entity !== $this->getUser()) {
            return;
        }

        if ($this->isFirstTimeEdition($event)) {
            $this->sendNotice($entity);
        }
    }

    /**
     * @param PreUpdateEventArgs $event
     * @return bool
     */
    private function isFirstTimeEdition(PreUpdateEventArgs $event)
    {
        if ($event->hasChangedField('email') && !preg_match('/example\.com$/', $event->getNewValue('email'))) {
            return true;
        }

        return false;
    }

    /**
     * @return User
     */
    private function getUser()
    {
        $tokenStorage = $this->container->get('security.token_storage');
        $token = $tokenStorage->getToken();
        $user = $token ? $token->getUser() : null;

        return $user instanceof User ? $user : null;
    }

    /**
     * @param User $user
     */
    private function sendNotice(User $user)
    {
        $from = 'noreply@pumaenergy.com';
        $fromName = $this->container->getParameter('email_from');
        $to = $user->getEmail();

        $message = \Swift_Message::newInstance()
            ->setSubject('Bienvenido a la plataforma de Recursos Humanos')
            ->setFrom(array($from => $fromName))
            ->setTo($to)
            ->setBody($this->renderView('@User/Frontend/email_welcome.html.twig', compact('user')), 'text/html');

        $mailer = $this->container->get('mailer');
        $mailer->send($message);
    }

    /**
     * @param $view
     * @param array $parameters
     * @return string
     */
    public function renderView($view, array $parameters = array())
    {
        $templating = $this->container->get('twig');

        return $templating->render($view, $parameters);
    }

}