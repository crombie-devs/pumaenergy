<?php

namespace Aper\UserBundle\Event\Listener;

use Aper\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class PasswordExpireListener.
 */
class PasswordExpireListener
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * IncompleteProfileListener constructor.
     *
     * @param Router                $router
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(Router $router, TokenStorageInterface $tokenStorage)
    {
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $token = $this->tokenStorage->getToken();
        if (!$token) {
            return;
        }

        /* @var User $user */
        $user = $token->getUser();
        if (!$user instanceof User) {
            return;
        }

        $request = $event->getRequest();
        $url = $request->getPathInfo();
        if('/register/resset/see/' == $url){
            return;
        }

        $isFrontEnd = $this->isFrontend($event->getRequest());
        $passwordIsExpired = $this->passwordIsExpired($user);
        $isEditionRoute = $this->isEditionRoute($event->getRequest());
        if ($isFrontEnd && $passwordIsExpired && !$isEditionRoute) {
            $event->getRequest()->getSession()->getFlashBag()->add('warning', 'Su contraseña ha expirado, por favor, ingrese una nueva.');

            $redirectUrl = $this->router->generate('frontend_web_factory_user_account_edit');
            $redirectResponse = RedirectResponse::create($redirectUrl);
            $event->setResponse($redirectResponse);
        }
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function passwordIsExpired(User $user)
    {
        return $user->getPasswordChangedAt() < new \DateTime('-60 day');
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isFrontend(Request $request)
    {
        return !preg_match('/^\/admin/', $request->getPathInfo());
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isEditionRoute(Request $request)
    {
        $routes = [
            'frontend_web_factory_user_account_edit',
            'frontend_web_factory_user_account_update',
        ];

        return in_array($request->attributes->get('_route'), $routes);
    }
}
