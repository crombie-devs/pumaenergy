<?php

namespace Aper\UserBundle\Event\Listener;

use Aper\UserBundle\Entity\Avatar;
use Aper\UserBundle\Entity\Education;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\Profile;
use Aper\UserBundle\Entity\SentimentalStatus;
use Aper\UserBundle\Entity\Son;
use Aper\UserBundle\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping\PostPersist;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SendProfileNoticeListener
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var array
     */
    private $changeSet;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * SendProfileNoticeListener constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {

        $entity = $event->getEntity();
        $sonsent = false;
        switch (true) {
            case $entity instanceof User:
                $this->user = $entity;
                break;
            case $entity instanceof Employee:
                $this->user = $entity->getUser();
                break;
            case $entity instanceof Profile:
                $this->user = $entity->getEmployee()->getUser();
                break;
            case $entity instanceof Avatar:
                $this->user = $entity->getProfile()->getEmployee()->getUser();
                break;
            case $entity instanceof Education:
                $this->user = $entity->getEmployee()->getUser();
                break;
            case $entity instanceof Son:
                $this->user = $entity->getEmployee()->getUser();

                break;
            case $entity instanceof SentimentalStatus:
                $this->user = $entity->getEmployee()->getUser();
                break;
        }

        $this->changeSet = $event->getEntityChangeSet();

        if ($this->user !== $this->getUser()) {
            $this->user = null;
            $this->changeSet = null;
        }

    }

     /**
     * @param PostPersist $event
     */
    public function postPersist(LifecycleEventArgs $event)
    {

        $what = '-';
        $entity = $event->getEntity();

        switch (true) {

            case $entity instanceof Education:
                $this->user = $entity->getEmployee()->getUser();
                $what = ' (Graduaciones)';
                $txt = ' Nuevo Título: ';
                $data ='';
                $arraye = $entity->getEmployee()->getEducations();
                $arrays = null;
                $this->sendNotice($arraye,$arrays, $txt,$data,$what, $this->user );
                $what2 ="education";
                $this->sendNotice2($what2,$this->user);
                $this->user = null;
                break;
            case $entity instanceof Son:
                $this->user = $entity->getEmployee()->getUser();
                $what = ' (Nacimientos)';
                $txt = ' Nuevo Nacimiento: ';
                $data = '';
                $arrays = $entity->getEmployee()->getSons();
                $arraye = null;
                $this->sendNotice($arraye,$arrays, $txt,$data,$what, $this->user );
                $what2 ="son";
                $this->sendNotice2($what2,$this->user);
                $this->user = null;
                break;
        }

    }



    /**
     * @return User
     */
    private function getUser()
    {
        $tokenStorage = $this->container->get('security.token_storage');
        $token = $tokenStorage->getToken();
        $user = $token ? $token->getUser() : null;

        return $user instanceof User ? $user : null;
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function postUpdate(LifecycleEventArgs $event)
    {
        $what = '-';
        $entity = $event->getEntity();

        switch (true) {

            case $entity instanceof Education:
                $this->user = $entity->getEmployee()->getUser();
                $what = ' (Graduaciones)';
                $txt = ' Cambia Título: ';
                $data ='';
                $arraye = $entity->getEmployee()->getEducations();
                $arrays = null;
                $this->sendNotice($arraye,$arrays, $txt,$data,$what, $this->user );
                $this->user = null;
                break;

            case $entity instanceof Son:
                $this->user = $entity->getEmployee()->getUser();
                $what = ' (Nacimientos)';
                $txt = ' Cambia Nacimiento: ';
                $data = '';
                $arrays = $entity->getEmployee()->getSons();
                $arraye = null;
                $this->sendNotice($arraye,$arrays, $txt,$data,$what, $this->user );
                $this->user = null;
                break;

            case $entity instanceof SentimentalStatus:
                $this->user = $entity->getEmployee()->getUser();
                $what  = ' (Casamiento)';
                $txt   = ' Nueva fecha de Casamiento: ';
                $data  = $entity->getEmployee()->getSentimentalStatus()->getDate();
                if ($data!=null){
                  $data =  $data->format('d/m/Y');
                }else{
                    $data = "(se ha eliminado la fecha)";
                }
                $arrays = null;
                $arraye = null;
                $this->sendNotice($arraye,$arrays,$txt,$data,$what, $this->user );
                $what2 ="sentimental";
                $this->sendNotice2($what2,$this->user);
                $this->user = null;
                break;
        }

    }

    /**
     * @param User $user
     */
    private function sendNotice($arraye,$arrays,$txt, $data,$what, User $user)
    {
        $fromName = $this->container->getParameter('email_from');
        $from = 'noreply@pumaenergy.com';
        $to = [
            // 'gleguizamon@storerk.com.ar', 'fnavarro@hoyts.com.ar', 'MErrasti@storerk.com.ar', 'jjez@storerk.com.ar'
        ];

        $changeSet = $this->changeSet;

        $message = \Swift_Message::newInstance()
            ->setSubject('Un empleado ha cambiado sus datos'. $what)
            ->setFrom(array($from => $fromName))
            ->setTo($to)
            ->setBody($this->renderView($what, '@User/Frontend/email_profile_notice.html.twig',
                compact('user', 'changeSet', 'what', 'data','txt','arrays','arraye' )), 'text/html');

        $mailer = $this->container->get('mailer');
        $mailer->send($message);
    }


    /**
     * @param User $user
     */
    private function sendNotice2($what2, User $user)
    {
        $fromName = $this->container->getParameter('email_from');
        $from = 'noreply@pumaenergy.com';
        $to = $user->getEmail();

        switch ($what2){
            case 'son':
                $subject ="Felicidades por el nacimiento";
                break;
            case 'education':
                $subject ="Felicitaciones por tu graduación";
                break;
            case 'sentimental':
                $subject ="Felicitaciones por tu Casamiento";
                break;
            default: 
                $subject ="Felicitaciones";
                break;
        }

        if ($to) {

            $changeSet = $this->changeSet;

            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom(array($from => $fromName))
                ->setTo($to)
                ->setBody($this->renderView($what2, '@User/Frontend/email_profile_notice_user.html.twig',
                    compact('user', 'changeSet', 'what2')), 'text/html');

            $mailer = $this->container->get('mailer');
            $mailer->send($message);
        }
    }






    /**
     * @param $view
     * @param array $parameters
     * @return string
     */
    public function renderView($what, $view, array $parameters = array())
    {
        $templating = $this->container->get('twig');

        return $templating->render($view, $parameters);
    }

    /**
     * @return array
     */
    private function getRRHHEmails()
    {
        $to = [];
        $userRepository = $this->container->get('doctrine')->getRepository('UserBundle:User');
        foreach ($userRepository->findAllRRHH() as $rrhh) {
            $to[] = [$rrhh->getEmail() => (string)$rrhh->getEmployee()->getProfile()];
        }

        return $to;
    }


}