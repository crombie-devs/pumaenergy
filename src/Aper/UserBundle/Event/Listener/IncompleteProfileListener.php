<?php

namespace Aper\UserBundle\Event\Listener;

use Aper\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class IncompleteProfileListener.
 */
class IncompleteProfileListener
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * IncompleteProfileListener constructor.
     *
     * @param Router                $router
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(Router $router, TokenStorageInterface $tokenStorage)
    {
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $token = $this->tokenStorage->getToken();
        if (!$token) {
            return;
        }

        /* @var User $user */
        $user = $token->getUser();
        if (!$user instanceof User) {
            return;
        }

        $request = $event->getRequest();
        $url = $request->getPathInfo();
        if('/register/resset/see/' == $url){
            return;
        }

        $userIncomplete = $this->isUserIncomplete($user);
        $isFrontend = $this->isFrontend($event->getRequest());
        $isEditionRoute = $this->isEditionRoute($event->getRequest());

        if ($userIncomplete && $isFrontend && !$isEditionRoute) {
            $event->getRequest()->getSession()->getFlashBag()->add('warning', 'Por favor complete su email, su telefono y su operador');

            $redirectUrl = $this->router->generate('frontend_web_factory_user_account_edit');
            $redirectResponse = RedirectResponse::create($redirectUrl);
            $event->setResponse($redirectResponse);
        }
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function isUserIncomplete(User $user)
    {
        if (preg_match('/example\.com$/', $user->getEmail())) {
            return true;
        }

        if (!$user->getEmployee()->getProfile()->getPhoneNumber() || 
            !$user->getEmployee()->getProfile()->getPhoneAreaCode()) {
            return true;
        }

        if (!$user->getEmployee()->getProfile()->getOperator()) {
            return true;
        }

        return false;
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isFrontend(Request $request)
    {
        return !preg_match('/^\/admin/', $request->getPathInfo());
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isEditionRoute(Request $request)
    {
        $routes = [
            'frontend_web_factory_user_account_edit',
            'frontend_web_factory_user_account_update',
        ];

        return in_array($request->attributes->get('_route'), $routes);
    }
}
