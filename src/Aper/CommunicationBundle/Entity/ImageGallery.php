<?php

namespace Aper\CommunicationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use WebFactory\Bundle\FileBundle\Model\Image as BaseEntity;

/**
 * Format
 *
 * @ORM\Entity
 * @ORM\Table(name="comm_gallery_images")
 */
class ImageGallery extends BaseEntity
{
    
    const DIRECTORY = 'uploads/communication/images';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *
     * @var Communication
     * @ORM\ManyToOne(targetEntity="Communication", inversedBy="gallery")
     * @ORM\JoinColumn(name="comm_id", referencedColumnName="id")
     */
    protected $communication;

    /**
     * Set Communication
     *
     * @param Communication $communication
     * @return ImageGallery
     */
    public function setCommunication(Communication $communication = null)
    {
        $this->communication = $communication;

        return $this;
    }
    /**
     * Get communication
     *
     * @return Communication
     */
    public function getCommunication()
    {
        return $this->communication;
    }


}