<?php

namespace Aper\CommunicationBundle\Entity;

use Aper\CommunicationBundle\Entity\CategoryComm;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\Security\Acl\Domain\DoctrineAclCache;

/**
 * CommunicationRepository
 *
 */
class CommunicationRepository extends EntityRepository
{

    /**
     * 
     * @param \Aper\CommunicationBundle\Entity\CategoryComm $categorycomm
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createQueryBuilderByCategoryAndTag(CategoryComm $categorycomm = null)
    {
        $queryBuilder = $this->createQueryBuilder('Communication')
                ->addSelect('Image')
           //     ->addSelect('Comment')
           //      ->addSelect('User')
               // ->addSelect('Profile')
           //      ->addSelect('Tag')
                ->leftJoin('Communication.mainImage', 'Image')
           //     ->leftJoin('Communication.tags', 'Tag')
          //        ->leftJoin('Communication.comments', 'Comment', Join::WITH, "Comment.status = 'published'")
             //  ->leftJoin('Comment.user', 'User')
             //   ->leftJoin('User.profile', 'Profile')
             //   ->where('Communication.active = true')
                ->orderBy('Communication.createdAt', 'DESC');

        if ($categorycomm) {
            $queryBuilder->andWhere('Communication.category = :category')
                    ->setParameter('category', $categorycomm->getId());
        }



        return $queryBuilder;
    }


}
