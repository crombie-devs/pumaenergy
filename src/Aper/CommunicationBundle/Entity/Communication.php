<?php

namespace Aper\CommunicationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Communication
 *
 * @ORM\Table(name="communication")
 * @ORM\Entity(repositoryClass="Aper\CommunicationBundle\Entity\CommunicationRepository")
 */
class Communication
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var CategoryComm
     * @ORM\ManyToOne(targetEntity="Aper\CommunicationBundle\Entity\CategoryComm")
     * @Assert\NotBlank()
     */
    protected $category;

     /**
     *
     * @var Image
     * @ORM\OneToOne(targetEntity="Image", mappedBy="communication", cascade={"all"})
     * @Assert\NotBlank()
     * @Assert\Valid()
     */
    protected $mainImage;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $title;


    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sent_at", type="datetime")
     */
    private $sentAt;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", type="boolean", nullable=true)
     */
    protected $active;


    /**
     * @var ImageGallery[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Aper\CommunicationBundle\Entity\ImageGallery", mappedBy="communication", cascade={"all"}, orphanRemoval=true)
     */
    private $gallery;


    /**
     * @var string[]|ArrayCollection
     *
     * @ORM\Column(name="mailusers", type="array")
     */
    private $mailusers;

    /**
     * @var string[]|ArrayCollection
     *
     * @ORM\Column(name="mailstores", type="array")
     *
     */
    private $mailstores;

    /**
     * @var string[]|ArrayCollection
     *
     * @ORM\Column(name="mailprofiles", type="array")
     */
    private $mailprofiles;

    /**
     * @var boolean
     *
     * @ORM\Column(name="mailall", type="boolean")
     */
    private $mailall;

    /**
     * @var string[]|ArrayCollection
     *
     * @ORM\Column(name="maillist", type="array")
     */
    private $maillist;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sent", type="boolean")
     */
    private $sent;

    /**
     * @var string[]
     *
     * @ORM\Column(name="custom_emails", type="simple_array", nullable=true)
     */
    private $customEmails;

    /**
     * Constructor
     */
    public function __construct($title = null, $content = null, CategoryComm $category = null)
    {
        $this->title = $title;
        $this->content = $content;
        $this->category = $category;
        $this->active = false;
        $this->gallery = new ArrayCollection();
        $this->customEmails = [];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Communication
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set content
     *
     * @param string $content
     * @return Communication
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Communication
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Communication
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }



    public function getMainImage()
    {
        return $this->mainImage;
    }

    public function setMainImage(Image $mainImage)
    {
        $this->mainImage = $mainImage;

        $mainImage->setCommunication($this);
    }



    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }



   //////////////////////////// //GALERIA//*/////////////////

    /**
     * Add gallery
     *
     * @param \Aper\CommunicationBundle\Entity\ImageGallery $gallery1
     * @return Communication
     */
    public function addGallery(\Aper\CommunicationBundle\Entity\ImageGallery $gallery1)
    {
        $gallery1->setCommunication($this);
        $this->gallery[] = $gallery1;

        return $this;
    }


    public function removeGallery(ImageGallery $galeries)
    {
        $this->gallery->removeElement($galeries);
    }

    /**
     * Get gallery []
     *
     */
    public function getGallery()
    {
        return $this->gallery;
    }



    /**
     * Set mailusers.
     *
     * @param string $mailusers
     *
     * @return Communication
     */
    public function setMailusers($mailusers)
    {
        $this->mailusers = $mailusers;

        return $this;
    }

    /**
     * Get mailusers.
     *
     * @return string
     */
    public function getMailusers()
    {

        return $this->mailusers;

    }

    /**
     * Set mailstores.
     *
     * @param string $mailstores
     *
     * @return Communication
     */
    public function setMailstores($mailstores)
    {
        $this->mailstores = $mailstores;

        return $this;
    }

    /**
     * Get mailstores.
     *
     * @return string
     */
    public function getMailstores()
    {

        return $this->mailstores;

    }

    /**
     * Set mailprofiles.
     *
     * @param string $mailprofiles
     *
     * @return Communication
     */
    public function setMailprofiles ($mailprofiles)
    {
        $this->mailprofiles = $mailprofiles;

        return $this;
    }

    /**
     * Get mailprofiles.
     *
     * @return string
     */
    public function getMailprofiles()
    {

        return $this->mailprofiles;

    }

    /**
     * Set mailall.
     *
     * @param boolean $mailall
     *
     * @return Communication
     */
    public function setMailall ($mailall)
    {
        $this->mailall = $mailall;

        return $this;
    }

    /**
     * Get mailall.
     *
     * @return boolean
     */
    public function getMailall()
    {

        return $this->mailall;

    }

    /**
     * Set maillist
     *
     * @param string $maillist
     *
     * @return Communication
     */
    public function setMaillist ($maillist)
    {
        $this->maillist = $maillist;

        return $this;
    }

    /**
     * Get maillist.
     *
     * @return string
     */
    public function getMaillist()
    {

        return $this->maillist;

    }

    /**
     * @param $mail
     *
     * @return $this
     */
    public function addMail($mail)
    {
            $this->maillist[] = $mail;

        return $this;

    }
    /**
     * Set sent.
     *
     * @param boolean $sent
     *
     * @return Communication
     */
    public function setSent ($sent)
    {
        $this->sent = $sent;

        return $this;
    }

    /**
     * Get sent.
     *
     * @return boolean
     */
    public function getSent()
    {

        return $this->sent;

    }


    /**
     * Set sentAt
     *
     * @param \DateTime $sentAt
     * @return Communication
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * Get sentAt
     *
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * @return string[]
     */
    public function getCustomEmails()
    {
        return $this->customEmails;
    }

    /**
     * @param string[] $customEmails
     */
    public function setCustomEmails($customEmails)
    {
        $this->customEmails = $customEmails;
    }

    /**
     * @return string
     */
    public function getCustomEmailsAsString()
    {
        return implode(', ', $this->customEmails);
    }
}