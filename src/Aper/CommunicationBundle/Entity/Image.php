<?php

namespace Aper\CommunicationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use WebFactory\Bundle\FileBundle\Model\Image as BaseEntity;

/**
 * Format
 *
 * @ORM\Entity
 * @ORM\Table(name="comm_images")
 */
class Image extends BaseEntity
{
    
    const DIRECTORY = 'uploads/communications/images';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     *
     * @var Communication
     * @ORM\OneToOne(targetEntity="Communication", inversedBy="mainImage", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="comm_id", referencedColumnName="id")
     */
    protected $communication;



    /**
     * Set book
     *
     * @param Communication $communication
     * @return Image
     */
    public function setCommunication(Communication $communication = null)
    {
        $this->communication = $communication;

        return $this;
    }

    /**
     * Get communication
     *
     * @return Communication
     */
    public function getCommunication()
    {
        return $this->communication;
    }








}