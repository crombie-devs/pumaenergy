<?php

namespace Aper\CommunicationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Xmon\ColorPickerTypeBundle\Validator\Constraints as XmonAssertColor;



/**
 * CategoryComm
 *
 * @ORM\Table(name="comm_categories")
 * @ORM\Entity(repositoryClass="Aper\CommunicationBundle\Entity\CategoryCommRepository")
 * @UniqueEntity("name")
 */
class CategoryComm
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $color;

    /**
     *
     * @var Image
     * @ORM\OneToOne(targetEntity="ImageCat", mappedBy="categorycomm", cascade={"all"})
     * @Assert\Valid()
     */
    protected $mainImage;




    ////////////////////////////////////////////////////////////////////////////////////////



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CategoryComm
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name;
    }


    public function getMainImage()
    {
        return $this->mainImage;
    }

    public function setMainImage(ImageCat $mainImage)
    {
        $this->mainImage = $mainImage;

        $mainImage->setCategoryComm($this);
    }


    /**
     * Set color
     *
     * @param string $color
     * @return CategoryComm
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }



}