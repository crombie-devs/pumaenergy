<?php

namespace Aper\CommunicationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use WebFactory\Bundle\FileBundle\Model\Image as BaseEntity;

/**
 * Format
 *
 * @ORM\Entity
 * @ORM\Table(name="category_comm_images")
 */
class ImageCat extends BaseEntity
{
    
    const DIRECTORY = 'uploads/categorycomms/images';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     *
     * @var CategoryComm
     * @ORM\OneToOne(targetEntity="CategoryComm", inversedBy="mainImage", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="comm_id", referencedColumnName="id")
     */
    protected $categorycomm;



    /**
     * Set CategoryComm
     *
     * @param CategoryComm $categorycomm
     * @return ImageCat
     */
    public function setCategoryComm(CategoryComm $categorycomm = null)
    {
        $this->categorycomm = $categorycomm;

        return $this;
    }

    /**
     * Get categorycomm
     *
     * @return CategoryComm
     */
    public function getCategoryComm()
    {
        return $this->categorycomm;
    }






}