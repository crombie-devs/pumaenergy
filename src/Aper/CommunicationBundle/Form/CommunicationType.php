<?php

namespace Aper\CommunicationBundle\Form;

use Aper\StoreBundle\StoreBundle;
use Aper\StoreBundle\Entity\Store;
use Aper\UserBundle\UserBundle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use Genemu;

class CommunicationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('mailprofiles', 'genemu_jqueryselect2_choice', array(
                'choices' => array(
                    'ROLE_HEAD_OFFICE'          => 'Perfil 1 (Comercial / Oficina Central)',
                    'ROLE_MANAGER'              => 'Perfil 2 (Operadores / Jefes)',
                    'ROLE_EMPLOYEE'             => 'Perfil 3 (Playeros)',
                    'ROLE_MANAGER_GLOBAL'       => 'Perfil 4 (Administración / Gerentes)',
                    'ROLE_ADMIN'                => 'Perfil 5 (Administrador Backend)',
                    'ROLE_ADMIN_CONSULTA'       => 'Perfil 6 (Adminitrador de Consulta)',
                    'ROLE_ADMIN_CAPACITACION'   => 'Perfil 7 (Adminitrador de Capacitaciones)',
            ),
              'multiple' =>true,
              'required' =>false,

            ))


            ->add('mailstores', 'genemu_jqueryselect2_choice', array(
                'choices' => $options['stores'],
                'multiple' =>true,
                'required' =>false,
                'mapped'=>true,

            ))


            ->add('mailusers', 'genemu_jqueryselect2_choice', array(
                'choices' => $options['users'],
                'multiple' =>true,
                'required' =>false,
                'mapped'=>true,
            ))

            ->add('customActualEmails', TextType::class, [
                'property_path' => 'getCustomEmailsAsString',
                'required' =>false,
                'disabled' => true,
            ])

            ->add('clearCustomActualEmails', CheckboxType::class, [
                'mapped' => false,
                'required' =>false,
                'label' => false,
            ])

            ->add('customEmails', FileType::class, [
                'mapped' => false,
                'required' =>false,
            ])


            ->add('mailall', null, array(
                'required' => false,
            ))

                ->add('title', 'text', array(

            ))

            ->add('category', EntityType::class, array(
                'class' => 'AperCommunicationBundle:CategoryComm',
                'query_builder' => function(EntityRepository $r) {
                    return $r->createQueryBuilder('name')->orderBy('name.name', 'asc');
                },

            ))


                ->add('active', null, array(
                    'required' => false,
                ))

                ->add('content', 'ckeditor', array(
                    'config' => array(
						'config_name' => 'my_config',
                        'height' => '450px',
                    ),
                ))


            ->add('gallery', CollectionType::class, [
                'type' => ImageGalleryType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'widget_add_btn' => [
                    'label' => 'Galería +',
                    'icon' => '',
                    'attr' => [
                        'class' => 'btn btn-primary',
                        'title' => 'GALERÍA +',
                    ],
                ],
                'options' => [
                    'widget_remove_btn' => [
                        'label' => 'ELIMINAR',
                        'attr' => [
                            'class' => 'btn btn-danger',
                            'title' => 'ELIMINAR',
                        ],
                        'icon' => '',
                        'wrapper_div' => [
                            'class' => 'span12',
                        ],
                    ],
                    'attr' => [
                        'class' => 'span12',
                    ],
                    'label' => false,
                    'error_bubbling' => false,
                    'show_child_legend' => false,
                ],
                'show_legend' => false,
                'show_child_legend' => false,
            ])


            ->add('mainImage', new ImageType, array(
            ))


               ->add('save', SubmitType::class)
        ;



    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\CommunicationBundle\Entity\Communication',
            'stores' => [],
            'users' => []
        ));
    }

    public function getName()
    {
        return 'communication';
    }

}
