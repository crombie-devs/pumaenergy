<?php

namespace Aper\CommunicationBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\FileBundle\Form\ImageType as Base;

class ImageGalleryType extends Base
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\CommunicationBundle\Entity\ImageGallery'
        ));
    }

    public function getName()
    {
        return 'ImageGallery';
    }
}
