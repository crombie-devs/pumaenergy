<?php

namespace Aper\CommunicationBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\FileBundle\Form\ImageType as Base;

class ImageType extends Base
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\CommunicationBundle\Entity\Image'
        ));
    }

    public function getName()
    {
        return 'image';
    }
}
