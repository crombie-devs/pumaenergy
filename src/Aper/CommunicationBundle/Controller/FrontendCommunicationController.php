<?php

namespace Aper\CommunicationBundle\Controller;

use Aper\BenefitBundle\Model\Category;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WebFactory\Bundle\UserBundle\Controller\ContextController;

/**
 * Class CommunicationController
 *
 * @Route("communication")
 */
class FrontendCommunicationController extends ContextController
{
    /**
     * @Route("/")
     *
     * @param
     * @return Response
     */
    public function indexAction()
    {
        //$repository = $this->getDoctrine()->getRepository('AperCommunicationBundle:Communication');
        $communications = [];


        return $this->render('AperCommunicationBundle:Frontend/Communication:index.html.twig', [
            'communications' => $communications,
        ]);
    }

    /**
     * @Route("/organization")
     *
     * @param
     * @return Response
     */
    public function indexOrganization()
    {

        $communications = [];


        return $this->render('AperCommunicationBundle:Frontend/Communication:org.html.twig', [
            'communications' => $communications,
        ]);
    }

    /**
     * @Route("/show/{category}")
     *
     * @param
     * @return Response
     */
    public function showAction($category)
    {
        //$repository = $this->getDoctrine()->getRepository('AperBenefitBundle:Benefit');
        $benefits = null; //$repository->findByRolesAllowed($this->getUser()->getRoles(), $category);

        return $this->render('AperCommunicationBundle:Frontend/Communication:show.html.twig', [
                       'benefits' => $benefits,
        ]);
    }
}