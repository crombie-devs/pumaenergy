<?php

namespace Aper\CommunicationBundle\Controller\Backend;

use Aper\StoreBundle\Entity\Store;
use Aper\CommunicationBundle\Entity\Communication;
use Aper\CommunicationBundle\Form\CommunicationType;
use Aper\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class CommunicationController extends Controller
{
    /**
     * Lists all Communication entities.
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $news = $this->getDoctrine()->getRepository(Communication::class)->createQueryBuilderByCategoryAndTag();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $news, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('AperCommunicationBundle:Backend/Communication:index.html.twig',
            array('pagination' => $pagination, 'news' => $news));

    }
    /**
     * Creates a new Communication entity.
     *
     * @Method("POST")
     * @Template("AperCommunicationBundle:Backend/Communication:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $store =[];
        $storesE = $this->getDoctrine()->getRepository(Store::class)->findAll();

        foreach ($storesE as $stores){
            $store[$stores->getId()] = $stores->getCode(). ' - '. $stores->getLocation();
        }

        $user =[];
        $usersE = $this->getDoctrine()->getRepository(User::class)->findUsersMails();
        foreach ($usersE as $users){
            $user[$users->getId()] = $users->getEmail();

        }


        $entity  = new Communication();
        $form = $this->createForm(new CommunicationType(), $entity,['stores' => $store, 'users' => $user  ] );
        $form->bind($request);



        if ($form->isValid() && $form->isSubmitted()) {

            $profiles = $form->get("mailprofiles")->getData();
            $users = $form->get("mailusers")->getData();
            $stores = $form->get("mailstores")->getData();



            $em = $this->getDoctrine()->getManager();

            $entity->setUpdatedAt(new \DateTime("now"));
            $entity->setCreatedAt(new \DateTime("now"));
            $entity->setSentAt(new \DateTime("now"));
            $entity->setSent(false);
            $entity->setMailstores($stores);
            $entity->setMailprofiles($profiles);
            $entity->setMailusers($users);

            $clearCustomActualEmails = $form->get('clearCustomActualEmails')->getData();
            if ($clearCustomActualEmails) {
                $entity->setCustomEmails([]);
            }
            /* @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('customEmails')->getData();
            if ($uploadedFile && $uploadedFile->isValid()) {
                $customEmailsPathname = $uploadedFile->getPathname();
                $emails = $this->getCustomEmails($customEmailsPathname);
                $entity->setCustomEmails($emails);
            }


            $em->persist($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Registro creado');

            return $this->redirect($this->generateUrl('aper_comm_index'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }




    /**
     * Displays a form to create a new Communication entity.
     *
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $store =[];
        $storesE = $this->getDoctrine()->getRepository(Store::class)->findAll();
        foreach ($storesE as $stores){
          $store[$stores->getId()] = $stores->getCode(). ' - '. $stores->getLocation();

       }

        $user =[];
        $usersE = $this->getDoctrine()->getRepository(User::class)->findUsersMails();
        foreach ($usersE as $users){
            $user[$users->getId()] = $users->getEmail();

        }


        $entity = new Communication();
            $form   = $this->createForm(new CommunicationType(), $entity, ['stores' => $store , 'users' => $user,
           // $form   = $this->createForm(new CommunicationType(), $entity, [
            'action' => $this->generateUrl('aper_comm_create')
        ]);




        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );





    }

    /**
     * Finds and displays a Communication entity.
     *
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {

        $repository = $this->getDoctrine()->getRepository('AperCommunicationBundle:Communication');
        $news = $repository->find($id);
        if(!$news){
            $messageException = 'Comunicación no encontrada';
            throw $this->createNotFoundException($messageException);
        }


        $mailusers=[];
        $users = $news->getMailusers();

        foreach($users as $user){
            $user = $this->getDoctrine()->getRepository(User::class)->findOneById($user);
            $mailusers[]=$user->getEmail();
        }

        $stores=[];
        $store = $news->getMailStores();
        foreach($store as $cine){
            $storeil = $this->getDoctrine()->getRepository(Store::class)->findOneById($cine);
            $stores[]=$storeil->getLocation();
        }

        return $this->render('AperCommunicationBundle:Backend/Communication:show.html.twig', array('news' => $news, 'mailusers'=>$mailusers,
            'stores'=>$stores));



    }

    /**
     * Displays a form to edit an existing Communication entity.
     *
     * @Route("/{id}/edit", name="backend_comm_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Request $request,$id)
    {

        $store =[];
        $storesE = $this->getDoctrine()->getRepository(Store::class)->findAll();

        foreach ($storesE as $stores){
            $store[$stores->getId()] = $stores->getCode(). ' - '. $stores->getLocation();
        }


        $user =[];
        $usersE = $this->getDoctrine()->getRepository(User::class)->findUsersMails();
        foreach ($usersE as $users){
            $user[$users->getId()] = $users->getEmail();

        }


        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AperCommunicationBundle:Communication')->find($id);
        $form = $this->createForm(CommunicationType::class, $entity ,['stores' => $store, 'users' => $user ]);
        $form->handleRequest($request);

        $stores = $entity->getMailstores();
        $users = $entity->getMailusers();



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Communication entity.');
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $clearCustomActualEmails = $form->get('clearCustomActualEmails')->getData();
            if ($clearCustomActualEmails) {
                $entity->setCustomEmails([]);
            }

            /* @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('customEmails')->getData();
            if ($uploadedFile && $uploadedFile->isValid()) {
                $customEmailsPathname = $uploadedFile->getPathname();
                $emails = $this->getCustomEmails($customEmailsPathname);
                $entity->setCustomEmails($emails);
            }

            $entity->setUpdatedAt(new \DateTime("now"));
            $entity->setSentAt(new \DateTime("now"));
            $entity->setSent(false);
            $em->flush();
            $this->addFlash('success', 'La comunicación ha sido modificada');

            return $this->redirectToRoute('aper_comm_index');
        }

        return $this->render('AperCommunicationBundle:Backend/Communication:edit.html.twig',
            array('stores' => $stores,'users' => $users,'entity' => $entity, 'form' => $form->createView()));
    }


    /**
     * Deletes a Communication entity.
     *
     * @Route("/{id}/delete", name="backend_news_delete")
     * @Method({"DELETE", "GET"})
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AperCommunicationBundle:Communication')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Communication entity.');
            }

            $em->remove($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Registro Eliminado');
        }

        return $this->redirect($this->generateUrl('aper_comm_index'));
    }

    /**
     * Creates a form to delete a Communication entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id), array('csrf_protection' => false))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * @param $customEmailsPathname
     * @return array|bool
     */
    private function getCustomEmails($customEmailsPathname)
    {
        $excel = $this->get('phpexcel')->createPHPExcelObject($customEmailsPathname);
        $sheet = $excel->getActiveSheet();
        $highestRow = $sheet->getHighestRow();

        $emails = array();
        for ($i = 1; $i <= $highestRow; $i++) {
            $emails[] = $sheet->getCell('A' . $i)->getValue();
        }

        $emails = array_map('trim', $emails);
        $emails = array_filter($emails, function ($e) {
            return filter_var($e, FILTER_VALIDATE_EMAIL);
        });

        return $emails;
    }
}
