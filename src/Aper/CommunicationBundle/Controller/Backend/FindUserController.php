<?php

namespace Aper\CommunicationBundle\Controller\Backend;

use Aper\CommunicationBundle\Entity\Communication;
use Aper\CommunicationBundle\Form\CommunicationType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class FindUserController extends Controller
{

    public function profileAction(Request $request)
    {
        $terms = $request->get('q');

        $array[] = ['id' => 1, 'text' => 'Perfil 1 (Oficina Central)'];
        $array[] = ['id' => 2, 'text' => 'Perfil 2 (Gerencia)'];
        $array[] = ['id' => 3, 'text' => 'Perfil 3 (Crew)'];
        $array[] = ['id' => 4, 'text' => 'Perfil 4 (Recursos Humanos)'];
        $array[] = ['id' => 5, 'text' => 'Perfil 5 (Administrador)'];
        $array[] = ['id' => 6, 'text' => 'Perfil 6 (Adminitrador de Consulta)'];
        $array[] = ['id' => 7, 'text' => 'Perfil 7 (Adminitrador de Capacitaciones)'];
        
        return JsonResponse::create($array);
    }



    public function userAction(Request $request)
    {
       $terms = $request->get('q');

       $users = $this->getDoctrine()->getRepository('UserBundle:User')->createQueryBuilder('User')
           ->where('User.email LIKE :email')
           ->orwhere ('User.username LIKE :name')
           ->setParameter('email', "%{$terms}%")
           ->setParameter('name', "%{$terms}%")
           ->setMaxResults(20)
           ->getQuery()->getResult();

       $array = [];
       foreach ($users as $user) {
           $array[] = ['id' => $user->getId(), 'text' => $user->getEmail()];
       }

       return JsonResponse::create($array);
    }


    public function storeAction(Request $request)
    {
        $terms = $request->get('q');

        $stores = $this->getDoctrine()->getRepository('StoreBundle:Store')->createQueryBuilder('Store')
            ->where('Store.location LIKE :location')
            ->setParameter('location', "%{$terms}%")
            ->distinct('Store.location')
            ->setMaxResults(20)
            ->getQuery()->getResult();

        $array = [];
        foreach ($stores as $store) {
            $array[] = ['id' => $store->getId(), 'text' => $store->getLocation()];
        }

        return JsonResponse::create($array);
    }
}
