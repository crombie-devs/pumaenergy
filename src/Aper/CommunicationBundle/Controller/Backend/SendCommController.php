<?php

namespace Aper\CommunicationBundle\Controller\Backend;


use Aper\StoreBundle\Entity\Store;
use Aper\CommunicationBundle\Entity\CategoryComm;
use Aper\CommunicationBundle\Entity\Communication;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Aper\UserBundle\Entity\User;
use Symfony\Component\HttpKernel\Tests\controller_func;


class SendCommController extends ContainerAwareCommand
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function configure()
    {
        $this
            ->setName('app:sendcomm_sending')
            ->setDescription('Envía por correo electrónico de comunicaciones ')

        ;
    }

    public function indexAction() //envía todos
    {
        $cantmails = 0;
        $what = "Comunicacion Interna";
        $from = 'mailing@pumaenergyarg.com.ar';

        $now = new \DateTime();
        $qb = $this->getContainer()->get('doctrine')->getRepository('AperCommunicationBundle:Communication')->createQueryBuilder('C');


        $qb
            ->where('C.sent = false')
            ->andwhere('C.active = true');


        $perfiles = $qb->getQuery()->getResult();

        $countProfiles = count($perfiles);

        if ($perfiles) {
            foreach ($perfiles as $comm) {

                //ACTUALIZA MAILS
                $users = $comm->getMailusers();
                $profiles = $comm->getMailprofiles();
                $stores = $comm->getMailstores();


                if ($comm->getMailall()) {
                    $allusers = $this->getContainer()->get('doctrine')->getRepository(User::class)->findUsersMails();
                    foreach ($allusers as $user) {
                        $comm->addMail($user->getEmail());
                    }
                } else {
                    $this->depureMail($users, $profiles, $stores, $comm);
                }
                //TERMINA DE ACTUALIZAR MAILS


                //ACTUALIZA CAMPO ENVIADO
                $em = $this->getContainer()->get('doctrine')->getManager();
                $entity = $em->getRepository('AperCommunicationBundle:Communication')->find($comm->getId());
                $entity->setSent(true);
                $entity->setMaillist(null);
                $em->flush();

                //OBTIENE MAILS DEFINITIVOS
                $mailTargets = $comm->getMaillist();
                $entity->setMaillist($mailTargets);
                $em->flush();

                if ($mailTargets) {
                    //ENVÍA TODOS LOS MAILS
                    foreach ($mailTargets as $mailTarget) {
                        $cantmails++;
                        $fromName = $this->getContainer()->getParameter('email_from');
                        $path = $this->getContainer()->getParameter('absolute_url');
                        $twig = $this->getContainer()->get('twig');
                        $body = $twig->render('@AperCommunication/Frontend/email_comm_notice.html.twig', array('comm' => $comm, 'path' => $path));
                        $mail = $mailTarget;
                        $message = \Swift_Message::newInstance()
                            ->setSubject($what)
                            ->setFrom(array($from => $fromName))
                            ->setTo($mail)
                            ->setBody($body, 'text/html');

                        $mailer = $this->getContainer()->get('mailer');
                        $mailer->send($message);
                    }
                }
            }
            return Response::create('Cantidad de comunicaciones enviadas: ' . $countProfiles . ' a un total de ' . $cantmails . ' casillas de mails.');
        }
    }

    public function editAction(Request $request, $id) //envía uno
    {

        $what = "Comunicacion Interna";
        $from = 'mailing@pumaenergyarg.com.ar';

        $now = new \DateTime();
        $qb = $this->getContainer()->get('doctrine')->getRepository('AperCommunicationBundle:Communication')->createQueryBuilder('C');
        $qb
            ->where('C.id = :ID')
            ->setParameter('ID',$id)
        ;

        $perfiles = $qb->getQuery()->getResult();




        foreach ($perfiles as $comm) {


            //ACTUALIZA CAMPO ENVIADO
            $em = $this->getContainer()->get('doctrine')->getManager();
            $entity = $em->getRepository('AperCommunicationBundle:Communication')->find($comm->getId());
            $entity->setSent(true);
            $entity->setMaillist([]);
            $em->flush();


            //ACTUALIZA MAILS
            $users= $comm->getMailusers();
            $profiles =$comm->getMailprofiles();
            $stores= $comm->getMailstores();


            if ($comm->getMailall()) {
                $allusers = $this->getContainer()->get('doctrine')->getRepository(User::class)->findUsersMails();
                foreach($allusers as $user){
                    $comm->addMail($user->getEmail());
                }
            }
            else {
                $this->depureMail($users, $profiles, $stores, $comm);
            }
            //TERMINA DE ACTUALIZAR MAILS


            //OBTIENE MAILS DEFINITIVOS
            $mailTargets = $comm->getMaillist();
            $entity->setMaillist($mailTargets);
            $em->flush();

            //ENVÍA TODOS LOS MAILS

            $countProfiles =  count($mailTargets);


            foreach ($mailTargets as $mailTarget) {


                $fromName = $this->getContainer()->getParameter('email_from');
                $path = $this->getContainer()->getParameter('absolute_url');
                $twig = $this->getContainer()->get('twig');
                $body = $twig->render('@AperCommunication/Frontend/email_comm_notice.html.twig', array('comm' => $comm, 'path' => $path));
                $mail = $mailTarget;
                $message = \Swift_Message::newInstance()
                    ->setSubject($what)
                    ->setFrom(array($from => $fromName))
                    ->setTo($mail)
                    ->setBody($body, 'text/html');

                $mailer = $this->getContainer()->get('mailer');
                $mailer->send($message);
            }
        }

        $request->getSession()->getFlashBag()->add('success', 'Cantidad de Mails enviados:'.$countProfiles);

        $redirectUrl = $this->getContainer()->get('router')->generate('aper_comm_index');
        return $this->getContainer()->get('controller')->redirect($redirectUrl);

    }


    public function depureMail($users, $profiles, $stores, $entity){


        //AGREGA MAILS DE USUARIOS
       if ($users) {

           foreach ($users as $user) {
               $usermail = $this->getContainer()->get('doctrine')->getRepository(User::class)->findOneById($user);
               if ($usermail){
               $entity->addMail($usermail->getEmail());
               }
           }
       }

        //AGREGA MAILS DE PERFILES

       if ($profiles) {
           foreach ($profiles as $profile) {
               $usersp = $this->getContainer()->get('doctrine')->getRepository(User::class)->findUsersByProfile($profile);
               foreach ($usersp as $user) {
                   $entity->addMail($user->getEmail());
               }
           }
       }
        //   AGREGA MAILS DE STORES
       $code = null;
        if ($stores) {
            foreach ($stores as $store) {
                $storeentity = $this->getContainer()->get('doctrine')->getRepository(Store::class)->findOneById($store);
                if ($storeentity) {
                    $code = $storeentity->getCode();
                }

                    $usersc = $this->getContainer()->get('doctrine')->getRepository(User::class)->findUsersByStore($code);

                if ($usersc) {
                    foreach ($usersc as $user) {
                        $entity->addMail($user->getEmail());
                    }
                }
            }
        }
        /* @var Communication $entity */
        foreach ($entity->getCustomEmails() as $email) {
            $entity->addMail($email);
        }
    }
}