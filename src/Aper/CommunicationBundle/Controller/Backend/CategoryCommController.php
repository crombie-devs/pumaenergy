<?php

namespace Aper\CommunicationBundle\Controller\Backend;

use Aper\CommunicationBundle\Entity\CategoryComm;
use Aper\CommunicationBundle\Form\CategoryCommType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class CategoryCommController extends Controller
{
    /**
     * Lists all Category entities.
     *
     * @Method("GET|POST")
     */
    public function indexAction(Request $request)
    {

        $categories = $this->getDoctrine()->getRepository(CategoryComm::class)->findAll();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $categories, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('AperCommunicationBundle:Backend/CategoryComm:index.html.twig', array('pagination' => $pagination, 'categories' => $categories));

    }

    /**
     * Creates a new Category entity.
     *
     * @Method("POST")
     * @Template("AperCommunicationBundle:Backend/CategoryComm:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $category = new CategoryComm();

        $form = $this->createForm(CategoryCommType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash('success', 'La categoría ha sido creada');

            return $this->redirectToRoute('aper_categorycomm_index');
        }

        return $this->render('AperCommunicationBundle:Backend/CategoryComm:new.html.twig', array('form' => $form->createView()));
    }

    /**
     * Displays a form to create a new Category entity.
     *
     * @Route("/new", name="backend_categorycomm_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {

        $entity = new CategoryComm();
        $form = $this->createForm(new CategoryComm(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     * @Route("/{id}/edit", name="backend_categorycomm_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AperCommunicationBundle:CategoryComm')->find($id);
        $form = $this->createForm(CategoryCommType::class, $category);
        $form->handleRequest($request);

        $color = $category->getColor();
        if(!$category){
            throw $this->createNotFoundException('Categoría no encontrada');
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $em->flush();

            $this->addFlash('success', 'La categoría ha sido modificada');

            return $this->redirectToRoute('aper_categorycomm_index');
        }

        return $this->render('AperCommunicationBundle:Backend/CategoryComm:edit.html.twig', array('color' => $color,'category' => $category, 'form' => $form->createView()));
    }

    /**
     * Deletes a Category entity.
     *
     * @Route("/{id}/delete", name="backend_categorycomm_delete")
     * @Method({"DELETE", "GET"})
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AperCommunicationBundle:CategoryComm')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Category entity.');
            }

            try {
                $em->remove($entity);
                $em->flush();
                $request->getSession()->getFlashBag()->add('success', 'Registro eliminado');
            } catch (\Exception $exc) {
                $request->getSession()->getFlashBag()->add('error', $exc->getMessage());
            }
        }

        return $this->redirect($this->generateUrl('aper_categorycomm_index'));
    }

    /**
     * Creates a form to delete a Category entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id), array('csrf_protection' => false))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
