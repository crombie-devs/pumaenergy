<?php

namespace Aper\PoliticsAndProceduresBundle\Controller\Backend;

use Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedureCategory;
use Aper\PoliticsAndProceduresBundle\Form\Backend\PoliticAndProcedureCategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PoliticAndProcedureCategoryController
 */
class PoliticAndProcedureCategoryController extends Controller
{
    /**
     * Lists all Politics And Procedure Categories entities.
     *
     * @Route("/", name="admin_politic_and_procedure_category_index")
     * @Method("GET|POST")
     */
    public function indexAction(Request $request)
    {
        $politicAndProcedureCategory = $this->getDoctrine()->getRepository(PoliticAndProcedureCategory::class)->getQueryBuilder();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $politicAndProcedureCategory,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@AperPoliticsAndProcedures/Backend/PoliticAndProcedureCategory/index.html.twig', [
            'pagination' => $pagination,
            'politicAndProcedureCategory' => $politicAndProcedureCategory,
        ]);
    }

    /**
     * Creates a new politicAndProcedureCategory entity.
     *
     * @Route("/new", name="admin_politic_and_procedure_category_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $politicAndProcedureCategory = new PoliticAndProcedureCategory();
        $form = $this->createForm(new PoliticAndProcedureCategoryType(), $politicAndProcedureCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($politicAndProcedureCategory);
            $em->flush();

            $this->addFlash('success', 'Categoria Creada.');

            return $this->redirectToRoute('admin_politic_and_procedure_category_index');
        }

        return $this->render('@AperPoliticsAndProcedures/Backend/PoliticAndProcedureCategory/new.html.twig', [
            'politicAndProcedureCategory' => $politicAndProcedureCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a politicAndProcedureCategory entity.
     *
     * @Route("/{id}", name="admin_politic_and_procedure_category_show")
     * @Method("GET")
     */
    public function showAction(PoliticAndProcedureCategory $politicAndProcedureCategory)
    {
        $deleteForm = $this->createDeleteForm($politicAndProcedureCategory);

        return $this->render('@AperPoliticsAndProcedures/Backend/PoliticAndProcedureCategory/show.html.twig', [
            'politicAndProcedureCategory' => $politicAndProcedureCategory,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing politicAndProcedureCategory entity.
     *
     * @Route("/{id}/edit", name="admin_politic_and_procedure_category_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PoliticAndProcedureCategory $politicAndProcedureCategory)
    {
        $deleteForm = $this->createDeleteForm($politicAndProcedureCategory);
        $editForm = $this->createForm(PoliticAndProcedureCategoryType::class, $politicAndProcedureCategory);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Categoria Actualizada.');

            return $this->redirectToRoute('admin_politic_and_procedure_category_index');
        }

        return $this->render('@AperPoliticsAndProcedures/Backend/PoliticAndProcedureCategory/edit.html.twig', [
            'politicAndProcedureCategory' => $politicAndProcedureCategory,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a politicAndProcedureCategory entity.
     *
     * @Route("/{id}/delete", name="admin_politic_and_procedure_category_delete")
     */
    public function deleteAction(Request $request, PoliticAndProcedureCategory $politicAndProcedureCategory)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($politicAndProcedureCategory);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('politic_and_procedure_category.actions.edition.successful'));

        return $this->redirectToRoute('admin_politic_and_procedure_category_index');
    }

    /**
     * Creates a form to delete a politicAndProcedureCategory entity.
     *
     * @param PoliticAndProcedureCategory $politicAndProcedureCategory The politicAndProcedureCategory entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PoliticAndProcedureCategory $politicAndProcedureCategory)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_politic_and_procedure_category_delete', ['id' => $politicAndProcedureCategory->getId()]))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
   