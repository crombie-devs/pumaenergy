<?php

namespace Aper\PoliticsAndProceduresBundle\Controller\Backend;

use Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedure;
use Aper\PoliticsAndProceduresBundle\Form\Backend\PoliticAndProcedureType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PoliticAndProcedureController
 */
class PoliticAndProcedureController extends Controller
{
    /**
     * Lists all Course entities.
     *
     * @Route("/", name="admin_politic_and_procedure_index")
     * @Method("GET|POST")
     */
    public function indexAction(Request $request)
    {
        $politicAndProcedure = $this->getDoctrine()->getRepository(PoliticAndProcedure::class)->getQueryBuilder();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $politicAndProcedure,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@AperPoliticsAndProcedures/Backend/PoliticAndProcedure/index.html.twig', [
            'pagination' => $pagination,
            'politicAndProcedure' => $politicAndProcedure,
        ]);
    }

    /**
     * Creates a new politicAndProcedure entity.
     *
     * @Route("/new", name="admin_politic_and_procedure_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $politicAndProcedure = new PoliticAndProcedure();
        $form = $this->createForm(new PoliticAndProcedureType(), $politicAndProcedure);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($politicAndProcedure);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('politic_and_procedure.actions.creation.successful'));

            return $this->redirectToRoute('admin_politic_and_procedure_index');
        }

        return $this->render('@AperPoliticsAndProcedures/Backend/PoliticAndProcedure/new.html.twig', [
            'politicAndProcedure' => $politicAndProcedure,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a politicAndProcedure entity.
     *
     * @Route("/{id}", name="admin_politic_and_procedure_show")
     * @Method("GET")
     */
    public function showAction(PoliticAndProcedure $politicAndProcedure)
    {
        $deleteForm = $this->createDeleteForm($politicAndProcedure);

        return $this->render('@AperPoliticsAndProcedures/Backend/PoliticAndProcedure/show.html.twig', [
            'politicAndProcedure' => $politicAndProcedure,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing politicAndProcedure entity.
     *
     * @Route("/{id}/edit", name="admin_politic_and_procedure_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PoliticAndProcedure $politicAndProcedure)
    {
        $deleteForm = $this->createDeleteForm($politicAndProcedure);
        $editForm = $this->createForm(PoliticAndProcedureType::class, $politicAndProcedure);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', $this->get('translator')->trans('politic_and_procedure.actions.edition.successful'));

            return $this->redirectToRoute('admin_politic_and_procedure_index');
        }

        return $this->render('@AperPoliticsAndProcedures/Backend/PoliticAndProcedure/edit.html.twig', [
            'politicAndProcedure' => $politicAndProcedure,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a politicAndProcedure entity.
     *
     * @Route("/{id}/delete", name="admin_politic_and_procedure_delete")
     */
    public function deleteAction(Request $request, PoliticAndProcedure $politicAndProcedure)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($politicAndProcedure);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('politic_and_procedure.actions.edition.successful'));

        return $this->redirectToRoute('admin_politic_and_procedure_index');
    }

    /**
     * Creates a form to delete a politicAndProcedure entity.
     *
     * @param PoliticAndProcedure $politicAndProcedure The politicAndProcedure entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PoliticAndProcedure $politicAndProcedure)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_politic_and_procedure_delete', ['id' => $politicAndProcedure->getId()]))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
