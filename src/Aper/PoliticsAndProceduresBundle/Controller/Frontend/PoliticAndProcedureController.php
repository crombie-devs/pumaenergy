<?php

namespace Aper\PoliticsAndProceduresBundle\Controller\Frontend;

use Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedure;
use Aper\PoliticsAndProceduresBundle\Form\Backend\PoliticAndProcedureType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PoliticAndProcedureController
 */
class PoliticAndProcedureController extends Controller
{
    /**
     * Class CourseController
     *
     * @Route("/index-resg")
     */
    public function indexResgAction(Request $request)
    {
        $user = $this->getUser();

        $politicsAndProcedures = $this->getDoctrine()->getRepository(PoliticAndProcedure::class)
            ->getQueryBuilder($user->getRoles());



        if (isset($_GET['lucas'])) {
            var_dump($politicsAndProcedures);exit;
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $politicsAndProcedures,
            $request->query->getInt('page', 1),
            100
        );

        if ($request->isXmlHttpRequest()) {
            $results = [];
            /** @var PoliticAndProcedure $new */
            foreach ($pagination as $new) {
                $image = $new->getImage();
                $imagePath = null;
                if ($image && $image->getWebPath()) {
                    //                    $imagePath = '//' . $request->getHost() . $request->getBaseUrl() . '/' . $image->getWebPath();
                    $imagePath = $request->getUriForPath(sprintf('/%s', $image->getWebPath()));
                }

                $pdf = $new->getPdf();
                $pdfPath = null;
                if ($pdf && $pdf->getWebPath()) {
//                    $pdfPath = '//' . $request->getHost() . $request->getBaseUrl() . '/' . $pdf->getWebPath();
                    $pdfPath = $request->getUriForPath(sprintf('/%s', $pdf->getWebPath()));
                }

                $results[] = array(
                    'title' => $new->getTitle(),
                    'content' => substr($new->getContent(), 0, 80),
                    'img' => $imagePath,
                    'pdf' => $pdfPath,
                );
            }

            $results = array_reverse($results);

            return new JsonResponse($results);
        }

        return $this->render('AperPoliticsAndProceduresBundle:Frontend/PoliticAndProcedure:index_resg.html.twig', ['pagination' => $pagination]);
    }

    /**
     * Class CourseController
     *
     * @Route("/index")
     */
    public function indexAction(Request $request)
    {
        $politicsAndProceduresByCategory = $this->getDoctrine()->getRepository(PoliticAndProcedure::class)->getPoliticAndProcedureByCategory();

        $results = [];
        
        foreach ($politicsAndProceduresByCategory as $politic) {
            $image = $politic->getImage();
            $imagePath = null;
            if ($image && $image->getWebPath()) {
                $imagePath = $request->getUriForPath(sprintf('/%s', $image->getWebPath()));
            }

            $pdf = $politic->getPdf();
            $pdfPath = null;
            if ($pdf && $pdf->getWebPath()) {
                $pdfPath = $request->getUriForPath(sprintf('/%s', $pdf->getWebPath()));
            }

            if ($politic->getCategory()) {
                $results[] = array(
                    'title' => $politic->getTitle(),
                    'content' => substr($politic->getContent(), 0, 80),
                    'img' => $imagePath,
                    'pdf' => $pdfPath,
                    'category' => $politic->getCategory()->getName(),
                );
            }

        }

        return $this->render('AperPoliticsAndProceduresBundle:Frontend/PoliticAndProcedure:index.html.twig', [
            'politicsAndProceduresByCategory' => $results
        ]);
    }
}
