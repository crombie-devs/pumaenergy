<?php

namespace Aper\PoliticsAndProceduresBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\FileBundle\Model\File;

/**
 * Class PoliticAndProcedureAttachment
 *
 * @ORM\Entity()
 * @ORM\Table(name="politcs_and_procedures_pdfs")
 */
class PoliticAndProcedurePDF extends File
{
    const DIRECTORY = 'uploads/politic-and-procedure/attachments';

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var PoliticAndProcedure
     * @ORM\OneToOne(targetEntity="Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedure", inversedBy="pdf")
     * @Assert\NotBlank()
     */
    private $politicAndProcedure;

    /**
     * {@inheritDoc}
     * @Assert\File()
     */
    protected $file;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PoliticAndProcedure
     */
    public function getPoliticAndProcedure()
    {
        return $this->politicAndProcedure;
    }

    /**
     * @param PoliticAndProcedure $politicAndProcedure
     */
    public function setPoliticAndProcedure($politicAndProcedure)
    {
        $this->politicAndProcedure = $politicAndProcedure;
    }
}
