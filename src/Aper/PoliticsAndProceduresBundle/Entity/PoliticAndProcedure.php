<?php

namespace Aper\PoliticsAndProceduresBundle\Entity;

use Aper\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PoliticAndProcedure
 *
 * @ORM\Entity(repositoryClass="Aper\PoliticsAndProceduresBundle\Repository\PoliticAndProcedureRepository")
 * @ORM\Table(name="politcs_and_procedures")
 */
class PoliticAndProcedure
{
    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\User", cascade={"persist"})
     * @Gedmo\Blameable(on="create")
     * @Gedmo\Blameable(on="update")
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @var PoliticAndProcedureImage
     *
     * @ORM\OneToOne(
     *     targetEntity="Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedureImage",
     *     mappedBy="politicAndProcedure",
     *     cascade={"all"}
     * )
     * @Assert\Valid()
     */
    private $image;

    /**
     * @var PoliticAndProcedurePDF
     *
     * @ORM\OneToOne(
     *     targetEntity="Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedurePDF",
     *     mappedBy="politicAndProcedure",
     *     cascade={"all"}
     * )
     * @Assert\Valid()
     */
    private $pdf;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     *
     */
    private $enabled;

    /**
     * @var string[]
     *
     * @ORM\Column(name="roles_allowed", type="simple_array")
     * @Assert\Count(min="1", minMessage="Debe elegir un rol como mínimo")
     */
    private $rolesAllowed;

    /**
     * @ORM\ManyToOne(targetEntity="Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedureCategory", inversedBy="politicAndProcedure")
     * @ORM\JoinColumn(name="category", referencedColumnName="id")
     */
    private $category;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return PoliticAndProcedureImage
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param PoliticAndProcedureImage $image
     */
    public function setImage($image)
    {
        $image->setPoliticAndProcedure($this);
        $this->image = $image;
    }

    /**
     * @return PoliticAndProcedurePDF
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @param PoliticAndProcedurePDF $pdf
     */
    public function setPdf($pdf)
    {
        $pdf->setPoliticAndProcedure($this);
        $this->pdf = $pdf;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * Set category
     *
     * @param \Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedureCategory $category
     *
     * @return PoliticAndProcedure
     */
    public function setCategory(\Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedureCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedureCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get rolesAllowed.
     *
     * @return string
     */
    public function getRolesAllowed()
    {
        return $this->rolesAllowed;
    }

    /**
     * @param array $rolesAllowed
     */
    public function setRolesAllowed(array $rolesAllowed = [])
    {
        $this->rolesAllowed = [];

        foreach ($rolesAllowed as $role) {
            $this->addRoleAllowed($role);
        }
    }

    /**
     * @param $roleAllowed
     */
    public function addRoleAllowed($roleAllowed)
    {
        $roleAllowed = strtoupper($roleAllowed);

        if (!in_array($roleAllowed, $this->rolesAllowed)) {
            $this->rolesAllowed[] = $roleAllowed;
        }
    }

    /**
     * @param $roleAllowed
     *
     * @return $this
     */
    public function removeRoleAllowed($roleAllowed)
    {
        if (false !== $key = array_search(strtoupper($roleAllowed), $this->rolesAllowed, true)) {
            unset($this->rolesAllowed[$key]);
            $this->rolesAllowed = array_values($this->rolesAllowed);
        }

        return $this;
    }

    /**
     * @param $roleAllowed
     * @return bool
     */
    public function hasRoleAllowed($roleAllowed)
    {
        return false !== array_search(strtoupper($roleAllowed), $this->rolesAllowed, true);
    }
}
