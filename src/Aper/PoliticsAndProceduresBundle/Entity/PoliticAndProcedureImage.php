<?php

namespace Aper\PoliticsAndProceduresBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\FileBundle\Model\Image;

/**
 * Class PoliticAndProcedureImage
 *
 * @ORM\Entity()
 * @ORM\Table(name="politcs_and_procedures_images")
 */
class PoliticAndProcedureImage extends Image
{
    const DIRECTORY = 'uploads/politic-and-procedure/images';

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var PoliticAndProcedure
     * @ORM\OneToOne(targetEntity="Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedure", inversedBy="image")
     * @Assert\NotBlank()
     */
    private $politicAndProcedure;

    /**
     * {@inheritDoc}
     * @Assert\Image()
     */
    protected $file;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PoliticAndProcedure
     */
    public function getPoliticAndProcedure()
    {
        return $this->politicAndProcedure;
    }

    /**
     * @param PoliticAndProcedure $politicAndProcedure
     */
    public function setPoliticAndProcedure($politicAndProcedure)
    {
        $this->politicAndProcedure = $politicAndProcedure;
    }
}
