<?php

namespace Aper\PoliticsAndProceduresBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PoliticAndProcedureCategoryRepository extends EntityRepository
{
    public function getQueryBuilder()
    {
        $qb = $this->createQueryBuilder('PoliticAndProcedureCategory');

        return $qb;
    }
}