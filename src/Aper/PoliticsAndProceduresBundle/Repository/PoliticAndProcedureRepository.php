<?php

namespace Aper\PoliticsAndProceduresBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PoliticAndProcedureRepository extends EntityRepository
{
    public function getQueryBuilder($roles = [])
    {
        $qb = $this->createQueryBuilder('PoliticAndProcedure')->orderBy('PoliticAndProcedure.id','DESC');;

        if (!empty($roles)) {
            foreach ($roles as $index => $role) {
                $qb->orWhere('PoliticAndProcedure.rolesAllowed LIKE :role_' . $index)
                    ->setParameter('role_' . $index, '%' . $role . '%');
            }
        }

        return $qb;
    }

    public function getPoliticAndProcedureByCategory()
    {
        $qb = $this->createQueryBuilder('p')->orderBy('p.category', 'DESC');

        return $qb->getQuery()->getResult();
    }
}