<?php

namespace Aper\PoliticsAndProceduresBundle\Form\Backend;

use Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedureCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PoliticAndProcedureCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('save', SubmitType::class)
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PoliticAndProcedureCategory::class,
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

 
}
 