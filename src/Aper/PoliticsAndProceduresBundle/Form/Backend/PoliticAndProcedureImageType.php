<?php

namespace Aper\PoliticsAndProceduresBundle\Form\Backend;

use Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedureImage;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\FileBundle\Form\ImageType;

class PoliticAndProcedureImageType extends ImageType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PoliticAndProcedureImage::class,
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    public function getName()
    {
        return 'politic_and_procedure_image';
    }
}
