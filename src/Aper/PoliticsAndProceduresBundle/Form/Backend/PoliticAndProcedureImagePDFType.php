<?php

namespace Aper\PoliticsAndProceduresBundle\Form\Backend;

use Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedurePDF;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\FileBundle\Form\FileType;

class PoliticAndProcedureImagePDFType extends FileType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PoliticAndProcedurePDF::class,
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    public function getName()
    {
        return 'politic_and_procedure_attachment';
    }
}
