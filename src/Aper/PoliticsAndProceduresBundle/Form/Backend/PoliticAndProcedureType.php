<?php

namespace Aper\PoliticsAndProceduresBundle\Form\Backend;

use Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedure;
use Aper\PoliticsAndProceduresBundle\Entity\PoliticAndProcedureCategory;
use Aper\UserBundle\Model\Role;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PoliticAndProcedureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles = Role::getChoices();
        // unset($roles['ROLE_ADMIN']);
        // unset($roles['ROLE_STORE_MANAGER']);
        $builder
            ->add('category', EntityType::class, [
                'class' => PoliticAndProcedureCategory::class,
                'choice_label' => function ($category) {
                    return $category->getName();
                }
            ])
            ->add('title')
            ->add('content', CKEditorType::class, array(
                'config' => [
                    'config_name' => 'my_config',
                    'height' => '450px',
                ],
            ))
            ->add('image', PoliticAndProcedureImageType::class)
            ->add('pdf', PoliticAndProcedureImagePDFType::class, [
                'label' => 'Adjunto'
            ])
            ->add('enabled', null, [
                'label' => 'politic_and_procedure.enabled'
            ])
            ->add('rolesAllowed', ChoiceType::class, [
                'choices' => $roles,
                'expanded' => true,
                'multiple' => true
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PoliticAndProcedure::class,
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    public function getName()
    {
        return 'politic_and_procedure';
    }
}
