<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\SubscribeExternalTrainingCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\ExamPermission;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\ExternalTrainingSubscribed;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class SubscribeExternalTraining
 */
class SubscribeExternalTraining
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * SubscribeExternalTraining constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param SubscribeExternalTrainingCommand $command
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(SubscribeExternalTrainingCommand $command)
    {
        $employee = $command->getEmployee();
        $training = $command->getExternalTraining();

        $module = $training->getModule();
        if($module->isPresencialExam()){
            if(!$module->getExam()){
                $fakeExam = new ModuleExam();
                $fakeExam->setName('Examen Presencial :: ' . $module->getPlan()->getTitle() . ' - ' . $module->getTitle());
                $fakeExam->setModule($module);
                $fakeExam->setEnabled(true);
//                $fakeExam->setMaxAttempts(1);
                $fakeExam->setMaxQuestionCount(0);
                $fakeExam->setPresencial($module->isPresencialExam());
                $this->em->persist($fakeExam);
                $this->em->flush();
                $module->setExam($fakeExam);
            } else {
                $fakeExam = $module->getExam();
                $employeeCareerModuleRepository = $this->em->getRepository(EmployeeCareerModule::class);
                $moduleEmployee = $employeeCareerModuleRepository->findByEmployeeAndModule($employee, $module);
                $examPermission = $this->em->getRepository(ExamPermission::class)->findByEmployeeModuleAndExam($moduleEmployee, $fakeExam);

                if($examPermission)
                {
                    if($fakeExam->getMaxAttempts() <= $examPermission->getEmployeeExams()->count()){
                        throw new \DomainException("El empleado ya agotó sus intentos de exámenes, no puede inscribirse a uno nuevo");
                    }
                }

//                $fakeExam->setMaxAttempts(count($employeeModuleTests) + 1);
//                $this->em->persist($fakeExam);
//                $this->em->flush();
            }
        }

        $training->subscribe($employee);
        $this->em->persist($training);
        $this->em->flush();

        $event = new ExternalTrainingSubscribed($employee, $training);
        $this->dispatcher->dispatch(CareerPlanEvents::EXTERNAL_TRAINING_SUBSCRIBED, $event);

    }


}