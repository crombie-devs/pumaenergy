<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\DisapprovePlanRequirementCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\PlanRequirement;
use Aper\PlanCarreraBundle\Entity\PlanRequirementsEmployee;
use Aper\PlanCarreraBundle\Repository\EmployeeCareerPlanRepository;
use Aper\PlanCarreraBundle\Repository\PlanRequirementRepository;
use Aper\PlanCarreraBundle\Repository\PlanRequirementsEmployeeRepository;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class DisapproveModule
 */
class DisapprovePlanRequirement
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;
    /**
     * @var EmployeeCareerPlanRepository
     */
    private $employeeCareerPlanRepository;
    /**
     * @var PlanRequirementRepository
     */
    private $planRequirementRepository;
    /**
     * @var PlanRequirementsEmployeeRepository
     */
    private $planRequirementEmployeeRepository;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * DisapprovePlanRequirement constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->employeeRepository = $em->getRepository(Employee::class);
        $this->employeeCareerPlanRepository = $em->getRepository(EmployeeCareerPlan::class);
        $this->planRequirementRepository = $em->getRepository(PlanRequirement::class);
        $this->planRequirementEmployeeRepository = $em->getRepository(PlanRequirementsEmployee::class);
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param DisapprovePlanRequirementCommand $command
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(DisapprovePlanRequirementCommand $command)
    {
        $employee    = $this->employeeRepository->find($command->getEmployeeId());
        $requirement = $this->planRequirementRepository->find($command->getPlanRequirementId());

        $employeeCareerPlan = $this->employeeCareerPlanRepository->findByUserAndPlan($employee->getUser(),$requirement->getPlan());

        if(!$employeeCareerPlan){
            throw new \DomainException("El empleado no tiene el Plan Activado.");
        }

        $requirementEmployee = $this->planRequirementEmployeeRepository->findByCareerPlanAndRequirement($employeeCareerPlan, $requirement);

        if(!$requirementEmployee){
            throw new \DomainException("El empleado no tiene el Requerimiento Activado.");
        }

        if(!$requirementEmployee->isEnabled()){
            throw new \DomainException("El empleado no tiene el Requerimiento Activado.");
        }

        $requirementEmployee->setApproved(false);
        $this->em->flush();
    }


}