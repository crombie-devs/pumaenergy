<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\CommentReceivedCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\CommentWasReceived;
use Aper\PlanCarreraBundle\Repository\ModuleRepository;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class CommentReceived
 */
class CommentReceived
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;

    /**
     * @var ModuleRepository
     */
    private $moduleRepository;


    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * ActivateModule constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->employeeRepository = $em->getRepository(Employee::class);
        $this->moduleRepository = $em->getRepository(Module::class);
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param CommentReceivedCommand $command
     */
    public function __invoke(CommentReceivedCommand $command)
    {
        $employee             = $this->employeeRepository->find($command->getEmployeeId());
        /** @var Module $module */
        $module               = $this->moduleRepository->find($command->getModuleId());
        $plan                 = $module->getPlan();
        $comment              = $command->getComment();
        $commentAuthor        = $command->getCommentAuthor();

        if (!$employee->isCareerPlanEnabled($plan)) {
            throw new \DomainException("El plan no esta habilitado para este empleado.");
        }

        /** @var EmployeeCareerPlan $employeeCareerPlan */
        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        /** @var EmployeeCareerModule $moduleEmployee */
        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if(!$moduleEmployee){
            throw new \DomainException("El empleado no tiene el Módulo del Plan.");
        }

        try {
            $moduleEmployee->setComment(substr(trim($comment), 0, 255));
            $moduleEmployee->setCommentAuthor($commentAuthor);

            $this->em->persist($moduleEmployee);
            $this->em->flush();

        } catch (\Exception $e){

        }

        $event = new CommentWasReceived($employee, $moduleEmployee);
        $this->dispatcher->dispatch(CareerPlanEvents::COMMENT_RECEIVED, $event);
    }


}