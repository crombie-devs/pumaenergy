<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\UnsubscribeExternalTrainingCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\ExternalTrainingUnsubscribed;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class UnsubscribeExternalTraining
 */
class UnsubscribeExternalTraining
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * SubscribeExternalTraining constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param UnsubscribeExternalTrainingCommand $command
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(UnsubscribeExternalTrainingCommand $command)
    {
        $employee = $command->getEmployee();
        $training = $command->getExternalTraining();

//        $module = $training->getModule();
//        if($module->isPresencialExam()){
//            if($module->getExam()){
//                $fakeExam = $module->getExam();
//                $employeeModuleExamRepository = $this->em->getRepository(EmployeeModuleExam::class);
//                $employeeModuleTests          = $employeeModuleExamRepository->findByEmployeeAndExam($fakeExam, $employee);
//
//                if(count($employeeModuleTests) == $fakeExam->getMaxAttempts()){
//                    $fakeExam->setMaxAttempts(count($employeeModuleTests) - 1);
//                    $this->em->persist($fakeExam);
//                    $this->em->flush();
//                }
//            }
//        }

        $training->unsubscribe($employee);
        $this->em->persist($training);
        $this->em->flush();

        $event = new ExternalTrainingUnsubscribed($employee, $training);
        $this->dispatcher->dispatch(CareerPlanEvents::EXTERNAL_TRAINING_UNSUBSCRIBED, $event);

    }


}