<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\ActivatePlanRequirementCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\PlanRequirement;
use Aper\PlanCarreraBundle\Entity\PlanRequirementsEmployee;
use Aper\PlanCarreraBundle\Repository\EmployeeCareerPlanRepository;
use Aper\PlanCarreraBundle\Repository\PlanRequirementRepository;
use Aper\PlanCarreraBundle\Repository\PlanRequirementsEmployeeRepository;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ActivateModule
 */
class ActivatePlanRequirement
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;
    /**
     * @var EmployeeCareerPlanRepository
     */
    private $employeeCareerPlanRepository;

    /**
     * @var PlanRequirementRepository
     */
    private $planRequirementRepository;
    /**
     * @var PlanRequirementsEmployeeRepository
     */
    private $planRequirementEmployeeRepository;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * ActivateModule constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->employeeRepository = $em->getRepository(Employee::class);
        $this->employeeCareerPlanRepository = $em->getRepository(EmployeeCareerPlan::class);
        $this->planRequirementRepository = $em->getRepository(PlanRequirement::class);
        $this->planRequirementEmployeeRepository = $em->getRepository(PlanRequirementsEmployee::class);
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param ActivatePlanRequirementCommand $command
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(ActivatePlanRequirementCommand $command)
    {
        $employee    = $this->employeeRepository->find($command->getEmployeeId());
        $requirement = $this->planRequirementRepository->find($command->getPlanRequirementId());

        $employeeCareerPlan = $this->employeeCareerPlanRepository->findByUserAndPlan($employee->getUser(),$requirement->getPlan());

        if(!$employeeCareerPlan){
            throw new \DomainException("El empleado no tiene el Plan Activado.");
        }

        $requirementEmployee = $this->planRequirementEmployeeRepository->findByCareerPlanAndRequirement($employeeCareerPlan, $requirement);

        if(!$requirementEmployee){
            $requirementEmployee = new PlanRequirementsEmployee();
            $requirementEmployee->setEmployeeCareerPlan($employeeCareerPlan);
            $requirementEmployee->setRequirement($requirement);
            $this->em->persist($requirementEmployee);
        }

        $requirementEmployee->setEnabled(true);
        $this->em->flush();
    }


}