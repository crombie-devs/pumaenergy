<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\DisapproveExternalTrainingExamCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Entity\ExamPermission;
use Aper\PlanCarreraBundle\Entity\ExternalTraining;
use Aper\PlanCarreraBundle\Entity\ExternalTrainingInscription;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\ModuleExamWasDisapproved;
use Aper\PlanCarreraBundle\Repository\EmployeeModuleExamRepository;
use Aper\PlanCarreraBundle\Repository\ExternalTrainingInscriptionRepository;
use Aper\PlanCarreraBundle\Repository\ExternalTrainingRepository;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class DisapproveExternalTrainingExam
 */
class DisapproveExternalTrainingExam
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;

    /**
     * @var ExternalTrainingRepository
     */
    private $externalTrainingRepository;
    /**
     * @var ExternalTrainingInscriptionRepository
     */
    private $externalTrainingInscriptionRepository;
    /**
     * @var EmployeeModuleExamRepository
     */
    private $employeeModuleExamRepository;

    /**
     * @var ModuleExam
     */
    private $moduleExamRepository;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * ActivateModule constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->employeeRepository                    = $em->getRepository(Employee::class);
        $this->employeeModuleExamRepository          = $em->getRepository(EmployeeModuleExam::class);
        $this->moduleExamRepository                  = $em->getRepository(ModuleExam::class);
        $this->externalTrainingRepository            = $em->getRepository(ExternalTraining::class);
        $this->externalTrainingInscriptionRepository = $em->getRepository(ExternalTrainingInscription::class);
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param DisapproveExternalTrainingExamCommand $command
     * @throws OptimisticLockException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(DisapproveExternalTrainingExamCommand $command)
    {
        $employee           = $this->employeeRepository->find($command->getEmployeeId());
        $externalTraining   = $this->externalTrainingRepository->find($command->getExternalTrainingId());
        $inscription        = $this->externalTrainingInscriptionRepository->findByUserAndTraining($employee,$externalTraining);

        /** @var Module $module */
        $module     = $externalTraining->getModule();
        $moduleExam = $module->getExam();
        $plan       = $module->getPlan();

        /** @var EmployeeCareerPlan $employeeCareerPlan */
        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            throw new \DomainException("El empleado no tiene el Plan Activado.");
        }

        if (!$employee->isCareerPlanEnabled($plan)) {
            throw new \DomainException("El empleado no tiene el Plan Activado.");
        }

        /** @var EmployeeCareerModule $moduleEmployee */
        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        /** @var ExamPermission $examPermission */
        $examPermission = $moduleEmployee->getExamPermissions()->filter(
            function (ExamPermission $employeeExamPermission) use ($moduleExam) {
                return $employeeExamPermission->getModuleExam() === $moduleExam;
            })->first();

        if(!$examPermission){
            throw new \DomainException("El empleado no tiene el Examen Activado.");
        }


        $employeeModuleExams = $this->employeeModuleExamRepository->findByEmployeeAndExam($moduleExam, $employee);

        if($moduleExam->getMaxAttempts()){
            if((count($employeeModuleExams) < $moduleExam->getMaxAttempts())){

                $test = EmployeeModuleExam::create($employee, $moduleExam);
                $testRepository = $this->em->getRepository(EmployeeModuleExam::class);
                $testRepository->save($test);

                $inscription->setEmployeeExam($test);
                $this->em->persist($inscription);
                $this->em->flush();

            } else {
                if($inscription->getEmployeeExam()){
                    $test = $inscription->getEmployeeExam();
                } else {
                    $test = null;
                    foreach ($employeeModuleExams as $employeeModuleExam){
                        $test           = $employeeModuleExam;
                        $testRegistered = $this->externalTrainingInscriptionRepository->findByEmployeeModuleExam($test);
                        if(!$testRegistered){
                            $inscription->setEmployeeExam($test);
                            try {
                                $this->em->persist($inscription);
                                $this->em->flush();
                                break;
                            } catch (\Exception $e){
                            }
                        }
                        $test = null;
                    }
                }
            }

            if(!is_null($test)){

                $test->finish();
                $test->setApproved(false);
                $this->em->persist($test);
                $this->em->flush();

                $event = new ModuleExamWasDisapproved($employee, $moduleExam);
                $this->dispatcher->dispatch(CareerPlanEvents::EXAM_HAS_BEEN_FAILED_AGAIN, $event);
            }
        }
    }


}