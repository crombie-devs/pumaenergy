<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\DeactivateModulePollCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\PollPermission;
use Aper\PlanCarreraBundle\Entity\ModulePoll;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\ModulePollWasDeactivated;
use Aper\PlanCarreraBundle\Repository\EmployeeCareerModuleRepository;
use Aper\PlanCarreraBundle\Repository\ModulePollRepository;
use Aper\PlanCarreraBundle\Repository\PollPermissionRepository;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class DeactivateModulePoll
 */
class DeactivateModulePoll
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;
    /**
     * @var ModulePollRepository
     */
    private $modulePollRepository;

    /**
     * @var EmployeeCareerModuleRepository
     */
    private $employeeCareerModuleRepository;

    /**
     * @var PollPermissionRepository
     */
    private $pollPermissionRepository;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * ActivateModule constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->pollPermissionRepository = $em->getRepository(PollPermission::class);
        $this->employeeRepository = $em->getRepository(Employee::class);
        $this->modulePollRepository = $em->getRepository(ModulePoll::class);
        $this->employeeCareerModuleRepository = $em->getRepository(EmployeeCareerModule::class);
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param DeactivateModulePollCommand $command
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(DeactivateModulePollCommand $command)
    {
        $employee             = $this->employeeRepository->find($command->getEmployeeId());
        $modulePoll           = $this->modulePollRepository->find($command->getModulePollId());
        $employeeCareerModule = $this->employeeCareerModuleRepository->findByEmployeeAndModule($employee, $modulePoll->getModule());
        if(!$employeeCareerModule){
            throw new \DomainException("El Empleado no tiene asignado el Módulo");
        }

        $pollPermission = $this->pollPermissionRepository->findByEmployeeModuleAndPoll($employeeCareerModule,$modulePoll);

        if(!$pollPermission){
            throw new \DomainException("El Empleado no tiene activado la Encuesta");
        }
        $pollPermission->setEnabled(false);
        $this->em->persist($pollPermission);
        $this->em->flush();

        $event = new ModulePollWasDeactivated($employee, $modulePoll);
        $this->dispatcher->dispatch(CareerPlanEvents::POLL_WAS_DEACTIVATED, $event);
    }


}