<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\ActivateModuleExamCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\ExamPermission;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\ModuleExamWasActivated;
use Aper\PlanCarreraBundle\Repository\EmployeeCareerModuleRepository;
use Aper\PlanCarreraBundle\Repository\ModuleExamRepository;
use Aper\PlanCarreraBundle\Repository\ExamPermissionRepository;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ActivateModule
 */
class ActivateModuleExam
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;
    /**
     * @var ModuleExamRepository
     */
    private $moduleExamRepository;

    /**
     * @var EmployeeCareerModuleRepository
     */
    private $employeeCareerModuleRepository;

    /**
     * @var ExamPermissionRepository
     */
    private $examPermissionRepository;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * ActivateModule constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->examPermissionRepository = $em->getRepository(ExamPermission::class);
        $this->employeeRepository = $em->getRepository(Employee::class);
        $this->moduleExamRepository = $em->getRepository(ModuleExam::class);
        $this->employeeCareerModuleRepository = $em->getRepository(EmployeeCareerModule::class);
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param ActivateModuleExamCommand $command
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(ActivateModuleExamCommand $command)
    {
        $employee             = $this->employeeRepository->find($command->getEmployeeId());
        $moduleExam           = $this->moduleExamRepository->find($command->getModuleExamId());
        $employeeCareerModule = $this->employeeCareerModuleRepository->findByEmployeeAndModule($employee, $moduleExam->getModule());

        if(!$employeeCareerModule){
            throw new \DomainException("El Empleado no tiene asignado el Módulo");
        }

        $examPermission = $this->examPermissionRepository->findByEmployeeModuleAndExam($employeeCareerModule,$moduleExam, $command->getPrepost());

        if(!$examPermission){
            $examPermission = new ExamPermission($employeeCareerModule, $moduleExam, $command->getPrepost());
            $this->em->persist($examPermission);
            $this->em->flush();
            $employeeCareerModule->addExamPermission($examPermission);
            $this->em->persist($employeeCareerModule);
            $this->em->flush();
        }

        if(!$examPermission->isEnabled()){
            $examPermission->setEnabled(true);
            $this->em->persist($examPermission);
//            $employeeCareerModule->addExamPermission($examPermission);
//            $this->em->persist($employeeCareerModule);
            $this->em->flush();

            $event = new ModuleExamWasActivated($employee, $moduleExam);
            $this->dispatcher->dispatch(CareerPlanEvents::EXAM_WAS_ACTIVATED, $event);
        }

    }


}