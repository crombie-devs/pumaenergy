<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\DisapproveModuleExamCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\ModuleExamWasDisapproved;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class DisapproveModuleExam
 */
class DisapproveModuleExam
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;
    /**
     * @var EmployeeModuleExam
     */
    private $employeeModuleExamRepository;

    /**
     * @var ModuleExam
     */
    private $moduleExamRepository;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * DisapproveModule constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->employeeRepository = $em->getRepository(Employee::class);
        $this->employeeModuleExamRepository = $em->getRepository(EmployeeModuleExam::class);
        $this->moduleExamRepository = $em->getRepository(ModuleExam::class);
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param DisapproveModuleExamCommand $command
     * @throws OptimisticLockException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(DisapproveModuleExamCommand $command)
    {
        $employee             = $this->employeeRepository->find($command->getEmployeeId());
        $moduleExam           = $this->moduleExamRepository->find($command->getModuleExamId());
        $employeeModuleExam   = $this->employeeModuleExamRepository->find($command->getEmployeeModuleExamId());

        $attemptsEmployeeExam = $this->employeeModuleExamRepository->countAttempts($moduleExam,$employee);

        if(!$employeeModuleExam){
            throw new \DomainException("El empleado no tiene el Examen Iniciado.");
        } else {
            $employeeModuleExam->setApproved(false);
            $this->em->persist($employeeModuleExam);
        }
        $this->em->flush();

        $event = new ModuleExamWasDisapproved($employee, $moduleExam);
        if($attemptsEmployeeExam > 1){
            $this->dispatcher->dispatch(CareerPlanEvents::EXAM_HAS_BEEN_FAILED_AGAIN, $event);
        } else {
            $this->dispatcher->dispatch(CareerPlanEvents::EXAM_HAS_BEEN_FAILED, $event);
        }
    }


}