<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\ApprovePlanCommand;
use Aper\PlanCarreraBundle\Entity\CareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\PlanWasApproved;
use Aper\PlanCarreraBundle\Repository\EmployeeCareerPlanRepository;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ApprovePlan
 */
class ApprovePlan
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;
    /**
     * @var EmployeeCareerPlanRepository
     */
    private $employeeCareerPlanRepository;
    /**
     * @var CareerPlan
     */
    private $careerPlanRepository;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * ActivateModule constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->employeeRepository = $em->getRepository(Employee::class);
        $this->employeeCareerPlanRepository = $em->getRepository(EmployeeCareerPlan::class);
        $this->careerPlanRepository = $em->getRepository(CareerPlan::class);
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param ApprovePlanCommand $command
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(ApprovePlanCommand $command)
    {
        $employee   = $this->employeeRepository->find($command->getEmployeeId());
        $careerPlan = $this->careerPlanRepository->find($command->getPlanId());

        $employeeCareerPlan = $this->employeeCareerPlanRepository->findByUserAndPlan($employee->getUser(),$careerPlan);

        if(!$employeeCareerPlan or !$employeeCareerPlan->isEnabled()){
            throw new \DomainException("El empleado no tiene el Plan Activado.");
        }
        $employeeCareerPlan->setApproved(true);
        $this->em->persist($employeeCareerPlan);
        $this->em->flush();

        $event = new PlanWasApproved($employee, $careerPlan);
        $this->dispatcher->dispatch(CareerPlanEvents::CAREER_PLAN_WAS_APPROVED, $event);
    }


}