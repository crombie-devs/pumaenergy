<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\ActivateModulePollCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\PollPermission;
use Aper\PlanCarreraBundle\Entity\ModulePoll;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\ModulePollWasActivated;
use Aper\PlanCarreraBundle\Repository\EmployeeCareerModuleRepository;
use Aper\PlanCarreraBundle\Repository\ModulePollRepository;
use Aper\PlanCarreraBundle\Repository\PollPermissionRepository;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ActivateModulePoll
 */
class ActivateModulePoll
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;
    /**
     * @var ModulePollRepository
     */
    private $modulePollRepository;

    /**
     * @var EmployeeCareerModuleRepository
     */
    private $employeeCareerModuleRepository;

    /**
     * @var PollPermissionRepository
     */
    private $pollPermissionRepository;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * ActivateModulePoll constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->pollPermissionRepository = $em->getRepository(PollPermission::class);
        $this->employeeRepository = $em->getRepository(Employee::class);
        $this->modulePollRepository = $em->getRepository(ModulePoll::class);
        $this->employeeCareerModuleRepository = $em->getRepository(EmployeeCareerModule::class);
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param ActivateModulePollCommand $command
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(ActivateModulePollCommand $command)
    {
        $employee             = $this->employeeRepository->find($command->getEmployeeId());
        $modulePoll           = $this->modulePollRepository->find($command->getModulePollId());
        $employeeCareerModule = $this->employeeCareerModuleRepository->findByEmployeeAndModule($employee, $modulePoll->getModule());

        if(!$employeeCareerModule){
            throw new \DomainException("El Empleado no tiene asignado el Módulo");
        }

        $pollPermission = $this->pollPermissionRepository->findByEmployeeModuleAndPoll($employeeCareerModule,$modulePoll);

        if(!$pollPermission){
            $pollPermission = new PollPermission($employeeCareerModule, $modulePoll);
            $this->em->persist($pollPermission);
        }
        $pollPermission->setEnabled(true);
        $this->em->persist($pollPermission);
        $employeeCareerModule->addPollPermission($pollPermission);
        $this->em->persist($employeeCareerModule);

        //$this->em->persist($employeeCareerModule);

        $this->em->flush();

        $event = new ModulePollWasActivated($employee, $modulePoll);
        $this->dispatcher->dispatch(CareerPlanEvents::POLL_WAS_ACTIVATED, $event);
    }


}