<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\RegisterToProgramCommand;
use Aper\PlanCarreraBundle\Entity\CareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\PlanWasAssigned;
use Aper\PlanCarreraBundle\Repository\CareerPlanRepository;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class RegisterToProgram
 */
class RegisterToProgram
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;
    /**
     * @var CareerPlanRepository
     */
    private $careerPlanRepository;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * RegisterToProgram constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->employeeRepository = $em->getRepository(Employee::class);
        $this->careerPlanRepository = $em->getRepository(CareerPlan::class);
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param RegisterToProgramCommand $command
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(RegisterToProgramCommand $command)
    {
        /** @var Employee $employee */
        $employee = $this->employeeRepository->find($command->getEmployeeId());
        $currentPlan = $this->careerPlanRepository->find($command->getCurrentPlanId());


        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($currentPlan) {
            return $employeeCareerPlan->getPlan() === $currentPlan;
        })->first();

        if(!$employeeCareerPlan){
            $employeeCareerPlan = EmployeeCareerPlan::createForCurrentPlan($currentPlan, $employee);
            $this->em->persist($employeeCareerPlan);

            // notifico solo si es la primer vez que se le asigna el plan.
            $event = new PlanWasAssigned($employee, $currentPlan);
            $this->dispatcher->dispatch(CareerPlanEvents::CAREER_PLAN_WAS_ASSIGNED, $event);
        }

        $employeeCareerPlan->setEnabled(true);
        $employee->setCurrentPlan($currentPlan);

        $this->em->persist($employee);
        $this->em->flush();
    }


}