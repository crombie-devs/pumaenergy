<?php

namespace Aper\PlanCarreraBundle\CQRS\CommandHandler;

use Aper\PlanCarreraBundle\CQRS\Command\ActivateModuleCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\ModuleWasActivated;
use Aper\PlanCarreraBundle\Repository\ModuleRepository;
use Aper\PlanCarreraBundle\Repository\EmployeeCareerPlanRepository;
use Aper\PlanCarreraBundle\Repository\EmployeeCareerModuleRepository;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ActivateModule
 */
class ActivateModule
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;
    /**
     * @var EmployeeCareerPlanRepository
     */
    private $employeeCareerPlanRepository;

    /**
     * @var EmployeeCareerModuleRepository
     */
    private $employeeCareerModuleRepository;
    /**
     * @var ModuleRepository
     */
    private $moduleRepository;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * ActivateModule constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->employeeRepository = $em->getRepository(Employee::class);
        $this->employeeCareerModuleRepository = $em->getRepository(EmployeeCareerModule::class);
        $this->employeeCareerPlanRepository = $em->getRepository(EmployeeCareerPlan::class);
        $this->moduleRepository = $em->getRepository(Module::class);
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param ActivateModuleCommand $command
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(ActivateModuleCommand $command)
    {
        $employee = $this->employeeRepository->find($command->getEmployeeId());
        $module   = $this->moduleRepository->find($command->getModuleId());

        $employeeCareerPlan = $this->employeeCareerPlanRepository->findByUserAndPlan($employee->getUser(),$module->getPlan());

        if(!$employeeCareerPlan){
            $employeeCareerPlan = new EmployeeCareerPlan($module->getPlan(), $employee);
            $this->em->persist($employeeCareerPlan);
        }

        if(!$employeeCareerPlan->isEnabled()){
            $employeeCareerPlan->setEnabled(true);
            $this->em->persist($employeeCareerPlan);
        }

        $employeeCareerModule = $this->employeeCareerModuleRepository->findByEmployeeAndModule($employee, $module);

        if(!$employeeCareerModule){
            $employeeCareerModule = new EmployeeCareerModule();
            $employeeCareerModule->setModule($module);
            $employeeCareerModule->setEmployeePlan($employeeCareerPlan);
            $employeeCareerModule->setDateFrom($command->getDateFrom());
            $employeeCareerModule->setDateTo($command->getDateTo());

            $this->em->persist($employeeCareerModule);
        }

        $employeeCareerModule->setEnabled(true);
        $this->em->persist($employeeCareerModule);
        $this->em->flush();

        $event = new ModuleWasActivated($employee, $module);
        $this->dispatcher->dispatch(CareerPlanEvents::MODULE_WAS_ACTIVATED, $event);
    }


}