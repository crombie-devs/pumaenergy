<?php

namespace Aper\PlanCarreraBundle\CQRS\Command;

use Aper\PlanCarreraBundle\Entity\CareerPlan;
use Aper\UserBundle\Entity\Employee;

class ApprovePlanCommand
{
    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var CareerPlan
     */
    private $careerPlan;

    /**
     * ActivateModule constructor.
     * @param Employee $employee
     * @param CareerPlan $careerPlan
     */
    public function __construct(Employee $employee, CareerPlan $careerPlan)
    {
        $this->employee   = $employee;
        $this->careerPlan = $careerPlan;
    }

    /**
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employee->getId();
    }

    /**
     * @return int
     */
    public function getPlanId()
    {
        return $this->careerPlan->getId();
    }
}