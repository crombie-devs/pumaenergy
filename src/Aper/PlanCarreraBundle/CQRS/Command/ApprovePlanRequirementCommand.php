<?php

namespace Aper\PlanCarreraBundle\CQRS\Command;

use Aper\PlanCarreraBundle\Entity\PlanRequirement;
use Aper\UserBundle\Entity\Employee;

class ApprovePlanRequirementCommand
{
    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var PlanRequirement
     */
    private $planRequirement;

    /**
     * ActivateModule constructor.
     * @param Employee $employee
     * @param PlanRequirement $planRequirement
     */
    public function __construct(Employee $employee, PlanRequirement $planRequirement)
    {
        $this->employee = $employee;
        $this->planRequirement = $planRequirement;
    }

    /**
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employee->getId();
    }

    /**
     * @return int
     */
    public function getPlanRequirementId()
    {
        return $this->planRequirement->getId();
    }
}