<?php

namespace Aper\PlanCarreraBundle\CQRS\Command;

use Aper\UserBundle\Entity\Employee;

class AssignTrainer
{
    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var Employee
     */
    private $trainer;

    /**
     * AssignTrainer constructor.
     * @param Employee $employee
     * @param Employee $trainer
     */
    public function __construct(Employee $employee, Employee $trainer = null)
    {
        $this->employee = $employee;
        $this->trainer = $trainer;
    }

    /**
     * @param Employee $employee
     * @return AssignTrainer
     */
    public static function fromEmployee(Employee $employee)
    {
        return new AssignTrainer($employee, $employee->getTrainer());
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return Employee
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * @param Employee $trainer
     */
    public function setTrainer($trainer)
    {
        $this->trainer = $trainer;
    }

}