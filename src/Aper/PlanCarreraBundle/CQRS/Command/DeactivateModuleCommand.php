<?php

namespace Aper\PlanCarreraBundle\CQRS\Command;

use Aper\PlanCarreraBundle\Entity\Module;
use Aper\UserBundle\Entity\Employee;

class DeactivateModuleCommand
{
    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var Module
     */
    private $module;

    /**
     * DeactivateModule constructor.
     * @param Employee $employee
     * @param Module $module
     */
    public function __construct(Employee $employee, Module $module)
    {
        $this->employee = $employee;
        $this->module = $module;
    }

    /**
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employee->getId();
    }

    /**
     * @return int
     */
    public function getModuleId()
    {
        return $this->module->getId();
    }
}