<?php

namespace Aper\PlanCarreraBundle\CQRS\Command;

use Aper\PlanCarreraBundle\Entity\ModulePoll;
use Aper\UserBundle\Entity\Employee;

class ActivateModulePollCommand
{
    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var ModulePoll
     */
    private $modulePoll;

    /**
     * ActivateModule constructor.
     * @param Employee $employee
     * @param ModulePoll $modulePoll
     */
    public function __construct(Employee $employee, ModulePoll $modulePoll)
    {
        $this->employee   = $employee;
        $this->modulePoll = $modulePoll;
    }

    /**
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employee->getId();
    }

    /**
     * @return int
     */
    public function getModulePollId()
    {
        return $this->modulePoll->getId();
    }

}