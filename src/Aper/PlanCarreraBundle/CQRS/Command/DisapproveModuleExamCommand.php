<?php

namespace Aper\PlanCarreraBundle\CQRS\Command;

use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\UserBundle\Entity\Employee;

class DisapproveModuleExamCommand
{
    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var ModuleExam
     */
    private $moduleExam;

    /**
     * @var EmployeeModuleExam
     */
    private $employeeModuleExam;

    /**
     * ActivateModule constructor.
     * @param Employee $employee
     * @param ModuleExam $moduleExam
     * @param EmployeeModuleExam $employeeModuleExam
     */
    public function __construct(Employee $employee, ModuleExam $moduleExam, EmployeeModuleExam $employeeModuleExam)
    {
        $this->employee           = $employee;
        $this->moduleExam         = $moduleExam;
        $this->employeeModuleExam = $employeeModuleExam;
    }

    /**
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employee->getId();
    }

    /**
     * @return int
     */
    public function getModuleExamId()
    {
        return $this->moduleExam->getId();
    }

    /**
     * @return int
     */
    public function getEmployeeModuleExamId(){
        return $this->employeeModuleExam->getId();
    }
}