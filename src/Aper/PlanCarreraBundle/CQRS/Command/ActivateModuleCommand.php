<?php

namespace Aper\PlanCarreraBundle\CQRS\Command;

use Aper\PlanCarreraBundle\Entity\Module;
use Aper\UserBundle\Entity\Employee;

class ActivateModuleCommand
{
    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var Module
     */
    private $module;

    /**
     * @var string
     */
    private $dateFrom;

    /**
     * @var string
     */
    private $dateTo;

    /**
     * ActivateModule constructor.
     * @param Employee $employee
     * @param Module $module
     */
    public function __construct(Employee $employee, Module $module, $dateFrom, $dateTo)
    {

        $this->employee = $employee;
        $this->module   = $module;
        $this->dateFrom = $dateFrom;
        $this->dateTo   = $dateTo;
    }

    public function getDateFrom(){
        return \DateTime::createFromFormat('Y-m-d', $this->dateFrom)->setTime(0,0,0);
    }

    public function getDateTo(){
        return \DateTime::createFromFormat('Y-m-d', $this->dateTo)->setTime(0,0,0);
    }

    /**
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employee->getId();
    }

    /**
     * @return int
     */
    public function getModuleId()
    {
        return $this->module->getId();
    }

}