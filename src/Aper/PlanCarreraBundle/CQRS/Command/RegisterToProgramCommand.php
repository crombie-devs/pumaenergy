<?php

namespace Aper\PlanCarreraBundle\CQRS\Command;

class RegisterToProgramCommand
{
    /**
     * @var integer
     */
    private $employeeId;
    /**
     * @var integer
     */
    private $currentPlanId;

    /**
     * RegisterToProgramCommand constructor.
     * @param int $employeeId
     * @param int $currentPlanId
     */
    public function __construct($employeeId, $currentPlanId)
    {
        $this->employeeId = $employeeId;
        $this->currentPlanId = $currentPlanId;
    }

    /**
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * @return int
     */
    public function getCurrentPlanId()
    {
        return $this->currentPlanId;
    }

}