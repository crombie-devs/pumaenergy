<?php

namespace Aper\PlanCarreraBundle\CQRS\Command;

use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\UserBundle\Entity\Employee;

class DeactivateModuleExamCommand
{
    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var ModuleExam
     */
    private $moduleExam;

    /**
     * @var boolean
     */
    private $prepost;

    /**
     * DeactivateModuleExam constructor.
     * @param Employee $employee
     * @param ModuleExam $moduleExam
     */
    public function __construct(Employee $employee, ModuleExam $moduleExam, $prepost)
    {
        $this->employee   = $employee;
        $this->moduleExam = $moduleExam;
        $this->prepost    = $prepost;
    }

    /**
     * @return bool
     */
    public function getPrepost(){
        return $this->prepost;
    }

    /**
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employee->getId();
    }

    /**
     * @return int
     */
    public function getModuleExamId()
    {
        return $this->moduleExam->getId();
    }
}