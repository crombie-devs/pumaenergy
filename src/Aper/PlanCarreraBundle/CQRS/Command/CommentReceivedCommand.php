<?php

namespace Aper\PlanCarreraBundle\CQRS\Command;

use Aper\PlanCarreraBundle\Entity\Module;
use Aper\UserBundle\Entity\Employee;

class CommentReceivedCommand
{
    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var Module
     */
    private $module;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var Employee
     */
    private $commentAuthor;

    /**
     * ActivateModule constructor.
     * @param Employee $employee
     * @param Module $module
     * @param $comment
     * @param Employee $commentAuthor
     */
    public function __construct(Employee $employee, Module $module, $comment, Employee $commentAuthor)
    {
        $this->employee      = $employee;
        $this->module        = $module;
        $this->comment       = $comment;
        $this->commentAuthor = $commentAuthor;
    }

    /**
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employee->getId();
    }

    /**
     * @return int
     */
    public function getModuleId()
    {
        return $this->module->getId();
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return Employee
     */
    public function getCommentAuthor()
    {
        return $this->commentAuthor;
    }

}