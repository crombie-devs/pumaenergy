<?php

namespace Aper\PlanCarreraBundle\CQRS\Command;

use Aper\PlanCarreraBundle\Entity\ExternalTraining;
use Aper\UserBundle\Entity\Employee;

class DisapproveExternalTrainingExamCommand
{
    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var ExternalTraining
     */
    private $externalTraining;

    /**
     * ActivateModule constructor.
     * @param Employee $employee
     * @param ExternalTraining $externalTraining
     */
    public function __construct(Employee $employee, ExternalTraining $externalTraining)
    {
        $this->employee         = $employee;
        $this->externalTraining = $externalTraining;
    }

    /**
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employee->getId();
    }

    /**
     * @return int
     */
    public function getExternalTrainingId()
    {
        return $this->externalTraining->getId();
    }

}