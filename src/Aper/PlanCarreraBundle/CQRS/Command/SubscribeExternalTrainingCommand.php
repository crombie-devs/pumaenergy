<?php

namespace Aper\PlanCarreraBundle\CQRS\Command;

use Aper\PlanCarreraBundle\Entity\ExternalTraining;
use Aper\UserBundle\Entity\Employee;

class SubscribeExternalTrainingCommand
{
    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var ExternalTraining
     */
    private $externalTraining;

    /**
     * ActivateModule constructor.
     * @param Employee $employee
     * @param ExternalTraining $externalTraining
     */
    public function __construct(Employee $employee, ExternalTraining $externalTraining)
    {
        $this->employee         = $employee;
        $this->externalTraining = $externalTraining;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return ExternalTraining
     */
    public function getExternalTraining()
    {
        return $this->externalTraining;
    }

}