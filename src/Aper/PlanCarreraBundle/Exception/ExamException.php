<?php


namespace Aper\PlanCarreraBundle\Exception;


use Throwable;

class ExamException extends \LogicException
{

    protected $type;

    /**
     * ExamException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @param string $type
     */
    public function __construct($type = 'NO_APPROVE', $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType(){
        return $this->type;
    }


}