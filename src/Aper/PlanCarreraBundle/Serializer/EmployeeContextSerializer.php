<?php

namespace Aper\PlanCarreraBundle\Serializer;

use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\ExternalTraining;
use Aper\PlanCarreraBundle\Entity\ModuleExamQuestion;
use Aper\PlanCarreraBundle\Entity\Multimedia;
use Aper\PlanCarreraBundle\Repository\EmployeeModuleExamRepository;

class EmployeeContextSerializer
{
    /**
     * @var EmployeeModuleExamRepository
     */
    private $testRepository;

    /**
     * EmployeeContextSerializer constructor.
     * @param EmployeeModuleExamRepository $testRepository
     */
    public function __construct(EmployeeModuleExamRepository $testRepository)
    {
        $this->testRepository = $testRepository;
    }

    /**
     * @param EmployeeCareerPlan[] $employeeCareerPlans
     * @return array
     */
    public function fromEmployeeCareerPlansCollection($employeeCareerPlans)
    {
        $data = [];
        foreach ($employeeCareerPlans as $employeeCareerPlan) {
            $plan = $employeeCareerPlan->getPlan();

            $requirements = $this->getRequirements($employeeCareerPlan);

            $modules = $this->getModules($employeeCareerPlan);

            $data[] = [
                'id' => $plan->getId(),
                'position' => $plan->getPosition(),
                "title" => $plan->getTitle(),
                "description" => $plan->getDescription(),
                "big_active_image" => $plan->getBigActiveImage() && $plan->getBigActiveImage()->getFilePath() ? $plan->getBigActiveImage()->getWebPath() : '',
                "big_inactive_image" => $plan->getBigInactiveImage() && $plan->getBigInactiveImage()->getFilePath() ? $plan->getBigInactiveImage()->getWebPath() : '',
                "tiny_active_image" => $plan->getTinyActiveImage() && $plan->getTinyActiveImage()->getFilePath() ? $plan->getTinyActiveImage()->getWebPath() : '',
                "tiny_inactive_image" => $plan->getTinyInactiveImage() && $plan->getTinyInactiveImage()->getFilePath() ? $plan->getTinyInactiveImage()->getWebPath() : '',
                "requirements" => $requirements,
                "all_requirements_approved" => $employeeCareerPlan->getEnabledRequirements()->count() === $employeeCareerPlan->getApprovedRequirements()->count(),
                "modules" => $modules,
                'approved' => $employeeCareerPlan->isApproved(),
                'enabled' => $employeeCareerPlan->isEnabled(),
            ];
        }

        $this->sortByPosition($data);

        return $data;
    }

    /**
     * @param EmployeeCareerPlan $employeeCareerPlan
     * @return array
     */
    private function getRequirements(EmployeeCareerPlan $employeeCareerPlan)
    {
        $requirements = [];
        foreach ($employeeCareerPlan->getPlan()->getRequirements() as $requirement) {
            if (!$employeeCareerPlan->isRequirementEnabled($requirement)) {
                continue;
            }
            $requirements[] = [
                "description" => $requirement->getDescription(),
                "approved" => $employeeCareerPlan->isRequirementApproved($requirement),
            ];
        }

        return $requirements;
    }

    /**
     * @param EmployeeCareerPlan $employeeCareerPlan
     * @return array
     */
    private function getModules(EmployeeCareerPlan $employeeCareerPlan)
    {
        $modules = [];
        foreach ($employeeCareerPlan->getPlan()->getModules() as $enabledModule) {
            $mod = $employeeCareerPlan->getModule($enabledModule);
            if (!$mod) {
                continue;
            }

            $module = [
                'id' => $enabledModule->getId(),
                'title' => $enabledModule->getTitle(),
                'description' => $enabledModule->getDescription(),
                'text' => $enabledModule->getText(),
                'embed' => $enabledModule->getYouTubeResource() ? $enabledModule->getYouTubeResource()->getUri() : '',
                "image" => $enabledModule->getImage() && $enabledModule->getImage()->getFilePath() ? $enabledModule->getImage()->getWebPath() : '',
                'presential' => $enabledModule->getPresencial(),
                'nivelation' => $enabledModule->getNivelation(),
                'priority' => $enabledModule->getOrden(),
                'type' => $enabledModule->getType(),
                'enabledForUser' => $employeeCareerPlan->isModuleEnabled($enabledModule),
                'approvedForUser' => $employeeCareerPlan->isModuleApproved($enabledModule),
            ];

            /** @var Multimedia $multimedia */
            foreach ($enabledModule->getMultimedias() as $multimedia) {
                $module['multimedias'][] = [
                    'title' => $multimedia->getTitle(),
                    'description' => $multimedia->getDescription(),
                    'path' => $multimedia->getFilePath() ? $multimedia->getWebPath() : '',
                    'ext' => $multimedia->getFilePath() ? $multimedia->getExtension() : '',
                ];
            }

            $module['trainings'] = [];

            /** @var ExternalTraining $externalTraining */
            $externalTrainings = $enabledModule->getExternalTrainings();


            $iterator = $externalTrainings->getIterator();

            $iterator->uasort(function ($first, $second) {
                return $first->getDate() > $second->getDate() ? 1 : -1;
            });



            foreach ($iterator as $externalTraining) {
//                $externalTraining = $enabledModule->getNextExternalTraining(new \DateTime());
                if ($externalTraining->getDate() < new \DateTime()) {
                    continue;
                }

                $employeeStore = $employeeCareerPlan->getUser()->getStore();
                if ($externalTraining->getStores()->isEmpty() || $externalTraining->getStores()->contains($employeeStore)) {
                    $external = [
                        "id" => $externalTraining->getId(),
                        "place" => $externalTraining->getPlace(),
                        "date" => $externalTraining->getDate()->format(\DATE_ISO8601),
                        "date_date" => $externalTraining->getDate()->format('d/m/Y'),
                        "date_time" => $externalTraining->getDate()->format('H:i'),
                        "duration" => $externalTraining->getDuration(),
                        "quota" => $externalTraining->getQuota(),
                        "subscribers" => $externalTraining->getInscriptions()->count(),
                        "inscribed" => (boolean)$externalTraining->getInscriptionForEmployee($employeeCareerPlan->getUser()),
                    ];
                    $module['trainings'][] = $external;
                }
            }

            $poll = null;
            if ($enabledModule->getPoll()) {
                $poll = [];
                foreach ($enabledModule->getPoll()->getQuestions() as $question) {
                    if ($mod->isAnswered($question)) {
                        continue;
                    }
                    $q = [
                        'id' => $question->getId(),
                        'text' => $question->getText(),
                        'image' => $question->getImage() && $question->getImage()->getFilePath() ? $question->getImage()->getWebPath() : '',
                        'type' => $question->getType(),
                        'answers' => [],
                        //'is_answered' => $mod->isAnswered($question),
                    ];
                    foreach ($question->getAnswers() as $answer) {
                        $a = [
                            'id' => $answer->getId(),
                            'text' => $answer->getText(),
                        ];
                        $q['answers'][] = $a;
                    }

                    $poll[] = $q;
                }
            }

            $module['poll'] = $poll ? $poll : null;

            $exam = null;
            if ($enabledModule->getExam() && $enabledModule->getExam()->isEnabled()) {
                $exam = [
                    'id' => $enabledModule->getExam()->getId(),
                    'question' => [],
                ];
                /** @var ModuleExamQuestion $question */
                foreach ($enabledModule->getExam()->getQuestions() as $question) {
                    if (!$question->isEnabled()) {
                        continue;
                    }
                    $q = [
                        'id' => $question->getId(),
                        'text' => $question->getText(),
                        'image' => $question->getFile() && $question->getFile()->getFilePath() ? $question->getFile()->getWebPath() : '',
                        'answers' => [],
                    ];
                    foreach ($question->getAnswers() as $answer) {
                        if (!$answer->isEnabled()) {
                            continue;
                        }
                        $a = [
                            'id' => $answer->getId(),
                            'text' => $answer->getText(),
                        ];
                        $q['answers'][] = $a;
                    }

                    $exam['question'][] = $q;
                }
            }

            $module['exam'] = $exam;

            $test = null;
            if ($enabledModule->getExam() && $enabledModule->getExam()->isEnabled()) {
                $testEntity = $this->testRepository->findLastByExam($enabledModule->getExam(), $employeeCareerPlan->getUser());
                if ($testEntity) {
                    $test = [];
                    foreach ($testEntity->getAnswers() as $answer) {

                        $a = [
                            'question_id' => $answer->getQuestion()->getId(),
                            'answer_id' => $answer->getAnswer()->getId(),

                        ];

                        $test[] = $a;
                    }
                }

            }

            $module['test'] = $test;

            $modules[] = $module;
        }

        return $modules;
    }

    /**
     * @param array $data
     */
    private function sortByPosition(&$data)
    {
        usort($data, function ($c1, $c2) {
            return $c1['position'] > $c2['position'];
        });
    }
}