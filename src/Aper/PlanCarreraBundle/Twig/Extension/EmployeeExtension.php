<?php

namespace Aper\PlanCarreraBundle\Twig\Extension;

use Aper\PlanCarreraBundle\Entity\CareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Entity\ExamPermission;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Entity\ModulePoll;
use Aper\PlanCarreraBundle\Entity\PlanRequirement;
use Aper\PlanCarreraBundle\Entity\PlanRequirementsEmployee;
use Aper\PlanCarreraBundle\Entity\PollPermission;
use Aper\UserBundle\Entity\Employee;
use Doctrine\Common\Collections\ArrayCollection;

class EmployeeExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'Employee CareerPlan Twig Extension';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('requirementApproved', array($this, 'requirementApproved')),
            new \Twig_SimpleFunction('getDates', array($this, 'getDates')),
            new \Twig_SimpleFunction('haveModuleActive', array($this, 'haveModuleActive')),
            new \Twig_SimpleFunction('haveModuleApprove', array($this, 'haveModuleApprove')),
            new \Twig_SimpleFunction('haveRequirementActive', array($this, 'haveRequirementActive')),
            new \Twig_SimpleFunction('haveModuleExamActive', array($this, 'haveModuleExamActive')),
            new \Twig_SimpleFunction('haveModulePollActive', array($this, 'haveModulePollActive')),
            new \Twig_SimpleFunction('haveModuleExamApproved', array($this, 'moduleExamApproved')),
            new \Twig_SimpleFunction('moduleExamFinished', array($this, 'moduleExamFinished')),
            new \Twig_SimpleFunction('planApproved', array($this, 'planApproved')),
            new \Twig_SimpleFunction('planActivated', array($this, 'planActivated')),
            new \Twig_SimpleFunction('testModuleFinish', array($this, 'testModuleFinish')),
            new \Twig_SimpleFunction('moduleExamStarted', array($this, 'moduleExamStarted')),
            new \Twig_SimpleFunction('testEvaluated', array($this, 'testEvaluated')),
            new \Twig_SimpleFunction('isInstanceOf', array($this, 'isInstanceOf')),
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('positionSort', array($this, 'positionSort')),
        );
    }

    /**
     * @param $multimediaCollection
     * @return ArrayCollection
     */
    public function positionSort(ArrayCollection $multimediaCollection)
    {
        $items = $multimediaCollection->toArray();
        usort($items, function ($item1, $item2)
        {
            if ($item1->getPosition() == $item2->getPosition()) return 0;
            return $item1->getPosition() < $item2->getPosition() ? -1 : 1;
        });

        return $items;
    }

    public function isInstanceOf( $var, $instance)
    {
//        return is_a($var, $instance, true) ? true: false;
        try {
            $reflexionClass = new \ReflectionClass($instance);
        } catch (\ReflectionException $e) {
            return (get_class($instance) == $var);
        }
        return ($reflexionClass->isInstance($var));
    }

    /**
     * @param Employee $employee
     * @param CareerPlan $careerPlan
     * @return bool
     */
    public function planApproved(Employee $employee, CareerPlan $careerPlan)
    {
        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($careerPlan) {
            return $employeeCareerPlan->getPlan() === $careerPlan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        return $employeeCareerPlan->isApproved();
    }

    /**
     * @param Employee $employee
     * @param CareerPlan $careerPlan
     * @return bool
     */
    public function planActivated(Employee $employee, CareerPlan $careerPlan)
    {
        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($careerPlan) {
            return $employeeCareerPlan->getPlan() === $careerPlan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        return $employeeCareerPlan->isEnabled();
    }

    /**
     * @param Employee $employee
     * @param PlanRequirement $requirement
     * @return bool
     */
    public function requirementApproved(Employee $employee, PlanRequirement $requirement)
    {
        $plan = $requirement->getPlan();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        $planRequirementsEmployee = $employeeCareerPlan->getRequirements()->filter(
            function (PlanRequirementsEmployee $planRequirementsEmployee) use ($requirement) {
                return $planRequirementsEmployee->getRequirement() === $requirement;
        })->first();

        if (!$planRequirementsEmployee) {
            return false;
        }

        return $planRequirementsEmployee->isApproved();
    }

    /**
     * @param Employee $employee
     * @param PlanRequirement $planRequirement
     * @return bool
     */
    public function haveRequirementActive(Employee $employee, PlanRequirement $planRequirement){

        $plan = $planRequirement->getPlan();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        $planRequirementsEmployee = $employeeCareerPlan->getRequirements()->filter(
            function (PlanRequirementsEmployee $planRequirementsEmployee) use ($planRequirement) {
                return $planRequirementsEmployee->getRequirement() === $planRequirement;
            })->first();

        if (!$planRequirementsEmployee) {
            return false;
        }

        return $planRequirementsEmployee->isEnabled();
    }

    /**
     * @param Employee $employee
     * @param ModuleExam $moduleExam
     * @return bool
     */
    public function haveModuleExamActive(Employee $employee, ModuleExam $moduleExam, $prepost){

        $module = $moduleExam->getModule();
        $plan   = $module->getPlan();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if (!$moduleEmployee) {
            return false;
        }

        if (!$moduleEmployee->isEnabled()) {
            return false;
        }

        if(!$moduleEmployee->getExamPermissions()){
            return false;
        }

        $examPermissions = $moduleEmployee->getExamPermissions()->filter(
            function (ExamPermission $employeeExamPermission) use ($moduleExam, $prepost) {
                return $employeeExamPermission->getModuleExam() === $moduleExam && $employeeExamPermission->getPrevioPost() == $prepost;
            })->first();

        if (!$examPermissions) {
            return false;
        }

        //$examPermissions = $moduleEmployee->getExamPermissions();
        //dump($examPermissions);die;
        /*$latestExamPermission = end($examPermissions);
        if(!$latestExamPermission){
            return false;
        }*/
        return $examPermissions->isEnabled();

    }

    /**
     * @param Employee $employee
     * @param ModulePoll $modulePoll
     * @return bool
     */
    public function haveModulePollActive(Employee $employee, ModulePoll $modulePoll){

        $module = $modulePoll->getModule();
        $plan   = $module->getPlan();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if (!$moduleEmployee) {
            return false;
        }

        if (!$moduleEmployee->isEnabled()) {
            return false;
        }

        if(!$moduleEmployee->getPollPermissions()){
            return false;
        }

        $pollPermissions = $moduleEmployee->getPollPermissions()->filter(
            function (PollPermission $employeePollPermission) use ($modulePoll) {
                return $employeePollPermission->getModulePoll() === $modulePoll;
            })->first();

        if (!$pollPermissions) {
            return false;
        }

        return $pollPermissions->isEnabled();

    }

    /**
     * @param Employee $employee
     * @param ModuleExam $moduleExam
     * @return bool
     */
    public function moduleExamApproved(Employee $employee, ModuleExam $moduleExam, $prepost){
        $module = $moduleExam->getModule();
        $plan   = $module->getPlan();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if (!$moduleEmployee) {
            return false;
        }

        if(!$moduleEmployee->getExamPermissions()){
            return false;
        }

        $examPermissions = $moduleEmployee->getExamPermissions()->filter(
            function (ExamPermission $employeeExamPermission) use ($moduleExam, $prepost) {
                return $employeeExamPermission->getModuleExam() === $moduleExam && $employeeExamPermission->getPrevioPost() == $prepost;
            })->first();

        if (!$examPermissions) {
            return false;
        }

        foreach ($examPermissions->getEmployeeExams() as $test){
            if($test->isEvaluated() and $test->isApproved()){
                return true;
            }
        }
        return false;

        //return $examPermissions->getEmployeeExam()->isApproved();
    }

    /**
     * @param Employee $employee
     * @param ModuleExam $moduleExam
     * @return bool
     */
    public function moduleExamFinished(Employee $employee, ModuleExam $moduleExam, $prepost){
        $module = $moduleExam->getModule();
        $plan   = $module->getPlan();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if (!$moduleEmployee) {
            return false;
        }

        if(!$moduleEmployee->getExamPermissions()){
            return false;
        }

        $examPermission = $moduleEmployee->getExamPermissions()->filter(
            function (ExamPermission $employeeExamPermission) use ($moduleExam, $prepost) {
                return $employeeExamPermission->getModuleExam() === $moduleExam && $employeeExamPermission->getPrevioPost() == $prepost;
            })->first();

        if (!$examPermission) {
            return false;
        }

        if(!$examPermission->getEmployeeExams()){
            return false;
        }
        //dump($examPermission);die;

        //return $examPermissions->getEmployeeExam()->isFinished();
        foreach ($examPermission->getEmployeeExams() as $test){
            //dump($test);die;
            if($test->isFinished()){
                return true;
            }
        }
        return false;
    }


    public function moduleExamStarted(Employee $employee, ModuleExam $moduleExam, $prepost){
        $module = $moduleExam->getModule();
        $plan   = $module->getPlan();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if (!$moduleEmployee) {
            return false;
        }

        if(!$moduleEmployee->getExamPermissions()){
            return false;
        }

        $examPermissions = $moduleEmployee->getExamPermissions()->filter(
            function (ExamPermission $employeeExamPermission) use ($moduleExam, $prepost) {
                return $employeeExamPermission->getModuleExam() === $moduleExam && $employeeExamPermission->getPrevioPost() == $prepost;
            })->first();

        if (!$examPermissions) {
            return false;
        }

        return (!empty($examPermissions->getEmployeeExams()));
    }

    /**
     * @param Employee $employee
     * @param ModuleExam $moduleExam
     * @return array
     */
    public function testModuleFinish(Employee $employee, ModuleExam $moduleExam, $prepost){

        $module = $moduleExam->getModule();
        $plan   = $module->getPlan();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if (!$moduleEmployee) {
            return false;
        }

        if(!$moduleEmployee->getExamPermissions()){
            return false;
        }

        $examPermissions = $moduleEmployee->getExamPermissions()->filter(
            function (ExamPermission $employeeExamPermission) use ($moduleExam, $prepost) {
                return $employeeExamPermission->getModuleExam() === $moduleExam && $employeeExamPermission->getPrevioPost() == $prepost;
            })->first();

        if (!$examPermissions) {
            return false;
        }

        if(!$examPermissions->getEmployeeExams()){
            return false;
        }

        return $examPermissions->getEmployeeExams();
    }


    public function testEvaluated(EmployeeModuleExam $test){
        return $test->isEvaluated();
    }

    /**
     * @param Employee $employee
     * @param Module $module
     * @return bool
     */
    public function getDates(Employee $employee, Module $module){

        $plan = $module->getPlan();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if (!$moduleEmployee) {
            return false;
        }

        return [$moduleEmployee->getDateFrom(), $moduleEmployee->getDateTo()];
    }


    /**
     * @param Employee $employee
     * @param Module $module
     * @return bool
     */
    public function haveModuleActive(Employee $employee, Module $module){

        $plan = $module->getPlan();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if (!$moduleEmployee) {
            return false;
        }

        return $moduleEmployee->isEnabled();
    }

    /**
     * @param Employee $employee
     * @param Module $module
     * @return int
     */
    public function haveModuleApprove(Employee $employee, Module $module){

        $plan = $module->getPlan();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            return false;
        }

        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if (!$moduleEmployee) {
            return false;
        }

        return $moduleEmployee->isApproved();
    }
}