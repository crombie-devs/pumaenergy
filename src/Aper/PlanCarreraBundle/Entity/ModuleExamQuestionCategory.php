<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * ModuleExamQuestion
 *
 * @ORM\Table(name="career_plan_module_exam_question_category")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\ModuleExamQuestionCategoryRepository")
 */
class ModuleExamQuestionCategory
{
    // use EnabledTrait;
    use DeletedTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="ModuleExamQuestion", mappedBy="category")
     */
    private $category;

    /**
     * ModuleExamQuestionCategory constructor.
     */
    public function __construct() { }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}

