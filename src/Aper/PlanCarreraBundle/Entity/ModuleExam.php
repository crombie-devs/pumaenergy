<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * ModuleExam
 *
 * @ORM\Table(name="career_plan_module_exam")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\ModuleExamRepository")
 */
class ModuleExam
{
    use EnabledTrait;
    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Module
     * @ORM\OneToOne(targetEntity="Module", inversedBy="exam")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id")
     */
    private $module;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var ModuleExamQuestion[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="ModuleExamQuestion", mappedBy="exam", cascade={"all"})
     * @Assert\Valid()
     */
    private $questions;

    /**
     * @var int
     * @ORM\Column(name="correct_answers_to_approve", type="integer")
     * @Assert\Range(min=0)
     */
    private $correctAnswersToApprove;

    /**
     * @var int
     * @ORM\Column(name="max_question_count", type="integer", nullable=true)
     * @Assert\Range(min=0)
     */
    private $maxQuestionCount;

    /**
     * @var integer
     * @ORM\Column(name="days_for_retry", type="integer")
     * @Assert\NotBlank()
     * @Assert\Range(min=0)
     */
    private $daysForRetry;

    /**
     * @var integer
     * @ORM\Column(name="limit_time_in_seconds", type="integer")
     * @Assert\NotBlank()
     * @Assert\Range(min=0)
     */
    private $limitTimeInSeconds;

    /**
     * @var integer
     * @ORM\Column(name="max_attempts", type="integer", options={"default" : 2})
     * @Assert\NotBlank()
     */
    private $maxAttempts;

    /**
     * ModuleExam constructor.
     */
    public function __construct()
    {
        $this->name = '';
        $this->questions = new ArrayCollection();
        $this->correctAnswersToApprove = 0;
        $this->daysForRetry = 0;
        $this->limitTimeInSeconds = 36000;
        $this->maxQuestionCount = 1;
        $this->maxAttempts = 1000;
    }

    public function isPresencial(){
        return false;
    }

    /**
     * @return int
     */
    public function getMaxAttempts()
    {
        return 1000;
        // return $this->maxAttempts;
    }

    /**
     * @param int $maxAttempts
     */
    public function setMaxAttempts($maxAttempts)
    {
        $this->maxAttempts = $maxAttempts;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param Module $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @param ModuleExamQuestion $question
     */
    public function addQuestion(ModuleExamQuestion $question) {
        $question->setExam($this);
        $this->questions[] = $question;
    }

    /**
     * @param ModuleExamQuestion $question
     */
    public function removeQuestion(ModuleExamQuestion $question) {
        $question->setDeleted(true);
//        $this->questions->removeElement($question);
    }

    /**
     * @param Collection $questions
     */
    public function setQuestions(Collection $questions)
    {
        $this->questions = $questions;
    }

    /**
     * @return ModuleExamQuestion[]|ArrayCollection
     */
    public function getQuestions()
    {
        $questions = $this->questions->filter(function(ModuleExamQuestion $q) { return ($q->isDeleted() === false);});
        $return = new ArrayCollection();
        foreach ($questions as $question){
            $return->add($question);
        }
        return $return;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $v
     * @return $this
     */
    public function setCorrectAnswersToApprove($v)
    {
        $this->correctAnswersToApprove = $v;

        return $this;
    }

    /**
     * @return int
     */
    public function getCorrectAnswersToApprove()
    {
        return $this->correctAnswersToApprove;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s - %s', $this->getModule()->getPlan(), $this->getModule()->getTitle());
    }

    /**
     * @return int
     */
    public function getDaysForRetry()
    {
        return 0;
        // return $this->daysForRetry;
    }

    /**
     * @return int
     */
    public function getLimitTimeInSeconds()
    {
        return $this->limitTimeInSeconds;
    }

    /**
     * @param int $limitTimeInSeconds
     */
    public function setLimitTimeInSeconds($limitTimeInSeconds)
    {
        $this->limitTimeInSeconds = $limitTimeInSeconds;
    }

    /**
     * @param int $daysForRetry
     */
    public function setDaysForRetry($daysForRetry)
    {
        $this->daysForRetry = $daysForRetry;
    }

    /**
     * @return ModuleExamQuestion[]|ArrayCollection
     */
    public function getEnabledQuestions()
    {
        $questions = $this->questions->filter(function (ModuleExamQuestion $question) {
            return ($question->isEnabled() and ($question->isDeleted() === false));
        });
        $return = new ArrayCollection();
        foreach ($questions as $question){
            $return->add($question);
        }
        return $return;
    }

    public function getEnabledQuestionsCount()
    {
        return count($this->getEnabledQuestions());
    }

    /**
     * @return int
     */
    public function getMaxQuestionCount()
    {
        return $this->maxQuestionCount;
    }

    /**
     * @param int $maxQuestionCount
     */
    public function setMaxQuestionCount($maxQuestionCount)
    {
        $this->maxQuestionCount = $maxQuestionCount;
    }

    /**
     * @return bool
     */
    public function isOpen()
    {
        return $this->questions->exists(function ($key, ModuleExamQuestion $question) {
            return $question->getType() === 'Completar';
        });
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     */
    public function validateCorrectAnswersToApproveCount(ExecutionContextInterface $context)
    {
        if($this->correctAnswersToApprove > $this->maxQuestionCount){
            $context->buildViolation('La cantidad de preguntas para aprobar no debe exceder la cantidad de preguntas del test')
                ->atPath('correctAnswersToApprove')
                ->addViolation();
        }
    }

    /**
     * @return Array
     */
    public function getExamQuestionsCategories(Array $examQuestions)
    {

        $return = array();

        foreach ($examQuestions as $examQuestion) {

            if(!(is_null($examQuestion->getCategory()))){

                if(isset($return[$examQuestion->getCategory()->getId()]))
                    continue;

                $return[$examQuestion->getCategory()->getId()] = $examQuestion->getCategory()->getName();
            }

        }

        return $return;
    }
}

