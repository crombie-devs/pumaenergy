<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ModulePoll
 *
 * @ORM\Table(name="career_plan_module_poll")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\ModulePollRepository")
 */
class ModulePoll
{
    use EnabledTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Module", inversedBy="poll")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id")
     */
    private $module;

    /**
     * @var ModulePollQuestion[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="ModulePollQuestion", mappedBy="poll", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $questions;

    /**
     * ModulePoll constructor.
     */
    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Module module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * Get module
     *
     * @return Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param ModulePollQuestion $question
     */
    public function addQuestion(ModulePollQuestion $question)
    {
        $question->setPoll($this);
        $this->questions->add($question);
    }

    /**
     * @param ModulePollQuestion $question
     */
    public function removeQuestion(ModulePollQuestion $question)
    {
        $question->setDeleted(true);
//        $this->questions->removeElement($question);
    }

    /**
     * @param Collection $questions
     */
    public function setQuestions(Collection $questions)
    {
        $this->questions = $questions;
    }

    /**
     * @return ModulePollQuestion[]|ArrayCollection
     */
    public function getQuestions()
    {
        $questions = $this->questions->filter(function($q){ return ($q->isDeleted() === false);});
        $return = new ArrayCollection();
        foreach ($questions as $question){
            $return->add($question);
        }
        return $return;
    }

}

