<?php

namespace Aper\PlanCarreraBundle\Entity;

use Aper\UserBundle\Entity\Employee;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * ExternalTrainingInscription
 *
 * @ORM\Table(name="career_plan_external_training_inscription")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\ExternalTrainingInscriptionRepository")
 */
class ExternalTrainingInscription
{
    use ApprovedTrait;
    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\Employee")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     */
    private $employee;

    /**

     * @ORM\ManyToOne(targetEntity="ExternalTraining", inversedBy="inscriptions")
     * @ORM\JoinColumn(name="training_id", referencedColumnName="id")
     */
    private $training;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default":null}, nullable=true)
     */
    private $assists;

    /**
     * @var EmployeeModuleExam
     * @ORM\OneToOne(targetEntity="EmployeeModuleExam")
     * @ORM\JoinColumn(name="exam_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $employeeExam;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set training
     *
     * @param string $training
     *
     * @return ExternalTrainingInscription
     */
    public function setTraining($training)
    {
        $this->training = $training;

        return $this;
    }

    /**
     * Get training
     *
     * @return ExternalTraining
     */
    public function getTraining()
    {
        return $this->training;
    }


    public function setAssistance($assists)
    {
        $this->assists = $assists;
    }

    /**
     * @return bool
     */
    public function getAssistance()
    {
       return $this->assists;
    }

    /**
     * return bool
     */
    public function getExternalTrainingApproved(){

        $exam   = $this->getTraining()->getModule()->getExam();
        $plan   = $this->getTraining()->getModule()->getPlan();
        $module = $this->getTraining()->getModule();

        if(!$module->isPresencialExam()) return true;

        /** @var EmployeeCareerPlan $employeeCareerPlan */
        $employeeCareerPlan = $this->getEmployee()->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan){
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if(!$employeeCareerPlan) return null;


        /** @var EmployeeCareerModule $employeeCareerModule */
        $employeeCareerModule = $employeeCareerPlan->getModules()->filter(function (EmployeeCareerModule $employeeCareerModule) use ($module){
            return $employeeCareerModule->getModule() === $module;
        })->first();

        if(!$employeeCareerModule) return null;

        /** @var ExamPermission $examPermission */
        $examPermission = $employeeCareerModule->getExamPermissions()->filter(function (ExamPermission $examPermission) use ($exam){
            return $examPermission->getModuleExam() === $exam;
        })->first();

        if(!$examPermission) return null;


        /** @var EmployeeModuleExam[] $tests */
        $tests = $examPermission->getEmployeeExams()->filter(function (EmployeeModuleExam $employeeModuleExam) use($exam) {
            return $employeeModuleExam->getExam() === $exam;
        });

        if(!$tests) return null;

        /** @var  $test */
        foreach ($tests as $test){
            if($test->isFinished()){
                if($test->isApproved()){
                    return true;
                }
            } else {
                return null;
            }
        }
        return null;
    }

    /**
     * @return EmployeeModuleExam
     */
    public function getEmployeeExam()
    {
        return $this->employeeExam;
    }

    /**
     * @param EmployeeModuleExam $employeeExam
     */
    public function setEmployeeExam($employeeExam)
    {
        $this->employeeExam = $employeeExam;
    }

    public function isApproved()
    {
        if($this->employeeExam){
            return $this->employeeExam->isApproved();
        }
        return null;
    }
}

