<?php

namespace Aper\PlanCarreraBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AnswerSnapshot
 * @package Aper\PlanCarreraBundle\Entity
 * @ORM\Embeddable()
 */
class AnswerSnapshot
{
    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $questionId;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $questionText;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $questionType;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $answerId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $answerText;

    /**
     * AnswerSnapshot constructor.
     * @param int $questionId
     * @param string $questionText
     * @param string $questionType
     * @param int $answerId
     * @param string $answerText
     */
    public function __construct($questionId, $questionText, $questionType, $answerId, $answerText)
    {
        $this->questionId = $questionId;
        $this->questionText = $questionText;
        $this->questionType = $questionType;
        $this->answerId = $answerId;
        $this->answerText = $answerText;
    }

    /**
     * @return int
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * @return string
     */
    public function getQuestionText()
    {
        return $this->questionText;
    }

    /**
     * @return string
     */
    public function getQuestionType()
    {
        return $this->questionType;
    }

    /**
     * @return int
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

    /**
     * @return string
     */
    public function getAnswerText()
    {
        return $this->answerText;
    }

}