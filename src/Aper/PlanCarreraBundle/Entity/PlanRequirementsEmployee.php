<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * PlanRequirementsEmployee
 *
 * @ORM\Table(name="career_plan_employee_requirements")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\PlanRequirementsEmployeeRepository")
 */
class PlanRequirementsEmployee
{
    use TimestampableEntity;
    use ApprovedTrait;
    use EnabledTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var PlanRequirement
     * @ORM\ManyToOne(targetEntity="PlanRequirement", cascade={"persist"})
     * @ORM\JoinColumn(name="requirement_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $requirement;

    /**
     * @var EmployeeCareerPlan
     * @ORM\ManyToOne(targetEntity="Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan",inversedBy="requirements", cascade={"persist"})
     * @ORM\JoinColumn(name="employee_career_plan_id", referencedColumnName="id")
     */
    private $employeeCareerPlan;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param PlanRequirement $requirement
     */
    public function setRequirement($requirement)
    {
        $this->requirement = $requirement;
    }

    /**
     * @return PlanRequirement
     */
    public function getRequirement()
    {
        return $this->requirement;
    }

    /**
     * @param EmployeeCareerPlan $employeeCareerPlan
     */
    public function setEmployeeCareerPlan(EmployeeCareerPlan $employeeCareerPlan)
    {
        $this->employeeCareerPlan = $employeeCareerPlan;
    }

    /**
     * @return EmployeeCareerPlan
     */
    public function getEmployeeCareerPlan()
    {
        return $this->employeeCareerPlan;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf("%s", $this->requirement);
    }

}

