<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * PollPermission
 * Este es el permiso que va a tener el empleado para poder realizar la encuesta del módulo.
 *
 * @ORM\Table(name="career_plan_poll_permission")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\PollPermissionRepository")
 */
class PollPermission
{
    use EnabledTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var EmployeeCareerModule
     * @ORM\ManyToOne(targetEntity="EmployeeCareerModule", inversedBy="pollPermissions")
     * @ORM\JoinColumn(name="employee_career_module_id", referencedColumnName="id")
     */
    private $employeeModule;

    /**
     * @var ModulePoll
     * @ORM\ManyToOne(targetEntity="ModulePoll", cascade={"persist"})
     * @ORM\JoinColumn(name="poll_id", referencedColumnName="id")
     */
    private $modulePoll;

    /**
     * ExamPermission constructor.
     * @param EmployeeCareerModule|null $employeeModule
     * @param ModulePoll|null $modulePoll
     */
    public function __construct(EmployeeCareerModule $employeeModule = null, ModulePoll $modulePoll = null)
    {
        $this->employeeModule = $employeeModule;
        $this->modulePoll     = $modulePoll;
    }

    /**
     * Función que devuelve el próximo paso permitido que tiene el empleado con el examen del módulo.
     *
     * @return string
     */
    public function getNextStep(){
        if($this->isEnabled() === false){
            //en caso que le hayan quitado el permiso de la encuesta
            return 'none';
        }


		// var_dump($this->getId());
		
        if(!$this->modulePoll->isEnabled()){
            return 'none';
        }

        if($this->employeeModule->getEmployeeAnswers()->isEmpty()){
            return 'start';
        }

        if($this->employeeModule->getEmployeeAnswers()->count() >= $this->modulePoll->getQuestions()->count()){
            return 'finish';
        }

//        if($this->employeeModule->getEmployeeAnswers()->count() < $this->modulePoll->getQuestions()->count()){
//            return 'continue';
//        }

        // si llega aca es porque puede comenzar una encuesta
        return 'continue';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set employeeModule
     *
     * @param EmployeeCareerModule $employeeModule
     */
    public function setEmployeeModule($employeeModule)
    {
        $this->employeeModule = $employeeModule;
    }

    /**
     * @return EmployeeCareerModule
     */
    public function getEmployeeModule()
    {
        return $this->employeeModule;
    }

    /**
     * @param $modulePoll
     */
    public function setModulePoll($modulePoll)
    {
        $this->modulePoll = $modulePoll;
    }

    /**
     * @return ModulePoll
     */
    public function getModulePoll()
    {
        return $this->modulePoll;
    }

}

