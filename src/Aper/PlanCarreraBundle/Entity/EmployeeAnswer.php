<?php

namespace Aper\PlanCarreraBundle\Entity;

use Aper\UserBundle\Entity\Employee;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class EmployeeAnswer
 * @package Aper\PlanCarreraBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name="employee_poll_answers")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\EmployeeAnswerRepository")
 */
class EmployeeAnswer
{
    use TimestampableEntity;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Employee
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\Employee")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $employee;

    /**
     * @var EmployeeCareerModule
     * @ORM\ManyToOne(targetEntity="Aper\PlanCarreraBundle\Entity\EmployeeCareerModule", inversedBy="employeeAnswers")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $module;

    /**
     * @var AnswerSnapshot
     * @ORM\Embedded(class="Aper\PlanCarreraBundle\Entity\AnswerSnapshot", columnPrefix="snapshot_")
     */
    private $data;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param Employee $employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    /**
     * @return EmployeeCareerModule
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param EmployeeCareerModule $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return AnswerSnapshot
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param AnswerSnapshot $data
     */
    public function setData(AnswerSnapshot $data)
    {
        $this->data = $data;
    }

}