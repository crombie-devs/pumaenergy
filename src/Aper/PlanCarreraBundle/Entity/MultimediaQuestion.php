<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\FileBundle\Model\Image;

/**
 * @ORM\Entity
 * @ORM\Table(name="career_plan_exam_question_multimedia")
 */
class MultimediaQuestion extends Image
{
    /**
     * Directorio relativo a guardar las imágenes.
     */
    const DIRECTORY = 'uploads/career/module/exam/question/';

    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var UploadedFile
     * @Assert\File(
     *     maxSize = "10M"
     * )
     */
    protected $file;

    /**
     * @var ModuleExamQuestion
     * @ORM\OneToOne(targetEntity="ModuleExamQuestion", inversedBy="file", cascade={"persist"})
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ModuleExamQuestion
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param ModuleExamQuestion $question
     */
    public function setQuestion(ModuleExamQuestion $question)
    {
        $this->question = $question;
    }



}
