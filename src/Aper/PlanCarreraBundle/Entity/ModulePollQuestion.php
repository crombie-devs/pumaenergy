<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ModulePollQuestion
 *
 * @ORM\Table(name="career_plan_module_poll_question")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\ModulePollQuestionRepository")
 */
class ModulePollQuestion
{
    // use DeletedTrait;
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", options={"default":false})
     */
    private $deleted = false;

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return 1;
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     * @Assert\NotBlank()
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity="ModulePoll", inversedBy="questions", cascade={"persist"})
     * @ORM\JoinColumn(name="poll_id", referencedColumnName="id")
     */
    private $poll;

    /**
     * @var ModulePollAnswer[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="ModulePollAnswer", mappedBy="question", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $answers;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @var ModulePollQuestionImage
     * @ORM\OneToOne(targetEntity="Aper\PlanCarreraBundle\Entity\ModulePollQuestionImage", mappedBy="question", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $image;

    /**
     * ModulePollQuestion constructor.
     */
    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return ModulePollQuestion
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }


    public function setAnswers(Collection $answers)
    {
        $this->answers = $answers;
    }

    public function addAnswer(ModulePollAnswer $answer)
    {
        $answer->setQuestion($this);
        $this->answers->add($answer);
    }

    public function removeAnswer(ModulePollAnswer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * @return ModulePollAnswer[]|ArrayCollection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    public function getPoll()
    {
        return $this->poll;
    }


    public function setPoll(ModulePoll $poll)
    {
        $this->poll = $poll;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;

    }

    /**
     * @return ModulePollQuestionImage
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param ModulePollQuestionImage $image
     */
    public function setImage($image)
    {
        $image->setQuestion($this);
        $this->image = $image;
    }


}

