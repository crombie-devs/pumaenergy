<?php

namespace Aper\PlanCarreraBundle\Entity;

use Aper\UserBundle\Entity\Employee;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * EmployeeCareerPlan
 *
 * @ORM\Table(name="career_plan_employee_module")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\EmployeeCareerModuleRepository")
 */
class EmployeeCareerModule
{
    use TimestampableEntity;
    use ApprovedTrait;
    use EnabledTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_from", type="datetime", nullable=true)
     */
    private $dateFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_to", type="datetime", nullable=true)
     */
    private $dateTo;

    /**
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id")
     */
    private $module;

    /**
     * @ORM\ManyToOne(targetEntity="EmployeeCareerPlan",inversedBy="modules")
     * @ORM\JoinColumn(name="employee_career_plan_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $employee_plan;

    /**
     * @var ExamPermission[]
     * @ORM\OneToMany(targetEntity="ExamPermission", mappedBy="employeeModule", cascade={"all"})
     */
    private $examPermissions;

    private $examPermissionsPost;

    /**
     * @var PollPermission[]
     * @ORM\OneToMany(targetEntity="PollPermission", mappedBy="employeeModule", cascade={"all"})
     */
    private $pollPermissions;

    /**
     * La cantidad de preguntas que tiene la encuesta del modulo al momento de asociar al empleado
     *
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $originalPollQuestionCount;
    /**
     * @var EmployeeAnswer[]
     * @ORM\OneToMany(targetEntity="Aper\PlanCarreraBundle\Entity\EmployeeAnswer", mappedBy="module")
     */
    private $employeeAnswers;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var Employee
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\Employee")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $commentAuthor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="interactive_course", type="boolean", options={"default":false})
     */
    private $interactiveCourse = false;

    /**
     * @var \DateTime
     * @ORM\Column(name="interactive_course_at", type="datetime", nullable=true)
     */
    protected $courseApprovedAt;


    /**
     * EmployeeCareerModule constructor.
     */
    public function __construct()
    {
        $this->employeeAnswers = new ArrayCollection();
        $this->examPermissions = new ArrayCollection();
        $this->pollPermissions = new ArrayCollection();


    }

    public function getExamPermissionsPre(){
        $exam = $this->module->getExam();
        $this->examPermissionsPost = $this->getExamPermissions()->filter(
            function (ExamPermission $employeeExamPermission) use ($exam) {
                return $employeeExamPermission->getModuleExam() === $exam && $employeeExamPermission->getPrevioPost() == false;
            })->first();
        return $this->examPermissionsPost;
    }

    public function getExamPermissionsPost(){
        $exam = $this->module->getExam();
        $this->examPermissionsPost = $this->getExamPermissions()->filter(
            function (ExamPermission $employeeExamPermission) use ($exam) {
                return $employeeExamPermission->getModuleExam() === $exam && $employeeExamPermission->getPrevioPost() == true;
            })->first();
        return $this->examPermissionsPost;
    }

    /**
     * @return bool
     */
    public function isCourseApproved()
    {
        return $this->interactiveCourse;
    }

    /**
     * @param bool $approved
     */
    public function setCourseApproved($approved)
    {
        if ($approved) {
            $this->courseApprovedAt = new \DateTime();
        } else {
            $this->courseApprovedAt = null;
        }
        $this->interactiveCourse = $approved;
    }

    public function getCourseApprovedAt()
    {
        return $this->courseApprovedAt;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setDateFrom($dateFrom){
        $this->dateFrom = $dateFrom;
        return $this;
    }

    public function setDateTo($dateTo){
        $this->dateTo = $dateTo;
        return $this;
    }

    public function getDateFrom(){
        return $this->dateFrom;
    }

    public function getDateTo(){
        return $this->dateTo;
    }

    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * @return Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return Employee
     */
    public function getCommentAuthor()
    {
        return $this->commentAuthor;
    }

    /**
     * @param Employee $commentAuthor
     */
    public function setCommentAuthor($commentAuthor)
    {
        $this->commentAuthor = $commentAuthor;
    }

    /**
     * @return array
     */
    public function getCommentArray()
    {
        if(is_null($this->commentAuthor)){
            return [];
        }

        return ['text'   => $this->comment,
                'author' => ucwords(strtolower($this->commentAuthor->getProfile()->getName())),
                'role'   => $this->commentAuthor->getUser()->getMaxRole()];
    }



    /**
     * @return bool
     * @deprecated
     */
    public function getApproved()
    {
        return $this->isApproved();
    }

    public function setEmployeePlan(EmployeeCareerPlan $plan)
    {
        $this->employee_plan = $plan;

        return $this;
    }

    /**
     * @return EmployeeCareerPlan
     */
    public function getEmployeePlan()
    {
        return $this->employee_plan;
    }

    public function getModulesApproved(){
        return $this->getModules()->filter(
            function($entry){
                return $entry->getNivelation() === true;
            }
        );
    }

    /**
     * @return int
     */
    public function getOriginalPollQuestionCount()
    {
        return $this->originalPollQuestionCount;
    }

    /**
     * @param int $originalPollQuestionCount
     */
    public function setOriginalPollQuestionCount($originalPollQuestionCount)
    {
        $this->originalPollQuestionCount = $originalPollQuestionCount;
    }

    /**
     * @return EmployeeAnswer[]|ArrayCollection
     */
    public function getEmployeeAnswers()
    {
        return $this->employeeAnswers;
    }

    /**
     * @param EmployeeAnswer[] $employeeAnswers
     */
    public function setEmployeeAnswers($employeeAnswers)
    {
        $this->employeeAnswers = $employeeAnswers;
    }

    /**
     * @param ModulePollQuestion $question
     * @return bool
     */
    public function isAnswered(ModulePollQuestion $question)
    {
        $answer = $this->employeeAnswers->filter(function (EmployeeAnswer $ea) use ($question) {
            return $ea->getData()->getQuestionId() === $question->getId();
        })->first();

        return (boolean)$answer;
    }

    /**
     * @return ExamPermission[]|ArrayCollection
     */
    public function getExamPermissions()
    {
        return $this->examPermissions;
    }

    /**
     * @return ExamPermission
     */
    public function getExamPermission()
    {
        return $this->examPermissions->last();
    }

    /**
     * @param ExamPermission $exam_permission
     */
    public function addExamPermission(ExamPermission $exam_permission)
    {
        $this->examPermissions->add($exam_permission);
    }

    /**
     * @return PollPermission[]|ArrayCollection
     */
    public function getPollPermissions()
    {
        return $this->pollPermissions;
    }

    /**
     * @return PollPermission
     */
    public function getPollPermission()
    {
        return $this->pollPermissions->last();
    }

    /**
     * @param PollPermission $pollPermission
     */
    public function addPollPermission(PollPermission $pollPermission)
    {
        $this->pollPermissions->add($pollPermission);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s', $this->module);
    }

    /**
     * Devuelve todos los entrenamientos externos del módulo
     *
     * @return ExternalTraining[]|ArrayCollection|string
     */
    public function getExternalTrainings(){
        return $this->getModule()->getExternalTrainings(); // fix temporal para enviar todos los entrenamientos externos

//        $employee = $this->getEmployeePlan()->getEmployee();
//        $return = $this->getModule()->getExternalTrainings()->filter(function (ExternalTraining $external) use ($employee) {
//            foreach ($external->getInscriptions() as $inscription){
//                if ($inscription->getEmployee() === $employee){
//                    return true;
//                }
//            }
//            return false;
//        });
//        foreach ($return as $external){
//            /** @var ExternalTraining $external*/
//            foreach ($external->getInscriptions() as $inscription){
//                if ($inscription->getEmployee() !== $employee){
//                    $external->removeInscription($inscription);
//                }
//            }
//        }
//        if($return->isEmpty()){
//            return "null";
//        }
//        return $return;
    }

    /**
     * Devuelve los entrenamientos externos en los que está inscripto el empleado en este módulo,
     * y el estado de esa inscripción (asistencia, aprobación)
     *
     * @return ExternalTraining[]|ArrayCollection|string
     */
    public function getExternalTrainingsInscribed(){

        $employee = $this->getEmployeePlan()->getEmployee();
        $return = $this->getModule()->getExternalTrainings()->filter(function (ExternalTraining $external) use ($employee) {
            foreach ($external->getInscriptions() as $inscription){
                if ($inscription->getEmployee() === $employee){
                    return true;
                }
            }
            return false;
        });
        foreach ($return as $external){
            /** @var ExternalTraining $external*/
            foreach ($external->getInscriptions() as $inscription){
                if ($inscription->getEmployee() !== $employee){
                    $external->removeInscription($inscription);
                }
            }
        }
        if($return->isEmpty()){
            return "null";
        }
        return $return;
    }
}

