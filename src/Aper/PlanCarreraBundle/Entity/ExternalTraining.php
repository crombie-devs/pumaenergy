<?php

namespace Aper\PlanCarreraBundle\Entity;

use Aper\StoreBundle\Entity\Store;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * ExternalTraining
 *
 * @ORM\Table(name="career_plan_external_training")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\ExternalTrainingRepository")
 */
class ExternalTraining
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var Store[]
     * @ORM\ManyToMany(targetEntity="Aper\StoreBundle\Entity\Store")
     */
    private $stores;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=150)
     * @Assert\NotBlank()
     */
    private $place;

    /**
     * @var int
     *
     * @ORM\Column(name="quota", type="smallint")
     * @Assert\NotBlank()
     */
    private $quota;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     * @Assert\NotBlank()
     */
    private $date;

    /**
     * @var string
     * @ORM\Column(type="string", length=150)
     * @Assert\NotBlank()
     */
    private $duration;

    /**
     * @var string
     * @ORM\Column(type="string", length=150, nullable=true)
     *
     */
    private $expositor;

    /** @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $urlMultimedia;

//    /**
//     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\User")
//     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
//     * @Assert\NotBlank()
//     */
//    private $trainer;

    /**
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\User", inversedBy="expositions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $trainer;

    /**
     * @var Module
     * @ORM\ManyToOne(targetEntity="Module", inversedBy="externalTrainings")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id", nullable=false)
     */
    private $module;


    /**
     * @var ExternalTrainingInscription[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="ExternalTrainingInscription", mappedBy="training", cascade={"all"}, orphanRemoval=true)
     */
    private $inscriptions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="archived", type="boolean", options={"default":false})
     */
    private $archived;

    /**
     * ExternalTraining constructor.
     */
    public function __construct()
    {
        $this->inscriptions = new ArrayCollection();
        $this->stores = new ArrayCollection();
        $this->archived = false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set place
     *
     * @param string $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set quota
     *
     * @param integer $quota
     */
    public function setQuota($quota)
    {
        $this->quota = $quota;
    }

    /**
     * Get quota
     *
     * @return int
     */
    public function getQuota()
    {
        return $this->quota;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set trainer
     *
     * @param integer $trainer
     */
    public function setTrainer($trainer)
    {
        $this->trainer = $trainer;
    }

    /**
     * Get trainer
     *
     * @return User
     */
    public function getTrainer()
    {
        return $this->trainer;
    }


    /**
     * @param Module $module
     */
    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    /**
     * @return bool
     */
    public function getEvaluable()
    {
        if(!$this->module) return false;
        return $this->module->isPresencialExam();
    }

    /**
     * @return Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @return ExternalTrainingInscription[]|ArrayCollection
     */
    public function getInscriptions()
    {
        return $this->inscriptions;
    }

    /**
     * @param ExternalTrainingInscription $inscription
     * @return $this
     */
    public function addInscription(ExternalTrainingInscription $inscription) {
        $this->inscriptions[] = $inscription;
        return $this;
    }

    /**
     * @param ExternalTrainingInscription $inscription
     */
    public function removeInscription(ExternalTrainingInscription $inscription) {
        $this->inscriptions->removeElement($inscription);
    }

    /**
     * @param ExternalTrainingInscription[]|ArrayCollection $inscriptions
     */
    public function setInscriptions($inscriptions)
    {
        $this->inscriptions = $inscriptions;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s - %s (%s)', $this->name, $this->place, ((!is_null($this->expositor)) ? $this->expositor : $this->getTrainer()));
    }

    /**
     * @param Employee $employee
     * @return ExternalTrainingInscription
     */
    public function getInscriptionForEmployee(Employee $employee)
    {
        $inscriptions = $this->inscriptions->filter(function (ExternalTrainingInscription $inscription) use ($employee) {
            return $inscription->getEmployee() === $employee;
        });

        return $inscriptions->last() ?: null;
    }

    /**
     * @param Employee $employee
     * @throws \Exception
     */
    public function subscribe(Employee $employee)
    {
        $now = new \DateTime();
        if ($this->date < $now) {
            throw new \DomainException("El curso ya terminó.");
        }

        if ($this->quota <= $this->inscriptions->count()) {
            throw new \DomainException("No hay cupo.");
        }

        if (!$employee->isCareerPlanEnabled($this->module->getPlan())) {
            throw new \DomainException("El plan no esta habilitado para este empleado.");
        }

        $inscription = $this->getInscriptionForEmployee($employee);
        if ($inscription) {
            throw new \InvalidArgumentException("El empleado ya esta inscripto.");
        }

        $inscription = new ExternalTrainingInscription();
        $inscription->setEmployee($employee);
        $inscription->setTraining($this);
        $inscription->setCreatedAt(new \DateTime());

        $this->inscriptions->add($inscription);
    }

    /**
     * @param Employee $employee
     * @throws \Exception
     */
    public function unsubscribe(Employee $employee)
    {
        $now = new \DateTime();
        if ($this->date < $now) {
            throw new \DomainException("El curso ya terminó.");
        }

        $inscription = $this->getInscriptionForEmployee($employee);
        if (!$inscription) {
            throw new \InvalidArgumentException("El empleado no esta inscripto.");
        }

        $this->inscriptions->removeElement($inscription);
    }

    /**
     * @param ExecutionContextInterface $context
     * @throws \Exception
     * @Assert\Callback()
     */
    public function validateDate(ExecutionContextInterface $context)
    {
        if (!$this->id && $this->date < new \DateTime()) {
            $context->buildViolation('La fecha del entrenamiento debe ser a futuro')
                ->atPath('date')
                ->addViolation();
        }
    }

    /**
     * @return Store[]|ArrayCollection
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * @param Store[] $stores
     */
    public function setStores($stores)
    {
        $this->stores = $stores;
    }

    /**
     * @param Store $store
     */
    public function addStore(Store $store)
    {
        $this->stores->add($store);
    }

    /**
     * @param Store $store
     */
    public function removeStore(Store $store)
    {
        $this->stores->removeElement($store);
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getExpositor()
    {
        return $this->expositor;
    }

    /**
     * @return string
     */
    public function getExpositorOrTrainer()
    {
        return ((is_null($this->expositor)) ? (($this->getTrainer()) ? sprintf('%s - %s', $this->getTrainer()->getEmployee()->getCode(), $this->getTrainer()->getEmployee()->getProfile()->getName()) : '') : $this->expositor);
    }

    /**
     * @param string $expositor
     */
    public function setExpositor($expositor)
    {
        $this->expositor = $expositor;
    }

    /**
     * @return string
     */
    public function getCapacityAvailable(){
        return count($this->getInscriptions()) . '/' . $this->quota;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isArchived()
    {
        if ($this->getDate() > new \DateTime()){
            return false;
        }
        return ($this->getDate()->diff(new \DateTime())->days >= 0);
        //return $this->archived;
    }

    /**
     * @param bool $archived
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;
    }


    /**
     * @return int
     */
    public function getAttendees()
    {
        return $this->inscriptions->filter(function (ExternalTrainingInscription $inscription){
            return $inscription->getAssistance();
        })->count();
    }


    /**
     * @return int
     */
    public function getApproved()
    {
//        $module = $this->getModule();
//        if($module->isPresencialExam()){
//
//            $moduleExam = $module->getExam();
//
//            if(!$moduleExam){
//                return null;
//            }
//
//            $examPermissions = $moduleExam->get
//        }
//
//            $employeeModuleExam = $this->employeeModuleExamRepository->findByEmployeeAndExam($moduleExam, $employee);
//
//            if($moduleExam->getMaxAttempts()){
//                if((count($employeeModuleExam) < $moduleExam->getMaxAttempts())){
//
//                    $test = EmployeeModuleExam::create($employee, $moduleExam);
//                    $testRepository = $this->em->getRepository(EmployeeModuleExam::class);
//                    $testRepository->save($test);
////                $this->em->save($test);
//
//                } else {
//                    $test = reset($employeeModuleExam);
//
//
//
//        }
        return $this->inscriptions->filter(function (ExternalTrainingInscription $inscription){
            $exam = $this->module->getExam();
            $plan = $this->module->getPlan();
            $module = $this->module;

            /** @var EmployeeCareerPlan $employeeCareerPlan */
            $employeeCareerPlan = $inscription->getEmployee()->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan){
                return $employeeCareerPlan->getPlan() === $plan;
            })->first();

            if(!$employeeCareerPlan) return false;


            /** @var EmployeeCareerModule $employeeCareerModule */
            $employeeCareerModule = $employeeCareerPlan->getModules()->filter(function (EmployeeCareerModule $employeeCareerModule) use ($module){
                return $employeeCareerModule->getModule() === $module;
            })->first();

            if(!$employeeCareerModule) return false;

            /** @var ExamPermission $examPermission */
            $examPermission = $employeeCareerModule->getExamPermissions()->filter(function (ExamPermission $examPermission) use ($exam){
               return $examPermission->getModuleExam() === $exam;
            })->first();

            if(!$examPermission) return false;


            /** @var EmployeeModuleExam[] $tests */
            $tests = $examPermission->getEmployeeExams()->filter(function (EmployeeModuleExam $employeeModuleExam) use($exam) {
               return $employeeModuleExam->getExam() === $exam;
            });

            if(!$tests) return false;

            /** @var  $test */
            foreach ($tests as $test){
                if($test->isFinished() and $test->isApproved()){
                    return true;
                }
            }
            return false;

//            $testsEmployee = $inscription->getEmployee()->
//                $inscription->getEmployee()->
//            return $inscription->isApproved();
        })->count();
    }

    /**
     * @return string
     */
    public function getUrlMultimedia()
    {
        return $this->urlMultimedia;
    }

    /**
     * @param string $urlMultimedia
     */
    public function setUrlMultimedia($urlMultimedia)
    {
        $this->urlMultimedia = $urlMultimedia;
    }



}

