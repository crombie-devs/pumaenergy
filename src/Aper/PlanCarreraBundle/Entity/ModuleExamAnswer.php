<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ModuleExamAnswer
 *
 * @ORM\Table(name="career_plan_module_exam_answer")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\ModuleExamAnswerRepository")
 */
class ModuleExamAnswer
{
    use EnabledTrait;
    use TimestampableEntity;
    use DeletedTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $text;

    /**
     * @var ModuleExamQuestion
     * @ORM\ManyToOne(targetEntity="ModuleExamQuestion", inversedBy="answers", cascade={"persist"})
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;

    /**
     * @var bool
     *
     * @ORM\Column(name="correct", type="boolean")
     * @Assert\NotNull()
     */
    private $correct;

    /**
     * ModuleExamAnswer constructor.
     */
    public function __construct()
    {
        $this->correct = false;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @param $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }


    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return ModuleExamQuestion
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param ModuleExamQuestion $question
     */
    public function setQuestion(ModuleExamQuestion $question)
    {
        $this->question = $question;
    }

    /**
     * @param $correct
     */
    public function setCorrect($correct)
    {
        $this->correct = $correct;
    }

    /**
     * @return bool
     */
    public function isCorrect()
    {
        return $this->correct;
    }

    /**
     *
     */
    public function checkEnabledTree()
    {
        if (!$this->isEnabled()) {
            throw new \InvalidArgumentException();
        }

        if (!$this->getQuestion()->isEnabled()) {
            throw new \InvalidArgumentException();
        }

        if (!$this->getQuestion()->getExam()->isEnabled()) {
            throw new \InvalidArgumentException();
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->text;
    }
}

