<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Requirements
 *
 * @ORM\Table(name="career_plan_requirements")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\PlanRequirementRepository")
 */
class PlanRequirement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="CareerPlan", inversedBy="requirements")
     * @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     */
    private $plan;


    public function __toString() {
        return $this->description;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set plan
     *
     * @param PlanCarrera $plan
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     *
     * @return PlanCarrera
     */
    public function getPlan()
    {
        return $this->plan;
    }

}

