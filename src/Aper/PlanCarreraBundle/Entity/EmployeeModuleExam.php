<?php

namespace Aper\PlanCarreraBundle\Entity;

use Aper\UserBundle\Entity\Employee;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * EmployeeModuleExam
 *
 * @ORM\Table(name="career_plan_employee_module_exam")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\EmployeeModuleExamRepository")
 */
class EmployeeModuleExam
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Employee
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\Employee")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     */
    private $employee;

    /**
     * @var ModuleExam
     * @ORM\ManyToOne(targetEntity="Aper\PlanCarreraBundle\Entity\ModuleExam")
     * @ORM\JoinColumn(name="exam_id", referencedColumnName="id")
     */
    private $exam;

    /**
     * @var ExamPermission
     * @ORM\ManyToOne(targetEntity="ExamPermission", inversedBy="employeeExams")
     * @ORM\JoinColumn(name="exam_permission_id", referencedColumnName="id")
     */
    private $examPermission;

    /**
     * @var EmployeeExamAnswer[]|Collection
     * @ORM\OneToMany(targetEntity="Aper\PlanCarreraBundle\Entity\EmployeeExamAnswer", mappedBy="employeeExam", cascade={"all"})
     */
    private $answers;

    /**
     * @var \DateTime
     * @ORM\Column(name="starts_at", type="datetime")
     */
    private $startsAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="finished_at", type="datetime", nullable=true)
     */
    private $finishedAt;

    /**
     * @var bool
     *@ORM\Column(type="boolean", nullable=true)
     */
    private $approved;

    /**
     * @var bool
     *
     * @ORM\Column(name="presencialExam", type="boolean", options={"default":false})
     */
    private $presencialExam = false;

    /**
     * @return bool
     */
    public function isPresencialExam()
    {
        return $this->presencialExam;
    }

    /**
     * @param bool $presencialExam
     */
    public function setPresencialExam($presencialExam)
    {
        $this->presencialExam = $presencialExam;
    }

    /**
     * EmployeeModuleExam constructor.
     * @param Employee $employee
     * @param ModuleExam $exam
     */
    private function __construct(Employee $employee, ModuleExam $exam, $prepost)
    {
        $module = $exam->getModule();
        $plan   = $module->getPlan();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            throw new \DomainException("El empleado no tiene el Plan Activado.");
        }
        /** @var EmployeeCareerModule $moduleEmployee */
        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if(!$moduleEmployee){
            throw new \DomainException("El empleado no tiene el Módulo del Plan.");
        }

        $examPermission = $moduleEmployee->getExamPermissions()->filter(
            function (ExamPermission $employeeExamPermission) use ($exam, $prepost) {
                return $employeeExamPermission->getModuleExam() === $exam && $employeeExamPermission->getPrevioPost() == $prepost;
            })->first();

        if(!$examPermission){
            throw new \DomainException("El empleado no tiene el Examen Activado.");
        }

        $this->employee       = $employee;
        $this->exam           = $exam;
        $this->startsAt       = new \DateTime();
        $this->answers        = new ArrayCollection();
        $this->approved       = null;
        $this->examPermission = $examPermission;
        $this->presencialExam = $exam->isPresencial();
    }

    /**
     * @param $employee
     * @param $exam
     * @return EmployeeModuleExam
     */
    public static function create(Employee $employee, ModuleExam $exam, $prepost)
    {
        $test = new EmployeeModuleExam($employee, $exam, $prepost);
        if(!$exam->getModule()->isPresencialExam()){

            $examQuestions = $exam->getQuestions()->toArray();
            $examTotalQuestions = $exam->getMaxQuestionCount();

            if ($exam->isOpen()) {
                $questions = $exam->getQuestions()->slice(0, $examTotalQuestions);
            } else {
                $questions = new ArrayCollection();
                $categories = $exam->getExamQuestionsCategories($examQuestions);

                $totalPerCategory = $examTotalQuestions / count($categories);

                $cant = 0;
                foreach ($categories as $categoryId => $categoryName) {
                    $cant++;

                    if($cant == count($categories))
                        $totalPerCategory = $examTotalQuestions - (($cant-1) * floor($totalPerCategory));

                    $questionsByCategory = (array)array_rand(
                        $exam->getQuestions()->filter(function(ModuleExamQuestion $q) use ($categoryId){
                            if(!(is_null($q->getCategory()))){
                                return ($q->getCategory()->getId() == $categoryId && $q->isEnabled());
                            }else{
                                return ;
                            }
                        })->toArray(),
                        floor($totalPerCategory));
                    
                    $questions = new ArrayCollection(
                        array_merge($questions->toArray(), $questionsByCategory)
                    );
                }
            }

            foreach ($questions as $question) {
                $testAnswer = new EmployeeExamAnswer();
                $testAnswer->setQuestion($examQuestions[$question]);
                $testAnswer->setEmployeeExam($test);
                $test->answers->add($testAnswer);
            }
        }

        return $test;
    }

    /**
     * @param ModuleExamQuestion $moduleExamQuestion
     * @return EmployeeExamAnswer|boolean
     */
    public function getAnswerByQuestion(ModuleExamQuestion $moduleExamQuestion)
    {
        return $this->answers->filter(function (EmployeeExamAnswer $a) use ($moduleExamQuestion) {
            return $a->getQuestion() === $moduleExamQuestion;
        })->first();
    }

    /**
     * @return bool
     */
    public function isCompleted()
    {
        return $this->exam->getMaxQuestionCount() === $this->getTotalAnswers();
    }

    /**
     *
     */
    public function finish()
    {
        $this->finishedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answers
     *
     * @param array $answers
     *
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }

    /**
     * Get answers
     *
     * @return ArrayCollection|EmployeeExamAnswer[]
     */
    public function getAnswers()
    {
        return $this->answers;
    }


    public function getExam()
    {
        return $this->exam;
    }


    public function setExam(ModuleExam $exam)
    {
        $this->exam = $exam;
        $this->presencialExam = $exam->isPresencial();
    }

    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    public function getExamPermission(){
        return $this->examPermission;
    }

    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Determina si el test puede realizarse, tomando en cuenta si lo ha terminado (o ha pasado el tiempo timeout)
     * Si no está aprobado y si debe esperar cierta cantidad de dias para volver a realizarlo.
     * @return bool
     */
    public function canBeStarted()
    {
        $isFinished = $this->isFinished();
        $isTimeOut = $this->isTimeOut();
        $isApprove = $this->isApproved();
        $nextChanceAllowed = $this->getStartsAt() < new \DateTime("- {$this->getExam()->getDaysForRetry()} days");

        return !$isFinished && !$isTimeOut || (($isFinished || $isTimeOut) && !$isApprove && $nextChanceAllowed);
    }

    public function getRemainingTime(){
        $due_date =  clone $this->getStartsAt();
        $due_date->modify("+ {$this->getExam()->getLimitTimeInSeconds()} secs");
        $now = new \DateTime();
        
        if($now >= $due_date){
            return '00:00:00';
        }

        $interval = $due_date->diff($now);
        $h = ($interval->days * 60) + $interval->h;
        $i = $interval->i;
        $s = $interval->s;
        
        return ($h . ':' . (($i < 10) ? '0' . $i : $i) . ':' . (($s < 10) ? '0' . $s : $s));
    }

    public function getRemainingTimeSeconds(){
        $due_date =  clone $this->getStartsAt();
        $due_date->modify("+ {$this->getExam()->getLimitTimeInSeconds()} secs");
        $now = new \DateTime();
        
        if($now >= $due_date){
            return '0';
        }
        
        return ($due_date->getTimestamp() - $now->getTimestamp());
    }

    public function getDueDate(){
        $due_date =  clone $this->getStartsAt();
        $due_date->modify("+ {$this->getExam()->getLimitTimeInSeconds()} secs");
        
        return $due_date;
    }

    public function getDueDateDate(){
        $due_date =  clone $this->getStartsAt();
        $due_date->modify("+ {$this->getExam()->getLimitTimeInSeconds()} secs");
        
        return $due_date->format('d/m/Y');
    }

    public function getDueDateTime(){
        $due_date =  clone $this->getStartsAt();
        $due_date->modify("+ {$this->getExam()->getLimitTimeInSeconds()} secs");
        
        return $due_date->format('H:i:s');
    }


    public function getNextChanceDateFormatted(){
        $d = $this->getNextChanceDatetime();
        
        if(is_bool($d)){
            return $d;
        }
        
        return $this->getNextChanceDatetime()->format('d/m/Y');
    }


    public function getNextChanceTimeFormatted(){
        $d = $this->getNextChanceDatetime();
        
        if(is_bool($d)){
            return $d;
        }
        
        return $this->getNextChanceDatetime()->format('H:i:s');
    }

    /**
     * @return \DateTime
     */
    public function getNextChanceDatetime(){
        $tests = $this->examPermission->getEmployeeExams();
        
        if (count($tests) >= $this->examPermission->getModuleExam()->getMaxAttempts()) {
            return false;
        }

        $nextChanceAllowed =  clone $this->getStartsAt();
        $nextChanceAllowed->modify("+ {$this->getExam()->getDaysForRetry()} days");

        return $nextChanceAllowed;
    }

    public function canBeAnswered()
    {
        return $this->canBeStarted();
    }

    public function isFinished()
    {
        return $this->finishedAt !== null;
    }

    public function getStartsAt()
    {
        return $this->startsAt;
    }

    /**
     * @return \DateTime
     */
    public function getFinishedAt()
    {
        return $this->finishedAt;
    }

    /**
     * @return bool
     */
    public function isApproved()
    {
        return $this->approved;
    }

    /**
     * @return bool
     */
    public function isEvaluated()
    {
        return (!is_null($this->approved));
    }

    /**
     * @param \DateTime $startsAt
     */
    public function setStartsAt($startsAt)
    {
        $this->startsAt = $startsAt;
    }

    /**
     * @param \DateTime $finishedAt
     */
    public function setFinishedAt($finishedAt)
    {
        $this->finishedAt = $finishedAt;
    }

    /**
     * @param bool $approved
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
    }

    /**
     * @return bool
     */
    public function isTimeOut()
    {
        return $this->startsAt < new \DateTime("- {$this->getExam()->getLimitTimeInSeconds()} secs");
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getCorrects()
    {
        if(empty($this->answers)){
            return new ArrayCollection();
        }
        
        return $this->answers->filter(function (EmployeeExamAnswer $answer) {
            return (is_null($answer->getAnswer()) ? false : $answer->getAnswer()->isCorrect());
        });
    }

    /**
     * @param EmployeeExamAnswer $testAnswer
     */
    public function replaceAnswer(EmployeeExamAnswer $testAnswer)
    {
        $oldTestAnswer = $this->getAnswerByQuestion($testAnswer->getQuestion());
       
        if ($oldTestAnswer) {
            $this->answers->removeElement($oldTestAnswer);
        }

        $this->answers->add($testAnswer);
    }

    public function __toString()
    {
        return sprintf('%s - Iniciado %s - Resultado (%s)', $this->getId(), $this->startsAt->format('d/m/Y H:i:s'),
            (($this->isFinished() and $this->isEvaluated()) ?
                        ( $this->isApproved() ?
                            'Aprobado' :
                            'Reprobado')
                    : 'Pendiente' ));
    }

    public function getTotalAnswers(){
        $total = 0;
 
        foreach ($this->answers as $answer){
            if (!empty($answer->getAnswer())){
                $total++;
            }
        }
 
        return $total;
    }
}