<?php

namespace Aper\PlanCarreraBundle\Listener;


use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class EasyAdminListener implements EventSubscriberInterface
{


    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            EasyAdminEvents::PRE_SEARCH => [
                ['adaptSearchQuery', 10],
            ],
            EasyAdminEvents::POST_SEARCH => [
                ['reAdaptSearchQuery', 10],
            ],
        ];
    }

    public function adaptSearchQuery(GenericEvent $event){

        $arguments = $event->getArguments();
        $request   = $arguments['request'];

        if($request->query->get('query')){

            $query = trim($request->query->get('query'));
            $query = str_replace(' ', '%', $query);

            $request->query->set('query', $query);
            $arguments['request'] = $request;
            $event->setArguments($arguments);
        }

    }

    public function reAdaptSearchQuery(GenericEvent $event){

        $arguments = $event->getArguments();
        $request   = $arguments['request'];

        if($request->query->get('query')){

            $query = trim($request->query->get('query'));
            $query = str_replace('%', ' ', $query);

            $request->query->set('query', $query);
            $arguments['request'] = $request;
            $event->setArguments($arguments);
        }

    }
}