<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;

use Aper\PlanCarreraBundle\Entity\EmployeeExamAnswer;
use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Entity\EmployeeModulePoll;
use Aper\PlanCarreraBundle\Entity\EmployeeModulePollAnswer;
use Aper\PlanCarreraBundle\Entity\ExternalTrainingInscription;
use Aper\PlanCarreraBundle\Entity\Module;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Dompdf\Dompdf;

class FrontController extends Controller
{

    //$moodleUrl = "https://pixelnet.net.ar/santi/auth/wdmwpmoodle/login.php"; // moodle de prueba
    private $virtualUrl = "https://www.campusvirtual-pumaenergy.com.ar/course/index.php?categoryid=10";
    // private $virtualUrl = "https://www.campusvirtual-pumaenergy.com.ar/course/index.php?categoryid=5";
    private $moodleUrl = "https://www.campusvirtual-pumaenergy.com.ar/auth/wdmwpmoodle/login.php";// moodle en producción
    //$userId = "71"; // usuario test en moodle de prueba
    private $userId = "2979"; // usuario test en sitio en produccion
    //$redirectUrl = "https://pixelnet.net.ar/santi";
    //$redirectUrl = "https://team.pumaenergyarg.com.ar/";
    private $redirectUrl = "https://www.campusvirtual-pumaenergy.com.ar";
    // header( "Location: $n_url" );
    private function encryptString($value, $key)
    {
        $key = $key;
        if (!$value) {
            return '';
        }
        $token = $value;
        $enc_method = 'AES-128-CTR';
        $enc_key = openssl_digest("edwiser-bridge", 'SHA256', true);
        $enc_iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($enc_method));
        $crypted_token = openssl_encrypt($token, $enc_method, $enc_key, 0, $enc_iv) . "::" . bin2hex($enc_iv);
        $newToken = $crypted_token;
        $newToken = $newToken;
        $data = base64_encode($crypted_token);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return trim($data);
    }



    /**
     * @Route("/career/static", name="frontend_aper_career_index_static")
     */
    public function indexStaticAction(){

        $employee = $this->getUser()->getEmployee();

        $progreso = [];
        if ($employee->getCareerPlans()) {
            foreach ($employee->getCareerPlans() as $careerPlan) {

                foreach ($careerPlan->getModules() as $moduleEmp){

                    
                    $module = $moduleEmp->getModule();
                                        
                        
                        if ($moduleEmp->isEnabled()) {

                            $progress = $this->getModuleProgress($moduleEmp);

                            $progreso[$module->getId()] = $progress;
                        }
                }
            }
        }


        return $this->render('PlanCarreraBundle:Front:index.html.twig',[
            'progreso' => $progreso,
        ]);
    }

    private function getModuleProgress($module) {

        // var_dump('-----------------------');
        // var_dump($module->getExamPermission()->getNextStep());
        // var_dump($module->getExamPermission()->isEnabled());
    
        // var_dump($module->getExamPermission()->getEmployeeExams()->getFinishedAt());
        // var_dump($module->getExamPermission()->getEmployeeExams()->getStartsAt());
        // exit;

        if (isset($_GET['lucas'])) {
            // var_dump($module->getId());
            // var_dump($module->getExamPermissionsPost()->getModuleExam());
            // var_dump($module->getExamPermissionsPost());
        }
    
        $progress = 0;
        if ($module->getExamPermission() == false && $module->getExamPermissionsPost() == false && $module->getPollPermission() == false) {
            $progress = 0;
        } else if ($module->getExamPermission() !== false && $module->getExamPermission()->isEnabled() !== false) {
    
            // if prev exam is completed = 25
            if ($module->getExamPermission()->getNextStep() === 'completed') {
                $progress = 25;
            }
    
            if ($module->getExamPermissionsPost() && $module->getExamPermissionsPost()->isEnabled() == true && $module->getExamPermissionsPost()->getNextStep() != 'none' ) {
                $progress = 75;
            }
    
            // check post exam
            if (
                $module->getExamPermissionsPost() !== false && 
                $module->getExamPermissionsPost()->getNextStep() === 'completed' && 
                $module->getExamPermissionsPost()->isEnabled() !== false) {
                $progress = 100;
            } else {
                if (
                    $module->getPollPermission() && 
                    $module->getPollPermission()->isEnabled() == true && 
                    $module->getPollPermission()->getNextStep() != 'none') {

                    if ($module->getModule()->getId() >= 9) {
                        if (
                            $module->getExamPermissionsPost() && $module->getExamPermissionsPost()->isEnabled() == true && 
                            ($module->getExamPermissionsPost()->getNextStep() === 'completed' || $module->getExamPermissionsPost()->getNextStep() === 'finish') 
                        ) {
                            $progress = 100;
                        }
                    } else {
                        if ($module->isApproved()) {
                            $progress = 100;
                        }
    
                    }
                }
            }
    
        } else if ($module->getExamPermission() !== false && $module->getExamPermission()->getNextStep() !== 'completed') {
            $progress = 0;
        }
    
        if ($module->isEnabled() && ($module->isApproved() || $progress == 100)) {
            $progress = 100;
        }
    
        return $progress;
    }
    /**
     * @Route("/module/static/{module}", name="frontend_aper_career_module_static")
     */
    public function myModuleAction(Module $module){

        // echo 'asdasd';exit;

        if (isset($_GET['log'])) {
            header('Location: https://www.campusvirtual-pumaenergy.com.ar/mod/scorm/view.php?id=' . $module->getIdPixelnet() . '&hideinterface=1');
            exit;
        }
        if (isset($_GET['logvirtual'])) {
            header('Location: ' . $this->virtualUrl);
            exit;
        }

        $user = $this->getUser();

        if ($module->getIdPixelnet() != 999) {
            $this->redirectUrl = "https://team.pumaenergyarg.com.ar/module/static/" . $module->getId() . "?log=1";
        } else {
            $this->redirectUrl = "https://team.pumaenergyarg.com.ar/module/static/" . $module->getId() . "?logvirtual=1";
        }

// echo "moodle_user_id=".$user->getEmployee()->getIdPixelnetUsr()."&login_redirect=".$this->redirectUrl;
        $n_url = $this->moodleUrl."?wdm_data=".$this->encryptString("moodle_user_id=".$user->getEmployee()->getIdPixelnetUsr()."&login_redirect=".$this->redirectUrl, "b50a4596d7a115031281d3db677a75bf");
        // $n_url = $this->moodleUrl."?wdm_data=".$this->encryptString("moodle_user_id=2979&login_redirect=".$this->redirectUrl, "b50a4596d7a115031281d3db677a75bf");
// echo $n_url;
        if ($module->getIdPixelnet() != 999) {
            $this->redirectUrl = "https://team.pumaenergyarg.com.ar/module/static/" . $module->getId() . "?log=1";
        } else {
            $this->redirectUrl = "https://team.pumaenergyarg.com.ar/module/static/" . $module->getId() . "?logvirtual=1";
            header('Location: ' . $n_url);
            exit;
        }

        $employee = $this->getUser()->getEmployee();

        $progreso = [];
        if ($employee->getCareerPlans()) {
            foreach ($employee->getCareerPlans() as $_careerPlan) {

                foreach ($_careerPlan->getModules() as $_moduleEmp){

                    
                    $_module = $_moduleEmp->getModule();
                                        
                        
                        if ($_moduleEmp->isEnabled()) {

                            $progress = $this->getModuleProgress($_moduleEmp);

                            $progreso[$_module->getId()] = $progress;
                        }
                }
            }
        }

        return $this->render('PlanCarreraBundle:Front:mymodule.html.twig',[
            'module_id' => $module->getId(), 'n_url' => $n_url, 'progreso' => $progreso,
        ]);
    }


    /**
     * @Route("/trainer/static", name="frontend_aper_career_trainer_static")
     */
    public function trainerStaticAction(){
        return $this->render('PlanCarreraBundle:Front:trainer.html.twig',[

        ]);
    }

    /**
     * @Route("/externals-trainings/static", name="frontend_aper_career_external_trainings_static")
     */
    public function externalTrainingsStaticAction(){
        return $this->render('PlanCarreraBundle:Front:external_trainings.html.twig',[

        ]);
    }

    /**
     * @Route("/certificado/{module}", name="module_certificado")
     * @Method("GET")
     */
    public function certificadoAction(Module $module)
    {        
        $dompdf = new Dompdf();
        $dompdf->loadHtml(
            $this->renderView('PlanCarreraBundle:pdf:course_certificate.html.twig', array(
                'modulo' => $module,
                'empleado' => $this->getUser()->getEmployee()
            ))
        );
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');
        // Render the HTML as PDF
        $dompdf->render();
        // Output the generated PDF to Browser
//	$dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
		ob_end_clean();
        $dompdf->stream('certificado-puma-energy.pdf',array('compress'=>true,'Attachment'=>true));
		exit();
	
	// $html = $this->renderView('PlanCarreraBundle:pdf:course_certificate.html.twig', array(
        //             'modulo' => $module,
        //             'empleado' => $this->getUser()->getEmployee()
        //         ));
        // return new Response(
        //     $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
        //     200,
        //     array(
        //         'Content-Type' => 'application/pdf',
        //         'Content-Disposition' => 'attachment; filename="fichero.pdf"'
        //     )
        // );
    }

}
