<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;

use Aper\PlanCarreraBundle\Entity\ModuleExamAnswer;
use Aper\PlanCarreraBundle\Exception\ExamException;
use Aper\UserBundle\Entity\Employee;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CareerPlanController
 * @Route("/ajax")
 */
class ExamAnswerController extends Controller
{
    /**
     * @Route("/career-plan/exam/{answer}/{prepost}")
     * @Method("POST")
     */
    public function __invoke(Request $request, ModuleExamAnswer $answer, $prepost)
    {
        //return JsonResponse::create([]);

        /** @var Employee $employee */
        $employee = $this->getUser()->getEmployee();

        try {
            $module = $answer->getQuestion()->getExam()->getModule();
            if (!$employee->isCareerPlanEnabled($module->getPlan())) {
                throw new \DomainException("El plan no esta habilitado para este empleado.");
            }

            $this->get('examiner')->answer($answer, $employee, $prepost);

        } catch (ExamException $e) {
            if($e->getType() == 'APPROVED'){
                return JsonResponse::create(['type' => 'timeout', 'message' => $e->getMessage()], JsonResponse::HTTP_OK);
            } else {
                return JsonResponse::create(['type' => 'error', 'message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $e) {
            return JsonResponse::create(['type' => 'error', 'message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }

        return JsonResponse::create([]);
    }

}