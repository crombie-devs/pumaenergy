<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;

use Aper\StoreBundle\Entity\Store;
use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CareerPlanController
 * @Route("/ajax")
 */
class StoreController extends Controller
{
    /**
     * @Route("/stores")
     * @Method("GET")
     */
    public function __invoke()
    {
        $stores = $this->getDoctrine()->getRepository(Store::class)->findAll();

        $serializer = $this->get('jms_serializer');
        $context = SerializationContext::create();
        $context->setGroups(['frontend']);

        return JsonResponse::create($serializer->toArray($stores, $context));
    }

}