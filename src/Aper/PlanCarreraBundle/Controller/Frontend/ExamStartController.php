<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;

use Aper\PlanCarreraBundle\CQRS\Command\ExamStartCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\ExamPermission;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\UserBundle\Entity\Employee;
use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CareerPlanController
 * @Route("/ajax")
 */
class ExamStartController extends Controller
{
    /**
     * @Route("/career-plan/exam/{module}/{prepost}/start")
     * @Method("POST")
     */
    public function __invoke(Request $request, Module $module, $prepost)
    {
        /** @var Employee $employee */
        $employee = $this->getUser()->getEmployee();
        $plan   = $module->getPlan();
        $exam   = $module->getExam();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            throw new \DomainException("El empleado no tiene el Plan Activado.");
        }

        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if(!$moduleEmployee){
            throw new \DomainException("El empleado no tiene el Módulo del Plan.");
        }

        $examPermission = $moduleEmployee->getExamPermissions()->filter(
            function (ExamPermission $employeeExamPermission) use ($exam, $prepost) {
                return $employeeExamPermission->getModuleExam() === $exam && $employeeExamPermission->getPrevioPost() == $prepost;
            })->first();

        if(!$examPermission){
            throw new \DomainException("El empleado no tiene el Examen Activado.");
        }
        $exam = $examPermission->getModuleExam();

        try {
            $module = $exam->getModule();
            if (!$employee->isCareerPlanEnabled($module->getPlan())) {
                throw new \DomainException("El plan no esta habilitado para este empleado.");
            }

            $test = $this->get('examiner')->start($exam, $employee, $prepost);

            $serializer = $this->get('jms_serializer');
            $context = SerializationContext::create();
            $context->setGroups(['employee']);

            return JsonResponse::create($serializer->toArray($test, $context));
        } catch (\Exception $e) {
            return JsonResponse::create(['message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

}