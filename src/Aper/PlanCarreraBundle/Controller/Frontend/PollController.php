<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;

use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Entity\ModulePollQuestion;
use Aper\PlanCarreraBundle\Model\AnswerDTO;
use Aper\UserBundle\Entity\Employee;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CareerPlanController
 * @Route("/ajax")
 */
class PollController extends Controller
{
    /**
     * @Route("/career-plan/module/{id}/question/{question}")
     * @Method("POST")
     */
    public function __invoke(Request $request, Module $module, ModulePollQuestion $question)
    {
        /** @var Employee $employee */
        $employee = $this->getUser()->getEmployee();
        $data = $request->get('answer');

        try {
            if (!$employee->isCareerPlanEnabled($module->getPlan())) {
                throw new \DomainException("El plan no esta habilitado para este empleado.");
            }

            if (!$module->getPoll()) {
                throw new \DomainException("El modulo no tiene encuesta.");
            }

            $answer = $this->getDoctrine()->getRepository('PlanCarreraBundle:ModulePollAnswer')->find($data['id']);
            $answerText = $answer ? null : $data['text'];

            //todo verificar valores incorrectos, validarlo
            $dto = new AnswerDTO($question, $answer, $answerText);

            $employeeAnswer = $this->get('pollster')->answer($module, $employee, $dto);

            $em = $this->getDoctrine()->getManager();
            $em->persist($employeeAnswer);
            $em->flush();
        } catch (\Exception $e) {
            return JsonResponse::create(['message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }

        return JsonResponse::create([]);
    }

}