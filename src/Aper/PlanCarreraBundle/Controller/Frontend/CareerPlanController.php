<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;

use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CareerPlanController
 * @Route("/ajax")
 */
class CareerPlanController extends Controller
{
    /**
     * @Route("/career-plan")
     * @Method("GET")
     */
    public function __invoke()
    {
//        ini_set('memory_limit','1G');
//        ini_set('max_execution_time', 600);
        $plans = $this->getDoctrine()->getRepository('PlanCarreraBundle:CareerPlan')->findAllWithOrder();
        $serializer = $this->get('jms_serializer');

        $context = SerializationContext::create();
        $context->setGroups(['employee']);

        return JsonResponse::create($serializer->toArray($plans, $context));
    }

}