<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;

use Aper\PlanCarreraBundle\CQRS\Command\ActivateModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\SubscribeExternalTrainingCommand;
use Aper\PlanCarreraBundle\Entity\ExternalTraining;
use Aper\UserBundle\Entity\Employee;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CareerPlanController
 * @Route("/ajax")
 */
class SubscribeToTrainingController extends Controller
{
    /**
     * @Route("/career-plan/external-training/subscribe/{id}")
     * @Method("GET")
     * @param ExternalTraining $training
     * @return JsonResponse
     */
    public function __invoke(ExternalTraining $training)
    {
        /** @var Employee $employee */
        $employee = $this->getUser()->getEmployee();

        $command = new SubscribeExternalTrainingCommand($employee, $training);
        $handler = $this->get('subscribe_external_training');

        try {
            $handler($command);
        } catch (\Exception $e) {
            return JsonResponse::create(['message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }

        if($training->getModule()->isPresencialExam()){
            $command = new ActivateModuleExamCommand($employee, $training->getModule()->getExam());
            $handler = $this->get('activate_exam');
            try {
                $handler($command);
            } catch (\Exception $e){

            }
        }

        return JsonResponse::create([]);
    }

}