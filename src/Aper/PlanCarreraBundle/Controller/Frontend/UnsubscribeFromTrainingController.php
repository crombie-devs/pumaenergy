<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;

use Aper\PlanCarreraBundle\CQRS\Command\UnsubscribeExternalTrainingCommand;
use Aper\PlanCarreraBundle\Entity\ExternalTraining;
use Aper\UserBundle\Entity\Employee;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CareerPlanController
 * @Route("/ajax")
 */
class UnsubscribeFromTrainingController extends Controller
{
    /**
     * @Route("/career-plan/external-training/unsubscribe/{id}")
     * @Method("GET")
     * @param ExternalTraining $training
     * @return JsonResponse
     */
    public function __invoke(ExternalTraining $training)
    {
        /** @var Employee $employee */
        $employee = $this->getUser()->getEmployee();

        $command = new UnsubscribeExternalTrainingCommand($employee, $training);
        $handler = $this->get('unsubscribe_external_training');

        try {
            $handler($command);
        } catch (\Exception $e) {
            return JsonResponse::create(['message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }

        return JsonResponse::create([]);
    }

}