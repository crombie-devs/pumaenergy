<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;

use Aper\PlanCarreraBundle\CQRS\Command\ActivateModuleCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ActivateModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ActivateModulePollCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ActivatePlanRequirementCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ApproveModuleCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ApproveModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ApprovePlanCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ApprovePlanRequirementCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DeactivateModuleCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DeactivateModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DeactivateModulePollCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DeactivatePlanRequirementCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapproveModuleCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapproveModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapprovePlanCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapprovePlanRequirementCommand;
use Aper\PlanCarreraBundle\CQRS\Command\RegisterToProgramCommand;
use Aper\PlanCarreraBundle\CQRS\Command\CommentReceivedCommand;
use Aper\PlanCarreraBundle\Entity\CareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Entity\ExternalTraining;
use Aper\PlanCarreraBundle\Entity\ExternalTrainingInscription;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Entity\ModulePoll;
use Aper\PlanCarreraBundle\Entity\PlanRequirement;
use Aper\UserBundle\Entity\Employee;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class TrainerController
 * @Route("/ajax")
 */
class TrainerController extends Controller
{
    /**
     * @Route("/trainer/employees")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function getEmployees(Request $request)
    {
        $trainer = $this->getUser();
        $data    = $request->request->all();
        $isStoreManager = in_array('ROLE_STORE_MANAGER', $trainer->getRoles());
        $search  = [];
        $search['dni']    = (isset($data['dni'])      and strlen(trim($data['dni'])))      ? trim($data['dni'])      : '';
        $search['name']   = (isset($data['name'])     and strlen(trim($data['name'])))     ? trim($data['name'])     : '';
        $search['pdc']    = (isset($data['plan'])     and strlen(trim($data['plan'])))     ? trim($data['plan'])     : '';
        $search['store']  = (isset($data['estacion']) and strlen(trim($data['estacion']))) ? trim($data['estacion']) : '';
        $search['page']   = (isset($data['page'])     and strlen(trim($data['page'])))     ? trim($data['page'])     : 1;

        $limit  = 10;
        $offset = $limit * ($search['page'] - 1);
        $users  = [];

        if(in_array('ROLE_MANAGER_GLOBAL', $trainer->getRoles()) or in_array('ROLE_TRAINER_ADMIN', $trainer->getRoles())){
            $users = $this->getDoctrine()->getRepository('UserBundle:Employee')->findAllEmployeesForAdmin($search/*, $limit, $offset*/);
            $total = $this->getDoctrine()->getRepository('UserBundle:Employee')->countAllEmployeesForAdmin($search);
        }
        elseif($isStoreManager){
            $store  = $trainer->getEmployee()->getStore();
            $users   = $this->getDoctrine()->getRepository('UserBundle:Employee')->findAllEmployeesTrainedByStore($store, $search/*, $limit, $offset*/);
            $total   = $this->getDoctrine()->getRepository('UserBundle:Employee')->countAllEmployeesTrainedByStore($store, $search);
        }
        elseif(in_array('ROLE_TRAINER', $trainer->getRoles())){
            $trainer = $trainer->getEmployee();
            $users   = $this->getDoctrine()->getRepository('UserBundle:Employee')->findAllEmployeesTrainedBy($trainer, $search);
            $total   = $this->getDoctrine()->getRepository('UserBundle:Employee')->countAllEmployeesTrainedBy($trainer, $search);
        }
        $i = 0;
        $t = new ArrayCollection();
        foreach ($users as $user)
        {
            if($isStoreManager && $user->getId() == $trainer->getEmployee()->getId())
                continue;

            if($i >= $offset)
            {
                $t->add($user);
            }
            $i++;
            if($i >= ($limit+$offset))
                break;


        }
        $users = $t;

        $plans      = $this->getDoctrine()->getRepository('PlanCarreraBundle:CareerPlan')->findAllWithOrder();
        $serializer = $this->get('jms_serializer');
        $context    = SerializationContext::create();
        $context2   = SerializationContext::create();
        $context->setGroups(['trainer']);
        $context2->setGroups(['trainer_2']);

        $basePlans = $serializer->toArray($plans, $context);
        foreach ($basePlans as &$base){
            $base['enabled'] = false;
            $base['approved'] = false;
        }
        $employees = $serializer->toArray($users, $context2);

        // Debo hacer el merge de todos los planes de carreras con todos los que tiene asignado el empleado ya.
        foreach ($employees as &$employee){
            if(!$employee['employeeCurrentPlan'] or !$employee['employeeCurrentPlan']['plan'])
                continue;

            /** Hack para suplir error de DOCTRINE que no levanta las colecciones correctamente de los inscriptos para un ExternalTraining */
            foreach ($employee['employeeCurrentPlan']['plan']['modules'] as $module)
            {
                foreach ($module['external_trainings'] as $external_training)
                {
                    /** @var Employee $emp */
                    $emp = $this->getDoctrine()->getRepository(Employee::class)->find($employee['id']);
                    /** @var ExternalTraining $ext */
                    $ext = $this->getDoctrine()->getRepository(ExternalTraining::class)->find($external_training['id']);
                    $inscriptions = $this->getDoctrine()->getRepository(ExternalTrainingInscription::class)->findAllByEmployeeAndTraining($emp,$ext);

                    if($inscriptions)
                    {
                        $inscriptions2 = $serializer->toArray($inscriptions, SerializationContext::create()->setGroups(['trainer_2']));
                        $ext4 = $serializer->toArray($ext, SerializationContext::create()->setGroups(['trainer_2']));
                        $ext4['inscriptions'] = $inscriptions2;
                        foreach ($employee['employeeCurrentPlan']['modules'] as &$mod)
                        {
                            if($mod['module']['id'] == $module['id'])
                            {
                                $mod['external_trainings'] = [$ext4];
                            }
                        }
                    }
                }
            }


            foreach ($basePlans as $basePlan){
                if(!isset($employee['employeeCareerPlans'][$basePlan['id']])){
                    $employee['employeeCareerPlans'][$basePlan['id']] = $basePlan;
                } else {
//                    if($basePlan['id'] != $employee['employeeCurrentPlan']['plan']['id']){
                        $approved = $employee['employeeCareerPlans'][$basePlan['id']]['approved'];
                        $enabled  = $employee['employeeCareerPlans'][$basePlan['id']]['enabled'];
                        $basePlan['modules']      = $employee['employeeCareerPlans'][$basePlan['id']]['modules'];
                        $basePlan['requirements'] = $employee['employeeCareerPlans'][$basePlan['id']]['requirements'];
                        $employee['employeeCareerPlans'][$basePlan['id']] = $basePlan;
                        $employee['employeeCareerPlans'][$basePlan['id']]['enabled']  = $enabled;
                        $employee['employeeCareerPlans'][$basePlan['id']]['approved'] = $approved;
//                    }
                }
            }
            asort($employee['employeeCareerPlans']);
//            $employee['employeeCareerPlans'] = array_values($employee['employeeCareerPlans']);
        }
        return JsonResponse::create(compact('employees', 'total'));
    }


    /**
     * @Route("/trainer/employee/{employee}/plan/{careerPlan}")
     * @Method("POST")
     * @param Employee $employee
     * @param CareerPlan $careerPlan
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function getEmployeeCareerPlan(Employee $employee, CareerPlan $careerPlan)
    {
        $trainer = $this->getUser();

        // Solo pasan 'ROLE_TRAINER', 'ROLE_TRAINER_ADMIN', 'ROLE_MANAGER_GLOBAL'
        if(     (in_array('ROLE_TRAINER', $trainer->getRoles()))
            and (!in_array('ROLE_MANAGER_GLOBAL', $trainer->getRoles()))
            and (!in_array('ROLE_STORE_MANAGER', $trainer->getRoles()))
        ){
            $trainer = $trainer->getEmployee();
            if($employee->getTrainer() != $trainer){
                return JsonResponse::create(['message' => 'No tiene permisos para visualizar este plan'], JsonResponse::HTTP_BAD_REQUEST);
            }
        }

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($careerPlan){
            return ($employeeCareerPlan->getPlan() === $careerPlan);
        })->first();

        $serializer = $this->get('jms_serializer');
        $context    = SerializationContext::create();
        $context->setGroups(['trainer_2']);
        $context2   = SerializationContext::create();
        $context2->setGroups(['trainer_2']);
        $emp = $serializer->toArray($employee, $context2);
        $employee = $emp;

        if(!$employeeCareerPlan){
            $r = ['plan'         => $careerPlan,
                  'modules'      => $careerPlan->getModules(),
                  'requirements' => $careerPlan->getRequirements(),
                  'enabled'      => false,
                  'approved'     => false];
            return JsonResponse::create($serializer->toArray($r, $context));
        }

        $ecp = $serializer->toArray($employeeCareerPlan, $context);

        /** Hack para suplir error de DOCTRINE que no levanta las colecciones correctamente de los inscriptos para un ExternalTraining */
        foreach ($ecp['plan']['modules'] as &$module)
        {
            foreach ($module['external_trainings'] as &$external_training)
            {
                /** @var Employee $emp */
                $emp = $this->getDoctrine()->getRepository(Employee::class)->find($employee['id']);
                /** @var ExternalTraining $ext */
                $ext = $this->getDoctrine()->getRepository(ExternalTraining::class)->find($external_training['id']);
                $inscriptions = $this->getDoctrine()->getRepository(ExternalTrainingInscription::class)->findAllByEmployeeAndTraining($emp,$ext);

                if($inscriptions)
                {
                    $inscriptions2 = $serializer->toArray($inscriptions, SerializationContext::create()->setGroups(['trainer_2']));
                    $ext4 = $serializer->toArray($ext, SerializationContext::create()->setGroups(['trainer_2']));
                    $ext4['inscriptions'] = $inscriptions2;
                    foreach ($employee['employeeCurrentPlan']['modules'] as &$mod)
                    {
                        if($mod['module']['id'] == $module['id'])
                        {
                            $mod['external_trainings'] = [$ext4];
                        }
                    }
                }
            }
        }

        return JsonResponse::create($ecp);
    }

    /**
     * @Route("/trainer/employee/{employee}/module/{module}/comment")
     * @Method("POST")
     * @param Request $request
     * @param Employee $employee
     * @param Module $module
     * @return JsonResponse
     */
    public function addModuleComment(Request $request, Employee $employee, Module $module)
    {
        $data     = $request->request->all();
        $comments = (isset($data['comments']) and strlen(trim($data['comments']))) ? trim($data['comments']) : '';

        try {
            $command = new CommentReceivedCommand($employee, $module, $comments, $this->getUser()->getEmployee());
            $handler = $this->get('comment_received');
            $handler($command);

        } catch (Exception $e) {
            return JsonResponse::create(['message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }

        return JsonResponse::create([]);
    }

    /**
     * @Route("/trainer/action")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function action(Request $request)
    {
        $data        = $request->request->all();
        $entity      = (isset($data['entity'])      and strlen(trim($data['entity'])))      ? trim($data['entity'])      : '';
        $entity_id   = (isset($data['entity_id'])   and strlen(trim($data['entity_id'])))   ? trim($data['entity_id'])   : '';
        $employee_id = (isset($data['employee_id']) and strlen(trim($data['employee_id']))) ? trim($data['employee_id']) : '';
        $action      = (isset($data['action'])      and strlen(trim($data['action'])))      ? trim($data['action'])      : '';

        if(   !strlen($entity)
           or !strlen($entity_id)
           or !strlen($employee_id)
           or !strlen($action)
        ){
            return JsonResponse::create(['message' => 'Todos los parámetros son obligatorios'], JsonResponse::HTTP_BAD_REQUEST);
        }
        /** @var Employee $employee */
        $employee = $this->getDoctrine()->getRepository(Employee::class)->findOneBy(['id' => $employee_id]);

        switch ($entity){
            case 'plan':
                {
                    /** @var CareerPlan $plan */
                    $plan = $this->getDoctrine()->getRepository(CareerPlan::class)->findOneBy(['id' => $entity_id]);
                    switch ($action){
                        case 'activate':
                            $command = new RegisterToProgramCommand($employee->getId(), $plan->getId());
                            $handler = $this->get('register_to_program');
                            break;
                        case 'deactivate':
                            return JsonResponse::create(['message' => 'Acción no permitida'], JsonResponse::HTTP_BAD_REQUEST);
                            break;
                        case 'approve':
                            $command = new ApprovePlanCommand($employee, $plan);
                            $handler = $this->get('approve_plan');
                            break;
                        case 'disapprove':
                            $command = new DisapprovePlanCommand($employee, $plan);
                            $handler = $this->get('disapprove_plan');
                            break;
                        default:
                            return JsonResponse::create([], JsonResponse::HTTP_BAD_REQUEST);
                    }
                }
                break;
            case 'module':
                {
                    /** @var Module $module */
                    $module = $this->getDoctrine()->getRepository(Module::class)->findOneBy(['id' => $entity_id]);
                    $plan   = $module->getPlan();
                    switch ($action){
                        case 'activate':
                            $command = new ActivateModuleCommand($employee, $module);
                            $handler = $this->get('activate_module');
                            break;
                        case 'deactivate':
                            $command = new DeactivateModuleCommand($employee, $module);
                            $handler = $this->get('deactivate_module');
                            break;
                        case 'approve':
                            $command = new ApproveModuleCommand($employee, $module);
                            $handler = $this->get('approve_module');
                            break;
                        case 'disapprove':
                            $command = new DisapproveModuleCommand($employee, $module);
                            $handler = $this->get('disapprove_module');
                            break;
                        default:
                            return JsonResponse::create([], JsonResponse::HTTP_BAD_REQUEST);
                    }
                }
                break;
            case 'requirement':
                {
                    /** @var PlanRequirement $requirement */
                    $requirement = $this->getDoctrine()->getRepository(PlanRequirement::class)->findOneBy(['id' => $entity_id]);
                    $plan        = $requirement->getPlan();
                    switch ($action){
                        case 'activate':
                            $command = new ActivatePlanRequirementCommand($employee, $requirement);
                            $handler = $this->get('activate_requirement');
                            break;
                        case 'deactivate':
                            $command = new DeactivatePlanRequirementCommand($employee, $requirement);
                            $handler = $this->get('deactivate_requirement');
                            break;
                        case 'approve':
                            $command = new ApprovePlanRequirementCommand($employee, $requirement);
                            $handler = $this->get('approve_requirement');
                            break;
                        case 'disapprove':
                            $command = new DisapprovePlanRequirementCommand($employee, $requirement);
                            $handler = $this->get('disapprove_requirement');
                            break;
                        default:
                            return JsonResponse::create([], JsonResponse::HTTP_BAD_REQUEST);
                    }
                }
                break;
            case 'exam':
                {
                    /** @var ModuleExam $moduleExam */
                    $moduleExam = $this->getDoctrine()->getRepository(ModuleExam::class)->findOneBy(['id' => $entity_id]);
                    $employeeModuleExam = $this->getDoctrine()->getRepository(EmployeeModuleExam::class)->findLastByExam($moduleExam, $employee);
                    $plan       = $moduleExam->getModule()->getPlan();
                    switch ($action){
                        case 'activate':
                            $command = new ActivateModuleExamCommand($employee, $moduleExam);
                            $handler = $this->get('activate_exam');
                            break;
                        case 'deactivate':
                            $command = new DeactivateModuleExamCommand($employee, $moduleExam);
                            $handler = $this->get('deactivate_exam');
                            break;
                        case 'approve':
                            if(!$employeeModuleExam){
                                return JsonResponse::create(['message' => 'El empleado no ha realizado el examen aún'], JsonResponse::HTTP_BAD_REQUEST);
                            }
                            $command = new ApproveModuleExamCommand($employee, $moduleExam, $employeeModuleExam);
                            $handler = $this->get('approve_exam');
                            break;
                        case 'disapprove':
                            if(!$employeeModuleExam){
                                return JsonResponse::create(['message' => 'El empleado no ha realizado el examen aún'], JsonResponse::HTTP_BAD_REQUEST);
                            }
                            $command = new DisapproveModuleExamCommand($employee, $moduleExam, $employeeModuleExam);
                            $handler = $this->get('disapprove_exam');
                            break;
                        default:
                            return JsonResponse::create([], JsonResponse::HTTP_BAD_REQUEST);
                    }
                }
                break;
            case 'poll':
                {
                    /** @var ModulePoll $modulePoll */
                    $modulePoll = $this->getDoctrine()->getRepository(ModulePoll::class)->findOneBy(['id' => $entity_id]);
                    $plan       = $modulePoll->getModule()->getPlan();
                    switch ($action){
                        case 'activate':
                            $command = new ActivateModulePollCommand($employee, $modulePoll);
                            $handler = $this->get('activate_poll');
                            break;
                        case 'deactivate':
                            $command = new DeactivateModulePollCommand($employee, $modulePoll);
                            $handler = $this->get('deactivate_poll');
                            break;
                        case 'approve':
                            return JsonResponse::create(['message' => 'Acción no permitida'], JsonResponse::HTTP_BAD_REQUEST);
                            break;
                        case 'disapprove':
                            return JsonResponse::create(['message' => 'Acción no permitida'], JsonResponse::HTTP_BAD_REQUEST);
                            break;
                        default:
                            return JsonResponse::create([], JsonResponse::HTTP_BAD_REQUEST);
                    }
                }
                break;
            default:
                return JsonResponse::create([], JsonResponse::HTTP_BAD_REQUEST);
        }
        try {
            $handler($command);

            $employeeCareerPlan = $this->getDoctrine()->getManager()->getRepository(EmployeeCareerPlan::class)->findByUserAndPlan($employee,$plan);
            if(!$employeeCareerPlan){

                return $this->getEmployeeCareerPlan($employee,  $plan);
            }
            $serializer = $this->get('jms_serializer');
            $context    = SerializationContext::create();
            $context->setGroups(['trainer_2']);

            return JsonResponse::create($serializer->toArray($employeeCareerPlan, $context));
        } catch (Exception $e) {
            return JsonResponse::create(['message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}