<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\UserBundle\Entity\Employee;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * Class ModuleController
 * @Route("/ajax")
 */
class ModuleApproveController extends Controller
{

    /**
     * @Route("/career-plan/employee/{employee}/module/{module}/approve")
     * @Method("POST")
     * @deprecated
     */
    public function __invoke(Request $request, Employee $employee, Module $module)
    {
        $plan     = $module->getPlan();
//        $comments = $request->get('comments') ? $request->get('comments') : '';

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            throw new \DomainException("El empleado no tiene el Plan Activado.");
        }

        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if(!$moduleEmployee){
            throw new \DomainException("El empleado no tiene el Módulo del Plan.");
        }

        try {
            if (!$employee->isCareerPlanEnabled($module->getPlan())) {
                throw new \DomainException("El plan no esta habilitado para este empleado.");
            }

            if($moduleEmployee->isApproved()){
                throw new \DomainException("El Módulo ha sido aprobado anteriormente para este empleado.");
            }
            $moduleEmployee->setApproved(true);
//            $moduleEmployee->setComment(substr(trim($comments),0,255));
//            $commentAuthor = $this->getUser()->getEmployee();
//            $moduleEmployee->setCommentAuthor($commentAuthor);

            $em = $this->getDoctrine()->getManager();
            $em->persist($moduleEmployee);
            $em->flush();

            return JsonResponse::create([]);

        } catch (\Exception $e) {
            return JsonResponse::create(['message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

}