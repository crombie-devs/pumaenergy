<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;

use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Entity\ExamPermission;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Exception\ExamException;
use Aper\UserBundle\Entity\Employee;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CareerPlanController
 * @Route("/ajax")
 */
class ExamFinishController extends Controller
{
    /**
     * @Route("/career-plan/exam/{module}/{prepost}/finish")
     * @Method("POST")
     * @param Request $request
     * @param Module $module
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(Request $request, Module $module, $prepost)
    {
        /** @var Employee $employee */
        $employee = $this->getUser()->getEmployee();
        $plan   = $module->getPlan();
        $exam   = $module->getExam();

        $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
            return $employeeCareerPlan->getPlan() === $plan;
        })->first();

        if (!$employeeCareerPlan) {
            throw new \DomainException("El empleado no tiene el Plan Activado.");
        }

        /** @var EmployeeCareerModule $moduleEmployee */
        $moduleEmployee = $employeeCareerPlan->getModules()->filter(
            function (EmployeeCareerModule $moduleEmployee) use ($module) {
                return $moduleEmployee->getModule() === $module;
            })->first();

        if(!$moduleEmployee){
            throw new \DomainException("El empleado no tiene el Módulo del Plan.");
        }

        /** @var ExamPermission $examPermission */
        $examPermission = $moduleEmployee->getExamPermissions()->filter(
            function (ExamPermission $employeeExamPermission) use ($exam, $prepost) {
                return $employeeExamPermission->getModuleExam() === $exam && $employeeExamPermission->getPrevioPost() == $prepost;
            })->first();

        if(!$examPermission){
            throw new \DomainException("El empleado no tiene el Examen Activado.");
        }
        $exam = $examPermission->getModuleExam();

        try {
            $module = $exam->getModule();
            if (!$employee->isCareerPlanEnabled($module->getPlan())) {
                throw new \DomainException("El plan no esta habilitado para este empleado.");
            }

            $this->get('examiner')->finish($exam, $employee, $prepost);

        } catch (ExamException $e) {
            /** @var EmployeeModuleExam $test */
            $test = $this->getDoctrine()->getRepository('PlanCarreraBundle:EmployeeModuleExam')->findLastByExam($exam, $employee);

            return JsonResponse::create([
                'type'           => 'timeout',
                'message'        => $e->getMessage(),
                'approved'       => $test->isApproved(),
                'corrects'       => $test->getCorrects()->count(),
                'total'          => $test->getAnswers()->count(),
                'days_for_retry' => $test->getExam()->getDaysForRetry(),
            ], JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return JsonResponse::create(['message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }

        /** @var EmployeeModuleExam $test */
        $test = $this->getDoctrine()->getRepository('PlanCarreraBundle:EmployeeModuleExam')->findLastByExam($exam, $employee);

        return JsonResponse::create([
            'approved'       => $test->isApproved(),
            'corrects'       => $test->getCorrects()->count(),
            'total'          => $test->getAnswers()->count(),
            'days_for_retry' => $test->getExam()->getDaysForRetry(),
        ]);
    }

}