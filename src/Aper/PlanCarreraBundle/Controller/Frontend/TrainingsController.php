<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;

use Aper\PlanCarreraBundle\CQRS\Command\ActivateModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ApproveExternalTrainingExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapproveExternalTrainingExamCommand;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\ExamPermission;
use Aper\PlanCarreraBundle\Entity\ExternalTraining;
use Aper\PlanCarreraBundle\Entity\ExternalTrainingInscription;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\UserBundle\Entity\Employee;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TrainingsController
 * @Route("/ajax")
 */
class TrainingsController extends Controller
{
    /**
     * @Route("/trainings/{id}")
     * @Method("GET")
     * @param Module $module
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function trainings(Module $module)
    {

        /** @var ExternalTraining $externalTraining */
        $externalTrainings = $module->getExternalTrainings();

        $iterator = $externalTrainings->getIterator();

        $iterator->uasort(function (ExternalTraining $first, ExternalTraining $second) {
            return $first->getDate() > $second->getDate() ? 1 : -1;
        });

        /** @var ExternalTrainingInscription[]|ArrayCollection $myInscriptions */
        $myInscriptions = new ArrayCollection($this->getDoctrine()->getRepository('PlanCarreraBundle:ExternalTrainingInscription')->findUserInscriptions($this->getUser()->getEmployee()));

        /** @var ExternalTrainingInscription[]|ArrayCollection $myModuleInscription */
        $myModuleInscription = $myInscriptions->filter(function(ExternalTrainingInscription $inscription) use ($module) {
            return ($inscription->getTraining()->getModule() === $module);
        });
        $now                  = new \DateTime();
        /** @var ExternalTrainingInscription[]|ArrayCollection $myModuleInscriptions */
        $myModuleInscriptions = [];
        $trainings            = [];
        $trainingApproved     = !$module->isPresencialExam();

        foreach ($myModuleInscription as $inscription){
            $myModuleInscriptions[$inscription->getTraining()->getId()] = $inscription;

            if((!$trainingApproved) and ($inscription->getExternalTrainingApproved())){
                $trainingApproved = $inscription->getExternalTrainingApproved();
            }
        }

        foreach ($iterator as $externalTraining) {

            if(isset($myModuleInscriptions[$externalTraining->getId()])) {
                $myModuleInscription = $myModuleInscriptions[$externalTraining->getId()];
                $external = [
                    "id"            => $myModuleInscription->getTraining()->getId(),
                    "name"          => $myModuleInscription->getTraining()->getName(),
                    "place"         => $myModuleInscription->getTraining()->getPlace(),
                    "date"          => $myModuleInscription->getTraining()->getDate()->format(\DATE_ISO8601),
                    "date_date"     => $myModuleInscription->getTraining()->getDate()->format('d/m/Y'),
                    "date_time"     => $myModuleInscription->getTraining()->getDate()->format('H:i'),
                    "duration"      => $myModuleInscription->getTraining()->getDuration(),
                    "quota"         => $myModuleInscription->getTraining()->getQuota(),
                    "subscribers"   => $myModuleInscription->getTraining()->getInscriptions()->count(),
                    "inscribed"     => (boolean) $myModuleInscription->getTraining()->getInscriptionForEmployee($this->getUser()->getEmployee()),
                    "assistance"    => is_null($myModuleInscription->getAssistance()) ? 'null' : $myModuleInscription->getAssistance(),
                    "approved"      => is_null($myModuleInscription->isApproved()) ? 'null' : $myModuleInscription->isApproved(),
                    "due"           => (boolean) ($myModuleInscription->getTraining()->getDate() < $now),
                    "evaluable"     => (boolean) ($myModuleInscription->getTraining()->getEvaluable()),
                    "urlMultimedia" => $myModuleInscription->getTraining()->getUrlMultimedia() ? $myModuleInscription->getTraining()->getUrlMultimedia() : 'null'
                ];
                $trainings[] = $external;
            } else {

//                if($trainingApproved){
//                    continue;
//                }
                if($module->isPresencialExam()){

                    $fakeExam = $module->getExam();
                    $employeeCareerModuleRepository = $this->getDoctrine()->getManager()->getRepository(EmployeeCareerModule::class);
                    $moduleEmployee = $employeeCareerModuleRepository->findByEmployeeAndModule($this->getUser()->getEmployee(), $module);
                    $examPermission = $this->getDoctrine()->getManager()->getRepository(ExamPermission::class)->findByEmployeeModuleAndExam($moduleEmployee, $fakeExam);

                    if($examPermission)
                    {
                        if($fakeExam->getMaxAttempts() <= $examPermission->getEmployeeExams()->count()){
                            continue;
                        }
                    }
                }


                if ($externalTraining->getDate() < new \DateTime()) {
                    continue;
                }

                $employeeStore = $this->getUser()->getEmployee()->getStore();
                if ($externalTraining->getStores()->isEmpty() || $externalTraining->getStores()->contains($employeeStore)) {
                    $external = [
                        "id"            => $externalTraining->getId(),
                        "name"          => $externalTraining->getName(),
                        "place"         => $externalTraining->getPlace(),
                        "date"          => $externalTraining->getDate()->format(\DATE_ISO8601),
                        "date_date"     => $externalTraining->getDate()->format('d/m/Y'),
                        "date_time"     => $externalTraining->getDate()->format('H:i'),
                        "duration"      => $externalTraining->getDuration(),
                        "quota"         => $externalTraining->getQuota(),
                        "subscribers"   => $externalTraining->getInscriptions()->count(),
                        "inscribed"     => (boolean) $externalTraining->getInscriptionForEmployee($this->getUser()->getEmployee()),
                        "assistance"    => 'null',
                        "approved"      => 'null',
                        "due"           => (boolean) ($externalTraining->getDate() < $now),
                        "evaluable"     => (boolean) ($externalTraining->getEvaluable()),
                        "urlMultimedia" => $externalTraining->getUrlMultimedia() ? $externalTraining->getUrlMultimedia() : 'null'
                    ];
                    $trainings[] = $external;
                }
            }
        }

        $serializer = $this->get('jms_serializer');

        return JsonResponse::create($serializer->toArray($trainings));
    }


    /**
     * @Route("/my-expositions/")
     * @param Request $request
     * @return JsonResponse
     */
    public function myTrainings(Request $request)
    {

        $data     = $request->request->all();
        $criteria = Criteria::create()->orderBy(["date" => Criteria::ASC]);
        $expr     = Criteria::expr();

        if(isset($data['name']) and strlen($data['name'])){
            $criteria->andWhere(
                $expr->contains('name', $data['name'])
            );
        }

        if(isset($data['date_desde']) and strlen($data['date_desde'])){
            $criteria->andWhere(
                $expr->gte('date', \DateTime::createFromFormat('d/m/Y', $data['date_desde'])->setTime(0,0,0))
            );
        }

        if(isset($data['date_hasta']) and strlen($data['date_hasta'])){
            $criteria->andWhere(
                $expr->lte('date', \DateTime::createFromFormat('d/m/Y', $data['date_hasta'])->setTime(23,59,59))
            );
        }

        if(in_array('ROLE_TRAINER_ADMIN', $this->getUser()->getRoles()) or
           in_array('ROLE_MANAGER_GLOBAL', $this->getUser()->getRoles()) or
           in_array('ROLE_STORE_MANAGER', $this->getUser()->getRoles())){
            $repoExternalTrainings = $this->getDoctrine()->getRepository(ExternalTraining::class);
            /** @var ExternalTraining[]|ArrayCollection $expositions */
            $expositions = new ArrayCollection($repoExternalTrainings->findAllTrainingFromCriteria($criteria));
        } else {
            /** @var ExternalTraining[]|ArrayCollection $expositions */
            $expositions = $this->getUser()->getExpositions()->matching($criteria);
        }

        $externalTrainings = [];
        $externalTrainings['active']   = $expositions->filter(function(ExternalTraining $externalTraining){return !$externalTraining->isArchived();})->getValues();
        
        $iterator = $expositions->getIterator();

        $iterator->uasort(function (ExternalTraining $first, ExternalTraining $second) {
            return $first->getDate() < $second->getDate() ? 1 : -1;
        });

        $expositions = new ArrayCollection(iterator_to_array($iterator));

        $externalTrainings['archived'] = $expositions->filter(function(ExternalTraining $externalTraining){return $externalTraining->isArchived();})->getValues();

        $serializer = $this->get('jms_serializer');

        return JsonResponse::create($serializer->toArray($externalTrainings,SerializationContext::create()->setGroups(['expositor'])));
    }

    /**
     * @Route("/externaltraining/{externalTraining}/archive")
     * @Method("POST")
     * @param Request $request
     * @param ExternalTraining $externalTraining
     * @return JsonResponse
     */
    public function archive(Request $request, ExternalTraining $externalTraining){

        $data     = $request->request->all();
        $archived = isset($data['archived']) ? $data['archived'] : true;

        // if($this->getUser() !== $externalTraining->getTrainer()){
        if(!(in_array('ROLE_TRAINER_ADMIN', $this->getUser()->getRoles()) or
             in_array('ROLE_MANAGER_GLOBAL', $this->getUser()->getRoles())) or
             in_array('ROLE_STORE_MANAGER', $this->getUser()->getRoles())){
            return JsonResponse::create(["message" => "No tienes permisos para editar el Entrenamiento Externo"], JsonResponse::HTTP_BAD_REQUEST);
        }
        $externalTraining->setArchived($archived);
        $em = $this->getDoctrine()->getManager();
        $em->persist($externalTraining);
        $em->flush();

        return JsonResponse::create([]);
    }


    /**
     * @Route("/externaltraining/{externalTraining}/assistance/{employee}")
     * @Method("POST")
     * @param Request $request
     * @param ExternalTraining $externalTraining
     * @param Employee $employee
     * @return JsonResponse
     * @throws \Exception
     */
    public function assistance(Request $request, ExternalTraining $externalTraining, Employee $employee){

        $data       = $request->request->all();
        $assistance = isset($data['assistance']) ? $data['assistance'] : true;

        if ($externalTraining->getDate() > new \DateTime()) {
            return JsonResponse::create(["message" => "No puede editar este campo porque el Taller no se dictó aún"], JsonResponse::HTTP_BAD_REQUEST);
        }

        if(($assistance === 'true') or ($assistance === true) or ($assistance === 1) or ($assistance === "1")){
            $assistance = true;
        }
        if(($assistance === 'false') or ($assistance === false) or ($assistance === 0) or ($assistance === "0")){
            $assistance = false;
        }

        if(!is_bool($assistance)){
            return JsonResponse::create(["message" => "Bad formed Data"], JsonResponse::HTTP_BAD_REQUEST);
        }

        // if($this->getUser() !== $externalTraining->getTrainer()){
        if(!(in_array('ROLE_TRAINER_ADMIN', $this->getUser()->getRoles()) or
             in_array('ROLE_MANAGER_GLOBAL', $this->getUser()->getRoles())) or 
             in_array('ROLE_STORE_MANAGER', $this->getUser()->getRoles())){
            return JsonResponse::create(["message" => "No tienes permisos para editar el Entrenamiento Externo"], JsonResponse::HTTP_BAD_REQUEST);
        }

//        if($externalTraining->isArchived()){
//            return JsonResponse::create(["message" => "El Entrenamiento Externo está archivado"], JsonResponse::HTTP_BAD_REQUEST);
//        }

        /** @var ExternalTrainingInscription $inscription */
        $inscription = $externalTraining->getInscriptions()->filter(function (ExternalTrainingInscription $inscription) use ($employee){
            return $inscription->getEmployee() === $employee;
        })->first();

        if(!$inscription){
            return JsonResponse::create(["message" => "El Empleado no está inscripto en el Entrenamiento Externo"], JsonResponse::HTTP_BAD_REQUEST);
        }
        $inscription->setAssistance($assistance);
        $em = $this->getDoctrine()->getManager();
        $em->persist($externalTraining);
        $em->flush();

        if($assistance)
        {
            /** @var ModuleExam $moduleExam */
            $moduleExam = $externalTraining->getModule()->getExam();
            if($moduleExam)
            {
                $command = new ActivateModuleExamCommand($employee, $moduleExam);
                $handler = $this->get('activate_exam');

                try
                {
                    $handler($command);
                } catch (\Exception $e) {
                }
            }
        }

        return JsonResponse::create([]);
    }


    /**
     * @Route("/externaltraining/{externalTraining}/approved/{employee}")
     * @Method("POST")
     * @param Request $request
     * @param ExternalTraining $externalTraining
     * @param Employee $employee
     * @return JsonResponse
     * @throws \Exception
     */
    public function approved(Request $request, ExternalTraining $externalTraining, Employee $employee){

        $data     = $request->request->all();
        $approved = isset($data['approved']) ? $data['approved'] : true;

        if ($externalTraining->getDate() > new \DateTime()) {
            return JsonResponse::create(["message" => "No puede editar este campo porque el Taller no se dictó aún"], JsonResponse::HTTP_BAD_REQUEST);
        }

        if(($approved === 'true') or ($approved === true) or ($approved === 1) or ($approved === "1")){
            $approved = true;
        }
        if(($approved === 'false') or ($approved === false) or ($approved === 0) or ($approved === "0")){
            $approved = false;
        }

        if(!is_bool($approved)){
            return JsonResponse::create(["message" => "Bad formed Data"], JsonResponse::HTTP_BAD_REQUEST);
        }

        // if($this->getUser() !== $externalTraining->getTrainer()){
        if(!(in_array('ROLE_TRAINER_ADMIN', $this->getUser()->getRoles()) or
             in_array('ROLE_MANAGER_GLOBAL', $this->getUser()->getRoles())) or
             in_array('ROLE_STORE_MANAGER', $this->getUser()->getRoles())){
            return JsonResponse::create(["message" => "No tienes permisos para editar el Taller de Nivelación"], JsonResponse::HTTP_BAD_REQUEST);
        }

//        if($externalTraining->isArchived()){
//            return JsonResponse::create(["message" => "El Entrenamiento Externo está archivado"], JsonResponse::HTTP_BAD_REQUEST);
//        }

        /** @var ExternalTrainingInscription $inscription */
        $inscription = $externalTraining->getInscriptions()->filter(function (ExternalTrainingInscription $inscription) use ($employee){
            return $inscription->getEmployee() === $employee;
        })->first();

        if(!$inscription){
            return JsonResponse::create(["message" => "El Empleado no está inscripto en el Taller de Nivelación"], JsonResponse::HTTP_BAD_REQUEST);
        }

        if($externalTraining->getModule()->isPresencialExam()){

            if(!$externalTraining->getModule()->getExam())
            {
                $fakeExam = new ModuleExam();
                $fakeExam->setName('Examen Presencial :: ' . $externalTraining->getModule()->getPlan()->getTitle() . ' - ' . $externalTraining->getModule()->getTitle());
                $fakeExam->setModule($externalTraining->getModule());
                $fakeExam->setEnabled(true);
//                $fakeExam->setMaxAttempts(1);
                $fakeExam->setMaxQuestionCount(0);
                $fakeExam->setPresencial($externalTraining->getModule()->isPresencialExam());
                $em = $this->getDoctrine()->getManager();
                $em->persist($fakeExam);
                $em->flush();
                $externalTraining->getModule()->setExam($fakeExam);

//            } else {
                // esto debería estar descomentado, para mantener la consistencia con SubscribeExternalTraining (línea 64),
                // pero eso evitaría que se cambie la calificación a 1 examen de 1 taller que ya fue evaluado anteriormente.

//                $fakeExam = $externalTraining->getModule()->getExam();
//                $em       = $this->getDoctrine()->getManager();
//                $employeeCareerModuleRepository = $em->getRepository(EmployeeCareerModule::class);
//                $moduleEmployee = $employeeCareerModuleRepository->findByEmployeeAndModule($employee, $externalTraining->getModule());
//                $examPermission = $em->getRepository(ExamPermission::class)->findByEmployeeModuleAndExam($moduleEmployee, $fakeExam);
//
//                if($examPermission)
//                {
//                    if($fakeExam->getMaxAttempts() <= $examPermission->getEmployeeExams()->count()){
//                        throw new \DomainException("El empleado ya agotó sus intentos de exámenes, no puede inscribirse a uno nuevo");
//                    }
//                }
            }

            $command = new ActivateModuleExamCommand($employee, $externalTraining->getModule()->getExam());
            $handler = $this->get('activate_exam');
            try {
                $handler($command);
            } catch (\Exception $e){

            }
        }

        if($approved){
            $command = new ApproveExternalTrainingExamCommand($employee, $externalTraining);
            $handler = $this->get('approve_external_training_exam');
        } else {
            $command = new DisapproveExternalTrainingExamCommand($employee, $externalTraining);
            $handler = $this->get('disapprove_external_training_exam');
        }


        try {
            $handler($command);
        } catch (\Exception $e) {
            return JsonResponse::create(['message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }

        return JsonResponse::create([]);

    }

}