<?php

namespace Aper\PlanCarreraBundle\Controller\Frontend;

use Aper\UserBundle\Entity\User;
use Aper\UserBundle\Entity\Employee;
use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\ExamPermission;
use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Class CareerPlanController
 * @Route("/ajax")
 */
class EmployeeController extends Controller
{
    /**
     * @Route("/my-career-plan")
     * @Method("GET")
     */
    public function __invoke()
    {
        /** @var Employee $employee */
        $employee = $this->getUser()->getEmployee();

        $plans = $employee->getCareerPlans();
        $serializer = $this->get('jms_serializer');
        $context = SerializationContext::create();
        $context->setGroups(['employee']);
        // $a = $plans[0]->getModules();
        // var_dump($a[0]->getExamPermissionsPost());die;
        return JsonResponse::create($serializer->toArray($plans, $context));
    }




    /**
     * @Route("/my-career-plan-user")
     * @Method("GET")
     */
    public function __invoke2()
    {

        $id = 0;
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        }

        // print_r($this);

        /** @var Employee $employee */
        $usuarioActual= $this->getDoctrine()->getRepository('UserBundle:Employee')->find($id);
        // $employee = $usuarioActual->getEmployee();
        $employee = $usuarioActual;

        $plans = $employee->getCareerPlans();
        $serializer = $this->get('jms_serializer');
        $context = SerializationContext::create();
        $context->setGroups(['employee']);
        // $a = $plans[0]->getModules();
        // var_dump($a[0]->getExamPermissionsPost());die;
        return JsonResponse::create($serializer->toArray($plans, $context));
    }

}