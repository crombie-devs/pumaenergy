<?php

namespace Aper\PlanCarreraBundle\Controller\Backend;

use Aper\PlanCarreraBundle\CQRS\Command\ActivateModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ActivateModulePollCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ActivatePlanRequirementCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ApproveModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ApprovePlanCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ApprovePlanRequirementCommand;
use Aper\PlanCarreraBundle\CQRS\Command\AssignTrainer;
use Aper\PlanCarreraBundle\CQRS\Command\ApproveModuleCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DeactivateModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DeactivateModulePollCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DeactivatePlanRequirementCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapproveModuleCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ActivateModuleCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DeactivateModuleCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapproveModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapprovePlanCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapprovePlanRequirementCommand;
use Aper\PlanCarreraBundle\CQRS\Command\RegisterToProgramCommand;
use Aper\PlanCarreraBundle\CQRS\CommandHandler\RegisterToProgram;
use Aper\PlanCarreraBundle\Entity\CareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Entity\ExamPermission;
use Aper\PlanCarreraBundle\Entity\PollPermission;
use Aper\PlanCarreraBundle\Entity\ExternalTraining;
use Aper\PlanCarreraBundle\Entity\ExternalTrainingInscription;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Entity\ModulePoll;
use Aper\PlanCarreraBundle\Entity\PlanRequirement;
use Aper\PlanCarreraBundle\Entity\PlanRequirementsEmployee;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class EmployeeController
 * @package Aper\PlanCarreraBundle\Controller\Backend
 *
 * @Route("/employee")
 */
class EmployeeController extends Controller
{
    /**
     * Lista todos los empleados
     * @Route("/", name="employee_career_plan_index")
     */
    public function indexAction()
    {
        $employeeRepository = $this->getDoctrine()->getRepository('UserBundle:Employee');
        $employees = $employeeRepository->findBy([], [], 100); //todo paginar, filtrar, ver...

        return compact('employees');
    }

    /**
     * Muestra el estado actual del empleado dentro del programa
     * Brinda acceso a las diferentes acciones sobre planes, módulos, etc.
     *
     * @param Employee $employee
     * @return array
     *
     * @Route("/show/{id}", name="employee_career_plan_show")
     * @Template("@PlanCarrera/Backend/Employee/show.html.twig")
     */
    public function showAction(Employee $employee)
    {
        $command = AssignTrainer::fromEmployee($employee);
        $form = $this->createAssignTrainerForm($command);

        $plans = $this->getDoctrine()->getRepository('PlanCarreraBundle:CareerPlan')->findAllWithOrder();

        return compact('employee', 'plans') + ['assign_trainer_form' => $form->createView()];
    }

    /**
     * Permite asignar un usuario Trainer al empleado
     *
     * @param Request $request
     * @param Employee $employee
     * @Route("/trainer/{id}", name="employee_career_plan_set_trainer")
     * @Method("PUT")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function assignTrainerAction(Request $request, Employee $employee)
    {
        $command = AssignTrainer::fromEmployee($employee);
        $form = $this->createAssignTrainerForm($command);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $trainerManager = $this->get('trainer_manager');
            $trainerManager->assign($command);

            $this->addFlash('success', 'Entrenador asignado!');
        }

        return $this->redirect($request->server->get('HTTP_REFERER'));
//        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * Permite asignar inicialmente un plan (el actual) al empleado
     *
     * @param Employee $employee
     * @param CareerPlan $initialPlan
     * @Route("/assign-plan/{employee}/{initialPlan}", name="employee_career_plan_assign_plan")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function assignPlanAction(Employee $employee, CareerPlan $initialPlan)
    {
        $command = new RegisterToProgramCommand($employee->getId(), $initialPlan->getId());
        /** @var RegisterToProgram $handler */
        $handler = $this->get('register_to_program');
        try {
            $handler($command);

            $this->addFlash('success', 'Plan asignado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido.' . $e->getMessage());
        }

        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * Permite asignar un módulo del plan indicado al empleado
     *
     * @param Employee $employee
     * @param CareerPlan $plan
     */
    public function assignModuleAction(Employee $employee, CareerPlan $plan)
    {

    }

    /**
     * @param Employee $employee
     * @param Module $module
     * @Route("/active-module/{employee}/{module}/", name="employee_career_plan_activate_module")
     *
     * * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function activeModuleAction(Employee $employee, Module $module, Request $request)
    {
        $dateFrom = $request->get('date_from');
        $dateTo = $request->get('date_to');
        if(!empty($dateFrom) && !empty($dateTo)){
            $command = new ActivateModuleCommand($employee, $module, $dateFrom, $dateTo);
            $handler = $this->get('activate_module');
            try {
                $handler($command);
    
                $this->addFlash('success', 'Módulo Activado!');
            } catch (\Exception $e) {
                $this->addFlash('error', 'Un error ha ocurrido.');
            }
        } else {
            $this->addFlash('error', 'Los campos de fecha no pueden estar vacíos');
        }

        return $this->redirectToRoute('employee_career_plan_activate_module_exam', ['employee' => $employee->getId(), 'moduleExam' =>$module->getExam()->getId(), 'prepost' => 0]);
    }

    /**
     * @param Employee $employee
     * @param Module $module
     * @Route("/deactivate-module/{employee}/{module}", name="employee_career_plan_deactivate_module")
     *
     * * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deactivateModuleAction(Employee $employee, Module $module)
    {
        $command = new DeactivateModuleCommand($employee, $module);
        $handler = $this->get('deactivate_module');
        try {
            $handler($command);

            $this->addFlash('success', 'Módulo Desactivado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido.');
        }

        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @param Employee $employee
     * @param Module $module
     * @Route("/approve-module/{employee}/{module}", name="employee_career_plan_approve_module")
     *
     * * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function approveModuleAction(Employee $employee, Module $module)
    {
        $command = new ApproveModuleCommand($employee, $module);
        $handler = $this->get('approve_module');
        try {
            $handler($command);

            $this->addFlash('success', 'Módulo Aprobado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido.');
        }

        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @param Employee $employee
     * @param Module $module
     * @Route("/disapprove-module/{employee}/{module}", name="employee_career_plan_disapprove_module")
     *
     * * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function disapproveModuleAction(Employee $employee, Module $module)
    {
        $command = new DisapproveModuleCommand($employee, $module);
        $handler = $this->get('disapprove_module');
        try {
            $handler($command);

            $this->addFlash('success', 'Módulo Desaprobado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido.');
        }

        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }


    /**
     * @param Employee $employee
     * @param PlanRequirement $planRequirement
     * @Route("/approve-requirement/{employee}/{planRequirement}", name="employee_career_plan_approve_requirement")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function approveRequirementAction(Employee $employee, PlanRequirement $planRequirement)
    {
        $command = new ApprovePlanRequirementCommand($employee, $planRequirement);
        $handler = $this->get('approve_requirement');
        try {
            $handler($command);

            $this->addFlash('success', 'Requerimiento Aprobado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido.');
        }

        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @param Employee $employee
     * @param PlanRequirement $planRequirement
     * @Route("/disapprove-requirement/{employee}/{planRequirement}", name="employee_career_plan_disapprove_requirement")
     *
     * * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function disapprovePlanRequirementAction(Employee $employee, PlanRequirement $planRequirement)
    {
        $command = new DisapprovePlanRequirementCommand($employee, $planRequirement);
        $handler = $this->get('disapprove_requirement');
        try {
            $handler($command);

            $this->addFlash('success', 'Módulo Desaprobado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido.');
        }

        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @param AssignTrainer $command
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createAssignTrainerForm(AssignTrainer $command)
    {
        $employeeRepository = $this->getDoctrine()->getRepository('UserBundle:Employee');
        $trainers = $employeeRepository->findAllTrainers();

        //todo mover
        $form = $this->createFormBuilder($command, [
            'action' => $this->generateUrl('employee_career_plan_set_trainer', ['id' => $command->getEmployee()->getId()]),
            'method' => 'PUT'
        ])
            ->add('trainer', EntityType::class, [
                'required' => false,
                'class'    => Employee::class,
                'choices'  => $trainers,
                'attr'     => ['style' => 'border:1px solid #ccc;']
            ])
            ->add('submit', SubmitType::class, ['attr' => ['class' => 'btn-primary pull-right']])
            ->getForm();

        return $form;
    }


    /**
     * @param Employee $employee
     * @param PlanRequirement $planRequirement
     * @Route("/active-requirement/{employee}/{planRequirement}", name="employee_career_plan_activate_requirement")
     *
     * * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function activePlanRequirementAction(Employee $employee, PlanRequirement $planRequirement)
    {
        $command = new ActivatePlanRequirementCommand($employee, $planRequirement);
        $handler = $this->get('activate_requirement');
        try {
            $handler($command);

            $this->addFlash('success', 'Requerimiento Activado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido.');
        }

        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @param Employee $employee
     * @param PlanRequirement $planRequirement
     * @Route("/deactivate-requirement/{employee}/{planRequirement}", name="employee_career_plan_deactivate_requirement")
     *
     * * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deactivatePlanRequirementAction(Employee $employee, PlanRequirement $planRequirement)
    {
        $command = new DeactivatePlanRequirementCommand($employee, $planRequirement);
        $handler = $this->get('deactivate_requirement');
        try {
            $handler($command);

            $this->addFlash('success', 'Requerimiento Desactivado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido.');
        }

        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @param Employee $employee
     * @param ModuleExam $moduleExam
     * @Route("/active-exam/{employee}/{moduleExam}/{prepost}/", name="employee_career_plan_activate_module_exam")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function activeModuleExamAction(Employee $employee, ModuleExam $moduleExam, $prepost){
        $command = new ActivateModuleExamCommand($employee, $moduleExam, $prepost);
        $handler = $this->get('activate_exam');

        try {
            $handler($command);

            $this->addFlash('success', 'Examen Activado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido. ' . $e->getMessage());
        }
        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @param Employee $employee
     * @param ModuleExam $moduleExam
     * @Route("/deactivate-exam/{employee}/{moduleExam}/{prepost}/", name="employee_career_plan_deactivate_module_exam")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deactivateModuleExamAction(Employee $employee, ModuleExam $moduleExam, $prepost){

        $command = new DeactivateModuleExamCommand($employee, $moduleExam, $prepost);
        $handler = $this->get('deactivate_exam');
        try {
            $handler($command);

            $this->addFlash('success', 'Examen Desactivado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido. ' . $e->getMessage());
        }
        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @param Employee $employee
     * @param ModulePoll $modulePoll
     * @Route("/active-poll/{employee}/{modulePoll}", name="employee_career_plan_activate_module_poll")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function activeModulePollAction(Employee $employee, ModulePoll $modulePoll){
        $command = new ActivateModulePollCommand($employee, $modulePoll);
        $handler = $this->get('activate_poll');
        try {
            $handler($command);

            $this->addFlash('success', 'Encuesta Activada!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido. ' . $e->getMessage());
        }
        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @param Employee $employee
     * @param ModulePoll $modulePoll
     * @Route("/deactivate-poll/{employee}/{modulePoll}", name="employee_career_plan_deactivate_module_poll")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deactivateModulePollAction(Employee $employee, ModulePoll $modulePoll){
        $command = new DeactivateModulePollCommand($employee, $modulePoll);
        $handler = $this->get('deactivate_poll');
        try {
            $handler($command);

            $this->addFlash('success', 'Encuesta Desactivada!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido. ' . $e->getMessage());
        }
        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @param Employee $employee
     * @param ModuleExam $moduleExam
     * @param EmployeeModuleExam $test
     * @Route("/approve-exam/{employee}/{moduleExam}/{test}", name="employee_career_plan_approve_module_exam")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function approveModuleExamAction(Employee $employee, ModuleExam $moduleExam, EmployeeModuleExam $test){
        $command = new ApproveModuleExamCommand($employee, $moduleExam, $test);
        $handler = $this->get('approve_exam');
        try {
            $handler($command);

            $this->addFlash('success', 'Examen Aprobado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido.');
        }
        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @param Employee $employee
     * @param ModuleExam $moduleExam
     * @param EmployeeModuleExam $test
     * @Route("/disapprove-exam/{employee}/{moduleExam}/{test}", name="employee_career_plan_disapprove_module_exam")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function disapproveModuleExamAction(Employee $employee, ModuleExam $moduleExam, EmployeeModuleExam $test){
        $command = new DisapproveModuleExamCommand($employee, $moduleExam, $test);
        $handler = $this->get('disapprove_exam');
        try {
            $handler($command);

            $this->addFlash('success', 'Examen Reprobado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido.');
        }
        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }




    /**
     * @param Employee $employee
     * @param CareerPlan $careerPlan
     * @Route("/approve-plan/{employee}/{careerPlan}", name="employee_career_plan_approve")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function approvePlanAction(Employee $employee, CareerPlan $careerPlan){
        $command = new ApprovePlanCommand($employee, $careerPlan);
        $handler = $this->get('approve_plan');
        try {
            $handler($command);

            $this->addFlash('success', 'Plan Aprobado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido.');
        }
        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @param Employee $employee
     * @param CareerPlan $careerPlan
     * @Route("/disapprove-plan/{employee}/{careerPlan}", name="employee_career_plan_disapprove")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function disapprovePlanAction(Employee $employee, CareerPlan $careerPlan){
        $command = new DisapprovePlanCommand($employee, $careerPlan);
        $handler = $this->get('disapprove_plan');
        try {
            $handler($command);

            $this->addFlash('success', 'Plan Reprobado!');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Un error ha ocurrido.');
        }
        return $this->redirectToRoute('employee_career_plan_show', ['id' => $employee->getId()]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @Route("/import", name="employee_career_plan_import")
     *
     */
    public function importAction(){

        ini_set('max_execution_time', 900);
        ini_set('memory_limit','1G');

        $initTime = microtime(true);
        $initExcel = microtime(true);

        $excelImporter = $this->get('excel_importer');
        $em            = $this->getDoctrine()->getManager();

        // $data          = $excelImporter->importarExcelTalleres('/var/www/pumaenergy-prod/www/web/uploads/import_file/Importador Atencion.xlsx');
        $data          = $excelImporter->importarExcelTalleres('/var/www/pumaenergy-prod/www/web/uploads/import_file/SegN2.xlsx', 17);
        // $data          = $excelImporter->importarExcelTalleres('/var/www/pumaenergy-dev.aper.cloud/www/web/uploads/import_file/Importador Planes Red propia.xlsx');
        // $data          = $excelImporter->importarExcelTalleres('D:\\wamp\\www\\aper\\puma56\\pumaenergy\\web\\uploads\\import_file\\PDC_importacion.xlsx');

        $endExcel = microtime(true);
        $requirements  = [];
        $modules       = [];
        $employee      = null;


        // $data = array_slice($data, 0, 1);
        // print_r($data);exit;



        $position = 1;
        if (isset($_GET['page'])) {
            $position = $_GET['page'];
        }
        $perpage = 30;
        $from = $perpage * ($position-1);
        $data = array_slice($data, $from, $perpage);
        // $data = array_slice($data, 2, 1);

// echo 'aaa';exit;

        // echo count($data);exit;

        foreach ($data as $email => $employeeData) {

            // $email = 'paizensztein@aper.com';
            $currentUser = $this->getDoctrine()->getRepository(User::class)->findOneByEmail($email);

            if($currentUser) {
                $employee = $currentUser->getEmployee();
                $initEmployee = microtime(true);

                foreach ($employeeData as $idMod => $statusMod) {

                    $examenPre = $statusMod['examenPre'];
                    $curso = $statusMod['curso'];
                    $examenPos = $statusMod['examenPos'];
                    $encuesta = $statusMod['encuesta'];
                    $moduleApproved = $examenPre  && $curso  && $examenPos  && $encuesta ;
                
                    if($examenPre || $curso || $examenPos || $encuesta ) {
                        if(!isset($modules[$idMod])) {
                            $module = $this->getDoctrine()->getRepository(Module::class)->find($idMod);
                            
                            if(!$module) continue;
                            
                            $modules[$idMod] = $module;
                        }

                        $module = $modules[$idMod];

                        if($module) {
                            // asigno el plan si no lo tiene.
                            /** @var CareerPlan $plan */
                            $plan = $module->getPlan();

                            $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
                                return $employeeCareerPlan->getPlan() === $plan;
                            })->first();

                            if(!$employeeCareerPlan) {
                                $employeeCareerPlan = EmployeeCareerPlan::createForCurrentPlan($plan, $employee);
                                $em->persist($employeeCareerPlan);
                                $em->flush();
                                $employee->addCareerPlan($employeeCareerPlan);
                            }

                            $employeeCareerPlan->setEnabled(true);
                            $em->persist($employeeCareerPlan);

                            if(!$employee->getCurrentPlan() or ($employee->getCurrentPlan()->getId() < $employeeCareerPlan->getId())) {
                                $employee->setCurrentPlan($plan);
                            }

                            $em->persist($employee);
                            $em->flush();

                            $employeeCareerModule = $this->getDoctrine()->getRepository(EmployeeCareerModule::class)->findByEmployeeAndModule($employee, $module);

                            if(!$employeeCareerModule){
                                $employeeCareerModule = new EmployeeCareerModule();
                                $employeeCareerModule->setModule($module);
                                $employeeCareerModule->setCourseApproved($curso);
                                $employeeCareerModule->setEnabled(true);
                                $employeeCareerModule->setApproved($moduleApproved);
                                $employeeCareerModule->setEmployeePlan($employeeCareerPlan);
                                $em->persist($employeeCareerModule);
                                $em->flush();
                                $employeeCareerPlan->addModule($employeeCareerModule);
                            }

                            if($examenPre) {
                                // Lógica para examen pre
                                $examPrePermission = null;
                                $module->setPresencialExam(false);

                                $moduleExamPre = $module->getExam();

                                $examPrePermission = $this->getDoctrine()->getRepository(ExamPermission::class)->findByEmployeeModuleAndExam($employeeCareerModule, $moduleExamPre, false);

                                if($examPrePermission) {
                                    if($moduleExamPre->getMaxAttempts() <= $examPrePermission->getEmployeeExams()->count()){
                                        //die("El empleado ya agotó sus intentos de exámenes, no puede inscribirse a uno nuevo");
                                    }
                                } else {
                                    $examPrePermission = new ExamPermission($employeeCareerModule, $moduleExamPre, 0);
                                    $examPrePermission->setPrevioPost(false);
                                    $em->persist($examPrePermission);
                                    $em->flush();
                                    $employeeCareerModule->addExamPermission($examPrePermission);
                                    $em->persist($employeeCareerModule);
                                    $em->flush();
                                }

                                if(!$examPrePermission->isEnabled()){
                                    $examPrePermission->setEnabled(true);
                                    $em->persist($examPrePermission);
                                    $em->flush();
                                }

                                /// Aprobación
                                $employeeModuleExams = $this->getDoctrine()->getRepository(EmployeeModuleExam::class)->findByEmployeeAndExam($moduleExamPre, $employee);

                                $test = null;
                                if($moduleExamPre->getMaxAttempts()) {
                                    if((count($employeeModuleExams) == 0)) {
                                        $test = EmployeeModuleExam::create($employee, $moduleExamPre, 0);
                                        $em->persist($test);
                                        $em->flush();
                                    }
                                }

                                if(!is_null($test)) {
                                    $test->finish();
                                    $test->setApproved($examenPre);
                                    $test->setPresencialExam(false);
                                    $test->setStartsAt(new \DateTime('now'));
                                    $test->setFinishedAt(new \DateTime('now'));
                                    $em->persist($test);
                                    $em->flush();
                                }

                                $module->setPresencialExam(false);
                            }

                            if($examenPos ){
                                // Lógica para examen pos
                                $examPosPermission = null;
                                $module->setPresencialExam(false);

                                $moduleExamPos = $module->getExam();

                                $examPosPermission = $this->getDoctrine()->getRepository(ExamPermission::class)->findByEmployeeModuleAndExam($employeeCareerModule, $moduleExamPos, true);

                                if($examPosPermission) {
                                    if($moduleExamPos->getMaxAttempts()+1 <= $examPosPermission->getEmployeeExams()->count()){
                                        //die("El empleado ya agotó sus intentos de exámenes, no puede inscribirse a uno nuevo");
                                    }
                                } else {
                                    $examPosPermission = new ExamPermission($employeeCareerModule, $moduleExamPos, 0);
                                    $examPosPermission->setPrevioPost(true);
                                    $em->persist($examPosPermission);
                                    $em->flush();
                                    $employeeCareerModule->addExamPermission($examPosPermission);
                                    $em->persist($employeeCareerModule);
                                    $em->flush();
                                }

                                if(!$examPosPermission->isEnabled()){
                                    $examPosPermission->setEnabled(true);
                                    $em->persist($examPosPermission);
                                    $em->flush();
                                }

                                /// Aprobación
                                $employeeModuleExams = $this->getDoctrine()->getRepository(EmployeeModuleExam::class)->findByEmployeeAndExam($moduleExamPos, $employee);

                                $test = null;
                                if($moduleExamPos->getMaxAttempts()) {
                                    if((count($employeeModuleExams) <= 1)) {
                                        $test = EmployeeModuleExam::create($employee, $moduleExamPos, 1);
                                        $em->persist($test);
                                        $em->flush();
                                    }
                                }

                                if(!is_null($test)) {
                                    $test->finish();
                                    $test->setApproved($examenPos);
                                    $test->setPresencialExam(false);
                                    $test->setStartsAt(new \DateTime('now'));
                                    $test->setFinishedAt(new \DateTime('now'));
                                    $em->persist($test);
                                    $em->flush();
                                }

                                $module->setPresencialExam(false);
                            }

                            if($encuesta){
                                // Lógica para encuesta
                                $pollPermisson = null;

                                $modulePoll = $module->getPoll();

                                if(!$modulePoll || !$employeeCareerModule)
                                    continue;

                                $pollPermisson = $this->getDoctrine()->getRepository(PollPermission::class)->findByEmployeeModuleAndPoll($employeeCareerModule, $modulePoll, true);

                                if(!$pollPermisson) {
                                    $pollPermisson = new PollPermission($employeeCareerModule, $modulePoll);
                                    $em->persist($pollPermisson);
                                    $em->flush();
                                    $employeeCareerModule->addPollPermission($pollPermisson);
                                    $em->persist($employeeCareerModule);
                                    $em->flush();
                                }

                                if(!$pollPermisson->isEnabled()){
                                    $pollPermisson->setEnabled(true);
                                    $em->persist($pollPermisson);
                                    $em->flush();
                                }
                            }
                        }
                    } else {


                        if(!isset($modules[$idMod])) {
                            $module = $this->getDoctrine()->getRepository(Module::class)->find($idMod);
                            
                            if(!$module) continue;
                            
                            $modules[$idMod] = $module;
                        }

                        $module = $modules[$idMod];

                        if($module) {
                            // asigno el plan si no lo tiene.
                            /** @var CareerPlan $plan */
                            $plan = $module->getPlan();

                            $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
                                return $employeeCareerPlan->getPlan() === $plan;
                            })->first();

                            if(!$employeeCareerPlan) {
                                $employeeCareerPlan = EmployeeCareerPlan::createForCurrentPlan($plan, $employee);
                                $em->persist($employeeCareerPlan);
                                $em->flush();
                                $employee->addCareerPlan($employeeCareerPlan);
                            }

                            $employeeCareerPlan->setEnabled(true);
                            $em->persist($employeeCareerPlan);

                            if(!$employee->getCurrentPlan() or ($employee->getCurrentPlan()->getId() < $employeeCareerPlan->getId())) {
                                $employee->setCurrentPlan($plan);
                            }

                            $em->persist($employee);
                            $em->flush();

                            $employeeCareerModule = $this->getDoctrine()->getRepository(EmployeeCareerModule::class)->findByEmployeeAndModule($employee, $module);

                            if(!$employeeCareerModule){
                                $employeeCareerModule = new EmployeeCareerModule();
                                $employeeCareerModule->setModule($module);
                                $employeeCareerModule->setCourseApproved($curso);
                                $employeeCareerModule->setEnabled(true);
                                $employeeCareerModule->setApproved($moduleApproved);
                                $employeeCareerModule->setEmployeePlan($employeeCareerPlan);
                                $em->persist($employeeCareerModule);
                                $em->flush();
                                $employeeCareerPlan->addModule($employeeCareerModule);
                            }

                            // if($examenPre) {
                            if (true) {
                                // Lógica para examen pre
                                $examPrePermission = null;
                                $module->setPresencialExam(false);

                                $moduleExamPre = $module->getExam();

                                $examPrePermission = $this->getDoctrine()->getRepository(ExamPermission::class)->findByEmployeeModuleAndExam($employeeCareerModule, $moduleExamPre, false);

                                if($examPrePermission) {
                                    if($moduleExamPre->getMaxAttempts() <= $examPrePermission->getEmployeeExams()->count()){
                                        //die("El empleado ya agotó sus intentos de exámenes, no puede inscribirse a uno nuevo");
                                    }
                                } else {
                                    $examPrePermission = new ExamPermission($employeeCareerModule, $moduleExamPre, 0);
                                    $examPrePermission->setPrevioPost(false);
                                    $em->persist($examPrePermission);
                                    $em->flush();
                                    $employeeCareerModule->addExamPermission($examPrePermission);
                                    $em->persist($employeeCareerModule);
                                    $em->flush();
                                }

                                if(!$examPrePermission->isEnabled()){
                                    $examPrePermission->setEnabled(true);
                                    $em->persist($examPrePermission);
                                    $em->flush();
                                }

                                if ($examenPre) {
                                    /// Aprobación
                                    $employeeModuleExams = $this->getDoctrine()->getRepository(EmployeeModuleExam::class)->findByEmployeeAndExam($moduleExamPre, $employee);

                                    $test = null;
                                    if($moduleExamPre->getMaxAttempts()) {
                                        if((count($employeeModuleExams) == 0)) {
                                            $test = EmployeeModuleExam::create($employee, $moduleExamPre, 0);
                                            $em->persist($test);
                                            $em->flush();
                                        }
                                    }

                                    if(!is_null($test)) {
                                        $test->finish();
                                        // $test->setApproved($examenPre);
                                        $test->setApproved($examenPre);
                                        $test->setPresencialExam(false);
                                        $test->setStartsAt(new \DateTime('now'));
                                        $test->setFinishedAt(new \DateTime('now'));
                                        $em->persist($test);
                                        $em->flush();
                                    }

                                    $module->setPresencialExam(false);

                                }
                            }

                            if($examenPos ){
                                // Lógica para examen pos
                                $examPosPermission = null;
                                $module->setPresencialExam(false);

                                $moduleExamPos = $module->getExam();

                                $examPosPermission = $this->getDoctrine()->getRepository(ExamPermission::class)->findByEmployeeModuleAndExam($employeeCareerModule, $moduleExamPos, true);

                                if($examPosPermission) {
                                    if($moduleExamPos->getMaxAttempts()+1 <= $examPosPermission->getEmployeeExams()->count()){
                                        //die("El empleado ya agotó sus intentos de exámenes, no puede inscribirse a uno nuevo");
                                    }
                                } else {
                                    $examPosPermission = new ExamPermission($employeeCareerModule, $moduleExamPos, 0);
                                    $examPosPermission->setPrevioPost(true);
                                    $em->persist($examPosPermission);
                                    $em->flush();
                                    $employeeCareerModule->addExamPermission($examPosPermission);
                                    $em->persist($employeeCareerModule);
                                    $em->flush();
                                }

                                if(!$examPosPermission->isEnabled()){
                                    $examPosPermission->setEnabled(true);
                                    $em->persist($examPosPermission);
                                    $em->flush();
                                }

                                /// Aprobación
                                $employeeModuleExams = $this->getDoctrine()->getRepository(EmployeeModuleExam::class)->findByEmployeeAndExam($moduleExamPos, $employee);

                                $test = null;
                                if($moduleExamPos->getMaxAttempts()) {
                                    if((count($employeeModuleExams) <= 1)) {
                                        $test = EmployeeModuleExam::create($employee, $moduleExamPos, 1);
                                        $em->persist($test);
                                        $em->flush();
                                    }
                                }

                                if(!is_null($test)) {
                                    $test->finish();
                                    $test->setApproved($examenPos);
                                    $test->setPresencialExam(false);
                                    $test->setStartsAt(new \DateTime('now'));
                                    $test->setFinishedAt(new \DateTime('now'));
                                    $em->persist($test);
                                    $em->flush();
                                }

                                $module->setPresencialExam(false);
                            }

                            if($encuesta){
                                // Lógica para encuesta
                                $pollPermisson = null;

                                $modulePoll = $module->getPoll();

                                if(!$modulePoll || !$employeeCareerModule)
                                    continue;

                                $pollPermisson = $this->getDoctrine()->getRepository(PollPermission::class)->findByEmployeeModuleAndPoll($employeeCareerModule, $modulePoll, true);

                                if(!$pollPermisson) {
                                    $pollPermisson = new PollPermission($employeeCareerModule, $modulePoll);
                                    $em->persist($pollPermisson);
                                    $em->flush();
                                    $employeeCareerModule->addPollPermission($pollPermisson);
                                    $em->persist($employeeCareerModule);
                                    $em->flush();
                                }

                                if(!$pollPermisson->isEnabled()){
                                    $pollPermisson->setEnabled(true);
                                    $em->persist($pollPermisson);
                                    $em->flush();
                                }
                            }
                        }

                    }
                }

                // print_r($employee->getId());
                // echo '<pre>';
                // print_r($employeeData);
                // echo 'asdsad';exit;
            }

            // echo 'chau';
            // exit;
        }

        if (count($data) >= $perpage) {

            foreach ($data as $email => $value) {
                echo '<br>';
                echo $email;
            }
            ?>

            <script type="text/javascript">
                setTimeout(function (){
                    window.location.href = '<?php echo $_SERVER['REDIRECT_URL'].'?page=' . ($position+1); ?>';
                }, 2000);
            </script>
            <?php
            exit;
        }
        // echo $_SERVER['REDIRECT_URL'];exit;

        $endTime = microtime(1);    

        return $this->redirectToRoute('easyadmin', ['action' => 'list', 'entity' => 'Employee']);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @Route("/talleres-import", name="employee_career_talleres_import")
     */
    public function importTalleresAction() {
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        $excelImporter = $this->get('excel_importer');
        $em            = $this->getDoctrine()->getManager();
        $data          = $excelImporter->importarExcelTalleres('/application/web/uploads/import_file/importador_talleres.xlsx');

        $employee      = null;

        foreach ($data as $dni => $employeeData) {
            foreach ($employeeData as $tallerId => $employeeTraiging) {
                $employee = $this->getDoctrine()->getRepository(Employee::class)->findEmployeeByUsername($dni);

                /** @var ExternalTraining $training */
                $training = $this->getDoctrine()->getRepository(ExternalTraining::class)->find($tallerId);

                if(!$training){
                    die('no encontró el training id: ' . $tallerId);
                }

                if(!$employee){
                    die('no encontró el empleado');
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /// Suscribir

                $module = $training->getModule();
                $flagExamenPresencial = $module->isPresencialExam();

                $plan   = $module->getPlan();

                $employeeCareerPlan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $employeeCareerPlan) use ($plan) {
                    return $employeeCareerPlan->getPlan() === $plan;
                })->first();

                if(!$employeeCareerPlan)
                {
                    $employeeCareerPlan = new EmployeeCareerPlan($module->getPlan(), $employee);
                    $em->persist($employeeCareerPlan);
                    $em->flush();
                    if(!$employee->getCareerPlans()->count())
                    {
                        $employee->setCurrentPlan($plan);
                    }
                    $employee->addCareerPlan($employeeCareerPlan);
                    $em->persist($employee);
                    $em->flush();
                }

                $employeeCareerModule = $this->getDoctrine()->getRepository(EmployeeCareerModule::class)->findByEmployeeAndModule($employee, $module);

                if(!$employeeCareerModule){

                    $employeeCareerModule = new EmployeeCareerModule();
                    $employeeCareerModule->setModule($module);
                    $employeeCareerModule->setEmployeePlan($employeeCareerPlan);
                    $em->persist($employeeCareerModule);
                    $em->flush();
                    $employeeCareerPlan->addModule($employeeCareerModule);
                }

                $examPermission = null;

                if(!$module->isPresencialExam()) {
                    $module->setPresencialExam(true);
                }

                if(!$module->getExam()){
                    // fake exam
                    $moduleExam = new ModuleExam();
                    $moduleExam->setName('Examen Presencial :: ' . $module->getPlan()->getTitle() . ' - ' . $module->getTitle());
                    $moduleExam->setModule($module);
                    $moduleExam->setEnabled(true);
                    $moduleExam->setMaxQuestionCount(0);
                    $moduleExam->setPresencial($module->isPresencialExam());
                    $em->persist($moduleExam);
                    $em->flush();
                    $module->setExam($moduleExam);
                } else {
                    $moduleExam = $module->getExam();
                }

                $examPermission = $this->getDoctrine()->getRepository(ExamPermission::class)->findByEmployeeModuleAndExam($employeeCareerModule, $moduleExam);

                if($examPermission) {
                    if($moduleExam->getMaxAttempts() <= $examPermission->getEmployeeExams()->count()){
                        //die("El empleado ya agotó sus intentos de exámenes, no puede inscribirse a uno nuevo");
                    }
                } else {
                    $examPermission = new ExamPermission($employeeCareerModule, $moduleExam);
                    $em->persist($examPermission);
                    $em->flush();
                    $employeeCareerModule->addExamPermission($examPermission);
                    $em->persist($employeeCareerModule);
                    $em->flush();
                }

                if(!$examPermission->isEnabled()){
                    $examPermission->setEnabled(true);
                    $em->persist($examPermission);
                    $em->flush();
                }

                // ver si está subscripto,
                $inscription = $training->getInscriptionForEmployee($employee);
                if(!$inscription) {
                    $inscription = new ExternalTrainingInscription();
                    $inscription->setEmployee($employee);
                    $inscription->setTraining($training);
                    $inscription->setCreatedAt($employeeTraiging['fecha_asistencia']);

                    $training->addInscription($inscription);

                    $em->persist($training);
                    $em->flush();
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // asistencia

                $inscription->setAssistance($employeeTraiging['asistencia']);
                $em->persist($inscription);
                $em->flush();

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /// Aprobación

                $employeeModuleExams = $this->getDoctrine()->getRepository(EmployeeModuleExam::class)->findByEmployeeAndExam($moduleExam, $employee);

                $test = null;
                if($moduleExam->getMaxAttempts()) {
                    if((count($employeeModuleExams) < $moduleExam->getMaxAttempts())) {
                        $test = EmployeeModuleExam::create($employee, $moduleExam, 0);
                        $em->persist($test);

                        $inscription->setEmployeeExam($test);
                        $em->persist($inscription);
                        $em->flush();
                    } else {
                        if($inscription->getEmployeeExam()) {
                            $test = $inscription->getEmployeeExam();
                        } else {
                            $test = null;
                            foreach ($employeeModuleExams as $employeeModuleExam){
                                $test           = $employeeModuleExam;
                                $testRegistered = $this->getDoctrine()->getRepository(ExternalTrainingInscription::class)->findByEmployeeModuleExam($test);
                                if(!$testRegistered){
                                    $inscription->setEmployeeExam($test);
                                    try {
                                        $em->persist($inscription);
                                        $em->flush();
                                        break;
                                    } catch (\Exception $e){ }
                                }
                                $test = null;
                            }
                        }
                    }
                } else {
                    //die('el module examen no tiene max intentos');
                }

                if(!is_null($test)) {
                    $test->finish();
                    $test->setApproved($employeeTraiging['aprobacion']);
                    $test->setPresencialExam($moduleExam->isPresencial());
                    $test->setStartsAt($employeeTraiging['fecha_aprobacion']);
                    $test->setFinishedAt($employeeTraiging['fecha_aprobacion']);
                    $em->persist($test);
                    $em->flush();
                }

                $module->setPresencialExam($flagExamenPresencial);
            }
        }

        return $this->redirectToRoute('easyadmin', ['action' => 'list', 'entity' => 'Employee']);
    }
}