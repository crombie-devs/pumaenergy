<?php

namespace Aper\PlanCarreraBundle\Controller\Backend;


use Aper\PlanCarreraBundle\Entity\EmployeeAnswer;
use Aper\PlanCarreraBundle\Entity\ModulePoll;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PollController
 * @package Aper\PlanCarreraBundle\Controller\Backend
 *
 * @Route("/poll")
 */
class PollController extends Controller
{

    /**
     * Muestra el detalle de una encuesta
     *
     * @param ModulePoll $poll
     * @return array
     *
     * @Route("/show/{id}", name="module_poll_show")
     * @Template("@PlanCarrera/Backend/Poll/show.html.twig")
     */
    public function showAction(ModulePoll $poll)
    {
        $form     = $this->createExportDateForm($poll);
        $module   = $poll->getModule();
        $plan     = $module->getPlan();
        $surveyed = $this->getDoctrine()->getManager()->getRepository(EmployeeAnswer::class)->countEmployeesAnswered($module);

        return compact('poll','module','plan') + ['employeesSurveyed' => $surveyed, 'export_data_form' => $form->createView()];
    }

    /**
     * Exporta el detalle de una encuesta
     *
     * @param Request $request
     * @param ModulePoll $poll
     * @return array
     *
     * @Method("PUT")
     * @Route("/export/{id}", name="module_poll_export")
     * @Template("@PlanCarrera/Backend/Poll/show.html.twig")
     */
    public function exportAction(Request $request, ModulePoll $poll)
    {
        $excelExporter = $this->get('excel_exporter');
        $form     = $request->get('form');
        $dateFrom = $form['date_from'];
        $dateTo   = $form['date_to'];
        $pollId   = $poll->getId();
        $doctrine = $this->getDoctrine();

        return $excelExporter->getResponseFromModulePoll(
            compact('dateFrom','dateTo','pollId','doctrine'),
            'Encuestas.xls'
        );
    }

    /**
     * @param ModulePoll $poll
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createExportDateForm(ModulePoll $poll)
    {
        $form = $this->createFormBuilder(null, [
            'action' => $this->generateUrl('module_poll_export', ['id' => $poll->getId()]),
            'method' => 'PUT'
        ])
            ->add('date_from', DateType::class, [
                'widget'   => 'single_text',
                'label'    => 'Desde',
                'required' => false
            ])
            ->add('date_to', DateType::class, [
                'widget'   => 'single_text',
                'label'    => 'Hasta',
                'required' => false,
            ])

            ->add('submit', SubmitType::class, ['label' => 'Exportar', 'attr' => ['class' => 'btn-primary pull-right']])
            ->getForm();

        return $form;
    }
}