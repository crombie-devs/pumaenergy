<?php

namespace Aper\PlanCarreraBundle\Controller\Backend;

use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Entity\ModuleExamQuestion;
use Aper\PlanCarreraBundle\Entity\ModulePoll;
use Doctrine\ORM\OptimisticLockException;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;


class AdminController extends BaseAdminController
{

    private $excelExporter;


    public function removeModuleExamQuestionEntity(ModuleExamQuestion $entity){
        $entity->setDeleted(true);
        try {
            $this->em->flush();
        } catch (OptimisticLockException $e) {
        }
    }

//    public function add_questionAction(){
//
//        return $this->redirectToRoute('easyadmin', array(
//            'action' => 'new',
//            'entity' => 'ModuleExamQuestion',
//        ));
//    }
//
//    public function addQuestionExamAction()
//    {
//        $redirect = ['action' => 'new',
//                     'entity' => 'ModuleExamQuestion'];
//
//        if($this->request->get('id')){
//            $redirect['examId'] = $this->request->get('id');
//        }
//        if($this->request->get('referer')){
//            $redirect['referer'] = $this->request->get('referer');
//        }
//
//        return $this->redirectToRoute('easyadmin', $redirect);
//    }

    protected function createEntityForm($entity, array $entityProperties, $view)
    {
        if (method_exists($this, $customMethodName = 'create'.$this->entity['name'].'EntityForm')) {
            $form = $this->{$customMethodName}($entity, $entityProperties, $view);
            if (!$form instanceof FormInterface) {
                throw new \UnexpectedValueException(sprintf(
                    'The "%s" method must return a FormInterface, "%s" given.',
                    $customMethodName, is_object($form) ? get_class($form) : gettype($form)
                ));
            }

            return $form;
        }

        $formBuilder = $this->executeDynamicMethod('create<EntityName>EntityFormBuilder', array($entity, $view));

        if($entity instanceof ModuleExamQuestion){

            $formBuilder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $form = $event->getForm();

                $form->add('exam', HiddenType::class, [
                    'read_only' => true,
                    'disabled'  => true,
                ]);

            });
            $formBuilder->get('enabled')->setData( true );
        }

        if (!$formBuilder instanceof FormBuilderInterface) {
            throw new \UnexpectedValueException(sprintf(
                'The "%s" method must return a FormBuilderInterface, "%s" given.',
                'createEntityForm', is_object($formBuilder) ? get_class($formBuilder) : gettype($formBuilder)
            ));
        }

        return $formBuilder->getForm();
    }

    public function listModuleExamAction(){
        if($this->request->query->get('sortField') == 'module'){
            $this->request->query->set('sortField', 'plan.title');
        }

        return parent::listAction();
    }

    public function persistEntity($entity)
    {
        if($entity instanceof ModuleExam){
            $entity->setName($entity->__toString());
        }
        if($entity instanceof ModuleExamQuestion){
            $moduleExam = $entity->getExam();
            $this->updateEntity($moduleExam);
        }
        parent::persistEntity($entity);
    }

    public function updateEntity($entity)
    {
        if($entity instanceof ModuleExam){
            $entity->setName($entity->__toString());
        }
        parent::updateEntity($entity);
        if($entity instanceof Module){
            if($entity->getExam()){
                $moduleExam = $entity->getExam();
//                if(!$entity->isPresencialExam()){
//                    $moduleExam->setMaxAttempts(2);
//                }
                // $moduleExam->setPresencial($entity->isPresencialExam());
                $this->updateEntity($moduleExam);
            }
            /*if($entity->getMultimedias()->count()){
                $positions = [];
                foreach ($entity->getMultimedias() as $multimedia){
                    $positions[$multimedia->getPosition()][$multimedia->getId()] = $multimedia->getId();
                }
                foreach ($entity->getMultimedias() as $multimedia)
                {
                    $position = $multimedia->getPosition();
                    if(count($positions[$position]) > 1)
                    {
                        $newPosition = max(array_keys($positions)) + 1;;
                        for($i=1; $i <= $entity->getMultimedias()->count(); $i++ )
                        {
                            $newPosition = $i;
                            if(!isset($positions[$newPosition])){
                                break;
                            }
                        }
                        $multimedia->setPosition($newPosition);
                        $this->updateEntity($multimedia);
                        $positions[$newPosition][$multimedia->getId()] = $multimedia->getId();
                        unset($positions[$position][$multimedia->getId()]);
                    }
                }
            }*/
        }
    }


    protected function createNewEntity()
    {
        $entity = parent::createNewEntity();
        if($entity instanceof ModuleExamQuestion){
            if($this->request->get('examId')){
                $exam = $this->em->getRepository(ModuleExam::class)->find($this->request->get('examId'));
                $entity->setExam($exam);
                $entity->setEnabled(true);
            }
        }
        if($entity instanceof ModuleExam){
            $entity->setEnabled(true);
        }
        if($entity instanceof ModulePoll){
            $entity->setEnabled(true);
        }
        if($entity instanceof Module){
            $plan = $this->getDoctrine()->getRepository('PlanCarreraBundle:CareerPlan')->find(1);
            $entity->setPlan($plan);
            $entity->setEnabled(true);
        }

        return $entity;
    }

    public function newModuleExamQuestionAction()
    {
        $response = parent::newAction();
        if ($response instanceof RedirectResponse) {
            $parts = parse_url($response->getTargetUrl());
            parse_str($parts['query'], $query);

            if($query['id']){
                /** @var ModuleExamQuestion $question */
                $question = $this->em->getRepository(ModuleExamQuestion::class)->find($query['id']);
                return $this->redirectToRoute('exam_show', [ 'id' => $question->getExam()->getId()]);
            }
        }

        return $response;
    }

    public function editModuleExamQuestionAction()
    {
        $response = parent::editAction();
        if ($response instanceof RedirectResponse) {
            $parts = parse_url($response->getTargetUrl());
            parse_str($parts['query'], $query);
            if($query['id']){
                /** @var ModuleExamQuestion $question */
                $question = $this->em->getRepository(ModuleExamQuestion::class)->find($query['id']);
                return $this->redirectToRoute('exam_show', [ 'id' => $question->getExam()->getId()]);
            }
        }

        return $response;
    }


    /**
     *
     */
    public function exportAction()
    {
        throw new \RuntimeException('Action for exporting an entity not defined');
    }


    public function exportEmployeeAction()
    {
        $this->excelExporter = $this->get('excel_exporter');

        $sortDirection = $this->request->query->get('sortDirection');
        if (empty($sortDirection) || !in_array(strtoupper($sortDirection), ['ASC', 'DESC'])) {
            $sortDirection = 'DESC';
        }
        $queryBuilder = $this->createListQueryBuilder(
            $this->entity['class'],
            $sortDirection,
            $this->request->query->get('sortField'),
            $this->entity['list']['dql_filter']
        );

        return $this->excelExporter->getResponseFromEmployeeQueryBuilder(
            $queryBuilder,
            'Empleados.xls'
        );

    }

}