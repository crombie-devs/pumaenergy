<?php

namespace Aper\PlanCarreraBundle\Controller\Backend;


use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Entity\ModuleExamAnswer;
use Aper\PlanCarreraBundle\Entity\ModuleExamQuestion;
use Aper\PlanCarreraBundle\Entity\ModuleExamQuestionCategory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExamController
 * @package Aper\PlanCarreraBundle\Controller\Backend
 *
 * @Route("/exam")
 */
class ExamController extends Controller
{

    /**
     * Muestra un examen
     *
     * @param Request $request
     * @param ModuleExam $exam
     * @return array
     *
     * @Route("/show/{id}", name="exam_show")
     * @Template("@PlanCarrera/Backend/Exam/show.html.twig")
     */
    public function showAction(Request $request, ModuleExam $exam)
    {
        $form     = $this->createExportDateForm($exam);
        $from     = $request->get('from') ?: null;
        $to       = $request->get('to')   ?: null;

        $module   = $exam->getModule();
        $plan     = $module->getPlan();
        $evaluated = $this->getDoctrine()->getManager()->getRepository(EmployeeModuleExam::class)->employeesEvaluated($exam, $from, $to);

        $approved = array_filter($evaluated, function (EmployeeModuleExam $employeeModuleExam){ return $employeeModuleExam->isApproved(); });
        $reproved = array_filter($evaluated, function (EmployeeModuleExam $employeeModuleExam){ return (!is_null($employeeModuleExam->isApproved()) and !$employeeModuleExam->isApproved()); });;

        $formImport = $this->createImportFileForm($exam);

        return compact('module','plan', 'exam') + ['export_data_form' => $form->createView(), 'evaluated' => count($evaluated), 'approved' => count($approved), 'reproved' => count($reproved), 'import_file' => $formImport->createView()];
    }

    /**
     * @param ModuleExam $exam
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createImportFileForm(ModuleExam $exam)
    {
        $form = $this->createFormBuilder([], [
            'csrf_protection' => false,
            'action'          => $this->generateUrl('module_exam_import', ['id' => $exam->getId()]),
            'method'          => 'PUT'
        ])
            ->add('file', FileType::class, ['label' => "Seleccione el archivo"])
            ->add('submit', SubmitType::class, ['label' => 'Importar', 'attr' => ['class' => 'btn-primary pull-right']])
            ->getForm();

        return $form;
    }

    /**
     * Muestra un examen
     *
     * @param EmployeeModuleExam $test
     * @return array
     *
     * @Route("/show/{id}/employee/{employee_id}", name="test_show")
     * @Template("@PlanCarrera/Backend/Exam/test.html.twig")
     */
    public function showTestAction(EmployeeModuleExam $test)
    {
        $employee = $test->getEmployee();
        $exam     = $test->getExam();
        $module   = $exam->getModule();
        $plan     = $module->getPlan();

        return compact('test', 'employee','module','plan', 'exam');
    }

    /**
     * Muestra una pregunta
     *
     * @param ModuleExamQuestion $question
     * @return array
     *
     * @Route("/question/show/{id}", name="question_show")
     * @Template("@PlanCarrera/Backend/Question/show.html.twig")
     */
    public function showQuestionAction(ModuleExamQuestion $question)
    {
        $exam     = $question->getExam();
        $module   = $exam->getModule();
//        $plan     = $module->getPlan();
        return compact('question', 'module');
    }


    /**
     * Exporta el detalle de una encuesta
     *
     * @param Request $request
     * @param ModuleExam $exam
     * @return array
     *
     * @Method("PUT")
     * @Route("/export/{id}", name="module_exam_export")
     * @Template("@PlanCarrera/Backend/Exam/show.html.twig")
     */
    public function exportAction(Request $request, ModuleExam $exam)
    {
        $excelExporter = $this->get('excel_exporter');
        $form     = $request->get('form');
        $dateFrom = $form['date_from'];
        $dateTo   = $form['date_to'];
        $examId   = $exam->getId();
        $doctrine = $this->getDoctrine();

        return $excelExporter->getResponseFromModuleExam(
            compact('dateFrom','dateTo','examId','doctrine'),
            'Examen.xls'
        );
    }

    /**
     * @param ModuleExam $exam
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createExportDateForm(ModuleExam $exam)
    {
        $form = $this->createFormBuilder(null, [
            'action' => $this->generateUrl('module_exam_export', ['id' => $exam->getId()]),
            'method' => 'PUT'
        ])
            ->add('date_from', DateType::class, [
                'widget'   => 'single_text',
                'label'    => 'Desde',
                'required' => false
            ])
            ->add('date_to', DateType::class, [
                'widget'   => 'single_text',
                'label'    => 'Hasta',
                'required' => false,
            ])

            ->add('submit', SubmitType::class, ['label' => 'Exportar', 'attr' => ['class' => 'btn-primary pull-right']])
            ->getForm();

        return $form;
    }

    /**
     *
     * @param Request $request
     * @param ModuleExam $exam
     *
     * @Method("PUT")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @Route("/import/{id}", name="module_exam_import")
     *
     */
    public function importAction(Request $request, ModuleExam $moduleExam){
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        $excelImporter = $this->get('excel_importer');
        $em            = $this->getDoctrine()->getManager();
        $data          = [];

        $form = $this->createImportFileForm($moduleExam);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var UploadedFile $file */
            $file = $form->get('file')->getData();
            $path = $file->getRealPath();
            $data = $excelImporter->importarExcelExamenes($path);
        }

        foreach ($data as $pregunta)
        {

            // print_r($pregunta);exit;
            $nro      = $moduleExam->getQuestions()->count() + 1;
            $question = new ModuleExamQuestion();
            $question->setIndexOf($nro);
            $question->setType('MULTCHOICE');
            $question->setText($pregunta['pregunta']);
            $question->setEnabled(true);

            $questionCategory = $this->getDoctrine()->getRepository(ModuleExamQuestionCategory::class)->findOneByName($pregunta['pregunta_categoria']);
            
            if(!isset($questionCategory)) { //si no existe, creo la categoría
                $questionCategory = new ModuleExamQuestionCategory();
                $questionCategory->setName($pregunta['pregunta_categoria']);
                // $questionCategory->setEnabled(true);
                $questionCategory->setDeleted(false);
                
                $em->persist($questionCategory);
                $aux = $em->flush();
            }

            $question->setCategory($questionCategory);

            if($pregunta['respuesta_correcta']){
                $answer = new ModuleExamAnswer();
                $answer->setText($pregunta['respuesta_correcta']);
                $answer->setCorrect(true);
                $question->addAnswer($answer);
            }
            if($pregunta['respuesta_incorrecta1']){
                $answer = new ModuleExamAnswer();
                $answer->setText($pregunta['respuesta_incorrecta1']);
                $answer->setCorrect(false);
                $question->addAnswer($answer);
            }
            if($pregunta['respuesta_incorrecta2']){
                $answer = new ModuleExamAnswer();
                $answer->setText($pregunta['respuesta_incorrecta2']);
                $answer->setCorrect(false);
                $question->addAnswer($answer);
            }
            if($pregunta['respuesta_incorrecta3']){
                $answer = new ModuleExamAnswer();
                $answer->setText($pregunta['respuesta_incorrecta3']);
                $answer->setCorrect(false);
                $question->addAnswer($answer);
            }

            $moduleExam->addQuestion($question);

            $em->persist($moduleExam);
            $em->flush();
        }

        $this->addFlash('success', 'Se importaron ' . count($data) . ' nuevas preguntas.');

        return $this->redirectToRoute('exam_show', ['id' => $moduleExam->getId()]);
    }
}