<?php

namespace Aper\PlanCarreraBundle\Manager;

use Aper\PlanCarreraBundle\Entity\ExternalTrainingInscription;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\ExternalTrainingIsComing;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ExternalTrainingsManager
 * @package Aper\PlanCarreraBundle\Manager
 */
class ExternalTrainingsManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repository;
    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->repository = $this->em->getRepository('PlanCarreraBundle:ExternalTrainingInscription');
    }

    /**
     * @return array|ExternalTrainingInscription[]
     */
    public function findInscriptionsForTomorrow()
    {
        $tomorrow = new \DateTime('tomorrow');
        $tomorrowStart = clone $tomorrow->setTime(0, 0);
        $tomorrowEnd   = clone $tomorrow->setTime(23, 59, 59);
        return $this->repository
            ->createQueryBuilder('ExternalTrainingInscription')
            ->innerJoin('ExternalTrainingInscription.training', 'ExternalTraining')
            ->andWhere('ExternalTraining.date >= :tomorrowStart')->setParameter('tomorrowStart', $tomorrowStart)
            ->andWhere('ExternalTraining.date <= :tomorrowEnd')->setParameter('tomorrowEnd', $tomorrowEnd)
            ->getQuery()->getResult();
    }

    /**
     * @param ExternalTrainingInscription $externalTrainingInscription
     */
    public function reminder(ExternalTrainingInscription $externalTrainingInscription)
    {
        $event = new ExternalTrainingIsComing($externalTrainingInscription->getEmployee(), $externalTrainingInscription->getTraining());
        $this->dispatcher->dispatch(CareerPlanEvents::EXTERNAL_TRAINING_INSCRIPTION_IS_COMING, $event);
    }

}