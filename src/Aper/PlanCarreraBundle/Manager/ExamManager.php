<?php

namespace Aper\PlanCarreraBundle\Manager;

use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

/**
 * Class ExamManager
 * @package Aper\PlanCarreraBundle\Manager
 */
class ExamManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('PlanCarreraBundle:EmployeeModuleExam');
    }

    /**
     * @return array|EmployeeModuleExam[]
     */
    public function findUnfinishedTests()
    {
        return $this->repository
            ->createQueryBuilder('EmployeeModuleExam')
            ->where('EmployeeModuleExam.finishedAt IS NULL')
            ->getQuery()->getResult();
    }

}