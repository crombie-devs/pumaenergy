<?php

namespace Aper\PlanCarreraBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class YouTubeResource
 * @package Aper\PlanCarreraBundle\Model
 * @ORM\Embeddable()
 */
class YouTubeResource
{
    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank(message="Este valor no es válido")
     */
    private $code;

    /**
     * YouTubeResource constructor.
     * @param string $code
     */
    private function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * @param string $uri Example: https://www.youtube.com/watch?v=4BNUyJl44m8
     * @return YouTubeResource
     * @throw \InvalidArgumentException
     */
    public static function fromUri($uri)
    {
        $code = self::extractCode($uri);
//        if (!$code) {
//            throw new \InvalidArgumentException();
//        }

        return new self($code);
    }

    /**
     * @param $url
     * @return null|string|string[]
     */
    public static function extractCode($url)
    {
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
            return $match[1];
        }

        return null;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        if (!$this->code) {
            return '';
        }

        $format = 'https://www.youtube.com/embed/';

        return sprintf('%s%s', $format, $this->code);
    }
}