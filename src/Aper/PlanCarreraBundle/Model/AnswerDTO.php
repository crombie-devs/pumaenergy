<?php

namespace Aper\PlanCarreraBundle\Model;

use Aper\PlanCarreraBundle\Entity\ModulePollAnswer;
use Aper\PlanCarreraBundle\Entity\ModulePollQuestion;

/**
 * Class AnswerDTO
 * @package Aper\PlanCarreraBundle\Model
 */
class AnswerDTO
{
    /**
     * @var ModulePollQuestion
     */
    private $question;
    /**
     * @var ModulePollAnswer
     */
    private $answer;
    /**
     * @var string
     */
    private $answerText;

    /**
     * AnswerDTO constructor.
     * @param ModulePollQuestion $question
     * @param ModulePollAnswer $answer
     * @param string $answerText
     */
    public function __construct(ModulePollQuestion $question, ModulePollAnswer $answer = null, $answerText = '')
    {
        $this->question = $question;
        $this->answer = $answer;
        $this->answerText = $answerText;
    }

    /**
     * @return int
     */
    public function getQuestionId()
    {
        return $this->question->getId();
    }

    /**
     * @return string
     */
    public function getQuestionText()
    {
        return $this->question->getText();
    }

    /**
     * @return string
     */
    public function getQuestionType()
    {
        return $this->question->getType();
    }

    /**
     * @return int
     */
    public function getAnswerId()
    {
        if ($this->question->getType() == 'MULTCHOICE') {
            return $this->answer->getId();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getAnswerText()
    {
        if ($this->question->getType() == 'MULTCHOICE') {
            return $this->answer->getText();
        }

        return $this->answerText;
    }
}