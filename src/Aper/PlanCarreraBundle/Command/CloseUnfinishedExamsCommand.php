<?php

namespace Aper\PlanCarreraBundle\Command;

use Aper\PlanCarreraBundle\Exception\ExamException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CloseUnfinishedExamsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('exams:close')
            ->setDescription('Finaliza y corrige exámenes que han sido abandonados y se les venció el tiempo para finalizarlos')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $testManager = $container->get('aper_plan_carrera.manager.exam_manager');

        $tests     = $testManager->findUnfinishedTests();

        $examiner = $container->get('examiner');

        $total = count($tests);
        $output->writeln('<info>' . $total . '</info> Exámenes que aún no han sido finalizados');

        $progress = $this->getHelper('progress');
        $progress->start($output, $total);
        $now = new \DateTime();
        $c   = 0;

        foreach ($tests as $test){

            if($test->getDueDate() < $now){
                try {
                    $examiner->finish($test->getExam(), $test->getEmployee());
                } catch (ExamException $e) {
                    $c++;
                } catch (NonUniqueResultException $e) {
                } catch (OptimisticLockException $e) {
                }
            }

            $progress->advance();
        }
        $progress->finish();

        $output->writeln("<info>Exámenes Finalizados automáticamente: {$c} de {$total}</info>");
        $output->writeln('<info>Finalizando</info>');

    }
}