<?php

namespace Aper\PlanCarreraBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateNotificationCommand
 * @package WebFactory\Bundle\NotificationBundle\Command
 */
class CreateNotificationCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('notification:reminders')
            ->setDescription('Crea notificaciones automáticamente')
        ;
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $externalTrainingsInscriptionsManager = $container->get('aper_plan_carrera.manager.external_trainings_manager');
        $externalTrainingsInscriptions = $externalTrainingsInscriptionsManager->findInscriptionsForTomorrow();

        $total = count($externalTrainingsInscriptions);
        $output->writeln('<info>' . $total . '</info> Empleados Inscriptos a Talleres que se dictarán mañana');

        $progress = $this->getHelper('progress');
        $progress->start($output, $total);

        $c   = 0;
        foreach ($externalTrainingsInscriptions as $externalTrainingInscription){

            try {
                $externalTrainingsInscriptionsManager->reminder($externalTrainingInscription);
                $c++;
            } catch (\Exception $e) {

            }
            $progress->advance();
        }
        $progress->finish();

        $output->writeln("<info>Recordatorios creados: {$c} de {$total}</info>");


        $output->writeln('<info>Finalizando</info>');

    }
}