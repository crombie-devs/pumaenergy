<?php

namespace Aper\PlanCarreraBundle\Form;

use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeCareerPlanType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plan',null,[
                "label" => "Plan de carrera",
                "required" => true
            ])
            ->add('user',null,[
                "required" => true
            ])
//            ->add("approved",null,[
//                "label" => "Aprobado [ se asignan todos los módulos del plan como aprobados ]"
//            ])
            ->add('save', SubmitType::class);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_plancarrerabundle_employeecareerplan';
    }


}
