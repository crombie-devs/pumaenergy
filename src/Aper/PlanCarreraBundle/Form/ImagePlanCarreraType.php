<?php

namespace Aper\PlanCarreraBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\FileBundle\Form\ImageType;

class ImagePlanCarreraType extends ImageType
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\PlanCarreraBundle\Entity\ImageCareerPlan'
        ));
    }

    public function getName()
    {
        return 'image';
    }
}
