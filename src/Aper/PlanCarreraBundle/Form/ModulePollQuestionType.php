<?php

namespace Aper\PlanCarreraBundle\Form;

use Aper\PlanCarreraBundle\Entity\ModulePollAnswer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModulePollQuestionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('Pregunta', FormType::class, ['inherit_data' => true, 'label' => 'Pregunta'])
        ->add('text','text',[
            'label' => "Texto pregunta"
        ])
            ->add('image', ModulePollQuestionImageType::class, ['label' => false,])
        ->add('type',ChoiceType::class, [
            'required' => true,
                'choices' => [
                    'MULTCHOICE' => 'Multiple choice',
                    'COMPLETAR' => 'Completar'
                ],
                'placeholder' => 'Seleccione el tipo de pregunta',
                'attr' => ['class' => 'tipoPregunta'],
            ]
        )
        ->add('answers',CollectionType::class, [
            'entry_type' => ModulePollAnswerType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
//            'widget_add_btn' => [
//                'label' => 'respuesta +',
//                'icon' => '',
//                'attr' => [
//                    'class' => 'btn btn-primary',
//                    'title' => 'RESPUESTA +',
//                ],
//            ],
            'options' => [
//                'widget_remove_btn' => [
//                    'label' => 'ELIMINAR RESPUESTA',
//                    'attr' => [
//                        'class' => 'btn btn-danger',
//                        'title' => 'ELIMINAR RESPUESTA',
//                    ],
//                    'icon' => '',
//                    'wrapper_div' => [
//                        'class' => 'span12',
//                    ],
//                ],
                'attr' => [
                    'class' => 'span12',
                ],
                'label' => false,
                'error_bubbling' => false,
                'show_child_legend' => false,
            ],
            'show_legend' => false,
            'show_child_legend' => false,

        ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();
            if ($data) {
                $form->add('Pregunta', FormType::class, [
                    'inherit_data' => true,
                    'label'        => '-- Pregunta #' . ($data->getPoll()->getQuestions()->indexOf($data)+1) . ' --'
                ]);
            }
        });
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\PlanCarreraBundle\Entity\ModulePollQuestion',
            'fix_label'=> 'col-sm-0',
            'fix_group' => 'col-sm-12'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_plancarrerabundle_modulepollquestion';
    }


}
