<?php

namespace Aper\PlanCarreraBundle\Form;

use Aper\StoreBundle\Entity\Store;
use Aper\UserBundle\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\EasyAdminAutocompleteType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExternalTrainingType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('trainer', EasyAdminAutocompleteType::class, [
                'class' => User::class,
                "label" => "Coordinador",
            ])
            ->add('stores', EasyAdminAutocompleteType::class, [
                'class' => Store::class,
                'multiple' => true,
            ])
            ->add('place', null, [
                "label" => "Lugar",
            ])
            ->add('name')
            ->add('quota', null, [
                "label" => "Cupo",
            ])
            ->add('date', DateTimeType::class, [
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
            ])
            ->add('duration', null, [
                "label" => "Duración"
            ])
            ->add('expositor', null, [
                "label" => "Expositor"
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            if (!$data) {
                return;
            }

            if ($data->getDate() > new \DateTime()) {
                return;
            }

            $event->getForm()->add('trainer', EasyAdminAutocompleteType::class, [
                    'class' => User::class,
                    "label" => "Coordinador",
                        'read_only' => true,
                        'disabled' => true,
                ])
                ->add('stores', EasyAdminAutocompleteType::class, [
                    'class' => Store::class,
                    'multiple' => true,
                    'read_only' => true,
                    'disabled' => true,
                ])
                ->add('place', null, [
                    "label" => "Lugar",
                    'read_only' => true,
                ])
                ->add('name', null, [
                    'read_only' => true,
                ])
                ->add('quota', null, [
                    "label" => "Cupo",
                    'read_only' => true,
                ])
                ->add('date', DateTimeType::class, [
                    'date_widget' => 'single_text',
                    'time_widget' => 'single_text',
                    'read_only' => true,
                    'disabled' => true,
                ])
                ->add('duration', null, [
                    'read_only' => true,
                ])
                ->add('expositor', null, [
                    'read_only' => true,
                ])
            ;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Aper\PlanCarreraBundle\Entity\ExternalTraining',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_plancarrerabundle_externaltraining';
    }


}
