<?php

namespace Aper\PlanCarreraBundle\Form;

use EasyCorp\Bundle\EasyAdminBundle\Form\Type\EasyAdminGroupType;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\EasyAdminSectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ModuleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $orden = [1,2,3,4,5,6,7,8,9,10];

        $builder
            ->add('xxxx', EasyAdminGroupType::class, [
                'label' => 'sarasa',
//                'css_class' => 'col-sm-6',
            'mapped' => false,
            ])

            ->add('title')
            ->add('description', TextareaType::class, [
                'attr' => [
                    'style' => 'min-height:64px',
                ]
            ])
//            ->add('xxxx2', EasyAdminGroupType::class, [
//                'label' => 'sarasa',
//                'css_class' => 'col-sm-6',
//            ])
            ->add('text', 'ckeditor', array(
                'config' => array(
                    'config_name' => 'my_config',
                    'height' => '250px',
                ),
            ))
            ->add('type',ChoiceType::class,array(
                'choices' => array(
                'BASIC' => 'Básico',
                'LEVELING' => 'De nivelación'
                ),
                'placeholder' => 'Seleccione el tipo de módulo')
            )
            ->add('presencial')
            ->add('required',null,[
                "label" => "Obligatorio"
            ])
            ->add('orden',ChoiceType::class,[
                    'choices' => $orden,
                    'placeholder' => 'Seleccione la prioridad',
                    "label" => "Prioridad"
                ]
            )
//            ->add('readingTime',null,[
//                "label" => "Tiempo lectura"
//            ])
//            ->add('plan',null,[
//                "required" => true
//            ])


            ->add('image')
            ->add('status')
//            ->add('image',ImageModuleType::class)
            ->add('multimedias', CollectionType::class, array(
                'entry_type' => MultimediaType::class,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
//                'xx'=> '0'
            ))
//            ->add('save', SubmitType::class)
        ;

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\PlanCarreraBundle\Entity\Module'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_plancarrerabundle_module';
    }


}
