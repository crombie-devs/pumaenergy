<?php

namespace Aper\PlanCarreraBundle\Form;

use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Repository\ModuleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModulePollModuleType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $builder
//            ->add('module');

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();

            if (!$data) {
                $form->add('module', EntityType::class, [
                    'class' => Module::class,
                    'query_builder' => function (ModuleRepository $repository) use ($data) {
                        return $repository->createQueryBuilder('Module')
                            ->where('Poll.id IS NULL')
                            ->leftJoin('Module.poll', 'Poll');
                    },
                ]);
            } else {
//                $form->add('module', EntityType::class, [
//                    'class' => Module::class,
////                    'read_only' => true,
//                    'query_builder' => function (ModuleRepository $repository) use ($data) {
//                        return $repository->createQueryBuilder('Module')
//                            ->where('Module.id = :module_id')
////                            ->where('Poll.id = :poll_id')
////                            ->leftJoin('Module.poll', 'Poll')
//                            ->setParameter('module_id', $data->getId());
//                    },
//                ]);
//                $form->remove('module');
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Module::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_plancarrerabundle_modulepoll';
    }

}
