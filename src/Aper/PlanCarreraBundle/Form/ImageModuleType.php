<?php

namespace Aper\PlanCarreraBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WebFactory\Bundle\FileBundle\Form\ImageType;
use WebFactory\Bundle\FileBundle\Form\Type\FilePreviewType;

class ImageModuleType extends ImageType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FilePreviewType::class, array(
                'required' => false,
                'allow_remove_file' => true,
            ))
        ;
        parent::buildForm($builder, $options);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\PlanCarreraBundle\Entity\ImageModule',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_plancarrerabundle_imagemodule';
    }


}
