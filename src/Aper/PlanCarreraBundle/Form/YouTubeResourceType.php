<?php

namespace Aper\PlanCarreraBundle\Form;

use Aper\PlanCarreraBundle\Model\YouTubeResource;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class YouTubeResourceType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url', UrlType::class, [
                'label' => 'Youtube',
            ])
            ->setDataMapper($this)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => YouTubeResource::class,
            'empty_data' => null,
            'error_mapping' => [
                '.' => 'url'
            ]
        ]);
    }

    public function getBlockPrefix()
    {
        return 'you_tube_resource';
    }

    /**
     * @param YouTubeResource $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        $forms['url']->setData($data ? $data->getUri() : null);
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param mixed $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);

        $data = null;
        if ($forms['url']->getData()) {
            try {
                $data =  YouTubeResource::fromUri($forms['url']->getData());
            } catch (\InvalidArgumentException $e) {
                $data = null;
            }
        }
    }
}
