<?php

namespace Aper\PlanCarreraBundle\Form;

use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Repository\ModuleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeCareerModuleType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('module')
            ->add('approved')
            ->add('enabled');

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            if (!$data) {
                return;
            }

            $form = $event->getForm();
            $form->add('module', EntityType::class, [
                'disabled' => true,
                'class' => Module::class,
                'query_builder' => function (ModuleRepository $repository) use ($data) {
                    return $repository->createQueryBuilder('Module')
                        ->where('Module.plan = :plan')
                        ->setParameter('plan', $data->getEmployeePlan()->getPlan());
                }
            ]);

        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => EmployeeCareerModule::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_plancarrerabundle_employeecareermodule';
    }


}
