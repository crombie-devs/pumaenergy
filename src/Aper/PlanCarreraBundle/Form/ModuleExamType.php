<?php

namespace Aper\PlanCarreraBundle\Form;

use Aper\PlanCarreraBundle\Entity\ModuleExamFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModuleExamType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('enabled')
            ->add('name', null, [
                "label" => "Nombre evaluación",
            ])
            ->add('maxQuestionCount', null, [
                "label" => "Cantidad preguntas a realizar",
            ])

            ->add('correctAnswersToApprove', null, [
                "label" => "Cantidad preguntas necesarias para aprobar",
            ])
            ->add('limitTimeInSeconds', null, [
                "label" => "Tiempo disponible en segundos",
            ])
            ->add('daysForRetry', null, [
                "label" => "Días para volver a hacerlo",
            ])

            ->add('questions', CollectionType::class, [
                'label' => 'Preguntas',
                'entry_type' => ModuleExamQuestionType::class,
                'allow_add' => true,
                'allow_delete' => false,
                'by_reference' => false,
                'widget_add_btn' => [
                    'label' => 'AGREGAR PREGUNTA',
                    'icon' => '',
                    'attr' => [
                        'class' => 'btn btn-primary',
                        'title' => 'OPCION +',
                    ],
                ],
                'options' => [
                    'widget_remove_btn' => [
                        'label' => 'ELIMINAR PREGUNTA',
                        'attr' => [
                            'class' => 'btn btn-danger',
                            'title' => 'ELIMINAR PREGUNTA',
                        ],
                        'icon' => '',
                        'wrapper_div' => [
                            'class' => 'span12',
                        ],
                    ],
                    'attr' => [
                        'class' => 'span12',
                    ],
                    'label' => false,
                    'error_bubbling' => false,
                    'show_child_legend' => false,
                ],
                'show_legend' => false,
                'show_child_legend' => false,
                'error_bubbling' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Aper\PlanCarreraBundle\Entity\ModuleExam',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'exam';
    }

}
