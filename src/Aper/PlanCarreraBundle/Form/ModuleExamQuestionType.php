<?php

namespace Aper\PlanCarreraBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModuleExamQuestionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('Pregunta', FormType::class, ['inherit_data' => true])
            ->add('enabled')
            ->add('indexOf',null, ['label' => 'Nro de Preg.'])
            ->add('text',null,[
                'label' => 'Texto pregunta'])
            ->add('type',ChoiceType::class, [
                    'required' => true,
                    'choices' => [
                        'MULTCHOICE' => 'Multiple choice',
//                        'COMPLETAR' => 'Completar'
                    ],
                    'placeholder' => 'Seleccione el tipo de pregunta',
                    'attr' => ['class' => 'tipoPregunta'],
                ]
            )
            ->add('file',MultimediaQuestionType::class)
            ->add('answers',CollectionType::class, [
                                            'label' => 'Opciones',
                                            'entry_type' => ModuleExamAnswerType::class,
                                            'allow_add' => true,
//'ads'=>false,
//                                            'label_attr' => ['class' => 'col-sm-2 blablabla'],
//                                            'widget_form_group_attr' => ['class' => 'col-sm-10'],
                                            'allow_delete' => false,
                                            'by_reference' => false,
                                            'widget_add_btn' => [
                                                'label' => 'opción +',
                                                'icon' => '',
                                                'attr' => [
                                                    'class' => 'btn btn-primary',
                                                    'title' => 'OPCIONES +',
                                                ],
                                            ],
                                            'options' => [
                                                'widget_remove_btn' => [
                                                    'label' => 'ELIMINAR OPCIÓN',
                                                    'attr' => [
                                                        'class' => 'btn btn-danger',
                                                        'title' => 'ELIMINAR OPCIÓN',
                                                    ],
                                                    'icon' => '',
                                                    'wrapper_div' => [
                                                        'class' => 'span12',
                                                    ],
                                                ],
                                                'attr' => [
                                                    'class' => 'span12',
                                                ],
                                                'label' => false,
                                                'error_bubbling' => false,
                                                'show_child_legend' => false,
                                            ],
                                            'show_legend' => false,
                                            'show_child_legend' => false,

                                        ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();
            if ($data) {
                $form->add('Pregunta', FormType::class, [
                    'inherit_data' => true,
                    'label'        => 'Pregunta #' . ($data->getIndexOf())
                ]);
            }
        });

        $builder->get('enabled')->setData( true );
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\PlanCarreraBundle\Entity\ModuleExamQuestion',
            'attr'       => ['class' => 'panel panel-default'],
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_plancarrerabundle_moduleexamquestion';
    }


}
