<?php

namespace Aper\PlanCarreraBundle\Form;

use Aper\PlanCarreraBundle\Entity\PlanRequirement;
use Aper\PlanCarreraBundle\Entity\PlanRequirementsEmployee;
use Aper\PlanCarreraBundle\Repository\PlanRequirementRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlanRequirementsEmployeeType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('requirement')
            ->add('approved')
            ->add('enabled');

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            if (!$data) {
                return;
            }

            $form = $event->getForm();
            $form->add('requirement', EntityType::class, [
                'disabled' => true,
                'class' => PlanRequirement::class,
                'query_builder' => function (PlanRequirementRepository $repository) use ($data) {
                    return $repository->createQueryBuilder('Requirement')
                        ->where('Requirement.plan = :plan')
                        ->setParameter('plan', $data->getEmployeeCareerPlan()->getPlan());
                }
            ]);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => PlanRequirementsEmployee::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_plancarrerabundle_planrequirementsemployee';
    }


}
