<?php
namespace Aper\PlanCarreraBundle\Form;

use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Doctrine\ORM\EntityRepository;
use Aper\PlanCarreraBundle\Entity\Module;

class AddModuleFieldSubscriber implements EventSubscriberInterface
{
    private $propertyPathToModule;

    public function __construct($propertyPathToModule)
    {
        $this->propertyPathToModule = $propertyPathToModule;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT   => 'preSubmit'
        );
    }

    private function addModuleForm($form, $module = null)
    {
        $formOptions = array(
            'class'         => 'Aper\PlanCarreraBundle\Entity\Module',
            'mapped'        => false,
            'label'         => 'Módulos',
            'empty_value'   => 'Módulos',
            'attr'          => array(
                'class' => 'country_selector',
            ),
        );

        if ($module) {
            $formOptions['data'] = $module;
        }

        $form->add('modules', CollectionType::class, [
            "type" => EmployeeCareerModuleType::class
        ]);
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $accessor = PropertyAccess::getPropertyAccessor();

        $module    = $accessor->getValue($data, $this->propertyPathToModule);
        $plan = ($module) ? $module : null;

        $this->addModuleForm($form, $plan);
    }

    public function preSubmit(FormEvent $event)
    {
        $form = $event->getForm();

        $this->addModuleForm($form);
    }
}