<?php

namespace Aper\PlanCarreraBundle\Form;

use Aper\PlanCarreraBundle\Entity\CareerPlan;
use Aper\PlanCarreraBundle\Entity\ModuleExamFile;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModuleExamTwoType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('module', 'entity', [
            'class' => 'PlanCarreraBundle:Module',
            'placeholder' => 'Seleccione un módulo ...',
            "label" => "Módulo",
            "required" => false
        ])

        ->add('name',null,[
                "label" => "Nombre evaluación"
            ])
            ->add('questionsAmount',null,[
                "label" => "Cantidad preguntas"
            ])
            ->add('examFile', ModuleExamFileType::class,[])

            ->add('time',null,[
                "label" => "Tiempo disponible"
            ])
            ->add('approval',null,[
                "label" => "Porcentaje aprobación"
            ])
            ->add('status',null,[
                "label" => "Activo"
            ])
            ->add('questions',CollectionType::class, [
                'label' => 'Preguntas',
                'entry_type' => ModuleExamQuestionType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'widget_add_btn' => [
                    'label' => 'AGREGAR PREGUNTA',
                    'icon' => '',
                    'attr' => [
                        'class' => 'btn btn-primary',
                        'title' => 'OPCION +',
                    ],
                ],
                'options' => [
                    'widget_remove_btn' => [
                        'label' => 'ELIMINAR PREGUNTA',
                        'attr' => [
                            'class' => 'btn btn-danger',
                            'title' => 'ELIMINAR PREGUNTA',
                        ],
                        'icon' => '',
                        'wrapper_div' => [
                            'class' => 'span12',
                        ],
                    ],
                    'attr' => [
                        'class' => 'span12',
                    ],
                    'label' => false,
                    'error_bubbling' => false,
                    'show_child_legend' => false,
                ],
                'show_legend' => false,
                'show_child_legend' => false,
            ]);
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\PlanCarreraBundle\Entity\ModuleExam'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_plancarrerabundle_moduleexam';
    }

}
