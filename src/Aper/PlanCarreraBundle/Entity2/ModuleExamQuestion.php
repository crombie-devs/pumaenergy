<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * ModuleExamQuestion
 *
 * @ORM\Table(name="career_plan_module_exam_question")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\ModuleExamQuestionRepository")
 */
class ModuleExamQuestion
{
    use EnabledTrait;
    use DeletedTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $indexOf;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     * @Assert\NotBlank()
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @var ModuleExam
     * @ORM\ManyToOne(targetEntity="ModuleExam", inversedBy="questions", cascade={"persist"})
     * @ORM\JoinColumn(name="exam_id", referencedColumnName="id")
     */
    private $exam;

    /**
     * @var ModuleExamAnswer[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="ModuleExamAnswer", mappedBy="question", cascade={"all"})
     * @Assert\Valid()
     */
    private $answers;

    /**
     * @ORM\OneToOne(targetEntity="MultimediaQuestion", mappedBy="question", cascade={"all"})
     * @Assert\Valid()
     */
    private $file;

    /**
     * @var ModuleExamQuestionCategory
     * @ORM\ManyToOne(targetEntity="ModuleExamQuestionCategory", inversedBy="category", cascade={"persist"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */   
    private $category;
    
    /**
     * ModuleExamQuestion constructor.
     */
    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->text;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param $text
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @param ModuleExamAnswer $answer
     */
    public function addAnswer(ModuleExamAnswer $answer)
    {
        $answer->setQuestion($this);
        $this->answers[] = $answer;
    }

    /**
     * @param ModuleExamAnswer $answer
     */
    public function removeAnswer(ModuleExamAnswer $answer)
    {
//        $this->answers->removeElement($answer);
        $answer->setDeleted(true);
    }

    /**
     * @return ModuleExamAnswer[]|ArrayCollection
     */
    public function getAnswers()
    {
//        return $this->answers;
        $answers = $this->answers->filter(function(ModuleExamAnswer $a) { return ($a->isDeleted() === false);});
        $return = new ArrayCollection();
        foreach ($answers as $answer){
            $return->add($answer);
        }
        return $return;
    }

    /**
     * @param Collection $answers
     * @return $this
     */
    public function setAnswers(Collection $answers)
    {
        $this->answers = $answers;

        return $this;
    }

    /**
     * @return ModuleExam
     */
    public function getExam()
    {
        return $this->exam;
    }


    /**
     * @param ModuleExam $exam
     */
    public function setExam(ModuleExam $exam)
    {
        $this->exam = $exam;
    }

    /**
     * @return MultimediaQuestion
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param MultimediaQuestion $multimedia
     */
    public function setFile(MultimediaQuestion $multimedia)
    {
        $multimedia->setQuestion($this);
        $this->file = $multimedia;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param int $indexOf
     */
    public function setIndexOf($indexOf)
    {
        $this->indexOf = $indexOf;
    }

    public function getIndexOf(){
        return $this->indexOf;
    }

    /**
     * @return ModuleExamQuestionCategory
     */
    public function getCategory()
    {
        return $this->category;
    }


    /**
     * @param ModuleExamQuestionCategory $category
     */
    public function setCategory(ModuleExamQuestionCategory $category)
    {
        $this->category = $category;
    }


    /**
     * @Assert\Callback()
     */
    public function validateAnswersCount(ExecutionContextInterface $context)
    {
        if(!$this->isDeleted()){
            if($this->answers->isEmpty()){
                $context->buildViolation('La pregunta debe tener al menos 1 opción de respuesta')
                    ->atPath('answers')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     */
    public function validateAnswersCorrectCount(ExecutionContextInterface $context)
    {
        if(!$this->isDeleted()){
            $correct = 0;
            foreach ($this->answers as $answer){
                if ($answer->isCorrect()){
                    $correct++;
                }
            }
            if($correct < 1){
                $context->buildViolation('La pregunta debe tener al menos 1 respuesta correcta')
                    ->atPath('answers')
                    ->addViolation();
            }
        }
    }
}

