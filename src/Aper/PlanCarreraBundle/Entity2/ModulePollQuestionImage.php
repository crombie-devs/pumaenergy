<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\FileBundle\Model\Image;

/**
 * @ORM\Entity
 * @ORM\Table(name="module_poll_question_images")
 */
class ModulePollQuestionImage extends Image
{
    const DIRECTORY = 'uploads/poll_question/images';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var UploadedFile
     * @Assert\Image(
     *     minWidth = 20,
     *     maxWidth = 4000,
     *     minHeight = 20,
     *     maxHeight = 4000,
     * )
     */
    protected $file;

    /**
     * @ORM\OneToOne(targetEntity="ModulePollQuestion", inversedBy="image")
     * @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     */
    private $question;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ModulePollQuestion
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param ModulePollQuestion $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

}