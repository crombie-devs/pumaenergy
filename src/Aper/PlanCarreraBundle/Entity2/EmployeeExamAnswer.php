<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * EmployeeExamAnswer
 *
 * @ORM\Table(name="career_plan_employee_exam_answer")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\EmployeeExamAnswerRepository")
 */
class EmployeeExamAnswer
{
    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ModuleExamQuestion
     * @ORM\ManyToOne(targetEntity="ModuleExamQuestion", cascade={"persist"})
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;

    /**
     * @var ModuleExamAnswer
     * @ORM\ManyToOne(targetEntity="ModuleExamAnswer", cascade={"persist"})
     * @ORM\JoinColumn(name="answer_id", referencedColumnName="id")
     */
    private $answer;

    /**
     * @var EmployeeModuleExam
     * @ORM\ManyToOne(targetEntity="Aper\PlanCarreraBundle\Entity\EmployeeModuleExam", cascade={"persist"}, inversedBy="answers")
     * @ORM\JoinColumn(name="employee_exam_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $employeeExam;

    /**
     * @var string
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param ModuleExamQuestion $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return ModuleExamQuestion
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param ModuleExamAnswer $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return ModuleExamAnswer
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    public function setEmployeeExam($exam)
    {
        $this->employeeExam = $exam;

        return $this;
    }

    public function getEmployeeExam()
    {
        return $this->employeeExam;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

}

