<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * ExamPermission
 * Este es el permiso que va a tener el empleado para poder realizar el examen del módulo.
 *
 * @ORM\Table(name="career_plan_exam_permission")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\ExamPermissionRepository")
 */
class ExamPermission
{
    use EnabledTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var EmployeeCareerModule
     * @ORM\ManyToOne(targetEntity="EmployeeCareerModule", inversedBy="examPermissions")
     * @ORM\JoinColumn(name="employee_career_module_id", referencedColumnName="id")
     */
    private $employeeModule;

    /**
     * @var EmployeeModuleExam[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="EmployeeModuleExam", mappedBy="examPermission")
     * @ORM\JoinColumn(name="exam_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $employeeExams;

    /**
     * @var ModuleExam
     * @ORM\ManyToOne(targetEntity="ModuleExam", cascade={"persist"})
     * @ORM\JoinColumn(name="exam_id", referencedColumnName="id")
     */
    private $moduleExam;

    /**
     * @var bool
     *
     * @ORM\Column(name="previoPost", type="boolean")
     */
    private $previoPost = false;

    /**
     * ExamPermission constructor.
     * @param EmployeeCareerModule|null $employeeModule
     * @param ModuleExam|null $moduleExam
     */
    public function __construct(EmployeeCareerModule $employeeModule = null, ModuleExam $moduleExam = null, $previoPost)
    {
        $this->employeeModule = $employeeModule;
        $this->moduleExam     = $moduleExam;
        $this->employeeExams  = [];
        $this->previoPost = $previoPost;
    }

    /**
     * Función que devuelve el próximo paso permitido que tiene el empleado con el examen del módulo.
     *
     * @return string
     */
    public function getNextStep()
    {

        if ($this->getPrevioPost() === false) {

            if ($this->employeeExams->count()) {
                // si hizo el examen prevo
                return 'completed';

            } else {

                return 'start';
            }

        } else {

            if ($this->isEnabled() === false) {
                //en caso que le hayan quitado el permiso del examen
                return 'none';
            }
            if ($this->getModuleExam()->isPresencial()) {

                if (!$this->employeeExams->count()) {
                    // si aún no hizo ningún examen
                    return 'workshop';
                }

                $lastExam = $this->employeeExams->last();

                if ($lastExam->isApproved()) {
                    return 'completed';
                } else {
                    return 'forbidden';
                }
            }
            if (!$this->employeeExams->count()) {
                // si aún no hizo ningún examen
                return 'start';
            }

            // obtengo el último examen que hizo el empleado
            $lastExam = $this->employeeExams->last();

            if ($lastExam->isApproved()) {
                return 'completed';
            }

            // si llega aca es porque el último examen está no-aprobado o no-evaluado
            $now = new \DateTime();
            if (is_null($lastExam->isApproved())) {

                // el examen no-evaluado puede o continuarlo, o finalizarlo, ninguna otra acción puede hacer el empleado con ese examen.
                if (!$lastExam->isTimeOut()) {
                    if ($lastExam->isCompleted()) {
                        return 'finish';
                    } else {
                        return 'continue';
                    }
                } else {
                    return 'finish';
                }
            }


            // si llega aca es porque el último examen está no-aprobado

            // si ya hizo los 2 exámenes, entonces no puede volver a hacer ninguno mas
            if ($this->employeeExams->count() >= $this->getModuleExam()->getMaxAttempts()) {
                return 'forbidden';
            }

            // obtengo la próxima chance
            $nextChance = $lastExam->getNextChanceDatetime();

            // si el método devuelve false es porque ya agotó las 2 chances que tenía. no puede hacer mas el examen.
            if (false === $nextChance) {
                return 'forbidden';
            }

            // si aún no pasó el tiempo de espera hasta la próxima chance, entonces tiene que esperar
            if ($now <= $nextChance) {
                return 'wait';
            }

            // si llega aca es porque puede comenzar un nuevo examen.
            return 'start';
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set employeeModule
     *
     * @param EmployeeCareerModule $employeeModule
     */
    public function setEmployeeModule($employeeModule)
    {
        $this->employeeModule = $employeeModule;
    }

    /**
     * @return EmployeeCareerModule
     */
    public function getEmployeeModule()
    {
        return $this->employeeModule;
    }

    /**
     * @param EmployeeModuleExam $exam
     */
    public function addEmployeeExam($exam)
    {
        $this->employeeExams[] = $exam;
    }

    /**
     * @return EmployeeModuleExam[]|ArrayCollection
     */
    public function getEmployeeExams()
    {
        return $this->employeeExams;
    }

    /**
     * @param $moduleExam
     */
    public function setModuleExam($moduleExam)
    {
        $this->moduleExam = $moduleExam;
    }

    /**
     * @return ModuleExam
     */
    public function getModuleExam()
    {
        return $this->moduleExam;
    }

    public function getPrevioPost(){
        return $this->previoPost;
    }

    public function setPrevioPost($previoPost){
        $this->previoPost = $previoPost;
        return $this;
    }
}

