<?php

namespace Aper\PlanCarreraBundle\Entity;

use Aper\PlanCarreraBundle\Model\YouTubeResource;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use PhpParser\Node\Expr\AssignOp\Mul;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Module
 *
 * @ORM\Table(name="career_plan_module")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\ModuleRepository")
 */
class Module
{
    use EnabledTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     * @Assert\NotBlank()
     * @Assert\Choice(callback="getTypes")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="Multimedia", mappedBy="module", cascade={"all"})
     * @Assert\Valid()
     */
    private $multimedias;

    /**
     * @var ImageModule
     * @ORM\OneToOne(targetEntity="ImageModule", mappedBy="module", cascade={"all"})
     * @Assert\Valid()
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="CareerPlan", inversedBy="modules")
     * @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     * @Assert\NotNull()
     * @Gedmo\SortableGroup()
     */
    private $plan;

    /**
     * @var bool
     *
     * @ORM\Column(name="nivelation", type="boolean")
     * @Assert\NotNull()
     */
    private $nivelation = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="presencial", type="boolean")
     * @Assert\NotNull()
     */
    private $presencial = false;

    /**
     * @ORM\OneToMany(targetEntity="ExternalTraining", mappedBy="module", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $externalTrainings;

    /**
     * @var bool
     *
     * @ORM\Column(name="presencialExam", type="boolean")
     * @Assert\NotNull()
     */
    private $presencialExam = false;

    /**
     * @var int
     * @ORM\Column(name="orden", type="integer")
     * @Gedmo\SortablePosition()
     * @Assert\NotBlank()
     * @Assert\Range(min="1")
     */
    private $orden;

    /**
     * @var int
     *
     * @ORM\Column(name="readingTime", type="integer", nullable=true)
     */
    private $readingTime;

    /**
     * @ORM\Column(name="id_pixelnet", type="integer", nullable=true)
     */
    private $idPixelnet;

    /**
     * @ORM\OneToOne(targetEntity="ModulePoll", mappedBy="module", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $poll;

    /**
     * @var ModuleExam
     * @ORM\OneToOne(targetEntity="ModuleExam", mappedBy="module", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $exam;

    /**
     * @var YouTubeResource
     * @ORM\Embedded(class="Aper\PlanCarreraBundle\Model\YouTubeResource", columnPrefix="you_tube_resource_")
     * @Assert\Valid()
     */
    private $youTubeResource;

    /**
     * Module constructor.
     */
    public function __construct()
    {
        $this->multimedias = new ArrayCollection();
        $this->externalTrainings = new ArrayCollection();
        $this->exam = null;
        //$this->setExam(new ModuleExam());
    }


    public function __toString() {
        return sprintf('%s', $this->title);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPixelnet
     *
     * @param integer $idPixelnet
     *
     * @return Module
     */
    public function setIdPixelnet($idPixelnet){
        $this->idPixelnet = $idPixelnet;

        return $this;
    }

    /**
     * Get idPixelnet
     *
     * @return integer
     */
    public function getIdPixelnet(){
        return $this->idPixelnet;
    }


    /**
     * Set title
     *
     * @param string $title
     *
     * @return Module
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Module
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Module
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function setMultimedias($multimedia)
    {
        $this->multimedias = $multimedia;

        return $this;
    }

    /**
     * @return ArrayCollection|Multimedia[]
     */
    public function getMultimedias()
    {
        $multimedias = $this->multimedias->filter(function(Multimedia $m){ return (false === $m->isDeleted());});
        $return = new ArrayCollection();
        foreach ($multimedias as $multimedia){
            $return->add($multimedia);
        }
        $items = $return->toArray();
        usort($items, function (Multimedia $item1, Multimedia $item2)
        {
            if ($item1->getPosition() == $item2->getPosition()) return 0;
            return $item1->getPosition() < $item2->getPosition() ? -1 : 1;
        });
        return $items;
    }

    public function addMultimedia(Multimedia $multimedia)
    {
        $multimedia->setModule($this);
        $this->multimedias->add($multimedia);
    }

    public function removeMultimedia(Multimedia $multimedia)
    {
        $multimedia->setDeleted(true);
        //$this->multimedias->removeElement($multimedia);
    }

    /**
     * Set image
     *
     * @param ImageModule $image
     *
     * @return Module
     */
    public function setImage(ImageModule $image)
    {
        $image->setModule($this);
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return ImageModule
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set plan
     *
     * @param string $plan
     *
     * @return Module
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return CareerPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set nivelation
     *
     * @param boolean $nivelation
     *
     * @return Module
     */
    public function setNivelation($nivelation)
    {
        $this->nivelation = $nivelation;

        return $this;
    }

    /**
     * Get nivelation
     *
     * @return bool
     * @deprecated
     */
    public function getNivelation()
    {
        return $this->isNivelation();
    }

    /**
     * Get nivelation
     *
     * @return bool
     */
    public function isNivelation()
    {
        return $this->nivelation;
    }


    public function setPresencial($presencial)
    {
        $this->presencial = $presencial;

        return $this;
    }

    /**
     * Get Presencial
     *
     * @return bool
     */
    public function getPresencial()
    {
        return $this->presencial;
    }

    /**
     * @return bool
     */
    public function isPresencialExam()
    {
        return $this->presencialExam;
    }

    /**
     * @param bool $presencialExam
     */
    public function setPresencialExam($presencialExam)
    {
        $this->presencialExam = $presencialExam;
    }


    public function setExternalTrainings(\Doctrine\Common\Collections\Collection $trainings)
    {
        $this->externalTrainings = $trainings;

        return $this;
    }

    /**
     * @return ArrayCollection|ExternalTraining[]
     */
    public function getExternalTrainings()
    {
        return $this->externalTrainings;
    }

    public function addExternalTraining(ExternalTraining $externalTraining)
    {
        $externalTraining->setModule($this);
        $this->externalTrainings->add($externalTraining);
    }

    public function removeExternalTraining(ExternalTraining $externalTraining)
    {
        $this->externalTrainings->removeElement($externalTraining);
    }

    /**
     * @param \DateTime $dateTime
     * @return ExternalTraining
     */
    public function getNextExternalTraining(\DateTime $dateTime)
    {
        $nextExternalTrainings = $this->externalTrainings->filter(function (ExternalTraining $externalTraining) use ($dateTime) {
            return $externalTraining->getDate() > $dateTime;
        });

        return $nextExternalTrainings->first() ?: null;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return Module
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return int
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set readingTime
     *
     * @param integer $readingTime
     *
     * @return Module
     */
    public function setReadingTime($readingTime)
    {
        $this->readingTime = $readingTime;

        return $this;
    }

    /**
     * Get readingTime
     *
     * @return int
     */
    public function getReadingTime()
    {
        return $this->readingTime;
    }

    public function setPoll(ModulePoll $poll)
    {
        $poll->setModule($this);
        $this->poll = $poll;

        return $this;
    }

    /**
     * @return ModulePoll
     */
    public function getPoll()
    {
        return $this->poll;
    }

    public function setExam(ModuleExam $exam)
    {
        $exam->setModule($this);
        $this->exam = $exam;

        return $this;
    }

    /**
     * @return ModuleExam
     */
    public function getExam()
    {
        return $this->exam;
    }

    /**
     * @return YouTubeResource
     */
    public function getYouTubeResource()
    {
        return $this->youTubeResource;
    }

    /**
     * @param YouTubeResource $youTubeResource
     */
    public function setYouTubeResource($youTubeResource)
    {
        $this->youTubeResource = $youTubeResource;
    }

    public static function getTypes()
    {
        return array('module', 'workshop');
    }

    /**
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function validateImageOrVideo(ExecutionContextInterface $context)
    {
        if ($this->youTubeResource) {
            return;
        }

        if (!$this->image) {
            return;
        }

        if (!$this->image->getFilePath() && !is_object($this->image->getFile())) {
            $context->addViolationAt('image.file', 'web_factory_file.validations.file.required', array(), null);
        } elseif ($this->image->getRemoveFile() && !is_object($this->image->getFile())) {
            $context->addViolationAt('image.file', 'web_factory_file.validations.file.required', array(), null);
        }
    }
}