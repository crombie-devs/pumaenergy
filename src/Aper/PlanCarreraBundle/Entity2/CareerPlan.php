<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PlanCarrera
 *
 * @ORM\Table(name="career_plan")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\CareerPlanRepository")
 * @UniqueEntity(fields={"title"}, errorPath="title")
 */
class CareerPlan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @var CareerPlanBigActiveImage
     * @ORM\OneToOne(targetEntity="CareerPlanBigActiveImage", mappedBy="plan", cascade={"all"})
     * @Assert\Valid()
     */
    private $bigActiveImage;

    /**
     * @var CareerPlanBigInactiveImage
     * @ORM\OneToOne(targetEntity="CareerPlanBigInactiveImage", mappedBy="plan", cascade={"all"})
     * @Assert\Valid()
     */
    private $bigInactiveImage;

    /**
     * @var CareerPlanTinyActiveImage
     * @ORM\OneToOne(targetEntity="CareerPlanTinyActiveImage", mappedBy="plan", cascade={"all"})
     * @Assert\Valid()
     */
    private $tinyActiveImage;

    /**
     * @var CareerPlanTinyInactiveImage
     * @ORM\OneToOne(targetEntity="CareerPlanTinyInactiveImage", mappedBy="plan", cascade={"all"})
     * @Assert\Valid()
     */
    private $tinyInactiveImage;

    /**
     * @var int
     *
     * @Gedmo\SortablePosition()
     * @ORM\Column(name="position", type="smallint", nullable=true)
     * @Assert\NotBlank()
     * @Gedmo\SortablePosition()
     */
    private $position;

    /**
     * @var PlanRequirement[]
     *
     * @ORM\OneToMany(targetEntity="PlanRequirement", mappedBy="plan", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $requirements;

    /**
     * @ORM\OneToMany(targetEntity="Module", mappedBy="plan", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"orden": "asc"})
     */
    private $modules;

    /**
     * CareerPlan constructor.
     */
    public function __construct()
    {
        $this->modules = new ArrayCollection();
        $this->requirements = new ArrayCollection();
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function addModule(Module $module)
    {
        $module->setPlan($this);
        $this->modules[] = $module;
    }

    public function removeModule(Module $module)
    {
        $this->modules->removeElement($module);
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getEnabledRequirements()
    {
        return $this->getRequirements();
    }

    /**
     * Get position
     *
     * @return PlanRequirement[] requirements
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * Set requirements
     *
     * @param PlanRequirement $reqs
     */
    public function setRequirements($reqs)
    {
        $this->requirements = $reqs;
    }

    public function addRequirement(PlanRequirement $requirement)
    {
        $requirement->setPlan($this);
        $this->requirements->add($requirement);
    }

    public function removeRequirement(PlanRequirement $requirement)
    {
        $this->requirements->removeElement($requirement);
    }

//    public function getPublicDetails()
//    {
//        return [
//            "id" => $this->getId(),
//            "title" => $this->getTitle(),
//            "description" => $this->getDescription()
//        ];
//    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CareerPlan
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CareerPlan
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return ArrayCollection|Collection|Module[]
     */
    public function getEnabledModules()
    {
        return $this->getModules()->filter(function (Module $entry) {
                return $entry->isEnabled();
            });
    }

    /**
     * @return ArrayCollection|Module[]
     */
    public function getModules()
    {
        return $this->modules;
    }

    public function setModules(Collection $module)
    {
        $this->modules = $module;

        return $this;
    }

    /**
     * @return CareerPlanBigActiveImage
     */
    public function getBigActiveImage()
    {
        return $this->bigActiveImage;
    }

    /**
     * @param CareerPlanBigActiveImage $bigActiveImage
     */
    public function setBigActiveImage(CareerPlanBigActiveImage $bigActiveImage)
    {
        $bigActiveImage->setPlan($this);
        $this->bigActiveImage = $bigActiveImage;
    }

    /**
     * @return CareerPlanBigInactiveImage
     */
    public function getBigInactiveImage()
    {
        return $this->bigInactiveImage;
    }

    /**
     * @param CareerPlanBigInactiveImage $bigInactiveImage
     */
    public function setBigInactiveImage(CareerPlanBigInactiveImage $bigInactiveImage)
    {
        $bigInactiveImage->setPlan($this);
        $this->bigInactiveImage = $bigInactiveImage;
    }

    /**
     * @return CareerPlanTinyActiveImage
     */
    public function getTinyActiveImage()
    {
        return $this->tinyActiveImage;
    }

    /**
     * @param CareerPlanTinyActiveImage $tinyActiveImage
     */
    public function setTinyActiveImage(CareerPlanTinyActiveImage $tinyActiveImage)
    {
        $tinyActiveImage->setPlan($this);
        $this->tinyActiveImage = $tinyActiveImage;
    }

    /**
     * @return CareerPlanTinyInactiveImage
     */
    public function getTinyInactiveImage()
    {
        return $this->tinyInactiveImage;
    }

    /**
     * @param CareerPlanTinyInactiveImage $tinyInactiveImage
     */
    public function setTinyInactiveImage(CareerPlanTinyInactiveImage $tinyInactiveImage)
    {
        $tinyInactiveImage->setPlan($this);
        $this->tinyInactiveImage = $tinyInactiveImage;
    }

}

