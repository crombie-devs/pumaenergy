<?php

namespace Aper\PlanCarreraBundle\Entity;

use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * EmployeeCareerPlan
 *
 * @ORM\Table(name="career_plan_employee")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\EmployeeCareerPlanRepository")
 * @UniqueEntity(fields={"user", "plan"})
 * @ORM\HasLifecycleCallbacks()
 */
class EmployeeCareerPlan
{
    use TimestampableEntity;
    use ApprovedTrait;
    use EnabledTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\Employee",inversedBy="careerPlans")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @var CareerPlan
     * @ORM\ManyToOne(targetEntity="CareerPlan")
     * @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $plan;

    /**
     * @var EmployeeCareerModule[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="EmployeeCareerModule",mappedBy="employee_plan", cascade={"all"}, orphanRemoval=true)
     */
    private $modules;

    /**
     * @var PlanRequirementsEmployee[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Aper\PlanCarreraBundle\Entity\PlanRequirementsEmployee",mappedBy="employeeCareerPlan", cascade={"all"}, orphanRemoval=true)
     */
    private $requirements;

    /**
     * @var User
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\User")
     */
    protected $createdBy;

    /**
     * @var User
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\User")
     */
    protected $updatedBy;

    /**
     * EmployeeCareerPlan constructor.
     * @param CareerPlan $plan
     * @param Employee $user
     */
    public function __construct(CareerPlan $plan = null, Employee $user = null)
    {
        $this->plan = $plan;
        $this->user = $user;
        $this->modules = new ArrayCollection();
        $this->requirements = new ArrayCollection();
    }

    /**
     * @param CareerPlan $careerPlan
     * @param Employee $employee
     * @return EmployeeCareerPlan
     */
    public static function create(CareerPlan $careerPlan, Employee $employee)
    {
        $employeeCareerPlan = new EmployeeCareerPlan($careerPlan, $employee);
        foreach ($careerPlan->getEnabledRequirements() as $requirement) {
            $employeeCareerPlan->addDisabledRequirement($requirement);
        }

        foreach ($careerPlan->getEnabledModules() as $module) {
            if (!$module->isNivelation()) {
                continue;
            }
            $employeeCareerPlan->addDisabledModule($module);
        }

        return $employeeCareerPlan;
    }

    /**
     * @param CareerPlan $careerPlan
     * @param Employee $employee
     * @return EmployeeCareerPlan
     */
    public static function createForCurrentPlan(CareerPlan $careerPlan, Employee $employee)
    {
        $employeeCareerPlan = new EmployeeCareerPlan($careerPlan, $employee);
        $employeeCareerPlan->setEnabled(true);

        foreach ($careerPlan->getEnabledRequirements() as $requirement) {
            $employeeCareerPlan->addEnabledRequirement($requirement);
        }
        /* 2018-07-31, comento la asignación de los requerimientos y módulos automáticamente, la acción de asignar un
                               plan es solo asignar un plan.
        foreach ($careerPlan->getEnabledModules() as $module) {
            if ($module->isNivelation()) {
                continue;
            }
            $employeeCareerPlan->addEnabledModule($module);
        }*/

        return $employeeCareerPlan;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Employee
     * @deprecated
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Employee
     */
    public function getEmployee(){
        return $this->user;
    }

    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }


    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Get approved
     *
     * @return bool
     * @deprecated
     */
    public function getApproved()
    {
        return $this->isApproved();
    }

    public function setModules(\Doctrine\Common\Collections\Collection $modules)
    {
        $this->modules = $modules;

        return $this;
    }

    public function addModule(EmployeeCareerModule $module) {
        $this->modules[] = $module;
        return $this;
    }

    public function removeAnswer(EmployeeCareerModule $module) {
        $this->modules->removeElement($module);
    }


    public function getModules()
    {
        return $this->modules;
    }


    public function setRequirements(\Doctrine\Common\Collections\Collection $reqs)
    {
        $this->requirements = $reqs;

        return $this;
    }

    public function addRequirement(PlanRequirementsEmployee $req) {
        $this->requirements[] = $req;
        return $this;
    }

    /**
     * @param PlanRequirement $requirement
     */
    public function addEnabledRequirement(PlanRequirement $requirement)
    {
        $r = new PlanRequirementsEmployee();
        $r->setEmployeeCareerPlan($this);
        $r->setRequirement($requirement);
        $r->setEnabled(true);
        $this->addRequirement($r);
    }

    /**
     * @param PlanRequirement $requirement
     */
    public function addDisabledRequirement(PlanRequirement $requirement)
    {
        $r = new PlanRequirementsEmployee();
        $r->setEmployeeCareerPlan($this);
        $r->setRequirement($requirement);
        $r->setEnabled(false);
        $this->addRequirement($r);
    }

    /**
     * @param Module $module
     */
    public function addEnabledModule(Module $module)
    {
        $m = new EmployeeCareerModule();
        $m->setEmployeePlan($this);
        $m->setModule($module);
        $m->setEnabled(true);
        $m->setOriginalPollQuestionCount($module->getPoll() ? $module->getPoll()->getQuestions()->count() : null);
        $this->modules->add($m);
    }

    /**
     * @param Module $module
     */
    public function addDisabledModule(Module $module)
    {
        $m = new EmployeeCareerModule();
        $m->setEmployeePlan($this);
        $m->setModule($module);
        $m->setEnabled(false);
        $m->setOriginalPollQuestionCount($module->getPoll() ? $module->getPoll()->getQuestions()->count() : null);
        $this->modules->add($m);
    }

    public function removeRequirement(PlanRequirementsEmployee $req) {
        $this->modules->removeElement($req);
    }


    public function getRequirements()
    {
        return $this->requirements;
    }

    public function getArrEnabledModules(){
        $modules = [];

        foreach ($this->getModules() as $module){
            $modules[] = $module->getModule()->getId();
        }

        return $modules;
    }

    public function getArrApprovedModules(){
        $modules = [];

        foreach ($this->getApprovedModules() as $module){
            $modules[] = $module->getModule()->getId();
        }

        return $modules;
    }

    public function getArrApprovedRequirements(){
        $ids = [];

        foreach ($this->getApprovedRequirements() as $requirement){
            $ids[] = $requirement->getRequirement()->getId();
        }

        return $ids;
    }

    /**
     * @return ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getEnabledModules(){
        return $this->getModules()->filter(function (EmployeeCareerModule $module) {
            return $module->isEnabled();
        });
    }

    /**
     * @return ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getApprovedModules(){
        return $this->getEnabledModules()->filter(function (EmployeeCareerModule $module) {
            return $module->isApproved();
        });
    }

    /**
     * @return ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getEnabledRequirements()
    {
        return $this->getRequirements()->filter(function (PlanRequirementsEmployee $req) {
            return $req->isEnabled();
        });
    }

    /**
     * @return ArrayCollection|\Doctrine\Common\Collections\Collection|PlanRequirementsEmployee[]
     */
    public function getApprovedRequirements()
    {
        return $this->getEnabledRequirements()->filter(function (PlanRequirementsEmployee $req) {
            return $req->isApproved();
        });
    }

    public function __toString()
    {
        return sprintf('%s', $this->plan);
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        if ($this->isApproved()) {
            if ($this->getEnabledRequirements()->count() != $this->getApprovedRequirements()->count()) {
                $context->buildViolation('Tiene requerimientos sin aprobar.')
                    ->atPath('approved')
                    ->addViolation();
            }

            if ($this->getEnabledModules()->count() != $this->getApprovedModules()->count()) {
                $context->buildViolation('Tiene módulos sin aprobar.')
                    ->atPath('approved')
                    ->addViolation();
            }
        }
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @param Module $module
     * @return bool
     */
    public function isModuleEnabled(Module $module)
    {
        return $this->getEnabledModules()->exists(function ($i, EmployeeCareerModule $em) use ($module) {
            return $em->getModule() === $module && $em->isEnabled();
        });
    }

    /**
     * @param Module $module
     * @return bool
     */
    public function isModuleApproved(Module $module)
    {
        return $this->getApprovedModules()->exists(function ($i, EmployeeCareerModule $em) use ($module) {
            return $em->getModule() === $module && $em->isApproved();
        });
    }

    /**
     * @param PlanRequirement $planRequirement
     * @return bool
     */
    public function isRequirementApproved(PlanRequirement $planRequirement)
    {
        return $this->getApprovedRequirements()->exists(function ($i, PlanRequirementsEmployee $req) use ($planRequirement) {
            return $req->getRequirement() === $planRequirement;
        });
    }

    /**
     * @param PlanRequirement $planRequirement
     * @return bool
     */
    public function isRequirementEnabled(PlanRequirement $planRequirement)
    {
        return $this->getEnabledRequirements()->exists(function ($i, PlanRequirementsEmployee $req) use ($planRequirement) {
            return $req->getRequirement() === $planRequirement;
        });
    }

    /**
     * @param $module
     * @return EmployeeCareerModule
     */
    public function getModule($module)
    {
        return $this->getModules()->filter(function (EmployeeCareerModule $em) use ($module) {
            return $em->getModule() === $module;
        })->first();
    }
}

