<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

trait EnabledTrait
{
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $enabled = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $enabledAt;

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled($enabled)
    {
        if ($enabled) {
            $this->enabledAt = new \DateTime();
        }
        $this->enabled = $enabled;
    }

    public function getEnabledAt()
    {
        return $this->enabledAt;
    }

}