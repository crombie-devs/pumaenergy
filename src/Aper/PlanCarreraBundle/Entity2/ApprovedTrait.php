<?php

namespace Aper\PlanCarreraBundle\Entity;

trait ApprovedTrait
{

    /**
     * @var boolean
     *
     * @ORM\Column(name="approved", type="boolean", options={"default":false})
     */
    private $approved = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $approvedAt;

    /**
     * @return bool
     */
    public function isApproved()
    {
        return $this->approved;
    }

    /**
     * @param bool $approved
     */
    public function setApproved($approved)
    {
        if ($approved) {
            $this->approvedAt = new \DateTime();
        }
        $this->approved = $approved;
    }

    public function getApprovedAt()
    {
        return $this->approvedAt;
    }

}