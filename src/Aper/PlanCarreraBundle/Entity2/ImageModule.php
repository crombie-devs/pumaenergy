<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use WebFactory\Bundle\FileBundle\Model\Image;


/**
 * Format
 *
 * @ORM\Entity
 * @ORM\Table(name="career_plan_module_images")
 */
class ImageModule extends Image
{
    
    const DIRECTORY = 'uploads/career/module/images';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var UploadedFile
     * @Assert\Image(
     *     minWidth = 20,
     *     maxWidth = 4000,
     *     minHeight = 20,
     *     maxHeight = 4000,
     * )
     */
    protected $file;
    
    /**
     * @ORM\OneToOne(targetEntity="Module", inversedBy="image")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id")
     */

    private $module;

    /**
     *
     * @param Module $module
     * @return ImageModule
     */
    public function setModule(Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     *
     * @return Module
     */
    public function getModule()
    {
        return $this->module;
    }

}