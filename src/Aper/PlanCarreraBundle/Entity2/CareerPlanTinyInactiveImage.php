<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use WebFactory\Bundle\FileBundle\Model\Image;


/**
 * Format
 *
 * @ORM\Entity
 */
class CareerPlanTinyInactiveImage extends Image
{

    const DIRECTORY = 'uploads/career/i/t/i';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var UploadedFile
     * @Assert\Image(
     *     minWidth = 20,
     *     maxWidth = 4000,
     *     minHeight = 20,
     *     maxHeight = 4000,
     * )
     */
    protected $file;

    /**
     * @var CareerPlan
     * @ORM\OneToOne(targetEntity="Aper\PlanCarreraBundle\Entity\CareerPlan", inversedBy="tinyInactiveImage")
     */
    private $plan;

    /**
     * @return CareerPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param CareerPlan $plan
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;
    }

    /**
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function ensureRequiredFile(ExecutionContextInterface $context)
    {
        parent::ensureRequiredFile($context);
    }
}