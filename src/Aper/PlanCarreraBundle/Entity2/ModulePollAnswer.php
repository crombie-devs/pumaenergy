<?php

namespace Aper\PlanCarreraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModulePollAnswer
 *
 * @ORM\Table(name="career_plan_module_poll_answer")
 * @ORM\Entity(repositoryClass="Aper\PlanCarreraBundle\Repository\ModulePollAnswerRepository")
 */
class ModulePollAnswer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity="ModulePollQuestion", inversedBy="answers", cascade={"persist"})
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }


    public function getText()
    {
        return $this->text;
    }

    public function getQuestion()
    {
        return $this->question;
    }


    public function setQuestion(ModulePollQuestion $question)
    {
        $this->question = $question;
    }
}

