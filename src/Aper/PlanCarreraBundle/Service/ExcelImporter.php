<?php

namespace Aper\PlanCarreraBundle\Service;

use Liuggio\ExcelBundle\Factory;

class ExcelImporter
{

    private $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }



    public function importarExcel($fileWithPath)
    {
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        $colLimitRequirements = 'Z';

        if(file_exists($fileWithPath)) {
           // echo 'exist'."<br>";
        } else {
            echo 'dont exist'."<br>";
            die;
        }
        //cargamos el archivo a procesar.
        $objPHPExcel = $this->factory->createPHPExcelObject($fileWithPath);

        //se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
        $total_sheets = $objPHPExcel->getSheetCount();
        $allSheetName = $objPHPExcel->getSheetNames();
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        //Se obtiene el número máximo de filas
        $highestRow = $objWorksheet->getHighestRow();
        //Se obtiene el número máximo de columnas
        $highestColumn = $objWorksheet->getHighestColumn();
        $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);
        //$headingsArray contiene las cabeceras de la hoja excel. Llos titulos de columnas
        $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
        $headingsArray = $headingsArray[1];


        //Se recorre toda la hoja excel desde la fila 2 y se almacenan los datos
        $r    = -1;
        $data = array();
        $ids  =  [];

        $dataRow = $objWorksheet->rangeToArray('A3:'.$highestColumn.'3',null, true, true, true);

        foreach($headingsArray as $columnKey => $columnHeading)
        {
            $col = $this->alpha2num($columnKey);
            if($col < 2) continue;
            $ids[$col] = is_null($dataRow[3][$columnKey]) ? null : (int)$dataRow[3][$columnKey];
        }

        for ($row = 4; $row <= $highestRow; ++$row)
        {
            $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);

            if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > ''))
            {
                ++$r;

                foreach($headingsArray as $columnKey => $columnHeading)
                {
                    $col = $this->alpha2num($columnKey);

                    if(!$col) continue; // columna A la salteamos

                    $dni = (int)trim($dataRow[$row]['B']);

                    if(!isset($data[$dni])){
                        $data[$dni] = array('requerimientos' => [], 'modulos' => []);
                    }

                    if(!isset($ids[$col]) or is_null($ids[$col])) continue;

                    $id = $ids[$col];

                    $delimiter = '-';
                    if(strpos($dataRow[$row][$columnKey], '–') !== false){
                        $delimiter = '–';
                    }

                    list($enabled, $approved) = array_pad(explode($delimiter,$dataRow[$row][$columnKey]), 2, null);

                    $status = array('enabled' => false, 'approved' => null);

                    if(strlen($enabled))
                    {
                        if (trim($enabled) == 'H')
                        {
                            $status['enabled'] = true;
                        }

                        if(!is_null($approved) and strlen($approved))
                        {
                            if (trim($approved) == 'A')
                            {
                                $status['approved'] = true;
                            } else {
                                $status['approved'] = false;
                            }
                        } else {
                            $status['approved'] = false;
                        }
                    }

                    if($col <= $this->alpha2num($colLimitRequirements)){
                        $data[$dni]['requerimientos'][$id] = $status;
                    } else {
                        $data[$dni]['modulos'][$id] = $status;
                    }
                }
            }
        }
//        dump($data);
//        die;
        return($data);
    }

    public function importarExcelExamenes($fileWithPath)
    {
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        $colLimitRequirements = 'G';

        if(file_exists($fileWithPath)) {
            // echo 'exist'."<br>";
        } else {
            echo 'dont exist'."<br>";
            die;
        }
        //cargamos el archivo a procesar.
        $objPHPExcel = $this->factory->createPHPExcelObject($fileWithPath);

        //se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        //Se obtiene el número máximo de filas
        $highestRow = $objWorksheet->getHighestRow();


        //Se recorre toda la hoja excel desde la fila 2 y se almacenan los datos
        $data = array();

        for ($row = 2; $row <= $highestRow; ++$row)
        {
            $dataRow = $objWorksheet->rangeToArray('A'.$row.':G'.$row,null, false, true, true);

            if ((isset($dataRow[$row]['B'])) && ($dataRow[$row]['B'] > ''))
            {
                $nro                   = trim($dataRow[$row]['A']) ? (int)trim($dataRow[$row]['A']) : (count($data) + 1) ;
                $pregunta              = trim($dataRow[$row]['B']);
                $pregunta_categoria    = trim($dataRow[$row]['C']);
                $respuesta_correcta    = trim($dataRow[$row]['D']) ?: false;
                $respuesta_incorrecta1 = trim($dataRow[$row]['E']) ?: false;
                $respuesta_incorrecta2 = trim($dataRow[$row]['F']) ?: false;
                $respuesta_incorrecta3 = trim($dataRow[$row]['G']) ?: false;


                if(isset($data[$nro])){
                    $nros = array_keys($data);
                    $nro  = (max($nros))+1;
                }

                $data[$nro] = compact('pregunta', 'pregunta_categoria', 'respuesta_correcta', 'respuesta_incorrecta1', 'respuesta_incorrecta2', 'respuesta_incorrecta3');
            }
        }

        return($data);
    }


    public function importarExcelTalleres($fileWithPath, $idCurso)
    {
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        $colLimitRequirements = 'Z';



        if(file_exists($fileWithPath)) {
            // echo 'exist'."<br>";
        } else {
            echo 'dont exist'."<br>";
            die;
        }
        //cargamos el archivo a procesar.
        $objPHPExcel = $this->factory->createPHPExcelObject($fileWithPath);

        //se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        //Se obtiene el número máximo de filas
        $highestRow = $objWorksheet->getHighestRow();

        //Se recorre toda la hoja excel desde la fila 2 y se almacenan los datos
        $data = array();
    
        //$highestRow = 4;//TODO:b orrar esto
        $highestColumn = $objWorksheet->getHighestColumn();
    
        for ($row = 4; $row <= $highestRow; ++$row) {
            $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row, null, false, true, true);

            if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {

                $email      = trim($dataRow[$row]['B']);

                $data[$email][$idCurso] = ['examenPre' => false, 'curso' => false, 'examenPos' => false, 'encuesta' => false];
                // $colPosition = 3;
                // var_dump($this->columnLetter($colPosition));exit;

                // for ($taller=1; $taller < 7; $taller++) { 
                //     $examenPre  = trim($dataRow[$row][$this->columnLetter($colPosition)]) == 'A' ? true : false; $colPosition++;
                //     $curso      = trim($dataRow[$row][$this->columnLetter($colPosition)]) == 'A' ? true : false; $colPosition++;
                //     $examenPos  = trim($dataRow[$row][$this->columnLetter($colPosition)]) == 'A' ? true : false; $colPosition++;
                //     $encuesta   = trim($dataRow[$row][$this->columnLetter($colPosition)]) == 'A' ? true : false; $colPosition++;

                //     $data[$email][$taller] = ['examenPre' => $examenPre,
                //                               'curso' => $curso,
                //                               'examenPos' => $examenPos,
                //                               'encuesta' => $encuesta];
                // }
            }
        }

        return($data);
    }


    public function columnLetter($c){

        $c = intval($c);
        if ($c <= 0) return '';

        $letter = '';
                 
        while($c != 0){
           $p = ($c - 1) % 26;
           $c = intval(($c - $p) / 26);
           $letter = chr(65 + $p) . $letter;
        }
        
        return $letter;
    }

    /**
     * @param int $col
     * @param int $row
     * @return string
     */
    private function num2alpha($col,$row = null)
    {
        for($r = ""; $col >= 0; $col = intval($col / 26) - 1)
            $r = chr($col%26 + 0x41) . $r;
        return $r.(!is_null($row) ? $row : '');
    }

    function alpha2num($a)
    {
        $a = strtoupper($a);
        $l = strlen($a);
        $n = 0;
        for($i = 0; $i < $l; $i++)
            $n = $n*26 + ord($a[$i]) - 0x40;

        return $n-1;
    }
}