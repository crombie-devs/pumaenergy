<?php

namespace Aper\PlanCarreraBundle\Service;

use Aper\UserBundle\Entity\Employee;
use Aper\PlanCarreraBundle\Entity\AnswerSnapshot;
use Aper\PlanCarreraBundle\Entity\CareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeAnswer;
use Aper\PlanCarreraBundle\Entity\EmployeeExamAnswer;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Entity\ModulePoll;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Liuggio\ExcelBundle\Factory;

class ExcelExporter
{
    private $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    public function getResponseFromModuleExam($params, $filename){
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        /* @var Registry */
        $doctrine = $params['doctrine'];
        /** @var ModuleExam $exam */
        $exam = $doctrine->getRepository(ModuleExam::class)->find($params['examId']);

        /** @var EmployeeExamAnswer[]|ArrayCollection $answers */
        $answers = $doctrine->getRepository(EmployeeExamAnswer::class)->findByExamBetweenDate($exam->getModule(), $params['dateFrom'],$params['dateTo']);

        $results = [];
        foreach ($exam->getQuestions() as $question){
            $results[$question->getId()]['text'] = $question->getText();
            foreach ($question->getAnswers() as $answer){
                $results[$question->getId()]['answers'][$answer->getId()]['text']  = $answer->getText();
                $results[$question->getId()]['answers'][$answer->getId()]['correct']  = ($answer->isCorrect() ? ' [Correcta]' : '');
                $results[$question->getId()]['answers'][$answer->getId()]['count'] = 0;

                $results[$question->getId()]['totals']['total']       = 0;
                $results[$question->getId()]['totals']['incorrectas'] = 0;
                $results[$question->getId()]['totals']['correctas']   = 0;
            }
        }

        $employees = [];
        foreach ($answers as $answer){
            $employees[$answer->getEmployeeExam()->getId() . '_' . $answer->getEmployeeExam()->getEmployee()->getId()] = $answer->getEmployeeExam()->isApproved();
            /** @var AnswerSnapshot $data */
            if(!isset($results[$answer->getQuestion()->getId()])){
                $results[$answer->getQuestion()->getId()]['text'] = $answer->getQuestion()->getText();

                $results[$answer->getQuestion()->getId()]['totals']['total']       = 0;
                $results[$answer->getQuestion()->getId()]['totals']['incorrectas'] = 0;
                $results[$answer->getQuestion()->getId()]['totals']['correctas']   = 0;
            }

            if(is_null($answer->getAnswer())){
                continue;
            }
            if(isset($results[$answer->getQuestion()->getId()]['answers'][$answer->getAnswer()->getId()])){
                $results[$answer->getQuestion()->getId()]['answers'][$answer->getAnswer()->getId()]['count']++;
                $results[$answer->getQuestion()->getId()]['totals']['total']++;
                if($answer->getAnswer()->isCorrect()){
                    $results[$answer->getQuestion()->getId()]['totals']['correctas']++;
                } else {
                    $results[$answer->getQuestion()->getId()]['totals']['incorrectas']++;
                }
            } else {
                $results[$answer->getQuestion()->getId()]['answers'][$answer->getAnswer()->getId()]['text']    = $answer->getText();
                $results[$answer->getQuestion()->getId()]['answers'][$answer->getAnswer()->getId()]['count']   = 1;
                $results[$answer->getQuestion()->getId()]['answers'][$answer->getAnswer()->getId()]['correct'] = '';

                $results[$answer->getQuestion()->getId()]['totals']['total']       = 0;
                $results[$answer->getQuestion()->getId()]['totals']['incorrectas'] = 0;
                $results[$answer->getQuestion()->getId()]['totals']['correctas']   = 0;
            }

        }

        // solicitamos el servicio 'phpexcel' y creamos el objeto vacío...
        $phpExcelObject = $this->factory->createPHPExcelObject();

        // ...y le asignamos una serie de propiedades
        $phpExcelObject->getProperties()
            ->setCreator("Plan de Carrera")
            ->setLastModifiedBy("Plan de Carrera")
            ->setTitle(utf8_decode("Exportación de estadísticas de Exámenes"))
            ->setSubject("Exámenes")
            ->setDescription("Estadísticas de Examen generado el " . date('d/m/Y') . " a las " . date('H:i:s'))
            ->setKeywords("Plan de carrera, examen");

        // establecemos como hoja activa la primera, y le asignamos un título
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->setTitle('Examen');

        // escribimos en distintas celdas del documento el título de los campos que vamos a exportar
        $sheet = $phpExcelObject->setActiveSheetIndex(0);

        $sheet->setCellValue('A1', 'Plan: ' . $exam->getModule()->getPlan());
        $sheet->setCellValue('A2', 'Examen: ' . $exam->getModule()->getTitle());
        $sheet->setCellValue('A3', 'Desde: ' . (strlen($params['dateFrom']) ? (new \DateTime($params['dateFrom']))->format('d/m/Y'): '' ));
        $sheet->setCellValue('A4', 'Hasta: ' . (strlen($params['dateTo']) ? (new \DateTime($params['dateTo']))->format('d/m/Y'): '' ));
        $sheet->setCellValue('A5', 'Evaluados: ' . count($employees));
        $sheet->setCellValue('A6', 'Aprobados: ' . count(array_filter($employees, function($approved) { return $approved; })));
        $sheet->setCellValue('A7', 'Reprobados: ' . count(array_filter($employees, function($approved) { return !$approved; })));

        $i= 9;
        foreach ($results as $question){
            $sheet->setCellValue('A'.$i, $question['text']);
            $j = 1;
            $sheet->setCellValue($this->num2alpha($j,$i), '% Correctas');
            $sheet->setCellValue($this->num2alpha($j,$i+1), (($question['totals']['total']) ? number_format(($question['totals']['correctas']/$question['totals']['total'])*100, 2) : '0'));
            $sheet->getColumnDimension($this->num2alpha($j))->setAutoSize(true);
            $j++;
            $sheet->setCellValue($this->num2alpha($j,$i), '% Incorrectas');
            $sheet->setCellValue($this->num2alpha($j,$i+1), (($question['totals']['total']) ? number_format(($question['totals']['incorrectas']/$question['totals']['total'])*100, 2) : '0'));
            $sheet->getColumnDimension($this->num2alpha($j))->setAutoSize(true);
            $j++;
            foreach ($question['answers'] as $answer){
                $sheet->setCellValue($this->num2alpha($j,$i), $answer['text'] . $answer['correct'] );

                $sheet->setCellValue($this->num2alpha($j,$i+1), $answer['count']);
                $j++;
            }
            $i = $i+3;
        }

        // se crea el writer
        $writer = $this->factory->createWriter($phpExcelObject, 'Excel5');
        // se crea el response
        $response = $this->factory->createStreamedResponse($writer);

        // y por último se añaden las cabeceras
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    public function getResponseFromModulePoll($params, $filename){
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        /* @var Registry */
        $doctrine = $params['doctrine'];
        /** @var ModulePoll $poll */
        $poll = $doctrine->getRepository(ModulePoll::class)->find($params['pollId']);
        /** @var EmployeeAnswer[]|ArrayCollection $answers */
        $answers = $doctrine->getRepository(EmployeeAnswer::class)->findByPollBetweenDate($poll->getModule(), $params['dateFrom'],$params['dateTo']);

        $results = [];
        foreach ($poll->getQuestions() as $question){
            if($question->getType() == 'MULTCHOICE'){
                $results[$question->getId()]['text']   = $question->getText();
                $results[$question->getId()]['totals'] = 0;
                foreach ($question->getAnswers() as $answer){
                    $results[$question->getId()]['answers'][$answer->getId()]['text']  = $answer->getText();
                    $results[$question->getId()]['answers'][$answer->getId()]['count'] = 0;
                }
            }
        }

        $employees = [];
        foreach ($answers as $answer){

            $employees[$answer->getEmployee()->getId()] = 1;
            /** @var AnswerSnapshot $data */
            $data = $answer->getData();
            if($data->getQuestionType() == 'MULTCHOICE'){
                if(!isset($results[$data->getQuestionId()])){
                    $results[$data->getQuestionId()]['text'] = $data->getAnswerText();
                }
                if(isset($results[$data->getQuestionId()]['answers'][$data->getAnswerId()])){
                    $results[$data->getQuestionId()]['answers'][$data->getAnswerId()]['count']++;
                    $results[$data->getQuestionId()]['totals']++;
                } else {
                    $results[$data->getQuestionId()]['answers'][$data->getAnswerId()]['text']  = $data->getQuestionText();
                    $results[$data->getQuestionId()]['answers'][$data->getAnswerId()]['count'] = 1;
                    $results[$data->getQuestionId()]['totals'] = 1;
                }
            }
        }

        // solicitamos el servicio 'phpexcel' y creamos el objeto vacío...
        $phpExcelObject = $this->factory->createPHPExcelObject();

        // ...y le asignamos una serie de propiedades
        $phpExcelObject->getProperties()
            ->setCreator("Plan de Carrera")
            ->setLastModifiedBy("Plan de Carrera")
            ->setTitle(utf8_decode("Exportación de resultados de Encuestas"))
            ->setSubject("Encuesta")
            ->setDescription("Resultados de Encuesta generado el " . date('d/m/Y') . " a las " . date('H:i:s'))
            ->setKeywords("Plan de carrera, encuesta");

        // establecemos como hoja activa la primera, y le asignamos un título
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->setTitle('Encuesta');

        // escribimos en distintas celdas del documento el título de los campos que vamos a exportar
        $sheet = $phpExcelObject->setActiveSheetIndex(0);

        $sheet->setCellValue('A1', 'Plan: ' . $poll->getModule()->getPlan());
        $sheet->setCellValue('A2', 'Capacitación: ' . $poll->getModule()->getTitle());
        $sheet->setCellValue('A3', 'Desde: ' . (strlen($params['dateFrom']) ? (new \DateTime($params['dateFrom']))->format('d/m/Y'): '' ));
        $sheet->setCellValue('A4', 'Hasta: ' . (strlen($params['dateTo']) ? (new \DateTime($params['dateTo']))->format('d/m/Y'): '' ));
        $sheet->setCellValue('A5', 'Encuestados: ' . count($employees));

        $i= 7;
        foreach ($results as $question){
            $sheet->setCellValue('A'.$i, $question['text']);
            $j = 1;
            foreach ($question['answers'] as $answer){
                $sheet->setCellValue($this->num2alpha($j,$i), $answer['text']);

                $sheet->mergeCells($this->num2alpha($j,$i) . ':' . $this->num2alpha($j+1,$i));
                $sheet->getStyle($this->num2alpha($j,$i))->getAlignment()->applyFromArray(
                    array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
                );

                $sheet->setCellValue($this->num2alpha($j,$i+1), 'Cant' );
                $v = (!$question['totals'] ? 0 : number_format( ( $answer['count'] / $question['totals']) * 100, 2));
                $sheet->setCellValue($this->num2alpha($j,$i+2), $answer['count'] );
                $sheet->getColumnDimension($this->num2alpha($j))->setAutoSize(true);
                $j++;

                $sheet->setCellValue($this->num2alpha($j,$i+1), 'Porc' );
                $sheet->setCellValue($this->num2alpha($j,$i+2), $v . '%');
                $sheet->getColumnDimension($this->num2alpha($j))->setAutoSize(true);
                $j++;


            }
            $sheet->getColumnDimension($this->num2alpha(0))->setAutoSize(true);
            $i = $i+4;
        }


        // se crea el writer
        $writer = $this->factory->createWriter($phpExcelObject, 'Excel5');
        // se crea el response
        $response = $this->factory->createStreamedResponse($writer);

        // y por último se añaden las cabeceras
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    public function getResponseFromEmployeeQueryBuilder(QueryBuilder $queryBuilder, $filename)
    {

        ini_set('max_execution_time', 9000);
        ini_set('memory_limit','2G');

        /* @var EntityManager */
        $em = $queryBuilder->getEntityManager();

        $comercials = [];
        $usersComercials = $em->getRepository('UserBundle:User')->findUsersByProfile('ROLE_HEAD_OFFICE');
        if ($usersComercials) {
            foreach ($usersComercials as $key => $value) {
                $comercials[$value->getId()] = $value->getProfileName();
            }
        }

        $offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 15000;

        $queryBuilder2 = $em->createQueryBuilder()
            ->select('entity')
            ->from(Employee::class, 'entity')
            ->setFirstResult( $offset )
            ->setMaxResults( $limit )
            ->orderBy('entity.id','DESC');

        /* @var QueryBuilder */
        $queryBuilderPdc = $em->createQueryBuilder()
            ->select('entity')
            ->from(CareerPlan::class, 'entity')
            ->setFirstResult( 0 )
            ->setMaxResults( 15000 )
            ->orderBy('entity.position','ASC');

        $pdc      = new ArrayCollection($queryBuilderPdc->getQuery()->getResult());
        $ids      = [];
        $entities = new ArrayCollection($queryBuilder2->getQuery()->getResult());


        // solicitamos el servicio 'phpexcel' y creamos el objeto vacío...
        $phpExcelObject = $this->factory->createPHPExcelObject();

        // ...y le asignamos una serie de propiedades
        $phpExcelObject->getProperties()
            ->setCreator("Plan de Carrera")
            ->setLastModifiedBy("Plan de Carrera")
            ->setTitle(utf8_decode("Exportación de Empleados"))
            ->setSubject("Empleados")
            ->setDescription("Listado de empleados generado el " . date('d/m/Y') . " a las " . date('H:i:s'))
            ->setKeywords("Plan de carrera empleados");

        // establecemos como hoja activa la primera, y le asignamos un título
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->setTitle('Empleados');

        // escribimos en distintas celdas del documento el título de los campos que vamos a exportar
        $sheet = $phpExcelObject->setActiveSheetIndex(0);

        $sheet->setCellValue('A1', 'ID Pixelnet');
        $sheet->setCellValue('B1', 'Empleado');
        $sheet->setCellValue('C1', 'Nombre de usuario');
        $sheet->setCellValue('D1', 'Territory Code');
        $sheet->setCellValue('E1', 'Nombre');
        $sheet->setCellValue('F1', 'Dirección');
        $sheet->setCellValue('G1', 'Localidad');
        $sheet->setCellValue('H1', 'Provincia');
        $sheet->setCellValue('I1', 'Zona');
        $sheet->setCellValue('J1', 'Rep. Comercial');
        $sheet->setCellValue('K1', 'Perfil');
        $sheet->setCellValue('L1', 'Puesto');
        $this->addPDCHeaders($phpExcelObject, $pdc, $ids);


        $row = 5;
        foreach ($entities as $employee){

            if(!$employee->getUser()->isEnabled()){
                continue;
            }
            $sheet->setCellValue('A'.$row, trim($employee->getIdPixelnetUsr()));
            $sheet->setCellValue('B'.$row, trim($employee->getProfile()->getName()));
            $sheet->setCellValue('C'.$row, trim($employee->getUser()->getUsername()));
            if($employee->getStore()){
                $sheet->setCellValue('D'.$row, $employee->getStore()->getCode());
                $sheet->setCellValue('E'.$row, $employee->getStore()->getLocation());
                $sheet->setCellValue('F'.$row, $employee->getStore()->getAddress());
                $sheet->setCellValue('G'.$row, $employee->getStore()->getLocalidad());
                $sheet->setCellValue('H'.$row, $employee->getStore()->getProvincia());
                $sheet->setCellValue('I'.$row, $employee->getStore()->getZona());
                if (isset($comercials[$employee->getStore()->getResponsableComercial()])) {
                    $sheet->setCellValue('J'.$row, $comercials[$employee->getStore()->getResponsableComercial()]);
                }
            }

            $area = ($employee->getArea() ? $employee->getArea() : $employee->getCategory());

            $sheet->setCellValue('K'.$row, trim($employee->getUser()->getMaxRole()));
            $sheet->setCellValue('L'.$row, trim($area));

            $sheet->mergeCells($this->num2alpha(0,1) . ':' . $this->num2alpha(0,4));
            $sheet->mergeCells($this->num2alpha(1,1) . ':' . $this->num2alpha(1,4));
            $sheet->mergeCells($this->num2alpha(2,1) . ':' . $this->num2alpha(2,4));
            $sheet->mergeCells($this->num2alpha(3,1) . ':' . $this->num2alpha(3,4));
            $sheet->mergeCells($this->num2alpha(4,1) . ':' . $this->num2alpha(4,4));
            $sheet->mergeCells($this->num2alpha(5,1) . ':' . $this->num2alpha(5,4));
            $sheet->mergeCells($this->num2alpha(6,1) . ':' . $this->num2alpha(6,4));
            $sheet->mergeCells($this->num2alpha(7,1) . ':' . $this->num2alpha(7,4));
            $sheet->mergeCells($this->num2alpha(8,1) . ':' . $this->num2alpha(8,4));
            $sheet->mergeCells($this->num2alpha(9,1) . ':' . $this->num2alpha(9,4));
            $sheet->mergeCells($this->num2alpha(10,1) . ':' . $this->num2alpha(10,4));
            $sheet->mergeCells($this->num2alpha(11,1) . ':' . $this->num2alpha(11,4));

            if($employee->getCareerPlans()){
                foreach ($employee->getCareerPlans() as $careerPlan){
                    $cell = $ids[$careerPlan->getPlan()->getId()]['cell'];
                    $sheet->setCellValue($this->num2alpha($cell,$row), ($careerPlan->isEnabled() ? (!is_null($careerPlan->getEnabledAt()) ? $careerPlan->getEnabledAt()->format('d/m/Y') : 'Si' ) : 'No'));
                    $sheet->setCellValue($this->num2alpha($cell+1,$row), ($careerPlan->isApproved() ? ( !is_null($careerPlan->getApprovedAt()) ? $careerPlan->getApprovedAt()->format('d/m/Y') : 'Si') : 'No'));

                    foreach ($careerPlan->getRequirements() as $requirement){

                        if(!isset($ids[$careerPlan->getPlan()->getId()])) continue;
                        if(!isset($ids[$careerPlan->getPlan()->getId()]['requirement'])) continue;
                        if(!isset($ids[$careerPlan->getPlan()->getId()]['requirement'][$requirement->getRequirement()->getId()])) continue;
                        $cell = $ids[$careerPlan->getPlan()->getId()]['requirement'][$requirement->getRequirement()->getId()]['enabled'];
                        $sheet->setCellValue($this->num2alpha($cell,$row), ($requirement->isEnabled() ? ( !is_null($requirement->getEnabledAt()) ? $requirement->getEnabledAt()->format('d/m/Y') : 'Si') : 'No'));
                        $cell = $ids[$careerPlan->getPlan()->getId()]['requirement'][$requirement->getRequirement()->getId()]['approved'];
                        $sheet->setCellValue($this->num2alpha($cell,$row), ($requirement->isApproved() ? ( !is_null($requirement->getApprovedAt()) ? $requirement->getApprovedAt()->format('d/m/Y') : 'Si' ): 'No'));
                    }

                    foreach ($careerPlan->getModules() as $module){
                        if(!isset($ids[$careerPlan->getPlan()->getId()])) continue;
                        if(!isset($ids[$careerPlan->getPlan()->getId()]['module'])) continue;
                        if(!isset($ids[$careerPlan->getPlan()->getId()]['module'][$module->getModule()->getId()])) continue;

                        $cell = $ids[$careerPlan->getPlan()->getId()]['module'][$module->getModule()->getId()]['enabled'];
                        $sheet->setCellValue(
                            $this->num2alpha($cell,$row), 
                            ($module->isEnabled() ? (!is_null($module->getEnabledAt()) ? $module->getEnabledAt()->format('d/m/Y') : 'Si' ) : 'No')
                        );
                        $cell = $ids[$careerPlan->getPlan()->getId()]['module'][$module->getModule()->getId()]['approved'];
                        $sheet->setCellValue(
                            $this->num2alpha($cell,$row), 
                            ($module->isApproved() ? (!is_null($module->getApprovedAt()) ? $module->getApprovedAt()->format('d/m/Y') : 'Si') : 'No')
                        );
                    
                        $date = '';
                        $progress = $this->getModuleProgress($module);

                        if ($progress == 100) {
                            if ($module->getExamPermission() && $module->getExamPermission()->getEmployeeExams()) {
                                foreach ($module->getExamPermission()->getEmployeeExams() as $aa => $mm) {
                                    if (empty($date)) {
                                        if (!is_null($mm->getFinishedAt())) {
                                            $date = $mm->getFinishedAt()->format('d/m/Y');
                                        }
                                        if (!is_null($mm->getStartsAt())) {
                                            $date = $mm->getStartsAt()->format('d/m/Y');
                                        }
                                    }
                                }
                                if (empty($date)) {
                                    $date = $module->getExamPermission()->getEnabledAt()->format('d/m/Y');

                                }
                            } else {
                                $date = $module->getEnabledAt()->format('d/m/Y');
                            }

                        }

                        $cell = $ids[$careerPlan->getPlan()->getId()]['module'][$module->getModule()->getId()]['enabled'];
                        $sheet->setCellValue(
                            $this->num2alpha($cell,$row), 
                            $progress
                        );
                        $cell = $ids[$careerPlan->getPlan()->getId()]['module'][$module->getModule()->getId()]['approved'];
                        $sheet->setCellValue(
                            $this->num2alpha($cell,$row), 
                            $date
                        );
                    }
                }
            }

            $row++;
        }

        // se crea el writer
        $writer = $this->factory->createWriter($phpExcelObject, 'Excel5');
        // se crea el response
        $response = $this->factory->createStreamedResponse($writer);

        // y por último se añaden las cabeceras
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }


    private function getModuleProgress($module) {
        $progress = 0;
        if ($module->getExamPermission() == false && $module->getExamPermissionsPost() == false && $module->getPollPermission() == false) {
            $progress = 0;
        } else if ($module->getExamPermission() !== false && $module->getExamPermission()->isEnabled() !== false) {

            // if prev exam is completed = 25
            if ($module->getExamPermission()->getNextStep() === 'completed') {
                $progress = 25;
            }

            if ($module->getExamPermissionsPost() && $module->getExamPermissionsPost()->isEnabled() == true && $module->getExamPermissionsPost()->getNextStep() != 'none' ) {
                $progress = 75;
            }

            // check post exam
            if ($module->getExamPermissionsPost() !== false && $module->getExamPermissionsPost()->getNextStep() === 'completed' && $module->getExamPermissionsPost()->isEnabled() !== false) {

                $progress = 100;
            } else {
                if ($module->getPollPermission() && $module->getPollPermission()->isEnabled() == true && $module->getPollPermission()->getNextStep() != 'none') {
                    if ($module->getModule()->getId() >= 9) {
                        if (
                            $module->getExamPermissionsPost() && $module->getExamPermissionsPost()->isEnabled() == true && 
                            ($module->getExamPermissionsPost()->getNextStep() === 'completed' || $module->getExamPermissionsPost()->getNextStep() === 'finish') 
                        ) {
                            $progress = 100;
                        }
                    } else {
                        if ($module->isApproved()) {
                            $progress = 100;
                        }
                    }
                }
            }

        } else if ($module->getExamPermission() !== false && $module->getExamPermission()->getNextStep() !== 'completed') {
            $progress = 0;
        }

        if ($module->isEnabled() && ($module->isApproved() || $progress == 100)) {
            $progress = 100;
        }

        return $progress;
    }


    private function addPDCHeaders(&$phpExcelObject, $pdc, &$ids)
    {
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
        $col1 = 12;
        $col2 = $col3 = $col4 = 12;

        foreach ($pdc as $plan){
            $sheet->setCellValue($this->num2alpha($col1, 1), $plan->getTitle());
            $ids[$plan->getId()]['cell'] = $col1;
            $sheet->getStyle($this->num2alpha($col1,1))->applyFromArray(['font' => ['bold' => true, 'italic' => true]]);
            $col3 = $col4 = $col2;

            $sheet->setCellValue($this->num2alpha($col2,2), 'Requerimientos');
            $sheet->getStyle($this->num2alpha($col2,2))->getAlignment()->applyFromArray(['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER]);
            $sheet->getStyle($this->num2alpha($col2,2))->applyFromArray(['font' => ['bold' => true]]);
            foreach ($plan->getRequirements() as $requirement){
                $sheet->setCellValue($this->num2alpha($col3,3), $requirement->getDescription());
                $sheet->setCellValue($this->num2alpha($col4,4), 'Habilitado');
                $ids[$plan->getId()]['requirement'][$requirement->getId()]['enabled']  = $col4;
                $col4++;
                $sheet->setCellValue($this->num2alpha($col4,4), 'Aprobado');
                $ids[$plan->getId()]['requirement'][$requirement->getId()]['approved'] = $col4;
                $col4++;
                $sheet->mergeCells($this->num2alpha($col3,3) . ':' . $this->num2alpha($col4-1,3));
                $col3 = $col4;
            }
            if($plan->getRequirements()->count()){
                $sheet->mergeCells($this->num2alpha($col2,2) . ':' . $this->num2alpha($col4-1,2));
                $col2 = $col3;
            }

            $sheet->setCellValue($this->num2alpha($col2,2), 'Módulos Nivelación');
            $sheet->getStyle($this->num2alpha($col2,2))->getAlignment()->applyFromArray(['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER]);
            $sheet->getStyle($this->num2alpha($col2,2))->applyFromArray(['font' => ['bold' => true]]);
            $niv = 0;
            foreach ($plan->getModules() as $module){
                if ($module->isNivelation()){
                    $niv++;
                    $sheet->setCellValue($this->num2alpha($col3,3), $module->getTitle());
                    $sheet->setCellValue($this->num2alpha($col4,4), 'Habilitado');
                    $ids[$plan->getId()]['module'][$module->getId()]['enabled'] = $col4;
                    $col4++;
                    $sheet->setCellValue($this->num2alpha($col4,4), 'Aprobado');
                    $ids[$plan->getId()]['module'][$module->getId()]['approved'] = $col4;
                    $col4++;

                    if($module->getExam()){
                        for ($i = 1; $i <= $module->getExam()->getMaxAttempts(); $i++){
                            $sheet->setCellValue($this->num2alpha($col4,4), sprintf("Examen #%s", $i));
                            $ids[$plan->getId()]['module'][$module->getId()]['exam'][$module->getExam()->getId()][$i] = $col4;
                            $col4++;
                        }
                    }

                    $sheet->mergeCells($this->num2alpha($col3,3) . ':' . $this->num2alpha($col4-1,3));
                    $col3 = $col4;
                }
            }
            if($niv){
                $sheet->mergeCells($this->num2alpha($col2,2) . ':' . $this->num2alpha($col4-1,2));
                $col2 = $col3;
            }

            $sheet->setCellValue($this->num2alpha($col2,2), 'Capacitaciones');
            $sheet->getStyle($this->num2alpha($col2,2))->getAlignment()->applyFromArray(['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER]);
            $sheet->getStyle($this->num2alpha($col2,2))->applyFromArray(['font' => ['bold' => true]]);
            $mod = 0;
            foreach ($plan->getModules() as $module){
                if ($module->isEnabled() and (!$module->isNivelation()) and ($module->getType() == 'module')){
                    $mod++;
                    $sheet->setCellValue($this->num2alpha($col3,3), $module->getTitle());
                    $sheet->setCellValue($this->num2alpha($col4,4), 'Progreso');
                    $ids[$plan->getId()]['module'][$module->getId()]['enabled'] = $col4;
                    $col4++;
                    $sheet->setCellValue($this->num2alpha($col4,4), 'Fecha');
                    $ids[$plan->getId()]['module'][$module->getId()]['approved'] = $col4;
                    $col4++;

                    $sheet->mergeCells($this->num2alpha($col3,3) . ':' . $this->num2alpha($col4-1,3));
                    $col3 = $col4;
                }
            }
            if($mod){
                $sheet->mergeCells($this->num2alpha($col2,2) . ':' . $this->num2alpha($col4-1,2));
                $col2 = $col3;
            }
            
            if($plan->getRequirements() or $plan->getModules()){
                $sheet->mergeCells($this->num2alpha($col1,1) . ':' . $this->num2alpha($col4-1,1));
                $col1 = $col4;
            } else {
                $col1++;
            }

        }

        for ($col = 0; $col <= $col4 ; $col++) {
            $col1 = $this->num2alpha($col);
            $sheet->getColumnDimension($col1)->setAutoSize(true);
        }
    }


    /**
     * @param int $col
     * @param int $row
     * @return string
     */
    private function num2alpha($col,$row = null)
    {
        for($r = ""; $col >= 0; $col = intval($col / 26) - 1)
            $r = chr($col%26 + 0x41) . $r;
        return $r.(!is_null($row) ? $row : '');
    }
}
