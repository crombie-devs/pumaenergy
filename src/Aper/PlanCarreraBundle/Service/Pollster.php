<?php

namespace Aper\PlanCarreraBundle\Service;

use Aper\PlanCarreraBundle\Entity\AnswerSnapshot;
use Aper\PlanCarreraBundle\Entity\EmployeeAnswer;
use Aper\PlanCarreraBundle\Entity\EmployeeCareerPlan;
use Aper\PlanCarreraBundle\Entity\Module;
use Aper\PlanCarreraBundle\Model\AnswerDTO;
use Aper\UserBundle\Entity\Employee;
use Doctrine\ORM\EntityManager;

class Pollster
{
    /**
     * @var
     */
    private $repo;

    /**
     * Pollster constructor.
     */
    public function __construct(EntityManager $em)
    {
        $this->repo = $em->getRepository('PlanCarreraBundle:EmployeeAnswer');
    }

    /**
     * @param Module $module
     * @param Employee $employee
     * @param AnswerDTO $answer
     * @return EmployeeAnswer
     */
    public function answer(Module $module, Employee $employee, AnswerDTO $answer)
    {
        if ($this->repo->findOneBy(['employee' => $employee, 'data.questionId' => $answer->getQuestionId()])) {
            throw new \LogicException("Pregunta ya respondida");
        }

        $employeeAnswer = new EmployeeAnswer();
        $employeeAnswer->setEmployee($employee);

        /** @var EmployeeCareerPlan $plan */
        $plan = $employee->getCareerPlans()->filter(function (EmployeeCareerPlan $plan) use ($module) {
            return $plan->getPlan() === $module->getPlan();
        })->first();

        $employeeModule = $plan->getModule($module);
        $employeeAnswer->setModule($employeeModule);

        $snapshot = new AnswerSnapshot($answer->getQuestionId(), $answer->getQuestionText(), $answer->getQuestionType(), $answer->getAnswerId(), $answer->getAnswerText());

        $employeeAnswer->setData($snapshot);

        return $employeeAnswer;
    }

}