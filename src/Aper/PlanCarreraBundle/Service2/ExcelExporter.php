<?php

namespace Aper\PlanCarreraBundle\Service;

use Aper\PlanCarreraBundle\Entity\AnswerSnapshot;
use Aper\PlanCarreraBundle\Entity\CareerPlan;
use Aper\PlanCarreraBundle\Entity\EmployeeAnswer;
use Aper\PlanCarreraBundle\Entity\EmployeeExamAnswer;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Entity\ModulePoll;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Liuggio\ExcelBundle\Factory;

class ExcelExporter
{
    private $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    public function getResponseFromModuleExam($params, $filename){
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        /* @var Registry */
        $doctrine = $params['doctrine'];
        /** @var ModuleExam $exam */
        $exam = $doctrine->getRepository(ModuleExam::class)->find($params['examId']);

        /** @var EmployeeExamAnswer[]|ArrayCollection $answers */
        $answers = $doctrine->getRepository(EmployeeExamAnswer::class)->findByExamBetweenDate($exam->getModule(), $params['dateFrom'],$params['dateTo']);

        $results = [];
        foreach ($exam->getQuestions() as $question){
            $results[$question->getId()]['text'] = $question->getText();
            foreach ($question->getAnswers() as $answer){
                $results[$question->getId()]['answers'][$answer->getId()]['text']  = $answer->getText();
                $results[$question->getId()]['answers'][$answer->getId()]['correct']  = ($answer->isCorrect() ? ' [Correcta]' : '');
                $results[$question->getId()]['answers'][$answer->getId()]['count'] = 0;

                $results[$question->getId()]['totals']['total']       = 0;
                $results[$question->getId()]['totals']['incorrectas'] = 0;
                $results[$question->getId()]['totals']['correctas']   = 0;
            }
        }

        $employees = [];
        foreach ($answers as $answer){
            $employees[$answer->getEmployeeExam()->getId() . '_' . $answer->getEmployeeExam()->getEmployee()->getId()] = $answer->getEmployeeExam()->isApproved();
            /** @var AnswerSnapshot $data */
            if(!isset($results[$answer->getQuestion()->getId()])){
                $results[$answer->getQuestion()->getId()]['text'] = $answer->getQuestion()->getText();

                $results[$answer->getQuestion()->getId()]['totals']['total']       = 0;
                $results[$answer->getQuestion()->getId()]['totals']['incorrectas'] = 0;
                $results[$answer->getQuestion()->getId()]['totals']['correctas']   = 0;
            }

            if(is_null($answer->getAnswer())){
                continue;
            }
            if(isset($results[$answer->getQuestion()->getId()]['answers'][$answer->getAnswer()->getId()])){
                $results[$answer->getQuestion()->getId()]['answers'][$answer->getAnswer()->getId()]['count']++;
                $results[$answer->getQuestion()->getId()]['totals']['total']++;
                if($answer->getAnswer()->isCorrect()){
                    $results[$answer->getQuestion()->getId()]['totals']['correctas']++;
                } else {
                    $results[$answer->getQuestion()->getId()]['totals']['incorrectas']++;
                }
            } else {
                $results[$answer->getQuestion()->getId()]['answers'][$answer->getAnswer()->getId()]['text']    = $answer->getText();
                $results[$answer->getQuestion()->getId()]['answers'][$answer->getAnswer()->getId()]['count']   = 1;
                $results[$answer->getQuestion()->getId()]['answers'][$answer->getAnswer()->getId()]['correct'] = '';

                $results[$answer->getQuestion()->getId()]['totals']['total']       = 0;
                $results[$answer->getQuestion()->getId()]['totals']['incorrectas'] = 0;
                $results[$answer->getQuestion()->getId()]['totals']['correctas']   = 0;
            }

        }

        // solicitamos el servicio 'phpexcel' y creamos el objeto vacío...
        $phpExcelObject = $this->factory->createPHPExcelObject();

        // ...y le asignamos una serie de propiedades
        $phpExcelObject->getProperties()
            ->setCreator("Plan de Carrera")
            ->setLastModifiedBy("Plan de Carrera")
            ->setTitle(utf8_decode("Exportación de estadísticas de Exámenes"))
            ->setSubject("Exámenes")
            ->setDescription("Estadísticas de Examen generado el " . date('d/m/Y') . " a las " . date('H:i:s'))
            ->setKeywords("Plan de carrera, examen");

        // establecemos como hoja activa la primera, y le asignamos un título
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->setTitle('Examen');

        // escribimos en distintas celdas del documento el título de los campos que vamos a exportar
        $sheet = $phpExcelObject->setActiveSheetIndex(0);

        $sheet->setCellValue('A1', 'Plan: ' . $exam->getModule()->getPlan());
        $sheet->setCellValue('A2', 'Examen: ' . $exam->getModule()->getTitle());
        $sheet->setCellValue('A3', 'Desde: ' . (strlen($params['dateFrom']) ? (new \DateTime($params['dateFrom']))->format('d/m/Y'): '' ));
        $sheet->setCellValue('A4', 'Hasta: ' . (strlen($params['dateTo']) ? (new \DateTime($params['dateTo']))->format('d/m/Y'): '' ));
        $sheet->setCellValue('A5', 'Evaluados: ' . count($employees));
        $sheet->setCellValue('A6', 'Aprobados: ' . count(array_filter($employees, function($approved) { return $approved; })));
        $sheet->setCellValue('A7', 'Reprobados: ' . count(array_filter($employees, function($approved) { return !$approved; })));

        $i= 9;
        foreach ($results as $question){
            $sheet->setCellValue('A'.$i, $question['text']);
            $j = 1;
            $sheet->setCellValue($this->num2alpha($j,$i), '% Correctas');
            $sheet->setCellValue($this->num2alpha($j,$i+1), (($question['totals']['total']) ? number_format(($question['totals']['correctas']/$question['totals']['total'])*100, 2) : '0'));
            $sheet->getColumnDimension($this->num2alpha($j))->setAutoSize(true);
            $j++;
            $sheet->setCellValue($this->num2alpha($j,$i), '% Incorrectas');
            $sheet->setCellValue($this->num2alpha($j,$i+1), (($question['totals']['total']) ? number_format(($question['totals']['incorrectas']/$question['totals']['total'])*100, 2) : '0'));
            $sheet->getColumnDimension($this->num2alpha($j))->setAutoSize(true);
            $j++;
            foreach ($question['answers'] as $answer){
                $sheet->setCellValue($this->num2alpha($j,$i), $answer['text'] . $answer['correct'] );
//                $sheet->getColumnDimension($this->num2alpha($j))->setAutoSize(true);

                $sheet->setCellValue($this->num2alpha($j,$i+1), $answer['count']);
//                $sheet->getColumnDimension($this->num2alpha($j))->setAutoSize(true);
                $j++;
            }
//            $sheet->getColumnDimension($this->num2alpha(0))->setAutoSize(true);
            $i = $i+3;
        }

        // se crea el writer
        $writer = $this->factory->createWriter($phpExcelObject, 'Excel5');
        // se crea el response
        $response = $this->factory->createStreamedResponse($writer);

        // y por último se añaden las cabeceras
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    public function getResponseFromModulePoll($params, $filename){
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        /* @var Registry */
        $doctrine = $params['doctrine'];
        /** @var ModulePoll $poll */
        $poll = $doctrine->getRepository(ModulePoll::class)->find($params['pollId']);
        /** @var EmployeeAnswer[]|ArrayCollection $answers */
        $answers = $doctrine->getRepository(EmployeeAnswer::class)->findByPollBetweenDate($poll->getModule(), $params['dateFrom'],$params['dateTo']);

        $results = [];
        foreach ($poll->getQuestions() as $question){
            if($question->getType() == 'MULTCHOICE'){
                $results[$question->getId()]['text']   = $question->getText();
                $results[$question->getId()]['totals'] = 0;
                foreach ($question->getAnswers() as $answer){
                    $results[$question->getId()]['answers'][$answer->getId()]['text']  = $answer->getText();
                    $results[$question->getId()]['answers'][$answer->getId()]['count'] = 0;
                }
            }
        }

        $employees = [];
        foreach ($answers as $answer){

            $employees[$answer->getEmployee()->getId()] = 1;
            /** @var AnswerSnapshot $data */
            $data = $answer->getData();
            if($data->getQuestionType() == 'MULTCHOICE'){
                if(!isset($results[$data->getQuestionId()])){
                    $results[$data->getQuestionId()]['text'] = $data->getAnswerText();
                }
                if(isset($results[$data->getQuestionId()]['answers'][$data->getAnswerId()])){
                    $results[$data->getQuestionId()]['answers'][$data->getAnswerId()]['count']++;
                    $results[$data->getQuestionId()]['totals']++;
                } else {
                    $results[$data->getQuestionId()]['answers'][$data->getAnswerId()]['text']  = $data->getQuestionText();
                    $results[$data->getQuestionId()]['answers'][$data->getAnswerId()]['count'] = 1;
                    $results[$data->getQuestionId()]['totals'] = 1;
                }
            }
        }

        // solicitamos el servicio 'phpexcel' y creamos el objeto vacío...
        $phpExcelObject = $this->factory->createPHPExcelObject();

        // ...y le asignamos una serie de propiedades
        $phpExcelObject->getProperties()
            ->setCreator("Plan de Carrera")
            ->setLastModifiedBy("Plan de Carrera")
            ->setTitle(utf8_decode("Exportación de resultados de Encuestas"))
            ->setSubject("Encuesta")
            ->setDescription("Resultados de Encuesta generado el " . date('d/m/Y') . " a las " . date('H:i:s'))
            ->setKeywords("Plan de carrera, encuesta");

        // establecemos como hoja activa la primera, y le asignamos un título
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->setTitle('Encuesta');

        // escribimos en distintas celdas del documento el título de los campos que vamos a exportar
        $sheet = $phpExcelObject->setActiveSheetIndex(0);

        $sheet->setCellValue('A1', 'Plan: ' . $poll->getModule()->getPlan());
        $sheet->setCellValue('A2', 'Capacitación: ' . $poll->getModule()->getTitle());
        $sheet->setCellValue('A3', 'Desde: ' . (strlen($params['dateFrom']) ? (new \DateTime($params['dateFrom']))->format('d/m/Y'): '' ));
        $sheet->setCellValue('A4', 'Hasta: ' . (strlen($params['dateTo']) ? (new \DateTime($params['dateTo']))->format('d/m/Y'): '' ));
        $sheet->setCellValue('A5', 'Encuestados: ' . count($employees));

//        $i= 6;
//        foreach ($poll->getQuestions() as $question){
//            $sheet->setCellValue('A'.$i, $question->getText());
//            $j = 1;
//            foreach ($question->getAnswers() as $answer){
//                $sheet->setCellValue($this->num2alpha($j,$i), $answer->getText());
//                $sheet->getColumnDimension($this->num2alpha($j))->setAutoSize(true);
//                $j++;
//            }
//            $sheet->getColumnDimension($this->num2alpha(0))->setAutoSize(true);
//            $i = $i+3;
//        }

        $i= 7;
        foreach ($results as $question){
            $sheet->setCellValue('A'.$i, $question['text']);
            $j = 1;
            foreach ($question['answers'] as $answer){
                $sheet->setCellValue($this->num2alpha($j,$i), $answer['text']);
//                $sheet->getColumnDimension($this->num2alpha($j))->setAutoSize(true);

                $sheet->mergeCells($this->num2alpha($j,$i) . ':' . $this->num2alpha($j+1,$i));
                $sheet->getStyle($this->num2alpha($j,$i))->getAlignment()->applyFromArray(
                    array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
                );

                $sheet->setCellValue($this->num2alpha($j,$i+1), 'Cant' );
                $v = (!$question['totals'] ? 0 : number_format( ( $answer['count'] / $question['totals']) * 100, 2));
                $sheet->setCellValue($this->num2alpha($j,$i+2), $answer['count'] );
                $sheet->getColumnDimension($this->num2alpha($j))->setAutoSize(true);
                $j++;

                $sheet->setCellValue($this->num2alpha($j,$i+1), 'Porc' );
                $sheet->setCellValue($this->num2alpha($j,$i+2), $v . '%');
                $sheet->getColumnDimension($this->num2alpha($j))->setAutoSize(true);
                $j++;


            }
            $sheet->getColumnDimension($this->num2alpha(0))->setAutoSize(true);
            $i = $i+4;
        }


        // se crea el writer
        $writer = $this->factory->createWriter($phpExcelObject, 'Excel5');
        // se crea el response
        $response = $this->factory->createStreamedResponse($writer);

        // y por último se añaden las cabeceras
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    public function getResponseFromEmployeeQueryBuilder(QueryBuilder $queryBuilder, $filename)
    {
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        /* @var EntityManager */
        $em = $queryBuilder->getEntityManager();
        /* @var QueryBuilder */
        $queryBuilderPdc = $em->createQueryBuilder()
            ->select('entity')
            ->from(CareerPlan::class, 'entity')
            ->orderBy('entity.position','ASC');

        $pdc      = new ArrayCollection($queryBuilderPdc->getQuery()->getResult());
        $ids      = [];
        $entities = new ArrayCollection($queryBuilder->getQuery()->getResult());

        // solicitamos el servicio 'phpexcel' y creamos el objeto vacío...
        $phpExcelObject = $this->factory->createPHPExcelObject();

        // ...y le asignamos una serie de propiedades
        $phpExcelObject->getProperties()
            ->setCreator("Plan de Carrera")
            ->setLastModifiedBy("Plan de Carrera")
            ->setTitle(utf8_decode("Exportación de Empleados"))
            ->setSubject("Empleados")
            ->setDescription("Listado de empleados generado el " . date('d/m/Y') . " a las " . date('H:i:s'))
            ->setKeywords("Plan de carrera empleados");

        // establecemos como hoja activa la primera, y le asignamos un título
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->setTitle('Empleados');

        // escribimos en distintas celdas del documento el título de los campos que vamos a exportar
        $sheet = $phpExcelObject->setActiveSheetIndex(0);

        $sheet->setCellValue('A1', 'Empleado');
        $sheet->setCellValue('B1', 'DNI');
        $sheet->setCellValue('C1', 'Complejo');
        $sheet->setCellValue('D1', 'Plan Actual');
        $sheet->setCellValue('E1', 'Entrenador');
        $this->addPDCHeaders($phpExcelObject, $pdc, $ids);

        $row = 5;
        foreach ($entities as $employee){

            if(!$employee->getUser()->isEnabled()){
                continue;
            }

            if($employee->getCareerPlans()->isEmpty()){
//                continue;
            }
            $sheet->setCellValue('A'.$row, trim($employee->getProfile()->getName()));
            $sheet->setCellValue('B'.$row, trim($employee->getUser()->getUsername()));
            if($employee->getStore()){
                $sheet->setCellValue('C'.$row, $employee->getStore());
            }
            if($employee->getCurrentPlan()){
                $sheet->setCellValue('D'.$row, $employee->getCurrentPlan()->getTitle());
            }
            if($employee->getTrainer()){
                $sheet->setCellValue('E'.$row, $employee->getTrainer());
            }

            $sheet->mergeCells($this->num2alpha(0,1) . ':' . $this->num2alpha(0,4));
            $sheet->mergeCells($this->num2alpha(1,1) . ':' . $this->num2alpha(1,4));
            $sheet->mergeCells($this->num2alpha(2,1) . ':' . $this->num2alpha(2,4));
            $sheet->mergeCells($this->num2alpha(3,1) . ':' . $this->num2alpha(3,4));
            $sheet->mergeCells($this->num2alpha(4,1) . ':' . $this->num2alpha(4,4));

            if($employee->getCareerPlans()){
                foreach ($employee->getCareerPlans() as $careerPlan){
                    $cell = $ids[$careerPlan->getPlan()->getId()]['cell'];
                    $sheet->setCellValue($this->num2alpha($cell,$row), ($careerPlan->isEnabled() ? (!is_null($careerPlan->getEnabledAt()) ? $careerPlan->getEnabledAt()->format('d/m/Y') : 'Si' ) : 'No'));
                    $sheet->setCellValue($this->num2alpha($cell+1,$row), ($careerPlan->isApproved() ? ( !is_null($careerPlan->getApprovedAt()) ? $careerPlan->getApprovedAt()->format('d/m/Y') : 'Si') : 'No'));

                    foreach ($careerPlan->getRequirements() as $requirement){
                        if(!isset($ids[$careerPlan->getPlan()->getId()])) continue;
                        if(!isset($ids[$careerPlan->getPlan()->getId()]['requirement'])) continue;
                        if(!isset($ids[$careerPlan->getPlan()->getId()]['requirement'][$requirement->getRequirement()->getId()])) continue;
                        $cell = $ids[$careerPlan->getPlan()->getId()]['requirement'][$requirement->getRequirement()->getId()]['enabled'];
                        $sheet->setCellValue($this->num2alpha($cell,$row), ($requirement->isEnabled() ? ( !is_null($requirement->getEnabledAt()) ? $requirement->getEnabledAt()->format('d/m/Y') : 'Si') : 'No'));
                        $cell = $ids[$careerPlan->getPlan()->getId()]['requirement'][$requirement->getRequirement()->getId()]['approved'];
                        $sheet->setCellValue($this->num2alpha($cell,$row), ($requirement->isApproved() ? ( !is_null($requirement->getApprovedAt()) ? $requirement->getApprovedAt()->format('d/m/Y') : 'Si' ): 'No'));
                    }

                    foreach ($careerPlan->getModules() as $module){
                        if(!isset($ids[$careerPlan->getPlan()->getId()])) continue;
                        if(!isset($ids[$careerPlan->getPlan()->getId()]['module'])) continue;
                        if(!isset($ids[$careerPlan->getPlan()->getId()]['exam'])) continue;
                        if(!isset($ids[$careerPlan->getPlan()->getId()]['module'][$module->getModule()->getId()])) continue;
                        $cell = $ids[$careerPlan->getPlan()->getId()]['module'][$module->getModule()->getId()]['enabled'];
                        $sheet->setCellValue($this->num2alpha($cell,$row), ($module->isEnabled() ? (!is_null($module->getEnabledAt()) ? $module->getEnabledAt()->format('d/m/Y') : 'Si' ) : 'No'));
                        $cell = $ids[$careerPlan->getPlan()->getId()]['module'][$module->getModule()->getId()]['approved'];
                        $sheet->setCellValue($this->num2alpha($cell,$row), ($module->isApproved() ? (!is_null($module->getApprovedAt()) ? $module->getApprovedAt()->format('d/m/Y') : 'Si') : 'No'));

                        if($module->getExamPermissions()->count()){
                            $permission = $module->getExamPermission();
                            if($permission->isEnabled()){
                                if($permission->getEmployeeExams()->count()){
                                    $i=1;
                                    foreach ($permission->getEmployeeExams() as $test){
                                        $cell = $ids[$careerPlan->getPlan()->getId()]['module'][$module->getModule()->getId()]['exam'][$test->getExam()->getId()][1];
                                        $sheet->setCellValue($this->num2alpha($cell,$row), (is_null($test->isApproved()) ? 'Pendiente Finalizar' : ($test->isApproved() ? 'Aprobado' : 'Reprobado' ) ));
                                        $i++;
                                    }
                                } else {
                                    $cell = $ids[$careerPlan->getPlan()->getId()]['module'][$module->getModule()->getId()]['exam'][$permission->getModuleExam()->getId()][1];
                                    $sheet->setCellValue($this->num2alpha($cell,$row), 'Pendiente');
                                }

                            }
                        }
                    }
                }
            }
            $row++;
        }

        // se crea el writer
        $writer = $this->factory->createWriter($phpExcelObject, 'Excel5');
        // se crea el response
        $response = $this->factory->createStreamedResponse($writer);

        // y por último se añaden las cabeceras
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    private function addPDCHeaders(&$phpExcelObject, $pdc, &$ids)
    {
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
        $col1 = 5;
        $col2 = $col3 = $col4 = 5;

        foreach ($pdc as $plan){
            $sheet->setCellValue($this->num2alpha($col1, 1), $plan->getTitle());
            $ids[$plan->getId()]['cell'] = $col1;
            $sheet->getStyle($this->num2alpha($col1,1))->applyFromArray(['font' => ['bold' => true, 'italic' => true]]);
            $sheet->setCellValue($this->num2alpha($col2,2), 'Habilitado');
            $sheet->mergeCells($this->num2alpha($col2,2) . ':' . $this->num2alpha($col2,4));
            $col2++;
            $sheet->setCellValue($this->num2alpha($col2,2), 'Aprobado');
            $sheet->mergeCells($this->num2alpha($col2,2) . ':' . $this->num2alpha($col2,4));
            $col2++;
            $col3 = $col4 = $col2;

            $sheet->setCellValue($this->num2alpha($col2,2), 'Requerimientos');
            $sheet->getStyle($this->num2alpha($col2,2))->getAlignment()->applyFromArray(['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER]);
            $sheet->getStyle($this->num2alpha($col2,2))->applyFromArray(['font' => ['bold' => true]]);
            foreach ($plan->getRequirements() as $requirement){
                $sheet->setCellValue($this->num2alpha($col3,3), $requirement->getDescription());
                $sheet->setCellValue($this->num2alpha($col4,4), 'Habilitado');
                $ids[$plan->getId()]['requirement'][$requirement->getId()]['enabled']  = $col4;
                $col4++;
                $sheet->setCellValue($this->num2alpha($col4,4), 'Aprobado');
                $ids[$plan->getId()]['requirement'][$requirement->getId()]['approved'] = $col4;
                $col4++;
                $sheet->mergeCells($this->num2alpha($col3,3) . ':' . $this->num2alpha($col4-1,3));
                $col3 = $col4;
            }
            if($plan->getRequirements()->count()){
                $sheet->mergeCells($this->num2alpha($col2,2) . ':' . $this->num2alpha($col4-1,2));
                $col2 = $col3;
            }

            $sheet->setCellValue($this->num2alpha($col2,2), 'Módulos Nivelación');
            $sheet->getStyle($this->num2alpha($col2,2))->getAlignment()->applyFromArray(['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER]);
            $sheet->getStyle($this->num2alpha($col2,2))->applyFromArray(['font' => ['bold' => true]]);
            $niv = 0;
            foreach ($plan->getModules() as $module){
                if ($module->isNivelation()){
                    $niv++;
                    $sheet->setCellValue($this->num2alpha($col3,3), $module->getTitle());
                    $sheet->setCellValue($this->num2alpha($col4,4), 'Habilitado');
                    $ids[$plan->getId()]['module'][$module->getId()]['enabled'] = $col4;
                    $col4++;
                    $sheet->setCellValue($this->num2alpha($col4,4), 'Aprobado');
                    $ids[$plan->getId()]['module'][$module->getId()]['approved'] = $col4;
                    $col4++;

                    if($module->getExam()){
                        for ($i = 1; $i <= $module->getExam()->getMaxAttempts(); $i++){
                            $sheet->setCellValue($this->num2alpha($col4,4), sprintf("Examen #%s", $i));
                            $ids[$plan->getId()]['module'][$module->getId()]['exam'][$module->getExam()->getId()][$i] = $col4;
                            $col4++;
                        }
                    }

                    $sheet->mergeCells($this->num2alpha($col3,3) . ':' . $this->num2alpha($col4-1,3));
                    $col3 = $col4;
                }
            }
            if($niv){
                $sheet->mergeCells($this->num2alpha($col2,2) . ':' . $this->num2alpha($col4-1,2));
                $col2 = $col3;
            }

            $sheet->setCellValue($this->num2alpha($col2,2), 'Capacitaciones');
            $sheet->getStyle($this->num2alpha($col2,2))->getAlignment()->applyFromArray(['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER]);
            $sheet->getStyle($this->num2alpha($col2,2))->applyFromArray(['font' => ['bold' => true]]);
            $mod = 0;
            foreach ($plan->getModules() as $module){
                if ((!$module->isNivelation()) and ($module->getType() == 'module')){
                    $mod++;
                    $sheet->setCellValue($this->num2alpha($col3,3), $module->getTitle());
                    $sheet->setCellValue($this->num2alpha($col4,4), 'Habilitado');
                    $ids[$plan->getId()]['module'][$module->getId()]['enabled'] = $col4;
                    $col4++;
                    $sheet->setCellValue($this->num2alpha($col4,4), 'Aprobado');
                    $ids[$plan->getId()]['module'][$module->getId()]['approved'] = $col4;
                    $col4++;

                    if($module->getExam()){
                        for ($i = 1; $i <= $module->getExam()->getMaxAttempts(); $i++){
                            $sheet->setCellValue($this->num2alpha($col4,4), sprintf("Examen #%s", $i));
                            $ids[$plan->getId()]['module'][$module->getId()]['exam'][$module->getExam()->getId()][$i] = $col4;
                            $col4++;
                        }
                    }

                    $sheet->mergeCells($this->num2alpha($col3,3) . ':' . $this->num2alpha($col4-1,3));
                    $col3 = $col4;
                }
            }
            if($mod){
                $sheet->mergeCells($this->num2alpha($col2,2) . ':' . $this->num2alpha($col4-1,2));
                $col2 = $col3;
            }

            $sheet->setCellValue($this->num2alpha($col2,2), 'Talleres');
            $sheet->getStyle($this->num2alpha($col2,2))->getAlignment()->applyFromArray(['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER]);
            $sheet->getStyle($this->num2alpha($col2,2))->applyFromArray(['font' => ['bold' => true]]);
            $tal = 0;
            foreach ($plan->getModules() as $module){
                if ((!$module->isNivelation()) and ($module->getType() == 'workshop')){
                    $tal++;
                    $sheet->setCellValue($this->num2alpha($col3,3), $module->getTitle());
                    $sheet->setCellValue($this->num2alpha($col4,4), 'Habilitado');
                    $ids[$plan->getId()]['module'][$module->getId()]['enabled'] = $col4;
                    $col4++;
                    $sheet->setCellValue($this->num2alpha($col4,4), 'Aprobado');
                    $ids[$plan->getId()]['module'][$module->getId()]['approved'] = $col4;
                    $col4++;

                    if($module->getExam()){
                        for ($i = 1; $i <= $module->getExam()->getMaxAttempts(); $i++){
                            $sheet->setCellValue($this->num2alpha($col4,4), sprintf("Examen #%s", $i));
                            $ids[$plan->getId()]['module'][$module->getId()]['exam'][$module->getExam()->getId()][$i] = $col4;
                            $col4++;
                        }
                    }

                    $sheet->mergeCells($this->num2alpha($col3,3) . ':' . $this->num2alpha($col4-1,3));
                    $col3=$col4;
                }
            }
            if($tal){
                $sheet->mergeCells($this->num2alpha($col2,2) . ':' . $this->num2alpha($col4-1,2));
                $col2 = $col3;
            }
            if($plan->getRequirements() or $plan->getModules()){
                $sheet->mergeCells($this->num2alpha($col1,1) . ':' . $this->num2alpha($col4-1,1));
                $col1 = $col4;
            } else {
                $col1++;
            }

        }

        for ($col = 0; $col <= $col4 ; $col++) {
            $col1 = $this->num2alpha($col);
            $sheet->getColumnDimension($col1)->setAutoSize(true);
        }
    }


    /**
     * @param int $col
     * @param int $row
     * @return string
     */
    private function num2alpha($col,$row = null)
    {
        for($r = ""; $col >= 0; $col = intval($col / 26) - 1)
            $r = chr($col%26 + 0x41) . $r;
        return $r.(!is_null($row) ? $row : '');
    }
}
