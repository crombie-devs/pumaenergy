<?php

namespace Aper\PlanCarreraBundle\Service;

use Aper\PlanCarreraBundle\Entity\EmployeeExamAnswer;
use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;
use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\PlanCarreraBundle\Entity\ModuleExamAnswer;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\ModuleExamWasApproved;
use Aper\PlanCarreraBundle\Event\ModuleExamWasDisapproved;
use Aper\PlanCarreraBundle\Exception\ExamException;
use Aper\PlanCarreraBundle\Repository\EmployeeModuleExamRepository;
use Aper\UserBundle\Entity\Employee;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Examiner
{
    /**
     * @var EmployeeModuleExamRepository
     */
    private $testRepository;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * Examiner constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->testRepository = $em->getRepository('PlanCarreraBundle:EmployeeModuleExam');
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param ModuleExamAnswer $answer
     * @param Employee $employee
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function answer(ModuleExamAnswer $answer, Employee $employee, $prepost)
    {
//        $answer->checkEnabledTree();

        $exam = $answer->getQuestion()->getExam();
        /** @var EmployeeModuleExam $test */
        $test = $this->testRepository->findLastByExam($exam, $employee, $prepost);

        if ($test && !$test->canBeAnswered()) {//pueden ser diferentes las causas
            if($test->isTimeOut()){
                $this->review($test);
                $this->testRepository->save($test);

                if($test->isApproved()){
                    throw new ExamException('APPROVED', "Felicitaciones, aprobaste el Examen");
                } else {
                    $nextChance = $test->getNextChanceDatetime();
                    if($nextChance === false){
                        throw new ExamException('NO_APPROVED', "No puede volver a intentar realizar el examen, usted ya agotó los intentos para este examen.");
                    } else {
                        throw new ExamException('NO_APPROVED', "No puede volver a intentar realizar el examen hasta el " . $nextChance->format('d/m/Y') . " a las " . $nextChance->format('H:i:s') . " hs");
                    }
                }
            }
            throw new \LogicException("Debe esperar {$test->getExam()->getDaysForRetry()} dias desde su ultimo intento para volver a realizar este examen.");
        }
        $testAnswer = $test->getAnswerByQuestion($answer->getQuestion());
        if (!$testAnswer) {
            $testAnswer = new EmployeeExamAnswer();
            $testAnswer->setAnswer($answer);
            $testAnswer->setQuestion($answer->getQuestion());
            $testAnswer->setEmployeeExam($test);
        }
        $testAnswer->setAnswer($answer);
        $test->replaceAnswer($testAnswer);

        $this->testRepository->save($test);
    }

    /**
     * @param ModuleExam $exam
     * @param Employee $employee
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function finish(ModuleExam $exam, Employee $employee, $prepost)
    {

        /** @var EmployeeModuleExam $test */
        $test = $this->testRepository->findLastByExam($exam, $employee, $prepost);

        if ($test && !$test->canBeAnswered() ) {//pueden ser diferentes las causas
            if ($test->isTimeOut()){
                $this->review($test);
                $test->finish();
                $this->testRepository->save($test);

                if($test->isApproved()){

                    $event = new ModuleExamWasApproved($employee, $exam);
                    $this->dispatcher->dispatch(CareerPlanEvents::EXAM_WAS_APPROVED, $event);

                    throw new ExamException('APPROVED', "Felicitaciones, aprobaste el Examen");
                } else {

                    $event      = new ModuleExamWasDisapproved($employee, $exam);
                    $nextChance = $test->getNextChanceDatetime();

                    if($nextChance === false){
//                        $this->dispatcher->dispatch(CareerPlanEvents::EXAM_HAS_BEEN_FAILED, $event);
                        $this->dispatcher->dispatch(CareerPlanEvents::EXAM_HAS_BEEN_FAILED_AGAIN, $event);
                        throw new ExamException('NO_APPROVED', "No puede volver a intentar realizar el examen, usted ya agotó los intentos para este examen.");
                    } else {
                        $this->dispatcher->dispatch(CareerPlanEvents::EXAM_HAS_BEEN_FAILED, $event);
                        throw new ExamException('NO_APPROVED', "No puede volver a intentar realizar el examen hasta el " . $nextChance->format('d/m/Y') . " a las " . $nextChance->format('H:i:s') . " hs");
                    }
                }
            }
            throw new \LogicException("Debe esperar {$test->getExam()->getDaysForRetry()} días desde su ultimo intento para volver a realizar este examen.");
        }

        if ($test->isCompleted()) {
            $test->finish();
            $this->review($test);

            $this->testRepository->save($test);

            if($test->isApproved()){

                $event = new ModuleExamWasApproved($employee, $exam);
                $this->dispatcher->dispatch(CareerPlanEvents::EXAM_WAS_APPROVED, $event);

//                throw new ExamException('APPROVED', "Felicitaciones, aprobaste el Examen");
            } else {
                $event      = new ModuleExamWasDisapproved($employee, $exam);
                $nextChance = $test->getNextChanceDatetime();
                if($nextChance === false){
                    $this->dispatcher->dispatch(CareerPlanEvents::EXAM_HAS_BEEN_FAILED_AGAIN, $event);
                    throw new ExamException('NO_APPROVED', "No puede volver a intentar realizar el examen, usted ya agotó los intentos para este examen.");
                } else {
                    $this->dispatcher->dispatch(CareerPlanEvents::EXAM_HAS_BEEN_FAILED, $event);
                    throw new ExamException('NO_APPROVED', "No puede volver a intentar realizar el examen hasta el " . $nextChance->format('d/m/Y') . " a las " . $nextChance->format('H:i:s') . " hs");
                }
            }
        }

        $this->testRepository->save($test);
    }

    /**
     * @param ModuleExam $exam
     * @param Employee $employee
     * @return EmployeeModuleExam
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function start(ModuleExam $exam, Employee $employee, $prepost)
    {
        /** @var EmployeeModuleExam $test */
        $test = $this->testRepository->findLastByExam($exam, $employee, $prepost);

        if ($test && !$test->canBeStarted()) {//pueden ser diferentes las causas
            $nextChance = $test->getNextChanceDatetime();
            if($nextChance > new \DateTime()){
                throw new \LogicException("No puede volver a intentar realizar el examen hasta el " . $nextChance->format('d/m/Y') . " a las " . $nextChance->format('H:i:s') . " hs");
            }
            $tests = $this->testRepository->findByEmployeeAndExam($exam, $employee);
            if (count($tests) >= 3) {
                throw new \LogicException("Usted ya agotó los 2 intentos para este Examen.");
            }
            throw new \LogicException("Debe esperar {$test->getExam()->getDaysForRetry()} días desde su ultimo intento para volver a realizar este examen.");
        }

        if (!$test || ($test->isFinished() || $test->isTimeOut())) {
            $tests = $this->testRepository->findByEmployeeAndExam($exam, $employee);
            if (count($tests) >= 3) {
                throw new \LogicException("Usted ya agotó los 2 intentos para este Examen.");
            }

            $test = EmployeeModuleExam::create($employee, $exam, $prepost);
        }

        $this->testRepository->save($test);

        foreach ($test->getAnswers() as $answer){
            $question = $answer->getQuestion();
            $options  = $question->getAnswers()->toArray();
            shuffle($options);
            $optionsReordered = new ArrayCollection();
            foreach ($options as $option){
                $optionsReordered->add($option);
            }
            $question->setAnswers($optionsReordered);
        }

        return $test;
    }

    /**
     * @param EmployeeModuleExam $test
     */
    private function review(EmployeeModuleExam $test)
    {
        $test->setApproved(
            $test->getCorrects()->count() >= $test->getExam()->getCorrectAnswersToApprove()
        );
    }
}