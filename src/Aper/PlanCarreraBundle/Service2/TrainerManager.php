<?php

namespace Aper\PlanCarreraBundle\Service;

use Aper\PlanCarreraBundle\CQRS\Command\AssignTrainer;
use Aper\PlanCarreraBundle\Event\CareerPlanEvents;
use Aper\PlanCarreraBundle\Event\TrainerWasAssigned;
use Aper\PlanCarreraBundle\Event\TrainerWasUnassigned;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class TrainerManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * TrainerManager constructor.
     * @param EntityManagerInterface $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param AssignTrainer $command
     */
    public function assign(AssignTrainer $command)
    {
        $employee = $command->getEmployee();
        $trainer = $command->getTrainer();
        $oldTrainer = $employee->getTrainer();
        $employee->setTrainer($trainer);

        $this->em->persist($employee);
        $this->em->flush();

        if ($trainer) {
            $event = new TrainerWasAssigned($employee, $trainer);
            $this->dispatcher->dispatch(CareerPlanEvents::TRAINER_WAS_ASSIGNED, $event);
        } elseif($oldTrainer) {
            $event = new TrainerWasUnassigned($employee, $oldTrainer);
            $this->dispatcher->dispatch(CareerPlanEvents::TRAINER_WAS_UNASSIGNED, $event);
        }
    }

}