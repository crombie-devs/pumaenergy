<?php

namespace Aper\PlanCarreraBundle\Repository;

/**
 * CareerPlanRepository
 */
class CareerPlanRepository extends \Doctrine\ORM\EntityRepository
{

    public function findAllWithOrder()
    {
        return $this->createQueryBuilder('CareerPlan')
            ->orderBy('CareerPlan.position','ASC')
            ->getQuery()->getResult();
    }

}
