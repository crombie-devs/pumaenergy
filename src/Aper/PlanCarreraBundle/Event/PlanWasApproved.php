<?php
namespace Aper\PlanCarreraBundle\Event;

use Aper\PlanCarreraBundle\Entity\CareerPlan;
use Aper\UserBundle\Entity\Employee;
use Symfony\Component\EventDispatcher\Event;

class PlanWasApproved extends Event
{
    /**
     * @var Employee
     */
    private $employee;
    /**
     * @var CareerPlan
     */
    private $plan;

    /**
     * AssignTrainer constructor.
     * @param Employee $employee
     * @param CareerPlan $plan
     */
    public function __construct(Employee $employee, CareerPlan $plan)
    {
        $this->employee = $employee;
        $this->plan = $plan;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return CareerPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

}