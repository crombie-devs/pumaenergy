<?php
namespace Aper\PlanCarreraBundle\Event;

use Aper\PlanCarreraBundle\Entity\ModulePoll;
use Aper\UserBundle\Entity\Employee;
use Symfony\Component\EventDispatcher\Event;

class ModulePollWasDeactivated extends Event
{
    /**
     * @var Employee
     */
    private $employee;
    /**
     * @var ModulePoll
     */
    private $modulePoll;

    /**
     * ModuleEvent constructor.
     * @param Employee $employee
     * @param ModulePoll $modulePoll
     */
    public function __construct(Employee $employee, ModulePoll $modulePoll)
    {
        $this->employee   = $employee;
        $this->modulePoll = $modulePoll;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return ModulePoll
     */
    public function getModulePoll()
    {
        return $this->modulePoll;
    }

}