<?php
namespace Aper\PlanCarreraBundle\Event;

use Aper\UserBundle\Entity\Employee;
use Symfony\Component\EventDispatcher\Event;

class TrainerWasUnassigned extends Event
{
    /**
     * @var Employee
     */
    private $employee;
    /**
     * @var Employee
     */
    private $trainer;

    /**
     * AssignTrainer constructor.
     * @param Employee $employee
     * @param Employee $trainer
     */
    public function __construct(Employee $employee, Employee $trainer)
    {
        $this->employee = $employee;
        $this->trainer = $trainer;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return Employee
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

}