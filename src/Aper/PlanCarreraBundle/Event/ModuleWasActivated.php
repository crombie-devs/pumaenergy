<?php
namespace Aper\PlanCarreraBundle\Event;

use Aper\PlanCarreraBundle\Entity\Module;
use Aper\UserBundle\Entity\Employee;
use Symfony\Component\EventDispatcher\Event;

class ModuleWasActivated extends Event
{
    /**
     * @var Employee
     */
    private $employee;
    /**
     * @var Module
     */
    private $module;

    /**
     * ModuleEvent constructor.
     * @param Employee $employee
     * @param Module $module
     */
    public function __construct(Employee $employee, Module $module)
    {
        $this->employee = $employee;
        $this->module   = $module;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return Module
     */
    public function getModule()
    {
        return $this->module;
    }

}