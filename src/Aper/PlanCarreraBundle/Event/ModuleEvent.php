<?php

namespace Aper\PlanCarreraBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class ModuleEvent extends Event
{
    /**
     * @var integer
     */
    private $moduleId;

    /**
     * ModuleEvent constructor.
     * @param int $moduleId
     */
    public function __construct($moduleId)
    {
        $this->moduleId = $moduleId;
    }

    /**
     * @return int
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

}