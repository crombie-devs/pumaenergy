<?php

namespace Aper\PlanCarreraBundle\Event;

class CareerPlanEvents
{
    //1. Mail indicando que se le habilitó o aprobó un módulo o examen (y tiene una semana para hacerlo).
    const MODULE_WAS_ACTIVATED = 'module_was_activated';
    const MODULE_WAS_DEACTIVATED = 'module_was_deactivated';
    const EXAM_WAS_ACTIVATED = 'exam_was_activated';
    const EXAM_WAS_DEACTIVATED = 'exam_was_deactivated';
    const POLL_WAS_ACTIVATED = 'poll_was_activated';
    const POLL_WAS_DEACTIVATED = 'poll_was_deactivated';
    const MODULE_WAS_APPROVED = 'module_was_approved';
    const MODULE_WAS_NOT_APPROVED = 'module_was_not_approved';
    const EXAM_WAS_APPROVED = 'exam_was_approved';
    //- Evaluaciones presenciales: //1. Mail indicando que aprobó el examen
    //2. A los (x-2)  dias de no haber aprobado una evaluación, le avisa que se le volvió a activar.
    const EXAM_WAS_ACTIVATED_AGAIN = 'exam_was_activated_again';
    //3. Cuando rinde mal un examen, le llega un mail indicando que dentro de x días puede volver a hacerla.
    const EXAM_HAS_BEEN_FAILED = 'exam_has_been_failed';
    //4. Cuando rinde mal por segunda vez, recibe un mail. El trainer también recibe un mail.
    const EXAM_HAS_BEEN_FAILED_AGAIN = 'exam_has_been_failed_again';
    //5. Cuando falte 1 día para el vencimiento del examen y no lo hizo.
    const EXAM_REMINDER = 'exam_reminder';
    //6. Cuando un usuario no hace la evaluación y se venció, el trainer recibe un mail indicando que el usr no lo hizo.
    const EXAM_WAS_EXPIRED = 'exam_was_expired';

    //- Cuando se le asigna un trainer a un usuario (ambos reciben un mail)
    const TRAINER_WAS_ASSIGNED = 'trainer_was_assigned';
    const TRAINER_WAS_UNASSIGNED = 'trainer_was_unassigned';

    //- Cuando te asignan un plan de nivelación, se te manda un mail
//    const LEVELING_MODULE_WAS_ASSIGNED = 'leveling_module_was_assigned';

    //- Cuando te inscribes a un taller, 24 hs antes del evento, te manda un mail de recordatorio.
    const EXTERNAL_TRAINING_INSCRIPTION_IS_COMING = 'external_training_inscription_is_coming';

    //5. Notificaciones para RRHH + GE Reciben:
    //- Cuando un usuario está llegando al fin del plazo de cumplimiento de un módulo,
    const MODULE_IS_EXPIRING = 'module_is_expiring';

    //- Cuando tiene pendiente de aprobarle un requisito/modulo a un usuario
    const REQUIREMENT_IS_WAITING_FOR_APPROVAL = 'requirement_is_waiting_for_approval';
    const MODULE_IS_WAITING_FOR_APPROVAL = 'module_is_waiting_for_approval';

    //- Cuando un usuario se asoció a un pdc
    const CAREER_PLAN_WAS_ASSIGNED = 'career_plan_was_assigned';

    //- Cuando un usuario ha aprobado un Plan de Carrera
    const CAREER_PLAN_WAS_APPROVED = 'career_plan_was_approved';

    //- Cuando un usuario ha desaprobado un Plan de Carrera
    const CAREER_PLAN_WAS_NOT_APPROVED = 'career_plan_was_not_approved';

    //- Cuando un usuario mira un video de un módulo
    const MODULE_YOUTUBE_WAS_WATCHED = 'module_youtube_was_watched';
    //- Cuando un usuario descarga un archivo de un módulo
    const MULTIMEDIA_WAS_DOWNLOADED = 'multimedia_was_downloaded';

    // Cuando un entrenador, gerente de entrenamiento o RRHH deja un comentario en el Módulo de 1 empleado
    const COMMENT_RECEIVED = 'comment_received';

    const EXTERNAL_TRAINING_SUBSCRIBED = 'external_training_subscribed';
    const EXTERNAL_TRAINING_UNSUBSCRIBED = 'external_training_unsubscribed';

    // Estos eventos son por cron
    //EXAM_REMINDER, EXTERNAL_TRAINING_INSCRIPTION_IS_COMING, MODULE_IS_EXPIRING, REQUIREMENT_IS_WAITING_FOR_APPROVAL,
    // MODULE_IS_WAITING_FOR_APPROVAL
}