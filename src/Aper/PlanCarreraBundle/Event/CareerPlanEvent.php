<?php

namespace Aper\PlanCarreraBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class CareerPlanEvent extends Event
{
    /**
     * @var integer
     */
    private $careerPlanId;

    /**
     * @var integer
     */
    private $employeeId;

    /**
     * CareerPlanEvent constructor.
     * @param integer $careerPlanId
     * @param integer $employeeId
     */
    public function __construct($careerPlanId, $employeeId)
    {
        $this->careerPlanId = $careerPlanId;
        $this->employeeId = $employeeId;
    }

    /**
     * @return integer
     */
    public function getCareerPlanId()
    {
        return $this->careerPlanId;
    }

    /**
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

}