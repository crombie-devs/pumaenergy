<?php
namespace Aper\PlanCarreraBundle\Event;

use Aper\PlanCarreraBundle\Entity\EmployeeCareerModule;
use Aper\UserBundle\Entity\Employee;
use Symfony\Component\EventDispatcher\Event;

class CommentWasReceived extends Event
{
    /**
     * @var Employee
     */
    private $employee;
    /**
     * @var EmployeeCareerModule
     */
    private $module;

    /**
     * ModuleEvent constructor.
     * @param Employee $employee
     * @param EmployeeCareerModule $module
     */
    public function __construct(Employee $employee, EmployeeCareerModule $module)
    {
        $this->employee = $employee;
        $this->module   = $module;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return EmployeeCareerModule
     */
    public function getModule()
    {
        return $this->module;
    }

}