<?php
namespace Aper\PlanCarreraBundle\Event;

use Aper\PlanCarreraBundle\Entity\ExternalTraining;
use Aper\UserBundle\Entity\Employee;
use Symfony\Component\EventDispatcher\Event;

class ExternalTrainingIsComing extends Event
{
    /**
     * @var Employee
     */
    private $employee;
    /**
     * @var ExternalTraining
     */
    private $externalTraining;

    /**
     * AssignTrainer constructor.
     * @param Employee $employee
     * @param ExternalTraining $externalTraining
     */
    public function __construct(Employee $employee, ExternalTraining $externalTraining)
    {
        $this->employee = $employee;
        $this->externalTraining = $externalTraining;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return ExternalTraining
     */
    public function getExternalTraining()
    {
        return $this->externalTraining;
    }

}