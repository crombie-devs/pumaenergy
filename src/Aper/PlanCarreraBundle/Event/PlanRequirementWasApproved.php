<?php
namespace Aper\PlanCarreraBundle\Event;

use Aper\PlanCarreraBundle\Entity\PlanRequirement;
use Aper\UserBundle\Entity\Employee;
use Symfony\Component\EventDispatcher\Event;

class PlanRequirementWasApproved extends Event
{
    /**
     * @var Employee
     */
    private $employee;
    /**
     * @var PlanRequirement
     */
    private $requirement;

    /**
     * ModuleEvent constructor.
     * @param Employee $employee
     * @param PlanRequirement $requirement
     */
    public function __construct(Employee $employee, PlanRequirement $requirement)
    {
        $this->employee    = $employee;
        $this->requirement = $requirement;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return PlanRequirement
     */
    public function getPlanRequirement()
    {
        return $this->requirement;
    }

}