<?php
namespace Aper\PlanCarreraBundle\Event;

use Aper\PlanCarreraBundle\Entity\ModuleExam;
use Aper\UserBundle\Entity\Employee;
use Symfony\Component\EventDispatcher\Event;

class ModuleExamWasActivated extends Event
{
    /**
     * @var Employee
     */
    private $employee;
    /**
     * @var ModuleExam
     */
    private $moduleExam;

    /**
     * ModuleEvent constructor.
     * @param Employee $employee
     * @param ModuleExam $moduleExam
     */
    public function __construct(Employee $employee, ModuleExam $moduleExam)
    {
        $this->employee   = $employee;
        $this->moduleExam = $moduleExam;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return ModuleExam
     */
    public function getModuleExam()
    {
        return $this->moduleExam;
    }

}