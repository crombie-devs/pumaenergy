<?php

namespace Aper\DevelopBundle\Controller;

use Aper\DevelopBundle\Form\CompetitionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Aper\DevelopBundle\Entity\Competition;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class CompetitionController extends Controller
{

    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $competitions = $em->getRepository('AperDevelopBundle:Competition')->findall();
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $competitions, $request->query->getInt('page', 1),
            1000
        );

        return $this->render('AperDevelopBundle:Competition:index.html.twig',
            array('pagination' => $pagination, 'competitions' => $competitions));

    }


    public function addAction(Request $request)
    {
        $competition = new Competition();
        $form = $this->createForm('Aper\DevelopBundle\Form\CompetitionType', $competition);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($competition);
            $em->flush($competition);

            return $this->redirectToRoute('aper_competition_show', array('id' => $competition->getId()));
        }

        return $this->render('AperDevelopBundle:Competition:add.html.twig', array(
            'competition' => $competition,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $competition = $em->getRepository('AperDevelopBundle:Competition')->find($id);
        $form = $this->createForm(CompetitionType::class, $competition);
        $form->handleRequest($request);

        if (!$competition) {
            throw $this->createNotFoundException('Competencia no encontrada');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'La Competencia ha sido modificada');

            return $this->redirectToRoute('aper_competition_index');
        }

        return $this->render('AperDevelopBundle:Competition:update.html.twig', ['competition' => $competition, 'form' => $form->createView()]);
    }


    public function showAction($id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AperDevelopBundle:Competition');

        $competition = $repository->find($id);

        if (!$competition) {
            $messageException = 'Competencia no encontrada';
            throw $this->createNotFoundException($messageException);
        }

        return $this->render('AperDevelopBundle:Competition:show.html.twig', ['competition' => $competition]);
    }

    public function deleteAction($id, Request $request)
    {
            $em = $this->getDoctrine()->getManager();
            $competition = $em->getRepository('AperDevelopBundle:Competition')->find($id);
            $em->remove($competition);
            $em->flush();
            $successMessage = 'Competencia eliminada';
            $this->addFlash('success', $successMessage);

        return $this->redirectToRoute('aper_competition_index');
    }

}
