<?php

namespace Aper\DevelopBundle\Controller;

use Aper\DevelopBundle\Form\CompetitionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Aper\DevelopBundle\Entity\Competition;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FrontEndCompetitionController
 *
 * @Route("competition")
 */
class FrontEndCompetitionController extends Controller
{
/**
     * @Route("/")
     *
     * @param
     * @return Response
     */
    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $competitions = $em->getRepository('AperDevelopBundle:Competition')->findall();
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $competitions, $request->query->getInt('page', 1),
            20
        );

        return $this->render('AperDevelopBundle:Frontend:index.html.twig',
            array('pagination' => $pagination, 'competitions' => $competitions));

    }




}
