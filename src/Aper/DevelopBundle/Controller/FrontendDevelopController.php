<?php

namespace Aper\DevelopBundle\Controller;

use Aper\BenefitBundle\Model\Category;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WebFactory\Bundle\UserBundle\Controller\ContextController;

/**
 * Class DevelopController
 *
 * @Route("develop")
 */
class FrontendDevelopController extends ContextController
{
    /**
     * @Route("/")
     *
     * @param
     * @return Response
     */
    public function indexAction()
    {
        //$repository = $this->getDoctrine()->getRepository('AperCommunicationBundle:Communication');
        $develops = [];


        return $this->render('AperDevelopBundle:Frontend/Develop:index.html.twig', [
            'develops' => $develops,
        ]);
    }

    /**
     * @Route("/show/{category}")
     *
     * @param
     * @return Response
     */
    public function showAction($category)
    {
        //$repository = $this->getDoctrine()->getRepository('AperBenefitBundle:Benefit');
        $develops = null; //$repository->findByRolesAllowed($this->getUser()->getRoles(), $category);

        return $this->render('AperDevelopBundle:Frontend/Develop:show.html.twig', [
                       'develops' => $develops,
        ]);
    }
}