<?php

namespace Aper\NewsBundle\DataFixtures\ORM;

use Aper\NewsBundle\Entity\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategories extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $category1 = new Category('Category 1');
        $category2 = new Category('Category 2');

        $manager->persist($category1);
        $manager->persist($category2);


        $manager->flush();

        $this->addReference('news-category-1', $category1);
        $this->addReference('news-category-2', $category2);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

}