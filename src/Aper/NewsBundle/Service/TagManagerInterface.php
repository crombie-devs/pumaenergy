<?php

namespace Aper\NewsBundle\Service;

interface TagManagerInterface
{
    public function findOneByNameOrCreate($name);
}