<?php

namespace Aper\NewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Aper\NewsBundle\Service\TagManagerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Aper\NewsBundle\Form\DataTransformer\KeywordsDataTransformer;

class KeywordType extends AbstractType
{

    /**
     * @var TagManagerInterface
     */
    private $tagManager;

    /**
     * Ctor.
     *
     * @param TagManagerInterface $tagManager
     */
    public function __construct(TagManagerInterface $tagManager = null)
    {
        $this->tagManager = $tagManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer(
                new KeywordsDataTransformer(
                $this->tagManager
                ), true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'keyword';
    }

}