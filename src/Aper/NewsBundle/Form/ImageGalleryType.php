<?php

namespace Aper\NewsBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\FileBundle\Form\ImageType as Base;

class ImageGalleryType extends Base
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\NewsBundle\Entity\ImageGallery'
        ));
    }

    public function getName()
    {
        return 'ImageGallery';
    }
}
