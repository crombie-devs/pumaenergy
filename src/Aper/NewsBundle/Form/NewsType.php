<?php

namespace Aper\NewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Aper\UserBundle\Model\Role;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class NewsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $roles = Role::getChoices();
     //   unset($roles['ROLE_RRHH']);
        unset($roles['ROLE_ADMIN']);
        unset($roles['ROLE_STORE_MANAGER']);
        unset($roles['ROLE_TRAINER']);
        unset($roles['ROLE_TRAINER_ADMIN']);
        
        $builder

            ->add('title', 'text', array(

            ))

            ->add('fechaAlta', DateType::class, [
                'widget' => 'single_text',
                'help_block' => 'Fecha en formato dd/mm/yyyy',
                'html5'=>false, 'attr' => ['class' => 'js-datepicker'],
                'format' => 'dd/MM/yyyy'
            ])
            // ->add(
            //         'fechaAlta',
            //         'date',
            //         array(
            //             'widget'   => 'single_text',
            //             'format'   => 'dd/MM/yyyy',
            //             'required' => false,
            //             'attr'     => array( 'class' => 'input campo_fecha' )
            //         )
            // )

            ->add('tags', KeywordType::class, array(
                'required' => false,
                'attr' => array(
                    'class' => 'keywordField'
                )
            ))

            ->add('category', 'entity', array(
                'class' => 'Aper\NewsBundle\Entity\Category',
                'query_builder' => function(\Aper\NewsBundle\Entity\CategoryRepository $r) {
                    return $r->createQueryBuilder('Category')->orderBy('Category.name', 'asc');
                }
            ))

                ->add('highlighted', null, array(
                    'required' => false,
                ))
                ->add('active', null, array(
                    'required' => false,
                ))

                ->add('content', 'ckeditor', array(
                    'config' => array(
						'config_name' => 'my_config',
                        'height' => '450px',
                    ),
                ))


               ->add('urlvideo', 'text',  array('required'   => false

               ))




            ->add('gallery', CollectionType::class, [
                'type' => ImageGalleryType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'widget_add_btn' => [
                    'label' => 'Galería +',
                    'icon' => '',
                    'attr' => [
                        'class' => 'btn btn-primary',
                        'title' => 'GALERÍA +',
                    ],
                ],
                'options' => [
                    'widget_remove_btn' => [
                        'label' => 'ELIMINAR',
                        'attr' => [
                            'class' => 'btn btn-danger',
                            'title' => 'ELIMINAR',
                        ],
                        'icon' => '',
                        'wrapper_div' => [
                            'class' => 'span12',
                        ],
                    ],
                    'attr' => [
                        'class' => 'span12',
                    ],
                    'label' => false,
                    'error_bubbling' => false,
                    'show_child_legend' => false,
                ],
                'show_legend' => false,
                'show_child_legend' => false,
            ])


            ->add('mainImage', new ImageType, array(
            ))


            ->add('rolesAllowed', ChoiceType::class, [
                'choices' => $roles,
                'expanded' => true,
                'multiple' => true
            ])


               ->add('save', SubmitType::class)
        ;

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\NewsBundle\Entity\News'
        ));
    }

    public function getName()
    {
        return 'news';
    }

}
