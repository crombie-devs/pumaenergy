<?php

namespace Aper\NewsBundle\Twig\Extension;

use Twig_Filter_Method;

class YoutubeTwigExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'Youtube Embed Twig Extension';
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('videoEmbed', array($this, 'embedVideo')),
        );
    }

    private function parseUrl($url)
    {
        $parts = parse_url($url);
        if (isset($parts['query'])) {
            parse_str($parts['query'], $qs);
            if (isset($qs['v'])) {
                return $qs['v'];
            } else if ($qs['vi']) {
                return $qs['vi'];
            }
        }
        if (isset($parts['path'])) {
            $path = explode('/', trim($parts['path'], '/'));
            return $path[count($path) - 1];
        }
        return NULL;
    }

    public function youtubeId($input)
    {
        $myId = $this->parseUrl($input);
        return $myId;
    }

    public function embedVideo($input)
    {
        $myId = $this->parseUrl($input);
        if (!isset($myId)) {
            return FALSE;
        }

        $iframe = '<iframe frameborder="0" src="//www.youtube.com/embed/'.$myId.'?rel=0" width="100%" height="100%" allowfullscreen></iframe>';
        return $iframe;
    }

    public function youtubeThumbnail($input, $retina = TRUE)
    {
        $myId = $this->parseUrl($input);
        if (!isset($myId)) {
            return FALSE;
        }
        if ($retina === TRUE) {
            return '//img.youtube.com/vi/' . $myId . '/maxresdefault.jpg';
        }
        return '//img.youtube.com/vi/' . $myId . '/hqdefault.jpg';
    }
}