<?php

namespace Aper\NewsBundle\Controller\Backend;

use Aper\NewsBundle\Form\CategoryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Aper\NewsBundle\Entity\Category;

/**
 * Category controller.
 *
 * @Route("/category")
 */
class CategoryController extends Controller
{

    /**
     * Lists all Category entities.
     *
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $categories, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('AperNewsBundle:Backend/Category:index.html.twig', array('pagination' => $pagination, 'categories' => $categories));

    }

    /**
     * Creates a new Category entity.
     *
     * @Method("POST")
     * @Template("AperNewsBundle:Backend/Category:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash('mensaje', 'La categoría ha sido creada');

            return $this->redirectToRoute('aper_category_index');
        }

        return $this->render('AperNewsBundle:Backend/Category:new.html.twig', array('form' => $form->createView()));
    }

    /**
     * Displays a form to create a new Category entity.
     *
     * @Route("/new", name="backend_category_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {

        $entity = new Category();
        $form = $this->createForm(new CategoryType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     * @Route("/{id}/edit", name="backend_category_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AperNewsBundle:Category')->find($id);
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if(!$category){
            throw $this->createNotFoundException('Categoría no encontrada');
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $em->flush();

            $this->addFlash('mensaje', 'La categoría ha sido modificada');

            return $this->redirectToRoute('aper_category_index');
        }

        return $this->render('AperNewsBundle:Backend/Category:edit.html.twig', array('category' => $category, 'form' => $form->createView()));
    }

    /**
     * Edits an existing Category entity.
     *
     * @Route("/{id}", name="backend_category_update")
     * @Method("PUT")
     * @Template("WebFactoryNewsBundle:Backend/Category:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WebFactoryNewsBundle:Category')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new CategoryComm(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            
            $request->getSession()->getFlashBag()->add('success', 'Record updated');

            return $this->redirect($this->generateUrl('backend_category_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Category entity.
     *
     * @Route("/{id}/delete", name="backend_category_delete")
     * @Method({"DELETE", "GET"})
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AperNewsBundle:Category')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Category entity.');
            }

            try {
                $em->remove($entity);
                $em->flush();
                $request->getSession()->getFlashBag()->add('success', 'Record deleted');
            } catch (\Exception $exc) {
                $request->getSession()->getFlashBag()->add('error', $exc->getMessage());
            }
        }

        return $this->redirect($this->generateUrl('aper_category_index'));
    }

    /**
     * Creates a form to delete a Category entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id), array('csrf_protection' => false))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
