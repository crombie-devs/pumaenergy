<?php

namespace Aper\NewsBundle\Controller\Backend;

use Aper\NewsBundle\Entity\News;
use Aper\NewsBundle\Form\NewsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;



class NewsController extends Controller
{
    /**
     * Lists all News entities.
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $news = $this->getDoctrine()->getRepository(News::class)->createQueryBuilderByCategoryAndTag();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $news, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('AperNewsBundle:Backend/News:index.html.twig', array('pagination' => $pagination, 'news' => $news));

    }
    /**
     * Creates a new News entity.
     *
     * @Method("POST")
     * @Template("AperNewsBundle:Backend/News:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new News();
        $form = $this->createForm(new NewsType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->addRoleAllowed('ROLE_ADMIN');
            $em->persist($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Registro creado');

            return $this->redirect($this->generateUrl('aper_news_index'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new News entity.
     *
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new News();
        $form   = $this->createForm(new NewsType(), $entity, [
            'action' => $this->generateUrl('aper_news_create')
        ]);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a News entity.
     *
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {

        $repository = $this->getDoctrine()->getRepository('AperNewsBundle:News');
        $news = $repository->find($id);
        if(!$news){
            $messageException = 'Comunicación no encontrada';
            throw $this->createNotFoundException($messageException);
        }

        return $this->render('AperNewsBundle:Backend/News:show.html.twig', array('news' => $news));




    }

    /**
     * Displays a form to edit an existing News entity.
     *
     * @Route("/{id}/edit", name="backend_news_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AperNewsBundle:News')->find($id);
        $form = $this->createForm(NewsType::class, $entity);
        $form->handleRequest($request);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }

        if ($form->isSubmitted() && $form->isValid()) {


            $em->flush();

            $this->addFlash('mensaje', 'La comunicación ha sido modificada');

            return $this->redirectToRoute('aper_news_index');
        }

        return $this->render('AperNewsBundle:Backend/News:edit.html.twig',
            array('entity' => $entity, 'form' => $form->createView()));
    }


    /**
     * Deletes a News entity.
     *
     * @Route("/{id}/delete", name="backend_news_delete")
     * @Method({"DELETE", "GET"})
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AperNewsBundle:News')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find News entity.');
            }

            $em->remove($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Registro Eliminado');
        }

        return $this->redirect($this->generateUrl('aper_news_index'));
    }

    /**
     * Creates a form to delete a News entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id), array('csrf_protection' => false))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
