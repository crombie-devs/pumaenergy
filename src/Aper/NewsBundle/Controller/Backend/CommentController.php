<?php

namespace Aper\NewsBundle\Controller\Backend;

use APY\DataGridBundle\Grid\Action\DeleteMassAction;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\NewsBundle\Entity\Comment;

/**
 * Comment controller.
 *
 * @Route("/comment")
 */
class CommentController extends Controller
{

    /**
     * Lists all Comment entities.
     *
     * @Route("/", name="backend_comment")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function indexAction()
    {
        die;
        /* @var $source \APY\DataGridBundle\Grid\Source\Entity */
        $source = new Entity('WebFactoryNewsBundle:Comment');
        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('main');
        $grid->setHiddenColumns(array('id'));
        $grid->setDefaultOrder('id', 'asc');

        $publishRowAction = new RowAction('Publish', 'backend_comment_moderate', false, '_self', array('class' => 'btn-action glyphicons check btn-success'));
        $publishRowAction->addRouteParameters(array('id', 'status' => 'published'));
        $grid->addRowAction($publishRowAction);
        $unpublishRowAction = new RowAction('Unpublish', 'backend_comment_moderate', false, '_self', array('class' => 'btn-action glyphicons unchecked btn-danger'));
        $unpublishRowAction->addRouteParameters(array('id', 'status' => 'unpublished'));
        $grid->addRowAction($unpublishRowAction);
        $deleteRowAction = new RowAction('Delete', 'backend_comment_delete', true, '_self', array('class' => 'btn-action glyphicons remove_2 btn-danger'));
        $grid->addRowAction($deleteRowAction);

        $grid->addMassAction(new DeleteMassAction());

        return $grid->getGridResponse('WebFactoryNewsBundle:Backend/Comment:index.html.twig');
    }

    /**
     * @Route("/{id}/moderate/{status}", name="backend_comment_moderate")
     * @Method({"GET"})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param Comment $comment
     * @param string $status
     */
    public function moderateAction(Request $request, Comment $comment, $status)
    {
        $comment->setStatus($status);
        $this->getDoctrine()->getManager()->flush();

        $request->getSession()->getFlashBag()->add('success', 'Record updated');

        return $this->redirect($this->generateUrl('backend_comment'));
    }

    /**
     * Deletes a Comment entity.
     *
     * @Route("/{id}/delete", name="backend_comment_delete")
     * @Method({"DELETE","GET"})
     */
    public function deleteAction(Request $request, Comment $comment)
    {
        $form = $this->createDeleteForm($comment->getId());
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comment);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Record deleted');
        }

        return $this->redirect($this->generateUrl('backend_comment'));
    }

    /**
     * Creates a form to delete a Comment entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id), array('csrf_protection' => false))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
