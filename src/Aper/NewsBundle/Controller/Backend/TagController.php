<?php

namespace WebFactory\Bundle\NewsBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Site controller.
 *
 * @Route("/tag")
 */
class TagController extends Controller
{

    /**
     * Lists all Site entities.
     *
     * @Route("/ajax", name="ajax_tag")
     * @Method("GET")
     * @Template()
     */
    public function findAction(Request $request)
    {
        $value = $request->get('term');

        $em = $this->getDoctrine()->getEntityManager();
        $tags = $em->getRepository('AperNewsBundle:Tag')->createQueryBuilder('t')
                ->where("t.name like :name")->setParameter('name', "%{$value}%")
                ->getQuery()->getResult();

        $json = array();
        foreach ($tags as $tag) {
            $json[] = array(
                'label' => $tag->getName(),
                'value' => $tag->getName()
            );
        }

        return new JsonResponse($json);
    }
}
