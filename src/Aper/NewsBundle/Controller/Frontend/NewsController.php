<?php

namespace Aper\NewsBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Aper\NewsBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Aper\NewsBundle\Entity\Tag;
use Aper\NewsBundle\Entity\News;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * News controller.
 *
 */
class NewsController extends Controller
{
    /**
     * Class NewsController
     *
     * @Route("news/index")
     */
    public function indexAction(Request $request)
    {

        $catQueryId = $request->get('category');
        $sinceQuery = $request->get('since');
        $untilQuery = $request->get('until');
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $catQuery = $this->getDoctrine()->getRepository(Category::class)->findOneBy(array('id' => $catQueryId));
        $rol = $this->get('security.token_storage')->getToken()->getUser()->getRoles();
        $tag = null;
        $since = null;
        $until = null;
        $categ = null;
        if ($catQuery) {
            $categ = $catQuery->getName();
        }


        if ($sinceQuery) {
            $array = explode('/', $sinceQuery);
            $since = New \DateTime($array[2] . '-' . str_pad($array[1], 2, '0') . '-' .
                str_pad($array[0], 2, '0') . " 00:00:00");
        }


        if ($untilQuery) {
            $array = explode('/', $untilQuery);
            $until = New \DateTime($array[2] . '-' . str_pad($array[1], 2, '0') . '-' .
                str_pad($array[0], 2, '0') . " 23:59:59");
        }


        $news = $this->getDoctrine()->getRepository(News::class)
            ->createQueryBuilderByCategoryAndTagFe($catQuery, $since, $until, $tag, $rol);


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $news, $request->query->getInt('page', 1),
          4
        );


        if ($request->isXmlHttpRequest()) {
            $ajaxNews = [];
            foreach ($pagination as $new) {
                $image = '';
                if ($new->getMainImage()) {
                    $image = '//' . $request->getHost() . $request->getBaseUrl() . '/' . $new->getMainImage()->getWebPath();
                }
                $ajaxNews[] = array(
                    'title' => $new->getTitle(),
                    'category' => $new->getCategory()->getName(),
                    'date' => $new->getCreatedAt(),
                    'content' => substr($new->getContent(), 0, 80),
                    'img' => $image,
                    'link' => '//' . $request->getHost() . $request->getBaseUrl() . '/news/show/' . $new->getId(),
                );
            }
            $response = new  \Symfony\Component\HttpFoundation\Response(json_encode($ajaxNews));
            return $response;
        }

        return $this->render('AperNewsBundle:Frontend/News:index.html.twig', array('pagination' => $pagination, 'categories' => $categories));

    }

    /**
     * Finds and displays a News entity.
     *
     * @Route("/news/show/{id}", name="frontend_news_show")
     * @Method("GET")
     */

    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AperNewsBundle:News');
        $rol = $this->get('security.token_storage')->getToken()->getUser()->getRoles();
        $news = $repository->findxid(null, null, $rol, 1, $id);
        if (!$news) {
            $messageException = 'Comunicación no encontrada';
            throw $this->createNotFoundException($messageException);
        }


        $relatedNews = $repository->findRelatedNews(null, null, $rol, 4, $id);


        return $this->render('AperNewsBundle:Frontend/News:show.html.twig', array('news' => $news, 'relatedNews' => $relatedNews));


    }
}