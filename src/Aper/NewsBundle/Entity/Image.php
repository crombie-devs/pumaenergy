<?php

namespace Aper\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use WebFactory\Bundle\FileBundle\Model\Image as BaseEntity;

/**
 * Format
 *
 * @ORM\Entity
 * @ORM\Table(name="news_images")
 */
class Image extends BaseEntity
{
    
    const DIRECTORY = 'uploads/news/images';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *
     * @var News
     * @ORM\OneToOne(targetEntity="News", inversedBy="mainImage", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="new_id", referencedColumnName="id")
     */
    protected $news;

    /**
     * Set book
     *
     * @param News $news
     * @return Image
     */
    public function setNews(News $news = null)
    {
        $this->news = $news;

        return $this;
    }

    /**
     * Get news
     *
     * @return News
     */
    public function getNews()
    {
        return $this->news;
    }

}