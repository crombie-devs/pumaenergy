<?php

namespace Aper\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;





/**
 * Category
 *
 * @ORM\Table(name="news_categories")
 * @ORM\Entity(repositoryClass="Aper\NewsBundle\Entity\CategoryRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity("name")
 */
class Category
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var type 
     * @ORM\OneToMany(targetEntity="Aper\NewsBundle\Entity\News", mappedBy="category", cascade={"persist"})
     * @ORM\JoinColumn(name="news_id", referencedColumnName="id")
     */
    protected $newsCollection;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;
    
    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=255, unique=true)
     */
    protected $slug;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;
    
    /**
     *
     * @param string $name
     */
    public function __construct($name = null)
    {
        $this->name = $name;
        $this->newsCollection = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function __toString()
    {
        return $this->name;
    }
    
    public function getNewsCollection()
    {
        return $this->newsCollection;
    }

    public function setNewsCollection($newsCollection)
    {
        $this->newsCollection = $newsCollection;
    }

    /**
     * @ORM\PreRemove()
     */
    public function preRemove()
    {
        if (!$this->newsCollection->isEmpty()) {
            throw new \LogicException('The category has news and can not be deleted. Please, first remove the news.');
        }
    }

    /**
     * 
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    /**
     * 
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * 
     * @param DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * 
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * 
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}
