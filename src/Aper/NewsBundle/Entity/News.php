<?php

namespace Aper\NewsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Aper\UserBundle\Entity\User;
use Aper\NewsBundle\Entity\ImageGallery;

/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="Aper\NewsBundle\Entity\NewsRepository")
 */
class News
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var \Aper\UserBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\User", cascade={"persist"}, fetch="LAZY")
     * @Gedmo\Blameable(on="create")
     * @Gedmo\Blameable(on="update")
     */
    protected $owner;

    /**
     *
     * @var Category
     * @ORM\ManyToOne(targetEntity="Aper\NewsBundle\Entity\Category", inversedBy="newsCollection", cascade={"persist"}, fetch="LAZY")
     * @Assert\NotBlank()
     */
    protected $category;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Tag", cascade={"persist"})
     * @ORM\JoinTable(name="news_tags",
     *      joinColumns={@ORM\JoinColumn(name="news_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     */
    protected $tags;

    /**
     *
     * @var Comment
     * @ORM\OneToMany(targetEntity="Aper\NewsBundle\Entity\Comment", mappedBy="news", cascade={"all"}, orphanRemoval=true)
     */
    protected $comments;

    /**
     *
     * @var Image
     * @ORM\OneToOne(targetEntity="Image", mappedBy="news", cascade={"all"})
     * @Assert\NotBlank()
     * @Assert\Valid()
     */
    protected $mainImage;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=255, nullable=true)
     */
    protected $slug;

    /**
     * @var string
     * @ORM\Column(name="abstract", type="string", length=255, nullable=true)
     */
    private $abstract;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(type="boolean")
     */
    protected $highlighted;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", type="boolean", nullable=true)
     */
    protected $active;

    /**
     * @var string[]
     *
     * @ORM\Column(name="roles_allowed", type="simple_array", nullable=true )
     * @Assert\Count(min="1", minMessage="Debe elegir un rol como mínimo")
     */
    private $rolesAllowed;

    /**
     * @var string
     * @ORM\Column(name="urlvideo", type="string", length=255, nullable=true)
     */
    private $urlvideo;

    /**
     * @var ImageGallery[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Aper\NewsBundle\Entity\ImageGallery", mappedBy="news", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"name" = "DESC"})
     */
    private $gallery;

    /**
     * @var DateTime
     * @ORM\Column(name="fecha_alta", type="date", nullable=true)
    */
    private $fechaAlta;

    /**
     * Constructor
     */
    public function __construct($title = null, $abstract = null, $content = null, Category $category = null)
    {
        $this->title = $title;
        $this->abstract = $abstract;
        $this->content = $content;
        $this->category = $category;
        $this->highlighted = false;
        $this->active = false;
        $this->tags = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->gallery = new ArrayCollection();
        $this->rolesAllowed = [];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set abstract
     *
     * @param string $abstract
     * @return News
     */
    public function setAbstract($abstract)
    {
        $this->abstract = $abstract;

        return $this;
    }

    /**
     * Get abstract
     *
     * @return string
     */
    public function getAbstract()
    {
        return $this->abstract;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return News
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return News
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return News
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

//    public function getComments()
//    {
//        return $this->comments;
//    }
//
//    public function setComments($comments)
//    {
//        $this->comments = $comments;
//    }



    public function getMainImage()
    {
        return $this->mainImage;
    }

    public function setMainImage(Image $mainImage)
    {
        $this->mainImage = $mainImage;

        $mainImage->setNews($this);
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }

    /**
     * Add tags
     *
     * @param \Aper\NewsBundle\Entity\Tag $tags
     * @return News
     */
    public function addTag(\Aper\NewsBundle\Entity\Tag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \Aper\NewsBundle\Entity\Tag $tags
     */
    public function removeTag(\Aper\NewsBundle\Entity\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add comments
     *
     * @param \Aper\NewsBundle\Entity\Comment $comments
     * @return News
     */
    public function addComment(\Aper\NewsBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \Aper\NewsBundle\Entity\Comment $comments
     */
    public function removeComment(\Aper\NewsBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set highlighted
     *
     * @param string $highlighted
     * @return News
     */
    public function setHighlighted($highlighted)
    {
        $this->highlighted = $highlighted;

        return $this;
    }

    /**
     * Get highlighted
     *
     * @return string
     */
    public function getHighlighted()
    {
        return $this->highlighted;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }
    /**
     * Set rolesAllowed.
     *
     * @param string $rolesAllowed
     *
     * @return News
     */
    public function setRolesAllowed($rolesAllowed)
    {
        $this->rolesAllowed = $rolesAllowed;

        return $this;
    }

    /**
     * Get rolesAllowed.
     *
     * @return string
     */
    public function getRolesAllowed()
    {

        return $this->rolesAllowed;

    }

    /**
     * @param $roleAllowed
     *
     * @return $this
     */
    public function addRoleAllowed($roleAllowed)
    {
        $roleAllowed = strtoupper($roleAllowed);

        if (!in_array($roleAllowed, $this->rolesAllowed)) {

            $this->rolesAllowed[] = $roleAllowed;
        }

        return $this;

    }

    /**
     * @param $roleAllowed
     *
     * @return $this
     */
    public function removeRoleAllowed($roleAllowed)
    {
        if (false !== $key = array_search(strtoupper($roleAllowed), $this->rolesAllowed, true)) {
            unset($this->rolesAllowed[$key]);
            $this->rolesAllowed = array_values($this->rolesAllowed);
        }

        return $this;
    }

    public function hasRoleAllowed($roleAllowed)
    {
        return false !== $key = array_search(strtoupper($roleAllowed), $this->rolesAllowed, true);
    }
    /**
     * Set urlvideo
     *
     * @param string $urlvideo
     * @return News
     */
    public function setUrlvideo($urlvideo)
    {
        $this->urlvideo = $urlvideo;

        return $this;
    }

    /**
     * Get urlvideo
     *
     * @return string
     */
    public function getUrlvideo()
    {
        return $this->urlvideo;
    }


   //////////////////////////// //GALERIA//*/////////////////

    /**
     * Add gallery
     *
     * @param \Aper\NewsBundle\Entity\ImageGallery $gallery1
     * @return News
     */
    public function addGallery(\Aper\NewsBundle\Entity\ImageGallery $gallery1)
    {
        $gallery1->setNews($this);
        $fileName = $gallery1->getFile()->getFilename();
        $gallery1->setName(  $fileName );
        $this->gallery[] = $gallery1;
        return $this;
    }


    public function removeGallery(ImageGallery $galeries)
    {
        $this->gallery->removeElement($galeries);
    }

    /**
     * Get gallery []
     *
     */
    public function getGallery()
    {

//           $galeria[] = $this->gallery;
//
//           usort($galeria, function ($a, $b) {
//           return substr($a->getWebPath(), 7,strlen($a->getWebPath()) ) < substr($b->getWebPath(), 7,strlen($b->getWebPath()));
//       }
//
//       );

        $galeria = $this->gallery;

        return $galeria;

    }

    public function getFechaAlta(){
        return $this->fechaAlta;
    }
    public function setFechaAlta($fechaAlta){
        $this->fechaAlta = $fechaAlta;
    }

}