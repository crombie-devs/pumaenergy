<?php

namespace Aper\NewsBundle\Entity;

use Aper\NewsBundle\Entity\Tag;
use Aper\NewsBundle\Entity\Category;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\TextType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use DoctrineExtensions\Query\Mysql\Date;

/**
 * NewsRepository
 *
 */
class NewsRepository extends EntityRepository
{

    /**
     * 
     * @param \Aper\NewsBundle\Entity\Category $category
     * @param \Aper\NewsBundle\Entity\Tag $tag
     * @return Type
     */
    public function createQueryBuilderByCategoryAndTag
    (Category $category = null,  $since = null,  $until = null, Tag $tag=null,  $rol=null)
    {
        $queryBuilder = $this->createQueryBuilder('News')
                ->addSelect('Image')
                ->addSelect('Tag')
                ->leftJoin('News.mainImage', 'Image')
                ->leftJoin('News.tags', 'Tag')
               // ->where('News.active = true')
                ->orderBy('News.fechaAlta', 'DESC');

        if ($since) {
            $queryBuilder->andWhere('News.fechaAlta >= :since')
                        ->setParameter('since', $since);
        }

       if ($until) {
            $queryBuilder->andWhere('News.fechaAlta <= :until')
                         ->setParameter('until', $until);
        }

        if ($category) {
            $queryBuilder->andWhere('News.category = :category')
                ->setParameter('category', $category->getId());
        }

if ($rol) {
    $roleDQL = [];
    foreach ($rol as $i => $r) {
        $roleDQL[] = 'News.rolesAllowed LIKE :rol' . $i;
        $queryBuilder
            ->setParameter('rol' . $i, '%' . $r . '%');

    }
    $queryBuilder->andwhere(implode(' OR ', $roleDQL));
}

        if ($tag) {
            $queryBuilder->innerJoin('News.tags', 'TagFilter')
                    ->andWhere('TagFilter = :tag')
                    ->setParameter('tag', $tag->getId());
        }

        return $queryBuilder;
    }

    /**
     *
     * @param \Aper\NewsBundle\Entity\Category $category
     * @param \Aper\NewsBundle\Entity\Tag $tag
     * @return array
     */
    public function findRelatedNews
    (Category $category = null,  Tag $tag=null,  $rol=null, $limit=4 , $id=null)
    {
        $queryBuilder = $this->createQueryBuilder('News')
            ->addSelect('Image')
            ->addSelect('Tag')
            ->leftJoin('News.mainImage', 'Image')
            ->leftJoin('News.tags', 'Tag')
            ->where('News.active = true')
            ->orderBy('News.fechaAlta', 'DESC');


        if ($category) {
            $queryBuilder->andWhere('News.category = :category')
                ->setParameter('category', $category->getId());
        }

        if ($id) {
            $queryBuilder->andWhere('News.id <> :id')
                ->setParameter('id', $id);
        }

        if ($rol) {
            $roleDQL = [];
            foreach ($rol as $i => $r) {
                $roleDQL[] = 'News.rolesAllowed LIKE :rol' . $i;
                $queryBuilder
                    ->setParameter('rol' . $i, '%' . $r . '%');

            }
            $queryBuilder->andwhere(implode(' OR ', $roleDQL));
        }

        if ($tag) {
            $queryBuilder->innerJoin('News.tags', 'TagFilter')
                ->andWhere('TagFilter = :tag')
                ->setParameter('tag', $tag->getId());
        }

        return $queryBuilder->setMaxResults($limit)->getQuery()->getResult();
    }

    /**
     *
     * @param \Aper\NewsBundle\Entity\Category $category
     * @param \Aper\NewsBundle\Entity\Tag $tag
     * @return News
     */
    public function findxid
    (Category $category = null,  Tag $tag=null,  $rol=null, $limit=1 , $id=null)
    {
        $queryBuilder = $this->createQueryBuilder('News')
            ->addSelect('Image')
            ->addSelect('Tag')
            ->leftJoin('News.mainImage', 'Image')
            ->leftJoin('News.tags', 'Tag')
          //  ->leftJoin('News.gallery', 'gallery')
            ->where('News.active = true')
            ->orderBy('News.fechaAlta', 'DESC');
          //  ->orderBy('News.gallery.name', 'DESC');

        if ($category) {
            $queryBuilder->andWhere('News.category = :category')
                ->setParameter('category', $category->getId());
        }

        if ($id) {
            $queryBuilder->andWhere('News.id = :id')
                ->setParameter('id', $id);
        }

        if ($rol) {
            $roleDQL = [];
            foreach ($rol as $i => $r) {
                $roleDQL[] = 'News.rolesAllowed LIKE :rol' . $i;
                $queryBuilder
                    ->setParameter('rol' . $i, '%' . $r . '%');

            }
            $queryBuilder->andwhere(implode(' OR ', $roleDQL));
        }

        if ($tag) {
            $queryBuilder->innerJoin('News.tags', 'TagFilter')
                ->andWhere('TagFilter = :tag')
                ->setParameter('tag', $tag->getId());
        }

        return $queryBuilder->setMaxResults($limit)->getQuery()->getOneOrNullResult();
    }


    /**
     *
     * @param \Aper\NewsBundle\Entity\Category $category
     * @param \Aper\NewsBundle\Entity\Tag $tag
     * @return Type
     */
    public function createQueryBuilderByCategoryAndTagFe
    (Category $category = null,  $since = null,  $until = null, Tag $tag=null,  $rol=null)
    {
        $queryBuilder = $this->createQueryBuilder('News')
            ->addSelect('Image')
            ->addSelect('Tag')
            ->leftJoin('News.mainImage', 'Image')
            ->leftJoin('News.tags', 'Tag')
            ->where('News.active = true')
            ->orderBy('News.fechaAlta', 'DESC');

        if ($since) {
            $queryBuilder->andWhere('News.fechaAlta >= :since')
                ->setParameter('since', $since);
        }

        if ($until) {
            $queryBuilder->andWhere('News.fechaAlta <= :until')
                ->setParameter('until', $until);
        }

        if ($category) {
            $queryBuilder->andWhere('News.category = :category')
                ->setParameter('category', $category->getId());
        }

        if ($rol) {
            $roleDQL = [];
            foreach ($rol as $i => $r) {
                $roleDQL[] = 'News.rolesAllowed LIKE :rol' . $i;
                $queryBuilder
                    ->setParameter('rol' . $i, '%' . $r . '%');

            }
            $queryBuilder->andwhere(implode(' OR ', $roleDQL));
        }

        if ($tag) {
            $queryBuilder->innerJoin('News.tags', 'TagFilter')
                ->andWhere('TagFilter = :tag')
                ->setParameter('tag', $tag->getId());
        }

        return $queryBuilder;
    }



    /**
     * 
     * @param type $limit
     * @return type
     */
    public function findLastest($limit = 2)
    {
        $queryBuilder = $this->createQueryBuilder('News')
                ->addSelect('Image')
                ->addSelect('Comment')
                ->addSelect('User')
                ->addSelect('Profile')
                ->addSelect('Tag')
                ->leftJoin('News.mainImage', 'Image')
                ->leftJoin('News.tags', 'Tag')
                ->leftJoin('News.comments', 'Comment', Join::WITH, "Comment.status = 'published'")
                ->leftJoin('Comment.user', 'User')
                ->leftJoin('User.profile', 'Profile')
                ->orderBy('News.fechaAlta', 'DESC')
                ->where('News.highlighted = false');

        return $queryBuilder->setMaxResults($limit)->getQuery()->getResult();
    }

    /**
     * 
     * @param type $limit
     * @return type
     */
    public function findHighlighted($limit = 2)
    {
        $queryBuilder = $this->createQueryBuilder('News')
                ->addSelect('Image')
                ->addSelect('Comment')
                ->addSelect('User')
                ->addSelect('Profile')
                ->addSelect('Tag')
                ->leftJoin('News.mainImage', 'Image')
                ->leftJoin('News.tags', 'Tag')
                ->leftJoin('News.comments', 'Comment', Join::WITH, "Comment.status = 'published'")
                ->leftJoin('Comment.user', 'User')
                ->leftJoin('User.profile', 'Profile')
                ->orderBy('News.fechaAlta', 'DESC')
                ->where('News.highlighted = true');

        return $queryBuilder->setMaxResults($limit)->getQuery()->getResult();
    }
    
    
    /**
     * 
     * @param type $limit
     * @return type
     */ 
    
     public function findByLast2Highlighted($limit = 2, $rol)
    {
        $queryBuilder = $this->createQueryBuilder('News')

                ->leftJoin('News.mainImage', 'Image')
                ->leftJoin('News.tags', 'Tag')
                ->orderBy('News.fechaAlta', 'DESC')
                ->where('News.highlighted = true');
       
        if ($rol) {
            $roleDQL = [];
            foreach ($rol as $i => $r) {
                $roleDQL[] = 'News.rolesAllowed LIKE :rol' . $i;
                $queryBuilder
                    ->setParameter('rol' . $i, '%' . $r . '%');

            }
            $queryBuilder->andwhere(implode(' OR ', $roleDQL));
        }
            

        return $queryBuilder->setMaxResults($limit)->getQuery()->getResult();
    }

}
