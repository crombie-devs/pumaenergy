<?php

namespace Aper\NewsBundle\Entity;

use Aper\NewsBundle\Form\ImageGalleryType;
use Doctrine\ORM\Mapping as ORM;
use WebFactory\Bundle\FileBundle\Model\Image as BaseEntity;

/**
 * Format
 *
 * @ORM\Entity
 * @ORM\Table(name="news_gallery_images")
 */
class ImageGallery extends BaseEntity
{
    
    const DIRECTORY = 'uploads/news/images';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *
     * @var News
     * @ORM\ManyToOne(targetEntity="News", inversedBy="gallery")
     * @ORM\JoinColumn(name="new_id", referencedColumnName="id")
     */
    protected $news;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;


    /**
     * Set name
     *
     * @param string $name
     * @return ImageGallery
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set News
     *
     * @param News $news
     * @return ImageGallery
     */
    public function setNews(News $news = null)
    {
        $this->news = $news;

        return $this;
    }
    /**
     * Get news
     *
     * @return News
     */
    public function getNews()
    {
        return $this->news;
    }



}