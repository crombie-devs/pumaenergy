<?php

namespace Aper\CourseBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\FileBundle\Form\ImageType as Base;

class VideoType extends Base
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\CourseBundle\Entity\Video'
        ));
    }

    public function getName()
    {
        return 'video';
    }
}
