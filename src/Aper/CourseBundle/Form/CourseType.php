<?php

namespace Aper\CourseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Aper\UserBundle\Model\Role;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CourseType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles = Role::getChoices();
        unset($roles['ROLE_ADMIN']);
        $builder
            ->add('title', 'text', array())

            ->add('category', 'entity', array(
                'class' => 'Aper\CourseBundle\Entity\Category',
                'query_builder' => function(\Aper\CourseBundle\Entity\CategoryRepository $r) {
                    return $r->createQueryBuilder('Category')->orderBy('Category.name', 'asc');
                }
            ))

            ->add('trainingFolder', 'entity', array(
                'class' => 'Aper\TrainingKitBundle\Entity\TrainingFolder',
                'query_builder' => function(\Aper\TrainingKitBundle\Repository\TrainingFolderRepository $r) {
                    return $r->createQueryBuilder('TrainingFolder');
                }
            ))
            ->add('active', null, array(
                'required' => false,
            ))

            ->add('content', 'ckeditor', array(
                'config' => array(
					'config_name' => 'my_config',
                    'height' => '450px',
                ),
            ))

            ->add('mainImage', new ImageType(), array(
            ))

            ->add('Pdf', new PdfType(), array(
                'label' => 'Adjunto',
            ))

            ->add('rolesAllowed', ChoiceType::class, [
                'choices' => $roles,
                'expanded' => true,
                'multiple' => true
            ])

            ->add('save', SubmitType::class);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\CourseBundle\Entity\Course'
        ));
    }

    public function getName()
    {
        return 'course';
    }
}