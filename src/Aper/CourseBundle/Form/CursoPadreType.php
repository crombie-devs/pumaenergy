<?php

namespace Aper\CourseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Aper\UserBundle\Model\Role;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CursoPadreType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles = Role::getChoices();
        unset($roles['ROLE_ADMIN']);
        $builder
            ->add('titulo')
            ->add('descripcion')
            ->add('rolesAllowed', ChoiceType::class, [
                'choices' => $roles,
                'expanded' => true,
                'multiple' => true
            ])
            ->add('activo')
            ->add('imagen', new CursoPadreImageType(), array(
            ))
            ->add('save', SubmitType::class);;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\CourseBundle\Entity\CursoPadre'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_coursebundle_cursopadre';
    }


}
