<?php

namespace Aper\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * ContenidoDeClase
 *
 * @ORM\Entity
 * @ORM\Table(name="contenido_de_clase")
 */
class ContenidoDeClase
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $archivo;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     */
    private $titulo;
//
//    /**
//     * @var UploadedFile
//     * @Assert\File(
//     *     maxSize = "10M"
//     * )
//     *
//     */
//    protected $file;

    /**
     * @var bool
     *
     * @ORM\Column(name="es_video", type="boolean", nullable=true)
     */
    private $esVideo;

    /**
     * Many contenidoDeClases have one course. This is the owning side.
     * @ManyToOne(targetEntity="Course", inversedBy="contenidoDeClases")
     * @JoinColumn(name="course_id", referencedColumnName="id")
     */
    private $course;

    /**
     * Many contenidoDeClases have one course. This is the owning side.
     * @ManyToOne(targetEntity="CursoPadre", inversedBy="contenidoDeClases")
     * @JoinColumn(name="curso_padre_id", referencedColumnName="id")
     */
    private $cursoPadre;

    /**
     *
     * @var Video
     * @ORM\OneToOne(targetEntity="Video", mappedBy="contenidoDeClase", cascade={"all"})
     */
    protected $video;

    /**
     *
     * @var Pdf
     * @ORM\OneToOne(targetEntity="Pdf", mappedBy="contenidoDeClase", cascade={"all"})
     */
    protected $pdf;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getArchivo()
    {
        return $this->archivo;
    }

    /**
     * @param string $archivo
     */
    public function setArchivo($archivo)
    {
        $this->archivo = $archivo;
    }


    /**
     * @return bool
     */
    public function isEsVideo()
    {
        return $this->esVideo;
    }

    /**
     * @param bool $esVideo
     */
    public function setEsVideo($esVideo)
    {
        $this->esVideo = $esVideo;
    }

    /**
     * @return mixed
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param mixed $course
     */
    public function setCourse($course)
    {
        $this->course = $course;
    }

    /**
     * @return Video
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * @param Video $video
     */
    public function setVideo($video)
    {
        $this->video = $video;
    }

    /**
     * @return Pdf
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @param Pdf $pdf
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return mixed
     */
    public function getCursoPadre()
    {
        return $this->cursoPadre;
    }

    /**
     * @param mixed $cursoPadre
     */
    public function setCursoPadre($cursoPadre)
    {
        $this->cursoPadre = $cursoPadre;
    }



}

