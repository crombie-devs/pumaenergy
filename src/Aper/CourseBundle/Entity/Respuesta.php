<?php

namespace Aper\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Aper\CourseBundle\Entity\Pregunta;

/**
 * Respuesta
 *
 * @ORM\Entity
 * @ORM\Table(name="respuesta")
 */
class Respuesta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="texto_respuesta", type="string", length=100)
     */
    private $textoRespuesta;

    /**
     * @var bool
     *
     * @ORM\Column(name="correcta", type="boolean")
     */
    private $correcta;

    /**
     * @var Pregunta
     * @ORM\ManyToOne(targetEntity="Pregunta", inversedBy="respuestas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pregunta_id", referencedColumnName="id")
     * })
     */
    private $pregunta;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pregunta
     *
     * @param Pregunta $pregunta
     *
     * @return Respuesta
     */
    public function  setPregunta(Pregunta $pregunta){
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta
     *
     * @return Pregunta
     */
    public function  getPregunta(){
        return $this->pregunta;
    }

    /**
     * Set textoRespuesta
     *
     * @param string $textoRespuesta
     *
     * @return Respuesta
     */
    public function setTextoRespuesta($textoRespuesta)
    {
        $this->textoRespuesta = $textoRespuesta;

        return $this;
    }

    /**
     * Get textoRespuesta
     *
     * @return string
     */
    public function getTextoRespuesta()
    {
        return $this->textoRespuesta;
    }

    /**
     * Set correcta
     *
     * @param string $correcta
     *
     * @return Respuesta
     */
    public function setCorrecta($correcta)
    {
        $this->correcta = $correcta;

        return $this;
    }

    /**
     * Get correcta
     *
     * @return string
     */
    public function getCorrecta()
    {
        return $this->correcta;
    }
}

