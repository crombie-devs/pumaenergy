<?php

namespace Aper\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use WebFactory\Bundle\FileBundle\Model\Image as BaseEntity;

/**
 * Format
 *
 * @ORM\Entity
 * @ORM\Table(name="courso_padre_images")
 */
class CursoPadreImage extends BaseEntity
{
    
    const DIRECTORY = 'uploads/course/images';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *
     * @var CursoPadre
     * @ORM\OneToOne(targetEntity="CursoPadre", inversedBy="imagen", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="courso_padre_id", referencedColumnName="id")
     */
    protected $cursoPadre;

    public function setCursoPadre($cursoPadre){
        $this->cursoPadre = $cursoPadre;
        return $this;
    }
    public function getCursoPadre(){
        return $this->cursoPadre;
    }

}