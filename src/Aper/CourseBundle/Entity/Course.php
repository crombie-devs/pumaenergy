<?php

namespace Aper\CourseBundle\Entity;

use Aper\TrainingKitBundle\Entity\TrainingFolder;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Aper\UserBundle\Entity\User;
use Aper\CourseBundle\Entity\Examen;
use Aper\CourseBundle\Entity\CursoPadre;


/**
 * Course
 *
 * @ORM\Table(name="course")
 * @ORM\Entity(repositoryClass="Aper\CourseBundle\Entity\CourseRepository")
 */
class Course
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\User", cascade={"persist"}, fetch="LAZY")
     * @Gedmo\Blameable(on="create")
     * @Gedmo\Blameable(on="update")
     */
    protected $owner;

    /**
     *
     * @var Category
     * @ORM\ManyToOne(targetEntity="Aper\CourseBundle\Entity\Category", inversedBy="courseCollection", cascade={"persist"}, fetch="LAZY")
     * @Assert\NotBlank()
     */
    protected $category;

    /**
     *
     * @var TrainingFolder
     * @ORM\ManyToOne(targetEntity="Aper\TrainingKitBundle\Entity\TrainingFolder", inversedBy="courseCollection", cascade={"persist"}, fetch="LAZY")
     * @Assert\NotBlank()
     */
    protected $trainingFolder;

    /**
     *
     * @var Image
     * @ORM\OneToOne(targetEntity="Image", mappedBy="course", cascade={"all"})
     * @Assert\NotBlank()
     * @Assert\Valid()
     */
    protected $mainImage;

    /**
     *
     * @var Pdf
     * @ORM\OneToOne(targetEntity="Pdf", mappedBy="course", cascade={"all"})
     */
    protected $pdf;

    /**
     * @ORM\OneToOne(targetEntity="Examen", mappedBy="cursoEncuesta", cascade={"all"})
     */
    private $encuesta;

    /**
     * @ORM\OneToOne(targetEntity="Examen", mappedBy="cursoPrevia", cascade={"all"})
     */
    private $examenPrevio;

    /**
     * @ORM\OneToOne(targetEntity="Examen", mappedBy="cursoFinal", cascade={"all"})
     */
    private $examenFinal;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", type="boolean", nullable=true)
     */
    protected $active;

    /**
     * @var string[]
     *
     * @ORM\Column(name="roles_allowed", type="simple_array")
     * @Assert\Count(min="1", minMessage="Debe elegir un rol como mínimo")
     */
    private $rolesAllowed;

    /**
     * @var CursoPadre
     * @ORM\ManyToOne(targetEntity="CursoPadre", inversedBy="cursos")
     * @ORM\JoinColumn(name="curso_padre_id", referencedColumnName="id")
     */
    private $cursoPadre;

    /**
    * One course has many contenidoDeClases. This is the inverse side.
    * @OneToMany(targetEntity="ContenidoDeClase", mappedBy="course")
    */
    private $contenidoDeClases;


    /**
     * Constructor
     * @param null $title
     * @param null $content
     * @param Category|null $category
     * @param TrainingFolder|null $trainingFolder
     */
    public function __construct($title = null,  $content = null, Category $category = null, TrainingFolder $trainingFolder=null)
    {
        $this->title = $title;
        $this->content = $content;
        $this->category = $category;
        $this->trainingFolder = $trainingFolder;
        $this->active = false;

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setcursoPadre($cursoPadre){
        $this->cursoPadre = $cursoPadre;
        return $this;
    }

    public function getcursoPadre(){
        return $this->cursoPadre;
    }

    public function setEncuesta(Examen $encuesta){
        $this->encuesta = $encuesta;
        return $this;
    }

    public function setExamenPrevio(Examen $examenPrevio){
        $this->examenPrevio = $examenPrevio;
        return $this;
    }

    public function setExamenFinal(Examen $examenFinal){
        $this->examenFinal = $examenFinal;
        return $this;
    }

    public function getEncuesta(){
        return $this->encuesta;
    }

    public function getExamenPrevio(){
        return $this->examenPrevio;
    }

    public function getExamenFinal(){
        return $this->examenFinal;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Course
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set content
     *
     * @param string $content
     * @return Course
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Course
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Course
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getTrainingFolder()
    {
        return $this->trainingFolder;
    }

    public function setTrainingFolder($trainingFolder)
    {
        $this->trainingFolder = $trainingFolder;
    }

    public function getMainImage()
    {
        return $this->mainImage;
    }

    public function setMainImage(Image $mainImage)
    {
        $this->mainImage = $mainImage;

        $mainImage->setCourse($this);
    }

    public function getPdf()
    {
        return $this->pdf;
    }

    public function setPdf(Pdf $pdf)
    {
        $this->pdf = $pdf;

        $pdf->setCourse($this);
    }


    public function getOwner()
    {
        return $this->owner;
    }

    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Set rolesAllowed.
     *
     * @param string $rolesAllowed
     *
     * @return Course
     */
    public function setRolesAllowed($rolesAllowed)
    {
        $this->rolesAllowed = $rolesAllowed;

        return $this;
    }

    /**
     * Get rolesAllowed.
     *
     * @return string
     */
    public function getRolesAllowed()
    {
        return $this->rolesAllowed;
    }

    /**
     * @param $roleAllowed
     *
     * @return $this
     */
    public function addRoleAllowed($roleAllowed)
    {
        $roleAllowed = strtoupper($roleAllowed);

        if (!in_array($roleAllowed, $this->rolesAllowed)) {

            $this->rolesAllowed[] = $roleAllowed;
        }

        return $this;
    }

    /**
     * @param $roleAllowed
     *
     * @return $this
     */
    public function removeRoleAllowed($roleAllowed)
    {
        if (false !== $key = array_search(strtoupper($roleAllowed), $this->rolesAllowed, true)) {
            unset($this->rolesAllowed[$key]);
            $this->rolesAllowed = array_values($this->rolesAllowed);
        }

        return $this;
    }

    public function hasRoleAllowed($roleAllowed)
    {
        return false !== $key = array_search(strtoupper($roleAllowed), $this->rolesAllowed, true);
    }


    /**
     * @return mixed
     */
    public function getContenidoDeClases()
    {
        return $this->contenidoDeClases;
    }

    /**
     * @param mixed $contenidoDeClases
     */
    public function setContenidoDeClases($contenidoDeClases)
    {
        $this->contenidoDeClases = $contenidoDeClases;
    }


    /**
     * Add ContenidoDeClase
     * @param ContenidoDeClase $contenidoDeClase
     */
    public function addContenidoDeClase( ContenidoDeClase $contenidoDeClase ) {
        $this->contenidoDeClases[] = $contenidoDeClase;
    }
}
