<?php

namespace Aper\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Aper\UserBundle\Entity\Employee;
use Aper\CourseBundle\Entity\Pregunta;

/**
 * EmployeePregunta
 *
 * @ORM\Entity(repositoryClass="Aper\CourseBundle\Repository\EmployeePreguntaRepository")
 * @ORM\Table(name="employee_pregunta")
 */
class EmployeePregunta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="actual", type="boolean", nullable=true)
     */
    private $actual;

    /**
     * @var Pregunta
     * @ORM\ManyToOne(targetEntity="Pregunta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pregunta_id", referencedColumnName="id")
     * })
     */
    private $pregunta;

    /**
     * @var Employee
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\Employee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     * })
     */
    private $employee;

    /**
     * @var EmployeeExamen
     * @ORM\ManyToOne(targetEntity="EmployeeExamen", inversedBy="preguntas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="employee_examen_id", referencedColumnName="id")
     * })
     */
    private $examen;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getExamen(){
        return $this->examen;
    }

    public function setExamen($examen){
        $this->examen = $examen;
        return $this;
    }

    public function setActual($actual){
        $this->actual = $actual;
        return $this;
    }
    public function getActual(){
        return $this->actual;
    }

    /**
     * Set pregunta
     *
     * @param Pregunta $pregunta
     *
     * @return Pregunta
     */
    public function setPregunta(Pregunta $pregunta){
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta
     *
     * @return Pregunta
     */
    public function getPregunta(){
        return $this->pregunta;
    }

    public function setEmployee(Employee $employee){
        $this->employee = $employee;
        return $this;
    }

    public function getEmployee(){
        return $this->employee;
    }
}

