<?php

namespace Aper\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Aper\CourseBundle\Entity\Examen;
use Aper\CourseBundle\Entity\Respuesta;

/**
 * Pregunta
 *
 * @ORM\Entity(repositoryClass="Aper\CourseBundle\Repository\PreguntaRepository")
 * @ORM\Table(name="pregunta")
 */
class Pregunta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="string", length=255)
     */
    private $texto;

    /**
     * @var Examen
     * @ORM\ManyToOne(targetEntity="Examen", inversedBy="preguntas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="examen_id", referencedColumnName="id")
     * })
     */
    private $examen;

    /**
     * @ORM\OneToMany(targetEntity="Respuesta", mappedBy="pregunta")
     */
    private $respuestas;


    /**
     * @ORM\Column(name="tiene_respuesta_correcta", type="integer")
     */
    private $tieneRespuestasCorrecta = 0;

    public function __construct() {
        $this->respuestas = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }    /*
     * @return \Doctrine\ORM\QueryBuilder
    public function getQueryBuilder()
    {
        return $this->createQueryBuilder('Localidad')->orderBy('Localidad.nombre', 'ASC');
    }
    */
    /*
     * @param $params
     *
     * @return \Aper\StoreBundle\Entity\Localidad
    public function findForUniqueValidation(array $params)
    {
        return $this->findBy($params);
    }

    public function findByProvincia($provincia)
    {
        $qb   = $this->createQueryBuilder('Localidad');
        $qb
            ->where('Localidad.provincia = :provincia')->setParameter('provincia', $provincia);

        return $qb->getQuery()->getResult();
    }
     */

    /**
     * Set examen
     *
     * @param Examen $examen
     *
     * @return Examen
     */
    public function setExamen(Examen $examen){
        $this->examen = $examen;

        return $this;
    }

    /**
     * Get examen
     *
     * @return Examen
     */
    public function getExamen(){
        return $this->examen;
    }


    /**
     * Set texto
     *
     * @param string $texto
     *
     * @return Pregunta
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Remove respuesta
     * @param Respuesta $respuesta
     */
    public function removeRespuesta( Respuesta $respuesta ) {
        $this->respuestas->removeElement( $respuesta );
    }

    /**
     * Get respuestas
     * @return ArrayCollection 
     */
    public function getRespuestas() {
        return $this->respuestas;
    }

    /**
     * Set respuestas
     * @param ArrayCollection 
     */
    public function setRespuestas( ArrayCollection $respuestas ) {
        $this->respuestas = $respuestas;
    }

    /**
     * Add Respuesta
     * @param Respuesta $respuesta
     */
    public function addRespuestas( Respuesta $respuesta ) {
        $this->respuestas[] = $respuesta;
    }

    /**
     * @return int
     */
    public function getTieneRespuestasCorrecta()
    {
        return $this->tieneRespuestasCorrecta;
    }

    /**
     * @param int $tieneRespuestasCorrecta
     */
    public function setTieneRespuestasCorrecta($tieneRespuestasCorrecta)
    {
        $this->tieneRespuestasCorrecta = $tieneRespuestasCorrecta;
    }

}

