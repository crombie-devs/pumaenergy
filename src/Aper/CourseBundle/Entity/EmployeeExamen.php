<?php

namespace Aper\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Aper\UserBundle\Entity\Employee;
use Aper\CourseBundle\Entity\EmployeePregunta;
use Aper\CourseBundle\Entity\EmployeeRespuesta;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * EmployeeExamen
 *
 * @ORM\Entity(repositoryClass="Aper\CourseBundle\Repository\EmployeeExamenRepository")
 * @ORM\Table(name="employee_examen")
 */
class EmployeeExamen
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var DateTime
     * @ORM\Column(name="fecha_examen", type="datetime", nullable=true)
     */
    private $fechaExamen;

    /**
     * @ORM\Column(name="aprobado", type="boolean")
     */
    private $aprobado = false;

    /**
     * @var int
     *
     * @ORM\Column(name="cant_intentos", type="integer")
     */
    private $cantIntentos;

    /**
     * @var Examen
     * @ORM\ManyToOne(targetEntity="Examen")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="examen_id", referencedColumnName="id")
     * })
     */
    private $examen;

    /**
     * @var float
     *
     * @ORM\Column(name="porcentaje", type="float", precision=3, scale=2, nullable=true)
     */
    private $porcentaje;

    /**
     * @var Employee
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\Employee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     * })
     */
    private $employee;

    // Preguntas que le tocaron al usuario
    /**
     * @var EmployeePregunta[]|Collection
     * @ORM\OneToMany(targetEntity="EmployeePregunta", mappedBy="examen")
     */
    private $preguntas;

    // Respuestas que contesto el usuario
    /**
     * @var EmployeeRespuesta[]|Collection
     * @ORM\OneToMany(targetEntity="EmployeeRespuesta", mappedBy="examen")
     */
    private $respuestas;

    public function __construct() {
        $this->preguntas = new ArrayCollection();
        $this->respuestas = new ArrayCollection();
    }

    public function setFechaExamen(\DateTime $fechaExamen){
        $this->fechaExamen = $fechaExamen;
        return $this->fechaExamen;
    }
    public function getFechaExamen(){
        return $this->fechaExamen;
    }
    /**
     * Remove pregunta
     * @param EmployeeRespuesta $pregunta
     */
    public function removeEmployeePregunta( EmployeePregunta $pregunta ) {
        $this->preguntas->removeElement( $pregunta );
    }

    /**
     * Get preguntas
     * @return EmployeePregunta[]|ArrayCollection
     */
    public function getEmployeePreguntas() {
        return $this->preguntas;
    }

    /**
     * Set preguntas
     * @param ArrayCollection 
     */
    public function setEmployeePreguntas( ArrayCollection $preguntas ) {
        $this->preguntas = $preguntas;
    }

    /**
     * Add EmployeePregunta
     * @param EmployeePregunta $pregunta
     */
    public function addEmployeePreguntas( EmployeePregunta $pregunta ) {
        $this->preguntas[] = $pregunta;
    }

    /**
     * Remove respuesta
     * @param EmployeeRespuesta $respuesta
     */
    public function removeEmployeeRespuesta( EmployeeRespuesta $respuesta ) {
        $this->respuestas->removeElement( $respuesta );
    }

    /**
     * Get respuestas
     * @return EmployeeRespuesta[]|ArrayCollection 
     */
    public function getEmployeeRespuestas() {
        return $this->respuestas->toArray();
    }

    /**
     * Set respuestas
     * @param ArrayCollection 
     */
    public function setEmployeeRespuestas( ArrayCollection $respuestas ) {
        $this->respuestas = $respuestas;
    }

    /**
     * Add EmployeeRespuesta
     * @param EmployeeRespuesta $respuesta
     */
    public function addEmployeeRespuestas( EmployeeRespuesta $respuesta ) {
        $this->respuestas[] = $respuesta;
    }

    public function setPorcentaje($porcentaje){
        $this->porcentaje = $porcentaje;
        return $this;
    }

    public function getPorcentaje(){
        return $this->porcentaje;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set examen
     *
     * @param Examen $examen
     *
     * @return Examen
     */
    public function setExamen(Examen $examen){
        $this->examen = $examen;

        return $this;
    }

    public function setAprobado($aprobado){
        $this->aprobado = $aprobado;
        return $this;
    }

    public function getAprobado(){
        return $this->aprobado;
    }
    public function setCantIntentos($cantIntentos){
        $this->cantIntentos = $cantIntentos;
        return $this;
    }

    public function getCantIntentos(){
        return $this->cantIntentos;
    }

    /**
     * Get examen
     *
     * @return Examen
     */
    public function  getExamen(){
        return $this->examen;
    }

    public function setEmployee(Employee $employee){
        $this->employee = $employee;
        return $this;
    }

    public function getEmployee(){
        return $this->employee;
    }
}

