<?php

namespace Aper\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Aper\UserBundle\Entity\Employee;
use Aper\CourseBundle\Entity\Respuesta;
use Aper\CourseBundle\Entity\EmployeeExamen;
/**
 * EmployeeRespuesta
 *
 * @ORM\Entity(repositoryClass="Aper\CourseBundle\Repository\EmployeeRespuestaRepository")
 * @ORM\Table(name="employee_respuesta")
 */
class EmployeeRespuesta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Respuesta
     * @ORM\ManyToOne(targetEntity="Respuesta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="respuesta_id", referencedColumnName="id")
     * })
     */
    private $respuesta;

    /**
     * @ORM\Column(name="verdadera", type="boolean")
     */
    private $verdadera;

    /**
     * @var Employee
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\Employee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     * })
     */
    private $employee;

    /**
     * @var EmployeeExamen
     * @ORM\ManyToOne(targetEntity="EmployeeExamen", inversedBy="respuestas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="employee_examen_id", referencedColumnName="id")
     * })
     */
    private $examen;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getExamen(){
        return $this->examen;
    }

    public function setExamen($examen){
        $this->examen = $examen;
        return $this;
    }

    public function getVerdadera(){
        return $this->verdadera;
    }

    public function setVerdadera($verdadera){
        $this->verdadera = $verdadera;
        return $this;
    }

    /**
     * Set respuesta
     *
     * @param Respuesta $respuesta
     *
     * @return Respuesta
     */
    public function setRespuesta(Respuesta $respuesta){
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get respuesta
     *
     * @return Respuesta
     */
    public function getRespuesta(){
        return $this->respuesta;
    }

    public function setEmployee(Employee $employee){
        $this->employee = $employee;
        return $this;
    }

    public function getEmployee(){
        return $this->employee;
    }
}

