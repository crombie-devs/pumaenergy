<?php

namespace Aper\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Aper\CourseBundle\Entity\Course;
use Aper\CourseBundle\Entity\Pregunta;

/*
    Descripcion del funcionamiento:
1- ABM de examenes dentro de Cursos
2- ABM de preguntas dentro de examenes
3- ABM de respuestas dentro de preguntas

El curso tiene tres entidades de examen guardado:
encuesta (donde se marca que eso es una encuesta y te muestra 
todas las preguntas), examen previo, examen final (estos dos últimos te da 
preguntas al azar).

El usuario va a un examen y se le crea una relación EmployeeExamen
El controlador de examen elije una pregunta al azar y envia el formulario a
la vista, al mismo tiempo se crea EmployeePregunta para que tenga un estado
guardado de cual es la pregunta actual.
Al contestarla se crea la relación EmployeeRespuesta que adquiere el booleano si
es correcta.

El examen va a generar N preguntas en función de la cantidad de preguntas que quedó 
configurado.
*/

/**
 * Examen
 *
 * @ORM\Entity(repositoryClass="Aper\CourseBundle\Repository\ExamenRepository")
 * @ORM\Table(name="examen")
 */
class Examen
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="encuesta", type="boolean")
     */
    private $encuesta = false;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=80)
     */
    private $titulo;

    /**
     * @var int
     *
     * @ORM\Column(name="cant_preg", type="integer")
     */
    private $cantPreg;

    /**
     * @var int
     *
     * @ORM\Column(name="porcentaje_aprobacion", type="integer")
     */
    private $porcentajeAprobacion;

    /**
     * @ORM\OneToOne(targetEntity="Course", inversedBy="encuesta")
     */
    private $cursoEncuesta;

    /**
     * @ORM\OneToOne(targetEntity="Course", inversedBy="examenPrevio")
     */
    private $cursoPrevia;

    /**
     * @ORM\OneToOne(targetEntity="Course", inversedBy="examenFinal")
     */
    private $cursoFinal;

    /**
     * @ORM\OneToOne(targetEntity="CursoPadre", inversedBy="encuesta")
     */
    private $cursoPadreEncuesta;

    /**
     * @ORM\OneToOne(targetEntity="CursoPadre", inversedBy="examenPrevio")
     */
    private $cursoPadrePrevia;

    /**
     * @ORM\OneToOne(targetEntity="CursoPadre", inversedBy="examenFinal")
     */
    private $cursoPadreFinal;

    /**
     * @ORM\OneToMany(targetEntity="Pregunta", mappedBy="examen")
     */
    private $preguntas;

    /**
     * @ORM\Column(name="encuesta_valida", type="boolean", nullable=true)
     */
    private $encuestaValida;

    /**
     * @ORM\Column(name="examen_valido", type="boolean", nullable=true)
     */
    private $examenValido;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad_intentos", type="integer")
     */
    private $cantidadIntentos;

    public function __construct() {
        $this->preguntas = new ArrayCollection();
    }

    public function getCursoPadreEncuesta(){
        return $this->cursoPadreEncuesta;
    }

    public function setCursoPadreEncuesta($cursoPadreEncuesta){
        $this->cursoPadreEncuesta = $cursoPadreEncuesta;
    }

    public function getCursoPadrePrevia(){
        return $this->cursoPadrePrevia;
    }

    public function setCursoPadrePrevia($cursoPadrePrevia){
        $this->cursoPadrePrevia = $cursoPadrePrevia;
    }

    public function getCursoPadreFinal(){
        return $this->cursoPadreFinal;
    }

    public function setCursoPadreFinal($cursoPadreFinal){
        $this->cursoPadreFinal = $cursoPadreFinal;
    }

    public function setEncuesta($encuesta){
        $this->encuesta = $encuesta;
        return $this;
    }

    public function getEncuesta(){
        return $this->encuesta;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setCursoEncuesta(Course $cursoEncuesta = null){
        $this->cursoEncuesta = $cursoEncuesta;
        return $this;
    }

    public function setCursoPrevia(Course $cursoPrevia = null){
        $this->cursoPrevia = $cursoPrevia;
        return $this;
    }

    public function setCursoFinal(Course $cursoFinal = null){
        $this->cursoFinal = $cursoFinal;
        return $this;
    }

    public function getCursoEncuesta(){
        return $this->cursoEncuesta;
    }

    public function getCursoPrevia(){
        return $this->cursoPrevia;
    }

    public function getCursoFinal(){
        return $this->cursoFinal;
    }


    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Examen
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set cantPreg
     *
     * @param integer $cantPreg
     *
     * @return Examen
     */
    public function setCantPreg($cantPreg)
    {
        $this->cantPreg = $cantPreg;

        return $this;
    }

    /**
     * Get cantPreg
     *
     * @return int
     */
    public function getCantPreg()
    {
        return $this->cantPreg;
    }

    /**
     * Set porcentajeAprobacion
     *
     * @param integer $porcentajeAprobacion
     *
     * @return Examen
     */
    public function setPorcentajeAprobacion($porcentajeAprobacion)
    {
        $this->porcentajeAprobacion = $porcentajeAprobacion;

        return $this;
    }

    /**
     * Get porcentajeAprobacion
     *
     * @return int
     */
    public function getPorcentajeAprobacion()
    {
        return $this->porcentajeAprobacion;
    }

    /**
     * Remove pregunta
     * @param Pregunta $pregunta
     */
    public function removePregunta( Pregunta $pregunta ) {
        $this->preguntas->removeElement( $pregunta );
    }

    /**
     * Get preguntas
     * @return ArrayCollection 
     */
    public function getPreguntas() {
        return $this->preguntas;
    }

    /**
     * Set preguntas
     * @param ArrayCollection 
     */
    public function setPreguntas( ArrayCollection $preguntas ) {
        $this->preguntas = $preguntas;
    }

    /**
     * Add Pregunta
     * @param Pregunta $pregunta
     */
    public function addPreguntas( Pregunta $pregunta ) {
        $this->preguntas[] = $pregunta;
    }

    public function __toString() {
        return (string)$this->getTitulo();
    }

    /**
     * @return int
     */
    public function getCantidadIntentos()
    {
        return $this->cantidadIntentos;
    }

    /**
     * @param int $cantidadIntentos
     */
    public function setCantidadIntentos($cantidadIntentos)
    {
        $this->cantidadIntentos = $cantidadIntentos;
    }

    /**
     * @return mixed
     */
    public function getExamenValido()
    {
        return $this->examenValido;
    }

    /**
     * @param mixed $examenValido
     */
    public function setExamenValido($examenValido)
    {
        $this->examenValido = $examenValido;
    }

    /**
     * @return mixed
     */
    public function getEncuestaValida()
    {
        return $this->encuestaValida;
    }

    /**
     * @param mixed $encuestaValida
     */
    public function setEncuestaValida($encuestaValida)
    {
        $this->encuestaValida = $encuestaValida;
    }


}

