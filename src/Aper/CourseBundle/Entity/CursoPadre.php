<?php

namespace Aper\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\OneToMany;
use Aper\CourseBundle\Entity\Pregunta;
use Aper\CourseBundle\Entity\Course;
use Aper\CourseBundle\Entity\CursoPadreImage;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CursoPadre
 *
 * @ORM\Entity
 * @ORM\Table(name="curso_padre")
 */
class CursoPadre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Examen", mappedBy="cursoPadreEncuesta", cascade={"all"})
     */
    private $encuesta;

    /**
     * @ORM\OneToOne(targetEntity="Examen", mappedBy="cursoPadrePrevia", cascade={"all"})
     */
    private $examenPrevio;

    /**
     * @ORM\OneToOne(targetEntity="Examen", mappedBy="cursoPadreFinal", cascade={"all"})
     */
    private $examenFinal;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=100)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=400)
     */
    private $descripcion;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @ORM\OneToMany(targetEntity="Course", mappedBy="cursoPadre")
     */
    private $cursos;

    /**
     * One course has many contenidoDeClases. This is the inverse side.
     * @OneToMany(targetEntity="ContenidoDeClase", mappedBy="cursoPadre")
     */
    private $contenidoDeClases;

    /**
     *
     * @var CursoPadreImage
     * @ORM\OneToOne(targetEntity="CursoPadreImage", mappedBy="cursoPadre", cascade={"all"})
     * @Assert\NotBlank()
     * @Assert\Valid()
     */
    private $imagen;

    /**
     * @var string[]
     *
     * @ORM\Column(name="roles_allowed", type="simple_array")
     * @Assert\Count(min="1", minMessage="Debe elegir un rol como mínimo")
     */
    private $rolesAllowed;

    public function __construct() {
        $this->cursos = new ArrayCollection();
    }

    /**
     * Remove curso
     * @param Course $curso
     */
    public function removeCourse( Course $curso ) {
        $this->cursos->removeElement( $curso );
    }

    /**
     * Add Course
     * @param Course $curso
     */
    public function addCourses( Course $curso ) {
        $this->cursos[] = $curso;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setEncuesta(Examen $encuesta){
        $this->encuesta = $encuesta;
        return $this;
    }

    public function setExamenPrevio(Examen $examenPrevio){
        $this->examenPrevio = $examenPrevio;
        return $this;
    }

    public function setExamenFinal(Examen $examenFinal){
        $this->examenFinal = $examenFinal;
        return $this;
    }

    public function getEncuesta(){
        return $this->encuesta;
    }

    public function getExamenPrevio(){
        return $this->examenPrevio;
    }

    public function getExamenFinal(){
        return $this->examenFinal;
    }

    public function setTitulo($titulo){
        $this->titulo = $titulo;
    }
    public function getTitulo(){
        return $this->titulo;
    }
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    public function setActivo($activo){
        $this->activo = $activo;
    }
    public function getActivo(){
        return $this->activo;
    }

    /**
     * Set rolesAllowed.
     *
     * @param string $rolesAllowed
     *
     * @return Course
     */
    public function setRolesAllowed($rolesAllowed)
    {
        $this->rolesAllowed = $rolesAllowed;

        return $this;
    }

    /**
     * Get rolesAllowed.
     *
     * @return string
     */
    public function getRolesAllowed()
    {
        return $this->rolesAllowed;
    }

    /**
     * @param $roleAllowed
     *
     * @return $this
     */
    public function addRoleAllowed($roleAllowed)
    {
        $roleAllowed = strtoupper($roleAllowed);

        if (!in_array($roleAllowed, $this->rolesAllowed)) {

            $this->rolesAllowed[] = $roleAllowed;
        }

        return $this;
    }

    /**
     * @param $roleAllowed
     *
     * @return $this
     */
    public function removeRoleAllowed($roleAllowed)
    {
        if (false !== $key = array_search(strtoupper($roleAllowed), $this->rolesAllowed, true)) {
            unset($this->rolesAllowed[$key]);
            $this->rolesAllowed = array_values($this->rolesAllowed);
        }

        return $this;
    }

    public function hasRoleAllowed($roleAllowed)
    {
        return false !== $key = array_search(strtoupper($roleAllowed), $this->rolesAllowed, true);
    }

    public function setImagen(CursoPadreImage $imagen){
        $this->imagen = $imagen;
    }
    public function getImagen(){
        return $this->imagen;
    }

    /**
     * @return mixed
     */
    public function getContenidoDeClases()
    {
        return $this->contenidoDeClases;
    }

    /**
     * @param mixed $contenidoDeClases
     */
    public function setContenidoDeClases($contenidoDeClases)
    {
        $this->contenidoDeClases = $contenidoDeClases;
    }

    /**
     * Add ContenidoDeClase
     * @param ContenidoDeClase $contenidoDeClase
     */
    public function addContenidoDeClase( ContenidoDeClase $contenidoDeClase ) {
        $this->contenidoDeClases[] = $contenidoDeClase;
    }

}

