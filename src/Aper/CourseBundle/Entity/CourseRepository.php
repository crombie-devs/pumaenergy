<?php

namespace Aper\CourseBundle\Entity;

use Aper\TrainingKitBundle\Entity\TrainingFolder;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * CourseRepository
 *
 */
class CourseRepository extends EntityRepository
{

    /**
     *
     * @param Category $category
     * @param TrainingFolder $folder
     * @param null $since
     * @param null $until
     * @param null $rol
     * @return QueryBuilder
     */
    public function createQueryBuilderByCategoryFolderAndTag (Category $category = null, TrainingFolder $folder = null,  $since = null,  $until = null,  $rol=null)
    {
        $queryBuilder = $this->createQueryBuilder('Course')
                ->addSelect('Image')
                ->leftJoin('Course.mainImage', 'Image')
                ->orderBy('Course.createdAt', 'DESC');

        if ($since) {
            $queryBuilder->andWhere('Course.createdAt >= :since')
                        ->setParameter('since', $since);
        }

       if ($until) {
            $queryBuilder->andWhere('Course.createdAt <= :until')
                         ->setParameter('until', $until);
        }

        if ($category) {
            $queryBuilder->andWhere('Course.category = :category')
                ->setParameter('category', $category->getId());
        }

        if ($folder) {
            $queryBuilder->andWhere('Course.trainingFolder = :folder')
                ->setParameter('folder', $folder->getId());
        }

        if ($rol) {
            $roleDQL = [];
            foreach ($rol as $i => $r) {
                $roleDQL[] = 'Course.rolesAllowed LIKE :rol' . $i;
                $queryBuilder
                    ->setParameter('rol' . $i, '%' . $r . '%');
            }
    
            $queryBuilder->andwhere(implode(' OR ', $roleDQL));
        }
        return $queryBuilder;
    }


    /**
     * @return QueryBuilder
     */
    public function createQueryBuilderByCategoryFolderAndTagForCursoPadre ($cursoPadreId)
    {
        $category = null;
        $folder = null;
        $since = null;
        $until = null;
        $rol=null;

        $queryBuilder = $this->createQueryBuilder('Course')
                ->addSelect('Image')
                ->leftJoin('Course.mainImage', 'Image')
                ->orderBy('Course.createdAt', 'DESC');

        if ($cursoPadreId) {
            $queryBuilder->andWhere('Course.cursoPadre = :padre')
                        ->setParameter('padre', $cursoPadreId);
        }

        if ($since) {
            $queryBuilder->andWhere('Course.createdAt >= :since')
                        ->setParameter('since', $since);
        }

       if ($until) {
            $queryBuilder->andWhere('Course.createdAt <= :until')
                         ->setParameter('until', $until);
        }

        if ($category) {
            $queryBuilder->andWhere('Course.category = :category')
                ->setParameter('category', $category->getId());
        }

        if ($folder) {
            $queryBuilder->andWhere('Course.trainingFolder = :folder')
                ->setParameter('folder', $folder->getId());
        }

        if ($rol) {
            $roleDQL = [];
            foreach ($rol as $i => $r) {
                $roleDQL[] = 'Course.rolesAllowed LIKE :rol' . $i;
                $queryBuilder
                    ->setParameter('rol' . $i, '%' . $r . '%');
            }

            $queryBuilder->andwhere(implode(' OR ', $roleDQL));
        }
        return $queryBuilder;
    }

    /**
     *
     * @param Category $category
     * @param TrainingFolder $folder
     * @param null $rol
     * @param int $limit
     * @param null $id
     * @return array
     */
    public function findRelatedCourse (Category $category = null, TrainingFolder $folder = null,   $rol=null, $limit=4 , $id=null)
    {
        $queryBuilder = $this->createQueryBuilder('Course')
            ->addSelect('Image')
            ->leftJoin('Course.mainImage', 'Image')
            ->where('Course.active = true')
            ->orderBy('Course.createdAt', 'DESC');

        if ($category) {
            $queryBuilder->andWhere('Course.category = :category')
                ->setParameter('category', $category->getId());
        }

        if ($folder) {
            $queryBuilder->andWhere('Course.folder = :folder')
                ->setParameter('folder', $folder->getId());
        }

        if ($id) {
            $queryBuilder->andWhere('Course.id <> :id')
                ->setParameter('id', $id);
        }

        if ($rol) {
            $roleDQL = [];
            foreach ($rol as $i => $r) {
                $roleDQL[] = 'Course.rolesAllowed LIKE :rol' . $i;
                $queryBuilder
                    ->setParameter('rol' . $i, '%' . $r . '%');

            }
            $queryBuilder->andwhere(implode(' OR ', $roleDQL));
        }

        return $queryBuilder->setMaxResults($limit)->getQuery()->getResult();
    }

    /**
     *
     * @param Category $category
     * @param TrainingFolder $folder
     * @param null $rol
     * @param int $limit
     * @param null $id
     * @return Course
     * @throws NonUniqueResultException
     */
    public function findxid (Category $category = null, TrainingFolder $folder = null, $rol=null, $limit=1 , $id=null)
    {
        $queryBuilder = $this->createQueryBuilder('Course')
            ->addSelect('Image')
            ->leftJoin('Course.mainImage', 'Image')
            ->where('Course.active = true')
            ->orderBy('Course.createdAt', 'DESC');

        if ($category) {
            $queryBuilder->andWhere('Course.category = :category')
                ->setParameter('category', $category->getId());
        }

        if ($folder) {
            $queryBuilder->andWhere('Course.folder = :folder')
                ->setParameter('folder', $folder->getId());
        }

        if ($id) {
            $queryBuilder->andWhere('Course.id = :id')
                ->setParameter('id', $id);
        }

        if ($rol) {
            $roleDQL = [];
            foreach ($rol as $i => $r) {
                $roleDQL[] = 'Course.rolesAllowed LIKE :rol' . $i;
                $queryBuilder
                    ->setParameter('rol' . $i, '%' . $r . '%');

            }
            $queryBuilder->andwhere(implode(' OR ', $roleDQL));
        }

        return $queryBuilder->setMaxResults($limit)->getQuery()->getOneOrNullResult();
    }


    /**
     *
     * @param Category $category
     * @param TrainingFolder $folder
     * @param null $since
     * @param null $until
     * @param null $rol
     * @return QueryBuilder
     */
    public function createQueryBuilderByCategoryFolderAndTagFe (Category $category = null, TrainingFolder $folder = null, $since = null,  $until = null,  $rol=null)
    {
        $queryBuilder = $this->createQueryBuilder('Course')
            ->addSelect('Image')
            ->leftJoin('Course.mainImage', 'Image')
            ->where('Course.active = true')
            ->orderBy('Course.createdAt', 'DESC');

        if ($since) {
            $queryBuilder->andWhere('Course.createdAt >= :since')
                ->setParameter('since', $since);
        }

        if ($until) {
            $queryBuilder->andWhere('Course.createdAt <= :until')
                ->setParameter('until', $until);
        }

        if ($category) {
            $queryBuilder->andWhere('Course.category = :category')
                ->setParameter('category', $category->getId());
        }
        
        if ($folder) {
            $queryBuilder->andWhere('Course.trainingFolder = :folder')
                ->setParameter('folder', $folder->getId());
        }

        if ($rol) {
            $roleDQL = [];
            foreach ($rol as $i => $r) {
                $roleDQL[] = 'Course.rolesAllowed LIKE :rol' . $i;
                $queryBuilder
                    ->setParameter('rol' . $i, '%' . $r . '%');

            }
            $queryBuilder->andwhere(implode(' OR ', $roleDQL));
        }

        return $queryBuilder;
    }

    /**
     *
     * @param null $rol
     * @return array
     */
    public function createQueryBuilderByRoles ($rol=null)
    {
        $queryBuilder = $this->createQueryBuilder('Course')
            ->addSelect('Image')
            ->leftJoin('Course.mainImage', 'Image')
            ->where('Course.active = true')
            ->orderBy('Course.createdAt', 'DESC');

        if ($rol) {
            $roleDQL = [];
            foreach ($rol as $i => $r) {
                $roleDQL[] = 'Course.rolesAllowed LIKE :rol' . $i;
                $queryBuilder
                    ->setParameter('rol' . $i, '%' . $r . '%');

            }
            $queryBuilder->andwhere(implode(' OR ', $roleDQL));
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     *
     * @param int $limit
     * @return array
     */
    public function findLastest($limit = 2)
    {
        $queryBuilder = $this->createQueryBuilder('Course')
                ->addSelect('Image')
                ->addSelect('Comment')
                ->addSelect('User')
                ->addSelect('Profile')
                ->leftJoin('Course.mainImage', 'Image')
                ->leftJoin('Course.comments', 'Comment', Join::WITH, "Comment.status = 'published'")
                ->leftJoin('Comment.user', 'User')
                ->leftJoin('User.profile', 'Profile')
                ->orderBy('Course.createdAt', 'DESC')
                ->where('Course.highlighted = false');

        return $queryBuilder->setMaxResults($limit)->getQuery()->getResult();
    }

    /**
     *
     * @param int $limit
     * @return array
     */
    public function findHighlighted($limit = 2)
    {
        $queryBuilder = $this->createQueryBuilder('Course')
                ->addSelect('Image')
                ->addSelect('Comment')
                ->addSelect('User')
                ->addSelect('Profile')
                ->leftJoin('Course.mainImage', 'Image')
                ->leftJoin('Course.comments', 'Comment', Join::WITH, "Comment.status = 'published'")
                ->leftJoin('Comment.user', 'User')
                ->leftJoin('User.profile', 'Profile')
                ->orderBy('Course.createdAt', 'DESC')
                ->where('Course.highlighted = true');

        return $queryBuilder->setMaxResults($limit)->getQuery()->getResult();
    }

}
