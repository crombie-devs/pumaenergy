<?php

namespace Aper\CourseBundle\Service;

use Aper\CourseBundle\Entity\Pdf;
use WebFactory\Bundle\FileBundle\Model\File;
use WebFactory\Bundle\FileBundle\Util\FilenameResolver;

/**
 * Class FilenameResolver
 * @package WebFactory\Bundle\FileBundle\Util
 */
class CustomFilenameResolver extends FilenameResolver
{


    /**
     * @param File $entity
     * @return array
     */
    public function getFilenameAndExtension(File $entity)
    {
        $result = parent::getFilenameAndExtension($entity);

        if ($entity instanceof Pdf) {
            $result[1] = 'pdf';
        }

        return $result;
    }

}