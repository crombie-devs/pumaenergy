<?php

namespace Aper\CourseBundle\Controller;

use Aper\UserBundle\Entity\Employee;
use Aper\CourseBundle\Entity\Course;
use Aper\CourseBundle\Entity\CursoPadre;
use Aper\CourseBundle\Entity\EmployeeExamen;
use Aper\CourseBundle\Entity\EmployeePregunta;
use Aper\CourseBundle\Entity\EmployeeRespuesta;
use Aper\CourseBundle\Entity\Examen;
use Aper\CourseBundle\Entity\Pregunta;
use Aper\CourseBundle\Entity\Respuesta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Respuesta controller.
 *
 * @Route("admin/course")
 */
class ExamenBackendController extends Controller
{
    /** 
     * Lists all respuesta entities.
     *
     * @Route("/lista/{id}/resultado_examenes", name="lista_resultados_examenes_index")
     * @Method("GET")
     */
    public function indexResultadoExamensAction(Course $course, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);
//        $employeeExamen = $repository->getAprobados($course);
        $employeesExamensEncuestas = $repository->getListadoResultados($course->getEncuesta());
        $employeesExamensPrevios = $repository->getListadoResultados($course->getExamenPrevio());
        $employeesExamensFinales = $repository->getListadoResultados($course->getExamenFinal());
//        dump($employeesExamens);
//        \Doctrine\Common\Util\Debug::dump($employeesExamens);
//        die();
        $paginator1 = $this->get('knp_paginator');
        $paginationEncuestas = $paginator1->paginate(
            $employeesExamensEncuestas, $request->query->getInt('page' , 1),
            10
        );
        $paginator2 = $this->get('knp_paginator');
        $paginationEPrevios = $paginator2->paginate(
            $employeesExamensPrevios, $request->query->getInt('page' , 1),
            10
        );
        $paginator3 = $this->get('knp_paginator');
        $paginationEFinales = $paginator3->paginate(
            $employeesExamensFinales, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('AperCourseBundle:Backend/ExamenResults:index.html.twig', array('paginationEncuestas' => $paginationEncuestas,'paginationEPrevios' => $paginationEPrevios,'paginationEFinales' => $paginationEFinales, 'course' => $course));
    }

//    /**
//     * Lists all respuesta entities.
//     *
//     * @Route("/{id}/reprobados", name="respuesta_index")
//     * @Method("GET")
//     */
//    public function indexReprobadosAction(Course $course)
//    {
//        $repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);
//        $employeeExamen = $repository->getReprobados($course);
//        $paginator = $this->get('knp_paginator');
//        $pagination = $paginator->paginate(
//            $employeeExamen, $request->query->getInt('page' , 1),
//            10
//        );
//
//        //return $this->render('AperCourseBundle:Backend/Category:index.html.twig', array('pagination' => $pagination, 'employeeExamen' => $employeeExamen));
//    }



    /**
     * Finds and displays a respuesta entity.
     *
     * @Route("/show_examen/{id}", name="examen_empleado_show")
     * @Method("GET")
     */
    public function showAction(EmployeeExamen $employeeExamen)
    {
        $examen = $employeeExamen->getExamen();
        $tipoExamen = '';
        if(!(is_null($examen->getCursoEncuesta()))){
            $tipoExamen = "Encuesta";
            $curso = $examen->getCursoEncuesta();
        }
        if(!(is_null($examen->getCursoPrevia()))){
            $tipoExamen = "Examen Previo";
            $curso = $examen->getCursoPrevia();
        }
        if(!(is_null($examen->getCursoFinal()))){
            $tipoExamen = "Examen Final";
            $curso = $examen->getCursoFinal();
        }
        return $this->render('AperCourseBundle:Backend/ExamenResults:show.html.twig', array(
            "employeeExamen" => $employeeExamen, 
            "tipoExamen" => $tipoExamen, 
            "curso" => $curso,
        ));
    }

    /**
     *
     * @Route("reiniciar/{id}", name="examen_empleado_reiniciar")
     * @Method("GET")
     */
    public function reiniciarExamenEmployeeAction(EmployeeExamen $employeeExamen){
        $em = $this->getDoctrine()->getManager();
        $exm_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);
        $examenes = $exm_repository->findBy(['employee' => $employeeExamen->getEmployee()->getId(), "examen" => $employeeExamen->getExamen()]);
        foreach ($examenesE as $examenE) {
            if($examenE->getAprobado())
                return $this->render('examen/error.html.twig', array('mensaje' => "El examen lo tiene aprobado.", 'volver' => 'aper_store_index'));
        }
        foreach ($examenesE as $examenE) {
            $examenE->setCantIntentos(0);
        }
        $em->flush();
    }

    /**
     * Finds and displays a respuesta entity.
     *
     * @Route("/relanzarcurso/{curso_id}", name="relanzar_curso")
     * @Method("GET")
     */
    public function relanzarCursoAction(Course $curso_id){
//        dump($curso_id);
//        \Doctrine\Common\Util\Debug::dump($curso_id);
        $em = $this->getDoctrine()->getManager();
        $db = $em->getConnection();
        $stmt = $db->prepare("UPDATE course c 
                JOIN examen e ON c.id = e.cursoFinal_id OR c.id = cursoPrevia_id 
                JOIN employee_examen ee ON ee.examen_id = e.id
                SET ee.cant_intentos = 0
                WHERE c.id = :curso_id");
        $stmt->bindValue( "curso_id", $curso_id->getId() );
        $stmt->execute();
        // Tendria que volver al Action que se ve la grilla que hace esto.
        return $this->redirectToRoute('aper_course_index', array('cursopadre' => $curso_id->getCursoPadre()->getId()));
    }


    /**
     * Finds and displays a respuesta entity.
     *
     * @Route("/relanzarcursopadre/{curso_id}", name="relanzar_curso_padre")
     * @Method("GET")
     */
    public function relanzarCursoPadreAction(CursoPadre $curso_id){
//        dump($curso_id);
//        \Doctrine\Common\Util\Debug::dump($curso_id);
        $em = $this->getDoctrine()->getManager();
        $db = $em->getConnection();
        $stmt = $db->prepare("UPDATE course c 
                JOIN examen e ON c.id = e.cursoPadreFinal_id OR c.id = cursoPrevia_id 
                JOIN employee_examen ee ON ee.examen_id = e.id
                SET ee.cant_intentos = 0
                WHERE c.id = :curso_id");
        $stmt->bindValue( "curso_id", $curso_id->getId() );
        $stmt->execute();
        // Tendria que volver al Action que se ve la grilla que hace esto.
        return $this->redirectToRoute('cursopadre_show', array('id' => $curso_id->getId()));
    }

}
