<?php

namespace Aper\CourseBundle\Controller\Backend;

use Aper\CourseBundle\Entity\Course;
use Aper\CourseBundle\Entity\CursoPadre;
use Aper\CourseBundle\Form\CourseType;
use Aper\CourseBundle\Form\CourseFilterType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Aper\TrainingKitBundle\Entity\TrainingFolder;
use WebFactory\Bundle\GridBundle\Event\ManipulateQueryEvent;
use WebFactory\Bundle\GridBundle\Grid\Configuration;

class CourseController extends Controller
{
    /**
     * Lists all Course entities.
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction(Request $request, CursoPadre $cursopadre)
    {
         $course = $this->getDoctrine()->getRepository(Course::class)->createQueryBuilderByCategoryFolderAndTagForCursoPadre($cursopadre->getId());
         $paginator = $this->get('knp_paginator');
         $pagination = $paginator->paginate(
             $course, $request->query->getInt('page' , 1),
             10
         );

        $examenPrevio = ($cursopadre->getExamenPrevio())? $cursopadre->getExamenPrevio() : null;
        $examenFinal = ($cursopadre->getExamenFinal())?$cursopadre->getExamenFinal() : null;
        $encuesta = ($cursopadre->getEncuesta())? $cursopadre->getEncuesta() : null;
        $em = $this->getDoctrine()->getManager();
        if(!is_null($examenPrevio) && $examenPrevio->getExamenValido() != true){
            $examenPrevio->setExamenValido($this->validarExamen($examenPrevio));
//            $em->persist($examenPrevio);
//            $em->flush();
        }
        if(!is_null($examenFinal) && $examenFinal->getExamenValido() != true){
            $examenFinal->setExamenValido($this->validarExamen($examenFinal));
//            $em->persist($examenFinal);
//            $em->flush();
        }
        if(!is_null($encuesta) && $encuesta->getEncuestaValida() != true){
            $encuesta->setEncuestaValida($this->validarEncuesta($encuesta));
//            $em->persist($encuesta);
//            $em->flush();
        }
        return array(
            'pagination' => $pagination,
            'cursoPadre' => $cursopadre
        );
    }
    /**
     * Creates a new Course entity.
     *
     * @Method("POST")
     * @Template("AperCourseBundle:Backend/Course:new.html.twig")
     */
    public function createAction(Request $request, CursoPadre $cursoPadre)
    {

        $entity  = new Course();
        $form = $this->createForm(new CourseType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->addRoleAllowed('ROLE_ADMIN');
            $entity->setcursoPadre($cursoPadre);
            $em->persist($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Registro creado');

            return $this->redirect($this->generateUrl('aper_course_index', array('cursopadre_id' => $cursoPadre->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Course entity.
     *
     * @Method("GET")
     * @Template()
     */
    public function newAction(CursoPadre $cursoPadre)
    {

        $entity = new Course();
        $form   = $this->createForm(new CourseType(), $entity, [
            'action' => $this->generateUrl('aper_course_create', array('id' => $cursoPadre->getId()))
        ]);

        return array(
            'entity' => $entity,
            'cursoPadre' => $cursoPadre,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Course entity.
     *
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AperCourseBundle:Course');
        $course = $repository->find($id);
        if(!$course){
            $messageException = 'Capacitación no encontrada';
            throw $this->createNotFoundException($messageException);
        }

        $examenPrevio = ($course->getExamenPrevio())? $course->getExamenPrevio() : null;
        $examenFinal = ($course->getExamenFinal())?$course->getExamenFinal() : null;
        $encuesta = ($course->getEncuesta())? $course->getEncuesta() : null;
        $em = $this->getDoctrine()->getManager();
        if(!is_null($examenPrevio) && $examenPrevio->getExamenValido() != true){
            $examenPrevio->setExamenValido($this->validarExamen($examenPrevio));
//            $em->persist($examenPrevio);
//            $em->flush();
        }
        if(!is_null($examenFinal) && $examenFinal->getExamenValido() != true){
            $examenFinal->setExamenValido($this->validarExamen($examenFinal));
//            $em->persist($examenFinal);
//            $em->flush();
        }
        if(!is_null($encuesta) && $encuesta->getEncuestaValida() != true){
            $encuesta->setEncuestaValida($this->validarEncuesta($encuesta));
//            $em->persist($encuesta);
//            $em->flush();
        }

        return $this->render('AperCourseBundle:Backend/Course:show.html.twig', array('course' => $course));
    }

    /**
     * Displays a form to edit an existing Course entity.
     *
     * @Route("/{id}/edit", name="backend_course_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AperCourseBundle:Course')->find($id);
        $form = $this->createForm(CourseType::class, $entity);
        $form->handleRequest($request);

        if (!$entity) {
            throw $this->createNotFoundException('Capacitacion no encontrada.');
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $entity->addRoleAllowed('ROLE_ADMIN');
            $em->persist($entity);
            $em->flush();


            $this->addFlash('success', 'La Capacitación ha sido modificada');

            return $this->redirectToRoute('aper_course_index');
        }

        return $this->render('AperCourseBundle:Backend/Course:edit.html.twig',
            array('entity' => $entity, 'form' => $form->createView()));
    }

    /**
     * Deletes a Course entity.
     *
     * @Route("/{id}/delete", name="backend_course_delete")
     * @Method({"DELETE", "GET"})
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AperCourseBundle:Course')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Course entity.');
            }

            $em->remove($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Registro Eliminado');
        }

        return $this->redirect($this->generateUrl('aper_course_index'));
    }

    /**
     * Creates a form to delete a Course entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id), array('csrf_protection' => false))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    protected function validarExamen($examen)
    {
        $em = $this->getDoctrine()->getManager();
        $preguntasValidas = $em->getRepository('AperCourseBundle:Pregunta')->cantidadPreguntasValidas($examen);

        return ($preguntasValidas >= $examen->getCantPreg());
    }

    protected function validarEncuesta($examen)
    {
        $em = $this->getDoctrine()->getManager();
        $preguntas = $em->getRepository('AperCourseBundle:Pregunta')->cantidadPreguntas($examen);

        return ($preguntas >= $examen->getCantPreg());
    }
}
