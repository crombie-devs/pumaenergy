<?php

namespace Aper\CourseBundle\Controller;

use Aper\CourseBundle\Entity\Examen;
use Aper\CourseBundle\Entity\Course;
use Aper\CourseBundle\Entity\CursoPadre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Examen controller.
 *
 * @Route("admin/course/examen")
 */
class ExamenController extends Controller
{
    private function examenes(Course $curso, $tipo){
        if($tipo == 'encuesta')
            if(!empty($curso->getEncuesta())){
                return $curso->getEncuesta();
            }
        if($tipo == 'ex_final')
            if(!empty($curso->getExamenFinal())){
                return $curso->getExamenFinal();
            }
        if($tipo == 'ex_previo')
            if(!empty($curso->getExamenPrevio())){
                return $curso->getExamenPrevio();
            }
        return null;
    }

    /**
     *
     * @Route("/encuestacurso/{id}/", name="examen_index_encuesta_curso")
     * @Method("GET")
     */
    public function indexEncuestaCursoAction(CursoPadre $curso)
    {
        return $this->render('examen/index.html.twig', array(
            'examen' => $this->examenes($curso,'encuesta'),
            'examen_new' => 'encuesta_curso_new',
            'curso_id' => $curso->getId(),
        ));
    }

    /**
     *
     * @Route("/exfinalcurso/{id}/", name="examen_index_final_curso")
     * @Method("GET")
     */
    public function indexFinalCursoAction(CursoPadre $curso)
    {
        return $this->render('examen/index.html.twig', array(
            'examen' => $this->examenes($curso,'ex_final'),
            'examen_new' => 'ex_final_curso_new',
            'curso_id' => $curso->getId(),
        ));
    }

    /**
     *
     * @Route("/expreviocurso/{id}/", name="examen_index_previo_curso")
     * @Method("GET")
     */
    public function indexPrevioCursoAction(CursoPadre $curso)
    {
        return $this->render('examen/index.html.twig', array(
            'examen' => $this->examenes($curso,'ex_previo'),
            'examen_new' => 'ex_previo_curso_new',
            'curso_id' => $curso->getId(),
        ));
    }



    /**
     *
     * @Route("/encuesta/{id}/", name="examen_index_encuesta")
     * @Method("GET")
     */
    public function indexEncuestaAction(Course $curso)
    {
        return $this->render('examen/index.html.twig', array(
            'examen' => $this->examenes($curso,'encuesta'),
            'examen_new' => 'encuesta_new',
            'curso_id' => $curso->getId(),
        ));
    }

    /**
     *
     * @Route("/exfinal/{id}/", name="examen_index_final")
     * @Method("GET")
     */
    public function indexFinalAction(Course $curso)
    {
        return $this->render('examen/index.html.twig', array(
            'examen' => $this->examenes($curso,'ex_final'),
            'examen_new' => 'ex_final_new',
            'curso_id' => $curso->getId(),
        ));
    }

    /**
     *
     * @Route("/exprevio/{id}/", name="examen_index_previo")
     * @Method("GET")
     */
    public function indexPrevioAction(Course $curso)
    {
        return $this->render('examen/index.html.twig', array(
            'examen' => $this->examenes($curso,'ex_previo'),
            'examen_new' => 'ex_previo_new',
            'curso_id' => $curso->getId(),
        ));
    }

    /**
     * Creates a new examen entity.
     *
     * @Route("/{id}/exfinalcurso/new", name="ex_final_curso_new")
     * @Method({"GET", "POST"})
     */
    public function newFinalCursoAction(Request $request, CursoPadre $curso)
    {
        $examen = new Examen();
        $form = $this->createForm('Aper\CourseBundle\Form\ExamenType', $examen);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $examen->setCursoPadreFinal($curso);
            $em->persist($examen);
            $em->flush();
            // Examen Show de cursos
            return $this->redirectToRoute('examen_curso_show', array('id' => $examen->getId()));
        }

        return $this->render('examen/new_curso.html.twig', array(
            'tipo' => 'examenfinal',
            'examen' => $examen,
            'ruta' => 'examen_index_final',
            'curso_id' => $curso->getId(),
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new examen entity.
     *
     * @Route("/{id}/expreviocurso/new", name="ex_previo_curso_new")
     * @Method({"GET", "POST"})
     */
    public function newPrevioCursoAction(Request $request, CursoPadre $curso)
    {
        $examen = new Examen();
        $form = $this->createForm('Aper\CourseBundle\Form\ExamenType', $examen);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $examen->setCursoPadrePrevia($curso);
            $em = $this->getDoctrine()->getManager();
            $em->persist($examen);
            $em->flush();

            return $this->redirectToRoute('examen_curso_show', array('id' => $examen->getId()));
        }

        return $this->render('examen/new_curso.html.twig', array(
            'tipo' => 'examenprevio',
            'examen' => $examen,
            'ruta' => 'examen_index_previo',
            'curso_id' => $curso->getId(),
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new examen entity.
     *
     * @Route("/{id}/encuestacurso/new", name="encuesta_curso_new")
     * @Method({"GET", "POST"})
     */
    public function newEncuestaCursoAction(Request $request, CursoPadre $curso)
    {
        $examen = new Examen();
        $form = $this->createForm('Aper\CourseBundle\Form\ExamenType', $examen);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $examen->setCursoPadreEncuesta($curso);
            $examen->setEncuesta(true);
            $em->persist($examen);
            $em->flush();
            // Examen Show de cursos
            return $this->redirectToRoute('examen_curso_show', array('id' => $examen->getId()));
        }

        return $this->render('examen/new_curso.html.twig', array(
            'tipo' => 'encuesta',
            'examen' => $examen,
            'ruta' => 'examen_index_encuesta',
            'curso_id' => $curso->getId(),
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new examen entity.
     *
     * @Route("/{id}/encuesta/new", name="encuesta_new")
     * @Method({"GET", "POST"})
     */
    public function newEncuestaAction(Request $request, Course $curso)
    {
        $examen = new Examen();
        $form = $this->createForm('Aper\CourseBundle\Form\ExamenType', $examen);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $examen->setCursoEncuesta($curso);
            $examen->setEncuesta(true);
            $em->persist($examen);
            $em->flush();

            return $this->redirectToRoute('examen_show', array('id' => $examen->getId()));
        }

        return $this->render('examen/new.html.twig', array(
            'tipo' => 'encuesta',
            'examen' => $examen,
            'ruta' => 'examen_index_encuesta',
            'curso_id' => $curso->getId(),
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new examen entity.
     *
     * @Route("/{id}/exfinal/new", name="ex_final_new")
     * @Method({"GET", "POST"})
     */
    public function newFinalAction(Request $request, Course $curso)
    {
        $examen = new Examen();
        $form = $this->createForm('Aper\CourseBundle\Form\ExamenType', $examen);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $examen->setCursoFinal($curso);
            $em->persist($examen);
            $em->flush();

            return $this->redirectToRoute('examen_show', array('id' => $examen->getId()));
        }

        return $this->render('examen/new.html.twig', array(
            'tipo' => 'examenfinal',
            'examen' => $examen,
            'ruta' => 'examen_index_final',
            'curso_id' => $curso->getId(),
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new examen entity.
     *
     * @Route("/{id}/exprevio/new", name="ex_previo_new")
     * @Method({"GET", "POST"})
     */
    public function newPrevioAction(Request $request, Course $curso)
    {
        $examen = new Examen();
        $form = $this->createForm('Aper\CourseBundle\Form\ExamenType', $examen);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $examen->setCursoPrevia($curso);
            $em = $this->getDoctrine()->getManager();
            $em->persist($examen);
            $em->flush();

            return $this->redirectToRoute('examen_show', array('id' => $examen->getId()));
        }

        return $this->render('examen/new.html.twig', array(
            'tipo' => 'examenprevio',
            'examen' => $examen,
            'ruta' => 'examen_index_previo',
            'curso_id' => $curso->getId(),
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a examen entity.
     *
     * @Route("/{id}/showcurso/", name="examen_curso_show")
     * @Method("GET")
     */
    public function showCursoAction(Examen $examen)
    {
        if(!is_null($examen->getCursoPadreEncuesta())){
            $curso = $examen->getCursoPadreEncuesta();
            $ruta = 'examen_index_encuesta_curso';
        }
        if(!is_null($examen->getCursoPadrePrevia())){
            $curso = $examen->getCursoPadrePrevia();
            $ruta = 'examen_index_previo_curso';
        }
        if(!is_null($examen->getCursoPadreFinal())){
            $curso = $examen->getCursoPadreFinal();
            $ruta = 'examen_index_final_curso';
        }

        $deleteForm = $this->createDeleteForm($examen);

        return $this->render('examen/show_curso.html.twig', array(
            'examen' => $examen,
            'delete_form' => $deleteForm->createView(),
            'ruta' => $ruta,
            'curso_id' => $curso->getId(),
        ));
    }

    /**
     * Finds and displays a examen entity.
     *
     * @Route("/{id}/show/", name="examen_show")
     * @Method("GET")
     */
    public function showAction(Examen $examen)
    {
        if(!is_null($examen->getCursoEncuesta())){
            $curso = $examen->getCursoEncuesta();
            $ruta = 'examen_index_encuesta';
        }
        if(!is_null($examen->getCursoPrevia())){
            $curso = $examen->getCursoPrevia();
            $ruta = 'examen_index_previo';
        }
        if(!is_null($examen->getCursoFinal())){
            $curso = $examen->getCursoFinal();
            $ruta = 'examen_index_final';
        }
        $deleteForm = $this->createDeleteForm($examen);

        return $this->render('examen/show.html.twig', array(
            'examen' => $examen,
            'delete_form' => $deleteForm->createView(),
            'ruta' => $ruta,
            'curso_id' => $curso->getId(),
        ));
    }

    /**
     * Displays a form to edit an existing examen entity.
     *
     * @Route("/{id}/edit/", name="examen_curso_edit")
     * @Method({"GET", "POST"})
     */
    public function editCursoAction(Request $request, Examen $examen)
    {
        $deleteForm = $this->createDeleteForm($examen);
        $editForm = $this->createForm('Aper\CourseBundle\Form\ExamenType', $examen);
        $editForm->handleRequest($request);
        $tipo='';

        if(!is_null($examen->getCursoPadreEncuesta())){
            $curso = $examen->getCursoPadreEncuesta();
            $ruta = 'examen_index_encuesta_curso';
            $tipo='encuesta';
        }
        if(!is_null($examen->getCursoPadrePrevia())){
            $curso = $examen->getCursoPadrePrevia();
            $ruta = 'examen_index_previo_curso';
            $tipo='examenprevio';
        }
        if(!is_null($examen->getCursoPadreFinal())){
            $curso = $examen->getCursoPadreFinal();
            $ruta = 'examen_index_final_curso';
            $tipo='examenfinal';
        }

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('examen_curso_show', array('id' => $examen->getId()));
        }

        return $this->render('examen/edit_curso.html.twig', array(
            'tipo' => $tipo,
            'examen' => $examen,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'ruta' => $ruta,
            'curso_id' => $curso->getId(),
        ));
    }

    /**
     * Displays a form to edit an existing examen entity.
     *
     * @Route("/{id}/edit/", name="examen_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Examen $examen)
    {
        $deleteForm = $this->createDeleteForm($examen);
        $editForm = $this->createForm('Aper\CourseBundle\Form\ExamenType', $examen);
        $editForm->handleRequest($request);
        $tipo='';

        if(!is_null($examen->getCursoEncuesta())){
            $curso = $examen->getCursoEncuesta();
            $ruta = 'examen_index_encuesta';
            $tipo='encuesta';
        }
        if(!is_null($examen->getCursoPrevia())){
            $curso = $examen->getCursoPrevia();
            $ruta = 'examen_index_previo';
            $tipo='examenprevio';
        }
        if(!is_null($examen->getCursoFinal())){
            $curso = $examen->getCursoFinal();
            $ruta = 'examen_index_final';
            $tipo='examenfinal';
        }

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('examen_show', array('id' => $examen->getId()));
        }

        return $this->render('examen/edit.html.twig', array(
            'tipo' => $tipo,
            'examen' => $examen,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'ruta' => $ruta,
            'curso_id' => $curso->getId(),
        ));
    }

    /**
     * Deletes a examen entity.
     *
     * @Route("/{id}/delete", name="examen_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Examen $examen)
    {
        $form = $this->createDeleteForm($examen);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($examen);
            $em->flush();
        }

        return $this->redirectToRoute('examen_index');
    }

    /**
     * Creates a form to delete a examen entity.
     *
     * @param Examen $examen The examen entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Examen $examen)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('examen_delete', array('id' => $examen->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
