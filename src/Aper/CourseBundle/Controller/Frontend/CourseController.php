<?php

namespace Aper\CourseBundle\Controller\Frontend;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\NonUniqueResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Aper\CourseBundle\Entity\Category;
use Aper\TrainingKitBundle\Entity\TrainingFolder;
use Aper\CourseBundle\Entity\Course;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Course controller.
 *
 */
class CourseController extends Controller
{
    /**
     * Class CourseController
     *
     * @Route("course/index")
     */
    public function indexAction()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

        $rootFolder = $this->getDoctrine()->getRepository('TrainingKitBundle:TrainingFolder')->findAllRootNodes();

        $rol   = $this->get('security.token_storage')->getToken()->getUser()->getRoles();

        /** @var Course[] $courses */
        $courses =  $this->getDoctrine()->getRepository(Course::class)
            ->createQueryBuilderByRoles($rol);

        $folders   = [];
        $folderIds = [];
        foreach ($courses as $cours)
        {
            $folder = $cours->getTrainingFolder();

            $folderIds[$folder->getId()] = $folder->getId();
            $r     = ["_".$cours->getId() => $cours->getId()];
            $t     = $this->findRoot($folder, $r,$folderIds);
//            $keys  = array_keys($t);
//            $tRoot = array_shift($keys);
//            if(!isset($folders[$tRoot]))
//            {
//                $folders[$tRoot] = $t[$tRoot];
//            } else {
//                $folders[$tRoot] = array_merge_recursive($folders[$tRoot], $t[$tRoot]);
//            }
        }

        $coursesCollection = new ArrayCollection($courses);
        $courseIds = $coursesCollection->map(function(Course $rr) { return $rr->getId();} );
        foreach ($rootFolder as $i => $child)
        {
            if(!isset($folderIds[$child->getId()]))
            {
                unset($rootFolder[$i]);
            } else {
                $this->filterFolders($child, $folderIds, $courseIds->toArray());
            }
        }
        //dump($trainingFolder);die;

        return $this->render('AperCourseBundle:Frontend/Course:index.html.twig', compact('categories', 'rootFolder'));
    }

    private function filterFolders(TrainingFolder $trainingFolder, $folderIds, $courseIds)
    {
        foreach ($trainingFolder->getCourseCollection() as $course)
        {
            if(!in_array($course->getId(), $courseIds))
            {
                $trainingFolder->removeCourse($course);
           }
        }

        foreach ($trainingFolder->getChildren() as $child)
        {
            if(!isset($folderIds[$child->getId()]))
            {
                $trainingFolder->removeChild($child);
            } else {
                $this->filterFolders($child, $folderIds, $courseIds);
            }
        }
    }

    private function findRoot(TrainingFolder $folder, $tree, &$folderIds)
    {
        $folderIds[$folder->getId()] = $folder->getId();
        $newTree = ["_".$folder->getId() => $tree];

        if (is_null($folder->getParent()))
        {
            return $newTree;

        } else {

            return $this->findRoot($folder->getParent(), $newTree, $folderIds);
        }

    }

    /**
     * Class CourseController
     * @deprecated 2019-08-27 (Ahora se arma el arbol completo en el index. De querer ha)
     *
     *
     * @Route("course/{slug}/index", name="frontend_course_folder_index")
     * @param TrainingFolder $folder
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function folderAction(TrainingFolder $folder, Request $request)
    {
        $catQueryId    = $request->get('category');
        $folderQueryId = $folder->getId();
        $sinceQuery    = $request->get('since');
        $untilQuery    = $request->get('until');
        $categories    = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $catQuery      = $this->getDoctrine()->getRepository(Category::class)->findOneBy(array('id' => $catQueryId));
        $folderQuery   = $this->getDoctrine()->getRepository(TrainingFolder::class)->findOneBy(array('id' => $folderQueryId));
        $rol   = $this->get('security.token_storage')->getToken()->getUser()->getRoles();
        $tag   = null;
        $since = null;
        $until = null;
        $categ = null;

        if ($catQuery) {
            $categ = $catQuery->getName();
        }

        if ($sinceQuery) {
            $array = explode('/', $sinceQuery);
            $since = New \DateTime($array[2] . '-' . str_pad($array[1], 2, '0') . '-' .
                str_pad($array[0], 2, '0') . " 00:00:00");
        }

        if ($untilQuery) {
            $array = explode('/', $untilQuery);
            $until = New \DateTime($array[2] . '-' . str_pad($array[1], 2, '0') . '-' .
                str_pad($array[0], 2, '0') . " 23:59:59");
        }

        $course = $this->getDoctrine()->getRepository(Course::class)
            ->createQueryBuilderByCategoryFolderAndTagFe($catQuery, $folderQuery, $since, $until,  $rol);


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $course, $request->query->getInt('page', 1),
            9
        );

        if ($request->isXmlHttpRequest()) {
            $ajaxCourse = [];
            /** @var Course $new */
            foreach ($pagination as $new) {
                $image = '';
                if ($new->getMainImage()) {
                    $image = '//' . $request->getHost() . $request->getBaseUrl() . '/' . $new->getMainImage()->getWebPath();
                }
                $ajaxCourse[] = array(
                    'title'     => $new->getTitle(),
                    'category'  => $new->getCategory()->getName(),
                    'folder'    => $new->getTrainingFolder()->getName(),
                    'date'      => $new->getCreatedAt(),
                    'content'   => substr($new->getContent(), 0, 120),
                    'img'       => $image,
                    'pdf'       => $new->getPdf()->getWebPath(),
                    'extension' => $new->getPdf()->getExtension(),
                    'link'      => '//' . $request->getHost() . $request->getBaseUrl() . '/course/show/' . $new->getId(),
                );
            }
            $response = new  Response(json_encode($ajaxCourse));
            return $response;
        }

        return $this->render('AperCourseBundle:Frontend/Course:folder.html.twig', compact('pagination', 'categories', 'folder'));
    }

    /**
     * Finds and displays a Course entity.
     *
     * @Route("/course/show/{id}", name="frontend_course_show")
     * @Method("GET")
     * @param $id
     * @return Response
     * @throws NonUniqueResultException
     */
    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AperCourseBundle:Course');
        $rol = $this->get('security.token_storage')->getToken()->getUser()->getRoles();
        $course = $repository->findxid(null, null, $rol, 1, $id);
        if (!$course) {
            $messageException = 'Comunicación no encontrada';
            throw $this->createNotFoundException($messageException);
        }

        return $this->render('AperCourseBundle:Frontend/Course:show.html.twig', array('course' => $course));
    }
}