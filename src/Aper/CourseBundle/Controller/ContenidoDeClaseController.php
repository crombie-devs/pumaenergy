<?php

namespace Aper\CourseBundle\Controller;

use Aper\CourseBundle\Entity\Course;
use Aper\CourseBundle\Entity\CursoPadre;
use Aper\CourseBundle\Entity\Respuesta;
use Aper\CourseBundle\Entity\ContenidoDeClase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Aper\TrainingKitBundle\Entity\TrainingFolder;
use WebFactory\Bundle\GridBundle\Event\ManipulateQueryEvent;
use WebFactory\Bundle\GridBundle\Grid\Configuration;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * ContenidoDeClase controller.
 *
 * @Route("admin/course/contenido_de_clase", name="contenido_de_clase")
 */
class ContenidoDeClaseController extends Controller
{
    /**
     * Lists all contenidoDeClase entities.
     *
     * @Route("/{id}/", name="contenido_de_clase_index")
     * @Method("GET")
     */
    public function indexAction(CursoPadre $curso)
    {
        $contenidoClaseRepository = $this->getDoctrine()->getRepository(ContenidoDeClase::Class);
        $contenidos = $contenidoClaseRepository->findBy(['cursoPadre'=> $curso]);
//        if(!empty($curso->getContenidoDeClases())){
//            $contenidos = $curso->getContenidoDeClases();
//        }
//        dump()
        return $this->render('contenidoDeClase/index.html.twig', array(
            'contenidos' => $contenidos,
            'curso' =>$curso,
        ));
    }
//
//    /**
//     * Lists all contenidoDeClase entities.
//     *
//     * @Route("/{id}/", name="contenido_de_clase_index")
//     * @Method("GET")
//     */
//    public function indexAction(Course $curso)
//    {
//        $contenidos = [];
//        if(!empty($curso->getContenidoDeClases())){
//            $contenidos = $curso->getContenidoDeClases();
//        }
//        return $this->render('contenidoDeClase/index.html.twig', array(
//            'contenidos' => $contenidos,
//            'curso' =>$curso,
//        ));
//    }
//

    /**
     * Creates a new contenidoDeClase entity.
     *
     * @Route("/new/{id}/", name="contenido_de_clase_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, CursoPadre $curso)
    {
        $contenidoDeClase = new ContenidoDeClase();
        $form = $this->createForm('Aper\CourseBundle\Form\ContenidoDeClaseType', $contenidoDeClase);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//            \Doctrine\Common\Util\Debug::dump($pdf);
//            dump($form->getData());
            $pdf = $contenidoDeClase->getPdf();
            if($pdf)$pdf->setContenidoDeClase($contenidoDeClase);

            $video = $contenidoDeClase->getVideo();
            if($video)$video->setContenidoDeClase($contenidoDeClase);

            $em = $this->getDoctrine()->getManager();
            $curso->addContenidoDeClase($contenidoDeClase);
            $contenidoDeClase->setCursoPadre($curso);
            $em->persist($contenidoDeClase);
            $em->flush();

            return $this->redirectToRoute('contenido_de_clase_index', array('id' => $curso->getId()));
        }

        return $this->render('contenidoDeClase/new.html.twig', array(
            'contenidoDeClase' => $contenidoDeClase,
            'curso' => $curso,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a contenidoDeClase entity.
     *
     * @Route("/show/{id}", name="contenido_de_clase_show")
     * @Method("GET")
     */
    public function showAction(ContenidoDeClase $contenido)
    {
//        $deleteForm = $this->createDeleteForm($respuesta);

        return $this->render('contenidoDeClase/show.html.twig', array(
            'contenido' => $contenido,
//            'pregunta' => $respuesta->getPregunta(),
//            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/{id}/edit", name="contenido_de_clase_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ContenidoDeClase $contenido)
    {
        $editForm = $this->createForm('Aper\CourseBundle\Form\ContenidoDeClaseType', $contenido);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $pdf = $contenido->getPdf();
            if($pdf)$pdf->setContenidoDeClase($contenido);
            $video = $contenido->getVideo();
            if($video)$video->setContenidoDeClase($contenido);

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('contenido_de_clase_index', array('id' => $contenido->getCursoPadre()->getId()));
        }

        return $this->render('contenidoDeClase/edit.html.twig', array(
            'contenido' => $contenido,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * @Route("/{id}", name="contenido_de_clase_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ContenidoDeClase $contenidoDeClase)
    {
        $cursoId = $contenidoDeClase->getCursoPadre()->getId();
        $em = $this->getDoctrine()->getManager();
        $pdf = $contenidoDeClase->getPdf();
        $video = $contenidoDeClase->getVideo();
        if($pdf)$em->remove($pdf);
        if($video)$em->remove($video);
        $em->remove($contenidoDeClase);
        $em->flush();

        return $this->redirectToRoute('contenido_de_clase_index', array('id' => $cursoId));
    }

    /**
     * @Route("/{path}", name="contenido_de_clase_descargar_archivo")
     * @Method("GET")
     */
    public function findAction(Request $request, $path)
    {

        return $this->redirectToRoute();
    }




}
