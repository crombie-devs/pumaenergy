<?php

namespace Aper\CourseBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Aper\CourseBundle\Entity\Course;
use Aper\CourseBundle\Entity\CursoPadre;
use Aper\CourseBundle\Entity\EmployeeExamen;
use Aper\CourseBundle\Entity\EmployeePregunta;
use Aper\CourseBundle\Entity\EmployeeRespuesta;
use Aper\CourseBundle\Entity\Examen;
use Aper\CourseBundle\Entity\Pregunta;
use Aper\CourseBundle\Entity\Respuesta;

/**
 * Category controller.
 *
 * @Route("/evaluacion")
 */
class ExamenFrontendController extends Controller
{

    /**
     * Lists all Category entities.
     *
     * @Route("/evaluacionprevop/{id}/", name="frontend_evaluacion_previa")
     * @Method("GET")
     * @Template()
     */
    public function indexExamenPreviaAction(Course $curso){
        /*
        1- Ingresa evaluación
        2- Crea empleado-examen si no lo tiene. -El numero de intento marca la unidad actual del examen, no un contador
        - Si son dos intentos, va a ver un empleado-examen por cada intento -
        3- Si lo tiene, revisa cuantas oportunidades de hacer el examen tiene.
        4- Si no tiene oportunidades, se le da error.

        5- Si tiene oportunidades, se borran empleado-pregunta y se crean nuevas con el nuevo random.

        6- Selecciona Random de preguntas en la cantidad que configuraron el Examen.
        7- Se crean los empleado-pregunta para que quede ese examen para ese empleado
        8- Se enfía a la función crear Formulario
        9- Se renderiza
        10- Al tomar el resultado del examen, debe comprobar que esas respuestas correspondan al empleado-pregunta
        11- Si corresponden a empleado-pregunta, se toma el resultado del examen y se crean los empleado-respuesta donde se tomarán las respuestas validas si las haya.
        12- Si no corresponden al empleado-pregunta se le da un aviso que hable con el administrador y se le corta todas las posibilidades.
        13- Se hace la regla de 3 simples y se lo aprueba o no
        14- Se redirige a ResultadoAction que se hace la regla de 3 simples y da el porcentaje de arpobación.
        */
        if(is_null($curso->getExamenPrevio())){
            return $this->render('examen/error.html.twig', array('mensaje' => "El curso no tiene examen previo", 'volver' => 'homepage'));    
        }
        $em = $this->getDoctrine()->getManager();
        $exm_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);


        $employeeExamen = $exm_repository->getEmployeeExamen($curso, 'previo', $this->getUser()->getEmployee());

        if(sizeof($employeeExamen) > 0)
            $employeeExamen = $employeeExamen[0];
        if(!$employeeExamen){
            $employeeExamen = new EmployeeExamen();
            $employeeExamen->setCantIntentos(1);
            $employeeExamen->setFechaExamen(new \DateTime("now"));
            $employeeExamen->setEmployee($this->getUser()->getEmployee());
            $employeeExamen->setExamen($curso->getExamenPrevio());
            $em->persist($employeeExamen);
        } else {
            $resp_repository = $this->getDoctrine()->getRepository(EmployeeRespuesta::class);
            $respuestas = $resp_repository->findBy(['examen' => $employeeExamen]);

            ///////////////// CREAR UN EXAMEN NUEVO CON EL SIGUIENTE INTENTO ////////////////
            if(sizeof($respuestas) > 0){
                $intento_previo = $employeeExamen->getCantIntentos();
                $examen = $employeeExamen->getExamen();
                if($intento_previo < $examen->getCantidadIntentos()){
                    $intento_previo++;
                    $employeeExamen = new EmployeeExamen();
                    $employeeExamen->setCantIntentos($intento_previo);
                    $employeeExamen->setFechaExamen(new \DateTime("now"));
                    $employeeExamen->setEmployee($this->getUser()->getEmployee());
                    $employeeExamen->setExamen($curso->getExamenPrevio());
                    $em->persist($employeeExamen);
                } elseif ($intento_previo >= $examen->getCantidadIntentos())
                    return $this->render('examen/error.html.twig', array('mensaje' => "Usted ya ha hecho el examen previo", 'volver' => 'homepage'));
            }
        }
        $examen = $employeeExamen->getExamen();
        $preg_repository = $this->getDoctrine()->getRepository(Pregunta::class);

//        $preguntas = $preg_repository->findBy(['examen' => $examen]);
        $preguntas = $preg_repository->recuperarPreguntasValidasSegunExamen($examen);
        if(!$preguntas || sizeof($preguntas)<$examen->getCantPreg()){
            return $this->render('examen/error.html.twig', array('mensaje' => "Este examen no tiene suficientes preguntas para este examen", 'volver' => 'homepage'));
        }

        //validar respuestas validas


        ///////////////// SELECCIONAR RANDOM /////////////////
        $preguntas_k = array_rand($preguntas, $examen->getCantPreg());
        if(!is_array($preguntas_k)){
            $temp = $preguntas_k;
            $preguntas_k = [];
            $preguntas_k[] = $temp;
        }
        $preguntas_form = [];
        foreach ($preguntas_k as $value) {
            $preguntas_form[] = $preguntas[$value];
        }
        foreach ($preguntas_form as $pregunta) {
                $preg_repository = $this->getDoctrine()->getRepository(EmployeePregunta::class);
                $preg = $preg_repository->findBy(['examen' => $employeeExamen]);
                foreach ($preg as $value) {
                    $em->remove($value);
                }
                $employeePregunta = new EmployeePregunta();
                $employeePregunta->setEmployee($this->getUser()->getEmployee());
                $employeePregunta->setPregunta($pregunta);
                $employeePregunta->setExamen($employeeExamen);
                $em->persist($employeePregunta);

        }
        $em->flush();
        return $this->render('examen/examen.html.twig', array('curso' => $curso, 'preguntas' => $preguntas_form, 'tipo' => 'examen', 'examen_id' => $employeeExamen->getId()));

    }

    /**
     * Lists all Category entities.
     *
     * @Route("/evaluacionfinal/{id}/", name="frontend_evaluacion")
     * @Method("GET")
     * @Template()
     */
    public function indexExamenFinalAction(Course $curso){
        /*
        1- Ingresa evaluación
        2- Crea empleado-examen si no lo tiene. -El numero de intento marca la unidad actual del examen, no un contador
        - Si son dos intentos, va a ver un empleado-examen por cada intento -
        3- Si lo tiene, revisa cuantas oportunidades de hacer el examen tiene.
        4- Si no tiene oportunidades, se le da error.

        5- Si tiene oportunidades, se borran empleado-pregunta y se crean nuevas con el nuevo random.
        6- Selecciona Random de preguntas en la cantidad que configuraron el Examen.
        7- Se crean los empleado-pregunta para que quede ese examen para ese empleado
        8- Se enfía a la función crear Formulario
        9- Se renderiza
        */
        if(is_null($curso->getExamenFinal())){
            return $this->render('examen/error.html.twig', array('mensaje' => "El curso no tiene encuestas", 'volver' => 'homepage'));    
        }
        $em = $this->getDoctrine()->getManager();
        $exm_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);

        $employeeExamen = $exm_repository->getEmployeeExamen($curso, 'final', $this->getUser()->getEmployee());
        if(sizeof($employeeExamen) > 0)
            $employeeExamen = $employeeExamen[0];
        if(!$employeeExamen){
            $employeeExamen = new EmployeeExamen();
            $employeeExamen->setCantIntentos(1);
            $employeeExamen->setFechaExamen(new \DateTime("now"));
            $employeeExamen->setEmployee($this->getUser()->getEmployee());
            $employeeExamen->setExamen($curso->getExamenFinal());
            $em->persist($employeeExamen);
        } else {
            $resp_repository = $this->getDoctrine()->getRepository(EmployeeRespuesta::class);
            $respuestas = $resp_repository->findBy(['examen' => $employeeExamen]);

            ///////////////// CREAR UN EXAMEN NUEVO CON EL SIGUIENTE INTENTO ////////////////
            if(!empty($respuestas)){
                $intento_previo = $employeeExamen->getCantIntentos();
                $examen = $employeeExamen->getExamen();
                if($intento_previo < $examen->getCantidadIntentos()){
                    $intento_previo++;
                    $employeeExamen = new EmployeeExamen();
                    $employeeExamen->setCantIntentos($intento_previo);
                    $employeeExamen->setFechaExamen(new \DateTime("now"));
                    $employeeExamen->setEmployee($this->getUser()->getEmployee());
                    $employeeExamen->setExamen($curso->getExamenFinal());
                    $em->persist($employeeExamen);
                } elseif ($intento_previo >= $examen->getCantidadIntentos())
                    return $this->render('examen/error.html.twig', array('mensaje' => "Usted ya ha hecho el examen previo", 'volver' => 'homepage'));
            }
        }
        $examen = $employeeExamen->getExamen();
        $preg_repository = $this->getDoctrine()->getRepository(Pregunta::class);
//        $preguntas = $preg_repository->findBy(['examen' => $examen]);
        $preguntas = $preg_repository->recuperarPreguntasValidasSegunExamen($examen);
        if(!$preguntas || sizeof($preguntas)<$examen->getCantPreg()){
            return $this->render('examen/error.html.twig', array('mensaje' => "Este examen no tiene suficientes preguntas para este examen", 'volver' => 'homepage'));
        }

        //validar respuestas validas


        ///////////////// SELECCIONAR RANDOM /////////////////
        $preguntas_k = array_rand($preguntas, $examen->getCantPreg());
        if(!is_array($preguntas_k)){
            $temp = $preguntas_k;
            $preguntas_k = [];
            $preguntas_k[] = $temp;
        }
        $preguntas_form = [];
        foreach ($preguntas_k as $value) {
            $preguntas_form[] = $preguntas[$value];
        }
        foreach ($preguntas_form as $pregunta) {
                $preg_repository = $this->getDoctrine()->getRepository(EmployeePregunta::class);
                $preg = $preg_repository->findBy(['examen' => $employeeExamen]);
                foreach ($preg as $value) {
                    $em->remove($value);
                }
                $employeePregunta = new EmployeePregunta();
                $employeePregunta->setEmployee($this->getUser()->getEmployee());
                $employeePregunta->setPregunta($pregunta);
                $employeePregunta->setExamen($employeeExamen);
                $em->persist($employeePregunta);

        }
        $em->flush();
        return $this->render('examen/examen.html.twig', array('curso' => $curso, 'preguntas' => $preguntas_form, 'tipo' => 'examen', 'examen_id' => $employeeExamen->getId()));

    }


    /**
     * Lists all Category entities.
     *
     * @Route("/encuesta/{id}/", name="frontend_encuesta")
     * @Method("GET")
     * @Template()
     */
    public function indexEncuestaAction(Course $curso){
        // La idea de que Employee esté en cada entidad relacionada al examen es por
        // posibles estadísticas
        /*
        1- Ingresa en encuesta
        3- Envía todas las preguntas que haya a renderizar el formulario.
        4- Renderiza los formularios (No debe dejarse no contestar)
        5- En otro action toma las respuestas y genera el empleado-respuesta
        $employeeExamen = $this->getEmployeeExamen($curso);
        if(empty($employeeExamen)){
            $employeeExamen = new EmployeeExamen();
        }
        */
        if(is_null($curso->getEncuesta())){
            return $this->render('examen/error.html.twig', array('mensaje' => "El curso no tiene encuestas", 'volver' => 'homepage'));    
        }
        $em = $this->getDoctrine()->getManager();
        $exm_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);

        $employeeExamen = $exm_repository->getEmployeeExamen($curso, 'encuesta', $this->getUser()->getEmployee());
        if(sizeof($employeeExamen) > 0)
            $employeeExamen = $employeeExamen[0];
        if(!$employeeExamen){
            $employeeExamen = new EmployeeExamen();
            $employeeExamen->setCantIntentos(1);
            $employeeExamen->setFechaExamen(new \DateTime("now"));
            $employeeExamen->setEmployee($this->getUser()->getEmployee());
            $employeeExamen->setExamen($curso->getEncuesta());
            $em->persist($employeeExamen);
        } else {
            $resp_repository = $this->getDoctrine()->getRepository(EmployeeRespuesta::class);
            $respuestas = $resp_repository->findBy(['examen' => $employeeExamen]);
            if(sizeof($respuestas) > 0)
                return $this->render('examen/error.html.twig', array('mensaje' => "Usted ya ha llenado la encuesta", 'volver' => 'homepage'));
        }
        $preg_repository = $this->getDoctrine()->getRepository(EmployeePregunta::class);
        $preguntas = $preg_repository->findBy(['examen' => $employeeExamen]);
        if(sizeof($preguntas) == 0){
            foreach ($curso->getEncuesta()->getPreguntas() as $value) {
                $employeePregunta = new EmployeePregunta();
                $employeePregunta->setEmployee($this->getUser()->getEmployee());
                $employeePregunta->setPregunta($value);
                $employeePregunta->setExamen($employeeExamen);
                $em->persist($employeePregunta);
            }
        }
        $em->flush();
        return $this->render('examen/encuesta.html.twig', array('curso' => $curso, 'tipo' => 'encuesta', 'examen_id' => $employeeExamen->getId()));
    }

    /**
     * Lists all Category entities.
     *
     * @Route("/setencuesta/{id}/{examen_id}/", name="frontend_encuesta_set")
     * @Method("POST")
     * @Template()
     */
    public function setEncuestaAction(Request $request, Course $curso, $examen_id){
        if(!preg_match('/^[0-9]$/', $examen_id)){
            return $this->render('examen/error.html.twig', array('mensaje' => "Intento de SQL Injection", 'volver' => 'homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $exm_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);
        $employeeExamen = $exm_repository->find($examen_id);

        $preg_emp_repository = $this->getDoctrine()->getRepository(EmployeePregunta::class);
        $employeePreguntas = $preg_emp_repository->findBy(['examen' => $employeeExamen]);
        $ids_preguntas = [];
        foreach ($employeePreguntas as $value) {
            $pregunta = $value->getPregunta();
            $ids_preguntas[] = $pregunta->getId();
        }

        $resp_emp_repository = $this->getDoctrine()->getRepository(EmployeeRespuesta::class);
        $employeeRespuestas = $resp_emp_repository->findBy(['examen' => $employeeExamen]);

        $params = $request->request->all();
        // Viene y la cantidad de respuestas es mayor a la cantidad de preguntas, vuelve al formulario
        // -1 por el submit que también viene.
        if(sizeof($params)-1 > sizeof($employeePreguntas)){
            return $this->redirectToRoute('frontend_encuesta', array('id' => $curso->getId()));
        }

        // Viene, si existe EmployeeExamen y tiene las respuestas, chau
        if(sizeof($employeeRespuestas) > 0){
            return $this->render('examen/error.html.twig', array('mensaje' => "Usted ya ha llenado la encuesta", 'volver' => 'homepage'));
        }

        // Viene y la cantidad de respuestas es menor a la cantidad de preguntas, se queda
        // Viene, si existe EmployeeExamen y no tiene las respuestas, se queda

        // Revisa los ids de las respuestas y ve los ids de las preguntas, si éstos coinciden con las
        // preguntas guardadas, las respuestas son válidas. Si alguno falla, se tendrá el examen sin hacer y
        // vuelve al formulario.
        $resp_repository = $this->getDoctrine()->getRepository(Respuesta::class);
        $esta = [];
        $ids_preguntas_resp = [];

        // A este lo voy a usar para generar los EmployeeRespuesta
        $respuestas = [];
        foreach ($params as $resp) {
            if(is_numeric($resp)){
                $respuesta = $resp_repository->find($resp);
                $ids_preguntas_resp[] = $respuesta->getPregunta()->getId();
                $respuestas[] = $respuesta;
            }
        }
        $diff = array_diff($ids_preguntas, $ids_preguntas_resp);
        $no = false;

        // Si tenes diferencias y los dos arrays tienen el mismo largo, obvio que metieron una pregunta.
        if(sizeof($diff) > 0){
            if(sizeof($ids_preguntas) == sizeof($ids_preguntas_resp))
                $no = true;

            // Si ids de respuestas contestadas en cantidad menor a preguntas no estan en id de preguntas
            // Entonces chau.
            foreach($ids_preguntas_resp as $d){
                if(!in_array($d, $ids_preguntas))
                    $no = true;
            }
            if($no){
                return $this->redirectToRoute('frontend_encuesta', array('id' => $curso->getId()));
            }
        }
        // \Doctrine\Common\Util\Debug::dump($respuestas);
        //die(sizeof($respuestas));
        foreach ($respuestas as $respuesta) {
            $employeeRespuesta = new EmployeeRespuesta();
            $employeeRespuesta->setRespuesta($respuesta);
            $employeeRespuesta->setVerdadera($respuesta->getCorrecta());
            $employeeRespuesta->setEmployee($employeeExamen->getEmployee());
            $employeeRespuesta->setExamen($employeeExamen);
            $em->persist($employeeRespuesta);
        }
        $em->flush();
        return $this->redirectToRoute('aper_course_frontend_course_index');
    }

    /**
     * Lists all Category entities.
     *
     * @Route("/setexamen/{id}/{examen_id}/", name="frontend_examen_set")
     * @Method("POST")
     * @Template()
     */
    public function setExamenAction(Request $request, Course $curso, $examen_id){
    /*
        10- Al tomar el resultado del examen, debe comprobar que esas respuestas correspondan al empleado-pregunta
        11- Si corresponden a empleado-pregunta, se toma el resultado del examen y se crean los empleado-respuesta donde se tomarán las respuestas validas si las haya.
        12- Si no corresponden al empleado-pregunta se le da un aviso que hable con el administrador y se le corta todas las posibilidades.
        13- Se hace la regla de 3 simples y se lo aprueba o no
        14- Se redirige a ResultadoAction que se hace la regla de 3 simples y da el porcentaje de arpobación.
    */
        if(!preg_match('/^[0-9]$/', $examen_id)){
            return $this->render('examen/error.html.twig', array('mensaje' => "Intento de SQL Injection", 'volver' => 'homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $exm_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);
        $employeeExamen = $exm_repository->find($examen_id);

        $preg_emp_repository = $this->getDoctrine()->getRepository(EmployeePregunta::class);
        $employeePreguntas = $preg_emp_repository->findBy(['examen' => $employeeExamen]);
        $ids_preguntas = [];
        foreach ($employeePreguntas as $value) {
            $pregunta = $value->getPregunta();
            $ids_preguntas[] = $pregunta->getId();
        }

        $resp_emp_repository = $this->getDoctrine()->getRepository(EmployeeRespuesta::class);
        $employeeRespuestas = $resp_emp_repository->findBy(['examen' => $employeeExamen]);

        $params = $request->request->all();
        $examen = $employeeExamen->getExamen();
        $ruta = '';
        if($curso->getExamenFinal())
            if($curso->getExamenFinal()->getId() == $examen->getId())
                $ruta = "frontend_evaluacion";
        if($curso->getExamenPrevio())
            if($curso->getExamenPrevio()->getId() == $examen->getId())
                $ruta = "frontend_evaluacion_previa";

        // Viene y la cantidad de respuestas es mayor a la cantidad de preguntas, vuelve al formulario
        if(sizeof($params)-1 != sizeof($employeePreguntas)){
            return $this->redirectToRoute($ruta, array('id' => $curso->getId()));
        }

        // Viene, si existe EmployeeExamen y tiene las respuestas, chau
        if(sizeof($employeeRespuestas) > 0){
            return $this->render('examen/error.html.twig', array('mensaje' => "Usted ya ha llenado la encuesta", 'volver' => 'homepage'));
        }
        $resp_repository = $this->getDoctrine()->getRepository(Respuesta::class);
        $respuestas = [];
        foreach ($params as $resp) {
            if(is_numeric($resp)){
                $respuesta = $resp_repository->find($resp);
                $ids_preguntas_resp[] = $respuesta->getPregunta()->getId();
                $respuestas[] = $respuesta;
            }
        }
        $diff = array_diff($ids_preguntas, $ids_preguntas_resp);
        if(!empty($diff)){
/////////////////////////////////////////////// Ver que se hace.
        }
        $cant_correctas = 0;
        foreach ($respuestas as $respuesta) {
            $employeeRespuesta = new EmployeeRespuesta();
            $employeeRespuesta->setRespuesta($respuesta);
            $employeeRespuesta->setVerdadera($respuesta->getCorrecta());
            $employeeRespuesta->setEmployee($employeeExamen->getEmployee());
            $employeeRespuesta->setExamen($employeeExamen);
            $em->persist($employeeRespuesta);
            if($respuesta->getCorrecta())
                $cant_correctas++;
        }
        $porcentaje = ($cant_correctas * 100) / $examen->getCantPreg();
        if($porcentaje >= $examen->getPorcentajeAprobacion())
        {
            $employeeExamen->setAprobado(true);
        } else {
            $employeeExamen->setAprobado(false);
        }
        $employeeExamen->setPorcentaje($porcentaje);
        $em->flush();

        return $this->redirectToRoute('clase_front', array('id' => $curso->getId()));
    }


    /**
     *
     * @Route("/evaluacioncursoeprevop/{id}/", name="frontend_evaluacion_padre_previa")
     * @Method("GET")
     * @Template()
     */
    public function indexExamenPreviaCursoAction(CursoPadre $curso){


        if(is_null($curso->getExamenPrevio())){
            return $this->render('examen/error.html.twig', array('mensaje' => "El curso no tiene examen previo", 'volver' => 'homepage'));    
        }
        $em = $this->getDoctrine()->getManager();
        $exm_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);

        // Curso entra por que pone curso->getExamen() para obtener el id
        $employeeExamen = $exm_repository->getEmployeeExamen($curso, 'previo', $this->getUser()->getEmployee());
        if(sizeof($employeeExamen) > 0)
            $employeeExamen = $employeeExamen[0];
        if(!$employeeExamen){
            $employeeExamen = new EmployeeExamen();
            $employeeExamen->setCantIntentos(1);
            $employeeExamen->setFechaExamen(new \DateTime("now"));
            $employeeExamen->setEmployee($this->getUser()->getEmployee());
            $employeeExamen->setExamen($curso->getExamenPrevio());
            $em->persist($employeeExamen);
        } else {
            $resp_repository = $this->getDoctrine()->getRepository(EmployeeRespuesta::class);
            $respuestas = $resp_repository->findBy(['examen' => $employeeExamen]);

            ///////////////// CREAR UN EXAMEN NUEVO CON EL SIGUIENTE INTENTO ////////////////
            if(sizeof($respuestas) > 0){
                $intento_previo = $employeeExamen->getCantIntentos();
                $examen = $employeeExamen->getExamen();
                if($intento_previo < $examen->getCantidadIntentos()){
                    $intento_previo++;
                    $employeeExamen = new EmployeeExamen();
                    $employeeExamen->setCantIntentos($intento_previo);
                    $employeeExamen->setFechaExamen(new \DateTime("now"));
                    $employeeExamen->setEmployee($this->getUser()->getEmployee());
                    $employeeExamen->setExamen($curso->getExamenPrevio());
                    $em->persist($employeeExamen);
                } elseif ($intento_previo >= $examen->getCantidadIntentos())
                    return $this->render('examen/error.html.twig', array('mensaje' => "Usted ya ha hecho el examen previo", 'volver' => 'homepage'));
            }
        }
        $examen = $employeeExamen->getExamen();
        $preg_repository = $this->getDoctrine()->getRepository(Pregunta::class);
//        $preguntas = $preg_repository->findBy(['examen' => $examen]);
        $preguntas = $preg_repository->recuperarPreguntasValidasSegunExamen($examen);
        if(!$preguntas || sizeof($preguntas)<$examen->getCantPreg()){
            return $this->render('examen/error.html.twig', array('mensaje' => "Este examen no tiene suficientes preguntas para este examen", 'volver' => 'homepage'));
        }

        //validar respuestas validas

        ///////////////// SELECCIONAR RANDOM /////////////////
        $preguntas_k = array_rand($preguntas, $examen->getCantPreg());
        if(!is_array($preguntas_k)){
            $temp = $preguntas_k;
            $preguntas_k = [];
            $preguntas_k[] = $temp;
        }
        $preguntas_form = [];
        foreach ($preguntas_k as $value) {
            $preguntas_form[] = $preguntas[$value];
        }
        foreach ($preguntas_form as $pregunta) {
                $preg_repository = $this->getDoctrine()->getRepository(EmployeePregunta::class);
                $preg = $preg_repository->findBy(['examen' => $employeeExamen]);
                foreach ($preg as $value) {
                    $em->remove($value);
                }
                $employeePregunta = new EmployeePregunta();
                $employeePregunta->setEmployee($this->getUser()->getEmployee());
                $employeePregunta->setPregunta($pregunta);
                $employeePregunta->setExamen($employeeExamen);
                $em->persist($employeePregunta);

        }
        $em->flush();
        return $this->render('examen/examen.html.twig', array('curso' => $curso, 'preguntas' => $preguntas_form, 'tipo' => 'examen', 'examen_id' => $employeeExamen->getId()));

    }

    /**
     * Lists all Category entities.
     *
     * @Route("/evaluacionfinalcurso/{id}/", name="frontend_evaluacion_padre")
     * @Method("GET")
     * @Template()
     */
    public function indexExamenFinalCursoAction(CursoPadre $curso){
        if(is_null($curso->getExamenFinal())){
            return $this->render('examen/error.html.twig', array('mensaje' => "El curso no tiene encuestas", 'volver' => 'homepage'));    
        }
        $em = $this->getDoctrine()->getManager();
        $exm_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);

        $employeeExamen = $exm_repository->getEmployeeExamen($curso, 'final', $this->getUser()->getEmployee());
        if(sizeof($employeeExamen) > 0)
            $employeeExamen = $employeeExamen[0];
        if(!$employeeExamen){
            $employeeExamen = new EmployeeExamen();
            $employeeExamen->setCantIntentos(1);
            $employeeExamen->setFechaExamen(new \DateTime("now"));
            $employeeExamen->setEmployee($this->getUser()->getEmployee());
            $employeeExamen->setExamen($curso->getExamenFinal());
            $em->persist($employeeExamen);
        } else {
            $resp_repository = $this->getDoctrine()->getRepository(EmployeeRespuesta::class);
            $respuestas = $resp_repository->findBy(['examen' => $employeeExamen]);

            ///////////////// CREAR UN EXAMEN NUEVO CON EL SIGUIENTE INTENTO ////////////////
            if(!empty($respuestas)){
                $intento_previo = $employeeExamen->getCantIntentos();
                $examen = $employeeExamen->getExamen();
                if($intento_previo < $examen->getCantidadIntentos()){
                    $intento_previo++;
                    $employeeExamen = new EmployeeExamen();
                    $employeeExamen->setCantIntentos($intento_previo);
                    $employeeExamen->setFechaExamen(new \DateTime("now"));
                    $employeeExamen->setEmployee($this->getUser()->getEmployee());
                    $employeeExamen->setExamen($curso->getExamenFinal());
                    $em->persist($employeeExamen);
                } elseif ($intento_previo >= $examen->getCantidadIntentos())
                    return $this->render('examen/error.html.twig', array('mensaje' => "Usted ya ha hecho el examen previo", 'volver' => 'homepage'));
            }
        }
        $examen = $employeeExamen->getExamen();
        $preg_repository = $this->getDoctrine()->getRepository(Pregunta::class);
//        $preguntas = $preg_repository->findBy(['examen' => $examen]);
        $preguntas = $preg_repository->recuperarPreguntasValidasSegunExamen($examen);
        if(!$preguntas || sizeof($preguntas)<$examen->getCantPreg()){
            return $this->render('examen/error.html.twig', array('mensaje' => "Este examen no tiene suficientes preguntas para este examen", 'volver' => 'homepage'));
        }

        //validar respuestas validas

        ///////////////// SELECCIONAR RANDOM /////////////////
        $preguntas_k = array_rand($preguntas, $examen->getCantPreg());
        if(!is_array($preguntas_k)){
            $temp = $preguntas_k;
            $preguntas_k = [];
            $preguntas_k[] = $temp;
        }
        $preguntas_form = [];
        foreach ($preguntas_k as $value) {
            $preguntas_form[] = $preguntas[$value];
        }
        foreach ($preguntas_form as $pregunta) {
                $preg_repository = $this->getDoctrine()->getRepository(EmployeePregunta::class);
                $preg = $preg_repository->findBy(['examen' => $employeeExamen]);
                foreach ($preg as $value) {
                    $em->remove($value);
                }
                $employeePregunta = new EmployeePregunta();
                $employeePregunta->setEmployee($this->getUser()->getEmployee());
                $employeePregunta->setPregunta($pregunta);
                $employeePregunta->setExamen($employeeExamen);
                $em->persist($employeePregunta);

        }
        $em->flush();
        return $this->render('examen/examencurso.html.twig', array('curso' => $curso, 'preguntas' => $preguntas_form, 'tipo' => 'examen', 'examen_id' => $employeeExamen->getId()));

    }

    /**
     * Lists all Category entities.
     *
     * @Route("/encuestacurso/{id}/", name="frontend_encuesta_curso")
     * @Method("GET")
     * @Template()
     */
    public function indexEncuestaCursoAction(CursoPadre $curso){
        if(is_null($curso->getEncuesta())){
            return $this->render('examen/error.html.twig', array('mensaje' => "El curso no tiene encuestas", 'volver' => 'homepage'));    
        }
        $em = $this->getDoctrine()->getManager();
        $exm_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);

        $employeeExamen = $exm_repository->getEmployeeExamen($curso, 'encuesta', $this->getUser()->getEmployee());
        if(sizeof($employeeExamen) > 0)
            $employeeExamen = $employeeExamen[0];
        if(!$employeeExamen){
            $employeeExamen = new EmployeeExamen();
            $employeeExamen->setCantIntentos(1);
            $employeeExamen->setFechaExamen(new \DateTime("now"));
            $employeeExamen->setEmployee($this->getUser()->getEmployee());
            $employeeExamen->setExamen($curso->getEncuesta());
            $em->persist($employeeExamen);
        } else {
            $resp_repository = $this->getDoctrine()->getRepository(EmployeeRespuesta::class);
            $respuestas = $resp_repository->findBy(['examen' => $employeeExamen]);
            if(sizeof($respuestas) > 0)
                return $this->render('examen/error.html.twig', array('mensaje' => "Usted ya ha llenado la encuesta", 'volver' => 'homepage'));
        }
        $preg_repository = $this->getDoctrine()->getRepository(EmployeePregunta::class);
        $preguntas = $preg_repository->findBy(['examen' => $employeeExamen]);
        if(sizeof($preguntas) == 0){
            foreach ($curso->getEncuesta()->getPreguntas() as $value) {
                $employeePregunta = new EmployeePregunta();
                $employeePregunta->setEmployee($this->getUser()->getEmployee());
                $employeePregunta->setPregunta($value);
                $employeePregunta->setExamen($employeeExamen);
                $em->persist($employeePregunta);
            }
        }
        $em->flush();
        return $this->render('examen/encuestacurso.html.twig', array('curso' => $curso, 'tipo' => 'encuesta', 'examen_id' => $employeeExamen->getId()));
    }

    /**
     * Lists all Category entities.
     *
     * @Route("/setencuestacurso/{id}/{examen_id}/", name="frontend_encuesta_curso_set")
     * @Method("POST")
     * @Template()
     */
    public function setEncuestaCursoAction(Request $request, CursoPadre $curso, $examen_id){
        if(!preg_match('/^[0-9]$/', $examen_id)){
            return $this->render('examen/error.html.twig', array('mensaje' => "Intento de SQL Injection", 'volver' => 'homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $exm_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);
        $employeeExamen = $exm_repository->find($examen_id);

        $preg_emp_repository = $this->getDoctrine()->getRepository(EmployeePregunta::class);
        $employeePreguntas = $preg_emp_repository->findBy(['examen' => $employeeExamen]);
        $ids_preguntas = [];
        foreach ($employeePreguntas as $value) {
            $pregunta = $value->getPregunta();
            $ids_preguntas[] = $pregunta->getId();
        }

        $resp_emp_repository = $this->getDoctrine()->getRepository(EmployeeRespuesta::class);
        $employeeRespuestas = $resp_emp_repository->findBy(['examen' => $employeeExamen]);

        $params = $request->request->all();
        // Viene y la cantidad de respuestas es mayor a la cantidad de preguntas, vuelve al formulario
        // -1 por el submit que también viene.
        if(sizeof($params)-1 > sizeof($employeePreguntas)){
            return $this->redirectToRoute('frontend_encuesta', array('id' => $curso->getId()));
        }

        // Viene, si existe EmployeeExamen y tiene las respuestas, chau
        if(sizeof($employeeRespuestas) > 0){
            return $this->render('examen/error.html.twig', array('mensaje' => "Usted ya ha llenado la encuesta", 'volver' => 'homepage'));
        }

        // Viene y la cantidad de respuestas es menor a la cantidad de preguntas, se queda
        // Viene, si existe EmployeeExamen y no tiene las respuestas, se queda

        // Revisa los ids de las respuestas y ve los ids de las preguntas, si éstos coinciden con las
        // preguntas guardadas, las respuestas son válidas. Si alguno falla, se tendrá el examen sin hacer y
        // vuelve al formulario.
        $resp_repository = $this->getDoctrine()->getRepository(Respuesta::class);
        $esta = [];
        $ids_preguntas_resp = [];

        // A este lo voy a usar para generar los EmployeeRespuesta
        $respuestas = [];
        foreach ($params as $resp) {
            if(is_numeric($resp)){
                $respuesta = $resp_repository->find($resp);
                $ids_preguntas_resp[] = $respuesta->getPregunta()->getId();
                $respuestas[] = $respuesta;
            }
        }
        $diff = array_diff($ids_preguntas, $ids_preguntas_resp);
        $no = false;

        // Si tenes diferencias y los dos arrays tienen el mismo largo, obvio que metieron una pregunta.
        if(sizeof($diff) > 0){
            if(sizeof($ids_preguntas) == sizeof($ids_preguntas_resp))
                $no = true;

            // Si ids de respuestas contestadas en cantidad menor a preguntas no estan en id de preguntas
            // Entonces chau.
            foreach($ids_preguntas_resp as $d){
                if(!in_array($d, $ids_preguntas))
                    $no = true;
            }
            if($no){
                return $this->redirectToRoute('frontend_encuesta', array('id' => $curso->getId()));
            }
        }
        // \Doctrine\Common\Util\Debug::dump($respuestas);
        //die(sizeof($respuestas));
        foreach ($respuestas as $respuesta) {
            $employeeRespuesta = new EmployeeRespuesta();
            $employeeRespuesta->setRespuesta($respuesta);
            $employeeRespuesta->setVerdadera($respuesta->getCorrecta());
            $employeeRespuesta->setEmployee($employeeExamen->getEmployee());
            $employeeRespuesta->setExamen($employeeExamen);
            $em->persist($employeeRespuesta);
        }
        $em->flush();
        return $this->redirectToRoute('curso_clase_front', array('curso_padre_id' => $curso->getId()));
    }

    /**
     * Lists all Category entities.
     *
     * @Route("/setexamencurso/{id}/{examen_id}/", name="frontend_examen_set_curso")
     * @Method("POST")
     * @Template()
     */
    public function setExamenCursoAction(Request $request, CursoPadre $curso, $examen_id){

        if(!preg_match('/^[0-9]$/', $examen_id)){
            return $this->render('examen/error.html.twig', array('mensaje' => "Intento de SQL Injection", 'volver' => 'homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $exm_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);
        $employeeExamen = $exm_repository->find($examen_id);

        $preg_emp_repository = $this->getDoctrine()->getRepository(EmployeePregunta::class);
        $employeePreguntas = $preg_emp_repository->findBy(['examen' => $employeeExamen]);
        $ids_preguntas = [];
        foreach ($employeePreguntas as $value) {
            $pregunta = $value->getPregunta();
            $ids_preguntas[] = $pregunta->getId();
        }

        $resp_emp_repository = $this->getDoctrine()->getRepository(EmployeeRespuesta::class);
        $employeeRespuestas = $resp_emp_repository->findBy(['examen' => $employeeExamen]);

        $params = $request->request->all();
        $examen = $employeeExamen->getExamen();
        $ruta = '';
        if($curso->getExamenFinal())
            if($curso->getExamenFinal()->getId() == $examen->getId())
                $ruta = "frontend_evaluacion_padre";
        if($curso->getExamenPrevio())
            if($curso->getExamenPrevio()->getId() == $examen->getId())
                $ruta = "frontend_evaluacion_padre_previa";

        // Viene y la cantidad de respuestas es mayor a la cantidad de preguntas, vuelve al formulario
        if(sizeof($params)-1 != sizeof($employeePreguntas)){
            return $this->redirectToRoute($ruta, array('id' => $curso->getId()));
        }

        // Viene, si existe EmployeeExamen y tiene las respuestas, chau
        if(sizeof($employeeRespuestas) > 0){
            return $this->render('examen/error.html.twig', array('mensaje' => "Usted ya ha llenado la encuesta", 'volver' => 'homepage'));
        }
        $resp_repository = $this->getDoctrine()->getRepository(Respuesta::class);
        $respuestas = [];
        foreach ($params as $resp) {
            if(is_numeric($resp)){
                $respuesta = $resp_repository->find($resp);
                $ids_preguntas_resp[] = $respuesta->getPregunta()->getId();
                $respuestas[] = $respuesta;
            }
        }
        $diff = array_diff($ids_preguntas, $ids_preguntas_resp);
        if(!empty($diff)){
/////////////////////////////////////////////// Ver que se hace.
        }
        $cant_correctas = 0;
        foreach ($respuestas as $respuesta) {
            $employeeRespuesta = new EmployeeRespuesta();
            $employeeRespuesta->setRespuesta($respuesta);
            $employeeRespuesta->setVerdadera($respuesta->getCorrecta());
            $employeeRespuesta->setEmployee($employeeExamen->getEmployee());
            $employeeRespuesta->setExamen($employeeExamen);
            $em->persist($employeeRespuesta);
            if($respuesta->getCorrecta())
                $cant_correctas++;
        }
        $porcentaje = ($cant_correctas * 100) / $examen->getCantPreg();
        if($porcentaje >= $examen->getPorcentajeAprobacion())
        {
            $employeeExamen->setAprobado(true);
        } else {
            $employeeExamen->setAprobado(false);
        }
        $employeeExamen->setPorcentaje($porcentaje);
        $em->flush();

        return $this->redirectToRoute('curso_clase_front', array('curso_padre_id' => $curso->getId()));
    }


}
