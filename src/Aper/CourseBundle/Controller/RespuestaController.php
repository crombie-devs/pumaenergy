<?php

namespace Aper\CourseBundle\Controller;

use Aper\CourseBundle\Entity\Respuesta;
use Aper\CourseBundle\Entity\Pregunta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Respuesta controller.
 *
 * @Route("admin/course/exame/pregunta/respuesta")
 */
class RespuestaController extends Controller
{
    /**
     * Lists all respuesta entities.
     *
     * @Route("/{id}/", name="respuesta_index")
     * @Method("GET")
     */
    public function indexAction(Pregunta $pregunta)
    {
        $respuestas = [];
        if(!empty($pregunta->getRespuestas())){
            $respuestas = $pregunta->getRespuestas();
        }
        return $this->render('respuesta/index.html.twig', array(
            'respuestas' => $respuestas,
            'pregunta' =>$pregunta,
            'examen_id' => $pregunta->getExamen()->getId(),
        ));
    }

    /**
     * Creates a new respuesta entity.
     *
     * @Route("/new/{id}/", name="respuesta_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Pregunta $pregunta)
    {
        $respuesta = new Respuesta();
        $form = $this->createForm('Aper\CourseBundle\Form\RespuestaType', $respuesta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if($respuesta->getCorrecta()){
//                $pregunta=$respuesta->getPregunta();
                $numAux = $pregunta->getTieneRespuestasCorrecta();
                $pregunta->setTieneRespuestasCorrecta($numAux+1);
            }
            $pregunta->addRespuestas($respuesta);
            $respuesta->setPregunta($pregunta);
            $em->persist($respuesta);
            $em->flush();

            return $this->redirectToRoute('respuesta_index', array('id' => $pregunta->getId()));
        }

        return $this->render('respuesta/new.html.twig', array(
            'respuesta' => $respuesta,
            'pregunta' => $pregunta,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a respuesta entity.
     *
     * @Route("/show/{id}", name="respuesta_show")
     * @Method("GET")
     */
    public function showAction(Respuesta $respuesta)
    {
        $deleteForm = $this->createDeleteForm($respuesta);

        return $this->render('respuesta/show.html.twig', array(
            'respuesta' => $respuesta,
            'pregunta' => $respuesta->getPregunta(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing respuesta entity.
     *
     * @Route("/{id}/edit", name="respuesta_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Respuesta $respuesta)
    {
        $eraCorrecta = $respuesta->getCorrecta();
        $deleteForm = $this->createDeleteForm($respuesta);
        $editForm = $this->createForm('Aper\CourseBundle\Form\RespuestaType', $respuesta);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()){
            if($respuesta->getCorrecta() && !$eraCorrecta){
                $pregunta=$respuesta->getPregunta();
                $numAux = $pregunta->getTieneRespuestasCorrecta();
                $pregunta->setTieneRespuestasCorrecta($numAux+1);
            }
            if(!($respuesta->getCorrecta()) && $eraCorrecta){
                $pregunta=$respuesta->getPregunta();
                $numAux = $pregunta->getTieneRespuestasCorrecta();
                $pregunta->setTieneRespuestasCorrecta($numAux-1);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('respuesta_index', array('id' => $respuesta->getPregunta()->getId()));
        }

        return $this->render('respuesta/edit.html.twig', array(
            'respuesta' => $respuesta,
            'pregunta' => $respuesta->getPregunta(),
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a respuesta entity.
     *
     * @Route("/{id}/delete", name="respuesta_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, Respuesta $respuesta)
    {
        $form = $this->createDeleteForm($respuesta);
        $form->handleRequest($request);
//        if ($form->isSubmitted() && $form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $pregunta=$respuesta->getPregunta();

        if($respuesta->getCorrecta()){
            $numAux = $pregunta->getTieneRespuestasCorrecta();
            $pregunta->setTieneRespuestasCorrecta($numAux-1);
        }
        $pregunta->removeRespuesta($respuesta);
        $em->remove($respuesta);
        $em->flush();
//        }

        return $this->redirectToRoute('respuesta_index', array('id' => $pregunta->getId()));
    }

    /**
     * Creates a form to delete a respuesta entity.
     *
     * @param Respuesta $respuesta The respuesta entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Respuesta $respuesta)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('respuesta_delete', array('id' => $respuesta->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

}
