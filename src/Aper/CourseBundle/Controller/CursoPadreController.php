<?php

namespace Aper\CourseBundle\Controller;

use Aper\CourseBundle\Entity\CursoPadre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Cursopadre controller.
 *
 * @Route("/admin/cursopadre")
 */
class CursoPadreController extends Controller
{
    /**
     * Lists all cursoPadre entities.
     *
     * @Route("/index", name="cursopadre_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $cursoPadres = $em->getRepository('AperCourseBundle:CursoPadre')->findAll();

        return $this->render('cursopadre/index.html.twig', array(
            'cursoPadres' => $cursoPadres,
        ));
    }

    /**
     * Creates a new cursoPadre entity.
     *
     * @Route("/new", name="cursopadre_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $cursoPadre = new Cursopadre();
        $form = $this->createForm('Aper\CourseBundle\Form\CursoPadreType', $cursoPadre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imagen = $cursoPadre->getImagen();
            if($imagen)$imagen->setCursoPadre($cursoPadre);
            $em = $this->getDoctrine()->getManager();
            $em->persist($cursoPadre);
            $em->flush();

            return $this->redirectToRoute('cursopadre_show', array('id' => $cursoPadre->getId()));
        }

        return $this->render('cursopadre/new.html.twig', array(
            'cursoPadre' => $cursoPadre,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a cursoPadre entity.
     *
     * @Route("/{id}", name="cursopadre_show")
     * @Method("GET")
     */
    public function showAction(CursoPadre $cursoPadre)
    {
        $deleteForm = $this->createDeleteForm($cursoPadre);

        return $this->render('cursopadre/show.html.twig', array(
            'cursoPadre' => $cursoPadre,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing cursoPadre entity.
     *
     * @Route("/{id}/edit", name="cursopadre_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CursoPadre $cursoPadre)
    {
        $deleteForm = $this->createDeleteForm($cursoPadre);
        $editForm = $this->createForm('Aper\CourseBundle\Form\CursoPadreType', $cursoPadre);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $imagen = $cursoPadre->getImagen();
            if($imagen)$imagen->setCursoPadre($cursoPadre);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cursopadre_index');
        }

        return $this->render('cursopadre/edit.html.twig', array(
            'cursoPadre' => $cursoPadre,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a cursoPadre entity.
     *
     * @Route("/{id}", name="cursopadre_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, CursoPadre $cursoPadre)
    {
        $form = $this->createDeleteForm($cursoPadre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cursoPadre);
            $em->flush();
        }

        return $this->redirectToRoute('cursopadre_index');
    }

    /**
     * Creates a form to delete a cursoPadre entity.
     *
     * @param CursoPadre $cursoPadre The cursoPadre entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CursoPadre $cursoPadre)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cursopadre_delete', array('id' => $cursoPadre->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
