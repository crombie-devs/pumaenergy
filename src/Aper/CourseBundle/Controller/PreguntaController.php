<?php

namespace Aper\CourseBundle\Controller;

use Aper\CourseBundle\Entity\Pregunta;
use Aper\CourseBundle\Entity\Examen;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Pregunta controller.
 *
 * @Route("admin/course/exame/pregunta")
 */
class PreguntaController extends Controller
{
    /**
     * Lists all pregunta entities.
     *
     * @Route("/{id}/", name="pregunta_index")
     * @Method("GET")
     */
    public function indexAction(Examen $examen)
    {
        $preguntas = [];
        $tipo='';
        if( !empty($examen->getPreguntas()) ){
            $preguntas = $examen->getPreguntas();
        }
        if(!is_null($examen->getCursoEncuesta())){
            $curso = $examen->getCursoEncuesta();
            $ruta = 'examen_index_encuesta';
            $tipo='encuesta';
        }
        if(!is_null($examen->getCursoPrevia())){
            $curso = $examen->getCursoPrevia();
            $ruta = 'examen_index_previo';
            $tipo='examenprevio';
        }
        if(!is_null($examen->getCursoFinal())){
            $curso = $examen->getCursoFinal();
            $ruta = 'examen_index_final';
            $tipo='examenfinal';
        }
        if(!is_null($examen->getCursoPadreEncuesta())){
            $curso = $examen->getCursoPadreEncuesta();
            $ruta = 'examen_index_encuesta_curso';
            $tipo='encuesta';
        }
        if(!is_null($examen->getCursoPadrePrevia())){
            $curso = $examen->getCursoPadrePrevia();
            $ruta = 'examen_index_previo_curso';
            $tipo='examenprevio';
        }
        if(!is_null($examen->getCursoPadreFinal())){
            $curso = $examen->getCursoPadreFinal();
            $ruta = 'examen_index_final_curso';
            $tipo='examenfinal';
        }
        return $this->render('pregunta/index.html.twig', array(
            'tipo' => $tipo,
            'examen' => $examen,
            'curso' => $curso,
            'preguntas' => $preguntas,
            'volver' => $ruta,
        ));
    }

    /**
     * Creates a new pregunta entity.
     *
     * @Route("/{id}/new", name="pregunta_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Examen $examen)
    {
        $pregunta = new Pregunta();
        $form = $this->createForm('Aper\CourseBundle\Form\PreguntaType', $pregunta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $examen->addPreguntas($pregunta);
            $pregunta->setExamen($examen);
            $em->persist($pregunta);
            $em->flush();

            return $this->redirectToRoute('pregunta_show', array('id' => $pregunta->getId()));
        }

        return $this->render('pregunta/new.html.twig', array(
            'pregunta' => $pregunta,
            'examen' => $examen,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a pregunta entity.
     *
     * @Route("/{id}/show", name="pregunta_show")
     * @Method("GET")
     */
    public function showAction(Pregunta $pregunta)
    {
        $deleteForm = $this->createDeleteForm($pregunta);

        return $this->render('pregunta/show.html.twig', array(
            'pregunta' => $pregunta,
            'examen' => $pregunta->getExamen(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing pregunta entity.
     *
     * @Route("/{id}/edit", name="pregunta_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Pregunta $pregunta)
    {
        $deleteForm = $this->createDeleteForm($pregunta);
        $editForm = $this->createForm('Aper\CourseBundle\Form\PreguntaType', $pregunta);
        $editForm->handleRequest($request);
        $examen= $pregunta->getExamen();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $examenCopy = ($pregunta->getExamen()) ? $pregunta->getExamen() : $examen;
            return $this->redirectToRoute('pregunta_index', array('id' => $pregunta->getId(),'examen' => $examenCopy));
        }

        return $this->render('pregunta/edit.html.twig', array(
            'pregunta' => $pregunta,
            'examen' => $examen,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a pregunta entity.
     *
     * @Route("/{id}/delete", name="pregunta_delete")
     * @Method({"GET","DELETE"})
     */
    public function deleteAction(Request $request, Pregunta $pregunta)
    {
        $form = $this->createDeleteForm($pregunta);
        $form->handleRequest($request);
        $examen = $pregunta->getExamen();
        $respuestas = $pregunta->getRespuestas();
//        if ($form->isSubmitted() && $form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        foreach ($respuestas as $respuesta){
            $em->remove($respuesta);
        }
        $examen->removePregunta($pregunta);
        $em->remove($pregunta);
        $em->flush();
//        }

        return $this->redirectToRoute('pregunta_index', array('id' => $examen->getId()));
    }

    /**
     * Creates a form to delete a pregunta entity.
     *
     * @param Pregunta $pregunta The pregunta entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Pregunta $pregunta)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pregunta_delete', array('id' => $pregunta->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
