<?php

namespace Aper\CourseBundle\Controller;

use Aper\UserBundle\Entity\Employee;
use Aper\CourseBundle\Entity\Course;
use Aper\CourseBundle\Entity\CursoPadre;
use Aper\CourseBundle\Entity\EmployeeExamen;
use Aper\CourseBundle\Entity\Examen;
use Aper\CourseBundle\Entity\ContenidoDeClase;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


/**
 * Respuesta controller.
 *
 * @Route("/curso")
 */
class CourseFrontendController extends Controller
{
    // Hasta que no quede debidamente presupuestado no puedo mostrar mucho mas.
    /** 
     * Lists all respuesta entities.
     *
     * @Route("/", name="curso_front")
     * @Method("GET")
     */
    public function indexAction()
    { //$this->getUser()->getEmployee()
        $curso_padre_repository = $this->getDoctrine()->getRepository(CursoPadre::class);
        $cursos = $curso_padre_repository->findAll();
        return $this->render('curso/index.html.twig', array('cursos' => $cursos));
    }

    /** 
     *
     * @Route("/clases/{curso_padre_id}/", name="curso_clase_front")
     * @Method("GET")
     */
    public function cursoClasesAction($curso_padre_id)
    {
        $curso_padre_repository = $this->getDoctrine()->getRepository(CursoPadre::class);
        $cursoPadre = $curso_padre_repository->find($curso_padre_id);
        $examenes_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);
        $examen_final = $examenes_repository->findBy(['examen' => $cursoPadre->getExamenFinal(), 
            'employee' => $this->getUser()->getEmployee()->getId()]);
        $examen_previo = $examenes_repository->findBy(['examen' => $cursoPadre->getExamenPrevio(), 
            'employee' => $this->getUser()->getEmployee()->getId()]);
        $examen_encuesta = $examenes_repository->findBy(['examen' => $cursoPadre->getEncuesta(), 
            'employee' => $this->getUser()->getEmployee()->getId()]);
        return $this->render('curso/clases.html.twig', array('cursoPadre' => $cursoPadre,
            'examenFinal' => $examen_final,
            'examenPrevio' => $examen_previo,
            'encuesta' => $examen_encuesta,
        ));


        /***cantidad de intentos(final y previo)--> ver si se usaron todos los intentos y no se aprobo el ultimo ***/
    }

    /** 
     *
     * @Route("/clases/clase/{course_id}/", name="clase_front")
     * @Method("GET")
     */
    public function claseIndividualAction($course_id){
        $curso_padre_repository = $this->getDoctrine()->getRepository(CursoPadre::class);
        $clase = $curso_padre_repository->find($course_id);
        $contenidoClaseRepository = $this->getDoctrine()->getRepository(ContenidoDeClase::Class);
        $contenidos = $contenidoClaseRepository->findBy(['cursoPadre'=> $clase]);

        return $this->render('curso/clase.html.twig', array('clase' => $clase, 'contenidos' => $contenidos));
    }

    /**** TESTEAR: contenido de clase(es igual a como estaba en la vista anterior) ******/

    /**
     * @Route("/certificado/{curso_padre_id}", name="cursopadre_certificado")
     * @Method("GET")
     */
    public function certificadoAction($curso_padre_id)
    {
        $curso_padre_repository = $this->getDoctrine()->getRepository(CursoPadre::class);
        $cursoPadre = $curso_padre_repository->find($curso_padre_id);
        $examenes_repository = $this->getDoctrine()->getRepository(EmployeeExamen::class);
        $examenes = $examenes_repository->findBy(['examen' => $cursoPadre->getExamenFinal(),
            'employee' => $this->getUser()->getEmployee()->getId()]);
        $aprobado = false;
        if(!$examenes){
            return $this->render('examen/error.html.twig', array('mensaje' => "Usted no ha rendido el examen final.", 'volver' => 'aper_store_index'));
        } else {
            foreach( $examenes as $examen ){
                if($examen->getAprobado())
                    $aprobado = true;
            }
            if($aprobado){
                $html = $this->renderView('curso/certificado.html.twig', array(
                    'cursoPadre' => $cursoPadre,
                    'empleado' => $this->getUser()->getEmployee()
                ));

                return new Response(
                    $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                    200,
                    array(
                        'Content-Type' => 'application/pdf',
                        'Content-Disposition' => 'attachment; filename="fichero.pdf"'
                    )
                );
            } else {
                return $this->render('examen/error.html.twig', array('mensaje' => "Usted no ha aprobado el examen final.", 'volver' => 'aper_store_index'));
            }
        }
    }
}
