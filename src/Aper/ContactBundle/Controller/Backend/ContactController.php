<?php

namespace Aper\ContactBundle\Controller\Backend;
use Aper\ContactBundle\Entity\Contact;
use Aper\ContactBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller
{

    public function indexAction(Request $request)
    {
        $contact = $this->getDoctrine()->getRepository(Contact::class)->findAll();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $contact, $request->query->getInt('page' , 1),
            10
        );

        return $this->render('AperContactBundle:Backend:index.html.twig', array('pagination' => $pagination));
    }


    public function newAction(Request $request)
    {
        $entity  = new Contact();
        $form = $this->createForm(new ContactType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Registro creado');
            return $this->redirect($this->generateUrl('aper_contact_index'));
        }

        return $this->render('AperContactBundle:Backend:new.html.twig', array('form' => $form->createView()));
    }

    public function deleteAction(Request $request, $id)
    {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AperContactBundle:Contact')->find($id);



            if (!$entity) {
                throw $this->createNotFoundException('No se encontró el contacto.');
            }

            try {
                $em->remove($entity);
                $em->flush();
                $request->getSession()->getFlashBag()->add('success', 'Contacto Eliminado');
            } catch (\Exception $exc) {
                $request->getSession()->getFlashBag()->add('error', $exc->getMessage());
            }


        return $this->redirect($this->generateUrl('aper_contact_index'));
    }
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $contact = $em->getRepository('AperContactBundle:Contact')->find($id);
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if(!$contact){
            throw $this->createNotFoundException('Contacto no encontrado');
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $em->flush();

            $this->addFlash('success', 'El contacto ha sido modificado');

            return $this->redirectToRoute('aper_contact_index');
        }


        return $this->render('AperContactBundle:Backend:edit.html.twig', array('contact' => $contact, 'form' => $form->createView()));

    }
}
