<?php

namespace Aper\ContactBundle\Controller\Frontend;
use Aper\StoreBundle\Entity\Store;
use Aper\ContactBundle\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;





class ContactController extends Controller
{

     /**
      * Lists all Category entities.
      *
      * @Route("/contact/index")

      */

    public function indexAction(Request $request)
    {

        $fullName = $request->get('fullname');
        $area = $request->get('area');
        $store = $request->get('store');
        $province= $request->get('province');

        $contact = $this->getDoctrine()->getRepository(Contact::class)->findByFilters($fullName,$area ,$store,$province);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $contact, $request->query->getInt('page' , 1),
            10
        );

        $stores = $this->getDoctrine()->getRepository(Store::class)->findAll();


        return $this->render('AperContactBundle:Frontend:index.html.twig', array('stores'=>$stores, 'pagination' => $pagination));
    }



}
