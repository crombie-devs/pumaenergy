<?php

namespace Aper\SurpriseTargetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ItemReporteConsolidadoFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id_objetivo', SearchType::class, ['label' => 'ID de Item Spot'])
            ->add('name', SearchType::class)
            ->add('station_code', SearchType::class)

            /*->add('date_from', DateType::class, [
                'widget' => 'single_text',
                'help_block' => 'Vigencia desde' ])
            ->add('date_to',  DateType::class, [
                'widget' => 'single_text',
                'help_block' => 'Vigencia hasta'])
            ->add('')*/
            ->add('export', SubmitType::class, array('label' => 'Exportar (Filtrar primero)'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);

        parent::configureOptions($resolver);
    }
}
