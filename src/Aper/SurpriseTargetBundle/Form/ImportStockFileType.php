<?php

namespace Aper\SurpriseTargetBundle\Form;

use Aper\SurpriseTargetBundle\Form\DTO\FileXLSDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WebFactory\Bundle\FileBundle\Form\Type\FilePreviewType;

class ImportStockFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FilePreviewType::class, [
                'show_legend' => false,
                'show_child_legend' => false,
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => FileXLSDTO::class,
            ]);
    }

    public function getBlockPrefix()
    {
        return 'avatar';
    }
}
