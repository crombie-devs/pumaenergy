<?php

namespace Aper\SurpriseTargetBundle\Form\DTO;

use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\FileBundle\Model\File;


class ImportDTO
{

    /**
     *
     * @var File
     * @Assert\Valid()
     */
    public $file;
}

