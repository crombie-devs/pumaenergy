<?php

namespace Aper\SurpriseTargetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ItemStockFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('station_code', SearchType::class)
            ->add('date_from', DateType::class, [
                'widget' => 'single_text',
                'help_block' => 'Vigencia desde' ])
            ->add('date_to',  DateType::class, [
                'widget' => 'single_text',
                'help_block' => 'Vigencia hasta']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);

        parent::configureOptions($resolver);
    }
}
