<?php

namespace Aper\SurpriseTargetBundle\Form;

use Aper\IncentiveBundle\Entity\SliderFile;
use Aper\SurpriseTargetBundle\Entity\SurpriseTarget;
use Aper\SurpriseTargetBundle\Entity\SurpriseTargetFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WebFactory\Bundle\FileBundle\Form\Type\FilePreviewType;

class SurpriseTargetFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FilePreviewType::class, [
                'show_legend' => false,
                'show_child_legend' => false
            ])
        ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => SurpriseTargetFile::class,
                'attr' => ['novalidate' => 'novalidate'],
            ]);
    }


    public function getBlockPrefix()
    {
        return 'surpriseTargetFile';
    }
}
