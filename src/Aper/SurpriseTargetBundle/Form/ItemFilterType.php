<?php

namespace Aper\SurpriseTargetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ItemFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', SearchType::class);
          /*  ->add('date_from', DateType::class, ['label' => "Vigencia desde",'widget' => 'single_text',
                'required' => true])
            ->add('date_to', DateType::class, ['label' => "Vigencia hasta",'widget' => 'single_text',
                'required' => true]
            ); */
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}
