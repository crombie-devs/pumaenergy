<?php

namespace Aper\SurpriseTargetBundle\Form;

use Aper\SurpriseTargetBundle\Form\DTO\ImportDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ImportSurpriseTargetStocksType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$months = range(1, 12);
        //$years = range(2020, 2035);

        $builder
            ->add('file', ImportStockFileType::class, [
                'label' => 'Debe tener las columnas: IdObjetivo, territoryCode, stock, dateFrom, dateTo, is_special, special_advance',
            ])
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => ImportDTO::class,
            ]);
    }


    public function getBlockPrefix()
    {
        return 'import_st_socks';
    }
}
