<?php

namespace Aper\SurpriseTargetBundle\Form;

use Aper\SurpriseTargetBundle\Entity\SurpriseTarget;
use Aper\SurpriseTargetBundle\Entity\SurpriseTargetFile;
use Doctrine\DBAL\Types\StringType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class CreateItemSpotGoalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$lastYear=date("Y",strtotime("-1 year"));
        //$currentYear=date("Y",time());

        $builder
        ->add('name', TextType::class)
        ->add('legals', TextareaType::class)
        ->add('surpriseTargetFile', SurpriseTargetFileType::class )
        ->add('save', SubmitType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => SurpriseTarget::class,
                'attr' => ['novalidate' => 'novalidate'],
            ]);
    }

    public function getBlockPrefix()
    {
        return 'surpriseTarget';
    }

}
