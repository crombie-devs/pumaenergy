<?php

namespace Aper\SurpriseTargetBundle\Services;


use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Liuggio\ExcelBundle\Factory;


class ExcelReportItemsDetallado
{
    private $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }


    public function getResponseFromItemsDetallado($items, $filename){
        ini_set('max_execution_time', 600);
        ini_set('memory_limit','1G');

        // solicitamos el servicio 'phpexcel' y creamos el objeto vacÃ­o...
        $phpExcelObject = $this->factory->createPHPExcelObject();

        // ...y le asignamos una serie de propiedades
        $phpExcelObject->getProperties()
            ->setCreator("Objetivos SPOT")
            ->setLastModifiedBy("Excel exporter")
            ->setTitle(utf8_decode("Reporte detallado de objetivos SPOT"))
            ->setSubject("Estadisticas")
            ->setDescription("Estadísticas de SPOTS generado el " . date('d/m/Y') . " a las " . date('H:i:s'))
            ->setKeywords("SPOTS, surprise targets, objetivos");

        // establecemos como hoja activa la primera, y le asignamos un título
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->setTitle('SpotsConsolidado');

        // escribimos en distintas celdas del documento el tÃ­tulo de los campos que vamos a exportar
        $sheet = $phpExcelObject->setActiveSheetIndex(0);


        $sheet->setCellValue('A1', 'ID SPOST');
        $sheet->setCellValue('B1', 'Período objetivo');
        $sheet->setCellValue('C1', 'Nombre');
        $sheet->setCellValue('D1', 'Territory Code');
        $sheet->setCellValue('E1', 'Colaborador');
        $sheet->setCellValue('F1', 'Fecha de venta');
        $sheet->setCellValue('G1', 'Cantidad vendida');
        $sheet->setCellValue('H1', 'Aprobado por el operador');
        $sheet->setCellValue('I1', 'Rechazado por el operador');

        $i= 2;
        foreach ($items as $item){
            /* #@var User $u */
            #$u = $user[0];
            $sheet->setCellValue('A'.$i, $item['id_objetivo']);
            $sheet->setCellValue('B'.$i, $item['fecha_desde']." - ".$item['fecha_hasta']);
            $sheet->setCellValue('C'.$i, $item['nombre_objetivo']);
            $sheet->setCellValue('D'.$i, $item['codigo_estacion']);
            $sheet->setCellValue('E'.$i, $item['email_employee']);
            $sheet->setCellValue('F'.$i, $item['fecha_carga']);
            $sheet->setCellValue('G'.$i, $item['cantidad_carga']);
            $sheet->setCellValue('H'.$i, $item['aprobado']);
            $sheet->setCellValue('I'.$i, $item['rechazado']);
            $i = $i+1;
        }
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $writer = $this->factory->createWriter($phpExcelObject, 'Excel5');
        $response = $this->factory->createStreamedResponse($writer);

        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }
}
