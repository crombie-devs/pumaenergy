<?php


namespace Aper\SurpriseTargetBundle\Services;

use Aper\SurpriseTargetBundle\Entity\SurpriseTarget;
use Aper\SurpriseTargetBundle\Entity\SurpriseTargetStock;
use Aper\SurpriseTargetBundle\Form\DTO\ImportDTO;
use Aper\SurpriseTargetBundle\Repository\SurpriseTargetStockRepository;
use DateTime;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Types\DateType;
use Doctrine\ORM\EntityManager;
use Liuggio\ExcelBundle\Factory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\VarDumper\VarDumper;

class SurpriseTargetStocksTypeImporter
{
    /**
     * @var Factory
     */
    private  $factory;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var SurpriseTargetStockRepository
     */
    private $repository;

    /**
     * SurpriseTargetStockImporter constructor.
     * @param Factory $factory
     * @param EntityManager $em
     */
    public function __construct(Factory $factory, EntityManager $em)
    {
        $this->factory = $factory;
        $this->em = $em;
        $this->repository = $this->em->getRepository('SurpriseTargetBundle:SurpriseTargetStock');
    }

    /**
     * @param ImportDTO $import
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \PHPExcel_Exception
     */
    public function import(ImportDTO $import)
    {

        $repository = $this->repository;

        $this->em->flush();

        $headers = array_combine([
                'IdObjetivo',
                'territoryCode',
                'stock',
                'dateFrom',
                'dateTo',
                'is_special',
                'special_advance'
            ],
            range('A', 'G')
        );

        $excel = $this->factory->createPHPExcelObject($import->file->getFile()->getPathname());

        $sheet = $excel->getActiveSheet();

        $highestRow = $this->getHighestDataRow($sheet);

        $allErrors = [];
        for ($i = 2; $i < $highestRow; $i++) {

            $errors = [];
            $IdObjetivo = trim($sheet->getCell($headers['IdObjetivo'] . $i)->getValue());
            if ('' === $IdObjetivo) {
                $errors[] = sprintf('La celda %s%s no puede estar en blanco', $headers['IdObjetivo'], $i);
            }

            $territoryCode = trim($sheet->getCell($headers['territoryCode'] . $i)->getValue());
            if ('' === $territoryCode) {
                $errors[] = sprintf('La celda %s%s no puede estar en blanco', $headers['territoryCode'], $i);
            }

            $stock = trim(strtoupper($sheet->getCell($headers['stock'] . $i)->getValue()));;
            if ('' === $stock) {
                $errors[] = sprintf('La celda %s%s no puede estar en blanco', $headers['stock'], $i);
            }

            $dateFrom = $sheet->getCell($headers['dateFrom'] . $i)->getValue();
            if ('' === $dateFrom) {
                $errors[] = sprintf('La celda %s%s no puede estar en blanco', $headers['dateFrom'], $i);
            }

            $dateTo = $sheet->getCell($headers['dateTo'] . $i)->getValue();
            if ('' === $dateTo) {
                $errors[] = sprintf('La celda %s%s no puede estar en blanco', $headers['dateTo'], $i);
            }

            $is_special = $sheet->getCell($headers['is_special'] . $i)->getValue();
            if ('' === $is_special) {
                $errors[] = sprintf('La celda %s%s no puede estar en blanco', $headers['is_special'], $i);
            }

            $special_advance = $sheet->getCell($headers['special_advance'] . $i)->getValue();
            if ('' === $special_advance) {
                $errors[] = sprintf('La celda %s%s no puede estar en blanco', $headers['special_advance'], $i);
            }

            # Busco un objetivo con el mismo item, para la misma estación, rango exacto de fechas o si la fecha que se quiere importar está solapando alguna existente, va a updatear y no crear uno nuevo.
            # Esto para evitar que haya fechas pisadas o doblemente incluidas en un rango
            $sql = "SELECT * FROM surprise_targets_station_stock where id_surprise_target = $IdObjetivo AND station_code = '$territoryCode'
            AND
            (
                (date_from = '$dateFrom' AND
                date_to = '$dateTo')
                or
                (date_from BETWEEN '$dateFrom' and '$dateTo')
                or
                (date_to BETWEEN '$dateFrom' and '$dateTo')
            )";

            $em = $this->em;
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $st_stock =  $stmt->fetchAll();

            if($st_stock){
                $id = $st_stock[0]['id'];
                $sql = "UPDATE surprise_targets_station_stock
                SET assigned_stock = $stock,
                is_special = $is_special,
                special_advance = $special_advance,
                date_to = '$dateTo', date_from = '$dateFrom'
                WHERE id = $id";
                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute();

            }else{
                $sql = "INSERT INTO surprise_targets_station_stock (id_surprise_target, station_code, assigned_stock, created_at, date_from, date_to, is_special, special_advance)
                values ($IdObjetivo, $territoryCode, $stock, now(), '$dateFrom','$dateTo', '$is_special', '$special_advance')";
                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute();

                if(!empty($errors)) {
                    $allErrors = array_merge($allErrors, $errors);
                }
            }

        }

        if (empty($allErrors)) {
            $this->em->flush();
        }

        return $allErrors;
    }


    /**
     * @param $sheet
     * @return int
     */
    private function getHighestDataRow($sheet)
    {
        for ($i = 2, $high = $sheet->getHighestDataRow(); $i <= $high; $i++) {
            // Si el valor de la celda A# es null se toma como una fila vacía
            if (
                null === $sheet->getCell('A'.$i)->getValue()
                && null === $sheet->getCell('B'.$i)->getValue()
                && null === $sheet->getCell('C'.$i)->getValue()

            ) {
                return $i - 1;
            }
        }

        return $i;
    }
}

