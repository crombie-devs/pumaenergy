<?php

namespace Aper\SurpriseTargetBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\FileBundle\Model\Image;
use Symfony\Component\Validator\Constraints\Callback;

/**
 * @ORM\Entity
 * @package WebFactory\Bundle\FileBundle\Model
 * @Callback(methods={"validate"})
 *
 */
class SurpriseTargetFile extends Image
{
    /**
     * Directorio relativo a guardar los archivos
     */
    const DIRECTORY = 'uploads/target_img/';
    //ver
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var SurpriseTarget
     * @ORM\OneToOne(targetEntity="SurpriseTarget", inversedBy="surpriseTargetFile")
     */
    public $surpriseTarget;


    /**
     * @var UploadedFile
     * @Assert\Image(
     *     minWidth = 200,
     *     minHeight = 200,
     *     maxWidth = 4000,
     *     maxHeight = 4000
     * )
     */
    public $file;



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return SurpriseTarget
     */
    public function getSurpriseTarget()
    {
        return $this->surpriseTarget;
    }

    /**
     * @param SurpriseTarget $surpriseTarget
     */
    public function setSurpriseTarget($surpriseTarget)
    {
        $this->surpriseTarget = $surpriseTarget;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        $this->ensureRequiredFile($context);
    }
}
