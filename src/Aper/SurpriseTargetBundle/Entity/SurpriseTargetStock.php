<?php

namespace Aper\SurpriseTargetBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SurpriseTargetStock
 *
 * @ORM\Table(name="surprise_targets_station_stock")
 * @ORM\Entity(repositoryClass="Aper\SurpriseTargetBundle\Repository\SurpriseTargetStockRepository")
 */
class SurpriseTargetStock
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     *  /**
     * @ORM\Column(name="id_surprise_target", type="integer")
     * @Assert\NotBlank()
     */
    private $id_surprise_target;


    /**
    * @ORM\ManyToOne(targetEntity="SurpriseTarget")
    * @ORM\JoinColumn(name="id_surprise_target", referencedColumnName="id")
    */
    private $surpriseTarget;


    /**
     * @ORM\Column(name="station_code", type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $station_code;

    /**
     * @var int
     *
     * @ORM\Column(name="assigned_stock", type="integer")
     * @Assert\NotBlank()
     */
    private $assigned_stock;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\NotBlank()
     */
    private $created_at;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date_from", type="date")
     * @Assert\NotBlank()
     */
    private $date_from;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date_to", type="date")
     * @Assert\NotBlank()
     */
    private $date_to;


    /**
    * @ORM\Column(name="is_special", type="boolean")
    */
    private $is_special;


    /**
    * @ORM\Column(name="special_advance", type="integer")
    */
    private $special_advance;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idSurpriseTarget
     *
     * @param integer $id_surprise_target
     *
     * @return SurpriseTargetStock
     */

    public function setIdSurpriseTarget($id_surprise_target)
    {
        $this->id_surprise_target = $id_surprise_target;
        //$this->surpriseTarget = $id_surprise_target;
        return $this;
    }



    public function setSurpriseTarget($surpriseTarget){
        $this->surpriseTarget = $surpriseTarget;
    }

    public function getSurpriseTarget(){
        return $this->surpriseTarget;
    }

    /**
     * Get idSurpriseTarget
     *
     * @return integer
     */

    public function getIdSurpriseTarget()
    {
        return $this->id_surprise_target;
    }


    /**
     * Set idStore
     *
     * @param integer $idStore
     *
     * @return SurpriseTargetStock
     */
    public function setStationCode($station_code)
    {
        $this->station_code = $station_code;

        return $this;
    }

    /**
     * Get idStore
     *
     * @return int
     */
    public function getStationCode()
    {
        return $this->station_code;
    }

    /**
     * Set assignedStock
     *
     * @param integer $assignedStock
     *
     * @return SurpriseTargetStock
     */
    public function setAssignedStock($assigned_stock)
    {
        $this->assigned_stock = $assigned_stock;

        return $this;
    }

    /**
     * Get assignedStock
     *
     * @return int
     */
    public function getAssignedStock()
    {
        return $this->assigned_stock;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return SurpriseTargetStock
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

     /**
     * Set dateFrom
     *
     * @param \Date $dateFrom
     *
     * @return SurpriseTarget
     */
    public function setDateFrom($dateFrom)
    {
        $this->date_from = $dateFrom;

        return $this;
    }

    /**
     * Get dateFrom
     *
     * @return \Date
     */
    public function getDateFrom()
    {
        return $this->date_from;
    }

    /**
     * Set dateTo
     *
     * @param \Date $dateTo
     *
     * @return SurpriseTarget
     */
    public function setDateTo($dateTo)
    {
        $this->date_to = $dateTo;

        return $this;
    }

    /**
     * Get dateTo
     *
     * @return \Date
     */
    public function getDateTo()
    {
        return $this->date_to;
    }
}

