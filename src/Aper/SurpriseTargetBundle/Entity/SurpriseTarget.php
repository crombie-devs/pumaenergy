<?php

namespace Aper\SurpriseTargetBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class SurpriseTarget
 *
 * @ORM\Table(name="surprise_targets")
 * @ORM\Entity(repositoryClass="Aper\SurpriseTargetBundle\Repository\SurpriseTargetRepository")
 */
class SurpriseTarget
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

      /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     *
     * @var SurpriseTargetFile
     * @ORM\OneToOne(targetEntity="SurpriseTargetFile", cascade={"all"}, mappedBy="surpriseTarget")
     * @Assert\Valid()
     */

    public $surpriseTargetFile;

        /**
     * @var string
     *
     * @ORM\Column(name="legals", type="string")
     * @Assert\NotBlank()
     */
    private $legals;


    /**
     * Get legals
     *
     * @return string
     */
    public function getLegals()
    {
        return $this->legals;
    }

     /**
     * Set legals
     *
     * @param string $legals
     *
     * @return SurpriseTarget
     */
    public function setLegals($legals)
    {
        $this->legals = $legals;

        return $this;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SurpriseTarget
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }



        /**
     * Set file
     *
     * @param SurpriseTargetFile $surpriseTargetFile
     *
     * @return SurpriseTarget
     */
    public function setSurpriseTargetFile($surpriseTargetFile)
    {
        $this->surpriseTargetFile = $surpriseTargetFile;
        $this->surpriseTargetFile->setSurpriseTarget($this);

        return $this;
    }

    /**
     * Get surpriseTargetFile
     *
     * @return SurpriseTargetFile
     */
    public function getSurpriseTargetFile()
    {
        return $this->surpriseTargetFile;
    }


}

