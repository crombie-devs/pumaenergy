<?php

namespace Aper\SurpriseTargetBundle\Entity;

use Aper\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SurpriseTargetUserStock
 *
 * @ORM\Table(name="surprise_targets_station_stock_users")
 * @ORM\Entity(repositoryClass="Aper\SurpriseTargetBundle\Repository\SurpriseTargetUserStockRepository")
 */
class SurpriseTargetUserStock
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="SurpriseTargetStock")
    * @ORM\JoinColumn(name="id_surprise_target_station_stock", referencedColumnName="id")
    */
    private $stationStock;



    /**
    * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\User", inversedBy="surpriseTargetStocks")
    * @ORM\JoinColumn(name="id_user", referencedColumnName="id")
    */
    private $user;


    /**
     * @var int
     *
     *  /**
     * @ORM\Column(name="assigned_stock", type="integer")
     * @Assert\NotBlank()
     */
    private $assignedStock;

  /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\NotBlank()
     */
    private $createdAt;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stationStock
     *
     * @param integer $stationStock
     *
     * @return SurpriseTargetUserStock
     */
    public function setStationStock($stationStock)
    {
        $this->stationStock = $stationStock;

        return $this;
    }

    /**
     * Get stationStock
     *
     * @return int
     */
    public function getstationStock()
    {
        return $this->stationStock;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return SurpriseTargetUserStock
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set assignedStock
     *
     * @param integer $assignedStock
     *
     * @return SurpriseTargetUserStock
     */
    public function setAssignedStock($assignedStock)
    {
        $this->assignedStock = $assignedStock;

        return $this;
    }

    /**
     * Get assignedStock
     *
     * @return int
     */
    public function getAssignedStock()
    {
        return $this->assignedStock;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return SurpriseTargetUserStock
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}

