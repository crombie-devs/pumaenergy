<?php

namespace Aper\SurpriseTargetBundle\Controller;

use Aper\SurpriseTargetBundle\Entity\SurpriseTarget;
use Aper\SurpriseTargetBundle\Entity\SurpriseTargetStock;
use Aper\SurpriseTargetBundle\Entity\SurpriseTargetUserStock;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints\Date;

class HomeController extends Controller
{
    /**
     * Add a new image file
     *
     * @Route()e("/", name="aper_target_surprise_front_home")
     * @Method()d({"GET","POST"})
     */

    public function listAction(Request $request)
    {
        /*
            Listado de items en la pantalla del operador, para ingresar a cada uno y hacer asignación de stock por colaborador
        */
        $user = $this->getUser()->getId();

        // $targets = $this->getDoctrine()->getRepository(SurpriseTarget::class)->getSurpriseTargets();

        //Stores del usuario
        $sql = "SELECT stores, store_id, u.roles
        FROM employees e
        JOIN users u on e.user_id = u.id
        WHERE e.user_id = $user";
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $stores =  $stmt->fetchAll();
        $st_id = $stores[0]['store_id'];

        //$is_commercial_profile = (strpos($stores[0]['roles'], 'ROLE_HEAD_OFFICE') !== false? true: false) || (strpos($stores[0]['roles'], 'ROLE_MANAGER_GLOBAL') !== false? true: false);

        //return new  \Symfony\Component\HttpFoundation\Response(json_encode($user->maxRoleType));
        if($stores[0]['stores'] != ''){

            $string_stores = $stores[0]['stores'];
            // Objetivos para sus stores (stores)
            $sql = "SELECT st.*, stss.date_from, stss.date_to, stss.is_special, stss.id  as idAsignacion, img.file_path as file
            FROM surprise_targets st
            LEFT JOIN SurpriseTargetFile img on st.id = img.surpriseTarget_id
            JOIN surprise_targets_station_stock stss on stss.id_surprise_target = st.id
            LEFT JOIN stores a on a.code = stss.station_code WHERE stss.station_code in ($string_stores) or a.id = '$st_id' order by stss.id desc";

            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $targets =  $stmt->fetchAll();

        }else{

            // Objetivos para su store (store_id)
            $sql = "SELECT a.*, b.date_from, b.date_to, b.is_special, b.id as idAsignacion, img.file_path as file
            FROM surprise_targets a
            LEFT JOIN SurpriseTargetFile img on a.id = img.surpriseTarget_id
            JOIN surprise_targets_station_stock b on a.id = b.id_surprise_target
            JOIN stores c on b.station_code = c.code WHERE c.id = $st_id ";

            $sql .= " order by b.id desc";
            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $targets =  $stmt->fetchAll();
        }


        return $this->render('@SurpriseTarget/Home/Operator/homeItems.html.twig', ['targets' => $targets]);
    }

    public function listManagerAction(Request $request)
    {
        /*
            Listado de items en la pantalla del operador, para ingresar a cada uno y hacer asignación de stock por colaborador
        */
        $user = $this->getUser()->getId();
		
		//Agrega el Max Role para determinar si es ADMIN
		$userRole = $this->getUser()->getMaxRoleType(); 

        // $targets = $this->getDoctrine()->getRepository(SurpriseTarget::class)->getSurpriseTargets();

        

			$sql = "SELECT stores, store_id, u.roles
			FROM employees e
			JOIN users u on e.user_id = u.id
			WHERE e.user_id = $user";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $stores =  $stmt->fetchAll();
        $st_id = $stores[0]['store_id'];

	if ($userRole != 'ROLE_MANAGER_GLOBAL') {
        if($stores[0]['stores'] != ''){

            $string_stores = $stores[0]['stores'];
            // Objetivos para sus stores (stores)
            $sql = "SELECT st.*, stss.date_from, stss.date_to, stss.is_special, stss.id as idAsignacion, img.file_path as file
            FROM surprise_targets st
            LEFT JOIN SurpriseTargetFile img on st.id = img.surpriseTarget_id
            JOIN surprise_targets_station_stock stss on stss.id_surprise_target = st.id
            LEFT JOIN stores a on a.code = stss.station_code
            WHERE stss.station_code in ($string_stores) or a.id = '$st_id'
            GROUP BY st.id, stss.date_from, stss.date_to";
            $sql .= " order by stss.id desc ";
            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $targets =  $stmt->fetchAll();

        }else{

            // Objetivos para su store (store_id)
            $sql = "SELECT a.*, b.date_from, b.date_to, b.is_special, b.id as idAsignacion , img.file_path as file
            FROM surprise_targets a
            LEFT JOIN SurpriseTargetFile img on a.id = img.surpriseTarget_id
            JOIN surprise_targets_station_stock b on a.id = b.id_surprise_target
            JOIN stores c on b.station_code = c.code
            WHERE c.id = $st_id
            GROUP BY a.id, b.date_from, b.date_to";

           $sql .= " order by b.id desc ";

            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $targets =  $stmt->fetchAll();
        }
	} else
		{
			$sql = "SELECT a.*, b.date_from, b.date_to, b.is_special, b.id as idAsignacion , img.file_path as file
            FROM surprise_targets a
            LEFT JOIN SurpriseTargetFile img on a.id = img.surpriseTarget_id
            JOIN surprise_targets_station_stock b on a.id = b.id_surprise_target
            JOIN stores c on b.station_code = c.code
            GROUP BY a.id, b.date_from, b.date_to";

           $sql .= " order by b.id desc ";

            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $targets =  $stmt->fetchAll();
			
			
		}
	
        //

        return $this->render('@SurpriseTarget/Home/Operator/homeItems.html.twig', ['targets' => $targets]);
    }


    public function listComercialAction(Request $request)
    {
        /*
            Listado de items en la pantalla del operador/comercial/Administrador, para ingresar a cada uno y hacer asignación de stock por colaborador
        */
        $user = $this->getUser()->getId();

        // $targets = $this->getDoctrine()->getRepository(SurpriseTarget::class)->getSurpriseTargets();

        //Stores del usuario
        $sql = "SELECT stores, store_id, u.roles
        FROM employees e
        JOIN users u on e.user_id = u.id
        WHERE e.user_id = $user";
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $stores =  $stmt->fetchAll();
        $st_id = $stores[0]['store_id'];

        if($stores[0]['stores'] != ''){

            $string_stores = $stores[0]['stores'];
            // Objetivos para sus stores (stores)
            $sql = "SELECT st.*, stss.date_from, stss.date_to, stss.is_special, stss.id as idAsignacion, img.file_path as file
            FROM surprise_targets st
            LEFT JOIN SurpriseTargetFile img on st.id = img.surpriseTarget_id
            JOIN surprise_targets_station_stock stss on stss.id_surprise_target = st.id
            LEFT JOIN stores a on a.code = stss.station_code
            WHERE stss.station_code in ($string_stores) or a.id = '$st_id'
            GROUP BY st.id, stss.date_from, stss.date_to";
            $sql .= " order by stss.id desc ";


            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $targets =  $stmt->fetchAll();

        }else{

            // Objetivos para su store (store_id)
            $sql = "SELECT a.*, b.date_from, b.date_to, b.is_special, b.id as idAsignacion , img.file_path as file
            FROM surprise_targets a
            LEFT JOIN SurpriseTargetFile img on a.id = img.surpriseTarget_id
            JOIN surprise_targets_station_stock b on a.id = b.id_surprise_target
            JOIN stores c on b.station_code = c.code GROUP BY a.id, b.date_from, b.date_to";

            $sql .= " order by b.id desc ";

            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $targets =  $stmt->fetchAll();
        }
        //

        return $this->render('@SurpriseTarget/Home/Operator/homeItems.html.twig', ['targets' => $targets]);
    }

    # VIEW
    # Descripción: Pantalla de detalle de un item spot. Con las estaciones para el filtro de select.
    public function itemDetailAction(Request $request, $id, $assign_id_stock)
    {
        /*
            Pantalla para asignar los stocks del item seleccionado a cada colaborador
        */

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $empleador = $user->getEmployee();

        $estacion = $empleador->getStore();
        $empleadosRepository = $this->getDoctrine()->getRepository(Employee::class);
        $targetRepository = $this->getDoctrine()->getRepository(SurpriseTarget::class);

        $idStores = $empleador->getStores();

        // Si tiene stores concatenadas
        if (!empty($idStores)) {
            $idStores = explode(',', $idStores);
            $idStores[] = $estacion->getCode();
            $string_from_array = implode(',', (array) $idStores);

            $sql = "SELECT * FROM stores where code in ($string_from_array)";

            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $estaciones =  $stmt->fetchAll();

        }else{
            $estaciones = null;
        }


        $target = $targetRepository->getById($id);

        return $this->render('@SurpriseTarget/Home/Operator/itemDetail.html.twig',
        [
            "idStores" => $idStores,
            "estaciones" => $estaciones,
            "estacion" => $estacion,
            "target" => $target,
            "assign_id_stock" => $assign_id_stock
        ]);
    }

    # VIEW
    # Descripción: Listado de empleados con sus % de objetivo cumplido. Devolvemos las stores para el filtro de select. Los empleados vienen por AJAX.
    public function performanceAction(Request $request, $id, $assign_id_stock)
    {
        /*
            Pantalla de rendimiento de ventas de los colaboradores del item seleccionado
        */

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $empleador = $user->getEmployee();

        $estacion = $empleador->getStore();
        $empleadosRepository = $this->getDoctrine()->getRepository(Employee::class);
        $targetRepository = $this->getDoctrine()->getRepository(SurpriseTarget::class);

        $idStores = $empleador->getStores();

        // Si tiene stores concatenadas
        if (!empty($idStores)) {
            $idStores = explode(',', $idStores);
            $idStores[] = $estacion->getCode();
            $string_from_array = implode(',', (array) $idStores);

            $sql = "SELECT * FROM stores where code in ($string_from_array)";

            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $estaciones =  $stmt->fetchAll();

        }else{
            $estaciones = null;
        }

        $target = $targetRepository->getById($id);

        return $this->render('@SurpriseTarget/Home/Operator/itemPerformance.html.twig', [
            "id" => $id,
            "idStores" => $idStores,
            "estaciones" => $estaciones,
            "estacion" => $estacion,
            "target" => $target,
            'assign_id_stock' => $assign_id_stock
        ]);
    }

     # VIEW
    # Descripción: Performance del objetivo especial para el operador
    public function performanceSpecialAction(Request $request, $id, $assign_id_stock)
    {
        /*
            Pantalla de rendimiento de ventas de los colaboradores del item seleccionado
        */

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $empleador = $user->getEmployee();

        $estacion = $empleador->getStore();
        $empleadosRepository = $this->getDoctrine()->getRepository(Employee::class);
        $targetRepository = $this->getDoctrine()->getRepository(SurpriseTarget::class);

        $idStores = $empleador->getStores();

        // Si tiene stores concatenadas
        if (!empty($idStores)) {
            $idStores = explode(',', $idStores);
            $idStores[] = $estacion->getCode();
            $string_from_array = implode(',', (array) $idStores);

            $sql = "SELECT * FROM stores where code in ($string_from_array)";

            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $estaciones =  $stmt->fetchAll();

        }else{
            $estaciones = null;
        }

        $target = $targetRepository->getById($id);

        return $this->render('@SurpriseTarget/Home/Operator/itemPerformanceSpecial.html.twig', [
            "id" => $id,
            "idStores" => $idStores,
            "estaciones" => $estaciones,
            "estacion" => $estacion,
            "target" => $target,
            'assign_id_stock' => $assign_id_stock
        ]);
    }

    # VIEW
    # Descripción: Pantalla donde el gerente confirma las ventas cargadas por sus empleados
    public function confirmSalesAction(Request $request, $id, $assign_id_stock)
    {
        /*
            Pantalla para que el gerente confirme las ventas de sus colaboradores una vez validado
        */

         /*
            Pantalla de rendimiento de ventas de los colaboradores del item seleccionado
        */

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $empleador = $user->getEmployee();

        $estacion = $empleador->getStore();
        $empleadosRepository = $this->getDoctrine()->getRepository(Employee::class);
        $targetRepository = $this->getDoctrine()->getRepository(SurpriseTarget::class);

        $idStores = $empleador->getStores();

        // Si tiene stores concatenadas
        if (!empty($idStores)) {
            $idStores = explode(',', $idStores);
            $idStores[] = $estacion->getCode();
            $string_from_array = implode(',', (array) $idStores);

            $sql = "SELECT * FROM stores where code in ($string_from_array)";

            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $estaciones =  $stmt->fetchAll();

        }else{
            $estaciones = null;
        }

        $target = $targetRepository->getById($id);

        return $this->render('@SurpriseTarget/Home/Operator/itemConfirmSales.html.twig',  [
            "id" => $id,
            "idStores" => $idStores,
            "estaciones" => $estaciones,
            "estacion" => $estacion,
            "target" => $target,
            "assign_id_stock" => $assign_id_stock
        ]);
    }


    # API - GET
    # Descripción: Devuelve el listado de empleados de la estación filtrada, con el stock asignado para cada uno. Se consume con AJAX.
    public function getAssignedStocksByEmployeeIdAction(Request $request ){

        $targetId =  $request->get('target');
        $assign_id_stock = $request->get('assign_id_stock');
        $estacionFiltrada = $request->get('station');
        $em = $this->getDoctrine()->getManager();

        // usuario autenticado
        $user = $this->get('security.token_storage')->getToken()->getUser();

        // traigo el operador
        $empleador = $user->getEmployee();

        // estación dle operador
        $estacion = $empleador->getStore();


        // Busco el target
        $sql = "SELECT a.*, img.file_path as file from surprise_targets a
        LEFT JOIN SurpriseTargetFile img on img.surpriseTarget_id = a.id
        where a.id = '$targetId'";


        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $targetSql =  $stmt->fetchAll();

        if(!$targetSql){
            $response = new  \Symfony\Component\HttpFoundation\Response(json_encode([
                "message" => "No se encontró el objetivo spot"
            ]));
            return $response;
        }
        $finalResponse = [];

        $finalResponse['employees'] = [];

        // Busco los empleados de la estación filtrada
        $sql = 'SELECT u.id, p.name, sr.short_name as subrol
        FROM users u
        JOIN employees e on e.user_id = u.id
        JOIN profiles p on p.employee_id = e.id
        LEFT JOIN sub_rol sr on e.subRol_id = sr.id
        left JOIN stores s on s.id = e.store_id
        WHERE u.roles like "%ROLE_EMPLOYEE%"
        AND s.id = "'.$estacionFiltrada.'" AND u.enabled = 1 order by subrol desc';

        //return new  \Symfony\Component\HttpFoundation\Response(json_encode($sql));


        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $data =  $stmt->fetchAll();

        // Asignado a los empleados
        $total_assigned = 0;

        // Stock asignado a la estación, lo que se debe asignar a los empleados
        $to_assign = 0;

        // BUSCO LAS FECHAS DEL ASSIGN_ID_STOCK RECIBIDO
        $busco = "SELECT date_from, date_to FROM surprise_targets_station_stock where id = $assign_id_stock";
        $stmt = $em->getConnection()->prepare($busco);
        $stmt->execute();
        $asignacionConFechas =  $stmt->fetchAll();


        $desde = $asignacionConFechas[0]['date_from'];
        $hasta = $asignacionConFechas[0]['date_to'];


        // Busco stock asignado de la estación CON LA MISMA FECHA
        $sqlSS = "SELECT ss.assigned_stock, ss.id as id_asignacion_store FROM  surprise_targets_station_stock ss
        JOIN stores s on ss.station_code = s.code  WHERE s.id = '$estacionFiltrada' AND ss.id_surprise_target = '$targetId' and ss.date_from = '$desde' and ss.date_to = '$hasta'";

        $stmt = $em->getConnection()->prepare($sqlSS);
        $stmt->execute();
        $stockStation =  $stmt->fetchAll();


        foreach ($data as $emp) {
            $id = $emp['id'];

            $sqlStock = "SELECT SUM(IFNULL(sts.assigned_stock, 0)) as stockAsignado, st.name as nameTarget
            FROM users u
            JOIN employees e on e.user_id = u.id
            JOIN profiles p on p.employee_id = e.id
            left JOIN stores s on s.id = e.store_id
            LEFT JOIN surprise_targets_station_stock_users sts on sts.id_user = u.id
            LEFT JOIN surprise_targets_station_stock stss on stss.id = sts.id_surprise_target_station_stock
            LEFT JOIN surprise_targets st on st.id = stss.id_surprise_target
            WHERE sts.id_user = '$id' and stss.id_surprise_target = '$targetId' and stss.id and stss.date_from = '$desde' and stss.date_to = '$hasta'
            group by u.id";



            $stmt = $em->getConnection()->prepare($sqlStock);
            $stmt->execute();
            $stock =  $stmt->fetchAll();

            $empleado = [];
            $empleado['id'] = $emp['id'];
            $empleado['name'] = $emp['name'];
            $empleado['subrol'] = $emp['subrol'];

            if(!$stock){
                $empleado['asignado'] = 0;
            }else{
                $empleado['asignado'] = $stock[0]['stockAsignado'];
            }

            array_push($finalResponse['employees'], $empleado);

        }

        if($stockStation){
            $to_assign = $stockStation[0]['assigned_stock'];
        }else{
            $to_assign = 0;
        }

        $finalResponse['to_assign'] = $to_assign - $total_assigned;
        $finalResponse['total_stock'] = $to_assign;
        $finalResponse['assigned'] = $total_assigned;
        $finalResponse['nameTarget'] = $targetSql[0]['name'];
        $finalResponse['fileTarget'] = $targetSql[0]['file'];
        $finalResponse['targetId'] = $targetId;
        $finalResponse['id_asignacion_store'] = $stockStation?$stockStation[0]['id_asignacion_store']: null;


        $response = new  \Symfony\Component\HttpFoundation\Response(json_encode($finalResponse));
        return $response;

    }

    # API - POST
    # Descripción: Recibe un arreglo de empleados con las asignaciones de stock y las crea o actualiza si el empleado ya tenía objetivo asignado.
    public function saveAssignedStocksByEmployeesAction(Request $request)
    {
        $employees = $request->get('employees');
        $targetId = $request->get('targetId');
        $station = $request->get('station');
        $assign_id_stock = $request->get('assign_id_stock');

        foreach ($employees as $emp ) {

            $asignado = $emp['asignado'];
            $user_id = $emp['id'];

            // Busco el id de stock de la estación
            $sql = "SELECT a.id FROM surprise_targets_station_stock a
            JOIN stores on a.station_code = stores.code
            WHERE stores.id = '$station' AND a.id_surprise_target = '$targetId' and a.id = $assign_id_stock";
            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $stockStation =  $stmt->fetchAll();
            $id_stock_station = $stockStation[0]['id'];


            // Chequeamos si el usuario ya tiene un stock asignado
            $checkStock = "SELECT a.id FROM surprise_targets_station_stock_users a
            JOIN surprise_targets_station_stock b on a.id_surprise_target_station_stock = b.id
            JOIN stores on b.station_code = stores.code
            WHERE stores.id = '$station' AND a.id_user = '$user_id' AND id_surprise_target_station_stock = $id_stock_station AND b.id = $assign_id_stock";
            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($checkStock);
            $stmt->execute();
            $stock =  $stmt->fetchAll();

            // Si ya tiene stock, actualizamos su asignado
            if($stock){
                $id_stock_user = $stock[0]['id'];

                $sqlStock = "UPDATE surprise_targets_station_stock_users
                SET assigned_stock = '$asignado' WHERE id = '$id_stock_user'";
                $em = $this->getDoctrine()->getManager();
                $stmt = $em->getConnection()->prepare($sqlStock);
                $exec = $stmt->execute();
            }else{
                // De lo contrario, creamos un nuevo stock para el usuario
                $sqlStock = "INSERT INTO surprise_targets_station_stock_users
                (id_user, assigned_stock, created_at, id_surprise_target_station_stock) values ($user_id, $asignado, now(), $id_stock_station)";
                $em = $this->getDoctrine()->getManager();
                $stmt = $em->getConnection()->prepare($sqlStock);
                $exec = $stmt->execute();
            }
        }

        $response = new  \Symfony\Component\HttpFoundation\Response(json_encode($exec));
        return $response;
    }

    # VIEW
    # Descripción: Pantalla de detalle de un item spot, para el perfil empleado.
    public function getSurpriseTargetEmployeeDetailAction(Request $request, $id, $assign_id_stock)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $user_id = $user->getId();


        // Ver si es especial
        $search = "SELECT *
        FROM surprise_targets_station_stock a
        JOIN surprise_targets b on a.id_surprise_target = b.id
        LEFT JOIN SurpriseTargetFile c on c.surpriseTarget_id = b.id
        WHERE a.id = $assign_id_stock";
        $stmt = $em->getConnection()->prepare($search);
        $stmt->execute();
        $stockIsSpecial =  $stmt->fetchAll();


        if($stockIsSpecial[0]['is_special'] == 1)
        {
            $loaded     = $stockIsSpecial[0]['special_advance'];
            $assigned   = $stockIsSpecial[0]['assigned_stock'] ? $stockIsSpecial[0]['assigned_stock'] : 0;
            $imagen     = $stockIsSpecial[0]['file_path'];
            $name       = $stockIsSpecial[0]['name'];
            $date_to = $stockIsSpecial[0]['date_to'];
            $date_from = $stockIsSpecial[0]['date_from'];
            $is_special = $stockIsSpecial[0]['is_special'];
            $file = $stockIsSpecial[0]['file_path'];
        }else{
            #Query:  Suma de cargas realizadas por el usuario y aprobadas por el gerente
            $countLoaded = "SELECT COALESCE(SUM(IF(a.confirmed_by_manager, IF(a.rejected != 1, a.stock_loaded, 0), 0)),0) as loaded
            FROM surprise_targets_station_stock_user_loaded a
            LEFT JOIN surprise_targets_station_stock_users b on a.id_surprise_targets_station_stock_users = b.id
            LEFT JOIN surprise_targets_station_stock c on b.id_surprise_target_station_stock = c.id
            WHERE b.id_user = '$user_id'
            AND  c.id_surprise_target = '$id' and c.id = $assign_id_stock";
            $stmt = $em->getConnection()->prepare($countLoaded);
            $stmt->execute();
            $stock =  $stmt->fetchAll();


            #Query: Asignado al usuario
            $sqlAssigned = "SELECT a.assigned_stock as assigned, c.name, img.file_path as file, b.date_from, b.date_to, b.is_special
            FROM surprise_targets_station_stock_users a
            JOIN surprise_targets_station_stock b on a.id_surprise_target_station_stock = b.id
            JOIN surprise_targets c on c.id = b.id_surprise_target
            LEFT JOIN SurpriseTargetFile img on c.id = img.surpriseTarget_id
            WHERE a.id_user = '$user_id'
            AND  b.id_surprise_target = '$id' and b.id = $assign_id_stock";
            $stmt = $em->getConnection()->prepare($sqlAssigned);
            $stmt->execute();
            $assign =  $stmt->fetchAll();


            if($assign){
                $loaded     = $stock[0]['loaded'];
                $assigned   = $assign[0]['assigned'] ? $assign[0]['assigned'] : 0;
                $imagen     = $assign[0]['file'];
                $name       = $assign[0]['name'];
                $date_to = $assign[0]['date_to'];
                $date_from = $assign[0]['date_from'];
                $is_special = $assign[0]['is_special'];
                $file = $assign[0]['file'];
            }else{
                $loaded = 0;
                $assigned = 0;
                $imagen = null;
                $name = null;
                $date_to = '2020-12-12';
                $date_from = '2020-12-12';
                $is_special = 1;
                $file = "";
            }
        }

        return $this->render('@SurpriseTarget/Home/Employee/itemDetail.html.twig',
        [
            'loaded' => $loaded,
            'assigned' => $assigned,
            'percent' => $assigned == 0? 0: bcdiv(($loaded*100)/$assigned, '1', 0),
            'name' => $name,
            'imagen' => $imagen,
            'target_id' => $id,
            'date_to' => $date_to,
            'date_from' => $date_from,
            'is_special' => $is_special,
            'assign_id_stock' => $assign_id_stock,
            'file' => $file
        ]);
    }

    # API - POST
    # Descripción: Guarda una nueva venta. Para el empleado.
    public function saveMySalesAction(Request $request)
    {
        $assign_id_stock = $request->get('assign_id_stock');
        $em = $this->getDoctrine()->getManager();
        $loaded = $request->get('newStock');
        $target_id = $request->get('target_id');
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $user_id = $user->getId();

        # Query: Busco el stock asignado del empleado

        $sql = "SELECT a.id
        FROM surprise_targets_station_stock_users a
        JOIN surprise_targets_station_stock b on a.id_surprise_target_station_stock = b.id
        WHERE b.id_surprise_target = '$target_id' AND a.id_user = '$user_id' and b.id = $assign_id_stock";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $myAssigned =  $stmt->fetchAll();
        $id = $myAssigned[0]['id'];


        # Query: Guardo el stock cargado relacionado al stock asignado buscado previamente
        $sql = "INSERT INTO surprise_targets_station_stock_user_loaded (id_surprise_targets_station_stock_users, stock_loaded, confirmed_by_manager, created_at)
        values ($id, $loaded, 0, now())";
        $stmt = $em->getConnection()->prepare($sql);
        $exec = $stmt->execute();

        $response = new  \Symfony\Component\HttpFoundation\Response(json_encode(["message" => $exec ? true: false]));
        return $response;
    }

    # API - GET
    # Descripción: devuelve el listado de ventas cargadas por el usuario logueado.
    public function getMySalesHistoryAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $user_id = $user->getId();
        $target_id = $request->get('target_id');
        $assign_id_stock = $request->get('assign_id_stock');

        # Query: Busco el stock asignado del empleado
        $sql = "SELECT a.id
        FROM surprise_targets_station_stock_users a
        JOIN surprise_targets_station_stock b on a.id_surprise_target_station_stock = b.id
        WHERE b.id_surprise_target = '$target_id' AND a.id_user = '$user_id' and b.id = $assign_id_stock";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $myAssigned =  $stmt->fetchAll();

       // return new  \Symfony\Component\HttpFoundation\Response(json_encode(["sql" => $myAssigned]));

       if($myAssigned){
            $id = $myAssigned[0]['id'];
            //return new  \Symfony\Component\HttpFoundation\Response(json_encode(["sql" => $id]));
             # Query: Busco las cargas realizadas por el usuario
            $sql = "SELECT *
            FROM surprise_targets_station_stock_user_loaded
            WHERE id_surprise_targets_station_stock_users = $id order by created_at desc";

            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $history =  $stmt->fetchAll();

            //return new  \Symfony\Component\HttpFoundation\Response(json_encode(["sql" => $sql]));
       }else{
           $history = [];
       }

       $response = new  \Symfony\Component\HttpFoundation\Response(json_encode(["history" => $history]));
        return $response;
    }

    public function getEmployeesPerformanceAction(Request $request)
    {

        $assign_id_stock = $request->get('assign_id_stock');
        $targetId =  $request->get('target');

        $estacionFiltrada = $request->get('station');

        // usuario autenticado
        $user = $this->get('security.token_storage')->getToken()->getUser();

        // traigo el operador
        $empleador = $user->getEmployee();

        // estación dle operador
        $estacion = $empleador->getStore();

        // Busco el target
        $sql = "SELECT * from surprise_targets where id = '$targetId'";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $targetSql =  $stmt->fetchAll();

        if(!$targetSql){
            $response = new  \Symfony\Component\HttpFoundation\Response(json_encode([
                "message" => "No se encontró el objetivo spot"
            ]));
            return $response;
        }
        $finalResponse = [];

        $finalResponse['employees'] = [];


         // BUSCO LAS FECHAS DEL ASSIGN_ID_STOCK RECIBIDO
         $busco = "SELECT date_from, date_to FROM surprise_targets_station_stock where id = $assign_id_stock";
         $stmt = $em->getConnection()->prepare($busco);
         $stmt->execute();
         $asignacionConFechas =  $stmt->fetchAll();

         $desde = $asignacionConFechas[0]['date_from'];
         $hasta = $asignacionConFechas[0]['date_to'];

        // Busco los empleados de la estación filtrada con su total de ventas y asignado por el gerente
        $sql = "SELECT u.id, p.name,
        COALESCE(SUM(IF(loaded.confirmed_by_manager, IF(loaded.rejected != 1, loaded.stock_loaded, 0), 0)),0) as totalSales,
        COALESCE(stock.assigned_stock ,0) as assigned,
        s.location as storeName
        FROM users u
        JOIN employees e on e.user_id = u.id
        JOIN profiles p on p.employee_id = e.id
        left JOIN stores s on s.id = e.store_id
        LEFT JOIN surprise_targets_station_stock_users stock on u.id = stock.id_user
        LEFT JOIN surprise_targets_station_stock stationStock on stationStock.id = stock.id_surprise_target_station_stock
        LEFT JOIN surprise_targets_station_stock_user_loaded loaded on loaded.id_surprise_targets_station_stock_users = stock.id
        WHERE u.roles like '%ROLE_EMPLOYEE%'
        AND s.id = '$estacionFiltrada'
        AND u.enabled = 1
        AND stationStock.id_surprise_target = $targetId
        AND stationStock.date_from = '$desde' and stationStock.date_to = '$hasta'
        GROUP BY u.id";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $data =  $stmt->fetchAll();

        //die($sql);
        // Ordenamiento del arreglo por el porcentaje de cumplimiento de objetivos de cada objeto
        usort($data, function($objeto1, $objeto2){
            if($objeto1['assigned'] == 0){
                $objeto1['assigned'] = 0.000001;
            }

            if($objeto2['assigned'] == 0){
                $objeto2['assigned'] = 0.000001;
            }

            return
                (
                    ($objeto1['totalSales']* 100 ) / $objeto1['assigned']
                )
                <
                (
                    ($objeto2['totalSales']* 100 ) / $objeto2['assigned']
                ) ||
                    $objeto1['assigned'] < $objeto2['assigned']
                ? 1 : -1;

        });

        // Asignado a los empleados
        $total_assigned = 0;

        // Stock asignado a la estación, lo que se debe asignar a los empleados
        $to_assign = 0;

        // Busco stock asignado de la estación
        $sqlSS = "SELECT ss.id as id_asignacion_store,
        ss.assigned_stock,
        st.name as targetName,
        img.file_path as targetFile
        FROM  surprise_targets_station_stock ss
        JOIN stores s on ss.station_code = s.code
        LEFT JOIN surprise_targets st on st.id = ss.id_surprise_target
        LEFT JOIN SurpriseTargetFile img on st.id = img.surpriseTarget_id
        WHERE s.id = '$estacionFiltrada' AND ss.id_surprise_target = '$targetId' AND ss.date_from = '$desde' AND ss.date_to = '$hasta'";
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sqlSS);
        $stmt->execute();
        $stockStation =  $stmt->fetchAll();


        $response = new  \Symfony\Component\HttpFoundation\Response(json_encode(
        [
            "employees" => $data,
            "station" => [
                'assigned' => $stockStation? $stockStation[0]['assigned_stock']: 0,
                'sold' => null
            ],
            "target" => [
                'name' => $stockStation? $stockStation[0]['targetName']: '',
                'file' => $stockStation? $stockStation[0]['targetFile']: '',
            ],
            "assign_id_stock" => $stockStation? $stockStation[0]['id_asignacion_store']: null
        ]));

        return $response;
    }


    public function getEmployeesPerformanceSpecialAction(Request $request)
    {

        $assign_id_stock = $request->get('assign_id_stock');
        $targetId =  $request->get('target');

        $estacionFiltrada = $request->get('station');

        // Busco los empleados de la estación filtrada con su total de ventas y asignado por el gerente
        $sql = "SELECT stationStock.assigned_stock, stationStock.special_advance,  a.name as targetName, img.file_path as targetFile
        FROM surprise_targets_station_stock stationStock
        JOIN surprise_targets a on a.id = stationStock.id_surprise_target
        JOIN SurpriseTargetFile img on img.surpriseTarget_id = a.id
        JOIN stores s on s.code = stationStock.station_code
        WHERE stationStock.id_surprise_target = $targetId
        AND stationStock.id = $assign_id_stock and s.id = $estacionFiltrada";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $data =  $stmt->fetchAll();


        $response = new  \Symfony\Component\HttpFoundation\Response(json_encode(
        [
            "station" => [
                'assigned' => $data? $data[0]['assigned_stock']: 0,
                'sold' => $data? $data[0]['special_advance']: 0,
            ],
            "target" => [
                'name' => $data? $data[0]['targetName']: '',
                'file' => $data? $data[0]['targetFile']: '',
            ],
            "assign_id_stock" => $assign_id_stock
        ]));

        return $response;
    }


    # API - GET
    # Descripción: Devuelve el listado de empleados de la estación filtrada, con sus ventas cargadas para que el gerente las apruebe.
    public function getSalesSumPerEmployeeAction(Request $request ){

        $targetId =  $request->get('target');
        $assign_id_stock = $request->get('assign_id_stock');
        $estacionFiltrada = $request->get('station');
        $em = $this->getDoctrine()->getManager();

        // BUSCO LAS FECHAS DEL ASSIGN_ID_STOCK RECIBIDO
        $busco = "SELECT date_from, date_to FROM surprise_targets_station_stock where id = $assign_id_stock";
        $stmt = $em->getConnection()->prepare($busco);
        $stmt->execute();
        $asignacionConFechas =  $stmt->fetchAll();

        $desde = $asignacionConFechas[0]['date_from'];
        $hasta = $asignacionConFechas[0]['date_to'];

        // usuario autenticado
        $user = $this->get('security.token_storage')->getToken()->getUser();

        // traigo el operador
        $empleador = $user->getEmployee();

        // estación dle operador
        $estacion = $empleador->getStore();


        // Busco el target
        $sql = "SELECT a.*, img.file_path as file
        from surprise_targets a
        LEFT JOIN SurpriseTargetFile img  on a.id = img.surpriseTarget_id
        where a.id = '$targetId'";


        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $targetSql =  $stmt->fetchAll();

        if(!$targetSql){
            $response = new  \Symfony\Component\HttpFoundation\Response(json_encode([
                "message" => "No se encontró el objetivo spot"
            ]));
            return $response;
        }
        $finalResponse = [];

        $finalResponse['employees'] = [];

        // Busco los empleados de la estación filtrada
        $sql = "SELECT stationStock.id as id_asignacion_store , u.id, p.name,
        COALESCE(SUM(IF(loaded.confirmed_by_manager,    IF(loaded.rejected != 1, loaded.stock_loaded,           0), 0)),0) as approvedSales,
        COALESCE(SUM(IF(loaded.rejected != 1,           IF(loaded.confirmed_by_manager != 1,loaded.stock_loaded,0), 0)),0) as pendingSales,
        s.location as storeName
        FROM users u
        JOIN employees e on e.user_id = u.id
        JOIN profiles p on p.employee_id = e.id
        left JOIN stores s on s.id = e.store_id
        LEFT JOIN surprise_targets_station_stock_users stock on u.id = stock.id_user
        LEFT JOIN surprise_targets_station_stock stationStock on stationStock.id = stock.id_surprise_target_station_stock
        LEFT JOIN surprise_targets_station_stock_user_loaded loaded on loaded.id_surprise_targets_station_stock_users = stock.id
        WHERE u.roles like '%ROLE_EMPLOYEE%'
        AND s.id = '$estacionFiltrada'
        AND u.enabled = 1
        AND stationStock.id_surprise_target = $targetId
        and stationStock.date_from = '$desde' and stationStock.date_to = '$hasta'
        GROUP BY u.id ORDER BY pendingSales DESC";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $data =  $stmt->fetchAll();

        // Asignado a los empleados
        $total_assigned = 0;

        // Stock asignado a la estación, lo que se debe asignar a los empleados
        $to_assign = 0;

        // Busco stock asignado de la estación
        $sqlSS = "SELECT ss.assigned_stock FROM  surprise_targets_station_stock ss
        JOIN stores s on ss.station_code = s.code  WHERE s.id = '$estacionFiltrada' AND ss.id_surprise_target = '$targetId' and ss.date_from = '$desde' and ss.date_to = '$hasta'";
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sqlSS);
        $stmt->execute();
        $stockStation =  $stmt->fetchAll();


        return new  \Symfony\Component\HttpFoundation\Response(json_encode(
            [
                'employees' => $data,
                'targetFile' => $targetSql[0]['file'],
                'id_asignacion_store' => ($data? $data[0]['id_asignacion_store'] : null) ]));

    }

    public function getSalesPerEmployeeAction(Request $request)
    {
        $targetId =  $request->get('target');

        $assign_id_stock = $request->get('assign_id_stock');

        $empUserId = $request->get('userId');

        $estacionFiltrada = $request->get('station');

        // usuario autenticado
        $user = $this->get('security.token_storage')->getToken()->getUser();

        // traigo el operador
        $empleador = $user->getEmployee();

        // estación dle operador
        $estacion = $empleador->getStore();


        // Busco el target
        $sql = "SELECT * from surprise_targets where id = '$targetId'";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $targetSql =  $stmt->fetchAll();

        if(!$targetSql){
            $response = new  \Symfony\Component\HttpFoundation\Response(json_encode([
                "message" => "No se encontró el objetivo spot"
            ]));
            return $response;
        }
        $finalResponse = [];

        $finalResponse['employees'] = [];

        # 0: Pendiente, 1: Confirmado, 2: Rechazado
        // Busco los empleados de la estación filtrada
        $sql = "SELECT u.id, p.name,
        loaded.id as saleId,
        loaded.stock_loaded as sold,
        loaded.created_at as date,
        IF(loaded.rejected, 2, IF(loaded.confirmed_by_manager,1,0)) as status
        FROM users u
        JOIN employees e on e.user_id = u.id
        JOIN profiles p on p.employee_id = e.id
        left JOIN stores s on s.id = e.store_id
        LEFT JOIN surprise_targets_station_stock_users stock on u.id = stock.id_user
        LEFT JOIN surprise_targets_station_stock stationStock on stationStock.id = stock.id_surprise_target_station_stock
        JOIN surprise_targets_station_stock_user_loaded loaded on loaded.id_surprise_targets_station_stock_users = stock.id
        WHERE u.roles like '%ROLE_EMPLOYEE%'
        AND s.id = '$estacionFiltrada'
        AND u.enabled = 1
        AND stationStock.id_surprise_target = $targetId
        AND u.id = $empUserId
        and stationStock.id = $assign_id_stock
        ORDER BY loaded.created_at desc";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $data =  $stmt->fetchAll();

        return new  \Symfony\Component\HttpFoundation\Response(json_encode(['employees' => $data]));
    }


    public function changeSaleStatusAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $saleId =  $request->get('id');
        $status =  $request->get('status');

        $sql = "UPDATE surprise_targets_station_stock_user_loaded ";

        if($status == 0){
            $sql .= " SET rejected = 1 ";
        }else{
            $sql .= " SET confirmed_by_manager = 1";
        }

        $sql .= " WHERE id = $saleId";


        $stmt = $em->getConnection()->prepare($sql);
        $exec = $stmt->execute();

        $buscar = "SELECT stock.id_user
        FROM surprise_targets_station_stock_user_loaded loaded
        JOIN surprise_targets_station_stock_users stock on stock.id = loaded.id_surprise_targets_station_stock_users
        where loaded.id=$saleId";
        $stmt = $em->getConnection()->prepare($buscar);
        $stmt->execute();
        $data =  $stmt->fetchAll();

        return new  \Symfony\Component\HttpFoundation\Response(json_encode(["message" => $exec, "user_id" => $data[0]['id_user'] ]));
    }

    public function viewPerformanceForZoneAction(Request $request, $id, $assign_id_stock)
    {
        $em = $this->getDoctrine()->getManager();
        // Buscamos las zonas para mostrar el filtro
        $sql = "SELECT DISTINCT zona FROM stores WHERE zona IS NOT NULL";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $zones =  $stmt->fetchAll();

        return $this->render('@SurpriseTarget/Home/Commercial/itemDetail.html.twig',
        [
            'zones' => $zones,
            'target' => $id,
            'assign_id_stock' => $assign_id_stock
        ]);
    }

    public function getStoresPerZoneAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $targetId =  $request->get('target');
        $zoneFiltered = $request->get('zone');
        $assign_id_stock = $request->get('assign_id_stock');

        # Busco las stores que tengan ese objetivo asignado
        $sql = " SELECT
        a.id,
        a.code,
        a.location,
        img.file_path as file,
        b.id as idStoreStock,
        b.date_from as desde,
        b.date_to as hasta,
        b.assigned_stock as totalAsignadoObjetivo,
        a.zona
        FROM stores a
        LEFT JOIN surprise_targets_station_stock b ON a.code = b.station_code
        LEFT JOIN surprise_targets c on c.id = b.id_surprise_target
        LEFT JOIN SurpriseTargetFile img on c.id = img.surpriseTarget_id
        where a.zona like '%$zoneFiltered%'
        AND tipo_comercio = 'EESS'
        AND b.id_surprise_target = $targetId
        GROUP BY b.id
        ORDER BY b.date_to desc";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $stores =  $stmt->fetchAll();

        // Por cada store, calculamos las ventas y el objetivo asignado
        for ($i=0; $i < sizeof($stores); $i++) {

            $idAsignacionStore = $stores[$i]['idStoreStock'];

            // Necesito:
            $sql = "SELECT
            COALESCE(SUM(c.stock_loaded),0) as totalVendidoObjetivo
            FROM
            surprise_targets_station_stock a
            LEFT JOIN surprise_targets_station_stock_users  b ON b.id_surprise_target_station_stock = a.id
            LEFT JOIN surprise_targets_station_stock_user_loaded c on c.id_surprise_targets_station_stock_users = b.id
            AND a.id = $idAsignacionStore
			WHERE c.confirmed_by_manager=1";

            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $moreInfo =  $stmt->fetchAll();

            if($moreInfo){
                $stores[$i]['totalVendidoObjetivo'] = $moreInfo[0]['totalVendidoObjetivo'];
            }else{
                $stores[$i]['totalVendidoObjetivo'] = 0;
            }
        }

        $con_ventas = [];
        $sin_ventas = [];

        $finalData = [];

        foreach ($stores as $st) {
            $st['percent'] = floor(($st['totalVendidoObjetivo'] * 100)/$st['totalAsignadoObjetivo']);

            if($st['percent'] > 0){
                array_push($con_ventas, $st);
            }else{
                array_push($sin_ventas, $st);
            }
        }


        // ORDENAMIENTO CON VENTAS

        usort($con_ventas, function($objeto1, $objeto2) use ($con_ventas){
            if($objeto1['totalAsignadoObjetivo'] == 0){
                $objeto1['totalAsignadoObjetivo'] = 0.000001;
            }

            if($objeto2['totalAsignadoObjetivo'] == 0){
                $objeto2['totalAsignadoObjetivo'] = 0.000001;
            }

            if(
                (($objeto1['totalVendidoObjetivo']* 100 ) / $objeto1['totalAsignadoObjetivo'])
                <
                (($objeto2['totalVendidoObjetivo']* 100 ) / $objeto2['totalAsignadoObjetivo'])
            ){
                return 1;
            }else{
                return -1;
            }
        });

        // ORDENAMIENTO SIN VENTAS
        usort($sin_ventas, function($objeto1, $objeto2) use ($sin_ventas){
            if($objeto1['totalAsignadoObjetivo'] == 0){
                $objeto1['totalAsignadoObjetivo'] = 0.000001;
            }

            if($objeto2['totalAsignadoObjetivo'] == 0){
                $objeto2['totalAsignadoObjetivo'] = 0.000001;
            }

            if(
                $objeto1['totalAsignadoObjetivo']
                <
                $objeto2['totalAsignadoObjetivo']
            ){
                return 1;
            }else{
                return -1;
            }
        });


        return new  \Symfony\Component\HttpFoundation\Response(json_encode([
            "target" => $targetId,
            "stores" => array_merge($con_ventas, $sin_ventas),
            "file" => $stores? $stores[0]['file']: ""
        ]));
    }

    public function getPerformancePerStationTargetAction(Request $request)
    {
        $stockStation =  $request->get('stockStation');
        $em = $this->getDoctrine()->getManager();

        $sql = "SELECT
        a.name as nombreEmpleado,
        d.location,
        h.name as targetName,
        img.file_path as targetImgUrl,
        f.assigned_stock as stockAsignadoEmpleado,
        COALESCE(SUM(g.stock_loaded),0) as stockVendidoEmpleado
        FROM profiles a
        JOIN employees b on a.employee_id = b.id
        JOIN users c on b.user_id = c.id
        JOIN stores d on b.store_id = d.id
        JOIN surprise_targets_station_stock e on e.station_code = d.code
        JOIN surprise_targets_station_stock_users f on f.id_surprise_target_station_stock = e.id and c.id = f.id_user
        LEFT JOIN surprise_targets_station_stock_user_loaded g on g.id_surprise_targets_station_stock_users = f.id
        JOIN surprise_targets h on h.id = e.id_surprise_target
        LEFT JOIN SurpriseTargetFile img on img.surpriseTarget_id = h.id
        WHERE e.id = $stockStation
        AND (g.confirmed_by_manager = 1 AND g.rejected <> 1 or g.id is null)
        group by c.id
        ";

        $newArray = [];

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $employees =  $stmt->fetchAll();


        $con_ventas = [];
        $sin_ventas = [];

        foreach ($employees as $emp) {
            if($emp['stockAsignadoEmpleado']){
                $emp['percent'] = floor(($emp['stockVendidoEmpleado'] * 100)/$emp['stockAsignadoEmpleado']);
            }else{
                $emp['percent'] = 0;
            }

            if($emp['percent'] > 0){
                array_push($con_ventas, $emp);
            }else{
                array_push($sin_ventas, $emp);
            }

            array_push($newArray, $emp);
        }

         // ORDENAMIENTO CON VENTAS

         usort($con_ventas, function($objeto1, $objeto2) use ($con_ventas){
            if($objeto1['stockAsignadoEmpleado'] == 0){
                $objeto1['stockAsignadoEmpleado'] = 0.000001;
            }

            if($objeto2['stockAsignadoEmpleado'] == 0){
                $objeto2['stockAsignadoEmpleado'] = 0.000001;
            }

            if(
                (($objeto1['stockVendidoEmpleado']* 100 ) / $objeto1['stockAsignadoEmpleado'])
                <
                (($objeto2['stockVendidoEmpleado']* 100 ) / $objeto2['stockAsignadoEmpleado'])
            ){
                return 1;
            }else{
                return -1;
            }
        });

        // ORDENAMIENTO SIN VENTAS
        usort($sin_ventas, function($objeto1, $objeto2) use ($sin_ventas){
            if($objeto1['stockAsignadoEmpleado'] == 0){
                $objeto1['stockAsignadoEmpleado'] = 0.000001;
            }

            if($objeto2['stockAsignadoEmpleado'] == 0){
                $objeto2['stockAsignadoEmpleado'] = 0.000001;
            }

            if(
                $objeto1['stockAsignadoEmpleado']
                <
                $objeto2['stockAsignadoEmpleado']
            ){
                return 1;
            }else{
                return -1;
            }
        });


        /*
        $keys = array_column($newArray, 'percent');
        array_multisort($keys, SORT_DESC, $newArray);


        usort($employees, function($objeto1, $objeto2){
            if($objeto1['stockAsignadoEmpleado'] == 0){
                $objeto1['stockAsignadoEmpleado'] = 0.000001;
            }

            if($objeto2['stockAsignadoEmpleado'] == 0){
                $objeto2['stockAsignadoEmpleado'] = 0.000001;
            }

            return
                (
                    ($objeto1['stockVendidoEmpleado']* 100 ) / $objeto1['stockAsignadoEmpleado']
                )
                <
                (
                    ($objeto2['stockVendidoEmpleado']* 100 ) / $objeto2['stockAsignadoEmpleado']
                )
                ? 1 : -1;

        });*/


        return new  \Symfony\Component\HttpFoundation\Response(json_encode(["employees"=> array_merge($con_ventas, $sin_ventas)] ));
    }

    public function getTargetsPerStoreIdAction(Request $request)
    {
        $storeId =  $request->get('store');
        $em = $this->getDoctrine()->getManager();


        $sql = "SELECT b.id, c.name, b.date_from, b.date_to
        FROM stores a
        JOIN surprise_targets_station_stock b on a.code = b.station_code
        JOIN surprise_targets c on c.id = b.id_surprise_target
        WHERE a.id = $storeId order by b.date_from desc";


        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $targets =  $stmt->fetchAll();

        return new  \Symfony\Component\HttpFoundation\Response(json_encode(["id" => $storeId, "targets" => $targets ] ));
    }

    public function getEmployeeTargetsPerStoreIdAction(Request $request)
    {
        $id =  $request->get('item');
        $em = $this->getDoctrine()->getManager();

        $sql = "SELECT
        b.id as user_id,
        e.name,
        d.assigned_stock as originalStore,
        a.category as categoriaEmpleado
        FROM employees a
        JOIN users b on b.id = a.user_id
        JOIN stores c on c.id = a.store_id
        JOIN surprise_targets_station_stock d on d.station_code = c.code
        JOIN profiles e on e.employee_id = a.id
        WHERE b.roles like '%ROLE_EMPLOYEE%' and (a.deleted != 1 or a.deleted is null) and d.id = $id GROUP by b.id";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $employees =  $stmt->fetchAll();

        $originalAsignadoStore = 0;

        if($employees){
            $originalAsignadoStore = $employees[0]['originalStore'];
        }

        $newData = [];

        foreach ($employees as $emp) {
            $idUser = $emp['user_id'];

            $sqlStock = "SELECT
            IFNULL(sts.assigned_stock, 0) as stockAsignado,
            sts.id as idAsignacionUser,
            COALESCE(SUM(z.stock_loaded), 0) as alcanzado,
            stss.assigned_stock as stockAsignadoEstacion
            FROM users u
            JOIN employees e on e.user_id = u.id
            JOIN profiles p on p.employee_id = e.id
            left JOIN stores s on s.id = e.store_id
            LEFT JOIN surprise_targets_station_stock_users sts on sts.id_user = u.id
            LEFT JOIN surprise_targets_station_stock stss on stss.id = sts.id_surprise_target_station_stock
            LEFT JOIN surprise_targets_station_stock_user_loaded z on z.id_surprise_targets_station_stock_users = sts.id
            WHERE sts.id_user = '$idUser' and stss.id = '$id'
            group by u.id";

            $stmt = $em->getConnection()->prepare($sqlStock);
            $stmt->execute();
            $asignado =  $stmt->fetchAll();

            if($asignado){
                $emp['asignado'] = $asignado[0]['stockAsignado'];
                $emp['id_asignacion'] = $asignado[0]['idAsignacionUser'];
                $emp['alcanzado'] = $asignado[0]['alcanzado'];
                $totalAssigned = $asignado[0]['stockAsignadoEstacion'];
            }else{
                $emp['asignado'] = 0;
                $emp['id_asignacion'] = null;
                $emp['alcanzado'] = 0;
                $totalAssigned = 0;
            }

            array_push($newData, $emp);
        }
        return new  \Symfony\Component\HttpFoundation\Response(json_encode(["id" => $id, "employees" => $newData, "totalAssigned" => $totalAssigned, "original" => $originalAsignadoStore] ));
    }

    public function setAssignedStockOfEmployeeAction(Request $request){

        $storeSpot =  $request->get('storeSpot');
        //$user_id =  $request->get('user_id');
        //$id_assign =  $request->get('id_assign');
        //$newValue =  $request->get('newValue');
        $g_currentlyAsigned = $request->get('g_currentlyAsigned');
        $g_originalAssigned = $request->get('g_originalAssigned');

        $g_mainData = $request->get('g_mainData');

        $em = $this->getDoctrine()->getManager();

        // Validaciones
        if(!$storeSpot ){
            return new  \Symfony\Component\HttpFoundation\Response(json_encode(["message" => "Ocurrió un problema en el servidor, contacte al administrador.", "type" => "error"]));
        }

        // Existe el objetivo de la estación?
        $existsStoreSpot = $this->getDoctrine()->getRepository(SurpriseTargetStock::class)->getById($storeSpot);
        if(!$existsStoreSpot){
            return new  \Symfony\Component\HttpFoundation\Response(json_encode(["message" => "El objetivo de la estación no existe", "type" => "error"]));
        }


        // Si modificó el total asignado a la estación, actualizamos ese valor
        if($g_currentlyAsigned != $g_originalAssigned && $g_currentlyAsigned > 0 && $g_currentlyAsigned < 50000000)
        {
            $sql = "UPDATE surprise_targets_station_stock SET assigned_stock = $g_currentlyAsigned  where id=$storeSpot";
            $stmt = $em->getConnection()->prepare($sql);
            $execute = $stmt->execute();
        }


        // Actualizar stocks asignados a empleados uno por uno

        foreach ($g_mainData as $emp ) {

            $asignado = $emp['asignado'];
            $id_assign = $emp['id_asignacion'];
            $user_id = $emp['user_id'];

            if($asignado < 0 || $asignado > 9999999){
                return new  \Symfony\Component\HttpFoundation\Response(json_encode(["message" => "Se excede la cantidad de stock permitido.", "type" => "error"]));
            }

            // Si llega el id de asignación, lo actualizamos con el emp['asignado']
            if($id_assign){
                $sqlStock = "UPDATE surprise_targets_station_stock_users
                SET assigned_stock = $asignado
                WHERE id_user = $user_id and id_surprise_target_station_stock = $storeSpot";

            }else{
            // De lo contrario, creamos la asignación con el nuevo value
                $sqlStock = "INSERT INTO  surprise_targets_station_stock_users
                (id_user, assigned_stock, created_at, id_surprise_target_station_stock)
                values
                ($user_id, $asignado, now(), $storeSpot)";
            }

            $stmt = $em->getConnection()->prepare($sqlStock);
            $execute = $stmt->execute();
        }


        if($execute){
            return new  \Symfony\Component\HttpFoundation\Response(json_encode(["message" => "Actualizado con éxito", "type"=>"success"]));
        }else{
            return new  \Symfony\Component\HttpFoundation\Response(json_encode(["message" => "Ocurrió un problema al actualizar el stock", "type"=>"error"]));
        }

    }
}
