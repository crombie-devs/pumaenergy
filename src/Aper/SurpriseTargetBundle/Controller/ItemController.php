<?php

namespace Aper\SurpriseTargetBundle\Controller;

use Aper\SurpriseTargetBundle\Form\CreateItemSpotGoalType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Aper\BenefitBundle\Entity\Benefit;
use Aper\SurpriseTargetBundle\Entity\SurpriseTarget;
use Aper\SurpriseTargetBundle\Entity\SurpriseTargetStock;
use Aper\SurpriseTargetBundle\Form\DTO\ImportDTO;
use Aper\SurpriseTargetBundle\Form\EditItemSpotGoalType;
use Aper\SurpriseTargetBundle\Form\ImportSurpriseTargetStocksType;
use Aper\SurpriseTargetBundle\Form\ItemFilterType;
use Aper\SurpriseTargetBundle\Form\ItemReporteConsolidadoFilterType;
use Aper\SurpriseTargetBundle\Form\ItemReporteDetalladoFilterType;
use Aper\SurpriseTargetBundle\Form\ItemStockFilterType;
use Aper\SurpriseTargetBundle\Repository\SurpriseTargetRepository;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use WebFactory\Bundle\GridBundle\Event\ManipulateQueryEvent;
use WebFactory\Bundle\GridBundle\Grid\Configuration;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ItemController extends Controller
{
    /**
     * Add a new image file
     *
     * @Route()e("/", name="aper_target_surprise_home")
     * @Method()d({"GET","POST"})
     */
    public function listAction(Request $request)
    {
        return $this->render('SurpriseTargetBundle:Item:list.html.twig');
    }

    public function createAction(Request $request)
    {
        $target = new SurpriseTarget();
        $form = $this->createForm(CreateItemSpotGoalType::class, $target);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($target);
            $em->flush();
            $this->addFlash('success', 'Objetivo spot creado con éxito');
            return $this->redirectToRoute('aper_target_surprise_home');
        }

        return $this->render('SurpriseTargetBundle:Items:create.html.twig' , [
            'form' => $form->createView()
        ]);
    }

        /**
     * Finds and displays a benefit entity.
     *
     * @Route("/details/{id}", name="aper_target_surprise_show")
     * @Method("GET")
     */
    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('SurpriseTargetBundle:SurpriseTarget');

        $target = $repository->find($id);

        if (!$target) {
            $messageException = 'Objetivo spot no encontrado';
            throw $this->createNotFoundException($messageException);
        }

        return $this->render('SurpriseTargetBundle:Items:show.html.twig' , ['target' => $target]);
    }


    /**
     * Creates a form to delete a benefit entity.
     *
     * @param Benefit $benefit The benefit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SurpriseTarget $surprise)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('aper_benefit_benefit_delete', array('id' => $surprise->getId())))
            ->add('submit', SubmitType::class, [
                'label' => 'Delete',
            ])
            ->setAttributes([
                'class' => 'delete-form',
                'id' => 'delete-form',
            ])
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $config = new Configuration(
            $this->getDoctrine()->getRepository(SurpriseTarget::class)->getQueryBuilder(),
            $this->get('web_factory_grid.form_filter_factory')->create(ItemFilterType::class),
            'main',
            'SurpriseTargetBundle:Items:list.html.twig'
        );

        $grid = $this->get('web_factory_grid.factory')->create($config);

        $grid->addListener(ManipulateQueryEvent::NAME, function (ManipulateQueryEvent $event) {
            $queryBuilder = $event->getQueryBuilder();

            $filters = $event->getFilters();

            if (null !== $filters['name']) {
                $queryBuilder->andWhere('SurpriseTarget.name LIKE :name')->setParameter('name', "%{$filters['name']}%");
            }

        });

        return $grid->handle($request);
    }


        /**
     * Importer of Goal Types
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/import/", name="aper_target_surprise_import_stock")
     * @Method({"GET", "POST"})
     */
    public function importStockAction(Request $request)
    {
        $import = new ImportDTO();

        $form = $this->createForm(ImportSurpriseTargetStocksType::class, $import);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $errors = $this->get('importer_st_stocks')->import($import);

            if (empty($errors)) {
                $this->addFlash('success', 'La importación fue exitosa');
            } else {
                $this->addFlash('error', 'El archivo no se ha importado. Estos fueron los errores:');
                foreach ($errors as $error) {
                    $this->addFlash('error', $error);
                }
            }
        }


        return $this->render('@SurpriseTarget/Items/import.html.twig', array(
            'form' => $form->createView(),
        ));
    }

        /**
     * @param Request $request
     *
     * @return Response
     */
    public function listStocksAction(Request $request)
    {

        $config = new Configuration(
            $this->getDoctrine()->getRepository(SurpriseTargetStock::class)->getQueryBuilder(),
            $this->get('web_factory_grid.form_filter_factory')->create(ItemStockFilterType::class),
            'main',
            'SurpriseTargetBundle:Items:listStocks.html.twig'
        );

        $grid = $this->get('web_factory_grid.factory')->create($config);

        $grid->addListener(ManipulateQueryEvent::NAME, function (ManipulateQueryEvent $event) {
            $queryBuilder = $event->getQueryBuilder();

            $filters = $event->getFilters();

            if (null !== $filters['station_code']) {
                $queryBuilder->andWhere('SurpriseTargetStock.station_code LIKE :storecode')->setParameter('storecode', "%{$filters['station_code']}%");
            }


            if(isset($_GET['date_from'])){
                if (null !== $_GET['date_from']) {
                    $queryBuilder
                    ->andWhere('SurpriseTargetStock.date_from = :vigence_from')->setParameter('vigence_from', "%{$_GET['date_from']}%");
                }
            }

            if(isset($_GET['date_to'])){
                if (null !== $_GET['date_to']) {
                    $queryBuilder->andWhere('SurpriseTargetStock.date_to = :vigence_to')->setParameter('vigence_to', "%{$_GET['date_to']}%");
                }
            }


        });

        return $grid->handle($request);
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $surpriseTarget = $em->getRepository('SurpriseTargetBundle:SurpriseTarget')->find($id);

        $form = $this->createForm(CreateItemSpotGoalType::class, $surpriseTarget);
        $form->handleRequest($request);
       // $slider->setFile(New SliderFile($slider->getFile()));

        if(!$surpriseTarget){
            throw $this->createNotFoundException('Objetivo spot no encontrado');
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $em->flush();

            $this->addFlash('success', 'El Objetivo spot ha sido modificado');

            return $this->redirectToRoute('aper_target_surprise_home');
        }

        return $this->render('SurpriseTargetBundle:Items:create.html.twig', array('surprise' => $surpriseTarget, 'form' => $form->createView()));
    }
    public function reporteConsolidadoAction(Request $request)
    {
        $config = new Configuration(
            $this->getDoctrine()->getRepository(SurpriseTargetStock::class)->getQueryBuilder(),
            $this->get('web_factory_grid.form_filter_factory')->create(ItemReporteConsolidadoFilterType::class),
            'main',
            'SurpriseTargetBundle:Items:reporteConsolidado.html.twig'
        );

        $em = $this->getDoctrine()->getManager();

        $grid = $this->get('web_factory_grid.factory')->create($config);

        $grid->addListener(ManipulateQueryEvent::NAME, function (ManipulateQueryEvent $event) {
            $queryBuilder = $event->getQueryBuilder();

            $filters = $event->getFilters();


        });

        $form       = $this->createForm(ItemReporteConsolidadoFilterType::class, null);
        $form->handleRequest($request);

        $export     = $form->get('export')->isClicked();
        $return     = $grid->handle($request);

        $f_id_objetivo = isset($_GET['id_objetivo']) ? $_GET['id_objetivo'] : null;
        $f_name = isset($_GET['name']) ? $_GET['name'] : null;
        $f_station_code = isset($_GET['station_code']) ? $_GET['station_code'] : null;


        if($export){
            $sql = "SELECT
            b.id as id_objetivo,
            a.id as id_asignacion,
            b.name as nombre_objetivo,
            a.station_code as codigo_estacion,
            a.date_from as fecha_desde,
            a.date_to as fecha_hasta,
            a.assigned_stock as stock_asignado_suc,
            COALESCE(SUM(c.assigned_stock),0) as stock_asignado_emp,
            0 as ventas_totales_emp
            FROM surprise_targets_station_stock a
            JOIN surprise_targets b on b.id = a.id_surprise_target
            LEFT JOIN surprise_targets_station_stock_users c on c.id_surprise_target_station_stock = a.id
            WHERE 1 = 1 ";

            if($f_id_objetivo){

                $sql .= " and b.id = $f_id_objetivo ";
            }

            if($f_name){

                $sql .= " and b.name like '%$f_name%' ";
            }

            if($f_station_code){

                $sql .= " and a.station_code = '$f_station_code' ";
            }


            $sql .= " GROUP BY a.id";

            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $dataExport = $stmt->fetchAll();

            for ($i = 0; $i < count($dataExport); $i++) {

                $sql = "SELECT SUM(stock_loaded) as stock_vendido FROM
                surprise_targets_station_stock_users a
                JOIN surprise_targets_station_stock_user_loaded b on a.id = b.id_surprise_targets_station_stock_users
                WHERE a.id_surprise_target_station_stock = ".$dataExport[$i]['id_asignacion'];

                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute();

                $exec = $stmt->fetchAll();

                $dataExport[$i]['ventas_totales_emp'] = $exec[0] ? $exec[0]['stock_vendido']: 0;
            }

            $excelExporter = $this->get('aper_surprise_target.services.excel_exporter_items_consolidado');
                return $excelExporter->getResponseFromItemsConsolidado(
                    $dataExport,
                    'ReporteConsolidadoItemsSpot.xls'
                );

        }

        return $return;

    }

    public function reporteDetalladoAction(Request $request)
    {
        $config = new Configuration(
            $this->getDoctrine()->getRepository(SurpriseTargetStock::class)->getQueryBuilder(),
            $this->get('web_factory_grid.form_filter_factory')->create(ItemReporteDetalladoFilterType::class),
            'main',
            'SurpriseTargetBundle:Items:reporteDetallado.html.twig'
        );

        $em = $this->getDoctrine()->getManager();
        $grid = $this->get('web_factory_grid.factory')->create($config);
        $grid->addListener(ManipulateQueryEvent::NAME, function (ManipulateQueryEvent $event) {
            $queryBuilder = $event->getQueryBuilder();
            $filters = $event->getFilters();
        });

        $form       = $this->createForm(ItemReporteDetalladoFilterType::class, null);
        $form->handleRequest($request);

        $export     = $form->get('export')->isClicked();
        $return     = $grid->handle($request);

        $f_id_objetivo = isset($_GET['id_objetivo']) ? $_GET['id_objetivo'] : null;
        $f_name = isset($_GET['name']) ? $_GET['name'] : null;
        $f_station_code = isset($_GET['station_code']) ? $_GET['station_code'] : null;


        if($export){
            $sql = 'SELECT
            b.id as id_objetivo,
            a.id as id_asignacion,
            b.name as nombre_objetivo,
            a.date_from as fecha_desde,
            a.date_to as fecha_hasta,
            a.station_code as codigo_estacion,
            d.created_at as fecha_carga,
            d.stock_loaded as cantidad_carga,
            e.email as email_employee,
            IF(d.rejected = 1, "x", "" ) as rechazado,
            IF(d.rejected != 1, IF(d.confirmed_by_manager = 1, "x", "") ,"") as aprobado
            FROM surprise_targets_station_stock a
            JOIN surprise_targets b on b.id = a.id_surprise_target
            LEFT JOIN surprise_targets_station_stock_users c on c.id_surprise_target_station_stock = a.id
            JOIN surprise_targets_station_stock_user_loaded d on d.id_surprise_targets_station_stock_users = c.id
            JOIN users e on e.id = c.id_user
            WHERE 1 = 1 ';

            if($f_id_objetivo){

                $sql .= " and b.id = $f_id_objetivo ";
            }

            if($f_name){

                $sql .= " and b.name like '%$f_name%' ";
            }

            if($f_station_code){

                $sql .= " and a.station_code = '$f_station_code' ";
            }


            $sql .= " GROUP BY d.id ORDER BY d.created_at DESC";

            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $dataExport = $stmt->fetchAll();

            $excelExporter = $this->get('aper_surprise_target.services.excel_exporter_items_detallado');
                return $excelExporter->getResponseFromItemsDetallado(
                    $dataExport,
                    'ReporteDetalladoItemsSpot.xls'
                );

        }

        return $return;
    }

    public function modifyStockAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // Buscamos las stores para el primer filtro
        $sql = "SELECT a.id, location, code FROM stores a
        JOIN surprise_targets_station_stock b on a.code = b.station_code
        GROUP BY a.id";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $stores = $stmt->fetchAll();

        return $this->render('SurpriseTargetBundle:Items:modifyStocks.html.twig', ["stores" => $stores]);
    }
}
