<?php

namespace Aper\TrainingKitBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Aper\TrainingKitBundle\Entity\Traits\NestedSet;

/**
 * @Gedmo\Tree(type="nested")
 *
 * @ORM\MappedSuperclass()
 */
abstract class AbstractFile
{
    use TimestampableEntity;
    use NestedSet;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var AbstractFile
     */
    protected $parent;

    /**
     * @var AbstractFile[]|Collection
     */
    protected $children;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=64)
     */
    protected $name;

    /**
     * AbstractFile constructor.
     */
    public function __construct()
    {
        $this->name = '';
        $this->children = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param AbstractFile|null $parent
     */
    public function setParent(AbstractFile $parent = null)
    {
        $this->parent = $parent;
    }

    /**
     * @return AbstractFile|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return AbstractFile[]|Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param Collection $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
        foreach ($children as $child) {
            $child->setParent($this);
        }
    }

    /**
     * @param bool $video
     */
    public function setVideo($video)
    {
        $this->video = $video;
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return !$this->children->isEmpty();
    }

    /**
     * @return bool
     */
    public function isRoot()
    {
        return $this->parent === null;
    }

    /**
     * @return bool
     */
    public function isLeaf()
    {
        return $this->children->isEmpty();
    }

    /**
     * @return bool
     */
    public function isFirstLevel()
    {
        return $this->getParent() && !$this->getParent()->getParent();
    }

    /**
     * @Assert\Callback()
     */
    public function validateName(ExecutionContextInterface $context)
    {
        if (empty($this->name)) {
            $context->buildViolation('La carpeta debe tener un nombre')
                ->atPath('name')
                ->addViolation()
            ;
        }
    }
}
