<?php

namespace Aper\TrainingKitBundle\Entity;

use Aper\CourseBundle\Entity\Course;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TrainingFolder.
 *
 *
 * @ORM\Table(name="training_folder")
 * @ORM\Entity(repositoryClass="Aper\TrainingKitBundle\Repository\TrainingFolderRepository")
 */
class TrainingFolder extends AbstractFile
{
    /**
     * @var TrainingFolder
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="TrainingFolder", inversedBy="children", cascade={"persist"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="TrainingFolder", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    protected $children;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $required;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $enabled;

    /**
     * @var Course[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Aper\CourseBundle\Entity\Course", mappedBy="trainingFolder", cascade={"all"})
     * @Assert\Valid()
     */
    protected $courseCollection;

    /**
     * AbstractSlide constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->required = false;
        $this->enabled = true;
    }

    /**
     * @return bool
     */
    public function isRequired()
    {
        return $this->required;
    }

    /**
     * @param bool $required
     * @return TrainingFolder
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * @return string
     */
    public function getIndentedName()
    {
        if (!$this->name) {
            return str_repeat('-', $this->lvl * 2).' '.'[No title]';
        }

        return str_repeat('-', $this->lvl * 2).' '.$this->name;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return TrainingFolder
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        if (!$enabled) {
            foreach ($this->getChildren() as $child) {
                $child->setEnabled($enabled);
            }
        }

        return $this;
    }

    /**
     * @return TrainingFolder
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        if ($this->name) {
            $name = ' > ' . $this->name;
        } else {
            $name = '';
        }

        $name = $this->parent ? $this->parent->getPath() . $name : $name;
        
        return preg_replace('/^ > /', '', $name);
    }

    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->lvl;
    }


    public function __toString()
    {
        return $this->getPath();
    }

    /**
     * @return Course[]|ArrayCollection
     */
    public function getCourseCollection()
    {
        return $this->courseCollection->filter(function (Course $course){ return $course->isActive(); });
    }

    /**
     * @return Course[]|ArrayCollection
     */
    public function getAllCourseCollection()
    {
        return $this->courseCollection;
    }

    /**
     * @param Course[]|ArrayCollection $courseCollection
     * @return TrainingFolder
     */
    public function setCourseCollection($courseCollection)
    {
        $this->courseCollection = $courseCollection;

        return $this;
    }

    /**
     * @param Course $course
     * @return TrainingFolder
     */
    public function addCourseCollection(Course $course)
    {
        $this->courseCollection->add($course);

        return $this;
    }

    /**
     * @param Course $course
     * @return TrainingFolder
     */
    public function removeCourse(Course $course)
    {
        $this->courseCollection->removeElement($course);

        return $this;
    }

    /**
     * @param TrainingFolder $child
     * @return $this
     */
    public function removeChild(TrainingFolder $child)
    {
        $this->children->removeElement($child);

        return $this;
    }


}