<?php

namespace Aper\TrainingKitBundle\Service;

use Aper\TrainingKitBundle\Entity\TrainingFolder;

class TrainingFolderParser
{
    /**
     * @param TrainingFolder[] $slides
     *
     * @return array
     */
    public function toArray($slides)
    {
        $trainingFolders = [];
        foreach ($slides as $trainingFolder) {
            foreach ($trainingFolder->getResources() as $resource) {
                $previewImages = [];
                foreach ($resource->getPreviewImages() as $previewImage) {
                    if (!$previewImage) {
                        continue;
                    }
                    $previewImages[] = [
                        $previewImage->getWebPath(),
                    ];
                }
            }

            $trainingFolderItem = [
                'id' => $trainingFolder->getId(),
                'level' => $trainingFolder->getLvl(),
                'title' => $trainingFolder->getName(),
                'children' => [],
                'parent' => $trainingFolder->getParent() ? $trainingFolder->getParent()->getId() : null,
                'required' => $trainingFolder->isRequired(),
                'order' => $trainingFolder->getLft(),
                'new' => ($trainingFolder->checkStatusDate() === TrainingFolder::IS_NEW),
                'updated' => ($trainingFolder->checkStatusDate() === TrainingFolder::UPDATED),
                'enabled' => $trainingFolder->isEnabled(),
                'path' => $trainingFolder->getPath(),
            ];

            if ($trainingFolder->hasChildren()) {
                $trainingFolderItem['children'] = $this->toArray($trainingFolder->getChildren());
            }
            if ($trainingFolder->getLvl() === 2) {
                $trainingFolderItem['blankPages'] = 0;
            }

            $trainingFolders[] = $trainingFolderItem;
        }

        return $baseSlides;
    }
}
