<?php

namespace Aper\TrainingKitBundle\Manager;

use Aper\TrainingKitBundle\Entity\TrainingFolder;
use Aper\TrainingKitBundle\Repository\TrainingFolderRepository;

class TrainingFolderManager implements TrainingFolderManagerInterface
{
    /**
     * @var TrainingFolderRepository
     */
    private $trainingFolderRepository;

    /**
     * @inheritdoc
     */
    public function save(TrainingFolder $trainingFolder)
    {
        $this->trainingFolderRepository->save($trainingFolder);
    }
}