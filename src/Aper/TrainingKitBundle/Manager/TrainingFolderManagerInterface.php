<?php

namespace Aper\TrainingKitBundle\Manager;

use Aper\TrainingKitBundle\Entity\TrainingFolder;

/**
 * Interface TrainingFolderManagerInterface
 * @package Aper\TrainingKitBundle\Manager
 */
interface TrainingFolderManagerInterface
{
    /**
     * @param TrainingFolder $trainingFolder
     */
    public function save(TrainingFolder $trainingFolder);
}