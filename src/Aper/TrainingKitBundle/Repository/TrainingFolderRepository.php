<?php

namespace Aper\TrainingKitBundle\Repository;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Aper\TrainingKitBundle\Entity\TrainingFolder;

/**
 * Class TrainingFolderRepository.
 */
class TrainingFolderRepository extends NestedTreeRepository
{
    /**
     * @param bool $returnQueryBuilder
     *
     * @return array|QueryBuilder
     */
    public function findAllOrderedByRootAndLft($returnQueryBuilder = false)
    {
        $qb = $this->createQueryBuilder('node')
            ->orderBy('node.root', 'ASC')
            ->addOrderBy('node.lft', 'ASC')
        ;

        if ($returnQueryBuilder) {
            return $qb;
        }

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @return Collection
     */
    public function findAllRootNodes()
    {
        $qb = $this->findAllOrderedByRootAndLft(true);
        $qb->where('node.lvl = 0');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Collection
     */
    public function findAllTreeNodes()
    {
        $qb = $this->findAllOrderedByRootAndLft(true);
        $qb->where('node.lvl = 0');

        $nodes = $qb->getQuery()->getResult();

        $trainingFolders = [];
        foreach ($nodes as $trainingFolder) {
            $trainingFolderItem = [
                'id' => $trainingFolder->getId(),
                'level' => $trainingFolder->getLvl(),
                'title' => $trainingFolder->getName(),
                'children' => [],
                'parent' => $trainingFolder->getParent() ? $trainingFolder->getParent()->getId() : null,
                'required' => $trainingFolder->isRequired(),
                'order' => $trainingFolder->getLft(),
                'enabled' => $trainingFolder->isEnabled(),
                'path' => $trainingFolder->getPath(),
            ];

            if ($trainingFolder->hasChildren()) {
                $trainingFolderItem['children'] = $trainingFolder->getChildren();
            }

            $trainingFolders[] = $trainingFolderItem;
        }

        return $trainingFolders;
    }

    /**
     * @param TrainingFolder $trainingFolder
     */
    public function save(TrainingFolder $trainingFolder)
    {
        $this->_em->persist($trainingFolder);
        $this->_em->flush();
    }

    /**
     * @param TrainingFolder $trainingFolder
     */
    public function remove(TrainingFolder $trainingFolder)
    {
        $this->_em->remove($trainingFolder);
        $this->_em->flush();
    }

    /**
     * @param TrainingFolder $node
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createQueryBuilderExcludingNode(TrainingFolder $node)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->where($qb->expr()->notIn(
            'c.id',
            $this
            ->createQueryBuilder('n')
            ->where('n.root = '.$node->getRoot())
            ->andWhere($qb->expr()->between('n.lft', $node->getLft(), $node->getRgt()))
            ->getDQL()
        ))->orderBy('c.root', 'ASC')->addOrderBy('c.lft', 'ASC');

        return $qb;
    }

    /**
     * @return TrainingFolder[]
     */
    public function findAllWithPath()
    {
        $qb = $this->createQueryBuilder('node')
            ->orderBy('node.root', 'ASC')
            ->addOrderBy('node.lft', 'ASC')
        ;

        $qb->where('node.path is not null');

        return $qb->getQuery()->getResult();
    }


    public function getFilesOnNodeForRole(TrainingFolder $node=null, string $role=null)
    {
        // SELECT trainingFolder_id, COUNT(*)
        //   FROM course
        //  WHERE course.roles_allowed LIKE '%ROLE_ADMIN%'
        //   GROUP BY 1
    }

}