<?php

namespace Aper\TrainingKitBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Aper\TrainingKitBundle\Entity\TrainingFolder;
use Aper\TrainingKitBundle\Repository\TrainingFolderRepository;

/**
 * Class TrainingFolderType.
 */
class TrainingFolderType extends AbstractType
{
    /**
     * @var ChoiceLoaderInterface
     */
    private $choiceLoader;

    /**
     * TrainingFolderType constructor.
     *
     * @param ChoiceLoaderInterface $choiceLoader
     */
    public function __construct(ChoiceLoaderInterface $choiceLoader = null)
    {
        $this->choiceLoader = $choiceLoader;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'base_slide_type';
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $options = [
            'class' => 'TrainingKitBundle:TrainingFolder',
            'empty_value' => '',
            'required' => false,
            'property' => 'indentedName',
            'query_builder' => function (TrainingFolderRepository $r) {
                return $r->findAllOrderedByRootAndLft(true);
            },
            'choice_attr' => function(TrainingFolder $val, $key, $index) {
                if ($val->isEnabled()) {
                    return [];
                }

                return ['class' => 'disabled', 'disabled' => 'disabled'];
            },
        ];
        if ($this->choiceLoader instanceof ChoiceLoaderInterface) {
            $options['choice_loader'] = $this->choiceLoader;
        }
        $builder->add('parent', 'entity', $options);

        $nameField = $builder->create('name', 'text', ['required' => false,])->addViewTransformer(new CallbackTransformer('trim', 'trim'));

        $builder->add($nameField)
                ->add('required', 'checkbox');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Aper\TrainingKitBundle\Entity\TrainingFolder',
            'attr' => ['novalidate' => 'novalidate'],
        ]);
    }
}
