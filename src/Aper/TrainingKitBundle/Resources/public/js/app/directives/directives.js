(function() {
    'use strict';

    angular.module('app.directives', [])
        .directive('getValue', [function () {
            return {
                require: '?ngModel',
                restrict: 'A',
                scope: {
                    value: '@'
                },
                link: function postLink(scope, elem, attrs, ctrl) {
                    ctrl.$setViewValue(scope.value, 'getValue');
                }
            }
        }])

})();