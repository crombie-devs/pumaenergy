(function () {
    'use strict';

    function blanks(helpers) {
        var _this = this;

        this.add = function (collection, node) {
            var nodeCopy = helpers.findNode(collection, node);

            if (_this.validate(nodeCopy)) {
                //si no existe blankPages, la inicializo, sino sumo uno
                if (!nodeCopy.blankPages)
                    nodeCopy.blankPages = 1;
                else
                    nodeCopy.blankPages++;
            }
        };

        this.aggregate = function (collection, node) {
            // Si el slide existe y tiene mas de un blanco, pongo en cero los blancos y el slide queda
            var nodeCopy = helpers.findNode(collection, node);
            if (nodeCopy.blankPages && nodeCopy.blankPages > 0) {
                nodeCopy.blankPages = 0;
            } else {
                // Si el slide existe, pero no tiene blancos es porque quiere agregarlos
                nodeCopy.blankPages = 1;
            }
        };

        this.remove = function (collection, node) {
            var nodeCopy = helpers.findNode(collection, node);
            if (!nodeCopy || angular.isUndefined(nodeCopy.blankPages) || nodeCopy.blankPages == 0) {
                return;
            }

            nodeCopy.blankPages--;
        };

        this.count = function (collection, node) {
            var nodeCopy = helpers.findNode(collection, node);
            if (nodeCopy && 'blankPages' in nodeCopy)
                return nodeCopy.blankPages;
            else
                return 0;
        };

        this.have = function (collection, node) {
            var nodeCopy = helpers.findNode(collection, node);

            if (nodeCopy && 'blankPages' in nodeCopy) {
                return nodeCopy.blankPages > 0;
            }
        }

        this.validate = function (node) {
            return (!node.blankPages || node.blankPages < 10);
        }
    }

    function slides(orderByFilter, helpers) {
        var _this = this;

        this.add = function (collection, node, parent, blank) {
            var newNode = angular.copy(node),
                exist = helpers.exist(collection, newNode);

            if (exist) {
                //Existe nodo

                // Eliminamos el elemento
                _this.remove(collection, newNode);

                //si no es de nivel 4, revisa si tiene hijos para borrar padre
                if (newNode.level !== 3) {

                    // Variable que indica si quedan o no marcados hijos
                    var have_child = false;

                    // Por cada hijo del padre, verificamos si hay alguno que esté marcado.
                    angular.forEach(parent.children, function (child, k) {
                        var exist_child = helpers.exist(collection, child);
                        if (exist_child) {
                            // Si uno está marcado, se indica que aún tiene un hijo
                            have_child = true;
                        }
                    });

                    // Si no hay ningún hijo marcado, se elimina el nodo del padre, porque sino genera error.
                    if (!have_child) {
                        var parentParentNode = helpers.findParent(collection, parent);
                        if (parentParentNode != null) {
                            _this.remove(collection, parent);
                        }
                    }
                }

            } else {

                //en caso de ser de 3er nivel, resetea paginas en blanco e inicializa hijos en vacio
                if (newNode.level == 2) {
                    newNode.blankPages = 0;
                    newNode.children = new Array;
                }

                //en caso de haber presionado agregar slide blanco
                if (blank)
                    newNode.blankPages = 1;

                if (!helpers.exist(collection, parent)) {
                    //No existe nodo - no existe padre

                    // Limpio el parent node para dejar solamente el node que estoy agregando
                    parent.children = new Array;
                    parent.children.push(newNode);
                    parent.children = orderByFilter(parent.children, 'order', false);

                    helpers.push(collection, parent);
                } else {
                    //No existe nodo - existe padre, se fija limite

                    var countParentNode = helpers.findNode(collection, parent);

                    //revisa si es de 4to nivel --> 5 items como maximo
                    if (newNode.level == 3 && countParentNode && countParentNode.children.length >= 5) {
                        alert('You have reached the maximum of five items for this section');
                        return;
                    } else {
                        //revisa si es de 3er nivel --> 10 items como maximo
                        if (newNode.hasOwnProperty('children') && newNode.children.length >= 10) {
                            alert('You have reached the maximum of ten items for this section');
                            return;
                        } else {

                            if (countParentNode && countParentNode.children.length >= 10) {
                                alert('You have reached the maximum of ten items for this section');
                                return;
                            }
                        }
                    }

                    helpers.push(collection, newNode);
                }

            }
        };

        this.remove = function (collection, node) {
            helpers.splice(collection, node);
        }
    }

    angular.module('slide.manager', ['helper.component'])
        .service('blanks', blanks)
        .service('slides', slides)
})();