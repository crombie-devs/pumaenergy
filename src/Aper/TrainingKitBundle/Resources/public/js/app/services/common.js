(function () {
    'use strict';

    function helpers(orderByFilter) {
        var _this = this;

        /**
         * Chequea existencia
         */
        this.exist = function (collection, item) {
            var exists = false;
            angular.forEach(collection, function (slide, key) {
                if (!exists) {
                    if (slide.id === item.id) {
                        exists = true;
                    } else {
                        exists = _this.exist(slide.children, item);
                    }
                }
            });

            return exists;
        };

        /**
         * Ecuentra el padre de un valor en una coleccion
         */
        this.findParent = function (collection, item) {
            var node = null;
            angular.forEach(collection, function (slide, key) {
                if (!node) {
                    if (slide.id === item.parent) {
                        node = angular.copy(slide);
                    } else {
                        node = _this.findParent(slide.children, item);
                    }
                }
            });

            return node;
        };

        /**
         * Ecuentra el ID de un nodo dentro de una colección y retorna el nodo completo
         */
        this.findNodeById = function (collection, id) {
            var node = null;
            angular.forEach(collection, function (slide, key) {
                if (!node) {
                    if (slide.id === id) {
                        node = slide;
                    } else {
                        node = _this.findNodeById(slide.children, id);
                    }
                }
            });

            return node;
        };

        /**
         * Ecuentra el nodo dentro de una colección
         */
        this.findNode = function (collection, item) {
            var node = null;
            angular.forEach(collection, function (slide, key) {
                if (!node) {
                    if (slide.id === item.id) {
                        node = slide;
                    } else {
                        node = _this.findNode(slide.children, item);
                    }
                }
            });

            return node;
        };

        /**
         * Pushea un valor a una coleccion
         */
        this.push = function (collection, item) {
            angular.forEach(collection, function (slide) {
                if (slide.id === item.parent) {
                    slide.children.push(item);
                    // Reordena para no perder el orden original
                    slide.children = orderByFilter(slide.children, 'order', false);
                } else {
                    _this.push(slide.children, item);
                }
            })
        };


        this.splice = function (collection, node) {
            angular.forEach(collection, function (child, k) {
                if (child.id === node.id) {
                    collection.splice(k, 1);
                } else {
                    _this.splice(child.children, node);
                }
            });
        }
    }

    angular.module('helper.component', [])
        .service('helpers', helpers)
})();