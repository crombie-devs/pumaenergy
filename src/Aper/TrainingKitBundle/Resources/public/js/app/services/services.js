(function(){
    'use strict';

    function initModel(SlidesProvider, helpers) {
        var disabledList = [];
        this.getDisabledList = function () {
            return disabledList;
        };
        this.execute = function(vm) {

            var nodeIsRequired = function (node) {
                var $isRequired = false;
                if (node.children.length == 0 && !node.required) {
                    return $isRequired;
                }

                if (node.required) {
                    $isRequired = true;
                } else {
                    angular.forEach(node.children, function (child) {
                        if (child.required && !$isRequired) {
                            $isRequired = true;
                        }

                        if (child.children.length > 0) {
                            angular.forEach(child.children, function (child2) {
                                if (child2.required) {
                                    $isRequired = true;
                                }
                            })
                        }
                    });
                }

                return $isRequired;
            };

            var generateDisabledList = function (collection, userSlides) {
                angular.forEach(collection, function (slide, i) {
                    if (!slide.enabled && helpers.findNode(userSlides, slide)) {
                        disabledList.push(slide);
                    }
                    if (slide.children.length) {
                        generateDisabledList(slide.children, userSlides);
                    }
                });
            };


            var generateDisabledListPlease = function (collection) {
                angular.forEach(collection, function (slide, i) {
                    if (!slide.enabled) {
                        collection.splice(i, 1);
                    }
                    if (slide.children.length) {
                        generateDisabledListPlease(slide.children);
                    }
                });
            };

            SlidesProvider.getNodes().then(function (response) {

                vm.listParent = response.data.baseSlides;
                vm.showTree(vm.listParent[0]);

                if (vm.data.presentation_baseSlides) {
                    //pantalla editar
                    vm.nodesMap = JSON.parse(vm.data.presentation_baseSlides);
                    generateDisabledList(vm.listParent, vm.nodesMap);
                    generateDisabledListPlease(vm.listParent);
                    if (disabledList.length) {
                        // var niceStringLines = [];
                        // for (var i in disabledList) {
                        //     niceStringLines.push(disabledList[i].title);
                        // }
                        // alert('These are deleted:<br><br>' + niceStringLines.join(', '));
                    }

                    vm.data.presentation_baseSlides = JSON.stringify(vm.nodesMap);

                } else {
                    //pantalla nuevo
                    angular.forEach(vm.listParent, function (slide) {
                        var newSlide = angular.copy(slide);
                        var childrenCount = newSlide.children.length;

                        // Si no tiene padre y ninguno de sus hijos es requerido limpio sus hijos para agregarlo
                        if (!newSlide.parent && !nodeIsRequired(newSlide)) {
                            newSlide.children.splice(0, childrenCount);
                        } else {
                            // No es requerido itero por todo los hijos y busco los requeridos
                            // Si es requerido tengo que buscar todos los hijos y quitar los que no lo sean
                            angular.forEach(newSlide.children, function (child, key) {
                                if (!nodeIsRequired(child)) {
                                    newSlide.children.splice(0, childrenCount);
                                } else {
                                    child.children = newSlide.children[key].children.filter(function (child) {
                                        return child.required;
                                    });
                                }
                            })
                        }

                        vm.nodesMap.push(newSlide);
                    });

                    vm.data.presentation_baseSlides = JSON.stringify(vm.nodesMap);
                }
            });
        }
    }

    function validator() {
        this.stepIsValid = function (step, vm, presentation) {
            switch (step) {
                case 0:
                    var path = validatePath,
                        data = {
                            title: vm.data['presentation_title'],
                            client: vm.data['presentation_clientName'],
                            date: vm.data['presentation_date']
                        },
                        isValid = false, valido = false;

                    if (presentationIsDefined) {
                        data['presentation'] = presentationId;
                    }

                    $.ajax({
                        url: path,
                        type: 'post',
                        async: false,
                        data: data
                    }).done(function (r) {
                        isValid = r['valid'];
                        if (!isValid) {
                            alert('A presentation already exists with that name. Please use a different name.');
                        }

                        valido = isValid && presentation['presentation[title]'].$valid &&
                            presentation['presentation[clientName]'].$valid &&
                            presentation['presentation[date]'].$valid &&
                            presentation['presentation[consultantName]'].$valid &&
                            presentation['presentation[prospectLogo][file]'].$valid &&
                            presentation['presentation[lineOfBusiness]'].$valid;
                    });

                    return valido;
                case 1:
                    return true;

                case 2:
                    var valid = false;
                    angular.forEach(vm.nodesMap, function (node, k) {
                        if (node.children.length > 0) {
                            valid = true;
                        }
                    });

                    return valid;
            }
        }
    }

    function preview(SlidesProvider) {
        this.show = function(slides, theme, cover) {
            SlidesProvider.getPreview(slides, theme, cover);
        }
    }

    angular.module('app.services', ['app.factory', 'helper.component'])
        .service('initModel', initModel)
        .service('validator', validator)
        .service('preview', preview)
})();