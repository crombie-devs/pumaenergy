(function(){
    'use strict';

    angular.module('app.factory', [])
        .factory('SlidesProvider', ['$http',
            function ($http) {
                return {
                    getNodes: function () {
                        return $http({
                            method: 'GET',
                            url: getNodesPath
                        });
                    },
                    getPreview: function (nodes, theme, cover) {
                        $.post(getPreviewPath + "?theme=" + theme, {
                            'baseSlides': nodes,
                            'presentationCover': cover
                        }).success(function (r) {

                            $('.tab-pane').removeClass('active');
                            $('#preview').html(r).addClass('active').css({
                                'z-index': 5000,
                                'position': 'relative'
                            });
                            show_overlay();
                        })
                    },
                    getCovers: function(){
                        return $http({
                            method: 'GET',
                            url: getCovers
                    });
                    }
                }
            }
        ]);
})();