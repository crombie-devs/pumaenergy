(function() {
    'use strict';

    angular.module('app', ['ngFileUpload', 'ngImgCrop', 'app.controllers', 'app.directives', 'app.services'])
        .config(function ($httpProvider){
            $httpProvider.interceptors.push('redirectInterceptor');
        })
        .factory('redirectInterceptor', ['$location', '$q', function($location, $q) {
            return {
                'request': function(config){
                    // console.log(config)
                    return config;
                },

                'requestError': function(rejection){
                    console.log(rejection);
                    return $q.reject(rejection);
                },

                'response': function(response){
                    console.log(response);

                    if (typeof response.data === 'string') {
                        if (response.data.indexOf instanceof Function &&
                            response.data.indexOf('name="_username"') != -1) {

                            // console.log(response);ç

                            $(window).unbind('beforeunload');

                            // $location.path("/logout");
                            window.location = logoutPath;

                            return $q.reject(response);
                        }
                    }
                    return response;
                },

                'responseError': function(rejection){
                    console.log(rejection);
                    // if (rejection.cause == [401, 403]) {
                    //     location-.
                    // }

                    return $q.reject(rejection);
                }
            };
        }]);
})();