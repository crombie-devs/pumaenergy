(function () {
    'use strict';

    function WizardController($http, $scope, initModel, validator, preview, helpers, blanks, slides, Upload, $timeout, SlidesProvider, $sce) {
        var vm = this;
        vm.step = 0;
        vm.currentBlock = 0;
        vm.data = {}; // model que maneja el form
        vm.listParent = {}; // Listado de slides padres
        vm.children = {};
        vm.nodesMap = new Array; // Listado de slides agregados
        vm.isForward = false;

        vm.myImage = '';
        vm.myCroppedImage = '';
        vm.data.presentation_croppedProspectLogo_coordinate = {};
        vm.imgChanged = false;
        vm.disabledListMessageShown = false;
        vm.searchResults = '';

        vm.currentPreviewHTML = '';

        //Search vars
        vm.stringSearched = ''
        $scope.search = {string: ''}
        vm.currentPreviewSlide = '';
        vm.disabledBtnInPreview = false;

        //Search pagination init vars
        vm.curPage = 0;
        vm.pageSize = 20;
        vm.totalSearchPages = 0;
        vm.countResults = 0;
        vm.searchResultsPaged = [];
        vm.totalSearch = [];

        SlidesProvider.getCovers().then(function (covers) {
            vm.data.covers = covers.data;

            //setea activo el primero por defecto
            vm.data.presentation_cover = vm.data.covers[0].id;
        });


        //watch para add to my presentation (prettyPhoto que esta con jQuery, asigna variable como string)
        $scope.$watch('vm.data.presentation_cover', function () {
            if (vm.data.presentation_cover) {
                vm.data.presentation_cover = parseInt(vm.data.presentation_cover);
            }
        })

        vm.applyCoords = function () {
            vm.data.presentation_croppedProspectLogo_file = $scope.myCroppedImage;
            vm.data.presentation_croppedProspectLogo_coordinate_x = $scope.myAreaCoords.x;
            vm.data.presentation_croppedProspectLogo_coordinate_y = $scope.myAreaCoords.y;
            vm.data.presentation_croppedProspectLogo_coordinate_w = $scope.myAreaCoords.w;
            vm.data.presentation_croppedProspectLogo_coordinate_h = $scope.myAreaCoords.h;

            $('form').addClass('dirty');
        };

        vm.crop100Percent = function ($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event) {
            // Este evento se dispara cuando muestra el selector de archivos
            // Aplicamos, solo cuando se selecciona efectivamente un archivo
            if ($file) {
                Upload.base64DataUrl($file).then(function (url) {
                    vm.data.presentation_croppedProspectLogo_file = url;
                    vm.imgChanged = true;
                });
                Upload.imageDimensions($file).then(function (dimensions) {
                    vm.data.presentation_croppedProspectLogo_coordinate_x = 0;
                    vm.data.presentation_croppedProspectLogo_coordinate_y = 0;
                    vm.data.presentation_croppedProspectLogo_coordinate_w = dimensions.width;
                    vm.data.presentation_croppedProspectLogo_coordinate_h = dimensions.height;
                });

                $('#presentation_prospectLogo_removeFile').prop("checked", false);
                $('#presentation_croppedProspectLogo_remove_file').prop("checked", false);
            }
        };

        vm.updateCropped = function () {

            if (vm.imgChanged) {
                $timeout(function () {
                    $scope.myImage = vm.data.presentation_prospectLogo_file;
                }, 50)
            } else {
                Upload.urlToBlob(vm.data.presentation_prospectLogo_file).then(function (blob) {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        $timeout(function () {
                            $scope.myImage = event.target.result;
                        }, 50)

                    }; // data url!
                    reader.readAsDataURL(blob);
                });
            }

            $scope.initCoords = {};
            $scope.sizeCoords = {};
            $scope.initCoords.x = vm.data.presentation_croppedProspectLogo_coordinate_x;
            $scope.initCoords.y = vm.data.presentation_croppedProspectLogo_coordinate_y;
            $scope.sizeCoords.w = vm.data.presentation_croppedProspectLogo_coordinate_w;
            $scope.sizeCoords.h = vm.data.presentation_croppedProspectLogo_coordinate_h;
        };

        initModel.execute(vm);

        //cambia de pantalla (boton preview - next)
        vm.moveBlock = function (blockNumber) {
            vm.currentBlock += blockNumber;
            vm.showTree(vm.listParent[vm.currentBlock]);
        };

        vm.haveBlanks = function (node) {
            return blanks.have(vm.nodesMap, node);
        };

        vm.haveChild = function (node) {
            var nodeCopy = helpers.findNode(vm.nodesMap, node);
            return nodeCopy && nodeCopy.children && nodeCopy.children.length > 0;
        };

        vm.getBlanksCount = function (node) {
            return blanks.count(vm.nodesMap, node);
        };

        vm.removeBlankPage = function (node) {
            blanks.remove(vm.nodesMap, node);

            vm.data.presentation_baseSlides = JSON.stringify(vm.nodesMap);
        };

        vm.addBlankPage = function (node) {
            if (!helpers.exist(vm.nodesMap, node))
                vm.addSlide(node, true);
            else
                blanks.add(vm.nodesMap, node);

            vm.data.presentation_baseSlides = JSON.stringify(vm.nodesMap);
        };

        vm.agregateBlanks = function (node) {
            if (!helpers.exist(vm.nodesMap, node)) {
                // Si el slide no existe en el array lo agrego indicando que es el primero
                vm.addSlide(node, true);
            } else {
                blanks.aggregate(vm.nodesMap, node);
                vm.data.presentation_baseSlides = JSON.stringify(vm.nodesMap);
            }
        };

        vm.canChange = function (node) {
            return node.blankPages >= 10;
        };

        vm.showTree = function (slides) {
            vm.children = {};
            vm.children = slides.children;
        };

        vm.submit = function (event) {
            // Horfix para presentaciones anteriores a agregagar la validación de maximo de slides por seccion
            angular.forEach(vm.nodesMap, function (node, k) {
                angular.forEach(node.children, function (child, key) {
                    var number = 10;
                    if (child.children.length > number) {
                        alert('You have reached the maximum of ' + number + ' items for the section ' + child.title);
                        event.preventDefault();
                    }
                });
            });

            if (!(vm.validateStep(0) && vm.validateStep(1) && vm.validateStep(2))) {
                event.preventDefault();
            }
        };

        /**
         * Chequea si es al step actual
         */
        vm.isCurrentStep = function (step) {
            return vm.step === step;
        };

        /**
         * Setea el step actual
         */

        vm.setCurrentStep = function (step) {

            $('#search-results').removeClass('active');
            $('form > .form-group').show();

            if (vm.step == 0) {
                if (!vm.validateStep(0)) {
                    vm.forward();
                    return;
                }
            }

            vm.step = step;

            if (vm.step === 2) {
                $('.tab-pane#last').addClass('active').show();
                vm.showDisabledListMessage();
            }

        };

        vm.showDisabledListMessage = function () {
            var disabledList = initModel.getDisabledList();
            if (!vm.disabledListMessageShown && disabledList.length > 0) {
                var niceStringLines = [];
                var texto = '';
                for (var i in disabledList) {
                    texto += '<li><strong>' + disabledList[i].path + '</strong></li>';
                }

                var html2 = '<p style="font-size: 16px; line-height: 18px;">The following slide or slides have been removed from the tool by an Administrator:</p><ul>' +
                    texto +
                    '</ul><p>When you rebuild your presentation, this content will no longer appear in the presentation.</p>';

                vm.currentPreviewHTML = html2

                // jQuery('.modal-body').html(html2);
                jQuery('.modal-body').parent().parent().parent().addClass('release ');
                //jQuery('.modal').css('max-height', 500 + 'px');
                jQuery('.modal').modal({
                    backdrop: 'true',
                    show: 'true'
                });
                jQuery('.modal-backdrop').eq(1).remove();

                vm.disabledListMessageShown = true;
            }
        };

        /**
         * Devuelve el step actual
         */
        vm.getCurrentStep = function () {
            return vm.step;
        };

        /**
         * Incrementa vm.step
         */
        vm.forward = function () {
            if (vm.validateStep(vm.step)) {
                vm.step++;
                vm.isForward = false;
                if (vm.step === 2) {
                    vm.showDisabledListMessage();
                }
            } else {
                vm.isForward = true;
            }
            window.scrollTo(0, 0);
        };

        /**
         * Decrementa vm.step
         */
        vm.backward = function () {
            vm.step--;
        };

        /**
         * Agrega un slide
         */
        vm.addSlide = function (node, blank) {

            if (node.required) {
                return;
            }

            var parent = helpers.findParent(vm.listParent, node);

            /*
                revisa si el nodo es de 4to nivel y no existe el padre en el arbol de nodos --> agrega primero
                el padre y despues el hijo seleccionado en el arbol de nodos
             */
            if (node.level === 3 && !helpers.exist(vm.nodesMap, parent)) {
                var parentParentNode = helpers.findParent(vm.listParent, parent);
                if (!parentParentNode)
                    return;

                //agrega el padre y despues el hijo
                slides.add(vm.nodesMap, parent, parentParentNode);
                slides.add(vm.nodesMap, node, parent);
            } else
                slides.add(vm.nodesMap, node, parent, blank);

            vm.data.presentation_baseSlides = JSON.stringify(vm.nodesMap);
        };

        /**
         * Elimina un slide
         */
        vm.removeSlide = function (node) {
            try {
                if (node.required) {
                    return;
                }
            } catch (err) {
            }

            vm.addOrRemove = 'remove';
            if (helpers.findNode(vm.nodesMap, vm.currentPreviewSlide) === null) {
                vm.addOrRemove = 'add';
            }

            vm.data.presentation_baseSlides = JSON.stringify(vm.nodesMap);
        };

        /**
         * Chequea existencia de un nodo
         */
        vm.exists = function (node) {
            return helpers.exist(vm.nodesMap, node);
        };

        /**
         * Mostrar modal de detalles
         */
        vm.showDetail = function (event, id) {
            event.preventDefault();
            event = event;
            $('.modal.fade.release').removeClass('release');
            var path = slideShowPath;
            path = path.replace('_ID_', id);

            vm.currentPreviewSlide = helpers.findNodeById(vm.listParent, id);
            $(event.target).button('loading')
            $http.get(path + "?theme=" + vm.data.presentation_theme).then(function (response) {
                // console.log(response)
                vm.currentPreviewHTML = $sce.trustAsHtml(response.data);
                // $('.modal-body').html(response.data);
                $(event.target).button('reset');
                vm.addOrRemove = 'remove';
                if (vm.currentPreviewSlide.required === true || vm.currentPreviewSlide.level < 2) {
                    vm.disabledBtnInPreview = true;
                } else {
                    vm.disabledBtnInPreview = false;
                }


                if (helpers.findNode(vm.nodesMap, vm.currentPreviewSlide) === null) {
                    vm.addOrRemove = 'add';
                }


                //jQuery('.modal').modal('show');
                jQuery('.modal').modal({
                    backdrop: 'true',
                    show: 'true'
                });
                jQuery('.modal-backdrop').eq(1).remove();
            });
        };

        vm.validateStep = function (step) {
            return validator.stepIsValid(step, vm, $scope.presentation);
        };

        vm.showPreview = function () {
            preview.show(vm.nodesMap, vm.data.presentation_theme, vm.data.presentation_cover);

            $('.previous').hide();
        }

        vm.searchSlide = function (theme, string) {
            var _url = searchPath;
            $('.searchForm button').button('loading')
            if (string !== undefined) {
                string = encodeURI(string);
                $http.get(_url + "?theme=" + theme + "&terms=" + string + '&limit=999').then(function (response) {
                    vm.showSearchContainer(response.data);
                    $('.searchForm button').button('reset');
                });
            }
            return false;
        }

        vm.checkInSearch = function (data) {
            var newData = []
            angular.forEach(data, function (v, i) {
                var currentSelectedSlides = vm.nodesMap;
                var availableSlides = vm.listParent;

                // Completamos el resultado de busqueda con el nodo correspondiente.
                v.node = helpers.findNode(availableSlides, v);

                // Aquí sabremos si existe el Slide (node) en nuestra colección (Coleccióm de slides)
                if (helpers.exist(currentSelectedSlides, v)) {
                    // console.log('Estoy en la colección: ' + v.id);
                    v.exists = true;
                    // Completamos el resultado de busqueda con el nodo correspondiente.
                    // v.node = helpers.findNode(vm.nodesMap, v)
                } else {
                    v.exists = false;
                    // v.node = helpers.findNode(vm.listParent, v)
                }
                newData.push(v);
            });
            return newData;
        }

        vm.showSearchContainer = function (data) {
            vm.countResults = data['count'];
            data = data.base_slides;
            vm.totalSearch = data;
            vm.totalSearchPages = Math.ceil(vm.countResults / 20);
            vm.curPage = 1;

            vm.searchResultsPaged = vm.totalSearch.slice(0 * vm.pageSize, vm.curPage * vm.pageSize);
            vm.searchResults = vm.checkInSearch(vm.searchResultsPaged);


            //vm.searchResultsPaged = vm.checkInSearch(searchResults.slice(vm.curPage * vm.pageSize - vm.curPage,vm.totalSearchPages - 1));
            // vm.searchResults = vm.checkInSearch(vm.searchResultsPaged);

            vm.stringSearched = $scope.search.string

            //console.log(data);
            $('.tab-content .tab-pane').removeClass('active')
            $('.tab-content #search-results').addClass('active')
            $('form > .form-group').hide();
        }

        vm.paginationAction = function (page) {
            vm.curPage = page;
            vm.searchResultsPaged = vm.totalSearch.slice(parseInt(page - 1) * parseInt(vm.pageSize), parseInt(page) * parseInt(vm.pageSize));
            vm.searchResults = vm.checkInSearch(vm.searchResultsPaged);
        }

        vm.backInSearch = function () {
            $('.tab-content .tab-pane').removeClass('active');
            $('.tab-content #search-results .results').empty();
            $('.tab-content #last').addClass('active');
            $('form > .form-group').show();
        }
    }

    angular.module('app.controllers', ['app.directives', 'app.services', 'slide.manager', 'helper.component', 'ngSanitize', 'bw.paging'])
        .controller('WizardController', WizardController)

        .filter('relativePath', function () {
            return function (covers, theme) {

                for (var c in covers) {
                    //busca el preview del tema elegido
                    if (covers[c].theme.id == theme) {
                        //verifica si existe preview, sino retorna el tema por defecto
                        if (covers[c].preview.web_path) {
                            return uploadPath + covers[c].preview.web_path;
                        } else {
                            return uploadPath + covers[0].preview.web_path;
                        }
                    }
                }
                return uploadPath + covers[0].preview.web_path;
            }
        })

        .filter('pagination', function () {
            return function (input, start) {
                start = +start;
                return input.slice(start);
            };
        })

        .directive('prettyp', function () {
            return function (scope, element, attrs) {
                $("[rel^='prettyPhoto']").prettyPhoto({
                    social_tools: false,
                    theme: 'light_square',
                    modal: false,
                    allow_resize: true,
                    show_title: false,
                    default_width: 500,
                    default_height: 344,
                    deeplinking: false,
                    overlay_gallery: true,
                    image_markup: '<img id="fullResImage" src="{path}" class="img-responsive"/>',
                    markup: '<div class="pp_pic_holder"> \
						<div class="ppt">&nbsp;</div> \
						<div class="pp_content_container"> \
							<div class="pp_left"> \
							<div class="pp_right"> \
								<div class="pp_content"> \
									<div class="pp_loaderIcon"></div> \
									<div class="pp_fade"> \
										<a href="#" class="pp_expand" title="Expand the image">Expand</a> \
										<div class="pp_hoverContainer"> \
											<a class="pp_next" href="#">next</a> \
											<a class="pp_previous" href="#">previous</a> \
										</div> \
										<div id="pp_full_res"></div> \
										<div class="pp_details"> \
											<div class="pp_nav"> \
												<a href="#" class="pp_arrow_previous">Previous</a> \
												<p class="currentTextHolder">0/0</p> \
												<a href="#" class="pp_arrow_next">Next</a> \
											</div> \
											<p class="pp_description"></p> \
											<a class="pp_close" href="#">Close</a> \
										</div> \
									</div> \
								</div> \
							</div> \
							</div> \
						<div class="pp_bottom"> \
							<div class="pp_left"></div> \
							<div class="pp_middle">\
							<button class="btn btn-primary" id="addpic" type="button" type="submit">ADD TO MY PRESENTATION</button>\
							</div> \
							<div class="pp_right"></div> \
						</div> \
						</div> \
					</div>'
                }).click(function () {
                    show_overlay();

                    var inputRadio = $(this).parent().find('input');
                    var _elem = document.getElementById('addpic');
                    $(_elem).attr('data-source', inputRadio.attr('id'));


                    $('.pp_close').on('click', function () {
                        hide_overlay();
                    });
                })
            }
        });

})();