$(document).ready(function () {

    $('#presentations').dataTable({
        paging: true,
        searching: false,
        lengthChange: false,
        pagingType: 'simple_numbers',
        "pageLength": 20,
        info: false,
        "language": {
            "emptyTable": "You have no active presentations. Select Create new presentation to begin."
        }
    });

   $('#presentations th').append('<span class="sort-icon"/>');

    $('.tab-covers label.control-label').on('click', function() {
        $(this).next('div').children().children('input').trigger("click")
    });

/*
    $('a.remoteThis').on('click', function() {
        $(this).prev('.form-group').children().children().children('input').trigger("click");
    });
*/

});

