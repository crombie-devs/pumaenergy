<?php

namespace Aper\TrainingKitBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bridge\Doctrine\Form\ChoiceList\DoctrineChoiceLoader;
use Symfony\Bridge\Doctrine\Form\ChoiceList\ORMQueryBuilderLoader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\ChoiceList\Factory\DefaultChoiceListFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Aper\TrainingKitBundle\Entity\TrainingFolder;
use Aper\TrainingKitBundle\Form\Type\TrainingFolderType;
use Aper\TrainingKitBundle\Repository\TrainingFolderRepository;

/**
 * Class TrainingFolderController.
 *
 * @Route("/admin/training_folder")
 */
class TrainingFolderController extends Controller
{
    /**
     * Visualiza el arbol completo.
     *
     * @return Response
     *
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $tree = $this->getRepository()->createQueryBuilder('Node')
            ->where('Node.lvl = 0')
            ->orderBy('Node.lft', 'asc')
            ->getQuery()->getResult();

        return compact('tree');
    }

    /**
     * Alterna el estado de habilitado de un nodo base.
     *
     * @return Response
     *
     * @Route("/toggle-enabled/{id}")
     */
    public function toggleEnabledAction(Request $request, TrainingFolder $node)
    {
        if (!$node->isEnabled() && $node->getParent() && !$node->getParent()->isEnabled()) {
            throw new \InvalidArgumentException('No se puede habilitar esta carpeta');
        }

        $node->setEnabled(!$node->isEnabled());

        $this->getDoctrine()->getManager()->flush();
        $request->getSession()->getFlashBag()->add('success', 'Carpeta actualizada');

        return $this->redirectToRoute('aper_trainingkit_index');
    }

    /**
     * Reordena el arbol de categorías.
     *
     * @param Request   $request
     * @param TrainingFolder $root
     * @param string    $direction
     *
     * @return RedirectResponse
     * @Route("/reorder/{id}/{direction}")
     */
    public function reorderAction(Request $request, TrainingFolder $root, $direction = 'asc')
    {
        $direction = in_array($direction, ['asc', 'desc'], false) ? $direction : 'asc';
        $this->getRepository()->reorder($root, 'name', $direction);
        $this->addFlash('success', 'Se ha modificado el orden de las carpetas');

        return $this->redirectToRoute('aper_trainingkit_index');
    }

    /**
     * Mueve hacia adelante la categoría dentro de su nivel.
     *
     * @param Request   $request
     * @param TrainingFolder $node
     *
     * @return RedirectResponse
     * @Route("/up/{id}")
     */
    public function moveUpAction(Request $request, TrainingFolder $node)
    {
        $this->getRepository()->moveUp($node);
        $this->addFlash('success', 'Se modificó el orden de la carpeta (hacia arriba)');

        return $this->redirectToRoute('aper_trainingkit_index');
    }

    /**
     * Mueve hacia atrás la categoría dentro de su nivel.
     *
     * @param Request   $request
     * @param TrainingFolder $node
     *
     * @return RedirectResponse
     * @Route("/down/{id}")
     */
    public function moveDownAction(Request $request, TrainingFolder $node)
    {
        $this->getRepository()->moveDown($node);
        $this->addFlash('success', 'Se modificó el orden de la carpeta (hacia abajo)');

        return $this->redirectToRoute('aper_trainingkit_index');
    }

    /**
     * Permite ver detalles de una categoría.
     *
     * @param TrainingFolder $node
     *
     * @return Response
     * @Route("/show/{id}")
     * @Template()
     */
    public function showAction(TrainingFolder $node)
    {
        $path = $this->getRepository()->getPathQuery($node)->getArrayResult();

        return compact('node', 'path');
    }

    /**
     * Permite editar una categoría.
     *
     * @param Request   $request
     * @param TrainingFolder $node
     *
     * @return Response
     * @Route("/edit/{id}")
     * @Template()
     */
    public function editAction(Request $request, TrainingFolder $node)
    {
        $loader = $this->createChoiceLoader($node);
        $form = $this->createTrainingFolderForm($node, $loader);

        if ('PUT' === $request->getMethod()) {
            $form->handleRequest($request);
            
            if ($form->isValid()) {
                if ($node->getParent() && !$node->getParent()->isEnabled()) {
                    $node->setEnabled(false);
                }

                $this->getRepository()->save($node);

                $this->addFlash('success', 'Carpeta actualizada');

                return $this->redirectToRoute('aper_trainingkit_index', ['id' => $node->getId()]);
            }
        }

        $form = $form->createView();

        return compact('form');
    }

    /**
     * Permite borrar una carpeta.
     *
     * @param Request   $request
     * @param TrainingFolder $node
     *
     * @return Response
     * @Route("/delete/{id}")
     * @Template()
     */
    public function deleteAction(Request $request, TrainingFolder $node)
    {
        if(count($node->getChildren()) > 0 || count($node->getAllCourseCollection()) > 0) {
            $this->addFlash('error', 'No se puede eliminar la carpeta porque tiene contenido');

            return $this->redirectToRoute('aper_trainingkit_index');
        }

        try {
            $this->getRepository()->removeFromTree($node);
        } catch (Exception $e) {
            $this->addFlash('error', 'No se puede eliminar la carpeta. ' . $e->getMessage());

            return $this->redirectToRoute('aper_trainingkit_index');    
        }
        
        $this->addFlash('success', 'Se ha eliminado la carpeta');

        return $this->redirectToRoute('aper_trainingkit_index');
    }

    /**
     * Presenta el formulario de alta de categoría.
     *
     * @return array
     *
     * @Route("/add")
     * @Template()
     */
    public function addAction()
    {
        $entity = new TrainingFolder();
        $form = $this->createTrainingFolderForm($entity);

        return ['form' => $form->createView(), 'entity' => $entity];
    }

    /**
     * Permite grabar cambios sobre una categoría.
     *
     * @return array|Response
     *
     * @Route("/save")
     * @Template("@TrainingKitBundle/TrainingFolder/add.html.twig")
     */
    public function saveAction(Request $request)
    {
        $node = new TrainingFolder();
        $form = $this->createTrainingFolderForm($node);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($node->getParent() && !$node->getParent()->isEnabled()) {
                $node->setEnabled(false);
            }

            $this->getRepository()->save($node);

            $this->addFlash('success', 'Carpeta creada');

            return $this->redirectToRoute('aper_trainingkit_index');
        }

        $form = $form->createView();

        return compact('form');
    }

    /**
     * @return TrainingFolderRepository
     */
    private function getRepository()
    {
        $repo = $this->getDoctrine()->getRepository('TrainingKitBundle:TrainingFolder');

        return $repo;
    }

    /**
     * @param TrainingFolder $node
     *
     * @return DoctrineChoiceLoader
     */
    private function createChoiceLoader(TrainingFolder $node)
    {
        $factory = new DefaultChoiceListFactory();
        $em = $this->getDoctrine()->getManager();
        $qb = $this->getRepository()->createQueryBuilderExcludingNode($node);
        $qbLoader = new ORMQueryBuilderLoader($qb);
        $loader = new DoctrineChoiceLoader($factory, $em, 'TrainingKitBundle:TrainingFolder', null, $qbLoader);

        return $loader;
    }

    /**
     * @param TrainingFolder $node
     * @param null      $loader
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createTrainingFolderForm(TrainingFolder $node, $loader = null)
    {
        if (!$loader) {
            $form = $this->createForm(new TrainingFolderType($loader), $node, [
                'action' => $this->generateUrl('aper_trainingkit_save'),
                'method' => 'POST',
            ]);
        } else {
            $form = $this->createForm(new TrainingFolderType($loader), $node, [
                'action' => $this->generateUrl('aper_trainingkit_edit', ['id' => $node->getId()]),
                'method' => 'PUT',
            ]);
        }

        $form->add('Submit', 'submit', ['attr' => ['class' => 'btn btn-primary']]);

        return $form;
    }

    /**
     * Permite ver detalles de una categoría.
     **
     * @return Response
     * @Route("/courses/root")
     * @Template("@TrainingKit/TrainingFolder/courses_by_folder.twig")
     */
    public function coursesRootFolderAction()
    {
        $roots = $this->getRepository()->createQueryBuilder('Node')
            ->where('Node.lvl = 0')
            ->orderBy('Node.lft', 'asc')
            ->getQuery()->getResult();

        return compact('roots');
    }

    /**
     * Permite ver detalles de una categoría.
     *
     * @param TrainingFolder $node
     *
     * @return Response
     * @Route("/{id}/courses")
     * @Template("@TrainingKit/TrainingFolder/courses_by_folder.twig")
     */
    public function coursesFolderAction(TrainingFolder $node)
    {
        return compact('node');
    }

}
