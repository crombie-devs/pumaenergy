<?php

namespace Aper\BenefitBundle\DataFixtures\ORM;

use Aper\BenefitBundle\Entity\Benefit;
use Aper\BenefitBundle\Model\Category;
use Aper\UserBundle\Model\Role;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BenefitFixture extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $this->container->get('web_factory_user.user_context')->setContextName('backend'); // Fix

        /** @var Benefit[] $benefits */
        $benefits = [];

        $benefits[] = new Benefit();

        for ($i = 0; $i < 4; ++$i) {
            $benefits[] = new Benefit();
            $benefits[$i]
                ->setName("Benefit ${i}")
                ->setDescription("Description benefit ${i}")
                ->setEnabled(true);
        }

        $benefits[0]
            ->setCategory(Category::MOMENTS)
            ->setRolesAllowed([Role::ROLE_EMPLOYEE]);
        $this->addReference('benefit-employee', $benefits[0]);

        $benefits[1]
            ->setCategory(Category::ENTERTAINMENT)
            ->setRolesAllowed([Role::ROLE_MANAGER]);
        $this->addReference('benefit-manager', $benefits[1]);

        $benefits[2]
            ->setCategory(Category::SERVICES)
            ->setRolesAllowed([Role::ROLE_RRHH]);
        $this->addReference('benefit-rrhh', $benefits[2]);

        $benefits[3]
            ->setCategory(Category::WELLNESS)
            ->setRolesAllowed([Role::ROLE_HEAD_OFFICE]);
        $this->addReference('benefit-welness', $benefits[3]);

        unset($benefits[4]); // Hack porque deja un elemento vacío

        foreach ($benefits as $benefit) {
            $manager->persist($benefit);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}
