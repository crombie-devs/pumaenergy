<?php

namespace Aper\BenefitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Benefit.
 *
 * @ORM\Entity(repositoryClass="Aper\BenefitBundle\Repository\BenefitRepository")
 * @ORM\Table(name="benefits")
 */
class Benefit
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string")
     * @Assert\NotBlank()
     */
    private $category;

    /**
     * @var string[]
     *
     * @ORM\Column(name="roles_allowed", type="simple_array")
     * @Assert\Count(min="1", minMessage="Debe elegir un rol como mínimo")
     */
    private $rolesAllowed;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Benefit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Benefit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set category.
     *
     * @param string $category
     *
     * @return Benefit
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set rolesAllowed.
     *
     * @param string $rolesAllowed
     *
     * @return Benefit
     */
    public function setRolesAllowed($rolesAllowed)
    {
        $this->rolesAllowed = $rolesAllowed;

        return $this;
    }

    /**
     * Get rolesAllowed.
     *
     * @return string
     */
    public function getRolesAllowed()
    {

            return $this->rolesAllowed;

    }

    /**
     * @param $roleAllowed
     *
     * @return $this
     */
    public function addRoleAllowed($roleAllowed)
    {
            $roleAllowed = strtoupper($roleAllowed);

            if (!in_array($roleAllowed, $this->rolesAllowed)) {

                $this->rolesAllowed[] = $roleAllowed;
            }

            return $this;

        }

    /**
     * @param $roleAllowed
     *
     * @return $this
     */
    public function removeRoleAllowed($roleAllowed)
    {
        if (false !== $key = array_search(strtoupper($roleAllowed), $this->rolesAllowed, true)) {
            unset($this->rolesAllowed[$key]);
            $this->rolesAllowed = array_values($this->rolesAllowed);
        }

        return $this;
    }

    public function hasRoleAllowed($roleAllowed)
    {
        return false !== $key = array_search(strtoupper($roleAllowed), $this->rolesAllowed, true);
    }

    /**
     * Set enabled.
     *
     * @param bool $enabled
     *
     * @return Benefit
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    public function __toString()
    {
        return sprintf('%s', $this->name);
    }
}
