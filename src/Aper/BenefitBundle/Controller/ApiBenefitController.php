<?php

namespace Aper\BenefitBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Aper\BenefitBundle\Entity\Benefit;

/**
 * Benefit controller.
 *
 * @Route("benefit/api")
 */
class ApiBenefitController extends Controller
{
    /**
     *
     */
    const KEY = 'EOEfcUTIL5sSmXpYM2YN679w0jdWVzpEdX9CYHaAkFXUdMj02ve9nYVCBnswckDY';
    /**
     *
     */
    const MICROSITE_ID = 909134;

    /**
     *
     */
    const END_POINT = 'https://apiv1.cuponstar.com/api/';

    const END_POINT_API_CUPONSTAR_BASE = 'https://apiv1.cuponstar.com/api/';

    const END_POINT_CUPONES_ON_CUPONSTAR = '/cupones';

    const END_POINT_CATEGORIES_ON_CUPONSTAR = '/categorias';

    const END_POINT_LOCATIONS_ON_CUPONSTAR = '/filtros';

    const CODIGO_AFILIADO_ID = 909134;

    /**
     * @Route("/coupons")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        return $this->doRequest('/cupones', $request->query->all());
    }

    /**
     * @Route("/show/{id}")
     * @Template()
     *
     * @param Request $request
     * @param $id
     * @return array
     */
    public function showAction(Request $request, $id)
    {

        $category = 'aper.benefit.category.services';
        $repository = $this->getDoctrine()->getRepository('AperBenefitBundle:Benefit');
        $beneficios = $repository->findByRolesAllowed($this->getUser()->getRoles(), $category);


        $coupon = $this->doRequest("/cupones/{$id}");
        $sucursales = $this->doRequest("/cupones/{$id}/sucursales");
       
        return ['response' => [
            'coupon' => $coupon['response'],
            'sucursales' => $sucursales['response'],
        ], 'beneficios' => $beneficios];
    }

    /**
     * @Route("/categories")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function categoriesAction(Request $request)
    {
        return $this->doRequest('/categorias');
    }

    /**
     * @Route("/search-ajax", name="search_ajax")
     */
    public function responseAction(Request $request)
    {
        $benefit = $_POST['text'];

	if($benefit!=""){
            $url = "https://apiv1.cuponstar.com/api/cupones?key=EOEfcUTIL5sSmXpYM2YN679w0jdWVzpEdX9CYHaAkFXUdMj02ve9nYVCBnswckDY&micrositio_id=909134&codigo_afiliado=paizensztein@aper.com&query=".$benefit;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            $result = curl_exec($ch);
        }

	$response = new JsonResponse();
        $response->setData($result);

        return $response;
    }

    // Routing.generate("search-ajax",{'id':id}),

    /**
     * @param $api
     * @param array $params
     * @return array
     */
    private function doRequest($api, $params = [])
    {
        $params['key'] = self::KEY;
        $params['micrositio_id'] = self::MICROSITE_ID;
        $params['codigo_afiliado'] = $this->getUser()->getEmail(); // 176
        // $params['codigo_afiliado'] = self::CODIGO_AFILIADO_ID;// remove this line dummy

        $category = 'aper.benefit.category.services';
        $repository = $this->getDoctrine()->getRepository('AperBenefitBundle:Benefit');
        $beneficios = $repository->findByRolesAllowed($this->getUser()->getRoles(), $category);

        $urlparam = "micrositio_id={$params['micrositio_id']}&key={$params['key']}&codigo_afiliado={$params['codigo_afiliado']}";

        // $url = self::END_POINT_API_CUPONSTAR_BASE . $api . '?' . http_build_query($params);
        $url = self::END_POINT_API_CUPONSTAR_BASE . $api . '?' . $urlparam;

        // echo $url;
        $json = "";
        try{
           $json = file_get_contents($url);
        }catch (\Exception $exception){
           $json = "";
       }


       $return = json_decode($json, true);

       if (isset($return['error']) && $return['code'] == 'CRED_NOT_FOUND_MDW') {     
            $code = $this->getUser()->getEmail();
            $url = "https://cms.bonda.us/api/v2/microsite/".self::MICROSITE_ID."/affiliates";

            $header = array(
                'Accept: application/json', 
                'Cache-Control: no-cache', 
                'Content-Type: multipart/form-data',
                'token: '.self::KEY 
            );

            $handle = curl_init($url);

            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_HEADER, true);
            curl_setopt($handle, CURLOPT_HTTPHEADER, $header);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($handle, CURLOPT_POSTFIELDS, array('code' => $code));

            curl_setopt($handle, CURLOPT_FAILONERROR, true);
            curl_setopt($handle, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
            curl_setopt($handle, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
            curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);

            $result = curl_exec($handle);

            if (curl_errno($handle)) {
                $errorMsg = curl_error($handle);
            }

            if (isset($errorMsg) && !empty($errorMsg)) {
            }

            curl_close($handle);

            try{
               $json = file_get_contents($url);
            }catch (\Exception $exception){
               $json = "";
           }
       }






        return ['response' => json_decode($json, true), 'beneficios' => $beneficios];
    }

    /**
     * @Route("/my-list")
     * @param Request $request
     */
    public function myListAction(Request $request)
    {
        $params['key'] = self::KEY;
        $params['micrositio_id'] = self::MICROSITE_ID;
        $params['codigo_afiliado'] = $this->getUser()->getEmail(); // 176
        // $params['codigo_afiliado'] = self::CODIGO_AFILIADO_ID;// remove this line dummy

        $urlparam = "micrositio_id={$params['micrositio_id']}&key={$params['key']}&codigo_afiliado={$params['codigo_afiliado']}";

        // $url = self::END_POINT_API_CUPONSTAR_BASE . 'cupones_recibidos/' . '?' . http_build_query($params);
        $url = self::END_POINT_API_CUPONSTAR_BASE . 'cupones_recibidos/' . '?' . $urlparam;
// echo $url;exit;
        $res = "";
        try{
            $res = file_get_contents($url);
           }catch (\Exception $exception){
            $res = "";
        }

        return $this->render('AperBenefitBundle:Frontend/Benefit:mylist.html.twig', array(
            'response' => json_decode($res, true),
        ));
    }



    /**
     * @Route("/redirect")
     * @param Request $request
     */
    public function redirectAction(Request $request)
    {
        $params = [];
        $params['key'] = self::KEY;
        $params['micrositio_id'] = self::MICROSITE_ID;
        $params['codigo_afiliado'] = $this->getUser()->getEmail();
        // $params['codigo_afiliado'] = self::CODIGO_AFILIADO_ID;// remove this line dummy

        $fields_string = null;
        $data = array_merge($request->query->all(), $params);
        
        foreach ($data as $key => $value) {
            if($key == "cupon_id")
                $cupon_id = $value;
            else
                $fields_string .= $key . '=' . $value . '&';
        }

        rtrim($fields_string, '&');
        $url = self::END_POINT_API_CUPONSTAR_BASE.'texto-sms/' . $cupon_id;

        $handle = curl_init($url);

        $url = htmlspecialchars($url);
        $url = str_replace("amp;", "" , $url);
        $url = htmlspecialchars($url);
        $url = str_replace("http", "https" , $url);
        $url = str_replace("amp;", "" , $url);

        error_reporting(0);
        curl_setopt($handle, CURLOPT_HEADER, 0); 
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_POST, count($data));
        curl_setopt($handle, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_FAILONERROR, true);

        $returnHeader = curl_exec($handle);

        if (curl_errno($handle)) {
            $errorMsg = curl_error($handle);
        }

        if (isset($errorMsg) && !empty($errorMsg)) {
        }

        curl_close($handle);


        return JsonResponse::create(json_decode($returnHeader, true));
    }

    /**
     * @Route("/cupones/findCuponBy")
     * @param Request $request
     */
    public function findCuponByAction(Request $request)
    {
        $params = [];
        $params['key'] = self::KEY;
        $params['micrositio_id'] = self::MICROSITE_ID;
        $params['codigo_afiliado'] = self::CODIGO_AFILIADO_ID;
        
        if(!is_null($this->getUser()) &&
            !is_null($this->getUser()->getEmail())){
            $params['codigo_afiliado'] = $this->getUser()->getEmail();
            // $params['codigo_afiliado'] = self::CODIGO_AFILIADO_ID;// remove this line dummy
        }


        $fields_string = null;
        $data = array_merge($request->query->all(), $params);

        foreach ($data as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }

        rtrim($fields_string, '&');


        $url = self::END_POINT_API_CUPONSTAR_BASE . self::END_POINT_CUPONES_ON_CUPONSTAR . '?' . http_build_query($data);
// print_r($url);
        $handle = curl_init($url);

        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_FAILONERROR, true);
        curl_setopt($handle, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
        curl_setopt($handle, CURLOPT_DNS_CACHE_TIMEOUT, 2 );

        $result = curl_exec($handle);

        if (curl_errno($handle)) {
            $errorMsg = curl_error($handle);
        }

        if (isset($errorMsg) && !empty($errorMsg)) {
        }

        curl_close($handle);

        return JsonResponse::create(json_decode($result, true));
    }

    /**
     * @Route("/categorias/findCategoriesBy")
     * @param Request $request
     */
    public function findCategoriesByAction(Request $request)
    {
        $params = [];
        $params['key'] = self::KEY;
        $params['micrositio_id'] = self::MICROSITE_ID;
        $params['codigo_afiliado'] = self::CODIGO_AFILIADO_ID;
        if(!is_null($this->getUser()) &&
            !is_null($this->getUser()->getEmail())){
            $params['codigo_afiliado'] = $this->getUser()->getEmail();
            // $params['codigo_afiliado'] = self::CODIGO_AFILIADO_ID;// remove this line dummy
        }


        $fields_string = null;
        $data = array_merge($request->query->all(), $params);
        foreach ($data as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }

        rtrim($fields_string, '&');


        $url = self::END_POINT_API_CUPONSTAR_BASE.
            self::END_POINT_CATEGORIES_ON_CUPONSTAR.
            '?' .
            http_build_query($data);

        $handle = curl_init($url);

        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_FAILONERROR, true);
        curl_setopt($handle, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
        curl_setopt($handle, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
        curl_setopt($handle, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $result = curl_exec($handle);

        if (curl_errno($handle)) {
            $errorMsg = curl_error($handle);
        }

        if (isset($errorMsg) && !empty($errorMsg)) {
        }

        curl_close($handle);

        return JsonResponse::create(json_decode($result, true));
    }

    /**
     * @Route("/locations/findLocationsBy")
     * @param Request $request
     */
    public function findLocationsByAction(Request $request)
    {

        $params = [];
        $params['key'] = self::KEY;
        $params['micrositio_id'] = self::MICROSITE_ID;
        $params['codigo_afiliado'] = self::CODIGO_AFILIADO_ID;
        if(!is_null($this->getUser()) &&
            !is_null($this->getUser()->getEmail())){
            $params['codigo_afiliado'] = $this->getUser()->getEmail();
            // $params['codigo_afiliado'] = self::CODIGO_AFILIADO_ID;// remove this line dummy
        }


        $fields_string = null;
        $data = array_merge($request->query->all(), $params);
        foreach ($data as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }

        rtrim($fields_string, '&');


        $url = self::END_POINT_API_CUPONSTAR_BASE.
            self::END_POINT_LOCATIONS_ON_CUPONSTAR.
            '?' .
            http_build_query($data);

        $handle = curl_init($url);

        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_FAILONERROR, true);
        curl_setopt($handle, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
        curl_setopt($handle, CURLOPT_DNS_CACHE_TIMEOUT, 2 );

        $result = curl_exec($handle);

        if (curl_errno($handle)) {
            $errorMsg = curl_error($handle);
        }

        if (isset($errorMsg) && !empty($errorMsg)) {
        }

        curl_close($handle);

        return JsonResponse::create(json_decode($result, true));
    }

}