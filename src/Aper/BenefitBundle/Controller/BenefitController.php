<?php

namespace Aper\BenefitBundle\Controller;

use Aper\BenefitBundle\Entity\Benefit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\GridBundle\Event\ManipulateQueryEvent;
use WebFactory\Bundle\GridBundle\Grid\Configuration;
use Aper\BenefitBundle\Form\BenefitFilterType;



/**
 * Benefit controller.
 *
 * @Route("benefit")
 */
class BenefitController extends Controller
{
    /**
     * Lists all benefit entities.
     *
     * @Route("/", name="aper_benefit_benefit_index")
     * @Method({"GET","POST"})
     */
    public function indexAction(Request $request)
    {
        $config = new Configuration(
            $this->getDoctrine()->getRepository(Benefit::class)->getQueryBuilder(),
            $this->get('web_factory_grid.form_filter_factory')->create(BenefitFilterType::class),
            'main',
            'AperBenefitBundle:Benefit:index.html.twig'
        );

        $grid = $this->get('web_factory_grid.factory')->create($config);

        $grid->addListener(ManipulateQueryEvent::NAME, function (ManipulateQueryEvent $event) {
            $queryBuilder = $event->getQueryBuilder();
            $filters = $event->getFilters();

            if (null !== $filters['name']) {
                $queryBuilder->andWhere('Benefit.name LIKE :name')->setParameter('name', "%{$filters['name']}%");
            }

            if (null !== $filters['description']) {
                $queryBuilder->andWhere('Benefit.description LIKE :description')->setParameter('description', "%{$filters['description']}%");
            }
        });

        return $grid->handle($request);


    }

    /**
     * Creates a new benefit entity.
     *
     * @Route("/new", name="aper_benefit_benefit_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $benefit = new Benefit();
        $form = $this->createForm('Aper\BenefitBundle\Form\BenefitType', $benefit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($benefit);
            $em->flush($benefit);

            return $this->redirectToRoute('aper_benefit_benefit_show', array('id' => $benefit->getId()));
        }

        return $this->render('AperBenefitBundle:Benefit:new.html.twig', array(
            'benefit' => $benefit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a benefit entity.
     *
     * @Route("/{id}", name="aper_benefit_benefit_show")
     * @Method("GET")
     */
    public function showAction(Benefit $benefit)
    {
        $deleteForm = $this->createDeleteForm($benefit);

        return $this->render('AperBenefitBundle:Benefit:show.html.twig', array(
            'benefit' => $benefit,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing benefit entity.
     *
     * @Route("/{id}/edit", name="aper_benefit_benefit_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Benefit $benefit)
    {
        $deleteForm = $this->createDeleteForm($benefit);
        $editForm = $this->createForm('Aper\BenefitBundle\Form\BenefitType', $benefit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('aper_benefit_benefit_show', array('id' => $benefit->getId()));
        }

        return $this->render('AperBenefitBundle:Benefit:edit.html.twig', array(
            'benefit' => $benefit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a benefit entity.
     *
     * @Route("/{id}/delete", name="aper_benefit_benefit_delete")
     * @Method({"DELETE", "GET"})
     */
    public function deleteAction(Request $request, Benefit $benefit)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($benefit);
        $em->flush();

        return $this->redirectToRoute('aper_benefit_benefit_index');
    }

    /**
     * Creates a form to delete a benefit entity.
     *
     * @param Benefit $benefit The benefit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Benefit $benefit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('aper_benefit_benefit_delete', array('id' => $benefit->getId())))
            ->add('submit', SubmitType::class, [
                'label' => 'Delete',
            ])
            ->setAttributes([
                'class' => 'delete-form',
                'id' => 'delete-form',
            ])
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
