<?php

namespace Aper\BenefitBundle\Controller;

use Aper\BenefitBundle\Model\Category;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use WebFactory\Bundle\UserBundle\Controller\ContextController;

/**
 * Class BenefitController
 *
 * @Route("benefit")
 */
class FrontendBenefitController extends ContextController
{
    /**
     * @Route("/")
     *
     * @param
     * @return Response
     */
    public function indexAction(Request $request)
    {
        // Las categorías pueden cambiar y hay que cambiarlo en el Model y en la vista cuando pase
        /**
         * @todo Desarrollar paginador (ver NewsController)
         */
        $cat = array_keys(Category::getChoices());
        $fcategory = $request->get('category');
        $name = $request->get('name');
        $role = $this->getUser()->getRoles();
        if(preg_match('/^[a-zA-Z0-9]$/', $name)){
            $name = '';
            $fcategory = '';
        }
        $repository = $this->getDoctrine()->getRepository('AperBenefitBundle:Benefit');

        if($name != '' || $fcategory != ''){
            if($fcategory != '')
                if(is_numeric($fcategory) && $fcategory < count($cat) && $fcategory > -1)
                    $fcategory = $cat[$fcategory];

            $benefitsResoutls = $repository->findByName([$name, $fcategory]);
            $benefits = [];
            foreach ($benefitsResoutls as $value) {
                //\Doctrine\Common\Util\Debug::dump($value->getRolesAllowed());
                //echo '<br/>';
                foreach ($role as $rol) {
                    if($value->hasRoleAllowed($rol) || $rol == "ROLE_ADMIN" || $rol == "ROLE_RRHH"){
                        $benefits[$value->getCategory()][] = $value;
                    }
                }
            }
        } else {
            $benefits = [];
            foreach (Category::getChoices() as $category) {
                $benefits[$category] = $repository->findByRolesAllowed($this->getUser()->getRoles(), $category);
            }
        }

        return $this->render('AperBenefitBundle:Frontend/Benefit:index.html.twig', [
            'benefits' => $benefits,
        ]);
    }

    /**
     * @Route("/show/{category}")
     *
     * @param
     * @return Response
     */
    public function showAction($category)
    {
        $repository = $this->getDoctrine()->getRepository('AperBenefitBundle:Benefit');
        $benefits = $repository->findByRolesAllowed($this->getUser()->getRoles(), $category);

        return $this->render('AperBenefitBundle:Frontend/Benefit:show.html.twig', [
            'category' => $category,
            'benefits' => $benefits,
        ]);
    }
}