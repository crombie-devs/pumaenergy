<?php

namespace Aper\BenefitBundle\Form;

use Aper\BenefitBundle\Model\Category;
use Aper\UserBundle\Model\Role;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BenefitType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles = Role::getChoices();
        // unset($roles['ROLE_RRHH']);
        unset($roles['ROLE_ADMIN']);

        $builder
            ->add('name')
            ->add('description', CKEditorType::class)
            ->add('category', ChoiceType::class, [
                'choices' => Category::getChoices(),
            ])
            ->add('enabled')
            ->add('rolesAllowed', ChoiceType::class, [
                'choices' => $roles,
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\BenefitBundle\Entity\Benefit',
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'aper_benefitbundle_benefit';
    }
}
