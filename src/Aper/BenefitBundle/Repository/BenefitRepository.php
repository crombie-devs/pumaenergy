<?php

namespace Aper\BenefitBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BenefitRepository extends EntityRepository
{
    /**
     * @param array $roles
     *
     * @return array
     */
    public function findByRolesAllowed(array $roles, $category)
    {
        $benefitsResult = $this->createQueryBuilder('Benefit')
            ->where('Benefit.enabled = true')
            ->getQuery()->getResult();

        $benefits = [];
        foreach ($benefitsResult as $benefit) {
            foreach ($roles as $role) {
                // if ($benefit->getCategory() === $category && ($benefit->hasRoleAllowed($role) || $role == "ROLE_ADMIN" || $role == "ROLE_RRHH")) {
                    $benefits[] = $benefit;
                // }
            }
        }

        return $benefits;
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->createQueryBuilder('Benefit')->orderBy('Benefit.id', 'ASC');
    }

    /**
     * @param $params
     *
     * @return \Aper\BenefitBundle\Entity\Benefit
     */
    public function findForUniqueValidation(array $params)
    {
        return $this->getEntityManager()->getRepository('AperBenefitBundle:Benefit')->findBy($params);
    }

    /**
     * @param $params
     *
     * @return array
     */
    public function findByName(array $params){
        $name = $params[0];
        $category = $params[1];

        $qb   = $this->createQueryBuilder('Benefit');
        if($name != ''){
            $qb->where('Benefit.name like :name')->setParameter('name', '%'.$name.'%');
        }
        if($category != ''){
            $qb->andWhere('Benefit.category like :category')->setParameter('category', '%'.$category.'%');
        }
        $qb->andWhere('Benefit.enabled = true');
        return $qb->getQuery()->getResult();

    }



}
