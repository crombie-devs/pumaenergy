<?php

namespace Aper\BenefitBundle\Model;

class Category
{
    const ENTERTAINMENT = 'aper.benefit.category.entertainment';
    const MOMENTS = 'aper.benefit.category.moments';
    const SERVICES = 'aper.benefit.category.services';
    const WELLNESS = 'aper.benefit.category.wellness';

    public static function getChoices()
    {
        return [
            self::WELLNESS => self::WELLNESS,
            self::MOMENTS => self::MOMENTS,
            self::ENTERTAINMENT => self::ENTERTAINMENT,
            self::SERVICES => self::SERVICES,
        ];
    }
}
