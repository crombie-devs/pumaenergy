<?php

namespace Aper\StoreBundle\Repository;

use Aper\UserBundle\Model\Role;
use Aper\StoreBundle\Entity\Store;
use Doctrine\ORM\EntityRepository;

/**
 * Class StoreRepository.
 */
class StoreRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->createQueryBuilder('Store')->orderBy('Store.id', 'ASC');
    }


    public function findAllOrderedByNombre() { 
        $qb   = $this->createQueryBuilder('Store');
        $qb->where('Store.code != :code')->setParameter('code', 0);
        $qb->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);
        $qb->orderBy('Store.location', 'ASC');

        return $qb->getQuery()->getResult();
    }


    /**
     * @param $params
     *
     * @return \Aper\StoreBundle\Entity\Store
     */
    public function findForUniqueValidation(array $params)
    {
        return $this->findBy($params);
    }

    /**
     * @param string $code
     * @return Store
     */
    public function findOneByCode($code)
    {
        $code = str_pad($code,4,"0", STR_PAD_LEFT);
        $qb   = $this->createQueryBuilder('Store');
        $qb
            ->where('Store.code = :code')->setParameter('code', $code)
            ->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);

        return $qb->getQuery()->getOneOrNullResult();
    }


    /**
     * @param string $code
     * @return Store
     */
    public function findOneById($id)
    {
        $qb   = $this->createQueryBuilder('Store');
        $qb->where('Store.id = :id')->setParameter('id', $id)
            ->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);

        return $qb->getQuery()->getOneOrNullResult();
    }


    public function findPerformance($month, $year, $limit = 3, $responsable = null, $zona = null, $storeId = null, $storesIds = null)
    {
        $query = $this->createQueryBuilder('Store');
        $query->innerJoin('Store.rankings', 'Ranking')

            ->where('Ranking.year = :year')->setParameter('year', $year)
            // ->andWhere('Store.code != :code')->setParameter('code', 0)
            ->andWhere('Ranking.month = :month')->setParameter('month', $month)
            ->andWhere('Ranking.type = :type')->setParameter('type', 'MONTH')
            ->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);
        if ($responsable) {
            $query->andWhere('Store.responsableComercial = :responsable')->setParameter('responsable', $responsable);
        }
        if ($zona) {
            $query->andWhere('Store.zona = :zona')->setParameter('zona', $zona);
        }
        if ($storeId) {
            $query->andWhere('Store.id = :id')->setParameter('id', $storeId);
        }
        if ($storesIds) {
            // $query->andWhere('Store.code IN (:code)')->setParameter('code', explode(',', $storesIds));
            $query->andWhere('Store.code IN (:code)')->setParameter('code', $storesIds);
        }
        $query->setMaxResults($limit)->orderBy('Ranking.position', 'ASC');

        return $query->getQuery()->getResult();
    }


    public function findStoresByRepresentante($responsable = null, $zona = null, $storeId = null, $storesIds = null)
    {
        $query = $this->createQueryBuilder('Store');
         $query->where('Store.enabled = :enabled')->setParameter('enabled', 1);
        // $query->andWhere('Store.code != :code')->setParameter('code', 0);
        
        if ($responsable) {
            $query->andWhere('Store.responsableComercial = :responsable')->setParameter('responsable', $responsable);
        }
        if ($zona) {
            $query->andWhere('Store.zona = :zona')->setParameter('zona', $zona);
        }
        if ($storeId) {
            $query->andWhere('Store.id = :id')->setParameter('id', $storeId);
        }
        if ($storesIds) {
            $query->andWhere('Store.code IN (:code)')->setParameter('code', $storesIds);
            // $query->andWhere('Store.code IN (:code)')->setParameter('code', explode(',', $storesIds));
        }
        $query->orderBy('Store.location', 'ASC');

        return $query->getQuery()->getResult();
    }



    public function findStoresByCodes($month, $year, $storesCodes = null)
    {

        $query = $this->createQueryBuilder('Store');
        $query->innerJoin('Store.rankings', 'Ranking');

        $query->where('Ranking.year = :year')->setParameter('year', $year);
            // ->andWhere('Store.code != :code')->setParameter('code', 0)

        if ($month == null) {
            $type = 'YEAR';
        } else {
            $query->andWhere('Ranking.month = :month')->setParameter('month', $month);
            $type = 'MONTH';
        }
        $query->andWhere('Ranking.type = :type')->setParameter('type', $type);
        $query->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);

        if ($storesCodes) {
            $query->andWhere('Store.code IN (:code)')->setParameter('code', $storesCodes);
        }
        $query->orderBy('Ranking.position', 'ASC');
        return $query->getQuery()->getResult();
    }


    /**
     * @param integer $month
     * @param integer $year
     * @param integer $limit
     * @return Store[]
     */
    public function findRankingByMonth($month, $year, $limit = 3, $zona = null)
    {
        $query = $this->createQueryBuilder('Store');
        $query->innerJoin('Store.rankings', 'Ranking')
            ->where('Ranking.year = :year')->setParameter('year', $year)
            ->andWhere('Ranking.month = :month')->setParameter('month', $month)
            ->andWhere('Ranking.type = :type')->setParameter('type', 'MONTH')
            ->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);
        if ($zona) {
            $query->andWhere('Store.zona = :zona')->setParameter('zona', $zona);
        }
        $query->setMaxResults($limit)->orderBy('Ranking.position', 'ASC');

        return $query->getQuery()->getResult();
    }

    /**
     * @param integer $year
     * @return Store[]
     */
    public function findRankingByYear($year, $zona = null)
    {
        $query = $this->createQueryBuilder('Store');
        $query->innerJoin('Store.rankings', 'Ranking')
            ->where('Ranking.year = :year')->setParameter('year', $year)
            ->andWhere('Ranking.type = :type')->setParameter('type', 'YEAR')
            ->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);
        if ($zona) {
            $query->andWhere('Store.zona = :zona')->setParameter('zona', $zona);
        }
        $query->orderBy('Ranking.position', 'ASC');
        return $query->getQuery()->getResult();
    }
}
