<?php

namespace Aper\StoreBundle\Entity;

use Aper\BenefitBundle\Model\Category;
use Aper\IncentiveBundle\Entity\ExtraPointsStore;
use Aper\IncentiveBundle\Entity\Goal;
use Aper\IncentiveBundle\Entity\Ranking;
use Aper\UserBundle\Entity\Employee;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Store.
 *
 * @ORM\Table(name="stores")
 * @ORM\Entity(repositoryClass="Aper\StoreBundle\Repository\StoreRepository")
 * @UniqueEntity(fields={"code"}, errorPath="code")
 */
class Store
{
    const EXTRA_POINTS = 1000;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Employee[]|Collection
     *
     * @ORM\OneToMany(targetEntity="Aper\UserBundle\Entity\Employee", mappedBy="store")
     */
    protected $employees;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=32)
     * @Assert\NotBlank()
     */
    private $code;

    //@Assert\NotBlank()
    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", nullable=true, length=100)
     *
     */
    private $location;

    /**
     * @Assert\Image(
     *     minWidth = 100,
     *     maxWidth = 1000,
     *     minHeight = 100,
     *     maxHeight = 1000
     * )
     */
    private $image;

    // @Assert\NotBlank()
    /**
     * @var string
     * @ORM\Column(name="brand", type="string", nullable=true, length=100)
     *
     * Options: Storerk / Hoyts
    */
    private $brand;

    /**
     * @var Goal[]|Collection
     * @ORM\OneToMany(targetEntity="Aper\IncentiveBundle\Entity\Goal", mappedBy="store", cascade={"all"}, orphanRemoval=true)
     */
    private $goals;

    /**
     * @var Ranking[]|Collection
     * @ORM\OneToMany(targetEntity="Aper\IncentiveBundle\Entity\Ranking", mappedBy="store", cascade={"all"}, orphanRemoval=true)
     */
    private $rankings;


    /**
     * @var ExtraPointsStore[]|Collection
     * @ORM\oneToMany(targetEntity="Aper\IncentiveBundle\Entity\ExtraPointsStore", mappedBy="store", cascade={"all"}, orphanRemoval=true)
     */
    private $extraPoints;
    
    /**
     * @var string
     * @ORM\Column(name="cliente", type="string", nullable=true)
     */
    private $cliente;
    
    /**
     * @var string
     * @ORM\Column(name="tipo_comercio", type="string", nullable=true)
     */
    private $tipoComercio;
    
    /**
     * @var string
     * @ORM\Column(name="segmento", type="string", nullable=true)
     */
    private $segmento;
    
    /**
     * @var string
     * @ORM\Column(name="bandera", type="string", nullable=true)
     */
    private $bandera;
    
    /**
     * @var string
     * @ORM\Column(name="responsable_comercial", type="string")
     */
    private $responsableComercial;
    
    /** 
     * @var date
     * @ORM\Column(name="fecha_baja", type="date", nullable=true)
     */
    private $fechaBaja;

    /**
     * @ORM\ManyToOne(targetEntity="Localidad")
     * @ORM\JoinColumn(name="localidad_id", referencedColumnName="id")
     */
    private $localidad;

    /**
     * @ORM\ManyToOne(targetEntity="Provincia")
     * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
     */
    private $provincia;

    /**
     * @ORM\ManyToOne(targetEntity="TipoActividad")
     * @ORM\JoinColumn(name="tipo_actividad_id", referencedColumnName="id", nullable=true)
     */
    private $tipoActividad;


    /**
     * @var string
     * @ORM\Column(name="zona", type="string", nullable=true)
     */
    private $zona;

    /**
     * @var int
     * @ORM\Column(name="enabled", type="integer")
     */
    private $enabled;


    /**
     * @var string
     * @ORM\Column(name="own", type="string", nullable=true, length=100)
     */
    private $own;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", nullable=true, length=100)
     *
     */
    private $address;


    /**
     * @var int
     *
     * @ORM\Column(name="bt", type="integer")
     */
    private $bt;

    /**
     * @var string
     *
     * @ORM\Column(name="btUser", type="string", nullable=true, length=100)
     *
     */
    private $btUser;

    /**
     * @var string
     *
     * @ORM\Column(name="btToken", type="string", nullable=true, length=200)
     *
     */
    private $btToken;


    /**
     * Store constructor.
     */
    public function __construct()
    {
        $this->employees = new ArrayCollection();
        $this->goals = new ArrayCollection();
        $this->rankings = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Set location.
     *
     * @param string $location
     *
     * @return Store
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location.
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set brand.
     *
     * @param string Storerk | Hoyts
     *
     * @return Store
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set extra points
     *
     * @param string
     *
     * @return Store
     */
    public function setExtraPoints($extraPoints)
    {
        $this->extraPoints = $extraPoints;

        return $this;
    }


    public function getExtraPoints()
    {
        return $this->extraPoints;
    }

    /**
     * @param File|null $file
     */
    public function setImage(File $file = null)
    {
        $this->image = $file;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return Employee[]|Collection
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * @param Employee $employees
     */
    public function setEmployees($employees)
    {
        $this->employees = $employees;
    }

    /**
     * @return Goal[]|Collection
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * @param Goal[]|Collection $goals
     */
    public function setGoals($goals)
    {
        $this->goals = $goals;
    }

    /**
     * @return Ranking[]|Collection
     */
    public function getRankings()
    {
        return $this->rankings;
    }

    /**
     * @param Ranking[]|Collection $rankings
     */
    public function setRankings($rankings)
    {
        $this->rankings = $rankings;
    }
   
    function getCliente() {
        return $this->cliente;
    }

    function getTipoComercio() {
        return $this->tipoComercio;
    }

    function getSegmento() {
        return $this->segmento;
    }

    function getBandera() {
        return $this->bandera;
    }

    function getResponsableComercial() {
        return $this->responsableComercial;
    }

    function setCliente($cliente) {
        $this->cliente = $cliente;
    }

    function setTipoComercio($tipoComercio) {
        $this->tipoComercio = $tipoComercio;
    }

    function setSegmento($segmento) {
        $this->segmento = $segmento;
    }

    function setBandera($bandera) {
        $this->bandera = $bandera;
    }

    function setResponsableComercial($responsableComercial) {
        $this->responsableComercial = $responsableComercial;
    }

    function getFechaBaja(){
        return $this->fechaBaja;
    }

    function getLocalidad() {
        return $this->localidad;
    }

    function setFechaBaja($fechaBaja) {
        $this->fechaBaja = $fechaBaja;
    }

    function setLocalidad($localidad) {
        $this->localidad = $localidad;
    }

    function getTipoActividad() {
        return $this->tipoActividad;
    }

    function setTipoActividad($tipoActividad) {
        $this->tipoActividad = $tipoActividad;
    }

    /**
     * @return string
     */
    public function getZona()
    {
        return $this->zona;
    }

    /**
     * @param string $zona
     */
    public function setZona($zona)
    {
        $this->zona = $zona;
    }

    /**
     * @return int
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param int $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return string
     */
    public function getOwn()
    {
        return $this->own;
    }

    /**
     * @param string $own
     */
    public function setOwn($own)
    {
        $this->own = $own;
    }


    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }


    /**
     * @return int
     */
    public function getBt()
    {
        return $this->bt;
    }

    /**
     * @param int $bt
     */
    public function setBt($bt)
    {
        $this->bt = $bt;
    }


    /**
     * @return string
     */
    public function getBtUser()
    {
        return $this->btUser;
    }

    /**
     * @param string $btUser
     */
    public function setBtUser($btUser)
    {
        $this->btUser = $btUser;
    }


    /**
     * @return string
     */
    public function getBtToken()
    {
        return $this->btToken;
    }

    /**
     * @param string $btToken
     */
    public function setBtToken($btToken)
    {
        $this->btToken = $btToken;
    }




    /**
     * @param \DateTime $month
     * @param $category
//     * @return Goal
     */
    public function getGoalsByMonthAndCategory(\DateTime $month, $category)
    {
        return $this->goals->filter(function (Goal $goal) use ($month, $category) {
            return
                $goal->getGoalType()->getCategory() === $category
                && $goal->getType() === 'MONTH'
                && $goal->getMonth() == $month->format('n')
                && $goal->getYear() == $month->format('Y');
        });
    }

    /**
     * @param \DateTime $month
     * @param $name
     * @param Category $category
     * @return Goal[]
     */
    public function getGoalsByMonthAndName(\DateTime $month, $name, $category = null)
    {
		
        $result = $this->goals->filter(function (Goal $goal) use ($month, $name, $category) {
            return
                ($category ? $goal->getGoalType()->getCategory() === $category : true)
                && $goal->getGoalType()->getName() === $name
                && $goal->getType() === 'MONTH'
                && $goal->getGoalType()->getYear() != 0
                && $goal->getGoalType()->getMonth() != 0
                && $goal->getMonth() == $month->format('n')
                && $goal->getYear() == $month->format('Y');
        });
        return $result;

    }

    public function getGoalsByYearAndName(\DateTime $year, $name, $category = null)
    {
        $extraPoints = $this->isDisabled($year->format('Y'));

        $result = $this->goals->filter(function (Goal $goal) use ($year, $name, $category) {
            return
                ($category ? $goal->getGoalType()->getCategory() === $category : true)
                && $goal->getGoalType()->getName() === $name
                && $goal->getType() === 'MONTH'
                //  && ($goal->getResult() == 0 &&  $goal->getObjetive() > 0)
                && $goal->getResult() > 0

                && $goal->getYear() == $year->format('Y');
        });


        $total = 0;
        $cant = 0;

        foreach ($result as $goal) {
            $cant++;

            if ($name == 'TOTAL_STORE_OBJETIVO') {
                $total += $goal->getObjetive();
            }
            else {
                $total += $goal->getResult();
            }
        }


        // EXISTE EN TABLA EXTRA POINT
        if(!empty($extraPoints)){

            // TIPO PUNTOS ACUMULADOS
            if($name != 'TOTAL_STORE_OBJETIVO') {
                $total = $extraPoints->getPoints();
            }
            // TIPO PUNTOS OBJETIVO + EXTRA OBJETIVOS
            else{
                $total=(int)(round($total / $cant))+Store::EXTRA_POINTS;
            }
        }
        else{
            if($cant > 0)
                $total=(int)(round($total / $cant));
            else
                $total=0;
        }


        return $total;
    }

    /**
     * @param \DateTime $month
     * @param $store
     * @return Ranking[]
     * @internal param $name
     */
    public function getRankingByMonthAndYear(\DateTime $month, $store)
    {
        return $this->rankings->filter(function (Ranking $ranking) use ($month, $store) {
            return
                $ranking->getStore() === $store
                && $ranking->getMonth() == $month->format('n')
                && $ranking->getYear() == $month->format('Y');
        });
    }

    public function getRankingByYear(\DateTime $year)
    {
        return $this->rankings->filter(function (Ranking $ranking) use ($year) {
            return $ranking->getYear() == $year->format('Y')
            && $ranking->getType() == 'YEAR';
        });
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s - %s', $this->code, $this->location);
    }

    /**
     * Get fullname.
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->code.' - '.$this->location  ;
    }

    /**
     * @param integer $month
     * @param integer $year
     * @return Ranking
     */
    public function getMonthRanking($month, $year)
    {
        return $this->rankings->filter(function (Ranking $ranking) use ($month, $year) {
            return $ranking->getType() === 'MONTH' && $ranking->getYear() == $year && $ranking->getMonth() == $month;
        })->first();
    }

    /**
     * @param integer $year
     * @return Ranking
     */
    public function getYearRanking($year)
    {
        return $this->rankings->filter(function (Ranking $ranking) use ($year) {
            return $ranking->getType() === 'YEAR' && $ranking->getYear() == $year;
        })->first();
    }

    /**
     * Esto es para identificar cines que han sido DESCALIFICADOS
     *
     * @return boolean
     */
    public function isDisabled($year){
        $extraPoint=$this->extraPoints->filter(function (ExtraPointsStore $extraPoint) use ($year) {
            return $extraPoint->getType() === 'AUSENTISM' && $extraPoint->getYear() == $year;
        })->first();

        if(!empty($extraPoint))
            return $extraPoint;
        else
            return null;
    }

    /**
     * Esto es para identificar cines que han sido DESCALIFICADOS
     *
     * @return string
     */
    public function getDisabledMessage(){

    }

    /**
     * @return mixed
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * @param mixed $provincia
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;
    }


}
