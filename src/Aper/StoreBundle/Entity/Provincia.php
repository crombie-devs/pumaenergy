<?php

namespace Aper\StoreBundle\Entity;

use Aper\IncentiveBundle\Entity\Goal;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Provincia
 *
 * @ORM\Table(name="provincia")
 * @ORM\Entity(repositoryClass="Aper\StoreBundle\Repository\ProvinciaRepository")
 */
class Provincia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $nombre;
    
    /**
     * One provincia has many features. This is the inverse side.
     * @ORM\OneToMany(targetEntity="Localidad", mappedBy="provincia")
     */
    private $localidades;
        
    /** 
     * @var date
     * @ORM\Column(name="fecha_baja", type="date", nullable=true)
     */
    private $fechaBaja;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Provincia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
//
//    function getLocalidades() {
//        return $this->localidades;
//    }
//
//    function setLocalidades($localidades) {
//        $this->localidades = $localidades;
//    }

    /**
     * @return Localidad[]|Collection
     */
    public function getLocalidades()
    {
        return $this->localidades;
    }

    /**
     * @param Localidad[]|Collection $localidades
     */
    public function setLocalidades($localidades)
    {
        $this->localidades = $localidades;
    }

    /**
     * Remove localidad
     * @param Localidad $localidad
     */
    public function removeLocalidad( Localidad $localidad ) {
        $this->localidades->removeElement( $localidad );
    }

    /**
     * Add Localidad
     * @param Localidad $localidad
     */
    public function addLocalidad( Localidad $localidad ) {
        $this->localidades[] = $localidad;
    }
    
    function getFechaBaja(){
        return $this->fechaBaja;
    }

    function setFechaBaja($fechaBaja) {
        $this->fechaBaja = $fechaBaja;
    }

    public function __toString()
    {
        return $this->nombre;
    }

}

