<?php

namespace Aper\StoreBundle\DataFixtures\ORM;

use Aper\StoreBundle\Entity\Store;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class StoreFixture extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $store = new Store();
        $store->setCode('Code');
        $store->setLocation('Location');
        $store->setBrand('Brand');

        $manager->persist($store);
        $manager->flush();

        $this->setReference('store', $store);
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}
