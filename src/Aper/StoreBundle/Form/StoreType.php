<?php

namespace Aper\StoreBundle\Form;

use Aper\StoreBundle\Entity\Store;
use Aper\StoreBundle\Entity\Provincia;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;

class StoreType extends AbstractType
{
    /**
     * @var EntityManager
     */
    protected $em;

   public function setEntityManager(EntityManagerInterface  $entityManager){
       $this->em = $entityManager;
   }
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', TextType::class)
            ->add('location', TextType::class)
            ->add('cliente', TextType::class, ['required' => false])
            ->add('address', TextType::class, ['required' => false])
            ->add('provincia', EntityType::class, array(
                'class' => 'StoreBundle:Provincia',
                'choice_label' => 'nombre',
                'placeholder' => '-- Seleccione --',
            ))
            ->add('zona', ChoiceType::class, array(
                'choices'  => $this->getOpcionesZonas(),
                'placeholder' => '-- Seleccione --',
                'choices_as_values' => true,))
            ->add('tipoComercio', ChoiceType::class, array(
                'choices'  => $this->getOpcionesTipoComercios(),
                    'placeholder' => '-- Seleccione --',
                    'choices_as_values' => true,))
            // ->add('segmento', ChoiceType::class, array(
            //     'choices'  => $this->getOpcionesSegmentos(),
            //     'placeholder' => '-- Seleccione --',
            //     'choices_as_values' => true,))
            // ->add('tipoActividad', EntityType::class, array(
            //     'class' => 'StoreBundle:TipoActividad',
            //     'choice_label' => 'nombre',
            //     'placeholder' => '-- Seleccione --',
            // ))
            // ->add('bandera', ChoiceType::class, array(
            //     'choices'  => $this->getOpcionesBanderas(),
            //     'placeholder' => '-- Seleccione --',
            //     'choices_as_values' => true,))
            ->add('responsableComercial', TextType::class)
            ->add('enabled', ChoiceType::class, array(
                'choices'  => $this->getOpcionesEnable(),
                'placeholder' => '-- Seleccione --',
                'choices_as_values' => true,))
            ->add('own', TextType::class, ['required' => false])
            ->add('bt', TextType::class, ['required' => false, 'label' => 'Be There - ID'])
            ->add('btUser', TextType::class, ['required' => false, 'label' => 'Be There - UserName'])
            ->add('btToken', TextType::class, ['required' => false, 'label' => 'Be There - Token'])
            ->add('save', SubmitType::class);

        $formModifier = function (FormInterface $form, Provincia $provincia = null) {
            $localidades = null === $provincia ? array() : $provincia->getLocalidades();

            $form->add('localidad', EntityType::class, array(
                'class' => 'StoreBundle:Localidad',
                'placeholder' => '',
                'choices' => $localidades,
                'choices_as_values' => true,
            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();

                $formModifier($event->getForm(), $data->getProvincia());
            }
        );

        $builder->get('provincia')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $provincia = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $provincia);
            }
        );
    }
    

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Store::class,
            'attr' => ['novalidate' => 'novalidate'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'store';
    }

    protected function addElements(FormInterface $form, Provincia $province = null) {
        $submit = $form->get('save');
        $form->remove('save');

        $form->add('provincia', EntityType::class, array(
            'data' => $province,
            'class' => 'StoreBundle:Provincia',
            'choice_label' => 'nombre',
            'placeholder' => '-- Seleccione --',
        ));

        $localidades = array();

        if ($province) {
            $repo = $this->em->getRepository('StoreBundle:Localidad');
            $localidades = $repo->findByProvincia($province);
        }

        $form->add('localidad', EntityType::class, array(
            // looks for choices from this entity
            'class' => 'StoreBundle:Localidad',
            // uses the User.username property as the visible option string
            'choice_label' => 'nombre',
            'placeholder' => '-- Seleccione --',
            'choices' => $localidades,
        ));

        // Add submit button again, this time, it's back at the end of the form
        $form->add($submit);
    }


    function onPreSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();

        // Note that the data is not yet hydrated into the entity.

        $provincia = $this->em->getRepository('StoreBundle:Provincia')->find($data['provincia']);
        $this->addElements($form, $provincia);
    }


    function onPreSetData(FormEvent $event) {
        /** @var Store $estacion */
        $estacion = $event->getData();
        $form = $event->getForm();

        // We might have an empty account (when we insert a new account, for instance)
        $provincia = $estacion->getProvincia() ? $estacion->getProvincia() : null;
        $this->addElements($form, $provincia);
    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\StoreBundle\Entity\Store'
        ));
    }

    private function getOpcionesTipoComercios()
    {
        return array(
            'EESS' => 'EESS',
            'HIPER' => 'HIPER'
        );
    }

    private function getOpcionesEnable()
    {
        return array(
            'Activa' => 1,
            'Inactiva' => 0
        );
    }


    private function getOpcionesBanderas()
    {
        return array(
            'Blanca' => 'Blanca',
            'Puma' => 'Puma',
            'Proximamente' => 'Proximamente'
        );
    }

    private function getOpcionesSegmentos()
    {
        return array(
            '#N/D' => '#N/D',
            'Náutica' => 'Nautica',
            'Rutera' => 'Rutera',
            'Rutera Pueblo' => 'Rutera Pueblo',
            'Rutera Pueblo' => 'Rutera Pueblo',
            'Rutera Urbana' => 'Rutera Urbana',
            'Urbana' => 'Urbana',
            'Urbana Pueblo' => 'Urbana Pueblo',
            'Urbana Top' => 'Urbana Top',
        );
    }

    private function getOpcionesZonas()
    {
        return array(
            'Red Propia' => 'Red Propia',
            'Centro' => 'Centro',
            'Norte' => 'Norte',
            'Sur' => 'Sur'
        );
    }

}
