<?php

namespace Aper\StoreBundle\Form;

use Aper\UserBundle\Entity\Employee;
use Aper\UserBundle\Form\AchievementType;
use Aper\UserBundle\Form\EducationType;
use Aper\UserBundle\Form\SentimentalStatusType;
use Aper\UserBundle\Form\SonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class EmployeeSubRolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        ini_set('xdebug.max_nesting_level', 2000);
        $perfil = "Perfil 2";
        $builder
            ->add('subRol', EntityType::class, array(
                'class' => 'UserBundle:SubRol',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('sr')
                        ->where('sr.name LIKE :name')
                        ->setParameter('name', "%Perfil 2%")
                        ->orderBy('sr.id', 'ASC');
                },
                'expanded' => false,
                'multiple' => false,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Employee::class,

            ]);
    }

    public function getBlockPrefix()
    {
        return 'employee';
    }
}
