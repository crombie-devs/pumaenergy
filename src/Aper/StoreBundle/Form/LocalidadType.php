<?php

namespace Aper\StoreBundle\Form;

use Aper\StoreBundle\Entity\Store;
use Aper\StoreBundle\Entity\Localidad;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocalidadType extends AbstractType
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function setEntityManager(EntityManagerInterface  $entityManager){
        $this->em = $entityManager;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre')
            ->add('provincia', EntityType::class, array(
                // looks for choices from this entity
                'class' => 'StoreBundle:Provincia',
                // uses the User.username property as the visible option string
                'choice_label' => 'nombre',
                'placeholder' => '-- Seleccione --',
            ))
            ->add('save', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
//        $resolver->setDefaults(array(
//            'data_class' => 'Aper\StoreBundle\Entity\Localidad'
//        ));
        $resolver->setDefaults([
            'data_class' => Localidad::class,
            'attr' => ['novalidate' => 'novalidate'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'localidad';
    }


}
