<?php

namespace Aper\StoreBundle\Controller;

use Aper\StoreBundle\Form\StoreFilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Aper\StoreBundle\Entity\Store;
use Aper\StoreBundle\Form\StoreType;
use WebFactory\Bundle\GridBundle\Event\ManipulateQueryEvent;
use WebFactory\Bundle\GridBundle\Grid\Configuration;

class StoreController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $config = new Configuration(
            $this->getDoctrine()->getRepository(Store::class)->getQueryBuilder(),
            $this->get('web_factory_grid.form_filter_factory')->create(StoreFilterType::class),
            'main',
            'StoreBundle:Store:index.html.twig'
        );

        $grid = $this->get('web_factory_grid.factory')->create($config);

        $grid->addListener(ManipulateQueryEvent::NAME, function (ManipulateQueryEvent $event) {
            $queryBuilder = $event->getQueryBuilder();
            $filters = $event->getFilters();

            if (null !== $filters['code']) {
                $queryBuilder->andWhere('Store.code LIKE :code')->setParameter('code', "%{$filters['code']}%");
            }

            if (null !== $filters['location']) {
                $queryBuilder->andWhere('Store.location LIKE :location')->setParameter('location', "%{$filters['location']}%");
            }

        });

        return $grid->handle($request);
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $store = new Store();

        $form = $this->createForm(StoreType::class, $store);
        $form->handleRequest($request);

//        print($form->isValid());
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($store);
            $em->flush();

            $this->addFlash('mensaje', 'Ha sido creada una nueva Estacion');

            return $this->redirectToRoute('aper_store_index');
        }

        return $this->render('StoreBundle:Store:add.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $store = $em->getRepository('StoreBundle:Store')->find($id);
        $form = $this->createForm(StoreType::class, $store);
        $form->handleRequest($request);

        if (!$store) {
            throw $this->createNotFoundException('Estacion no encontrado');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('mensaje', 'La Estacion ha sido modificada');

            return $this->redirectToRoute('aper_store_index');
        }

        return $this->render('StoreBundle:Store:edit.html.twig', ['store' => $store, 'form' => $form->createView()]);
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $store = $em->getRepository('StoreBundle:Store')->find($id);

        if ($store->getEmployees()->isEmpty()) {
            $em->remove($store);
            $em->flush();
            $this->addFlash('success', 'Estacion eliminada');
        } else {
            $this->addFlash('error', 'La Estacion tiene empleados asignados, no puede ser eliminada');
        }

        return $this->redirectToRoute('aper_store_index');
    }

    public function showAction($id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('StoreBundle:Store');

        $store = $repository->find($id);

        if (!$store) {
            $messageException = 'Estacion no encontrada';
            throw $this->createNotFoundException($messageException);
        }

        return $this->render('StoreBundle:Store:show.html.twig', ['store' => $store]);
    }
}
