<?php

namespace Aper\StoreBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Aper\UserBundle\Entity\Employee;
use Aper\IncentiveBundle\Entity\GoalType;
use Aper\IncentiveBundle\Entity\Goal;

/**
 * Class HomepageController
 * @package Aper\StoreBundle\Controller
 * @Route("/")
 */
class HomepageController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Route("/", name="frontend_homepage")
     * @Template()
     * @return array
     */
    public function indexAction()
    {
        if (isset($_GET['mistery'])) {


            ini_set('max_execution_time', 9000);
            ini_set("memory_limit",-1);

            $repoStores = $this->getDoctrine()->getRepository('StoreBundle:Store');
            $stores = $repoStores->findAllIsBt();


            $month = (isset($_GET['month']) ? $_GET['month'] : 0);
            $year = (isset($_GET['year']) ? $_GET['year'] : 0);

            switch ($month) {
                case 1:
                case 2:
                    $projectID = 0;
                    $month = 1;
                    break;
                case 3:
                case 4:
                    $projectID = 0;
                    $month = 3;
                    break;
                case 5:
                case 6:
                    $projectID = 0;
                    $month = 5;
                    break;
                case 7:
                case 8:
                    $projectID = 3299;
                    $month = 7;
                    break;
                case 9:
                case 10:
                    $projectID = 3315;
                    $month = 9;
                    break;
                case 11:
                case 12:
                    $projectID = 3328; 
                    $month = 11;
                    break;
            }

            if (!$month || !$year || !$projectID) {
                echo 'Parámetros erróneos';exit;
            }



            if (isset($_GET['lucas'])) {
                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();

                $goalTypesList = [];

                $repoGoalType = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');
                $goalTypes = $repoGoalType->findAllWithBt();

                $repoGoal = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');

                $sql = 'INSERT INTO `goal` (`goaltype_id`, `store_id`, `month`, `year`, `type`, `result`, `objetive`, `result_met`, `objetive_met`, `symbolf`, `symbolb`) VALUES ';
                $sqls = [];

                $storeId = 370;
                foreach ($goalTypes as $key => $goalType) {
                    $goal = $repoGoal->findLastGoalByGoalTypeAndStore($goalType->getId(), $storeId, $month, $year);

                    if ($goal) {
                        // var_dump($goal[0]->getId());exit;
                        $sqls[] = "({$goal[0]->getGoalType()->getId()}, {$storeId}, '{$month}', '{$year}', 'MONTH', '0', '{$goal[0]->getObjetive()}', '0', '{$goal[0]->getObjetiveMet()}', '', '')";
                    }
                }


                // Buscamos los últimos objetivos subidos
                $goalType = $this->getDoctrine()->getRepository(GoalType::class)->findByMonthAndYear($year, $month);
                $goalTypeMonth = $goalType[0]->getMonth();
                $goalTypeYear = $goalType[0]->getYear();

                // Traemos el Goal Type PUNTAJES OBJETIVO de Mystery
                $goalTypeObjetive = $this->getDoctrine()->getRepository(GoalType::class)->findOneBy([
                    'month' => $goalTypeMonth,
                    'year' => $goalTypeYear,
                    'name' => "PUNTAJES OBJETIVO",
                    'category' => "MYSTERY SHOPPER"
                ]);

                $id_GoalType_Objetive = 0;
                if ($goalTypeObjetive !== null) {
                    $id_GoalType_Objetive = $goalTypeObjetive->getId();
                }


                // Traemos el Goal Type PUNTAJES OBJETIVO de Mystery
                $goalTypeResult = $this->getDoctrine()->getRepository(GoalType::class)->findOneBy([
                    'month' => $goalTypeMonth,
                    'year' => $goalTypeYear,
                    'name' => "PUNTAJES RESULTADO",
                    'category' => "MYSTERY SHOPPER"
                ]);

                $id_GoalType_Result = 0;
                if ($goalTypeResult !== null) {
                    $id_GoalType_Result = $goalTypeResult->getId();
                }

                $sqls[] = "({$id_GoalType_Objetive}, {$storeId}, '{$month}', '{$year}', 'MONTH', '40', '40', '0', '0', '', '')";
                // $sqls[] = "({$id_GoalType_Objetive}, {$storeId}, '{$month}', '{$year}', 'MONTH', '40', '40', '0', '0', '', '')";

                $sql .= implode(',', $sqls);
                $sql .= ';';

                $stmt = $conn->prepare($sql);
                $stmt->execute();




                exit;

            }


            $results = [];
            $resultsNro = [];

            if (isset($_GET['code'])) {

                $store = $repoStores->findOneByCode($_GET['code']);

                if ($store) {
                    $result = $repoStores->findMysteryByBt($store->getBt());
                    if ($result['success']) {
                        foreach ($result['data'] as $key => $data) {
                            // print_r($data['Projects']);
                            if ($data['Projects'][0]['ProjectID'] == $projectID) {
                                $results[$store->getId()] = $data['Chapters'];
                                $resultsNro[$store->getId()] = $data['Result'];                                
                            }
                        }
                    }
                } else {
                    die('No existe una estación con el código: ' . $_GET['code']);
                }

            } else {
                foreach ($stores as $key => $store) {
                    $result = $repoStores->findMysteryByBt($store->getBt());
                    if ($result['success']) {
                        foreach ($result['data'] as $key => $data) {
                            if ($data['Projects'][0]['ProjectID'] == $projectID) {
                                $results[$store->getId()] = $data['Chapters'];
                                $resultsNro[$store->getId()] = $data['Result'];
                            }
                        }
                    }
                }
            }
            // echo 'no habilitado por el momento';exit;


            // var_dump($results, true);exit;
            // var_dump($results, true);exit;

            if ($results) {

                $goalTypesList = [];

                $repoGoalType = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');
                $goalTypes = $repoGoalType->findAllWithBt($month, $year);

                foreach ($goalTypes as $key => $goalType) {
                    if ($month == $goalType->getMonth() && $year == $goalType->getYear()) {
                        $goalTypesList[$goalType->getBt()] = $goalType->getId();
                    }
                }



                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();


                // Buscamos los últimos objetivos subidos
                $goalType = $this->getDoctrine()->getRepository(GoalType::class)->findByMonthAndYear($year, $month);
                $goalTypeMonth = $goalType[0]->getMonth();
                $goalTypeYear = $goalType[0]->getYear();


                foreach ($results as $storeId => $values) {

                    $goalInserts = [];

                    foreach ($values as $key => $value) {
                        // Verificamos que sea una categoría principal
                        if (isset($goalTypesList[$value['ChapterID']])) {
                            if (!is_null($value['ChapterResult'])) {
                                $goalInserts[] = [
                                    $value['ChapterName'],
                                    $goalTypesList[$value['ChapterID']],    // goaltype_id
                                    $storeId,                               // store_id
                                    $month,                                 // month
                                    $year,                                  // year
                                    'MONTH',                                // type
                                    $value['ChapterResult'],                // result
                                    100,                                    // objetive
                                    $value['ChapterResult'],                // result_met
                                    100,                                    // objetive_met
                                ];

                            } else {
                                $goalInserts[] = [
                                    $value['ChapterName'],
                                    $goalTypesList[$value['ChapterID']],    // goaltype_id
                                    $storeId,                               // store_id
                                    $month,                                 // month
                                    $year,                                  // year
                                    'MONTH',                                // type
                                    0,                                      // result
                                    0,                                      // objetive
                                    0,                                      // result_met
                                    0,                                      // objetive_met
                                ];
                            }

                            foreach ($value['SubChapters'] as $subKey => $subChapter) {

                                // Verificamos si está dentro de las subcategorías
                                if (isset($goalTypesList[$subChapter['SubchapterID']])) {
                                    if (!is_null($subChapter['SubchapterResult'])) {
                                        $goalInserts[] = [
                                            $subChapter['SubchapterName'],
                                            $goalTypesList[$subChapter['SubchapterID']],    // goaltype_id
                                            $storeId,                                       // store_id
                                            $month,                                         // month
                                            $year,                                          // year
                                            'MONTH',                                        // type
                                            $subChapter['SubchapterResult'],                // result
                                            100,                                            // objetive
                                            $subChapter['SubchapterResult'],                // result_met
                                            100,                                            // objetive_met
                                        ];
                                    } else {
                                        $goalInserts[] = [
                                            $subChapter['SubchapterName'],
                                            $goalTypesList[$subChapter['SubchapterID']],    // goaltype_id
                                            $storeId,                                       // store_id
                                            $month,                                         // month
                                            $year,                                          // year
                                            'MONTH',                                        // type
                                            0,                                              // result
                                            0,                                              // objetive
                                            0,                                              // result_met
                                            0,                                              // objetive_met
                                        ];
                                    }

                                } else {

                                    // Buscamos en todas las Questions
                                    foreach ($subChapter['Questions'] as $questionKey => $question) {

                                        // Verificamos si está dentro de una Question
                                        if (isset($goalTypesList[$question['QuestionID']])) {

                                            unset($question['Answers']['FreeText']);

                                            $tttotal = 0;
                                            $noAplica = false;
                                            foreach ($question['Answers'] as $ansId => $ansArr) {
                                                if (!is_null($ansArr['AnswerValue'])) {
                                                    $tttotal = $tttotal + $ansArr['AnswerValue'];
                                                } else {
                                                    $noAplica = true;
                                                }
                                            }

                                            if (!$noAplica) {
                                                $goalInserts[] = [
                                                    $question['Question'],
                                                    $goalTypesList[$question['QuestionID']],    // goaltype_id
                                                    $storeId,                                   // store_id
                                                    $month,                                     // month
                                                    $year,                                      // year
                                                    'MONTH',                                    // type
                                                    $tttotal,                                   // result
                                                    100,                                        // objetive
                                                    $tttotal,                                   // result_met
                                                    100,                                        // objetive_met
                                                ];
                                            } else {
                                                $goalInserts[] = [
                                                    $question['Question'],
                                                    $goalTypesList[$question['QuestionID']],    // goaltype_id
                                                    $storeId,                                   // store_id
                                                    $month,                                     // month
                                                    $year,                                      // year
                                                    'MONTH',                                    // type
                                                    0,                                          // result
                                                    0,                                          // objetive
                                                    0,                                          // result_met
                                                    0,                                          // objetive_met
                                                ];
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }



                    $em = $this->getDoctrine()->getManager();
                    $conn = $em->getConnection();


                    // Traemos el Goal Type PUNTAJES OBJETIVO de Mystery
                    $goalTypeObjetive = $this->getDoctrine()->getRepository(GoalType::class)->findOneBy([
                        'month' => $goalTypeMonth,
                        'year' => $goalTypeYear,
                        'name' => "PUNTAJES OBJETIVO",
                        'category' => "MYSTERY SHOPPER"
                    ]);

                    $id_GoalType_Objetive = 0;
                    if ($goalTypeObjetive !== null) {
                        $id_GoalType_Objetive = $goalTypeObjetive->getId();
                    }


                    // Traemos el Goal Type PUNTAJES OBJETIVO de Mystery
                    $goalTypeResult = $this->getDoctrine()->getRepository(GoalType::class)->findOneBy([
                        'month' => $goalTypeMonth,
                        'year' => $goalTypeYear,
                        'name' => "PUNTAJES RESULTADO",
                        'category' => "MYSTERY SHOPPER"
                    ]);

                    $id_GoalType_Result = 0;
                    if ($goalTypeResult !== null) {
                        $id_GoalType_Result = $goalTypeResult->getId();
                    }


                    // PUNTAJES OBJETIVO
                    $goalInserts[] = [
                        'TOTAL',
                        $id_GoalType_Objetive,      // goaltype_id
                        $storeId,                   // store_id
                        $month,                     // month
                        $year,                      // year
                        'MONTH',                    // type
                        40,                         // result
                        40,                         // objetive
                        0,                          // result_met
                        0,                          // objetive_met
                    ];

                    // PUNTAJES RESULTADO
                    $goalInserts[] = [
                        'TOTAL',
                        $id_GoalType_Result,                    // goaltype_id
                        $storeId,                               // store_id
                        $month,                                 // month
                        $year,                                  // year
                        'MONTH',                                // type
                        $resultsNro[$storeId],                  // result (antes estaba ($resultsNro[$storeId] * 40 / 100))
                        100,                                    // objetive
                        ($resultsNro[$storeId] * 40 / 100),                                      // result_met
                        0,                                      // objetive_met
                    ];


                    // Obtenemos el valor anterior del puntaje de la categoría
                    $repoGoal = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
                    $goal = $repoGoal->findGoalByGoalTypeAndStore($id_GoalType_Result, $storeId, $month, $year);
                    $resultadoAnterior = 0;
                    if ($goal) {
                        $resultadoAnterior = $goal[0]->getResult();
                    }


                    $sqlDeletes = [];
                    $sql = 'INSERT INTO `goal` (`goaltype_id`, `store_id`, `month`, `year`, `type`, `result`, `objetive`, `result_met`, `objetive_met`, `symbolf`, `symbolb`) VALUES ';

                    $sqls = [];
                    foreach ($goalInserts as $key => $value) {
                        $sqls[] = "({$value[1]}, {$storeId}, '$month', '$year', 'MONTH', '{$value[6]}', '{$value[7]}', '{$value[8]}', '{$value[8]}', '', '')";
                        $sqlDeletes[] = $value[1];
                    }
                    $sql .= implode(',', $sqls);
                    $sql .= ';';


                    $sqlDeletes = "DELETE FROM `goal` WHERE `store_id` = '{$storeId}' AND `goaltype_id` IN (" . implode(',', $sqlDeletes) . ") AND `month` = '{$month}' AND `year` = '{$year}';";



                    // Traemos el Goal Type PUNTAJES OBJETIVO de Mystery
                    $goalTypeTotal_Result = $this->getDoctrine()->getRepository(GoalType::class)->findOneBy([
                        'month' => $goalTypeMonth,
                        'year' => $goalTypeYear,
                        'name' => "TOTAL_STORE_RESULTADO",
                        'category' => "TOTAL_STORE_RESULTADO"
                    ]);

                    $id_GoalType_Total_Result = 0;
                    if ($goalTypeTotal_Result !== null) {
                        $id_GoalType_Total_Result = $goalTypeTotal_Result->getId();
                    }


                    $goalFinal = $repoGoal->findGoalByGoalTypeAndStore($id_GoalType_Total_Result, $storeId, $month, $year);
                    if ($goalFinal) {
                        // Al puntaje final de la estación, le sumamos el nuevo valore, pero le restamos el anterior valor de la última vez ejecutado
                        $sqlUpdate = "UPDATE `goal` SET `result` = (`result` + " . (($resultsNro[$storeId] * 40 / 100) - $resultadoAnterior) . ") WHERE `store_id` = '{$storeId}' AND `month` = '{$month}' AND `year` = '{$year}' AND `goaltype_id` = '{$id_GoalType_Total_Result}';";
                    } else {
                        // Al puntaje final de la estación, tenemos que insertarlo
                        $r = ($resultsNro[$storeId] * 40 / 100);
                        $sqlUpdate = 'INSERT INTO `goal` (`goaltype_id`, `store_id`, `month`, `year`, `type`, `result`, `objetive`, `result_met`, `objetive_met`, `symbolf`, `symbolb`) VALUES ' . "({$id_GoalType_Total_Result}, {$storeId}, '$month', '$year', 'MONTH', '{$r}', '0', '0', '0', '', '')";
                    }



                    $stmt = $conn->prepare($sqlDeletes);
                    $stmt->execute();

                    $stmt = $conn->prepare($sql);
                    $stmt->execute();

                    $stmt = $conn->prepare($sqlUpdate);
                    $stmt->execute();

                    print_r($sqlDeletes);
                    print_r('<br>');
                    print_r($sql);
                    print_r('<br>');
                    print_r($sqlUpdate);
                    print_r('<br>---------------------<br>');
                    // exit;
                }

            }

            exit;
        }



        $repo = $this->getDoctrine()->getRepository('UserBundle:User');
				
        $user = $this->getUser();
		
		//VAR_DUMP($user);EXIT;
		
        $birthDaysSameStore = $repo->findNextBirthDays($user, true, 3);
        $birthDaysOtherStore = $repo->findNextBirthDays($user, false, 6);
       
        //TRAE DOS ULTIMAS NOTICIAS DESTACADAS
        $roles = $this->get('security.token_storage')->getToken()->getUser()->getRoles();
        $last2news  =$this->getDoctrine()->getRepository('AperNewsBundle:News')->findByLast2Highlighted($limit = 3, $roles);
        $slider = $this->getDoctrine()->getRepository('IncentiveBundle:Slider')->findSliderByRoles($roles);

        $homeDestacados = $this->getDoctrine()->getRepository('IncentiveBundle:HomeDestacado')->findHomeByRoles($roles);
        $ruffle = $this->getDoctrine()->getRepository('AperRuffleBundle:Ruffle')->findLastestRuffle(1, $roles);

        $empleador = $user->getEmployee();
        $estacion = $empleador->getStore();
        $empleadosRepository = $this->getDoctrine()->getRepository(Employee::class);

        return compact('birthDaysSameStore', 'birthDaysOtherStore', 'slider','last2news', 'ruffle','homeDestacados');
    }

    /**
     * @Route("/birthdays")
     * @Template()
     * @return array
     */
    public function birthdaysAction()
    {
        $repo = $this->getDoctrine()->getRepository('UserBundle:User');
        $birthDaysSameStore = $repo->findNextBirthDays($this->getUser(), true, 2000);
        $birthDaysOtherStore = $repo->findNextBirthDays($this->getUser(), false, 2000);

        return compact('birthDaysSameStore', 'birthDaysOtherStore');
    }

    /**
     * @Route("/term")
     * @Template()
     * @return array
     */
    public function termAction()
    {
        $term = $this->getDoctrine()->getRepository('StoreBundle:Term')->findOneBy(array(),array('id'=>'DESC'),1,0);
        return compact('term');
    }
}
