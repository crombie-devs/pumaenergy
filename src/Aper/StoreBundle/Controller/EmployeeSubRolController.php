<?php

namespace Aper\StoreBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Aper\UserBundle\Entity\Employee;
use Aper\IncentiveBundle\Entity\GoalType;
use Aper\IncentiveBundle\Entity\Goal;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Aper\StoreBundle\Form\EmployeeSubRolType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class EmployeeSubRolController
 * @package Aper\StoreBundle\Controller
 * @Route("employee_sub_rol")
 */
class EmployeeSubRolController extends Controller
{
    /**
     * @Route("/", name="sub_rol_form")
     * @Method({"GET", "POST"})
     */
    public function subRolAction(Request $request)
    {
        $user = $this->getUser();
        $empleado = $user->getEmployee();
        $em = $this->getDoctrine()->getManager();

        if ($request->get('submit')) {
            $subRolForm = $request->get('role');

            $subrol = $em->getRepository('UserBundle:SubRol')->findLikeName($subRolForm);
            $empleado->setSubRol($subrol);

            $em->flush($empleado);
    
            return $this->redirectToRoute('homepage');
        }

        return $this->render('StoreBundle:SubRol:index.html.twig');
    }

}