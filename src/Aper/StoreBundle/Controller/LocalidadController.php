<?php

namespace Aper\StoreBundle\Controller;

use Aper\StoreBundle\Entity\Store;
use Aper\StoreBundle\Entity\Localidad;
use Aper\StoreBundle\Form\StoreFilterType;
use Aper\StoreBundle\Form\StoreType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WebFactory\Bundle\GridBundle\Event\ManipulateQueryEvent;
use WebFactory\Bundle\GridBundle\Grid\Configuration;
use Aper\StoreBundle\Form\LocalidadType;
use Aper\StoreBundle\Form\LocalidadFilterType;

class LocalidadController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $config = new Configuration(
            $this->getDoctrine()->getRepository(Localidad::class)->getQueryBuilder(),
            $this->get('web_factory_grid.form_filter_factory')->create(LocalidadFilterType::class),
            'main',
            'StoreBundle:Localidad:index.html.twig'
        );

        $grid = $this->get('web_factory_grid.factory')->create($config);

        $grid->addListener(ManipulateQueryEvent::NAME, function (ManipulateQueryEvent $event) {
            $queryBuilder = $event->getQueryBuilder();
            $queryBuilder->join('Localidad.provincia', 'p');
            $filters = $event->getFilters();

            if (null !== $filters['nombre']) {
                $queryBuilder->andWhere('Localidad.nombre LIKE :nombre')->setParameter('nombre', "%{$filters['nombre']}%");
            }

            if (null !== $filters['provincia']) {
                $queryBuilder->andWhere('p.nombre LIKE :provincia')->setParameter('provincia', "%{$filters['provincia']}%");
            }
            
        });

        return $grid->handle($request);
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $localidad = new Localidad();

        $form = $this->createForm(LocalidadType::class, $localidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($localidad);
            $em->flush();

            $this->addFlash('mensaje', 'Ha sido creada una nueva Localidad');

            return $this->redirectToRoute('localidad_de_estacion_index');
        }

        return $this->render('StoreBundle:Localidad:add.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $localidad = $em->getRepository('StoreBundle:Localidad')->find($id);
        $form = $this->createForm(LocalidadType::class, $localidad);
        $form->handleRequest($request);

        if (!$localidad) {
            throw $this->createNotFoundException('Localidad no encontrada');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('mensaje', 'La Localidad ha sido modificada');

            return $this->redirectToRoute('localidad_de_estacion_index');
        }

        return $this->render('StoreBundle:Localidad:edit.html.twig', ['localidad' => $localidad, 'form' => $form->createView()]);
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $localidad = $em->getRepository('StoreBundle:Localidad')->find($id);

        $em->remove($localidad);
        $em->flush();
        $this->addFlash('success', 'Localidad eliminada');

        return $this->redirectToRoute('localidad_de_estacion_index');
    }

    public function showAction($id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('StoreBundle:Localidad');

        $localidad = $repository->find($id);

        if (!$localidad) {
            $messageException = 'Localidad no encontrada';
            throw $this->createNotFoundException($messageException);
        }

        return $this->render('StoreBundle:Localidad:show.html.twig', ['localidad' => $localidad]);
    }
}
