<?php

namespace Aper\StoreBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Aper\UserBundle\Entity\Employee;
use Aper\IncentiveBundle\Entity\GoalType;
use Aper\IncentiveBundle\Entity\Goal;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Aper\StoreBundle\Form\EmployeeSubRolType;

/**
 * Class HomepageController
 * @package Aper\StoreBundle\Controller
 * @Route("/")
 */
class HomepageController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Route("/", name="frontend_homepage")
     * @Template()
     * @return array
     */
    public function indexAction()
    {
		
		if (isset($_GET['testrolfi'])) {
			//phpinfo(); exit;
			
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://api.estrategiasempresariales.org/api/protocol/reports?year=2022&period=3&code=SS0014',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => false,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkb21haW4iOiJjbG91ZC5lc3RyYXRlZ2lhc2VtcHJlc2FyaWFsZXMub3JnIiwiYWNjZXNzIjoic2lldmFkYXRhY2VudGVyIn0.JQix7TmzZXZ0ps9s5uWjXGGeCnfsBi7AlrAh5-REd6A'
              ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            echo $response;
            exit;
			echo(phpinfo()); exit;
		}
		
		
		
        if (isset($_GET['mistery'])) {
            ini_set('max_execution_time', 9000);
            ini_set("memory_limit",-1);

            $repoStores = $this->getDoctrine()->getRepository('StoreBundle:Store');
            $stores = $repoStores->findAllIsBt();


            $month = (isset($_GET['month']) ? $_GET['month'] : 0);
            $year = (isset($_GET['year']) ? $_GET['year'] : 0);

            switch ($year) {
    			case 2021:
    					switch ($month) {
    						case 1:
    						case 2:
    							$projectID = 0;
    							$month = 1;
    							break;
    						case 3:
    						case 4:
    							$projectID = 0;
    							$month = 3;
    							break;
    						case 5:
    						case 6:
    							$projectID = 0;
    							$month = 5;
    							break;
    						case 7:
    						case 8:
    							$projectID = 3299;
    							$month = 7;
    							break;
    						case 9:
    						case 10:
    							$projectID = 3315;
    							$month = 9;
    							break;
    						case 11:
    						case 12:
    							$projectID = 3328; 
    							$month = 11;
    							break;
    					}
				break;
    			case 2022:
    					switch ($month) {
    						case 1:
    						case 2:
    							$projectID = 3351;
    							$month = 1;
    							break;
    						case 3:
    						case 4:
    							$projectID = 3364; 
    							$month = 3;
    							break;
    						case 5:
    						case 6:
    							$projectID = 0;
    							$month = 5;
    							break;
    						case 7:
    						case 8:
    							$projectID = 0;
    							$month = 7;
    							break;
    						case 9:
    						case 10:
    							$projectID = 0;
    							$month = 9;
    							break;
    						case 11:
    						case 12:
    							$projectID = 0; 
    							$month = 11;
    							break;
    					}
    			break;
    		}

            if (!$month || !$year || !$projectID) {
                echo 'Parámetros erróneos';exit;
            }


            $results = [];
            $resultsNro = [];

            if (isset($_GET['code'])) {

                $store = $repoStores->findOneByCode($_GET['code']);
				
				 

                if ($store) {
					
					// Se mantiene el registro del ranking, solo porque para obtener todos los objetivos, la estación tiene que estar asociada si o si, a un ranking
                   $rankinSql = "DELETE FROM `ranking` WHERE (`store_id` = {$store->getId()}) and (month={$month}) and (year = {$year}); INSERT INTO `ranking` (`store_id`, `month`, `year`, `position`, `type`) VALUES ({$store->getId()}, {$month}, {$year}, '1', 'MONTH'), ({$store->getId()}, {$month}, {$year}, '1', 'YEAR')";
                   $em = $this->getDoctrine()->getManager();
                   $conn = $em->getConnection();
				   $stmt = $conn->prepare($rankinSql);
                   $stmt->execute();
                   $stmt->closeCursor(); 
					
					
                    $result = $repoStores->findMysteryByBt($store->getBt());
					//var_dump($result); exit;
                    if ($result['success']) {
                        foreach ($result['data'] as $key => $data) {
                            // print_r($data['Projects']);
                            if ($data['Projects'][0]['ProjectID'] == $projectID) {
                                $results[$store->getId()] = $data['Chapters'];
                                $resultsNro[$store->getId()] = $data['Result'];                                
                            }
                        }
                    }
					//var_dump($results); exit;
                } else {
                    die('No existe una estación con el código: ' . $_GET['code']);
                }

            } else {
                foreach ($stores as $key => $store) {
					
					// Se mantiene el registro del ranking, solo porque para obtener todos los objetivos, la estación tiene que estar asociada si o si, a un ranking
					$rankinSql = "DELETE FROM `ranking` WHERE (`store_id` = {$store->getId()}) and (month={$month}) and (year = {$year}); INSERT INTO `ranking` (`store_id`, `month`, `year`, `position`, `type`) VALUES ({$store->getId()}, {$month}, {$year}, '1', 'MONTH'), ({$store->getId()}, {$month}, {$year}, '1', 'YEAR')";
                    $em = $this->getDoctrine()->getManager();
                    $conn = $em->getConnection();
					$stmt = $conn->prepare($rankinSql);
                    $stmt->execute();
					$stmt->closeCursor(); 
					
                    $result = $repoStores->findMysteryByBt($store->getBt());
                    if ($result['success']) {
                        foreach ($result['data'] as $key => $data) {
                            if ($data['Projects'][0]['ProjectID'] == $projectID) {
                                $results[$store->getId()] = $data['Chapters'];
                                $resultsNro[$store->getId()] = $data['Result'];
                            }
                        }
                    }
                }
            }
            // echo 'no habilitado por el momento';exit;


            // var_dump($results, true);exit;
            // var_dump($results, true);exit;

            if ($results) {

                $goalTypesList = [];

                $repoGoalType = $this->getDoctrine()->getRepository('IncentiveBundle:GoalType');
                $goalTypes = $repoGoalType->findAllWithBt($month, $year);

                foreach ($goalTypes as $key => $goalType) {
                    $goalTypesList[$goalType->getBt()] = $goalType->getId();
                }



                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();


                // Buscamos los últimos objetivos subidos
                $goalType = $this->getDoctrine()->getRepository(GoalType::class)->findByMonthAndYear($year, $month);
                $goalTypeMonth = $goalType[0]->getMonth();
                $goalTypeYear = $goalType[0]->getYear();


                // print_r($results);exit;
                foreach ($results as $storeId => $values) {

                    $goalInserts = [];

                    foreach ($values as $key => $value) {
                        // Verificamos que sea una categoría principal
                        if (isset($goalTypesList[$value['ChapterID']])) {
                            $goalInserts[] = [
                                $value['ChapterName'],
                                $goalTypesList[$value['ChapterID']],    // goaltype_id
                                $storeId,                               // store_id
                                $month,                                 // month
                                $year,                                  // year
                                'MONTH',                                // type
                                (is_null($value['ChapterResult']) ? 0 : $value['ChapterResult']),                // result
                                (is_null($value['ChapterResult']) ? 0 : 100),                                    // objetive
                                (is_null($value['ChapterResult']) ? 0 : $value['ChapterResult']),                // result_met
                                (is_null($value['ChapterResult']) ? 0 : 100),                                    // objetive_met
                            ];

                            foreach ($value['SubChapters'] as $subKey => $subChapter) {
								
								// var_dump($subChapter); 

                                // Verificamos si está dentro de las subcategorías
                                if (isset($goalTypesList[$subChapter['SubchapterID']])) {
                                    $goalInserts[] = [
                                        $subChapter['SubchapterName'],
                                        $goalTypesList[$subChapter['SubchapterID']],    // goaltype_id
                                        $storeId,                                       // store_id
                                        $month,                                         // month
                                        $year,                                          // year
                                        'MONTH',                                        // type
                                        (is_null($subChapter['SubchapterResult']) ? 0 : $subChapter['SubchapterResult']),                // result
                                        (is_null($subChapter['SubchapterResult']) ? 0 : 100),                                            // objetive
                                        (is_null($subChapter['SubchapterResult']) ? 0 : $subChapter['SubchapterResult']),                // result_met
                                        (is_null($subChapter['SubchapterResult']) ? 0 : 100),                                             // objetive_met
                                    ];

                                } else {

                                    // Buscamos en todas las Questions
                                    foreach ($subChapter['Questions'] as $questionKey => $question) {

                                        // Verificamos si está dentro de una Question
                                        if (isset($goalTypesList[$question['QuestionID']])) {

                                            unset($question['Answers']['FreeText']);

                                            $tttotal = 0;
                                            $noAplica = false;
                                            foreach ($question['Answers'] as $ansId => $ansArr) {
                                                if (!is_null($ansArr['AnswerValue'])) {
                                                    $tttotal = $tttotal + $ansArr['AnswerValue'];
                                                } else {
                                                    $noAplica = true;
                                                }
                                            }

                                            if (!$noAplica) {
                                                $goalInserts[] = [
                                                    $question['Question'],
                                                    $goalTypesList[$question['QuestionID']],    // goaltype_id
                                                    $storeId,                                   // store_id
                                                    $month,                                     // month
                                                    $year,                                      // year
                                                    'MONTH',                                    // type
                                                    $tttotal,                                   // result
                                                    100,                                        // objetive
                                                    $tttotal,                                   // result_met
                                                    100,                                       // objetive_met
                                                ];
                                            } else {

                                                $goalInserts[] = [
                                                    $question['Question'],
                                                    $goalTypesList[$question['QuestionID']],    // goaltype_id
                                                    $storeId,                                   // store_id
                                                    $month,                                     // month
                                                    $year,                                      // year
                                                    'MONTH',                                    // type
                                                    0,                                   // result
                                                    0,                                        // objetive
                                                    0,                                   // result_met
                                                    0,                                       // objetive_met
                                                ];
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }



                    $em = $this->getDoctrine()->getManager();
                    $conn = $em->getConnection();


                    // Traemos el Goal Type PUNTAJES OBJETIVO de Mystery
                 //  $goalTypeObjetive = $this->getDoctrine()->getRepository(GoalType::class)->findOneBy([
                 //      'month' => $goalTypeMonth,
                 //      'year' => $goalTypeYear,
                 //      'name' => "PUNTAJES OBJETIVO",
                 //      'category' => "MYSTERY SHOPPER"
                 //  ]);
					
					
					$stmt = $conn->prepare( "SELECT * FROM goal_type where month = $goalTypeMonth and year = $goalTypeYear and name = 'PUNTAJES OBJETIVO' and category = 'MYSTERY SHOPPER' ");
					$stmt->execute();
					$goalTypeObjetive = $stmt->fetchAll();

                    $id_GoalType_Objetive = 0;
                    if ($goalTypeObjetive !== null) {
                        $id_GoalType_Objetive = $goalTypeObjetive[0]["id"];
                    }


                    // Traemos el Goal Type PUNTAJES RESULTADO de Mystery
                 //  $goalTypeResult = $this->getDoctrine()->getRepository(GoalType::class)->findOneBy([
                 //      'month' => $goalTypeMonth,
                 //      'year' => $goalTypeYear,
                 //      'name' => "PUNTAJES RESULTADO",
                 //      'category' => "MYSTERY SHOPPER"
                 //  ]);
					
					$stmt = $conn->prepare( "SELECT * FROM goal_type where month = $goalTypeMonth and year = $goalTypeYear and name = 'PUNTAJES RESULTADO' and category = 'MYSTERY SHOPPER' ");
                    $stmt->execute();
                    $goalTypeResult = $stmt->fetchAll();

                    $id_GoalType_Result = 0;
                    if ($goalTypeResult !== null) {
                        $id_GoalType_Result = $goalTypeResult[0]["id"];
                    }


                    // PUNTAJES OBJETIVO
                    $goalInserts[] = [
                        'TOTAL',
                        $id_GoalType_Objetive,      // goaltype_id
                        $storeId,                   // store_id
                        $month,                     // month
                        $year,                      // year
                        'MONTH',                    // type
                        40,                         // result
                        40,                         // objetive
                        0,                          // result_met
                        0,                          // objetive_met
                    ];

                    // PUNTAJES RESULTADO
                    $goalInserts[] = [
                        'TOTAL',
                        $id_GoalType_Result,                    // goaltype_id
                        $storeId,                               // store_id
                        $month,                                 // month
                        $year,                                  // year
                        'MONTH',                                // type
                        ($resultsNro[$storeId]),     // result
                        100,                                    // objetive
                        ($resultsNro[$storeId] * 40 / 100),     // result_met
                        100,                                    // objetive_met
                    ];


                    // Obtenemos el valor anterior del puntaje de la categoría
                    $repoGoal = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
                    $goal = $repoGoal->findGoalByGoalTypeAndStore($id_GoalType_Result, $storeId, $month, $year);
                    $resultadoAnterior = 0;
                    if ($goal) {
                        $resultadoAnterior = $goal[0]->getResult();
                    }


                    $sqlDeletes = [];
                    $sql = 'INSERT INTO `goal` (`goaltype_id`, `store_id`, `month`, `year`, `type`, `result`, `objetive`, `result_met`, `objetive_met`, `symbolf`, `symbolb`) VALUES ';

                    $sqls = [];
                    foreach ($goalInserts as $key => $value) {
                        $sqls[] = "({$value[1]}, {$storeId}, '$month', '$year', 'MONTH', '{$value[6]}', '{$value[7]}', '{$value[8]}', '{$value[9]}', '', '')";
                        $sqlDeletes[] = $value[1];
                    }
                    $sql .= implode(',', $sqls);
                    $sql .= ';';


                    $sqlDeletes = "DELETE FROM `goal` WHERE `store_id` = '{$storeId}' AND `goaltype_id` IN (" . implode(',', $sqlDeletes) . ") AND `month` = '{$month}' AND `year` = '{$year}';";



                    // Traemos el Goal Type PUNTAJES OBJETIVO de Mystery
                    $goalTypeTotal_Result = $this->getDoctrine()->getRepository(GoalType::class)->findOneBy([
                        'month' => $goalTypeMonth,
                        'year' => $goalTypeYear,
                        'name' => "TOTAL_STORE_RESULTADO",
                        'category' => "TOTAL_STORE_RESULTADO"
                    ]);

                    $id_GoalType_Total_Result = 0;
                    if ($goalTypeTotal_Result !== null) {
                        $id_GoalType_Total_Result = $goalTypeTotal_Result->getId();
                    }


                    $goalFinal = $repoGoal->findGoalByGoalTypeAndStore($id_GoalType_Total_Result, $storeId, $month, $year);
                    if ($goalFinal) {
                        // Al puntaje final de la estación, le sumamos el nuevo valore, pero le restamos el anterior valor de la última vez ejecutado
                        $sqlUpdate = "UPDATE `goal` SET `result` = (`result` + " . (($resultsNro[$storeId] * 40 / 100) - $resultadoAnterior) . ") WHERE `store_id` = '{$storeId}' AND `month` = '{$month}' AND `year` = '{$year}' AND `goaltype_id` = '{$id_GoalType_Total_Result}';";
                    } else {
                        // Al puntaje final de la estación, tenemos que insertarlo
                        $r = ($resultsNro[$storeId] * 40 / 100);
                        $sqlUpdate = 'INSERT INTO `goal` (`goaltype_id`, `store_id`, `month`, `year`, `type`, `result`, `objetive`, `result_met`, `objetive_met`, `symbolf`, `symbolb`) VALUES ' . "({$id_GoalType_Total_Result}, {$storeId}, '$month', '$year', 'MONTH', '{$r}', '0', '0', '0', '', '')";
                    }



                    $stmt = $conn->prepare($sqlDeletes);
                    $stmt->execute();
					$stmt->closeCursor();

                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
					$stmt->closeCursor();

                    $stmt = $conn->prepare($sqlUpdate);
                    $stmt->execute();
					$stmt->closeCursor();

                    print_r($sqlDeletes);
                    print_r('<br>');
                    print_r($sql);
                    print_r('<br>');
                    print_r($sqlUpdate);
                    print_r('<br>---------------------<br>');
                    // exit;

                    // Se mantiene el registro del ranking, solo porque para obtener todos los objetivos, la estación tiene que estar asociada si o si, a un ranking
                    $rankinSql = "DELETE FROM `ranking` WHERE (`store_id` = {$storeId}) and (month={$month}) and (year = {$year}); INSERT INTO `ranking` (`store_id`, `month`, `year`, `position`, `type`) VALUES ({$storeId}, {$month}, {$year}, '1', 'MONTH'), ({$storeId}, {$month}, {$year}, '1', 'YEAR')";
                    $stmt = $conn->prepare($rankinSql);
                    $stmt->execute();

                    print_r('<br>');
                    print_r($rankinSql);
                    print_r('<br>---------------------<br>');
                }                
            }            
            exit;
        }
        
        $repo = $this->getDoctrine()->getRepository('UserBundle:User');
        $user = $this->getUser();
        $birthDaysSameStore = $repo->findNextBirthDays($user, true, 3);
        $birthDaysOtherStore = $repo->findNextBirthDays($user, false, 6);
        
        $empleador = $user->getEmployee();
        $username = $user->getUsername();
        
        if ($empleador->getSubRol() == null && in_array('ROLE_MANAGER', $user->getRoles())) {
            $response = $this->forward('StoreBundle:EmployeeSubRol:subRol');
            return $response;
        }

        $rolePerfilArray = explode("  -", $empleador->getSubRol()->getName());
        $rolePerfil = $rolePerfilArray[0];

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        switch ($rolePerfil) {
            case 'PERFIL 1':
                if (!in_array('ROLE_HEAD_OFFICE', $user->getRoles())) {
                    $role = 'a:1:{i:0;s:16:"ROLE_HEAD_OFFICE";}';
                    $sqlUpdateRoles = "UPDATE users SET roles='$role' WHERE username='$username';";
                    $stmt = $conn->prepare($sqlUpdateRoles);
                    $stmt->execute();
                    header("Refresh:0");
                }
                break;
            case 'PERFIL 2':
                if (!in_array('ROLE_MANAGER', $user->getRoles())) {
                    $role = 'a:1:{i:0;s:12:"ROLE_MANAGER";}';
                    $sqlUpdateRoles = "UPDATE users SET roles='$role' WHERE username='$username';";
                    $stmt = $conn->prepare($sqlUpdateRoles);
                    $stmt->execute();
                    header("Refresh:0");
                }
                break;
            case 'PERFIL 3':
                if (!in_array('ROLE_EMPLOYEE', $user->getRoles())) {
                    $role = 'a:1:{i:0;s:13:"ROLE_EMPLOYEE";}';
                    $sqlUpdateRoles = "UPDATE users SET roles='$role' WHERE username='$username';";
                    $stmt = $conn->prepare($sqlUpdateRoles);
                    $stmt->execute();
                    header("Refresh:0");
                }
                break;
            case 'PERFIL 4':
                if (!in_array('ROLE_MANAGER_GLOBAL', $user->getRoles())) {
                    $role = 'a:1:{i:0;s:19:"ROLE_MANAGER_GLOBAL";}';
                    $sqlUpdateRoles = "UPDATE users SET roles='$role' WHERE username='$username';";
                    $stmt = $conn->prepare($sqlUpdateRoles);
                    $stmt->execute();
                    header("Refresh:0");
                }
                break;
            case 'PERFIL 5':
                if (!in_array('ROLE_ADMIN', $user->getRoles())) {
                    $role = 'a:1:{i:0;s:10:"ROLE_ADMIN";}';
                    $sqlUpdateRoles = "UPDATE users SET roles='$role' WHERE username='$username';";
                    $stmt = $conn->prepare($sqlUpdateRoles);
                    $stmt->execute();
                    header("Refresh:0");
                }
                break;
            case 'PERFIL 6':
                if (!in_array('ROLE_ADMIN_CONSULTA', $user->getRoles())) {
                    $role = 'a:1:{i:0;s:19:"ROLE_ADMIN_CONSULTA";}';
                    $sqlUpdateRoles = "UPDATE users SET roles='$role' WHERE username='$username';";
                    $stmt = $conn->prepare($sqlUpdateRoles);
                    $stmt->execute();
                    header("Refresh:0");
                }
                break;
            case 'PERFIL 7':
                if (!in_array('ROLE_ADMIN_CAPACITACION', $user->getRoles())) {
                    $role = 'a:1:{i:0;s:23:"ROLE_ADMIN_CAPACITACION";}';
                    $sqlUpdateRoles = "UPDATE users SET roles='$role' WHERE username='$username';";
                    $stmt = $conn->prepare($sqlUpdateRoles);
                    $stmt->execute();
                    header("Refresh:0");
                }
                break;
        }
       
        //TRAE DOS ULTIMAS NOTICIAS DESTACADAS
        $roles = $user->getRoles();
        $last2news  =$this->getDoctrine()->getRepository('AperNewsBundle:News')->findByLast2Highlighted($limit = 3, $roles);
        $slider = $this->getDoctrine()->getRepository('IncentiveBundle:Slider')->findSliderByRoles($roles);

        $homeDestacados = $this->getDoctrine()->getRepository('IncentiveBundle:HomeDestacado')->findHomeByRoles($roles);
        $ruffle = $this->getDoctrine()->getRepository('AperRuffleBundle:Ruffle')->findLastestRuffle(1, $roles);

        $estacion = $empleador->getStore();
        $empleadosRepository = $this->getDoctrine()->getRepository(Employee::class);


        return compact('birthDaysSameStore', 'birthDaysOtherStore', 'slider','last2news', 'ruffle','homeDestacados');
    }

    /**
     * @Route("/birthdays")
     * @Template()
     * @return array
     */
    public function birthdaysAction()
    {
        $repo = $this->getDoctrine()->getRepository('UserBundle:User');
        $birthDaysSameStore = $repo->findNextBirthDays($this->getUser(), true, 2000);
        $birthDaysOtherStore = $repo->findNextBirthDays($this->getUser(), false, 2000);

        return compact('birthDaysSameStore', 'birthDaysOtherStore');
    }

    /**
     * @Route("/term")
     * @Template()
     * @return array
     */
    public function termAction()
    {
        $term = $this->getDoctrine()->getRepository('StoreBundle:Term')->findOneBy(array(),array('id'=>'DESC'),1,0);
        return compact('term');
    }
}
