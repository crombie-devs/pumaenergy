<?php

namespace Aper\StoreBundle\Controller;

use Aper\StoreBundle\Form\ContactType;
use Aper\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Class ContactController
 * @package Aper\StoreBundle\Controller
 * @Route("contact")
 */
class ContactController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     * @Template()
     * @return array
     */
    public function formAction()
    {
        $form = $this->createContactForm();

        return ['form' => $form->createView()];
    }

    /**
     * @return Form
     */
    public function createContactForm()
    {
        $form = $this->createForm(ContactType::class, null, [
            'action' => $this->generateUrl('aper_store_contact_send'),
        ]);

        $form->add('submit', SubmitType::class, array(
            'label' => 'Enviar  »',
            'attr' => array('class' => 'btn btn-primary sm')
        ));

        return $form;
    }

    /**
     * @Route("/")
     * @Method("POST")
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function sendAction(Request $request)
    {
        $form = $this->createContactForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $subject = $form->get('subject')->getData();
            $message = $form->get('message')->getData();
            $file = $form->get('attachment')->getData();

            // $fileName = uniqid(rand(), true).'.'.$file->guessExtension();

            // try {
            //     $file->move($this->getParameter('contact_directory'), $fileName);
            // } catch (FileException $e) {
            //     $this->addFlash('success', 'La imagen NO ha sido enviada');
            // }

            $fileName = '';

            $this->sendContactEmail($this->getUser(), $subject, $message, $fileName);

            $this->addFlash('success', 'El mensaje ha sido enviado');
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @param User $user
     * @param $subject
     * @param $message
     */
    private function sendContactEmail(User $user, $subject, $message, $fileName)
    {
        $from = 'mailing@pumaenergyarg.com.ar';
        $to = ['soporteteam@pumaenergyarg.com.ar'];

        $body = $this->renderView('@Store/Contact/email.html.twig', compact('user', 'subject', 'message', 'fileName'));

        $message = \Swift_Message::newInstance()
            ->setSubject('Contacto desde la plataforma Puma Energy')
            ->setFrom($from)
            ->setTo($to)
            ->setBody($body, 'text/html');

        // Create the Transport
        $transport = \Swift_SmtpTransport::newInstance('smtp.sendgrid.net', 25)
          ->setUsername('apikey')
          ->setPassword('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')
        ;
        // Create the Mailer using your created Transport
        $mailer = \Swift_Mailer::newInstance($transport);

        return $mailer->send($message);
    }
}