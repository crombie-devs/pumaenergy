<?php

namespace Aper\StoreBundle\Repository;

use Aper\UserBundle\Model\Role;
use Aper\StoreBundle\Entity\Store;
use Doctrine\ORM\EntityRepository;

/**
 * Class StoreRepository.
 */
class StoreRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->createQueryBuilder('Store')->orderBy('Store.id', 'ASC');
    }


    public function findAllIsBt() { 
        $qb   = $this->createQueryBuilder('Store');
        $qb->where('Store.bt != :bt')->setParameter('bt', 0);
        $qb->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);
        return $qb->getQuery()->getResult();
    }


    public function findMysteryByBt($bt) { 

		//$bt=268307;
        $ch = curl_init();
        $json='{
            "controller": "Review",
            "action": "selectrecord",
            "where": {
                "BranchLink": "' . $bt . '"
            }
        }';
        $json=mcrypt_encrypt(MCRYPT_RIJNDAEL_256,'420d497b5d8ab922f6c46573629d74ba',$json,MCRYPT_MODE_ECB);
        $request='app_id=16&enc_request='.urlencode(base64_encode($json));
        $options=array(CURLOPT_SSL_VERIFYPEER=>FALSE,
            CURLOPT_SSL_VERIFYHOST=>2,
            CURLOPT_FOLLOWLOCATION=>1,
            CURLOPT_RETURNTRANSFER=>1,
            CURLOPT_TIMEOUT=>20,
            CURLOPT_POST=>1,
            CURLOPT_URL=>"https://www.webethere.net/api_index.php",
            CURLOPT_POSTFIELDS=>$request
        );
        curl_setopt_array($ch, $options);
		//var_dump($options); exit;
        $response = curl_exec($ch);
        curl_close($ch);

		//var_dump($response);exit;
        return json_decode($response, true);
    }


    public function findAllOrderedByNombre() { 
        $qb   = $this->createQueryBuilder('Store');
        $qb->where('Store.code != :code')->setParameter('code', 0);
        $qb->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);
        $qb->orderBy('Store.location', 'ASC');

        return $qb->getQuery()->getResult();
    }


    /**
     * @param $params
     *
     * @return \Aper\StoreBundle\Entity\Store
     */
    public function findForUniqueValidation(array $params)
    {
        return $this->findBy($params);
    }

    /**
     * @param string $code
     * @return Store
     */
    public function findOneByCode($code)
    {
        $code = str_pad($code,4,"0", STR_PAD_LEFT);
        $qb   = $this->createQueryBuilder('Store');
        $qb
            ->where('Store.code = :code')->setParameter('code', $code)
            ->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);

        return $qb->getQuery()->getOneOrNullResult();
    }


    /**
     * @param string $code
     * @return Store
     */
    public function findOneById($id)
    {
        $qb   = $this->createQueryBuilder('Store');
        $qb->where('Store.id = :id')->setParameter('id', $id)
            ->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);

        return $qb->getQuery()->getOneOrNullResult();
    }


    public function findPerformance($month, $year, $limit = 3, $responsable = null, $zona = null, $storeId = null, $storesIds = null)
    {
        $query = $this->createQueryBuilder('Store');
        $query->innerJoin('Store.rankings', 'Ranking')

            ->where('Ranking.year = :year')->setParameter('year', $year)
            // ->andWhere('Store.code != :code')->setParameter('code', 0)
            ->andWhere('Ranking.month = :month')->setParameter('month', $month)
            ->andWhere('Ranking.type = :type')->setParameter('type', 'MONTH')
            ->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);
        if ($responsable) {
            $query->andWhere('Store.responsableComercial = :responsable')->setParameter('responsable', $responsable); 
        }
        if ($zona) {
            $query->andWhere('Store.zona = :zona')->setParameter('zona', $zona);
        }
        if ($storeId) {
            $query->andWhere('Store.id = :id')->setParameter('id', $storeId);
        }
        if ($storesIds) {
            $query->andWhere('Store.code IN (:code)')->setParameter('code', $storesIds);
        }
        $query->setMaxResults($limit)->orderBy('Ranking.position', 'ASC');
        return $query->getQuery()->getResult();
    }


    public function findStoresByRepresentante($responsable = null, $zona = null, $storeId = null, $storesIds = null)
    {
        $query = $this->createQueryBuilder('Store');
        $query->where('Store.enabled = :enabled')->setParameter('enabled', 1);
        // $query->andWhere('Store.code != :code')->setParameter('code', 0);
        
        if ($responsable) {
            $query->andWhere('Store.responsableComercial = :responsable')->setParameter('responsable', $responsable);
        }
        if ($zona) {
            $query->andWhere('Store.zona = :zona')->setParameter('zona', $zona);
        }
        if ($storeId) {
            $query->andWhere('Store.id = :id')->setParameter('id', $storeId);
        }
        if ($storesIds) {
            $query->andWhere('Store.code IN (:code)')->setParameter('code', $storesIds);
        }
        $query->orderBy('Store.location', 'ASC');

        return $query->getQuery()->getResult();
    }


    public function findStoresByCodes($month, $year, $storesCodes = null)
    {


        $query = $this->createQueryBuilder('Store');
        $query->innerJoin('Store.rankings', 'Ranking');

        $query->where('Ranking.year = :year')->setParameter('year', $year);
            // ->andWhere('Store.code != :code')->setParameter('code', 0)

        if ($month == null) {
            $type = 'YEAR';
        } else {
            $query->andWhere('Ranking.month = :month')->setParameter('month', $month);
            $type = 'MONTH';
        }
        $query->andWhere('Ranking.type = :type')->setParameter('type', $type);
        $query->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);

        if ($storesCodes) {
            $query->andWhere('Store.code IN (:code)')->setParameter('code', $storesCodes);
        }
        $query->orderBy('Ranking.position', 'ASC');
        return $query->getQuery()->getResult();
    }



    /**
     * @param integer $month
     * @param integer $year
     * @param integer $limit
     * @return Store[]
     */
    public function findRankingByMonth($month, $year, $limit = 3, $zona = null, $responsable = null)
    {
		
		//var_dump($zona_nuevo);exit;
		
		
		
		
        $query = $this->createQueryBuilder('Store');
        $query->innerJoin('Store.rankings', 'Ranking')
            ->where('Ranking.year = :year')->setParameter('year', $year)
            ->andWhere('Ranking.month = :month')->setParameter('month', $month)
            ->andWhere('Ranking.type = :type')->setParameter('type', 'MONTH')
            ->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);
        if ($zona) {
            $query->andWhere('Store.zona = :zona')->setParameter('zona', $zona);
        }
		if ($responsable) {
			$query_zona = $this->createQueryBuilder('Store');
			$query_zona->select('Store.zona');
			$query_zona->where('Store.responsableComercial = :responsable')->setParameter('responsable', $responsable); 
			$query_zona->setMaxResults(1);
			$zona_nuevo = $query_zona->getQuery()->getResult();
			//var_dump($zona_nuevo);exit;
			$zona_nuevo = $zona_nuevo[0]["zona"];
			if ($zona_nuevo){		
            $query->andWhere('Store.zona = :zona_nuevo')->setParameter('zona_nuevo', $zona_nuevo); }
        }
        $query->setMaxResults($limit)->orderBy('Ranking.position', 'ASC');

        return $query->getQuery()->getResult();
    }

    /**
     * @param integer $year
     * @return Store[]
     */
    public function findRankingByYear($year, $zona = null)
    {
        $query = $this->createQueryBuilder('Store');
        $query->innerJoin('Store.rankings', 'Ranking')
            ->where('Ranking.year = :year')->setParameter('year', $year)
            ->andWhere('Ranking.type = :type')->setParameter('type', 'YEAR')
            ->andWhere('Store.enabled = :enabled')->setParameter('enabled', 1);
        if ($zona) {
            $query->andWhere('Store.zona = :zona')->setParameter('zona', $zona);
        }
        $query->orderBy('Ranking.position', 'ASC');
        return $query->getQuery()->getResult();
    }
}
