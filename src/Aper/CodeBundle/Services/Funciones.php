<?php
 
namespace Aper\CodeBundle\Services;
 
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Aper\CodeBundle\Entity\Code;
use Aper\UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Response; //bcomas 
use Symfony\Component\HttpFoundation\Request;

 
class Funciones {
 
    protected $em;
    protected $router;
    protected $paginator;
    protected $request;
 
    public function __construct(EntityManager $entityManager, Router $router, $paginator) {
        $this->em = $entityManager;
        $this->router = $router;
        $this->paginator = $paginator;
    }
 
    public function getAllCodes($catQueryId, $sinceQuery, $untilQuery,$usuarioActual,$rol) {
        //$func= new Funciones();
        $em = $this->getEm();
 
        //$codigo= $em->getRepository('CodeBundle:Code')->findAll();
        /*if($codigo){

        echo "viene codigo";

        }else{

            echo "no viene";
        }*/

        //$user = $em->get('security.token_storage')->getToken()->getUser();
        //$id= $user->getId();

        //$usuarioActual= $this->getRepository('UserBundle:Employee')->find($id);
        //$repository = $this->getDoctrine()->getRepository('UserBundle:Employee');
        //$usuarioActual = $repository->find($id);  //id =4 jefe

        //si es empleado ve solo lo suyo
        //si es rol_1 o rol_2 ve todos de su estacion

        if ($rol == "BOSS" ) {
            $empleados = $usuarioActual->getEmployees();
            foreach ($empleados as $empleado) {
                $categories[] = array(
                    'id' => $empleado->getId(),
                    'nombre' => $empleado->getProfileName(),
                );
            }
        } else {
            $categories[] = array(
                'id' => $usuarioActual->getId(),
                'nombre' => $usuarioActual->getProfileName(),
            ); 
        }

        if($catQueryId != "-1") {
        //$catQuery = $this->getDoctrine()->getRepository(Employee::class)->findOneBy(array('id' => $catQueryId));
          $catQuery = $em->getRepository('UserBundle:Employee')->findOneBy(array('id' => $catQueryId));

        } else {
            //EstacionId
            //$user = $this->get('security.token_storage')->getToken()->getUser();
            //catQuery es un array
            /*$catQuery = $this->getDoctrine()->getRepository(Employee::class)->findBy(array('boss' => $usuarioActual->getEmployee()->getId()));*/
            $user = $usuarioActual->getUser();
            $catQuery = $em->getRepository('UserBundle:Employee')->findBy(array('boss' => $user->getEmployee()->getId()));
        }

        //$rol = $this->get('security.token_storage')->getToken()->getUser()->getRoles();
        $tag = null;
        $since = null;
        $until = null;
        $categ = null;

        if ($sinceQuery) {
            $array = explode('/', $sinceQuery);
            $since = New \DateTime($array[2] . '-' . str_pad($array[1], 2, '0') . '-' .
                str_pad($array[0], 2, '0') . " 00:00:00");
        }

        if ($untilQuery) {
            $array = explode('/', $untilQuery);
            $until = New \DateTime($array[2] . '-' . str_pad($array[1], 2, '0') . '-' .
                str_pad($array[0], 2, '0') . " 23:59:59");
        }

        if($catQueryId == "-1"){
         //$this->getDoctrine()->getRepository(Code::class)
        $news = $em->getRepository('CodeBundle:Code')
                ->createQueryBuilderByBoss($catQuery, $since, $until);
        }else{
        /*$news = $this->getDoctrine()->getRepository(Code::class)
                ->createQueryBuilderByUser($catQuery, $since, $until);*/
        $news = $em->getRepository('CodeBundle:Code')
                ->createQueryBuilderByUser($catQuery, $since, $until);
        }

        //$paginator = $this->get('knp_paginator');
        /*$paginator = $this->getPaginator('knp_paginator');
        $pagination = $paginator->paginate(
            $news, $this->request->query->getInt('page', 1),
            9
        );*/

        $response = ['news' => $news , 'categories' => $categories];
        return $response;
    }

    public function test($var) {
        $response = ["recibido" => $var];
        return $response;
    }

    public function getEm()
    {
        return $this->em;
    }

    public function getPaginator()
    {
        return $this->paginator;
    }

    public function getRequest()
    {
        return $this->request;
    }
}
