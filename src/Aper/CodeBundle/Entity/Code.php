<?php

namespace Aper\CodeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Aper\UserBundle\Entity\User;

/**
 * Code
 *
 * @ORM\Table(name="code")
 * @ORM\Entity(repositoryClass="Aper\CodeBundle\Repository\CodeRepository")
 */
class Code
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var employee
     *
     *
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\Employee", inversedBy="codesQr")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     * })
     */
    private $employee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;

    /**
     * @var string
     *
     * @ORM\Column(name="data_qr", type="string", length=255, nullable=true)
     */
    private $dataQr;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_valid", type="boolean")
     */
    private $isValid;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set employee
     *
     * @param Employee $employee
     *
     * @return Employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    
        return $this;
    }

    /**
     * Get employee
     *
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Code
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    
        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set dataQr
     *
     * @param string $dataQr
     *
     * @return Code
     */
    public function setDataQr($dataQr)
    {
        $this->dataQr = $dataQr;
    
        return $this;
    }

    /**
     * Get dataQr
     *
     * @return string
     */
    public function getDataQr()
    {
        return $this->dataQr;
    }

    /**
     * Set isValid
     *
     * @param boolean $isValid
     *
     * @return Code
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;
    
        return $this;
    }

    /**
     * Get isValid
     *
     * @return boolean
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    public function findAll()
    {
        return $this->findBy(array(), array('datetime' => 'DESC'));
    }
}

