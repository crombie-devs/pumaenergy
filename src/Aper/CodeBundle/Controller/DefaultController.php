<?php

namespace Aper\CodeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
/**
 * Code controller.
 *
 * @Route("/defaultcode")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('CodeBundle:Default:index.html.twig');
    }


    
}
