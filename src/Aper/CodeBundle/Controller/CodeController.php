<?php

namespace Aper\CodeBundle\Controller;

use Aper\CodeBundle\Entity\Code;
use Aper\UserBundle\Entity\User;
use Aper\UserBundle\Entity\Employee;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Response; //bcomas 
use Aper\CinemaBundle\Form\CodeFilterType;
use WebFactory\Bundle\GridBundle\Event\ManipulateQueryEvent;
use WebFactory\Bundle\GridBundle\Grid\Configuration;

/**
 * Code controller.
 *
 * @Route("/code")
 */
class CodeController extends Controller
{
    /**
     * @Route("/", name="aper_code_index")
     */
    public function indexAction() {
        return $this->render('CodeBundle:Code:index.html.twig');
    }

     /**
     * Creates a new benefit entity.
     *
     * @Route("/new", name="aper_code_new")
     * @Method({"POST"})
     */
    public function newAction(Request $request) {
        try {
            $msj ="";
            $id = $this->getUser()->getId(); //Usuario Logueado
            if($id) {
                if ($this->isGranted('ROLE_EMPLOYEE')) {
                    $codigoqr = $request->request->get("inputcode");

                    if(!$codigoqr)
                        return;

                    $code = new Code();
                    $repository = $this->getDoctrine()->getRepository('UserBundle:Employee');

                    $empleado = $repository->find($id);

                    $em = $this->getDoctrine()->getManager();
                    $empleado->addDataQr($code);
                    $code->setEmployee($empleado);
                    $code->setDatetime(new \Datetime());
                    $code->setDataQr($codigoqr);
                    $code->setIsValid(false);
                    $em->persist($code);
                    $em->flush();
                    $this->addFlash('success', 'Lectura exitosa ');
                    return $this->redirectToRoute('aper_code_index');
                } else {
                    $msj ="Solo los empleados pueden realizar la lectura de datos QR";
                }
            } else {
                $msj = "No se pudo identificar el usuario logueado.";
            }
        } catch (Exception $e) {
            $this->addFlash('error', 'Se ha producido un error al intentar grabar el QR leído.');
        }

        $this->addFlash('error',$msj);
        return $this->redirectToRoute('aper_code_index');
    }

    /**
     * Creates a new benefit entity.
     *
     * @Route("/list", name="aper_code_list")
     * @Method({"GET"})
     */
    public function listAction(Request $request) {
        $id = $this->getUser()->getId();
        $repository = $this->getDoctrine()->getRepository('UserBundle:Employee');
        $usuarioActual = $repository->find($id);
        $categories = "";
        $catQueryId = $request->get('category');
        $sinceQuery = $request->get('since');
        $untilQuery = $request->get('until');
        $UserRol = "";
        
        if ($this->isGranted('ROLE_HEAD_OFFICE')||($this->isGranted('ROLE_MANAGER'))) {

            $estacion = $usuarioActual->getStore();
            $empleadosRepository = $this->getDoctrine()->getRepository(Employee::class);
            $empleados = $empleadosRepository->findAllEmployeesByStationActiveOrNotAndNotDeleted($estacion,true);

            foreach ($empleados as $empleado) {
                $categories[] = array(
                    'id' => $empleado->getId(),
                    'nombre' => $empleado->getProfileName(),
                );
                $UserRol = "BOSS";
            }
        } else {
            $categories[] = array(
                'id' => $usuarioActual->getId(),
                'nombre' => $usuarioActual->getProfileName(),
            ); 
            $UserRol = "EMPLOYEE";
        }

        if($UserRol == "BOSS") {
            if(($catQueryId == "-1" )||($catQueryId == null)) {
                $user = $this->get('security.token_storage')->getToken()->getUser();
                $catQuery = $this->getDoctrine()->getRepository(Employee::class)->findBy(array('store' => $user->getEmployee()->getStore()));
            } else {
                $catQuery = $this->getDoctrine()->getRepository(Employee::class)->findOneBy(array('id' => $catQueryId));  
            }
        }else {
            $catQuery = $this->getDoctrine()->getRepository(Employee::class)->findOneBy(array('id' => $usuarioActual->getId()));  
        }

        $since = null;
        $until = null;
        $categ = null;

        if ($sinceQuery) {
            $array = explode('/', $sinceQuery);
            $since = New \DateTime($array[2] . '-' . str_pad($array[1], 2, '0') . '-' .
                str_pad($array[0], 2, '0') . " 00:00:00");
        }

        if ($untilQuery) {
            $array = explode('/', $untilQuery);
            $until = New \DateTime($array[2] . '-' . str_pad($array[1], 2, '0') . '-' .
                str_pad($array[0], 2, '0') . " 23:59:59");
        }

        if($UserRol == "BOSS") {
            if(($catQueryId == "-1") || ($catQueryId == null)) {
                $new = $this->getDoctrine()->getRepository(Code::class);
                $news = $new->createQueryBuilderByBoss($catQuery, $since, $until);
            } else {
                $new = $this->getDoctrine()->getRepository(Code::class);
                $news = $new->createQueryBuilderByUser($catQuery, $since, $until);
            }
        } else {
            $new = $this->getDoctrine()->getRepository(Code::class);
            $news = $new->createQueryBuilderByUser($catQuery, $since, $until);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $news, $request->query->getInt('page', 1),
            9
        );

        if ($request->isXmlHttpRequest()) {
            $ajaxNews = [];
            foreach ($pagination as $new) {

                $ajaxNews[] = array(
                    'user' => $new->getEmployee(), 
                    'data' => $new->getDataQr(),
                    'date' => $new->getDatetime(),
                    'status' => $new->getIsValid(), 
                );
            }
            $response = new  \Symfony\Component\HttpFoundation\Response(json_encode($ajaxNews));

            return $response;
        }

        return $this->render('CodeBundle:Code:list.html.twig', array('pagination' => $pagination, 'categories' => $categories));
    }

     /**
     * Edit a code status entity.
     *
     * @Route("/edit/{id}", name="aper_code_edit")
     * @Method({"GET"})
     */
    public function editAction(Request $request, $id) 
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $codigo = $em->getRepository('CodeBundle:Code')->find($id);
            if(!$codigo) {
                throw $this->createNotFoundException('Código no encontrado');
            }
            if($codigo->getIsValid() == true) {
                $codigo->setIsValid(false);
            }else{
                $codigo->setIsValid(true);
            }
            $em->persist($codigo);
            $em->flush();
            $this->addFlash('success','Código validado con éxito');
            return $this->redirect($this->generateUrl('aper_code_list'));

        } catch (Exception $e) {
            $this->addFlash('error','Se produjo un error al intentar validar el código');
            return $this->redirect($this->generateUrl('aper_code_list'));
        }
    }
}
