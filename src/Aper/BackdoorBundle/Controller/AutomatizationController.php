<?php

namespace Aper\BackdoorBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Aper\UserBundle\Entity\Employee;
use Aper\IncentiveBundle\Entity\GoalType;
use Aper\IncentiveBundle\Entity\Goal;

/**
 * Class AutomatizationController
 *
 * @Route("/automatization")
 */
class AutomatizationController extends Controller{

    /**
     * Lists all benefit entities.
     *
     * @Route("/", name="aper_backdoor_automatization")
     * @Method({"GET","POST"})
     */
    public function automatization()
    {
        ini_set('max_execution_time', 9000);
        ini_set("memory_limit",-1);
        
        $stores = $this->getDoctrine()->getRepository('StoreBundle:Store')->findAll();

        $goal = $this->getDoctrine()->getRepository(Goal::class)->findLastUploadByMonth();


        // Buscamos el período actual
        $bimestreMonth = $goal[0][0]->getMonth();
		//$bimestreMonth = 1;
        $bimestreYear = $goal[0][0]->getYear();
		//$bimestreYear = 2022;

        // Buscamos los últimos objetivos subidos
        $goalType = $this->getDoctrine()->getRepository(GoalType::class)->findByMonthAndYear($bimestreYear, $bimestreMonth);
        $goalTypeMonth = $goalType[0]->getMonth();
        $goalTypeYear = $goalType[0]->getYear();
        
     //   var_dump('CANTIDAD TOTAL: ' . count($stores));

        $contador = 1;
        foreach ($stores as $store) { //recorrer de a uno para probar
        //var_dump('ESTACION : ' . $store->getId() . ' - NRO: ' . $contador);
        $contador++;

            $objectiveStore = [];
            $fulfillment = [];

			//DEBUG
			//if ($store->getId() != 148) {
			// 
            //  continue;
			//	}
			var_dump('ESTACION : ' . $store->getId() . ' | NOMBRE: ' . $store->getLocation());
			
			
            //if ($store->getCode() == 322196001) {
                $employees = $this->getDoctrine()->getRepository(Employee::class)->findAllEmployeesTrainedByStore($store);
                
                $computa = false;
				foreach ($employees as $employee) {
					
					//var_dump($employee->getProfile()->getName());
					
					//echo("COMIENZA USUARIO");
					//var_dump($employee->getUser()->getMaxRole());

					
					
					
                    if(!($employee->getUser()->isEnabled())){
                        continue;
                    }
					var_dump($employee->getProfile()->getName());

                    $subRolEmployee = $employee->getSubRol();
					//var_dump("TRAE LOS SUBROLES");
					//var_dump($employee->getId() . " -- " . $employee->getSubRol() . "\n");
					
					
    
                    if ($employee->getCareerPlans()) {                
                        foreach ($employee->getCareerPlans() as $careerPlan) {
							
							
							
                            foreach ($careerPlan->getModules() as $moduleEmp){
                          
								
								$module = $moduleEmp->getModule();
								
								//var_dump("jumpo" . $module->getId() . " -- " . $module->getTitle());
                                foreach ($module->getSubRol() as $subRolModule) {
                                    if ($subRolEmployee == $subRolModule->getName()) {
										//var_dump("Entró");
                                        if ($module->getMonth() == $bimestreMonth && $module->getYear() == $bimestreYear) {
									
                                            if ($moduleEmp->isEnabled()) {
                                                    
                                                    //if ($module->getId() == 9){
                                                    //echo("COMPUTA 9");
                                                    //var_dump($module->getId());
                                                    
                                                    //}
                                                    //if ($module->getId() == 10){
                                                    //echo("COMPUTA 10");
                                                    //var_dump($module->getId());
                                                    
                                                    //}
                
                                                if (!isset($objectiveStore[$module->getId()])){
                                                    $objectiveStore[$module->getId()] = 0;
                                                }
                
                                                $objectiveStore[$module->getId()]++;
												
                                                if($module->getId()==11){
                                                    var_dump($employee->getId());
                                                }
            
                                                $progress = $this->getModuleProgress($moduleEmp);
                                                if ($progress == 100) {
                                                    if (!isset($fulfillment[$module->getId()])){
                                                        $fulfillment[$module->getId()] = 0;
                                                    }
                
                                                    $fulfillment[$module->getId()]++;
                                                }
                                            }
                                        }
                                    }
                                
								
								}
								
								/* if ($employee->getId() == 5024 && $module->getId()==10) {
								echo("MODULE EMP");
								var_dump($moduleEmp);
								echo("ID_EMPLEADO");
								var_dump($employee->getId());
								echo("Modulo");
								var_dump($module->getId());
								echo("Enabled?");
								var_dump($moduleEmp->isEnabled());
								echo("Full data module");
								var_dump($module);
								} */
                           // }
                        }
                    }
                }

                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();
        
                $sumObjetive = 0;
                $sumFulfillment = 0;
                

               

                foreach ($objectiveStore as $idModule => $os) {
        
                    $sumObjetive += $os;
                    if (isset($fulfillment[$idModule])) {
                        $sumFulfillment += $fulfillment[$idModule];
                    } else {
                        $fulfillment[$idModule] = 0;
                    }
        
                    $goalType = $this->getDoctrine()->getRepository(GoalType::class)->findOneBy([
                        'bt' => $idModule, 
                        'month' => $goalTypeMonth,
                        'year' => $goalTypeYear
                    ]);
            
            
                    if ($goalType === null) {
                        $msg = "No se encontro GoalType";
                    } else {
                        $goal = $this->getDoctrine()->getRepository(Goal::class)->findOneBy([
                            'goalType' => $goalType, 
                            'month' => $bimestreMonth,
                            'year' => $bimestreYear,
                            'store' => $store
                        ]);
            
                        if (isset($fulfillment[$idModule])) {
                            $porcentaje = $fulfillment[$idModule] * 100 / $os;

                            if ($goal === null) {
                                $sql = "INSERT INTO `goal` (`id`, `goaltype_id`, `store_id`, `month`, `year`, `type`, 
                                `result`, `objetive`, `result_met`, `objetive_met`, `symbolf`, `symbolb`) VALUES 
                                (null, ". $goalType->getId() .", ". $store->getId() .", ". $bimestreMonth .", ". $bimestreYear .",  
                                'MONTH',  ". $porcentaje .", '100',  ". $fulfillment[$idModule] .",  ". $os .", '', '');";
                            } else {
                                $sql = "UPDATE `goal` SET 
                                `goaltype_id` = ". $goalType->getId() .", 
                                `store_id` = ". $store->getId() .", 
                                `month` = ". $bimestreMonth .",
                                `year` = ". $bimestreYear .",
                                `type` = 'MONTH',
                                `result` = ". $porcentaje .",
                                `objetive` = '100',
                                `result_met` = ". $fulfillment[$idModule] .",
                                `objetive_met` = ". $os .",
                                `symbolf` = '',
                                `symbolb` = ''
                                WHERE `id` = ". $goal->getId() .";";
                            }
    
                           // echo $sql;
                
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();
                        }
                    }
                }
    
                $this->updateTotalizer($store, $sumFulfillment, $sumObjetive, $bimestreMonth, $bimestreYear, $goalTypeMonth, $goalTypeYear);
                //echo "Cumplimiento: ". $sumFulfillment .", Objetivo: ". $sumObjetive .", Estacion ID: ". $store->getId() . ' -- Putos Finales: ' . ($sumObjetive ? ($sumFulfillment * 10 / $sumObjetive) : 0);
            //    break;
            }
        }

       die(var_dump("se termino"));
    }

    public function updateTotalizer($store, $sumFulfillment, $sumObjetive, $month, $year, $goalTypeMonth, $goalTypeYear)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $goalTypeObjetive = $this->getDoctrine()->getRepository(GoalType::class)->findOneBy([
            'month' => $goalTypeMonth,
            'year' => $goalTypeYear,
            'name' => "PUNTAJES OBJETIVO",
            'category' => "CAPACITACIÓN"
        ]);

        if ($goalTypeObjetive === null) {
            $msg = "No se encontro goalTypeObjetive";
        } else {
            $goal = $this->getDoctrine()->getRepository(Goal::class)->findOneBy([
                'goalType' => $goalTypeObjetive, 
                'month' => $month,
                'year' => $year,
                'store' => $store
            ]);

            if ($goal === null) {
                $sql = "INSERT INTO `goal` (`id`, `goaltype_id`, `store_id`, `month`, `year`, `type`, 
                        `result`, `objetive`, `result_met`, `objetive_met`, `symbolf`, `symbolb`) VALUES 
                        (null, ". $goalTypeObjetive->getId() .", ". $store->getId() .", ". $month .", 
                        ". $year .", 'MONTH',  10,  10, 
                        '0', '0',  '', '');";
            } else {
                $sql = "UPDATE `goal` SET 
                `month` = ". $month .",
                `year` = ". $year .",
                `type` = 'MONTH',
                `result` = 10,
                `objetive` = 10,
                `result_met` = '0',
                `objetive_met` = '0',
                `symbolf` = '',
                `symbolb` = ''
                WHERE `id` = ". $goal->getId() .";";
            }
            //var_dump($sql);

            $stmt = $conn->prepare($sql);
            $stmt->execute();
        }

        $goalTypeResult = $this->getDoctrine()->getRepository(GoalType::class)->findOneBy([
            'month' => $goalTypeMonth,
            'year' => $goalTypeYear,
            'name' => "PUNTAJES RESULTADO",
            'category' => "CAPACITACIÓN"
        ]);


        // Obtenemos el valor anterior del puntaje de la categoría
        $repoGoal = $this->getDoctrine()->getRepository('IncentiveBundle:Goal');
        $goal = $repoGoal->findGoalByGoalTypeAndStore($goalTypeResult->getId(), $store->getId(), $month, $year);
        $resultadoAnterior = 0;
        if ($goal) {
            $resultadoAnterior = $goal[0]->getResult();
        }


        if ($goalTypeResult === null) {
            $msg = "No se encontro goalTypeResult";
        } else {
            $goal = $this->getDoctrine()->getRepository(Goal::class)->findOneBy([
                'goalType' => $goalTypeResult, 
                'month' => $month,
                'year' => $year,
                'store' => $store
            ]);


            // Lucas 15/10
           // var_dump($sumObjetive);
           // var_dump($sumFulfillment);
            $sumFulfillment = ($sumObjetive ? ($sumFulfillment * 10 / $sumObjetive) : 0);

            if ($goal === null) {
                $sql = "INSERT INTO `goal` (`id`, `goaltype_id`, `store_id`, `month`, `year`, `type`, 
                        `result`, `objetive`, `result_met`, `objetive_met`, `symbolf`, `symbolb`) VALUES 
                        (null, ". $goalTypeResult->getId() .", ". $store->getId() .", ". $month .", 
                        ". $year .", 'MONTH', ". $sumFulfillment .",  ". $sumFulfillment .", 
                        '0', '0',  '', '');";
            } else {
                $sql = "UPDATE `goal` SET 
                `month` = ". $month .",
                `year` = ". $year .",
                `type` = 'MONTH',
                `result` = ". $sumFulfillment .",
                `objetive` = ". $sumFulfillment .",
                `result_met` = '0',
                `objetive_met` = '0',
                `symbolf` = '',
                `symbolb` = ''
                WHERE `id` = ". $goal->getId() .";";
            }


         //   var_dump($sql);

            $stmt = $conn->prepare($sql);
            $stmt->execute();
        }



        // Puntaje final
        $goalTypeResultFinal = $this->getDoctrine()->getRepository(GoalType::class)->findOneBy([
            'month' => $goalTypeMonth,
            'year' => $goalTypeYear,
            'name' => "TOTAL_STORE_RESULTADO",
            'category' => "TOTAL_STORE_RESULTADO"
        ]);

        if ($goalTypeResultFinal === null) {
            $msg = "No se encontro goalTypeResultFinal";
        } else {
            $goal = $this->getDoctrine()->getRepository(Goal::class)->findOneBy([
                'goalType' => $goalTypeResultFinal, 
                'month' => $month,
                'year' => $year,
                'store' => $store
            ]);

            $id_GoalType_Total_Result = $goalTypeResultFinal->getId();
            $storeId = $store->getId();


            if ($goal === null) {

                $sql = 'INSERT INTO `goal` (`goaltype_id`, `store_id`, `month`, `year`, `type`, `result`, `objetive`, `result_met`, `objetive_met`, `symbolf`, `symbolb`) VALUES ' . "({$id_GoalType_Total_Result}, {$storeId}, '{$month}', '{$year}', 'MONTH', '{$sumFulfillment}', '0', '0', '0', '', '')";
            } else {
                $sql = "UPDATE `goal` SET `result` = (`result` + " . ($sumFulfillment - $resultadoAnterior) . ") WHERE `store_id` = '{$storeId}' AND `month` = '{$month}' AND `year` = '{$year}' AND `goaltype_id` = '{$id_GoalType_Total_Result}';";
            }


          //  var_dump($sql);

            $stmt = $conn->prepare($sql);
            $stmt->execute();
        }


    }

    private function getModuleProgress($module) {

        // var_dump('-----------------------');
        // var_dump($module->getExamPermission()->getNextStep());
        // var_dump($module->getExamPermission()->isEnabled());
    
        // var_dump($module->getExamPermission()->getEmployeeExams()->getFinishedAt());
        // var_dump($module->getExamPermission()->getEmployeeExams()->getStartsAt());
        // exit;
    
        $progress = 0;
        if ($module->getExamPermission() == false && $module->getExamPermissionsPost() == false && $module->getPollPermission() == false) {
            $progress = 0;
        } else if ($module->getExamPermission() !== false && $module->getExamPermission()->isEnabled() !== false) {
    
            // if prev exam is completed = 25
            if ($module->getExamPermission()->getNextStep() === 'completed') {
                $progress = 25;
            }
    
            if ($module->getExamPermissionsPost() && $module->getExamPermissionsPost()->isEnabled() == true && $module->getExamPermissionsPost()->getNextStep() != 'none' ) {
                $progress = 75;
            }
    
            // check post exam
            if ($module->getExamPermissionsPost() !== false && $module->getExamPermissionsPost()->getNextStep() === 'completed' && $module->getExamPermissionsPost()->isEnabled() !== false) {    
                $progress = 100;
            } else {
                if ($module->getPollPermission() && $module->getPollPermission()->isEnabled() == true && $module->getPollPermission()->getNextStep() != 'none') {
                    if ($module->getModule()->getId() >= 9) {
                        if (
                            $module->getExamPermissionsPost() && $module->getExamPermissionsPost()->isEnabled() == true && 
                            ($module->getExamPermissionsPost()->getNextStep() === 'completed' || $module->getExamPermissionsPost()->getNextStep() === 'finish') 
                        ) {
                            $progress = 100;
                        }
                    } else {
                        if ($module->isApproved()) {
                            $progress = 100;
                        }
                    }
                }
            }
    
        } else if ($module->getExamPermission() !== false && $module->getExamPermission()->getNextStep() !== 'completed') {
            $progress = 0;
        }
    
        if ($module->isEnabled() && ($module->isApproved() || $progress == 100)) {
            $progress = 100;
        }
    
        return $progress;
    }
}