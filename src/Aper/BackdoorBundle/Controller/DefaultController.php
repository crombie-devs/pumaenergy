<?php

namespace Aper\BackdoorBundle\Controller;

// use Aper\CinemaBundle\Entity\Cinema;
use Aper\IncentiveBundle\Entity\GoalType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Aper\PlanCarreraBundle\Entity\ExamPermission;



use Aper\PlanCarreraBundle\CQRS\Command\ActivateModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ActivateModulePollCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ActivatePlanRequirementCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ApproveModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ApprovePlanCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ApprovePlanRequirementCommand;
use Aper\PlanCarreraBundle\CQRS\Command\AssignTrainer;
use Aper\PlanCarreraBundle\CQRS\Command\ApproveModuleCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DeactivateModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DeactivateModulePollCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DeactivatePlanRequirementCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapproveModuleCommand;
use Aper\PlanCarreraBundle\CQRS\Command\ActivateModuleCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DeactivateModuleCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapproveModuleExamCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapprovePlanCommand;
use Aper\PlanCarreraBundle\CQRS\Command\DisapprovePlanRequirementCommand;
use Aper\PlanCarreraBundle\CQRS\Command\RegisterToProgramCommand;
use Aper\PlanCarreraBundle\CQRS\CommandHandler\RegisterToProgram;
use Aper\PlanCarreraBundle\Entity\EmployeeModuleExam;

/**
 * Class DefaultController
 * @package Aper\BackdoorBundle\Controller
 */
class DefaultController extends FOSRestController
{
    /**
     * @Rest\Get("/course_approved", name="backdoor_course_approved")
     * @Rest\View(serializerGroups={"course_approved"}, serializerEnableMaxDepthChecks=true)
     *
     * @Rest\QueryParam(name="course_id", default=null, strict=true, map=true, nullable=true)
     * @Rest\QueryParam(name="user_id", default=null, strict=true, map=true, nullable=true)
     * @Rest\QueryParam(name="approved", default=true, strict=true, map=true, nullable=true)
     *
     * @param null $course_id
     * @param null $user_id
     * @param null $approved
     *
     * @return array
     */
    public function courseApprovedAction($course_id = null, $user_id = null, $approved = true)
    {

        /*
        Prevención COVID-19
        - ID Scorm: 459
        - ID Curso: 6

        Atención al Cliente N-1
        - ID Scorm: 23
        - ID Curso: 3

        Lubricantes Puma Energy N-1
        - ID Scorm: 6
        - ID Curso: 2

        Combustibles Puma Energy N-1
        - ID Scorm: 41
        - ID Curso: 5

        Lubricantes Puma Energy N-2
        - ID Scorm: 137
        - ID Curso: 26

        Seguridad en Estación de Servicio N-1
        - ID Scorm: 109
        - ID Curso: 19

        Transporte y Descarga de Combustibles N-1
        - ID Scorm: 36
        - ID Curso: 4

        Atención al Cliente N-2
        - ID Scorm: 492
        - ID Curso: 111

        Lubricantes Puma Energy N-3
        - ID Scorm: 494
        - ID Curso: 112
		
		Puma APP
		- ID Scorm: 614
		- ID Curso: 139
		
		Combustibles Puma Energy N-2
        - ID Scorm: 617
        - ID Curso: 140
		
		Descarga en Estaciones N1
		- ID Scorm: 656
		- ID Curso: 154
		
		AGREGAR CURSOS NUEVOS CUANDO ESTE LA RELACION (LOS DOS VALORES SON DE PIXELNET)

        */


        $cursos_scorm = [];
        $cursos_scorm[6] = 459;
        $cursos_scorm[3] = 23;
        $cursos_scorm[2] = 6;
        $cursos_scorm[5] = 41;
        $cursos_scorm[26] = 137;
        $cursos_scorm[19] = 109;
        $cursos_scorm[4] = 36;
        $cursos_scorm[111] = 492;
        $cursos_scorm[112] = 494;
		$cursos_scorm[139] = 614;
		$cursos_scorm[140] =  617;
		$cursos_scorm[154] =  656;
		$cursos_scorm[173] =  710;
		$cursos_scorm[180] =  732;

        $em = $this->getDoctrine()->getManager();
        $careerPlanRepository = $this->getDoctrine()->getRepository('PlanCarreraBundle:EmployeeCareerModule');
        $employeeCareerModule = $careerPlanRepository->findByUserCourse($cursos_scorm[$course_id], $user_id);
        $approved = boolval($approved);


        if(!$employeeCareerModule){

            $text = date('Y-m-d h:i:s') . ' :: CursoId: ' . $course_id . ' :: UserId: ' . $user_id . "\n";
            $text .= "No se encontró el curso ------ " . "\n";
            $text .= "----------------------------------------------------- " . "\n";
            
            file_put_contents("_method.log", $text, FILE_APPEND);

            echo 'No se encontró curso';
            die;
        }
        $msg = '';


        $employeeCareerModule->setCourseApproved($approved);

        $employee = $employeeCareerModule->getEmployeePlan()->getUser();
        $moduleExam = $employeeCareerModule->getModule()->getExam();

        $examPermissionRepository = $this->getDoctrine()->getRepository(ExamPermission::class);

        // Buscamos el examen previo
        $examPermissionPrev = $examPermissionRepository->findByEmployeeModuleAndExam($employeeCareerModule, $moduleExam, 0);

        $text_examen_prev = '';
        $employeeModuleExams = $this->getDoctrine()->getRepository(EmployeeModuleExam::class)->findByEmployeeAndExam($moduleExam, $employee);
        if((count($employeeModuleExams) == 0)) {
            // Llegó a la aprobación del curso directamente, pero no pasó por el formulario de Puma. 
            // Fue directo desde Pixelnet, por lo tanto no tiene el curso previo realizado. 
            // Lo tenemos que crear y aprobar.

            $exam = EmployeeModuleExam::create($employee, $moduleExam, 0);
            $em->persist($exam);
            $em->flush();

            if(!is_null($exam)) {
                $exam->finish();
                $exam->setApproved(true);
                $exam->setPresencialExam(false);
                $exam->setStartsAt(new \DateTime('now'));
                $exam->setFinishedAt(new \DateTime('now'));
                $em->persist($exam);
                $em->flush();

                $text_examen_prev = 'Examen no existía. Se lo creó y aprobó.';
            }
        } else {

            foreach ($employeeModuleExams as $key => $exam) {
                if ($exam->getExamPermission()->getPrevioPost() == false && !$exam->isApproved()) {
                    // Es un examen previo, hay que aprobarlo.
                    $exam->finish();
                    $exam->setApproved(true);
                    $exam->setPresencialExam(false);
                    $exam->setStartsAt(new \DateTime('now'));
                    $exam->setFinishedAt(new \DateTime('now'));
                    $em->persist($exam);
                    $em->flush();

                    $text_examen_prev = 'Examen previo ya existía, se lo aprobó.';
                }
            }

        }


        // Buscamos si ya existe el examen posterior

        $text_examen_post = 'Examen posterior: ninguna acción.';

        $command = new ActivateModuleExamCommand($employee, $moduleExam, 1);
        $handler = $this->get('activate_exam');

        try {
            $handler($command);

            $text_examen_post = 'Examen posterior activado!';
        } catch (\Exception $e) {
            $text_examen_post = 'Examen posterior: Un error ha ocurrido. ' . $e->getMessage();
        }


        $examPermissionPost = $examPermissionRepository->findByEmployeeModuleAndExam($employeeCareerModule,$moduleExam, 1);
        if ($examPermissionPost) {
            // Tenemos que forzar la aprobación del examen?
            if (isset($_GET['approvedall']) || $course_id==139 || $course_id==173) {
                $employeeModuleExams = $this->getDoctrine()->getRepository(EmployeeModuleExam::class)->findByEmployeeAndExam($moduleExam, $employee);
                if((count($employeeModuleExams) != 0)) {
                    $countPost = 0;
                    foreach ($employeeModuleExams as $key => $exam) {

                        if ($exam->getExamPermission()->getPrevioPost() == true) {
                            $countPost = 1;

                            // Es un examen previo, hay que aprobarlo.
                            $exam->finish();
                            $exam->setApproved(true);
                            $exam->setPresencialExam(false);
                            $exam->setStartsAt(new \DateTime('now'));
                            $exam->setFinishedAt(new \DateTime('now'));
                            $em->persist($exam);
                            $em->flush();

                            $text_examen_post = 'Examen posterior ya existía, se lo aprobó.';
                        }
                    }

                    if ($countPost == 0) {
                        // Creamos y aprobamos el examen posterior.

                        $exam = EmployeeModuleExam::create($employee, $moduleExam, 1);
                        $em->persist($exam);
                        $em->flush();

                        if(!is_null($exam)) {
                            $exam->finish();
                            $exam->setApproved(true);
                            $exam->setPresencialExam(false);
                            $exam->setStartsAt(new \DateTime('now'));
                            $exam->setFinishedAt(new \DateTime('now'));
                            $em->persist($exam);
                            $em->flush();

                            $text_examen_post = 'Examen posterior no existía. Se lo creó y aprobó.';
                        }
                    }
                }
            }
        }


        if($approved){
            $msg = "Curso Interactivo: Aprobado con exito";
        } else {
            $msg = "Curso Interactivo: Reprobado con exito";
        }
        $em->flush();



        $text = date('Y-m-d h:i:s') . ' :: CursoId: ' . $course_id . ' :: UserId: ' . $user_id . "\n";
        $text .= $text_examen_prev . " ------ " . "\n";
        $text .= $msg . " ------ " . "\n";
        $text .= $text_examen_post . " ------ " . "\n";
        $text .= "----------------------------------------------------- " . "\n";
        
        file_put_contents("_method.log", $text, FILE_APPEND);



        die(nl2br($text));
    }
}
