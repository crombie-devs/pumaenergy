<?php

namespace Aper\RuffleBundle\Controller\Backend;

use Aper\RuffleBundle\Entity\Ruffle;
use Aper\RuffleBundle\Form\RuffleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class RuffleController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $contact = $this->getDoctrine()->getRepository(Ruffle::class)->findAll();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $contact,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('AperRuffleBundle:Backend:index.html.twig', array('pagination' => $pagination));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $entity = new Ruffle();
        $form = $this->createForm(new RuffleType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Registro creado');
            return $this->redirect($this->generateUrl('aper_ruffle_index'));
        }

        return $this->render('AperRuffleBundle:Backend:new.html.twig', [
            'form' => $form->createView(),
            'ruffle' => $entity,
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AperRuffleBundle:Ruffle')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se encontró el Concurso');
        }

        try {
            $em->remove($entity);
            $em->flush();
            $request->getSession()->getFlashBag()->add('success', 'Concurso Eliminado');
        } catch (\Exception $exc) {
            $request->getSession()->getFlashBag()->add('error', $exc->getMessage());
        }


        return $this->redirect($this->generateUrl('aper_ruffle_index'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $ruffle = $em->getRepository('AperRuffleBundle:Ruffle')->find($id);
        $form = $this->createForm(RuffleType::class, $ruffle);
        $form->handleRequest($request);

        if (!$ruffle) {
            throw $this->createNotFoundException('Concurso no encontrado');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'El Concurso ha sido modificado');

            return $this->redirectToRoute('aper_ruffle_index');
        }

        return $this->render('AperRuffleBundle:Backend:edit.html.twig', array('ruffle' => $ruffle, 'form' => $form->createView()));
    }

    /**
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AperRuffleBundle:Ruffle');
        $ruffle = $repository->find($id);
        if (!$ruffle) {
            $messageException = 'Concurso no encontrado';
            throw $this->createNotFoundException($messageException);
        }

        return $this->render('AperRuffleBundle:Backend:show.html.twig', array('ruffle' => $ruffle));
    }

    /**
     * @param Ruffle $ruffle
     * @return StreamedResponse
     */
    public function exportAction(Ruffle $ruffle)
    {
        if ($ruffle->isQuestion()) {
            throw new \LogicException();
        }

        $em = $this->getDoctrine()->getManager();

        if ($ruffle->isTrivia()) {
            $chooses = $em->getRepository('AperRuffleBundle:Choose')->findByRuffle($ruffle);
        } else {
            $chooses = $em->getRepository('AperRuffleBundle:Subscribe')->findByRuffle($ruffle);
        }

        $response = new StreamedResponse();
        $response->setCallback(function () use ($ruffle, $chooses) {
            $translator = $this->get('translator');
            $handle = fopen('php://output', 'w+');

            if ($ruffle->isTrivia()) {
                $headers = [
                    'CONCURSO',
                    'TIPO DE CONCURSO',
                    'NOMBRE Y APELLIDO',
                    'EMAIL',
                    'FECHA DE PARTICIPACIÓN (INI)',
                    'FECHA DE PARTICIPACIÓN (FIN)',
                    'FECHA RESPUESTA',
                    'PREGUNTA',
                    'RESPUESTA',
                    'CORRECTA'
                ];
            }
            else{
                $headers = [
                    'CONCURSO',
                    'TIPO DE CONCURSO',
                    'NOMBRE Y APELLIDO',
                    'EMAIL',
                    'FECHA DE PARTICIPACIÓN',
                ];   
            }

            fputcsv($handle, $headers, ';');

            if (!empty($chooses)) {
                if ($ruffle->isTrivia()) {
                    foreach ($chooses as $choose) {
                        $subscribe = $choose->getSubscribe();
                        $user = $subscribe->getUser();
                        $employee = $user->getEmployee();
                        $profile = $employee->getProfile();

                        $response = $choose->getResponded();
                        $question = $response->getMchoice();

                        fputcsv($handle, [
                            $ruffle->getTitle(),
                            $translator->trans($ruffle->getRuffletype()),
                            $profile->getName(),
                            $user->getEmail(),
                            $subscribe->getDatesus()->format('d/m/Y H:i:s'),
                            $subscribe->getDateEnd()->format('d/m/Y H:i:s'),
                            $choose->getCreatedAt()->format('d/m/Y H:i:s'),
                            $question->getInquiry(),
                            $response->getAnswer(),
                            $response->isCorrect() ? 'SI' : 'NO',
                        ], ';');
                    }
                } elseif ($ruffle->isDirect()) {
                    foreach ($chooses as $subscribe) {
                        $user = $subscribe->getUser();
                        $employee = $user->getEmployee();
                        $profile = $employee->getProfile();

                        fputcsv($handle, [
                            $ruffle->getTitle(),
                            $translator->trans($ruffle->getRuffletype()),
                            $profile->getName(),
                            $user->getEmail(),
                            $subscribe->getDatesus()->format('d/m/Y H:i:s'),
                        ], ';');
                    }
                }

                fclose($handle);
            }
        });


        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="export_empleados.csv"');

        return $response;
    }
}
