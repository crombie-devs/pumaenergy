<?php

namespace Aper\RuffleBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ResponseController extends Controller
{
    /**
     * Lists all Category entities.
     *
     * @Route("/response/view", name="frontend_ruffle_view")
     */
    public function viewAction()
    {
        $ruffles = $this->getDoctrine()->getRepository('AperRuffleBundle:Ruffle')->findLastestRuffle(1);

        return $this->render('AperRuffleBundle:Frontend:view.html.twig', ['ruffle' => $ruffles]);
    }
}