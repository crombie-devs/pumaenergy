<?php

namespace Aper\RuffleBundle\Controller\Frontend\Api;

use Aper\RuffleBundle\Entity\Choose;
use Aper\RuffleBundle\Entity\Response;
use Aper\RuffleBundle\Entity\Ruffle;
use Aper\RuffleBundle\Entity\Subscribe;
use Aper\RuffleBundle\Form\Frontend\Api\ResponseMultipleChoiceType;
use Aper\RuffleBundle\Form\Frontend\Api\ResponseQuestionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\UserBundle\Model\UserInterface;

/**
 * Class RuffleController
 */
class RuffleController extends Controller
{
    /**
     * @Route(path="/{ruffle}/start", methods={"POST"}, name="api_ruffle_start")
     *
     * @param Ruffle $ruffle
     * @return JsonResponse
     */
    public function startAction(Request $request, Ruffle $ruffle)
    {
        $user = $this->getUser();

        $subscribe = $this->getSubscribeByUserAndRuffle($ruffle, $user);

        if (null !== $subscribe) {
            return new JsonResponse(['data' => [
                'code' => JsonResponse::HTTP_CONFLICT,
                'message' => $this->get('translator')->trans('ruffle.already_subscribed'),
            ]], JsonResponse::HTTP_CONFLICT);
        }

        $subscribe = new Subscribe($user, $ruffle);

        $validator = $this->get('validator');
        $errors = $validator->validate($subscribe);
        if (count($errors)) {
            return new JsonResponse(['data' => $errors], JsonResponse::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($subscribe);
        $em->flush();

        return new JsonResponse(['data' => [
            'code' => JsonResponse::HTTP_OK,
            'start_at' => $subscribe->getDatesus(),
            'finish_at' => $subscribe->getDateEnd(),
        ]]);
    }

    /**
     * @Route(path="/{ruffle}/respond-choice", methods={"POST"} , name="api_ruffle_respond_multiple_choice")
     *
     * @param Ruffle $ruffle
     * @return JsonResponse
     */
    public function respondMultipleChoiceAction(Request $request, Ruffle $ruffle)
    {
        $user = $this->getUser();

        try {
            $this->ensureAjaxRequest($request);
        } catch (\LogicException $exception) {
            return new JsonResponse(unserialize($exception->getMessage()), $exception->getCode());
        }

        if (!$ruffle->isTrivia()) {
            return new JsonResponse([
                    'code' => JsonResponse::HTTP_BAD_REQUEST,
                    'message' => $this->get('translator')->trans('ruffle.type.is_not.trivia'),
                ], JsonResponse::HTTP_BAD_REQUEST);
        }

        $now = new \DateTime();
        try {
            $this->ensureRuffleIsNotOutOfDateRange($ruffle, $now);
        } catch (\LogicException $exception) {
            return new JsonResponse(unserialize($exception->getMessage()), $exception->getCode());
        }

        try {
            $subscribe = $this->ensureSubscribeToRuffle($ruffle, $user);
        } catch (\LogicException $exception) {
            return new JsonResponse(unserialize($exception->getMessage()), $exception->getCode());
        }

        try {
            $this->ensureSubscribeIsNotTimeOut($subscribe, $now);
        } catch (\LogicException $exception) {
            return new JsonResponse(unserialize($exception->getMessage()), $exception->getCode());
        }

        try {
            $content = $this->ensureContentIsNotEmpty($request);
        } catch (\LogicException $exception) {
            return new JsonResponse(unserialize($exception->getMessage()), $exception->getCode());
        }

        $response = $this->createChoose($subscribe);
        $form = $this->createForm(ResponseMultipleChoiceType::class, $response);
        $form->submit($content);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return new JsonResponse(['data' => [
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => $this->getFormErrors($form),
            ]], JsonResponse::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($response);
        $em->flush();

        /**
         * Check if ruffle is finished
         */
        $chooses = [];
        foreach ($ruffle->getMChoice() as $mChoice) {

            /** @var Choose $choose */
            $choose = $em->getRepository(Choose::class)->findOneBy([
                'mchoice' => $mChoice->getId(),
                'subscribe' => $subscribe->getId(),
            ]);

            if (null === $choose) {
                continue;
            }

            $rChoice = $choose->getResponded();
            $chooses[] = [
                'question' => $mChoice->getInquiry(),
                'answer' => $rChoice->getAnswer(),
                'right'  => $rChoice->isCorrect(),
            ];
        }

        if ($ruffle->getMChoice()->count() === count($chooses)) {
            $subscribe->finish();
            $em->flush();

            return new JsonResponse(['data' => [
                'code' => JsonResponse::HTTP_OK,
                'message' => $chooses,
            ]], JsonResponse::HTTP_OK);
        }

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route(path="/{ruffle}/respond-question", methods={"POST"} , name="api_ruffle_respond_question")
     *
     * @param Ruffle $ruffle
     * @return JsonResponse
     */
    public function respondQuestionAction(Request $request, Ruffle $ruffle)
    {
        $user = $this->getUser();

        try {
            $this->ensureAjaxRequest($request);
        } catch (\LogicException $exception) {
            return new JsonResponse(unserialize($exception->getMessage()), $exception->getCode());
        }

        if (!$ruffle->isQuestion()) {
            return new JsonResponse([
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => $this->get('translator')->trans('ruffle.type.is_not.question'),
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        $now = new \DateTime();
        try {
            $this->ensureRuffleIsNotOutOfDateRange($ruffle, $now);
        } catch (\LogicException $exception) {
            return new JsonResponse(unserialize($exception->getMessage()), $exception->getCode());
        }

        try {
            $subscribe = $this->ensureSubscribeToRuffle($ruffle, $user);
        } catch (\LogicException $exception) {
            return new JsonResponse(unserialize($exception->getMessage()), $exception->getCode());
        }

        try {
            $this->ensureSubscribeIsNotTimeOut($subscribe, $now);
        } catch (\LogicException $exception) {
            return new JsonResponse(unserialize($exception->getMessage()), $exception->getCode());
        }

        try {
            $content = $this->ensureContentIsNotEmpty($request);
        } catch (\LogicException $exception) {
            return new JsonResponse(unserialize($exception->getMessage()), $exception->getCode());
        }

        $response = $this->createResponseQuestion($subscribe);
        $form = $this->createForm(ResponseQuestionType::class, $response);
        $form->submit($content);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return new JsonResponse(['data' => [
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => $this->getFormErrors($form),
            ]], JsonResponse::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($response);
        $em->flush();

        /**
         * Check if ruffle is finished
         */
        $chooses = [];
        foreach ($ruffle->getQuestion() as $question) {

            /** @var Response $response */
            $response = $em->getRepository(Response::class)->findOneBy([
                'question' => $question->getId(),
                'subscribe' => $subscribe->getId(),
            ]);

            if (null === $response) {
                continue;
            }

            $question = $response->getQuestion();
            $chooses[] = [
                'question' => $question->getInquiry(),
                'answer' => $response->getResponded(),
            ];
        }

        if ($ruffle->getQuestion()->count() === count($chooses)) {
            $subscribe->finish();
            $em->flush();

            return new JsonResponse(['data' => [
                'code' => JsonResponse::HTTP_OK,
                'message' => $chooses,
            ]], JsonResponse::HTTP_OK);
        }

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     */
    private function ensureAjaxRequest(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new \LogicException(serialize([
                'code' => JsonResponse::HTTP_FORBIDDEN,
                'message' => $this->get('translator')->trans('request.must_be_ajax'),
            ]), JsonResponse::HTTP_FORBIDDEN);
        }
    }

    /**
     * @param Ruffle $ruffle
     * @param UserInterface $user
     * @return Subscribe|null
     */
    private function getSubscribeByUserAndRuffle(Ruffle $ruffle, UserInterface $user)
    {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository(Subscribe::class)->findOneBy([
            'user' => $user->getId(),
            'ruffle' => $ruffle->getId(),
        ]);
    }

    /**
     * @param Ruffle $ruffle
     * @param \DateTime $dateTime
     */
    private function ensureRuffleIsNotOutOfDateRange(Ruffle $ruffle, \DateTime $dateTime)
    {
        if ($ruffle->isOutOfDateRange($dateTime)) {
            throw new \LogicException(serialize([
                'code' => JsonResponse::HTTP_CONFLICT,
                'message' => $this->get('translator')->trans('ruffle.out_of_date'),
            ]), JsonResponse::HTTP_CONFLICT);
        }
    }

    /**
     * @param Ruffle $ruffle
     * @param UserInterface $user
     * @return Subscribe|null
     */
    private function ensureSubscribeToRuffle(Ruffle $ruffle, UserInterface $user)
    {
        $subscribe = $this->getSubscribeByUserAndRuffle($ruffle, $user);

        if (null === $subscribe) {
            throw new \LogicException(serialize([
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => $this->get('translator')->trans('ruffle.not_subscribed'),
            ]), JsonResponse::HTTP_BAD_REQUEST);
        }

        return $subscribe;
    }

    /**
     * @param Subscribe $subscribe
     * @param \DateTime $dateTime
     */
    private function ensureSubscribeIsNotTimeOut(Subscribe $subscribe, \DateTime $dateTime)
    {
        if ($subscribe->isTimeOut($dateTime)) {
            throw new \LogicException(serialize([
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => $this->get('translator')->trans('ruffle.time_out'),
            ]), JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    private function ensureContentIsNotEmpty(Request $request)
    {
        $content = $request->getContent();
        if (empty($content)) {
            throw new \LogicException(serialize([
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => $this->get('translator')->trans('request.content_empty'),
            ]), JsonResponse::HTTP_BAD_REQUEST);
        }

        return json_decode($content, true);
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    private function getFormErrors(FormInterface $form)
    {
        $errors = [];

        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $key => $child) {
            if ($err = $this->getFormErrors($child)) {
                $errors[$key] = $err;
            }
        }

        return $errors;
    }

    /**
     * @param Subscribe $subscribe
     * @return Choose
     */
    private function createChoose(Subscribe $subscribe)
    {
        $response = new Choose();
        $response->setSubscribe($subscribe);

        return $response;
    }

    /**
     * @param Subscribe $subscribe
     * @return Response
     */
    private function createResponseQuestion(Subscribe $subscribe)
    {
        $response = new Response();
        $response->setSubscribe($subscribe);

        return $response;
    }
}
