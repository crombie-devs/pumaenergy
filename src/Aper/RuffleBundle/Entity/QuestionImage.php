<?php

namespace Aper\RuffleBundle\Entity;

use Aper\RuffleBundle\Entity\Question;
use Doctrine\ORM\Mapping as ORM;
use WebFactory\Bundle\FileBundle\Model\Image as BaseEntity;

/**
 * Format
 *
 * @ORM\Entity
 * @ORM\Table(name="question_images")
 */
class QuestionImage extends BaseEntity
{
    const DIRECTORY = 'uploads/question/questionimages';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *
     * @var Question
     * @ORM\OneToOne(targetEntity="Question", inversedBy="questionimage")
     * @ORM\JoinColumn(name="question_id")
     */
    protected $question;

    /**
     * Set question
     *
     * @param Question $question
     * @return QuestionImage
     */
    public function setQuestion(Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
