<?php

namespace Aper\RuffleBundle\Entity;

use Aper\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Response
 *
 * @ORM\Table(
 *     name="response",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uk_response", columns={"subscribe_id", "question_id"})
 * })
 *
 * @ORM\Entity(repositoryClass="Aper\RuffleBundle\Repository\ResponseRepository")
 *
 * @UniqueEntity(
 *     fields={"subscribe", "question"},
 *     errorPath="question"
 * )
 */
class Response
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="responded", type="string", length=255, nullable=true)
     */
    private $responded;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     *
     * @var Question
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumn(name="question_id")
     */
    protected $question;

    /**
     *
     * @var Subscribe
     * @ORM\ManyToOne(targetEntity="Aper\RuffleBundle\Entity\Subscribe")
     */
    protected $subscribe;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set responded
     *
     * @param string $responded
     *
     * @return Response
     */
    public function setResponded($responded)
    {
        $this->responded = $responded;

        return $this;
    }

    /**
     * Get responded
     *
     * @return string
     */
    public function getResponded()
    {
        return $this->responded;
    }

    /**
     * Set responseini
     *
     * @param \DateTime $createdAt
     *
     * @return Response
     */
    public function setResponseIni($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get responseini
     *
     * @return \DateTime
     */
    public function getResponseIni()
    {
        return $this->createdAt;
    }

    /**
     * Set Question
     *
     * @param Question $question
     * @return Response
     */
    public function setQuestion(Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get Question
     *
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set User
     *
     * @param User $user
     * @return Response
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Subscribe
     */
    public function getSubscribe()
    {
        return $this->subscribe;
    }

    /**
     * @param Subscribe $subscribe
     */
    public function setSubscribe(Subscribe $subscribe)
    {
        $this->subscribe = $subscribe;
    }
}
