<?php

namespace Aper\RuffleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RuffleTime
 *
 * @ORM\Table(name="ruffle_time")
 * @ORM\Entity(repositoryClass="Aper\RuffleBundle\Repository\RuffleTimeRepository")
 */
class RuffleTime
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rtini", type="datetime", nullable=true)
     */
    private $rtini;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rtend", type="datetime", nullable=true)
     */
    private $rtend;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rtini
     *
     * @param \DateTime $rtini
     *
     * @return RuffleTime
     */
    public function setRtini($rtini)
    {
        $this->rtini = $rtini;

        return $this;
    }

    /**
     * Get rtini
     *
     * @return \DateTime
     */
    public function getRtini()
    {
        return $this->rtini;
    }

    /**
     * Set rtend
     *
     * @param \DateTime $rtend
     *
     * @return RuffleTime
     */
    public function setRtend($rtend)
    {
        $this->rtend = $rtend;

        return $this;
    }

    /**
     * Get rtend
     *
     * @return \DateTime
     */
    public function getRtend()
    {
        return $this->rtend;
    }
}
