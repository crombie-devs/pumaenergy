<?php

namespace Aper\RuffleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use WebFactory\Bundle\FileBundle\Model\File as BaseEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Format
 *
 * @ORM\Entity
 * @ORM\Table(name="ruffle_pdf")
 *
 */
class Legal extends BaseEntity
{
    const DIRECTORY = 'uploads/ruffle/pdf';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *
     * @var Ruffle
     * @ORM\OneToOne(targetEntity="Ruffle", inversedBy="legal")
     * @ORM\JoinColumn(name="ruffle_id")
     */
    protected $ruffle;

    /**
     * @var UploadedFile
     * @Assert\File(
     *     maxSize = "10M",
     *     mimeTypes = {"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage = "Please upload a valid PDF"
     * )
     *
     */
    protected $file;

    /**
     * Set FilePath
     *
     * @param string $filePath
     *
     * @return Legal
     */
    public function setFilePath($filePath)
    {
        $this->filePath =  str_replace('.tmp', '.pdf', $filePath);

        return $this;
    }

    /**
     * @param mixed $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * Set book
     *
     * @param Ruffle $ruffle
     *
     * @return Legal
     */
    public function setRuffle(Ruffle $ruffle = null)
    {
        $this->ruffle = $ruffle;

        return $this;
    }

    /**
     * Get ruffle
     *
     * @return Ruffle
     */
    public function getRuffle()
    {
        return $this->ruffle;
    }
}
