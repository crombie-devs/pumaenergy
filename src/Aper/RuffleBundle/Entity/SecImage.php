<?php

namespace Aper\RuffleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use WebFactory\Bundle\FileBundle\Model\Image as BaseEntity;

/**
 * Format
 *
 * @ORM\Entity
 * @ORM\Table(name="ruffle_secimages")
 */
class SecImage extends BaseEntity
{
    const DIRECTORY = 'uploads/ruffle/secimages';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *
     * @var Ruffle
     * @ORM\OneToOne(targetEntity="Ruffle", inversedBy="secImage", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="ruffle_id", referencedColumnName="id")
     */
    protected $ruffle;

    /**
     * Set book
     *
     * @param Ruffle $ruffle
     * @return SecImage
     */
    public function setRuffle(Ruffle $ruffle = null)
    {
        $this->ruffle = $ruffle;

        return $this;
    }

    /**
     * Get ruffle
     *
     * @return Ruffle
     */
    public function getRuffle()
    {
        return $this->ruffle;
    }
}
