<?php

namespace Aper\RuffleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use WebFactory\Bundle\FileBundle\Model\Image;

/**
 * @ORM\Table(name="mchoice_images")
 * @ORM\Entity(repositoryClass="")
 */
class MChoiceImage extends Image
{
    const DIRECTORY = 'uploads/mchoice/images';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var MChoice
     *
     * @ORM\OneToOne(targetEntity="Aper\RuffleBundle\Entity\MChoice", inversedBy="image")
     */
    private $mchoice;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return MChoice
     */
    public function getMchoice()
    {
        return $this->mchoice;
    }

    /**
     * @param MChoice $mchoice
     */
    public function setMchoice($mchoice)
    {
        $this->mchoice = $mchoice;
    }
}
