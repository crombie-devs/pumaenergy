<?php

namespace Aper\RuffleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use WebFactory\Bundle\FileBundle\Model\Image as BaseEntity;

/**
 * Format
 *
 * @ORM\Entity
 * @ORM\Table(name="ruffle_images")
 */
class MainImage extends BaseEntity
{
    const DIRECTORY = 'uploads/ruffle/images';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *
     * @var Ruffle
     * @ORM\OneToOne(targetEntity="Ruffle", inversedBy="mainImage")
     * @ORM\JoinColumn(name="ruffle_id")
     */
    protected $ruffle;

    /**
     * Set book
     *
     * @param Ruffle $ruffle
     * @return MainImage
     */
    public function setRuffle(Ruffle $ruffle = null)
    {
        $this->ruffle = $ruffle;

        return $this;
    }

    /**
     * Get ruffle
     *
     * @return Ruffle
     */
    public function getRuffle()
    {
        return $this->ruffle;
    }
}
