<?php

namespace Aper\RuffleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MChoice
 *
 * @ORM\Table(name="rchoice")
 * @ORM\Entity(repositoryClass="Aper\RuffleBundle\Repository\RChoiceRepository")
 */
class RChoice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string", length=255, nullable=true)
     */
    private $answer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="righta", type="boolean", nullable=true)
     */
    protected $righta;

    /**
    *
    * @var MChoice
    * @ORM\ManyToOne(targetEntity="MChoice", inversedBy="rchoice")
    * @ORM\JoinColumn(name="mchoice_id")
    */
    protected $mchoice;

    /**
     * @var Choose[]
     *
     * @ORM\OneToMany(targetEntity="Aper\RuffleBundle\Entity\Choose", mappedBy="responded", cascade={"all"})
     */
    protected $chooses;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return RChoice
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @return boolean
     */
    public function getRighta()
    {
        return $this->righta;
    }

    /**
     * @param boolean $righta
     */
    public function setRighta($righta)
    {
        $this->righta = $righta;
    }

    /**
     * @return MChoice
     */
    public function getMchoice()
    {
        return $this->mchoice;
    }

    /**
     * @param MChoice $mchoice
     */
    public function setMchoice($mchoice)
    {
        $this->mchoice = $mchoice;
    }

    /**
     * @return boolean
     */
    public function isCorrect()
    {
        return (bool) $this->righta;
    }
}
