<?php

namespace Aper\RuffleBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * MChoice
 *
 * @ORM\Table(name="mchoice")
 * @ORM\Entity(repositoryClass="Aper\RuffleBundle\Repository\MChoiceRepository")
 *
 * @Assert\Callback(callback="validateOneCorrectAnswer")
 */
class MChoice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="inquiry", type="string", length=255, nullable=true)
     */
    private $inquiry;

    /**
    *
    * @var Ruffle
    * @ORM\ManyToOne(targetEntity="Ruffle", inversedBy="mchoice")
    * @ORM\JoinColumn(name="ruffle_id")
    */
    protected $ruffle;

    /**
     * @var MChoiceImage
     *
     * @ORM\OneToOne(targetEntity="Aper\RuffleBundle\Entity\MChoiceImage", mappedBy="mchoice", cascade={"all"}, orphanRemoval=true)
     */
    private $image;

    /**
     * @var RChoice[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Aper\RuffleBundle\Entity\RChoice", mappedBy="mchoice", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $rchoice;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inquiry
     *
     * @param string $inquiry
     *
     * @return MChoice
     */
    public function setInquiry($inquiry)
    {
        $this->inquiry = $inquiry;

        return $this;
    }

    /**
     * Get inquiry
     *
     * @return string
     */
    public function getInquiry()
    {
        return $this->inquiry;
    }

    /**
     * Set Ruffle
     *
     * @param Ruffle $ruffle
     * @return MChoice
     */
    public function setRuffle(Ruffle $ruffle = null)
    {
        $this->ruffle = $ruffle;

        return $this;
    }

    /**
     * Get ruffle
     *
     * @return Ruffle
     */
    public function getRuffle()
    {
        return $this->ruffle;
    }

    /**
     * @return MChoiceImage
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param MChoiceImage $image
     */
    public function setImage(MChoiceImage $image)
    {
        $image->setMchoice($this);
        $this->image = $image;
    }

    /**
     * Add rchoice
     *
     * @param RChoice $rchoice
     * @return MChoice
     */
    public function addRchoice(RChoice $rchoice)
    {
        $rchoice->setMchoice($this);
        $this->rchoice[] = $rchoice;

        return $this;
    }

    /**
     * @param RChoice $rchoice
     */
    public function removeRchoice(RChoice $rchoice)
    {
        $this->rchoice->removeElement($rchoice);
    }

    /**
     * Get rchoice []
     *
     */
    public function getRchoice()
    {
        return $this->rchoice;
    }

    /**
     * @param ExecutionContextInterface $context
     */
    public function validateOneCorrectAnswer(ExecutionContextInterface $context)
    {
        $corrects = [];
        $keys = [];

        foreach ($this->rchoice as $key => $rchoice) {
            $keys[] = $key;

            if ($rchoice->isCorrect()) {
                $corrects[] = $key;
            }
        }

        $count = count($corrects);
        if (1 === $count) {
            return;
        }

        if (0 === $count) {
            foreach ($keys as $key) {
                $context->buildViolation('multiple_choice.choose_correct_response.one')
                    ->atPath(sprintf("rchoice[%d].righta", $key))
                    ->addViolation();
            }

            return;
        }

        foreach ($corrects as $key) {
            $context->buildViolation('multiple_choice.choose_correct_response.only_one')
                ->atPath(sprintf("rchoice[%d].righta", $key))
                ->addViolation();
        }
    }
}
