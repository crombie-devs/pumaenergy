<?php

namespace Aper\RuffleBundle\Entity;

use Aper\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Subscribe
 *
 * @ORM\Table(name="subscribe")
 * @ORM\Entity(repositoryClass="Aper\RuffleBundle\Repository\SubscribeRepository")
 */
class Subscribe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datesus", type="datetime", nullable=true)
     */
    private $datesus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="Aper\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id")
     */
    protected $user;

    /**
     *
     * @var Ruffle
     * @ORM\ManyToOne(targetEntity="Aper\RuffleBundle\Entity\Ruffle", inversedBy="subscribes")
     * @ORM\JoinColumn(name="ruffle_id")
     */
    protected $ruffle;

    /**
     * Subscribe constructor.
     * @param User $user
     * @param Ruffle $ruffle
     */
    public function __construct(User $user, Ruffle $ruffle)
    {
        $this->user = $user;
        $this->ruffle = $ruffle;
        $this->datesus = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datesus
     *
     * @param \DateTime $datesus
     *
     * @return Subscribe
     */
    public function setDatesus($datesus)
    {
        $this->datesus = $datesus;

        return $this;
    }

    /**
     * Get datesus
     *
     * @return \DateTime
     */
    public function getDatesus()
    {
        return $this->datesus;
    }

    /**
     * Set User
     *
     * @param User $user
     * @return Subscribe
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get User
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set Ruffle
     *
     * @param Ruffle $ruffle
     * @return Subscribe
     */
    public function setRuffle(Ruffle $ruffle = null)
    {
        $this->ruffle = $ruffle;

        return $this;
    }

    /**
     * Get ruffle
     *
     * @return Ruffle
     */
    public function getRuffle()
    {
        return $this->ruffle;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    public function finish()
    {
        $this->dateEnd = new \DateTime();
    }

    /**
     * @param \DateTime $dateTime
     * @return bool
     */
	public function isTimeOut(\DateTime $dateTime)
	{
		if ($this->ruffle->getTimeResponse() == 0) {
			return false;
		}

		$dateEnd = clone $this->datesus;
		$dateEnd->add(new \DateInterval('PT'.$this->ruffle->getTimeResponse().'S'));

		return strcmp($dateEnd->format('Y-m-d H:i:s'), $dateTime->format('Y-m-d H:i:s')) <= 0;
	}
}
