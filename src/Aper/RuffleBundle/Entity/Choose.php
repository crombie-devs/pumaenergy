<?php

namespace Aper\RuffleBundle\Entity;

use Aper\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Choose
 *
 * @ORM\Table(
 *     name="choose",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uk_choose", columns={"subscribe_id", "mchoice_id"})
 * })
 * @ORM\Entity(repositoryClass="Aper\RuffleBundle\Repository\ChooseRepository")
 *
 * @UniqueEntity(
 *     fields={"subscribe", "mchoice"},
 *     errorPath="mchoice"
 * )
 */
class Choose
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var RChoice
     * @ORM\ManyToOne(targetEntity="RChoice", inversedBy="chooses")
     * @ORM\JoinColumn(name="responded_id")
     * @Assert\NotBlank()
     */
    protected $responded;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\NotBlank()
     */
    private $createdAt;

    /**
     *
     * @var MChoice
     * @ORM\ManyToOne(targetEntity="MChoice")
     * @ORM\JoinColumn(name="mchoice_id")
     * @Assert\NotBlank()
     */
    protected $mchoice;

    /**
     *
     * @var Subscribe
     * @ORM\ManyToOne(targetEntity="Aper\RuffleBundle\Entity\Subscribe")
     */
    protected $subscribe;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set responded
     *
     * @param RChoice $responded
     *
     * @return Choose
     */
    public function setResponded(RChoice $responded)
    {
        $this->mchoice = $responded->getMchoice();
        $this->responded = $responded;

        return $this;
    }

    /**
     * Get responded
     *
     * @return RChoice
     */
    public function getResponded()
    {
        return $this->responded;
    }

    /**
     * Get responseAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set MChoice
     *
     * @param MChoice $mchoice
     * @return Choose
     */
    public function setMChoice(MChoice $mchoice = null)
    {
        $this->mchoice = $mchoice;

        return $this;
    }

    /**
     * Get MChoice
     *
     * @return MChoice
     */
    public function getMChoice()
    {
        return $this->mchoice;
    }

    /**
     * @return Subscribe
     */
    public function getSubscribe()
    {
        return $this->subscribe;
    }

    /**
     * @param Subscribe $subscribe
     */
    public function setSubscribe(Subscribe $subscribe)
    {
        $this->subscribe = $subscribe;
    }
}
