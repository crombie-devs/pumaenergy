<?php

namespace Aper\RuffleBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Ruffle
 *
 * @ORM\Table(name="ruffle")
 * @ORM\Entity(repositoryClass="Aper\RuffleBundle\Repository\RuffleRepository")
 */
class Ruffle
{
    const TYPE_DIRECT = 'ruffle.type.direct';

    const TYPE_QUESTION = 'ruffle.type.question';

    const TYPE_TRIVIA = 'ruffle.type.trivia';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateIni", type="date")
     */
    private $dateIni;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="date")
     */
    private $dateEnd;


    /**
     * @var integer
     *
     * @ORM\Column(name="timeresponse", type="integer")
     */
    private $timeResponse;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     *
     * @var MainImage
     * @ORM\OneToOne(targetEntity="MainImage", mappedBy="ruffle", cascade={"all"})
     * @Assert\NotBlank()
     * @Assert\Valid()
     */
    private $mainImage;

    /**
     *
     * @var SecImage
     * @ORM\OneToOne(targetEntity="SecImage", mappedBy="ruffle", cascade={"all"})
     * @Assert\NotBlank()
     * @Assert\Valid()
     */
    private $secImage;

    /**
     *
     * @var Legal
     * @ORM\OneToOne(targetEntity="Legal", mappedBy="ruffle", cascade={"all"})
     * @Assert\NotBlank()
     */
    private $legal;

    /**
     * @var string
     *
     * @ORM\Column(name="ruffletype", type="string", length=255)
     */
    private $ruffletype;

    /**
     * @var string[]
     *
     * @ORM\Column(name="roles_allowed", type="simple_array")
     * @Assert\Count(min="1", minMessage="Debe elegir un rol como mínimo")
     */
    private $rolesAllowed;

    /**
     * @var Question[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Aper\RuffleBundle\Entity\Question", mappedBy="ruffle", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"inquiry" = "DESC"})
     */
    private $question;

    /**
     * @var MChoice[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Aper\RuffleBundle\Entity\MChoice", mappedBy="ruffle", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"id" = "ASC"})
     * @Assert\Valid()
     */
    private $mchoice;

    /**
     * @var Subscribe[]
     *
     * @ORM\OneToMany(targetEntity="Aper\RuffleBundle\Entity\Subscribe", mappedBy="ruffle", cascade={"all"})
     */
    protected $subscribes;

    /**
     * Ruffle constructor.
     */
    public function __construct()
    {
        $this->question = new ArrayCollection();
        $this->mchoice  = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateIni
     *
     * @param \DateTime $dateIni
     *
     * @return Ruffle
     */
    public function setDateIni($dateIni)
    {
        $this->dateIni = $dateIni;

        return $this;
    }

    /**
     * Get dateIni
     *
     * @return \DateTime
     */
    public function getDateIni()
    {
        return $this->dateIni;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Ruffle
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set timeResponse
     *
     * @param integer $timeResponse
     *
     * @return Ruffle
     */
    public function setTimeResponse($timeResponse)
    {
        $this->timeResponse = $timeResponse;

        return $this;
    }

    /**
     * Get timeResponse
     *
     * @return integer
     */
    public function getTimeResponse()
    {
        return $this->timeResponse;
    }

    /**
     * @param MainImage $mainImage
     *
     * @return $this
     */
    public function setMainImage(MainImage $mainImage)
    {
        $this->mainImage = $mainImage;
        $mainImage->setRuffle($this);

        return $this;
    }

    /**
     * @return MainImage
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    public function setSecImage(SecImage $secImage)
    {
        $this->secImage = $secImage;
        $secImage->setRuffle($this);

        return $this;
    }

    /**
     * @return SecImage
     */
    public function getSecImage()
    {
        return $this->secImage;
    }

    /**
     * @param Legal $legal
     *
     * @return $this
     */
    public function setLegal(Legal $legal)
    {
        $this->legal = $legal;
        $legal->setRuffle($this);

        return $this;
    }

    /**
     * Get legal
     *
     * @return string
     */
    public function getLegal()
    {
        return $this->legal;
    }

    /**
     * Set ruffletype
     *
     * @param string $ruffletype
     *
     * @return Ruffle
     */
    public function setRuffletype($ruffletype)
    {
        $this->ruffletype = $ruffletype;

        return $this;
    }

    /**
     * Get ruffletype
     *
     * @return string
     */
    public function getRuffletype()
    {
        return $this->ruffletype;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Ruffle
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Ruffle
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }
    /**
     * Set rolesAllowed.
     *
     * @param string $rolesAllowed
     *
     * @return Ruffle
     */
    public function setRolesAllowed($rolesAllowed)
    {
        $this->rolesAllowed = $rolesAllowed;

        return $this;
    }

    /**
     * Get rolesAllowed.
     *
     * @return string
     */
    public function getRolesAllowed()
    {
        return $this->rolesAllowed;
    }

    /**
     * @param $roleAllowed
     *
     * @return $this
     */
    public function addRoleAllowed($roleAllowed)
    {
        $roleAllowed = strtoupper($roleAllowed);

        if (!in_array($roleAllowed, $this->rolesAllowed)) {
            $this->rolesAllowed[] = $roleAllowed;
        }

        return $this;
    }

    /**
     * Add question
     *
     * @param Question $question
     *
     * @return Ruffle
     */
    public function addQuestion(Question $question)
    {
        $question->setRuffle($this);
        $this->question[] = $question;

        return $this;
    }

    /**
     * @param Question $question
     */
    public function removeQuestion(Question $question)
    {
        $this->question->removeElement($question);
    }

    /**
     * Get question []
     *
     */
    public function getQuestion()
    {
        $question = $this->question;

        return $question;
    }

    /**
     * Add mchoice
     *
     * @param MChoice $mchoice
     * @return Ruffle
     */
    public function addMChoice(MChoice $mchoice)
    {
        $mchoice->setRuffle($this);
        $this->mchoice[] = $mchoice;

        return $this;
    }

    /**
     * @param MChoice $mchoice
     */
    public function removeMChoice(MChoice $mchoice)
    {
        $this->mchoice->removeElement($mchoice);
    }

    /**
     * Get mchoice []
     *
     */
    public function getMChoice()
    {
        $mchoice = $this->mchoice;
        return $mchoice;
    }

    /**
     * @return bool
     */
    public function isDirect()
    {
        return $this->ruffletype == self::TYPE_DIRECT;
    }

    /**
     * @return bool
     */
    public function isQuestion()
    {
        return $this->ruffletype == self::TYPE_QUESTION;
    }

    /**
     * @return bool
     */
    public function isTrivia()
    {
        return $this->ruffletype == self::TYPE_TRIVIA;
    }

    public static function getTypeChoices()
    {
        return [
            self::TYPE_DIRECT => self::TYPE_DIRECT,
            self::TYPE_QUESTION => self::TYPE_QUESTION,
            self::TYPE_TRIVIA => self::TYPE_TRIVIA,
        ];
    }

    /**
     * @param \DateTime $dateTime
     * @return bool
     */
    public function isOutOfDateRange(\DateTime $dateTime)
    {
        return $dateTime->format('Y-m-d') < $this->dateIni->format('Y-m-d') ||
            $this->dateEnd->format('Y-m-d') < $dateTime->format('Y-m-d');
    }
}
