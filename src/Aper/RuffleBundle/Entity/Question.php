<?php

namespace Aper\RuffleBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="Aper\RuffleBundle\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="inquiry", type="string", length=255, nullable=true)
     */
    private $inquiry;

    /**
     *
     * @var QuestionImage
     * @ORM\OneToOne(targetEntity="QuestionImage", mappedBy="question", cascade={"all"})
     * @Assert\Valid()
     */
    private $questionimage;

    /**
    *
    * @var Ruffle
    * @ORM\ManyToOne(targetEntity="Ruffle", inversedBy="question")
    * @ORM\JoinColumn(name="ruffle_id")
    */
    protected $ruffle;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inquiry
     *
     * @param string $inquiry
     *
     * @return Question
     */
    public function setInquiry($inquiry)
    {
        $this->inquiry = $inquiry;

        return $this;
    }

    /**
     * Get inquiry
     *
     * @return string
     */
    public function getInquiry()
    {
        return $this->inquiry;
    }

    /**
     * Set questionimage
     *
     * @param QuestionImage $questionimage
     *
     * @return Question
     */
    public function setQuestionimage(QuestionImage $questionimage)
    {
        $this->questionimage = $questionimage;

        $questionimage->setQuestion($this);

        return $this;
    }

    /**
     * Get questionimage
     *
     * @return string
     */
    public function getQuestionimage()
    {
        return $this->questionimage;
    }

    /**
     * Set Ruffle
     *
     * @param Ruffle $ruffle
     * @return Question
     */
    public function setRuffle(Ruffle $ruffle = null)
    {
        $this->ruffle = $ruffle;

        return $this;
    }

    /**
     * Get ruffle
     *
     * @return Ruffle
     */
    public function getRuffle()
    {
        return $this->ruffle;
    }
}
