<?php

namespace Aper\RuffleBundle\Repository;
use Aper\RuffleBundle\Entity\Ruffle;
use Doctrine\ORM\EntityRepository;

/**
 * ChooseRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ChooseRepository extends EntityRepository
{
    public function findByRuffle(Ruffle $ruffle)
    {
        $queryBuilder = $this->createQueryBuilder('Choose')
            ->select('Choose', 'Subscribe', 'MChoice', 'RChoice')
            ->leftJoin('Choose.subscribe', 'Subscribe')
            ->leftJoin('Choose.mchoice', 'MChoice')
            ->leftJoin('Choose.responded', 'RChoice')
            ->where('Subscribe.ruffle = :ruffle')->setParameter('ruffle', $ruffle);

        return $queryBuilder->getQuery()->getResult();
    }
}
