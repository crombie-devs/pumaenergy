<?php

namespace Aper\RuffleBundle\Repository;

use Doctrine\ORM\Mapping\ClassMetadata;


/**
 * RuffleRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RuffleRepository extends \Doctrine\ORM\EntityRepository

{
    /**
     *
     * @param int $limit
     * @return type
     */
    public function findLastestRuffle($limit = 1, $role = null)
    {
        $now = new \DateTime("now");

        if($role == null) {
            $queryBuilder = $this->createQueryBuilder('Ruffle')
                ->setMaxResults($limit)
                ->where('Ruffle.dateIni <= :ini')
                ->setParameter('ini', $now->format('Y-m-d'))
                ->andWhere('Ruffle.dateEnd >= :end')
                ->setParameter('end', $now->format('Y-m-d'));
        }
        else {
                $queryBuilder = $this->createQueryBuilder('Ruffle')
                ->setMaxResults($limit)
                ->where('Ruffle.dateIni <= :ini')
                ->setParameter('ini', $now->format('Y-m-d'))
                ->andWhere('Ruffle.dateEnd >= :end')
                ->setParameter('end', $now->format('Y-m-d'));
                // ->andWhere("Ruffle.rolesAllowed LIKE '%". $role . "%'")

                foreach($role as $i => $role_aux) {
                    $query[] = "Ruffle.rolesAllowed LIKE :role_aux{$i}";
                    $queryBuilder->setParameter('role_aux' . $i, '%'.$role_aux.'%');
                }
                
                $queryBuilder->andWhere(implode(' OR ', $query));
        }

	    $query = $queryBuilder->getQuery();

	    return $query->setFetchMode('AperRuffleBundle\Ruffle', 'mchoice', ClassMetadata::FETCH_EAGER)->getOneOrNullResult();
    }

}
