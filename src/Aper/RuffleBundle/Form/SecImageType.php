<?php

namespace Aper\RuffleBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\FileBundle\Form\ImageType as Base;

class SecImageType extends Base
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\RuffleBundle\Entity\SecImage'
        ));
    }

    public function getName()
    {
        return 'secimage';
    }
}
