<?php

namespace Aper\RuffleBundle\Form;


use Aper\RuffleBundle\Entity\MChoice;
use Aper\RuffleBundle\Entity\Ruffle;
use Doctrine\Common\Collections\Collection;
use DoctrineExtensions\Query\Mysql\DateFormat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Aper\UserBundle\Model\Role;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;



class RuffleType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $types = Ruffle::getTypeChoices();
        unset($types[Ruffle::TYPE_QUESTION]);

        $roles = Role::getChoices();
        unset($roles['ROLE_ADMIN']);
        unset($roles['ROLE_STORE_MANAGER']);
        $builder
            ->add('title', 'text', array())
            ->add('description', 'text', array())
            ->add('ruffletype', ChoiceType::class, [
                'choices' => $types,
                'required' => true,
            ])
            ->add('timeresponse', NumberType::class, array())
            ->add('dateIni', DateType::class, [
                'label' => 'ruffle.date_ini',
                'widget' => 'single_text',
                ])
            ->add('dateEnd', DateType::class, [
                'label' => 'ruffle.date_end',
                'widget' => 'single_text'
            ])

            //MCHOICE
            ->add('mchoice', CollectionType::class, [
                'type' => MChoiceType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'widget_add_btn' => [
                    'label' => 'AGREGAR PREGUNTA',
                    'icon' => '',
                     'attr' => [
                        'class' => 'btn btn-primary',
                        'title' => 'OPCION +',
                    ],
                ],
                'options' => [
                    'widget_remove_btn' => [
                        'label' => 'ELIMINAR PREGUNTA',
                        'attr' => [
                            'class' => 'btn btn-danger',
                            'title' => 'ELIMINAR PREGUNTA',
                        ],
                        'icon' => '',
                        'wrapper_div' => [
                            'class' => 'span12',
                        ],
                    ],
                    'attr' => [
                        'class' => 'span12',
                    ],
                    'label' => false,
                    'error_bubbling' => false,
                    'show_child_legend' => false,
                ],
                'show_legend' => false,
                'show_child_legend' => false,
            ])

            ->add('mainImage', new MainImageType(), array())
            ->add('secImage', new SecImageType(), array())
            ->add('legal', new LegalType(), array())
            ->add('rolesAllowed', ChoiceType::class, [
                'choices' => $roles,
                'expanded' => true,
                'multiple' => true
            ])
            ->add('save', SubmitType::class);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\RuffleBundle\Entity\Ruffle'
        ));
    }

    public function getName()
    {
        return 'ruffle';
    }

}
