<?php

namespace Aper\RuffleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MChoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('inquiry', 'text', array())
            ->add('image', new MChoiceImageType())
            ->add('rchoice', CollectionType::class, [
                'type' => RChoiceType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'widget_add_btn' => [
                    'label' => 'respuesta +',
                    'icon' => '',
                    'attr' => [
                        'class' => 'btn btn-primary',
                        'title' => 'RESPUESTA +',
                    ],
                ],
                'options' => [
                    'widget_remove_btn' => [
                        'label' => 'ELIMINAR RESPUESTA',
                        'attr' => [
                            'class' => 'btn btn-danger',
                            'title' => 'ELIMINAR RESPUESTA',
                        ],
                        'icon' => '',
                        'wrapper_div' => [
                            'class' => 'span12',
                        ],
                    ],
                    'attr' => [
                        'class' => 'span12',
                    ],
                    'label' => false,
                    'error_bubbling' => false,
                    'show_child_legend' => false,
                ],
                'show_legend' => false,
                'show_child_legend' => false,
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\RuffleBundle\Entity\MChoice'
        ));
    }

    public function getName()
    {
        return 'mchoice';
    }
}
