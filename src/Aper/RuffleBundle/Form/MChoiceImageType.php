<?php

namespace Aper\RuffleBundle\Form;

use Aper\RuffleBundle\Entity\MChoiceImage;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\FileBundle\Form\ImageType;

class MChoiceImageType extends ImageType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => MChoiceImage::class
        ));
    }

    public function getName()
    {
        return 'mchoice_image';
    }
}
