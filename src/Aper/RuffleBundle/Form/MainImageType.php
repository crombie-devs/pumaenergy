<?php

namespace Aper\RuffleBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\FileBundle\Form\ImageType as Base;

class MainImageType extends Base
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\RuffleBundle\Entity\MainImage'
        ));
    }

    public function getName()
    {
        return 'mainimage';
    }
}
