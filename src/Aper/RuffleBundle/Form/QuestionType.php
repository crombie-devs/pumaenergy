<?php

namespace Aper\RuffleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class QuestionType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('questionImage', new QuestionImageType(), array())
            ->add('inquiry', 'text', array())

        ;

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\RuffleBundle\Entity\Question'
        ));
    }

    public function getName()
    {
        return 'question';
    }

}
