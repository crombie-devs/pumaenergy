<?php

namespace Aper\RuffleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class RChoiceType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('answer', 'text', array())
            ->add('righta', 'checkbox', array('required'=>false))
        ;

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aper\RuffleBundle\Entity\RChoice'
        ));
    }

    public function getName()
    {
        return 'rchoice';
    }

}
