<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            //----------------------------------------------------------------------------------------------------------
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Mopa\Bundle\BootstrapBundle\MopaBootstrapBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),
            //----------------------------------------------------------------------------------------------------------
            new WebFactory\Bundle\BackOfficeThemeBundle\WebFactoryBackOfficeThemeBundle(),
            new WebFactory\Bundle\GridBundle\WebFactoryGridBundle(),
            new WebFactory\Bundle\UserBundle\WebFactoryUserBundle(),
            new WebFactory\Bundle\FileBundle\WebFactoryFileBundle(),
            //----------------------------------------------------------------------------------------------------------
            new Aper\UserBundle\UserBundle(),
            new Aper\StoreBundle\StoreBundle(),
            new Aper\BenefitBundle\AperBenefitBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new Aper\CommunicationBundle\AperCommunicationBundle(),
            new Aper\NewsBundle\AperNewsBundle(),
            new Aper\RuffleBundle\AperRuffleBundle(),
            new Aper\CourseBundle\AperCourseBundle(),
            new Aper\DevelopBundle\AperDevelopBundle(),
            new Aper\IncentiveBundle\IncentiveBundle(),
            new Aper\TrainingKitBundle\TrainingKitBundle(),
            new Liuggio\ExcelBundle\LiuggioExcelBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Tetranz\Select2EntityBundle\TetranzSelect2EntityBundle(),
            new Genemu\Bundle\FormBundle\GenemuFormBundle(),
            new Vihuvac\Bundle\RecaptchaBundle\VihuvacRecaptchaBundle(),
            new Aper\PoliticsAndProceduresBundle\AperPoliticsAndProceduresBundle(),
            new Aper\BackdoorBundle\AperBackdoorBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Aper\PlanCarreraBundle\PlanCarreraBundle(),
            new EasyCorp\Bundle\EasyAdminBundle\EasyAdminBundle(),
            new Aper\CodeBundle\CodeBundle(),
			new Aper\SurpriseTargetBundle\SurpriseTargetBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }



}
