var locationToSearch = '';
var cityToSearch = '';
var categoryToSearch = '';


document.addEventListener('DOMContentLoaded', function () {

    ajaxSearchResult();
    select2_categories();
    select2_locations();

    $('.filters').hide();

    $("#filterby-location-city").select2({
        placeholder: 'Localidad',
        allowClear: true,
        minimumResultsForSearch: -1,
        width: 175
    });
    $(document).on('change', ".filterby-location-city", function () {
        cityToSearch = $(this).val();
        ajaxSearchResult();
    })

    $(document).on('click', '.navigationBenefits .paginator a', function (e) {
        e.preventDefault();
        if ($(this).hasClass('active')) return false;
        $('.benefitsList').animate({opacity: 0}, 400)
        $('.benefitsList').css('min-height', $('.benefitsList').height());
        // $('body').animate({scrollTop: $('.descuentosTitle').offset().top}, 400);
        ajaxSearchResult($(this).attr('href'), $(this).attr('data-page'));

    })

    setTimeout( function filterShow() {
        $('.filters').show();
        // $('.filters').animate({opacity: 1}, 500);
    }, 4000)


});

function select2_categories() {
    // endPoint + '/categorias/?key=' + apiKey + '&micrositio_id=' + microSiteID
    var urlToCategories = backEndCategoriesEndpoint + '?key=' + apiKey + '&micrositio_id=' + microSiteID +  '&codigo_afiliado=' + codigoAfiliado;
    $.ajax(urlToCategories, {
        dataType: 'json',
        success: function (response) {
            var cats = [];

            $.each(response, function (index, cat) {
                var temp_cat = {id: cat.id, text: cat.nombre}
                cats.push(temp_cat);
            })

            //Alphabetical order
            cats.sort(compare);


            $(".filterby-category").select2({
                data: cats,
                placeholder: 'Categoría',
                allowClear: true,
                minimumResultsForSearch: -1,
                width: 175
            });

            $(".filterby-category").on('change', function () {
                categoryToSearch = $(this).val();
                ajaxSearchResult();

                var data = $('#filterby-category').select2('data');
                var text = data[0].text;
                $('#cat-title').replaceWith('<h3 class="title text-center" id="cat-title" style="color: red; font-weight: bolder; text-transform: uppercase;">' + text + '</h3>');

            })



        }



    })
}


function compare(a, b) {
    if (a.text < b.text)
        return -1;
    if (a.text > b.text)
        return 1;
    return 0;
}


function select2_locations() {
    // endPoint + '/filtros/?key=' + apiKey + '&micrositio_id=' + microSiteID
    var urlToLocations = backEndLocationsEndpoint + '?key=' + apiKey + '&micrositio_id=' + microSiteID +  '&codigo_afiliado=' + codigoAfiliado;
    $.ajax(urlToLocations, {
        dataType: 'json',
        success: function (response) {
            var locations = [];
            var arrayLocations = Object.keys(response.ubicacion)

            $.each(arrayLocations, function (index, filter) {
                var _tmp = {
                    id: filter,
                    text: filter,
                    childs: response.ubicacion[filter]
                }

                locations.push(_tmp)
            })
            $(".filterby-location").select2({
                data: locations,
                placeholder: 'Provincia',
                allowClear: true,
                minimumResultsForSearch: -1,
                width: 175
            });

            $(".filterby-location").on('change', function () {
                locationToSearch = $(this).val();
                var childsArray = [];
                locations.forEach(function (val, index) {
                    if (val.id == locationToSearch) {
                        var _childsArray = Object.keys(val.childs);
                        $.each(_childsArray, function (index, childVal) {
                            var _tempChilds = {
                                id: childVal,
                                text: childVal
                            }
                            childsArray.push(_tempChilds)
                        });
                        $(".filterby-location-city option:not(:first-child)").remove();

                        $(".filterby-location-city").select2({
                            data: childsArray,
                            placeholder: 'Localidad',
                            minimumResultsForSearch: -1,
                            allowClear: true,
                            width: 175
                        });
                    }
                })
                ajaxSearchResult()
            });
        }
    })
}

function ajaxSearchResult(url2, activePage) {

    $('.benefitsList').animate({opacity: 0}, 400);



    $('.loaderSpinner').fadeIn(400);

    // var url = endPoint + '/cupones/?key=' + apiKey + '&micrositio_id=' + microSiteID +  '&codigo_afiliado=' + codigoAfiliado;

   setTimeout( function () {

       var url = backEndCuponEndpoint + '?key=' + apiKey + '&micrositio_id=' + microSiteID +  '&codigo_afiliado=' + codigoAfiliado;
       activePage = activePage || null
       if (locationToSearch) {

           if (cityToSearch || $('.filterby-location-city').val()) {
               locationToSearch = $('.filterby-location-city').val();
               cityToSearch = '';
           } else {
               locationToSearch = $('.filterby-location').val();
           }
           url += '&localidad=' + locationToSearch;
       }
       if (categoryToSearch) {
           url += '&categoria=' + categoryToSearch;
       }
       url2 = url2 || url

       $('.loaderSpinner').fadeIn(400);

       $.ajax(url2, {
           dataType: 'json',
           success: function (results) {

               var html = '';
               if (results.count > 0) {
                   var pages = Math.ceil(results.count / 15);
                   if (pages > 1) {
                       renderPaginator(pages, url);
                       if (activePage) {
                           $(document).find('.paginator [data-page=' + activePage + ']').addClass('active');
                       } else {
                           $(document).find('.paginator [data-page=1]').addClass('active');
                       }
                   } else {
                       $('.navigationBenefits .paginator').remove();
                   }
                   $.each(results.results, function (index, benefit) {
                       specialSize = "";
                       if (benefit.descuento == "PROMO") {
                           specialSize = 'style="font-size: 3em !important; "';
                       }
                       html +=
                           '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">' +
                           '<div class="discount">' +
                           '<div class="left">' +
                           '<div class="img" style="background-image: url(' + benefit.foto_apaisada['240x80'] + ')">' +
                           '</div>' +
                           '</div>' +
                           '<div class="right">' +
                           '<div class="text">' +
                           '<h3 class="reduceText2Lines percentage"'+ specialSize +'>' + benefit.descuento + '</h3>' +
                           benefit.descripcion_breve +
                           '</div>' +
                           '</div>' +
                           '<a class="btn btn-primary show sm" href="show/' + benefit.id + '" data-benefit-id="' + benefit.id + '">' +
                           '<div class="inner">Quiero mi beneficio</div>' +
                           '</a>' +
                           '</div>' +
                           '</div>';
                   })
               } else {
                   $('.navigationBenefits .paginator').remove();
                   html = '<p class="noResults medium">No se han encontrado beneficios</p>';
               }


               $('.benefitsList').html(html);
               $('.benefitsList').animate({opacity: 1}, 400);
               $('.loaderSpinner').hide();
           },
           error: function(XHR, textStatus, errorThrown) {
               console.log("Response error ");
               console.error("error: " , textStatus);
               console.error("error: " , errorThrown);
           }
       })
   }, 5000);

}


function renderPaginator(pages, apiUrl) {
    pages = parseInt(pages);
    var html = '';
    var i;
    for (i = 1; i <= pages; i++) {
        var _href = encodeURI(apiUrl + '&count=9&page=' + i);
        html += '<a href="' + _href + '" data-page="' + i + '">' + i + '</a>';
    }

    var html = '<div class="clearfix"></div><div class="paginator"><ul class="list-inline list-unstyled">' + html + '</div>'
    $('.navigationBenefits .paginator').remove();
    $('.navigationBenefits').html(html);
}