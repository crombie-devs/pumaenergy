// ​function ver(p) {
function ver(p) {
    var flag = false;
    $.ajax({
        url: "register/resset/see/",
        data: {'pass': p},
        type: 'POST', // CAMBIAR A POST
        async: false,
        success: function (r) {
            if(r['noError'] == 1){
                flag = true; // true
            } else if(r['noError'] == 0){
                flag = false;
                $('p.pw-error').text(r['mensaje']).show();
            }
        }
    });
    return flag;
}
$(function () {
    $('form[name=resetting_change_password]').on('submit', function (e) {
        e.preventDefault();
        $('.form-control').on('focus', function () {
            $('p.pw-error').hide();
        })
        if (!(ver($('#resetting_change_password_plainPassword_first').val()))) {
            return false;
        }
        if ($('#resetting_change_password_plainPassword_first').val() == $('#resetting_change_password_plainPassword_second').val()) {
            this.submit();
        } else {
            $('p.pw-error').text('Las contraseñas no coinciden').show();
            return false;
        }
    });
    $('form[name=account]').on('submit', function (e) {
        e.preventDefault();
        $('.form-control').on('focus', function () {
            $('p.pw-error').hide();
        })
        if (!(ver($('#account_plainPassword_first').val()))) {
            return false;
        }
        if ($('#account_plainPassword_first').val() == $('#account_plainPassword_second').val()) {
            this.submit();
        } else {
            $('p.pw-error').text('Las contraseñas no coinciden').show();
            return false;
        }
    });
    $('form[name=user]').on('submit', function (e) {
        e.preventDefault();
        $('.form-control').on('focus', function () {
            $('p.pw-error').hide();
        })
        if ($('#user_employee_plainPassword_first').val() == '' && $('#user_employee_plainPassword_second').val() == '') {
            this.submit();
            return false;
        }
        if (!(ver($('#user_employee_plainPassword_first').val()))) {
            return false;
        }
        if ($('#user_employee_plainPassword_first').val() == $('#user_employee_plainPassword_second').val()) {
            this.submit();
        } else {
            $('p.pw-error').text('Las contraseñas no coinciden').show();
            return false;
        }
    })
})//# sourceMappingURL=pass.js.map
//# sourceMappingURL=pass.js.map
//# sourceMappingURL=pass.js.map
//# sourceMappingURL=pass.js.map